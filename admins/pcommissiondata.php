<?php
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
$conn = new \Classes\CONNECT();
?>

<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SFTailor || Admin</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
</head>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index" class="site_title"><img src="images/logo.png" style="height: 51px;"></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="images/img.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome</span>
                        <h2>Admin</h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">

                            <li><a href="administrator.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Administrator</a></li>
                            <li><a href="index.php?type=Coupons"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Coupons</a></li>
                            <li><a href="orders.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Customer Order</a></li>
                            <li><a href="stylegenie.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Customer Measurement</a></li>
                            <li><a href="levelcommission.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Commission Level</a></li>
                            <li><a href="commissionpersonal.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Commission Personal</a></li>
                            <li><a href="fabric.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Find Fabric</a></li>
                            <li><a href="fabricdabase.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Fabric Database</a></li>
                            <li><a href="ordersp.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Invoice</a></li>
                            <li><a href="index.php?type=price_list"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Price List</a></li>
                            <li><a href="reorder1.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Reorder Product</a></li>
                            <li><a href="paytostoreowner.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Refer Payment</a></li>
                            <li><a href="stores.php "><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Stylist Management</a></li>
                            <li><a href="supplier.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Supplier</a></li>
                            <li><a href="supplier_order.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Supplier Orders</a></li>
                            <li><a href="sizematching.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Size Matching</a></li>
                            <li><a href="allowance.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Style Genie Allowances </a></li>
                            <li><a href="sales-terms.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Sales Terms</a></li>
                            <li><a href="pdf.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Upload PDF</a></li>
                            <li><a href="users.php"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>User Management</a></li>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- top navigation -->
      <style>
            .ddkfxx ul li .p1 {
                font-weight: bold;
                width: 34% !important;
            }

            .ddkfxx td {
                border: 1px solid #999999;
                padding: 10px 100px 7px 11px;
            }
            .newddkfxx {
                padding: 28px 2px!important;
                width: 1149px !important;
            }
            .newddxq_tc {
                width: 1149px!important;
            }
            .newddkfxx th {
                font-size: 12px;
                font-weight: 500;
                text-align: center;
            }
            .saver{color: orangered;font-size: 12px;cursor: pointer;display: none;}
            .cancler{color: red;font-size: 12px;cursor: pointer;display: none;}
            .editors{color: brown;font-size: 12px;cursor: pointer;float:right;display: none;}
        </style>
        <!-- page content -->
        <script src="js/jquery.min.js"></script>
        <script src="js/location.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/moment.min.js"></script>
        <script src="js/custom.min.js"></script>
        <script src="js/jquery.dataTables.min.js"></script>
        <script src="js/bootstrap-toggle.min.js"></script>
        <script src="js/bootstrap-datetimepicker.js"></script>
        <script src="js/myscript.js"></script>
        <script src="../js/jquery-ui.js" type="text/javascript"></script>

        <link rel="stylesheet" type="text/css" href="css/defaulten.css">
        <div class="layui-layer-shade" id="layui-layer-shade5" times="5" style="z-index: 19891018; background-color: rgb(0, 0, 0); opacity: 0.7;" onclick="close_diolog()">
        </div>
        <div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5" type="page" times="5" showtime="0" contype="string" style="z-index: 19891019; top: 65px; height: 433px; width: 1017px; left: 180px;">
            <div class="layui-layer-title" style="cursor: move;color: green;font-weight: 600;" move="ok">Personal Commission</div>
            <div class="layui-layer-content order-detail-modal" style="height: 556px;">
                <div class="ddxq_tc">
                    <div class="ddkfxx">

                        <table class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Order Amount</th>
                                <th>Running Status</th>
                                <th>Monthly Status</th>
                                <th>Commission %</th>
                                <th>Earning Amount</th>
                                <th>Payment Status</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();//for scan2tailor
                            if ($link) {
                                $query1 = "select * from store_sales where store_id='".$_REQUEST["s"]."'";
                                $result = mysqli_query($link, $query1);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        while ($rows = mysqli_fetch_assoc($result)) {

                            ?>
                            <tr>
                                <td>1</td>
                                <td id="order_amount"><?php echo $rows['order_amount'];?></td>
                                <td id="monthly" ></td>
                                <td id="show_status"></td>
                                <td id="commission"></td>
                                <td id="commision_amount"></td>
                                <td>Not Paid</td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="float:right;">
                            <tr>
                                <td>Total Earnings </td>
                            </tr>
                            <tr>
                                <td style="font-weight: 600;color:#999;" id="paid_amount"></td>
                            </tr>
                        </table>
                    </div>
                    <?php
                                }
                            }
                        }
                    }

                    $link = $conn->connect();//for scan2tailor
                    if ($link) {
                        $query1 = "select * from wp_store where store_name='" . $_REQUEST["s"] . "'";
                        $result = mysqli_query($link, $query1);
                        if ($result) {
                            $num = mysqli_num_rows($result);
                            if ($num > 0) {
                                $rows = mysqli_fetch_assoc($result);
                                ?>
                                <input type='hidden' value="<?php echo $rows['start_month_bonus']; ?>" id="month_status">
                           <?php }
                        }
                    }
                    ?>
                </div>
                <table style="float: right; text-align: center; height: 38px; margin-right: 7%; width: 21%; color: white;">
                    <tr style="background: steelblue none repeat scroll 0% 0%; text-align: center; color: white;">
                        <td>Pay Now</td>
                    </tr>

                </table>
            </div>
        </div>

</body>
</html>

<script>
function close_diolog() {
    window.location = "paytostoreowner.php";
}

function get_personal_commission_details() {
    var get_status = $("#month_status").val();
    $("#show_status").text(get_status);
    var url = "api/paymentProcess.php?type=personal&month_status="+get_status;
    $.get(url, function (data) {
        var json = $.parseJSON(data)
        {
            var paymentData = json.personaldata;
            var status = json.status;
            if (status == "done") {
                var monthly = paymentData.monthly_status;
                var commission = paymentData.commission;
                var order_amount = $("#order_amount").text();
                $("#monthly").html(monthly);
                var commissions = $("#commission").html(commission);
                var after_commission = order_amount*commission/100;
                $("#commision_amount").html(after_commission);
                $("#paid_amount").html(after_commission);


            }
            else {
                alert(status);
            }
        }
    });
}

get_personal_commission_details();
</script>
