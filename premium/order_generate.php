<?php
error_reporting(0);
include('header.php');
require_once('../admins/api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
$allUsersData = $userClass->getAllUsersData();
?>
<style>
    .shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }

    .hideloader {
        display: none;
    }

    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }

    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }

    #create_stores {
        background: white none repeat scroll 0% 0%;
        padding: 37px 25px !important;
        border-radius: 4px;
        top: -100px;
        z-index: 999999;
    }

    @media print {
        body * {
            visibility: hidden;
        }

        #section-to-print, #section-to-print * {
            visibility: visible;
        }

        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }

    .userMgmtBoxes {
        display: none;
    }
</style>
<!-- page content -->
<div id="loadd" class="loaderr hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Order Generate
                            <small></small>
                        </h2>

                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px"
                                        onclick="window.location='api/excelProcess.php?dataType=allOrders'"
                                        class="btn btn-info btn-sm">Download Excel File
                                </button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control"
                                               name="startDate" id="startFilter"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate"
                                               id="endFilter"/>
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton"
                                           style="margin-top: 5px"/>
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>


                    <?php if ($_REQUEST["type"] == "edit") { ?>
                        <div class="shadoows" onclick="close_diolog()" style="display: block;"></div>
                        <div class="col-md-12 create_stores" id="create_stores" style="padding: 0;display: block;">
                            <?php
                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $userId = $_REQUEST['userId'];
                                $query = "select * from wp_customers where user_id='$userId'";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $orderDatas = mysqli_fetch_assoc($result);
                                        $name = $orderDatas['name'];
                                        $stylist = $orderDatas['stylist'];
                                        $fname = explode(" ", $name)[0];
                                        $lname = explode(" ", $name)[1];
                                    }
                                }
                            }
                            ?>
                            <form method="Post" enctype="multipart/form-data" id="update_user">
                                <div class="form-group col-md-4">
                                    <label>First Name</label>
                                    <input type="text" name="user_fname" class="form-control"
                                           value="<?php echo $fname; ?>"
                                           placeholder="Enter Your First Name" required="true"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Last Name</label>
                                    <input type="text" name="user_lname" class="form-control"
                                           value="<?php echo $lname; ?>" placeholder="Enter Your Last Name"
                                           required="true"/>
                                </div>
                                <input type="hidden" name="userId" value="<?php echo $userId; ?>">
                                <div class="form-group col-md-4">
                                    <label>E-Mail</label>
                                    <input type="text" name="user_email" class="form-control"
                                           value="<?php echo $orderDatas['email']; ?>"
                                           placeholder="Enter Your E-Mail Address" required="true"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Password</label>
                                    <input type="text" name="user_password" class="form-control"
                                           value="<?php echo $orderDatas['password']; ?>"
                                           placeholder="Enter Your Password" required="true"/>
                                    <input type="hidden" name="store_status" value="0">
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Contact</label>
                                    <input type="text" name="contact_name" class="form-control"
                                           value="<?php echo $orderDatas['mobile']; ?>"
                                           placeholder="Enter Your Contact Number" required="true"/>
                                </div>

                                <div class="form-group col-md-4" style="margin-bottom: 40px; margin-top: 12px;">
                                    <label>Stylist</label>
                                    <select class="form-control" name="stylist">
                                        <option selected="" value="">Select Stylist</option>
                                        <?php
                                        $link = $conn->connect();
                                        if ($link) {
                                            $query = "select * from wp_store order by store_id DESC";
                                            $result = mysqli_query($link, $query);
                                            if ($result) {
                                                $num = mysqli_num_rows($result);
                                                if ($num > 0) {
                                                    while ($orderData = mysqli_fetch_array($result)) {
                                                        $bottm = $orderData['store_name'];
                                                        if ($stylist == $bottm) {
                                                            $store_name = $orderData['store_name'];
                                                            $selected = "selected='selected'";
                                                        } ?>

                                                        <option class="cc_<?php echo $orderData['store_name']; ?>"
                                                                value="<?php echo $orderData['store_name']; ?>"><?php echo $orderData['store_name']; ?></option>
                                                    <?php } ?>

                                                    <option <?php echo $selected; ?>
                                                            value="<?php echo $store_name ?>"><?php echo $store_name ?></option>

                                                <?php }
                                            }
                                        }
                                        ?>
                                    </select>
                                    <style>
                                        .cc_<?php echo $username ?> {
                                            display: none;
                                        }
                                    </style>
                                </div>

                                <div style='clear:both'></div>

                                <div class="form-group">
                                    <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Update User Info
                                    </button>
                                </div>
                            </form>
                        </div>
                    <?php } ?>

                    <div class="userDataBox">
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                List of Available Users
                            </p>
                            <table id="orderTable" class="table table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Profile Pic</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Email</th>
                                    <th>Password</th>
                                    <th>Contact</th>
                                    <th>Gender</th>
                                    <th>Stylist</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $j = 0;
                                $allusersData = $allUsersData['usersData'];
                                for ($i = 0; $i < count($allusersData); $i++) {
                                    $j++;
                                    ?>
                                    <tr>
                                        <td data-title='#'><?php echo $j ?></td>
                                        <td><img src="<?php echo $allusersData[$i]['profile_pic']; ?>"
                                                 style="width:20px;"></td>
                                        <td><?php echo explode(" ", $allusersData[$i]['name'])[0]; ?></td>
                                        <td><?php echo explode(" ", $allusersData[$i]['name'])[1]; ?></td>
                                        <td><?php echo $allusersData[$i]['email']; ?></td>
                                        <td><?php echo $allusersData[$i]['password']; ?></td>
                                        <td><?php echo $allusersData[$i]['mobile']; ?></td>
                                        <td><?php echo $allusersData[$i]['gender']; ?></td>
                                        <td><?php echo $allusersData[$i]['stylist']; ?></td>
                                        <td>
                                            <a href="order_customize.php?userId=<?php echo $allusersData[$i]['user_id']; ?> ">
                                                <button class="btn btn-sm btn-info">
                                                    Order Generate
                                                </button>
                                            </a>
                                        </td>
                                    </tr>
                                <?php } ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="createUserBox" style="display: none">
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                Create User
                            </p>
                            <div class="col-md-12"
                                 style="box-shadow:0 0 10px #333;border-radius:5px;background:rgba(255,255,255,0.7)"
                                 id="signup_form">
                                <form method="Post" enctype="multipart/form-data" id="registerform">
                                    <div class="row cardview" style="">
                                        <p id="errorhere" style="color:red;font-size:16px;padding:10px"></p>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>First Name</label>
                                                <input placeholder="First Name" name="fname" class="form-control"
                                                       type="text"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Last Name</label>
                                                <input placeholder="Last Name" name="lname" class="form-control"
                                                       type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>Select Country</label>
                                                <select name="b_country" class="countries form-control" id="countryId"
                                                        onchange='datedate()'>
                                                    <option value="">Select Country</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Select State</label>
                                                <select name="b_state" class="states form-control" id="stateId">
                                                    <option value="">Select State</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Select City</label>
                                                <div class="col-md-12" id="cityfeild" style="padding:0;">
                                                    <select name="b_city" class="cities form-control" id="cityId">
                                                        <option value="">Select City</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label style="margin-top:10px">D.O.B</label>
                                                <input id="inputdob" name="dob" placeholder="Enter D.O.B"
                                                       class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label style="">Choice Menu</label>
                                                <select name="choice_menu" class="choice_menu form-control"
                                                        id="choice_menu">
                                                    <option value="">Select Choice Menu</option>
                                                    <option value="customer">Customer</option>
                                                    <option value="prospect">Prospect</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Profile Picture</label>
                                                <input name="file0" class="form-control" type="file">
                                            </div>
                                            <div class="form-group">
                                                <label>Enter Height</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select name="heightparam" class="form-control"
                                                                id="height_parameter" onchange='height_param()'>
                                                            <option value="Inches">Inches</option>
                                                            <option value="Centimeters">Centimeters</option>
                                                            <option value="Feets">Feets</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6" id='heightdiv'>
                                                        <input name='height' class='form-control'
                                                               placeholder="Enter Height"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>E-Mail</label>
                                                <input placeholder="Email" required name="email" class="form-control"
                                                       type="email">
                                            </div>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input placeholder="Password" required id="password" name="password"
                                                       class="form-control" type="password">
                                            </div>
                                            <div class="form-group">
                                                <label>Confirm Password</label>
                                                <input placeholder="Repeat Password" required id="cpassword"
                                                       class="form-control" type="password">
                                            </div>
                                            <div class="form-group">
                                                <label>Contact</label>
                                                <input placeholder="Mobile No" id="mobile_no" name="mobile_no"
                                                       class="form-control" type="text">
                                            </div>
                                            <div class="form-group">
                                                <label>Stylist</label>
                                                <select class="form-control" name="stylist">
                                                    <option selected="" value="">Select Stylist</option>
                                                    <?php
                                                    $link = $conn->connect();
                                                    if ($link) {
                                                        $query = "select * from wp_store order by store_id DESC";
                                                        $result = mysqli_query($link, $query);
                                                        if ($result) {
                                                            $num = mysqli_num_rows($result);
                                                            if ($num > 0) {
                                                                while ($orderData = mysqli_fetch_array($result)) {
                                                                    ?>
                                                                    <option selected=""
                                                                            value="<?php echo $orderData['store_name']; ?>">
                                                                        <?php echo $orderData['store_name']; ?>
                                                                    </option>
                                                                    <?php
                                                                }
                                                            }
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Select Gender</label>
                                                <select class="form-control" name="gender">
                                                    <option selected="" value="">Select Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Language</label>
                                                <select class="form-control" name="lang">
                                                    <option selected="" value="">Select Language</option>
                                                    <option value="English(US)">English(US)</option>
                                                    <option value="English(UK)">English(UK)</option>
                                                    <option value="French">French</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Weight</label>
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <select name="weightparam" class="form-control">
                                                            <option selected="" value="">Parameter</option>
                                                            <option value="Kilograms">Kilograms</option>
                                                            <option value="Pounds">Pounds</option>
                                                            <option value="Stones">Stones</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <input type='text' name='weight' class='form-control'
                                                               placeholder='Enter Weight'/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label>ZipCode</label>
                                                <input type="text" placeholder="Enter ZipCode" name="zipcode"
                                                       class="form-control"/>
                                            </div>
                                            <div class="form-group">
                                                <label>Address / Street No.</label>
                                                <textarea placeholder="Enter Address" name="address"
                                                          class="form-control"></textarea>
                                            </div>
                                            <div class="form-group" style="margin:0px">
                                                <input value="Register" class="btn btn-danger pull-right"
                                                       style="width:200px" type="submit">
                                            </div>
                                            <div class="row" style="margin-top:10px">
                                                <input value="sftailor" name="site_name" type="hidden">
                                                <input value="Website" name="app_type" type="hidden">
                                            </div>
                                        </div>
                                        <div style="clear:both"></div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css"/>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function switchBox(boxValue) {
        if (boxValue == "customer_create") {
            $(".userDataBox").hide();
            $(".createUserBox").show();
        }
        else {
            $(".userDataBox").show();
            $(".createUserBox").hide();
        }
    }

    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });

    function confirmDelete_user(user_id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display", "block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
            'class="btn btn-primary" data-dismiss="modal"> Close</button>' + '<button type="button" class="btn btn-danger" ' +
            'onclick=delUser("' + user_id + '")>Delete</button>');
        $("#myModal").modal("show");
    }

    function delUser(user_id) {
        var url = "api/userProcess.php";
        $.post(url, {"type": "deleteUser", "user_id": user_id}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                window.location = "users.php";
            }
        });
    }

    $("#update_user").submit(function (e) {
        e.preventDefault();
        $('.loader').fadeIn("slow");
        $.ajax({
            url: "update_users.php?type=update",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                $('.loader').fadeOut("slow");
                var json = $.parseJSON(data)
                {
                    var status = json.status;
                    var message = json.message;
                    if (status == "done") {
                        alert(message);
                        window.location = "users.php";
                    }
                    else {
                        alert(message);
                    }
                }
            }
        });
    });

    function close_diolog() {
        window.location = "users.php"
    }

    /*user create starrt here*/
    $("#registerform").submit(function (e) {
        e.preventDefault();
        $('.loaderr').removeClass("hideloader");
        $.ajax({
            url: "../admin/webserver/admin-signup.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                $('.loaderr').addClass("hideloader");
                var json = $.parseJSON(data);
                var status = json.status;
                var message = json.message;
                if (status == "Registered Successfully") {
                    var user_id = json.user_id;
                    showMessage(status, "green");
                    setTimeout(function () {
                        window.location = 'customer_step_two.php?userId=' + user_id;
                    }, 3000);
                } else {
                    $('.loaderr').addClass("hideloader");
                    $(".modal-title").html("<span>Error Occured</span>");
                    $(".modal-body").html("<span style='color:red'>" + status + "</span>");
                    $("#myModal").modal("show");
                }
            }
        });
    });

    function height_param() {
        var height_param_val = $("#height_parameter").val();
        if (height_param_val == "Feets") {
            $("#heightdiv").html("<div class='col-md-6' style='padding:0'><input name='height0' class='form-control' placeholder='Enter Feets' /></div><div class='col-md-6' style='padding:0'><input name='height1' class='form-control' placeholder='Enter Inches' /></div>");
        }
        else if (height_param_val == "Centimeters") {
            $("#heightdiv").html("<input name='height' class='form-control' placeholder='Enter Height' />");
        }
        else {
            $("#heightdiv").html("<input name='height' class='form-control' placeholder='Enter Height' />");
        }
    }

    /* $("#inputdob").datetimepicker({
         format: "MM/DD/YYYY",
         minDate: today
     });*/
    function datedate() {
        var country = $("#countryId").val();
        if (country == "United States") {
            $('#inputdob').datetimepicker({
                format: "MM/DD/YYYY"
            });
        } else {
            $('#inputdob').datetimepicker({
                format: "DD/MM/YYYY"
            });
        }
    }

    /*user create end  here*/
</script>
