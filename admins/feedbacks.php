<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
    <style>
        .cop-price {
            float: left;
            height: 28px;
            width: 55px;
        }

        .couponForCheck {
            margin: 0 10px !important;
        }
        .loaderdiv {
            background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
            height: 700px;
            position: fixed;
            width: 100%;
            z-index: 99999;
            display: block;
        }
        .block {
            display: block !important;
        }
        .loaderhidden {
            display: none !important;
        }
        .loaderdiv > img {
            height: 50px;
            margin: 300px 50%;
            width: 50px;
        }
    </style>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">

        </div>
        <div class="row" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                                <div class="x_title">
                                    <h2><span style="color:#1ABB9C;"></span>
                                        Feedbacks
                                        <small></small>
                                    </h2>
                                    <ul class="nav navbar-right panel_toolbox">
                                        <li>
                                            <div class="form-group form-inline">
                                                <input type="button" class="btn btn-danger btn-sm" onclick="addFeedback()"
                                                value=" + Add Feedback" />
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        From here admin can manage/modify the content of the feedbacks
                                    </p>
                                    <div id="feedbackData">

                                    </div>

                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function addFeedback(){
        $(".modal-title").html("<label style='color: green'>Add New Feedback</label>");
        $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='feedError'" +
            " style='color:red'></p><div class='col-md-6 form-group'><label>User Name</label>" +
            "<input type='text' class='form-control' id='userName' placeholder='user name' /></div>" +
            "<div class='col-md-6 form-group'><label>User From</label>" +
            "<input type='text' id='userFrom' class='form-control' placeholder='user from' /></div>" +
            "<div class='col-md-6 form-group'><label>Feedback Title</label>" +
            "<textarea rows='2' placeholder='feedback title' class='form-control' id='feedbackTitle' ></textarea></div>" +
            "<div class='col-md-6 form-group'><label>Feedback Description</label>" +
            "<textarea rows='2' placeholder='feedback description' class='form-control' id='feedbackDescription' ></textarea></div>" +
            "<div class='col-md-12'><input type='button' class='btn btn-danger pull-right' style='margin-top:30px' onclick='feedbackAdd()'" +
            " id='subbtn' value='Submit'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
        $(".modal-footer").css('display', 'none');
        $("#myModal").modal("show");
    }
    function feedbackAdd(){
        var userName = $("#userName").val();
        var userFrom = $("#userFrom").val();
        var feedbackTitle = $("#feedbackTitle").val();
        var feedbackDescription = $("#feedbackDescription").val();
        if(userName == "" || userFrom == "" || feedbackTitle == "" || feedbackDescription == ""){
            $("#feedError").html("please fill all required fields");
            $("#feedError").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "feedback",'name':userName,'from':userFrom,'feedback_text':feedbackTitle,
            'feedback_description':feedbackDescription}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status == "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getFeedbackData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
        $(".modal-backdrop").modal("hide");

    }



    function getFeedbackData(){
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "getFeedbackData"}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var feedData = data.feedData;
             if (Status == "Success") {
                 var tbodyData ="";
                 var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                     '<thead><tr><th>#</th><th>User Name</th><th>From</th><th>Feedback Title</th><th>Feedback Description</th>' +
                     '<th>Added on</th><th>Visibility</th><th>Action</th></tr></thead>';

                 for(var a=0; a < feedData.length; a++){
                     var feedback_status = feedData[a].feedback_status;
                     if(feedback_status == "1"){
                         var selectStatus = "<select id='updateFeedStatus_"+a+"' style='width: 108px; height: 28px; margin-top: 3px;'" +
                             " onchange=updateFeedStatus('"+feedData[a].feed_id+"','"+a+"');>" +
                             "<option selected value='"+feedback_status+"'>Visible</option>" +
                             "<option value='0'>Not Visible</option></select>";
                     }
                     else{
                         var selectStatus = "<select id='updateFeedStatus_"+a+"' style='width: 108px; height: 28px; margin-top: 3px;'" +
                             " onchange=updateFeedStatus('"+feedData[a].feed_id+"','"+a+"');>" +
                             "<option  value='1'>Visible</option>" +
                             "<option selected  value='0'>Not Visible</option></select>";
                     }
                     tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td>"+feedData[a].f_name+"</td>" +
                         "<td>"+feedData[a].f_from+"</td><td>"+(feedData[a].feedback_text).substr(0,30)+"...</td>" +
                         "<td>"+(feedData[a].feedback_description).substr(0,30)+"...</td><td>"+feedData[a].created_date+"</td>" +
                         "<td>"+selectStatus+"</td><td><a  onclick=confirmDelete_feedback('"+feedData[a].feed_id+"'); style='float: right; font-size: 18px;padding: 2px 4px; " +
                         "background: red none repeat scroll 0% 0%; color: white; margin-left: 5px;'><i class='fa fa-trash-o'></i>" +
                         "</a>" + "<a  onclick=getFeedbackPart('"+feedData[a].feed_id+"'); style='float: right; font-size: 18px;padding: 2px 4px; " +
                         "background: red none repeat scroll 0% 0%; color: white;'><i class='fa fa-pencil'></i>" +
                         "</a></td></tr>";
                 }
                 $("#feedbackData").html(table+"<tbody>"+tbodyData+"</tbody></table>");
                 $(".loaderdiv").addClass("loaderhidden");
             }
             else{
                 $(".loaderdiv").addClass("loaderhidden");
                 showMessage(Message,"red");
             }
        });
    }
    getFeedbackData();
    function getFeedbackPart(feedId){
        var url = "api/userProcess.php";
        $.post(url, {"type": "getPartFeedback", "feedId": feedId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var feedData = data.feedData;
            if (Status == "Success") {
                $(".modal-title").html("<label style='color: green'>Update Feedback</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='feedError'" +
                    " style='color:red'></p><div class='col-md-6 form-group'><label>User Name</label>" +
                    "<input type='text' class='form-control' id='userName' value='"+feedData.f_name+"' placeholder='user name' /></div>" +
                    "<div class='col-md-6 form-group'><label>User From</label>" +
                    "<input type='text' id='userFrom' class='form-control' placeholder='user from' value='"+feedData.f_from+"' /></div>" +
                    "<div class='col-md-6 form-group'><label>Feedback Title</label><textarea rows='2' placeholder='feedback title' " +
                    "class='form-control' id='feedbackTitle' >"+feedData.feedback_text+"</textarea></div><div class='col-md-6 form-group'>" +
                    "<label>Feedback Description</label><textarea rows='2' placeholder='feedback description' class='form-control' " +
                    "id='feedbackDescription' >"+feedData.feedback_description+"</textarea></div><div class='col-md-12'>" +
                    "<input type='button' class='btn btn-danger pull-right' style='margin-top:30px' onclick=updateFeedback('"+feedData.feed_id+"');" +
                    " id='subbtn' value='Update'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function updateFeedback(feedId){
        var userName = $("#userName").val();
        var userFrom = $("#userFrom").val();
        var feedbackTitle = $("#feedbackTitle").val();
        var feedbackDescription = $("#feedbackDescription").val();
        if(userName == "" || userFrom == "" || feedbackTitle == "" || feedbackDescription == ""){
            $("#feedError").html("please fill all required fields");
            $("#feedError").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updateFeedBack",'name':userName,'from':userFrom,'feedback_text':feedbackTitle,
            'feedback_description':feedbackDescription,"feedId":feedId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status == "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getFeedbackData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
        $(".modal-backdrop").modal("hide");
    }
     function updateFeedStatus(feedId,a){
         var feedValue = $("#updateFeedStatus_"+a).val();
         var url = "api/userProcess.php";
         $(".loaderdiv").removeClass("loaderhidden");
         $.post(url, {"type": "updateFeedStatus", "feedId": feedId, "feedValue": feedValue}, function (data) {
             var Status = data.Status;
             var Message = data.Message;
             if (Status == "Success") {
                 getFeedbackData();
             }
             else{
                 showMessage(Message,"red");
                 $(".loaderdiv").addClass("loaderhidden");
             }
         });
     }
    function confirmDelete_feedback(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
            'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
            'onclick=delFeedback("'+id+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function delFeedback(feedId) {
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "deleteFeedback", "feedId": feedId}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                $("#myModal").modal("hide");
                getFeedbackData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
</script>
