<?php
include("header.php");
include("front_header.php");
?>
<style>
    .top-bar {
        background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
    }
    .nav-md .container.body .right_col{
        padding: 10px 0 0;
    }
</style>
<div class="right_col" role="main">
    <div class="about-bottom wthree-3" style="padding: 0px">
        <div class="container-fluid" style="padding: 0px">
            <h2 class="tittle" style="margin-top: 50px; font-weight: 600;">order now</h2>
            <div class="agileinfo_about_bottom_grids">
                <div class="col-md-12 agileinfo_about_bottom_grid" style="padding: 0px">
                    <div class="col-md-12" style="padding: 0px">
                        <div class="col-md-3">
                            <img id="left" src="../images/coat.jpg" style="width: 260px"/>
                        </div>
                        <div class="col-md-4 custom_center">
                            <h2>Bespoke Menswear</h2>
                            <h2>Leisure, Business and Formal</h2>
                        </div>
                        <div class="col-md-5" style="padding: 0 1px 0 10px">
                            <div class="col-md-6 custom_right"
                                 onclick="window.location='order_startjacket.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Jacket</h1>
                                <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true"
                                                                            style="margin-left:10px"></i></p>
                            </div>
                            <div class="col-md-6 custom_right" onclick="window.location='order_startVest.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Outfits</h1>
                                <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true"
                                                                           style="margin-left:10px"></i></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 custom_right" onclick="window.location='order_startGift.php?userId=<?php echo $user_id; ?>'"
                                 style="float: right;margin-right: 5%;">
                                <h1 class="cstm_hdr_btn" style="font-size: 26px;">Gift Certificate</h1>
                                <p class="button_caption">Our Collection<i class="fa fa-gift" aria-hidden="true"
                                                                           style="margin-left:10px"></i></p>
                            </div>

                            <div class="col-md-6 custom_right" onclick="window.location='order_startVest.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Vest</h1>
                                <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true"
                                                                            style="margin-left:10px"></i></p>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-6 custom_right" onclick="window.location='order_startpant.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Pants</h1>
                                <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true"
                                                                            style="margin-left:10px"></i></p>
                            </div>

                            <div class="clearfix"></div>
                            <div class="col-md-6 custom_right" onclick="window.location='order_startshirt.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Shirts</h1>
                                <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true"
                                                                            style="margin-left:10px"></i></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 custom_right"
                                 onclick="window.location='order_startjacketcollection.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Overcoat</h1>
                                <p class="button_caption">Design yourself
                                    &nbsp;&nbsp;&nbsp;<i class="fa fa-caret-right" aria-hidden="true"></i></p>
                            </div>
                            <div class="col-md-6 custom_right"
                                 onclick="window.location='order_startconfigovercoat.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Overcoat </h1>
                                <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true"
                                                                           style="margin-left:10px"></i></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 custom_right" onclick="window.location='order_accessories.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Accessories</h1>
                                <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true"
                                                                           style="margin-left:10px"></i></p>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 custom_right" onclick="window.location='order_stylebox.php?userId=<?php echo $user_id; ?>'">
                                <h1 class="cstm_hdr_btn">Style Box</h1>
                                <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true"
                                                                           style="margin-left:10px"></i></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>
<?php include("footer.php"); ?>

<script>
    function preference(selection) {
        var url = "../admin/webserver/selectionData.php?preference=" + selection;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                window.location = selection + ".php";
            }
            else {
                alert(status);
            }
        }).fail(function () {
            alert("page is not available");
        });
    }
    var user_id = $("#user_id").val();
    getCart(user_id);
</script>



