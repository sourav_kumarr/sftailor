<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 05-12-2017
 * Time: 11:35
 */

namespace Classes;


class MESSAGE {
    public $link = null;
    public $msg_id = null;
    public $msg_text = null;
    public $msg_desc = null;
    public $msg_attachment = null;
    public $msg_signature = null;
    public $msg_date = null;
    public $msg_broadcast_date = null;
    public $response = array();

    public function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    /**
     * @param false|string $currentDateTime
     */
    public function setCurrentDateTime($currentDateTime)
    {
        $this->currentDateTime = $currentDateTime;
    }

    /**
     * @param false|int $currentDateTimeStamp
     */
    public function setCurrentDateTimeStamp($currentDateTimeStamp)
    {
        $this->currentDateTimeStamp = $currentDateTimeStamp;
    }

    /**
     * @param null $msg_attachment
     */
    public function setMsgAttachment($msg_attachment)
    {
        $this->msg_attachment = $msg_attachment;
    }

    /**
     * @param null $msg_date
     */
    public function setMsgDate($msg_date)
    {
        $this->msg_date = $msg_date;
    }

    /**
     * @param null $msg_desc
     */
    public function setMsgDesc($msg_desc)
    {
        $this->msg_desc = $msg_desc;
    }

    /**
     * @param null $msg_id
     */
    public function setMsgId($msg_id)
    {
        $this->msg_id = $msg_id;
    }

    /**
     * @param null $msg_signature
     */
    public function setMsgSignature($msg_signature)
    {
        $this->msg_signature = $msg_signature;
    }

    /**
     * @param null $msg_text
     */
    public function setMsgText($msg_text)
    {
        $this->msg_text = $msg_text;
    }

    /**
     * @param null $msg_broadcast_date
     */
    public function setMsgBroadcastDate($msg_broadcast_date)
    {
        $this->msg_broadcast_date = $msg_broadcast_date;
    }

    /*
     * this will save all message data...
     *
     * */

    public function save() {
       $link = $this->link->connect();
       if(!$link) {
         $this->response[Status] = Error;
         $this->response[Message] = "Unable to connect with database";
         return $this->response;
       }
       $messages = $this->getAllMessages();
       if($messages[Status] === Success) {
           date_default_timezone_set("America/New_York");
           $msgData = $messages["data"][0]["msg_broadcast_date"];
//           $date = date("Y-m-d h:i:s A",$msgData);
           $this->msg_broadcast_date = date("Y-m-d h:i:s A",strtotime(date("Y-m-d h:i:s A", strtotime($msgData)) . " +1 week"));
       }
       $query = "insert into wp_messages(msg_text, msg_desc, msg_attachment, msg_signature,msg_broadcast_date) values('$this->msg_text','$this->msg_desc','$this->msg_attachment','$this->msg_signature','$this->msg_broadcast_date')";
       $result = mysqli_query($link,$query);
       if(!$result) {
           $this->response[Status] = Error;
           $this->response[Message] = "Unable to insert record ".$this->link->sqlError();
           return $this->response;

       }
       $id_ = mysqli_insert_id($link);
       if($id_ === 0) {
           $this->response[Status] = Error;
           $this->response[Message] = "Unable to insert records ";
           return $this->response;
       }
        $this->response[Status] = Success;
        $this->response[Message] = "Message added successfully";
        $this->response["msg_id"] = $id_;

        return $this->response;

    }

    /*
     * this will update message data..
     *
     * */

    public function update() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "update wp_messages set msg_desc='$this->msg_desc',msg_text='$this->msg_text' where msg_id='$this->msg_id'";
        $result = mysqli_query($link,$query);
        if(!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to insert record ".$this->link->sqlError();
            return $this->response;

        }
        $id_ = mysqli_affected_rows($link);
        if($id_ === 0) {
            $this->response[Status] = Error;
            $this->response[Message] = "No new changes found";
            return $this->response;
        }
        $this->response[Status] = Success;
        $this->response[Message] = "Message updated successfully";
//        $this->response["msg_id"] = $id_;

        return $this->response;

    }

    /*
     * this will get all messages data . .
     *
     * */

    public function getAllMessages() {
        $link = $this->link->connect();
        $data = array();

        if(!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "select * from wp_messages order by msg_id desc";
        $result = mysqli_query($link,$query);
        if(!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to insert record ".$this->link->sqlError();
            return $this->response;

        }
        $rows = mysqli_num_rows($result);
        if($rows === 0) {
            $this->response[Status] = Error;
            $this->response[Message] = "Data not found ";
            return $this->response;
        }
        while($items = mysqli_fetch_assoc($result)) {
            $data[] = $items;
        }
        $this->response[Status] = Success;
        $this->response[Message] = "Data found";
        $this->response["data"] = $data;

//        $this->response["msg_id"] = $id_;

        return $this->response;
    }

    public function getParticularMessage() {
        $link = $this->link->connect();

        if(!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "select * from wp_messages where msg_id='$this->msg_id'";

        $result = mysqli_query($link,$query);
        if(!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to get record ".$this->link->sqlError();
            return $this->response;

        }
        $rows = mysqli_num_rows($result);

        if($rows === 0) {
            $this->response[Status] = Error;
            $this->response[Message] = "Data not found ";
            return $this->response;
        }
        $row = mysqli_fetch_assoc($result);
        $this->response[Status] = Success;
        $this->response[Message] = "Data found";
        $this->response["data"] = $row;
        return $this->response;
    }

    public function removeMessage() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "delete from wp_messages where msg_id='$this->msg_id'";
        $result = mysqli_query($link,$query);
        if(!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to remove record ".$this->link->sqlError();
            return $this->response;

        }
        mysqli_query($link,"delete from wp_broadcasts where broad_msg_id='$this->msg_id'");
        $id_ = mysqli_affected_rows($link);
        if($id_ === 0) {
            $this->response[Status] = Error;
            $this->response[Message] = "Record is already deleted";
            return $this->response;
        }
        $this->response[Status] = Success;
        $this->response[Message] = "Message removed success";
        return $this->response;

    }

    public function sendMessages($ids,$msg,$subject) {
        require 'SMTP/PHPMailerAutoload.php';
        $ids = explode(",",$ids);
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'scan2fit.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'scan2fitsftailor@scan2fit.com';
        $mail->Password = 'sftailor';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom('noreply@sftailor.com', 'SFTailor');   // sender
        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $msg;
        foreach($ids as $email) {
            $mail->addAddress($email);
        }
        if(!$mail->send()) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to send email";
            return $this->response;
        }
        $this->response[Status] = Success;
        $this->response[Message] = "Email is sent successfully";
        return $this->response;
    }

    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}