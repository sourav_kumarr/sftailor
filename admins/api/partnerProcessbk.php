<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:42 PM
 */

require_once('Classes/PARTNER.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$partnerClass = new \Classes\PARTNER();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $partnerClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
 if($type === 'getCategory') {
    $logoResponse = $partnerClass->getCategory();
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'addPartner') {
    $requiredfields = array('fairTitle','fairLink','catPartner');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $fairTitle = $_REQUEST['fairTitle'];
    $fairLink = $_REQUEST['fairLink'];
    $catPartner = $_REQUEST['catPartner'];
    $logoResponse = $partnerClass->addPartner($fairTitle,$fairLink,$catPartner);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'editPartner') {
    $requiredfields = array('pid','fairTitle','fairLink','catPartner','oldEventImagechanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $pid = $_REQUEST['pid'];
    $fairTitle = $_REQUEST['fairTitle'];
    $fairLink = $_REQUEST['fairLink'];
    $catPartner = $_REQUEST['catPartner'];
    $oldEventImagechanged   = $_REQUEST['oldEventImagechanged'];
    $logoResponse = $partnerClass->editPartner($pid,$fairTitle,$fairLink,$catPartner,$oldEventImagechanged);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'getPartner') {
    $logoResponse = $partnerClass->getPartner();
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'getParticularPartner') {
     $requiredfields = array('catName');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $catName = $_REQUEST['catName'];
    $logoResponse = $partnerClass->getParticularPartner($catName);
    $partnerClass->apiResponse($logoResponse);
}

else if($type === 'deletePartner') {
    $requiredfields = array('id','path');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $id = $_REQUEST['id'];
    $path = $_REQUEST['path'];
    $logoResponse = $partnerClass->deletePartner($id,$path);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'updateCategory') {
    $requiredfields = array('newCatName','oldCatName');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $newCatName = $_REQUEST['newCatName'];
    $oldCatName = $_REQUEST['oldCatName'];
    $logoResponse = $partnerClass->updateCategory($newCatName,$oldCatName);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'deleteCategory') {
    $requiredfields = array('catName');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $catName = $_REQUEST['catName'];
    $logoResponse = $partnerClass->deleteCategory($catName);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'updateSortOrder') {
    $requiredfields = array('sortOrder','idOrder');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $sortOrder = $_REQUEST['sortOrder'];
    $idOrder = $_REQUEST['idOrder'];
    $logoResponse = $partnerClass->updateSortOrder($sortOrder,$idOrder);
    $partnerClass->apiResponse($logoResponse);
}
?>