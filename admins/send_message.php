<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 05-12-2017
 * Time: 20:27
 */

include("header.php");
require_once ('api/Classes/USERCLASS.php');
require_once('api/Classes/MESSAGE.php');
require_once ('api/Constants/functions.php');
require_once('api/Constants/configuration.php');
require_once('api/Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();
$userResponse = $userClass->getAllUsersData();
$status = $userResponse[Status];
$users = $userResponse["usersData"];

$msgClass = new \Classes\MESSAGE();
$id = base64_decode($_REQUEST['id']);
$msgClass->setMsgId($id);
$response = $msgClass->getParticularMessage();

$msg_text = $response['data']["msg_text"];
$msg_desc = $response['data']["msg_desc"];
$msg_id = $response['data']["msg_id"];
$msg_signature = $response['data']["msg_signature"];
echo "<input type='hidden' id='msg_txt' value='".$msg_text."'/>";
echo "<input type='hidden' id='msg_desc' value='".$msg_desc."'/>";

?>
<link rel="stylesheet" href="css/create_message.css"/>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="msg-body msg-body-1 active">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title" style="padding-bottom:2px; ">
                        <label style="font-size: 25px;letter-spacing: 1px;cursor: pointer" onclick="window.location='draft_message.php'"><i class="fa fa-arrow-circle-left"></i> Mass Email</label>
                        <button class="btn btn-primary btn-sm pull-right" onclick="send_maill();"> <i class="fa fa-envelope"></i>&nbsp;&nbsp;Send</button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-gutter ">
                        <div class="col-md-8 col-sm-8 col-xs-8 no-gutter">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter msgs-header">
<!--                                <label style="margin-top: 7px;">All Subscribers</label>-->
                                <div class="checkbox-inline" style="margin-top: 7px;">
                                    <input type="checkbox" value="select_all"
                                           id="CheckboxSelectAll" onclick="selected_checkbox(this);"/>
                                    <label for="CheckboxSelectAll">Select All</label>
                                </div>

                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter user-container">
                                <!--<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 1%">
                                    <div class="checkbox-inline">
                                        <input type="checkbox" value="select_all"
                                               id="CheckboxSelectAll" onclick="selected_checkbox(this);"/>
                                        <label for="CheckboxSelectAll" style="font-weight: normal">Select All</label>
                                    </div>

                                </div>-->
                                <?php
                                foreach($users as $user) {
                                    ?>
                                    <div class="col-md-6 col-xs-6" style="padding: 1%;background-color: #286090;color: #ffffff;border: 1px solid #fff">
                                        <div class="checkbox-inline">
                                            <input type="checkbox" value="<?php echo $user['email']; ?>"
                                                   id="Checkbox_<?php echo $user['user_id']; ?>" class="styled" name="checked_item"/>
                                            <label for="Checkbox_<?php echo $user['user_id']; ?>" style="font-weight: normal"><?php echo $user['email']; ?></label>
                                        </div>
                                    </div>
                                    <?php
                                }
                               ?>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 no-gutter">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter msgs-header">
                                <label style="margin-top: 7px;">Preview : HTML</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter html-data">
                                <?php echo $msg_desc.$msg_signature;?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
 function selected_checkbox(ref) {
     var isChecked = $("#"+ref.id).is(":checked");
     $(".styled").prop("checked",isChecked);
 }

 function send_maill() {
     var selected_items = [];
     var subject = $("#msg_txt").val();
     var msg = $("#msg_desc").val();

     $("input:checkbox[name=checked_item]:checked").each(function(){
         selected_items.push($(this).val());
     });

     if(selected_items.length === 0) {
         $(".modal-title").html("<label style='color: #202020;' >Error</label>");
         $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>Please select all fields</label>");
         $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>');
         $("#myModal").modal("show");
         return false;
     }
     var url = "api/messageProcess.php";
     $.post(url,{type:'sendMessage',ids:selected_items.join(','),subject:subject,msg:msg},function(data){
         var status = data.Status;
         var message = data.Message;
         if(status === 'Success') {
             $(".modal-title").html("<label style='color: #202020;' >Success</label>");
             $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>Message sent successfully</label>");
             $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>');
             $("#myModal").modal("show");

         }
         else{
             $(".modal-title").html("<label style='color: #202020;' >Error</label>");
             $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>"+message+"</label>");
             $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>');
             $("#myModal").modal("show");

         }
     });
     console.log('ids --- '+selected_items);
 }
</script>
<?php
 include("footer.php");
?>
