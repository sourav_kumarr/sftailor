<?php
include('database/db.php');
$status = "init";
$message = "";
$dated = date("d/M/Y h:i:s A");
$user_id = $_REQUEST['user_id'];
$coupon_price = 0;
$coupon_code = "";
if($_REQUEST) {
    if (isset($_REQUEST['type'])) {
        $type = $_REQUEST['type'];
        if($type == "isCouponApplied"){
            $query = "select * from used_coupons where used_status = 'Pending' and user_id='$user_id'";
            $result = mysql_query($query);
            if ($result)
            {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $roww = mysql_fetch_assoc($result);
                    $coupon_code = $roww['coupon_code'];

                    /* get Category Coupon list according to  coupon applied start here */
                    $cat_query = "select * from categories  where cat_type = 'Coupons' and cat_name='$coupon_code'";
                    $cat_result = mysql_query($cat_query);
                    if($cat_result){
                        $catNum = mysql_num_rows($cat_result);
                        if($catNum > 0 )
                        {
                            $catRow = mysql_fetch_assoc($cat_result);
                            /*get coupon data  start here*/
                            $cat_name = $catRow['cat_name'];
                            $discount_price = $catRow['cat_price'];
                            $coupon_infinity = $catRow['coupon_infinity'];
                            $coupon_for = $catRow['coupon_for'];
                            $coupon_condition = $catRow['coupon_condition'];
                            $freebies_item_with = $catRow['freebies_item_with'];
                            $freebies_item = $catRow['freebies_item'];
                            /*get coupon data  end   here*/
                            /* for in condition  discount start here*/
                            if($coupon_condition == "discount"){
                                $total_prod_price = 0;
                                $coupon_applyd_on = "";
                                if(strpos($coupon_for, ',') !== false){
                                    $coupon_for_Arr = explode(",",$coupon_for);
                                    for($k = 0; $k< count($coupon_for_Arr); $k++){
                                        $coupon_applyd_on = $coupon_for_Arr[$k].",".$coupon_applyd_on;
                                        $prod_price = getProductPrice($user_id,$coupon_for_Arr[$k]);
                                        $total_prod_price = $total_prod_price + $prod_price;
                                    }
                                }
                                else
                                {
                                    if($coupon_for == "All"){
                                        $coupon_for_Arr = array("Custom Suit","Custom Shirt","Custom Pant",
                                            "Custom 2Pc Suit","Custom Vest","Accessories","Custom Overcoat","Collection");
                                        $total_prod_price = 0;
                                        $coupon_applyd_on = "All";
                                        for($k = 0; $k< count($coupon_for_Arr); $k++){
                                            $prod_price = getProductPrice($user_id,$coupon_for_Arr[$k]);
                                            $total_prod_price = $total_prod_price + $prod_price;
                                        }
                                    }
                                    else{
                                        $coupon_applyd_on = $coupon_for;
                                        $total_prod_price = getProductPrice($user_id,$coupon_for);
                                    }
                                }
                                $status = "done";
                                $discount_amount = ($total_prod_price * $discount_price)/100;
                                $message = "Get discount amount by this coupon";
                                $coupon_applyd_on = $coupon_applyd_on;
                                $response['coupon_applyd_on']=$coupon_applyd_on;
                                $response['discount_amount']=$discount_amount;
                            }
                            else if ($coupon_condition == "freebies"){

                                $freebies_query = "select * from wp_cart where product_type = '$freebies_item_with' and user_id='$user_id'";
                                $freebies_result = mysql_query($freebies_query);
                                if($freebies_result){
                                    $freebies_num = mysql_num_rows($freebies_result);
                                    if($freebies_num > 0)
                                    {
                                        while($freebies_data = mysql_fetch_assoc($freebies_result)){
                                            $cart_id = $freebies_data['cart_id'];
                                            $coupon_applied = $freebies_data['coupon_applied'];
                                           /*for add freebies item in cart start here*/
                                           if($coupon_applied == "No/Selected" || $coupon_applied == ""){
                                              $update_cart = "update wp_cart set coupon_code ='$coupon_code', coupon_applied = 'Yes/NoSelected',freebies_item='$freebies_item'
                                                   where cart_id='$cart_id'";
                                                   $update_cart_result = mysql_query($update_cart);
                                                   if($update_cart_result){
                                                       $status = "done";
                                                       $message = "Coupon applied successfully";
                                                   }
                                                   else
                                                   {
                                                       $status = "error";
                                                       $message = mysql_error($update_cart_result);
                                                   }

                                           }
                                           /*for add freebies item in cart end   here*/
                                        }
                                    }
                                    else{
                                        $status = "error";
                                        $message = "This Coupon is not valid.";
                                    }

                                }
                                else
                                {
                                    $status = "error";
                                    $message = mysql_error($freebies_result);
                                }
                            }
                            $response['coupon_condition'] = $coupon_condition;
                            /* for in condition  discount end   here*/

                        }
                        else
                        {
                            $status = "error";
                            $message = "Invalid Coupon Code";
                        }
                    }
                    else
                    {
                        $status = "error";
                        $message = mysql_error($cat_result);
                    }
                }
                else
                {
                    $status = "error";
                    $message = "empty coupon list";
                }
            }
            else
            {
                $status = "error";
                $message = mysql_error();
            }
        }
        else if($type == "discard") {
            $user_id = $_REQUEST['user_id'];
            $query = "select * from used_coupons where used_status = 'Pending' and user_id='$user_id'";
            $result = mysql_query($query);
            if ($result) {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $discount = mysql_fetch_array($result)['discountAmount'];
                    $query = "delete from used_coupons where used_status = 'Pending' and user_id='$user_id'";
                    $resu = mysql_query($query);
                    if ($resu) {
                        $status = "done";
                        $message = $discount;
                    } else {
                        $status = mysql_error();
                    }
                } else {
                    $status = "error";
                }
            } else {
                $status = mysql_error();
            }
        }
        else if($type == "getfullcartcount") {
            $user_id = $_REQUEST['user_id'];
            $query = mysql_query("SELECT (select SUM(cart_product_total_amount) from wp_cart WHERE user_id='$user_id') as total,(select SUM(cart_extra_price) FROM wp_cart where user_id='$user_id' ) as extra from wp_cart where user_id='$user_id'");
            if ($query) {
                $rows = mysql_fetch_array($query);
                $total = intval($rows['total'])+intval($rows['extra']);

                $status = "done";
                $message = $total;
            } else {
                $status = mysql_error();
            }
        }
    }
    else {
        $coupon_code = $_REQUEST['couponCode'];
        $query = "select * from categories where cat_type='Coupons' and cat_name='$coupon_code'";
        $result = mysql_query($query);
        if ($result) {
            $num = mysql_num_rows($result);
            if ($num > 0) {
                $row = mysql_fetch_assoc($result);
                $coupon_price = $row['cat_price'];
                $coupon_usage = $row['coupon_infinity'];
                $cat_coupon_for = $row['coupon_for'];
                $coupon_condition = $row['coupon_condition'];
                $freebies_item_with = $row['freebies_item_with'];
                $freebies_item = $row['freebies_item'];

                $query = "select * from used_coupons where used_status = 'Pending' and user_id='$user_id'";
                $result = mysql_query($query);
                if ($result) {
                    $num = mysql_num_rows($result);
                    if ($num > 0) {
                        $status = "error";
                        $message = "Coupon code is allready applied on this";
                    }
                    else
                    {
                        $query = "select * from used_coupons where coupon_code = '$coupon_code' and user_id='$user_id'";
                        $result = mysql_query($query);
                        $num = mysql_num_rows($result);
                        if ($num > 0) {
                            while($crow = mysql_fetch_array($result)){
                                if($crow['used_status'] == "Pending"){
                                    $status = "error";
                                    $message = "This Coupon Code is Already Used for this Order.";
                                }
                                else if($crow['used_status'] == "Redeemed"){
                                    if($coupon_usage == "once"){
                                        $status = "error";
                                        $message = "You Have Already Used this Coupon Code.";
                                    }
                                    else{
                                        /*coupon apply start here*/
                                        /* for discount condition coupon apply start here*/
                                        if($coupon_condition == "discount"){
                                            $total_prod_price = 0;
                                            $coupon_applyd_on =$cat_coupon_for;
                                            if(strpos($cat_coupon_for, ',') !== false){
                                                $coupon_for_Arr = explode(",",$cat_coupon_for);
                                                for($k = 0; $k< count($coupon_for_Arr); $k++){
                                                    $prod_price = getProductPrice($user_id,$coupon_for_Arr[$k]);
                                                    $total_prod_price = $total_prod_price + $prod_price;
                                                }
                                            }
                                            else
                                            {
                                                if($cat_coupon_for == "All"){
                                                    $coupon_for_Arr = array("Custom Suit","Custom Shirt","Custom Pant",
                                                        "Custom 2Pc Suit","Custom Vest","Accessories","Custom Overcoat","Collection");
                                                    $coupon_applyd_on = "All";
                                                    for($k = 0; $k< count($coupon_for_Arr); $k++){
                                                        $prod_price = getProductPrice($user_id,$coupon_for_Arr[$k]);
                                                        $total_prod_price = $total_prod_price + $prod_price;
                                                    }
                                                }
                                                else
                                                {
                                                    $coupon_applyd_on = $cat_coupon_for;
                                                    $total_prod_price = getProductPrice($user_id,$cat_coupon_for);
                                                }
                                            }
                                            if($total_prod_price < 1){
                                                $status = "error";
                                                $message = "Coupon code not valid ";
                                            }
                                            else
                                            {
                                                $que = mysql_query("SELECT SUM(cart_product_total_amount) AS Total FROM wp_cart
                                                WHERE user_id='$user_id'");
                                                if ($que) {
                                                    $total = mysql_fetch_assoc($que)['Total'];
                                                    $qq = mysql_query("insert into used_coupons(coupon_code,coupon_condition,user_id,
                                                    total_cart_value,used_status,discountAmount,coupon_amount_percent) values('$coupon_code','$coupon_condition','$user_id',
                                                    '$total','Pending','$total_prod_price','$coupon_price')");
                                                    if ($qq)
                                                    {
                                                        $message = "You get discount coupon";
                                                        $status = "done";
                                                        $response['coupon_applyd_on']=$freebies_item;
                                                        $response['coupon_condition']=$coupon_condition;
                                                        $response['discount_amount']= $total_prod_price;
                                                    }
                                                    else
                                                    {
                                                        $status = "error";
                                                        $message = mysql_error($qq);
                                                    }
                                                }
                                                else
                                                {
                                                    $status = "Error" . Mysql_error();
                                                }
                                            }
                                        }
                                        /* for discount condition coupon apply end here*/



                                        /*for freebiese coupon apply start here*/
                                        else if ($coupon_condition == "freebies"){
                                            $cartItemExist = getProductPrice($user_id,$freebies_item_with);
                                            if($cartItemExist){
                                                $que = mysql_query("SELECT SUM(cart_product_total_amount) AS Total FROM wp_cart
                                                WHERE user_id='$user_id'");
                                                if ($que) {
                                                    $total = mysql_fetch_assoc($que)['Total'];
                                                    $qq = mysql_query("insert into used_coupons(coupon_code,coupon_condition,user_id,
                                                    total_cart_value,used_status,freebies_item_with,freebies_item) values('$coupon_code','$coupon_condition','$user_id',
                                                    '$total','Pending','$freebies_item_with','$freebies_item')");
                                                    if ($qq)
                                                    {
                                                        $message = "You get freebies item";
                                                        $status = "done";
                                                        $response['coupon_applyd_on']=$freebies_item_with;
                                                        $response['freebies_item']=$freebies_item;
                                                        $response['coupon_condition']=$coupon_condition;
                                                        $response['discount_amount']='free item';
                                                    }
                                                    else
                                                    {
                                                        $status = "error";
                                                        $message = mysql_error($qq);
                                                    }
                                                }
                                                else
                                                {
                                                    $status = "Error" . Mysql_error();
                                                }

                                            }
                                            else
                                            {
                                                $status = "error";
                                                $message = "Coupon code not valid ";
                                            }

                                        }
                                        /*for freebiese coupon apply end   here*/
                                        /*coupon apply end   here*/

                                    }
                                }
                            }
                        }
                        else{
                            /*coupon apply start here*/
                            /* for discount condition coupon apply start here*/
                            if($coupon_condition == "discount"){
                                $total_prod_price = 0;
                                $coupon_applyd_on = $cat_coupon_for;
                                if(strpos($cat_coupon_for, ',') !== false){
                                    $coupon_for_Arr = explode(",",$cat_coupon_for);
                                    for($k = 0; $k< count($coupon_for_Arr); $k++){
                                        $prod_price = getProductPrice($user_id,$coupon_for_Arr[$k]);
                                        $total_prod_price = $total_prod_price + $prod_price;
                                    }
                                }
                                else
                                {
                                    if($cat_coupon_for == "All"){
                                        $coupon_for_Arr = array("Custom Suit","Custom Shirt","Custom Pant",
                                            "Custom 2Pc Suit","Custom Vest","Accessories","Custom Overcoat","Collection");
                                        $coupon_applyd_on = "All";
                                        for($k = 0; $k< count($coupon_for_Arr); $k++){
                                            $prod_price = getProductPrice($user_id,$coupon_for_Arr[$k]);
                                            $total_prod_price = $total_prod_price + $prod_price;
                                        }
                                    }
                                    else
                                    {
                                        $coupon_applyd_on = $cat_coupon_for;
                                        $total_prod_price = getProductPrice($user_id,$cat_coupon_for);
                                    }
                                }
                                if($total_prod_price < 1){
                                    $status = "error";
                                    $message = "Coupon code not valid ";
                                }
                                else
                                {
                                    $que = mysql_query("SELECT SUM(cart_product_total_amount) AS Total FROM wp_cart
                                                WHERE user_id='$user_id'");
                                    if ($que) {
                                        $total = mysql_fetch_assoc($que)['Total'];
                                        $qq = mysql_query("insert into used_coupons(coupon_code,coupon_condition,user_id,
                                                    total_cart_value,used_status,discountAmount,coupon_amount_percent) values('$coupon_code','$coupon_condition','$user_id',
                                                    '$total','Pending','$total_prod_price','$coupon_price')");
                                        if ($qq)
                                        {
                                            $message = "You get discount coupon";
                                            $status = "done";
                                            $response['coupon_applyd_on']=$coupon_applyd_on;
                                            $response['coupon_condition']=$coupon_condition;
                                            $response['discount_amount']= $total_prod_price;
                                        }
                                        else
                                        {
                                            $status = "error";
                                            $message = mysql_error($qq);
                                        }
                                    }
                                    else
                                    {
                                        $status = "Error" . Mysql_error();
                                    }
                                }
                            }
                            /* for discount condition coupon apply end here*/
                            /*for freebiese coupon apply start here*/
                            else if ($coupon_condition == "freebies"){
                                $cartItemExist = getProductPrice($user_id,$freebies_item_with);
                                if($cartItemExist){
                                    $que = mysql_query("SELECT SUM(cart_product_total_amount) AS Total FROM wp_cart
                                                WHERE user_id='$user_id'");
                                    if ($que) {
                                        $total = mysql_fetch_assoc($que)['Total'];
                                        $qq = mysql_query("insert into used_coupons(coupon_code,coupon_condition,user_id,
                                                    total_cart_value,used_status,freebies_item_with,freebies_item) values('$coupon_code','$coupon_condition','$user_id',
                                                    '$total','Pending','$freebies_item_with','$freebies_item')");
                                        if ($qq)
                                        {
                                            $message = "You get freebies item";
                                            $status = "done";
                                            $response['coupon_applyd_on']=$freebies_item_with;
                                            $response['coupon_condition']=$coupon_condition;
                                            $response['freebies_item']=$freebies_item;
                                            $response['discount_amount']='free item';
                                        }
                                        else
                                        {
                                            $status = "error";
                                            $message = mysql_error($qq);
                                        }
                                    }
                                    else
                                    {
                                        $status = "Error" . Mysql_error();
                                    }

                                }
                                else
                                {
                                    $status = "error";
                                    $message = "Coupon code not valid ";
                                }

                            }
                            /*for freebiese coupon apply end   here*/
                        }
                    }
                }
                else {
                    $status = "error";
                    $message =  mysql_error();
                }
            } else {
                $status = "error";
                $message = "Invalid Coupon Code Entered";
            }
        } else {
            $status = "error";
            $message = mysql_error();
        }
    }
}
else{
    $status = "Missing Params";
}

function getProductPrice($user_id,$cat_coupon_for){
    $cart_query = "select * from wp_cart where user_id = '$user_id' and product_type = '$cat_coupon_for'";
    $cart_result = mysql_query($cart_query);
    if ($cart_result)
    {
        $num1 = mysql_num_rows($cart_result);
        if($num1 > 0){
            $total_prod_price = 0;
            while($cartRow = mysql_fetch_assoc($cart_result)){
                $prod_price = $cartRow['cart_product_total_amount'];
                $total_prod_price = $total_prod_price + $prod_price;
            }
            return $total_prod_price;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return -1;
    }
}

$response['status']=$status;
$response['message']=$message;
$response['coupon_code']=$coupon_code;
echo json_encode($response);
?>