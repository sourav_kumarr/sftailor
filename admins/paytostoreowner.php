<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if (isset($_POST['filterButton'])) {
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query = "select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
} else {
    $query = "select * from orders order by order_id DESC";
}
?>
<style>
    .ddkfxx ul li .p1 {
        font-weight: bold;
        width: 34% !important;
    }

    .ddkfxx td {
        border: 1px solid #999999;
        padding: 10px 100px 7px 11px;
    }
    .newddkfxx {
        padding: 28px 2px!important;
        width: 1149px !important;
    }
    .newddxq_tc {
        width: 1149px!important;
    }
    .newddkfxx th {
        font-size: 12px;
        font-weight: 500;
        text-align: center;
    }
    .saver{color: orangered;font-size: 12px;cursor: pointer;display: none;}
    .cancler{color: red;font-size: 12px;cursor: pointer;display: none;}
    .editors{color: brown;font-size: 12px;cursor: pointer;float:right;display: none;}
    .inputer{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer0{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer1{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer2{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer3{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer4{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer5{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer6{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer7{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer8{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer9{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer10{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer11{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer12{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer13{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
    .inputer14{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 85%;display: none}
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Stylist
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px"
                                        onclick="window.location='api/excelProcess.php?dataType=allOrders'"
                                        class="btn btn-info btn-sm">Download Excel File
                                </button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control"
                                               name="startDate" id="startFilter"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate"
                                               id="endFilter"/>
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton"
                                           style="margin-top: 5px"/>
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                   <!--- <button style="margin-top:5px;float: right;display: block;" onclick="getCommissionStatus()" class="btn btn-info btn-sm">Commission Level</button>---->
                    <div class="x_content">
                        <div style="float: right; font-weight: bold; position: absolute; top: 23px; left: 18%; z-index: 99;">
						Sort By Category:&nbsp;&nbsp;
                            <select style="height: 25px; width: 144px;" onchange="sortbycat(this)">
                                <option value="">Select Stylist</option>
                                <option value="Stylist">Stylist</option>
                                <option value="Senior Stylist">Senior Stylist</option>
                                <option value="Style Associate">Style Associate</option>
                                <option value="Style Leader">Style Leader</option>
                                <option value="Style Manager">Style Manager</option>
                                <option value="Style Director">Style Director</option>
                                <option value="Style Executive">Style Executive</option>
                                <option value="Style Ambassador">Style Ambassador</option>
                                <option value="Style Vice President">Style Vice President</option>
                                <option value="Style President">Style President</option>
                            </select>
						</div>
						
						<div style="float: right; font-weight: bold; position: absolute; top: 23px; left: 50%; z-index: 99;">
						Sort By Month:&nbsp;&nbsp;
                            <select style="height: 25px; width: 144px;" onchange="sortbymonth(this)">
                                <option value="">Select Month</option>
                                <option value="Jan">January</option>
                                <option value="Feb">February</option>
                                <option value="Mar">March</option>
                                <option value="Apr">April</option>
                                <option value="May">May</option>
                                <option value="Jun">June</option>
                                <option value="Jul">July</option>
                                <option value="Aug">August</option>
                                <option value="Sep">September</option>
                                <option value="Oct">October</option>
                                <option value="Nov">November</option>
                                <option value="Dec">December</option>
                            </select>
						</div>
                        <p class="text-muted font-13 m-b-30">
                            List of Orders Generated
                        </p>
                        <script>
                            function sortbycat(sel){
                                window.location="paytostoreowner.php?type=cat&filter="+sel.value;
                            }
							function sortbymonth(sel){
                                window.location="paytostoreowner.php?type=month&filter="+sel.value;
                            }

                        </script>

                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>

                            <tr>
                                <th>#</th>
                                <th>Stylist Name</th>
                                <th>Stylist Email</th>
                                <th>Phone</th>
                                <th>Refferal Code</th>
                                <th>No.of Refferals</th>
                                <th>Designation</th>
                                <!--<th>Stylist Website</th>-->
                                <th>Payment Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php

                            $link = $conn->connect();//for scan2tailor
                            $store_name = "";
		
                            if ($link) {
								
								if($_REQUEST['type'] =="month"){
									 $dated = $_REQUEST['filter'];
									
									$query1 = "select order_date from wp_orders where order_payment_status = 'Failed' AND paid_amount >= 500";
									 $result = mysqli_query($link, $query1);
									if ($result) {
										
										$num = mysqli_num_rows($result);
										if ($num > 0) {
											
											 while ($rows = mysqli_fetch_assoc($result)) {
												$order_date = $rows['order_date'];
												$exd = explode(" ",$order_date);
												$orderDate = $exd[0];
												if($dated == $orderDate){
													$query = "select distinct(store_name) from wp_orders where order_payment_status = 'Failed' AND paid_amount >= 500";
												}
											 }
										}
									}

								}
                                else {
									$query = "select distinct(store_name) from wp_orders where order_payment_status = 'Failed' AND paid_amount >= 500"; 
								}
								
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
										
                                        if($_REQUEST['type'] =="cat"){
											
                                            $store_name = $_REQUEST['filter'];
                                            $query = mysqli_query($link, "select * from wp_store where designation = '$store_name'");

                                        }
										
										
                                        while ($rows = mysqli_fetch_array($result)) {

                                            if($_REQUEST['filter'] =="")
                                            {
												
                                                $store_name = $rows['store_name'];
                                                $query = mysqli_query($link, "select * from wp_store where store_name = '$store_name'");
                                            }
											if($_REQUEST['type'] =="month")
                                            {
												
                                                $store_name = $rows['store_name'];
                                                $query = mysqli_query($link, "select * from wp_store where store_name = '$store_name'");
                                            }

                                            if ($query) {
												
                                                $num = mysqli_num_rows($query);
												
                                                if ($num > 0) {
													
                                                    while ($rows = mysqli_fetch_array($query)) {
                                                        $desig = $rows['designation'];
                                                        $designation = str_replace(' ', '', $desig);
                                                        ?>
                                                        <tr>
                                                            <td data-title='#'><?php echo $j + 1; ?></td>
                                                            <td data-title='User gender'><?php echo $rows['store_name']; ?></td>
                                                            <td data-title='User gender'><?php echo $rows['user_email']; ?></td>
                                                            <td data-title='User gender'><?php echo $rows['store_phone']; ?></td>
                                                            <td data-title='User gender'><?php echo $rows['store_refferal_code']; ?></td>
                                                            <td data-title='User gender'><?php echo $rows['referal_count']; ?></td>
                                                            <td data-title='User gender'><?php echo $rows['designation']; ?></td>
                                                            <td data-title='User gender'
                                                                style="text-align: center;"><span onclick=get_personal_commission('<?php echo $rows['store_name']; ?>','<?php echo $designation; ?>','<?php echo $rows['store_refferal_code']; ?>') style="color:forestgreen;background: green;
                                                            padding: 5px;color: #ffffff;cursor: pointer; border-radius: 2px;">Personal Commission</span>
                                                                    <span onclick=get_referel_payment_details('<?php echo $rows['store_name']; ?>','<?php echo $designation; ?>','<?php echo $rows['store_refferal_code']; ?>') style="color:forestgreen;background: green; padding: 5px;color: #ffffff;cursor: pointer; border-radius: 2px;">Pay Details</span>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }

                                            } else {
                                                //error
                                            }
                                        }
                                    }
                                }
                            }
                            /*if ($store_name != "") {


                            }*/
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showmodel_data"></div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });
    function storeStatus(status, store_id) {
        var url = "update_store.php?store_status=" + status + "&store_id=" + store_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                if (status == "done") {
                    alert("Company status update successfully...");
                    window.location = "";
                }
                else {
                    //alert('error');
                }
            }
        });
    }

    function get_referel_payment_details(store_id, designation,reffercode) {
        var url = "api/paymentProcess.php?type=level&store_id=" + store_id + "&designation=" + designation +"&refer_code="+ reffercode;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var paymentData = json.storedata;
                var status = json.status;
                var orderModalData = '';
                if (status == "done") {
                    orderModalData = orderModalData + '<link rel="stylesheet" type="text/css" href="css/defaulten.css">' +
                        '<div class="layui-layer-shade" id="layui-layer-shade5" times="5" style="z-index: 19891018; background-color: rgb(0, 0, 0); opacity: 0.7;" onclick="close_diolog()">' +
                        '</div><div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5" type="page" times="5" showtime="0" contype="string" style="z-index: 19891019;' +
                        ' top: 65px; height: 433px; width: 1017px; left: 180px;">' +
                        '<div class="layui-layer-title" style="cursor: move;color: green;font-weight: 600;" move="ok">' + paymentData[0].store_name + '&nbsp( ' + paymentData[0].designation + ' )</div>' +
                        '<div class="layui-layer-content order-detail-modal" style="height: 556px;">' +
                        '<div class="ddxq_tc"><div class="ddkfxx"> <table class="table table-striped table-bordered"><thead>' +
                        '<tr><th>#</th><th>Order Amount</th><th>Commission %</th><th>Earning Amount</th><th>Payment Status</th></tr></thead>';
                        var j = 0;
                        var total = 0;
                        for (var a = 0; a < paymentData.length; a++) {
                            if(paymentData[a].level_commission ==""){
                                var level = '';
                            }
                            else{

                                var level = '<span>+'+paymentData[a].level_commission+'%</span>';
                            }

                            j++;
                            total = total + paymentData[a].total_commission;
                            orderModalData = orderModalData + '<tbody>' + '<tr><td>' + j + '</td><td>$' + paymentData[a].total_order + '</td><td>' + paymentData[a].single_commission +' '+level+'</td><td>' + paymentData[a].total_commission + '</td><td>' + paymentData[a].comm_status + '</td>'
                                + '</tr></tbody>';
                        }
                        orderModalData = orderModalData + '</table><table style="float:right;"><tr><td>Total Earnings </td></tr><tr><td style="font-weight: 600;color:#999;">$' + total + '</td></tr></table></div></div> <table style="float: right; text-align: center; height: 38px; margin-right: 7%; width: 21%; color: white;"><tr style="background: steelblue none repeat scroll 0% 0%; text-align: center; color: white;"><td>Pay Now</td></tr></table></div></div>';
                        $("#showmodel_data").html(orderModalData);
                }
                else {
                    alert(status);
                }
            }
        });
    }
    function close_diolog() {
        window.location = "paytostoreowner.php";
    }


    function getCommissionStatus(){
        var url = "api/levelProcess.php?type=viewcommission";
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var carrerData = json.carrerdata;
                var status = json.status;
                var commissionModalData = '';
                if (status == "done") {
                    commissionModalData = commissionModalData + '<link rel="stylesheet" type="text/css" href="css/defaulten.css">' +
                        '<div class="layui-layer-shade" id="layui-layer-shade5" times="5" style="z-index: 19891018; background-color: rgb(0, 0, 0); opacity: 0.7;" onclick="close_diolog()">' +
                        '</div><div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5" type="page" times="5" showtime="0" contype="string" style="z-index: 19891019;' +
                        ' z-index: 19891019; width: 1190px; left: 79px; top: 43px; height: 530px; overflow-x: scroll;">' +
                        '<div class="layui-layer-title" style="cursor: move;color: green;font-weight: 600;" move="ok">' +
                        'sftailor Career Plans</div>' +
                        '<div class="layui-layer-content order-detail-modal" style="height: 556px;">' +
                        '<div class="ddxq_tc newddxq_tc"><div class="ddkfxx newddkfxx"> <table class="table table-striped table-bordered"><thead>' +
                        '<tr><th>Title</th><th>Stylist</th><th>Senior Stylist</th><th>Style Associate</th><th>Style Leader</th>' +
                        '<th>Style Manager</th><th>Style Director</th><th>Style Executive</th>' +
                        '<th>Style Ambassador</th><th>Style Vice President</th><th>Style President</th>';
                        for (var a = 0; a < carrerData.length; a++) {
							
                            commissionModalData = commissionModalData + '<tr id="showedit"><td style="width: 145px;">'+carrerData[a].title+'<div style="cursor: pointer; color: white; border-radius: 2px; background: steelblue none repeat scroll 0% 0%; height: 18px; padding: 1px 6px; float: right;" onclick="editData('+carrerData[a].level_id+')"><i class="fa fa-pencil"></i></div><div style="cursor: pointer; color: white; border-radius: 2px; background: seagreen none repeat scroll 0% 0%; height: 18px; padding: 1px 6px; float: right;margin-right: 2px;" onclick="updateData('+carrerData[a].level_id+')"><i class="fa fa-floppy-o"></i></div></td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer1'+a+'" value="'+carrerData[a].stylist+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].stylist+'</span>' + '</td>' +
							'<td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer2'+a+'" value="'+carrerData[a].senior_stylist+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].senior_stylist+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer3'+a+'" value="'+carrerData[a].style_associate+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_associate+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer4'+a+'" value="'+carrerData[a].style_leader+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_leader+'</span>&nbsp;&nbsp;' + '</td>' +
                                '<td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer5'+a+'" value="'+carrerData[a].style_manager+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_manager+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer6'+a+'" value="'+carrerData[a].style_director+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_director+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer7'+a+'" value="'+carrerData[a].style_executive+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_executive+'</span>&nbsp;&nbsp;' + '</td>' +
                                '<td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer8'+a+'" value="'+carrerData[a].style_ambassador+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_ambassador+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer9'+a+'" value="'+carrerData[a].style_vice_president+'"> <span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_vice_president+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer10'+a+'" value="'+carrerData[a].style_president+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_president+'</span>&nbsp;&nbsp;' + '</td></tr>';
                                
						}
                        commissionModalData = commissionModalData + '</tr></thead></table></div></div>';
                    $("#showmodel_data").html(commissionModalData);
                }
            }
        });


    }

function updateData(levelId){
var inputs = $(".inputer"+levelId);
var levels = [];
for(var i = 0; i < inputs.length; i++){
		var level1 = $(inputs[i]).val();
		levels.push(level1);
	}
	var levelss = levels.join(',');
	var url = "api/levelProcess.php?type=updateCommission&level_id="+levelId+"&inputval="+levelss;
	$.get(url, function (data) {
		var json = $.parseJSON(data)
		{
			var carrerData = json.carrerdata;
			var status = json.status;
			var commissionModalData = '';
			if (status == "done") {
				alert("carrer plan updated successfully!");
				window.location="paytostoreowner.php";
			}
		}
	}); 
}
function editData(levelid){
	$(".inputer"+levelid).show();
	$(".stylist"+levelid).hide();
}

function get_personal_commission(store_id, designation,reffercode) {
    window.location="pcommissiondata.php?s="+store_id;
}
</script>
