<?php
include('header.php');
include("admin/webserver/database/db.php");
?>
    <style>
        .primarysuggestion {
            height: 200px;
            margin-left: 10px;
            margin-top: 10px;
            width: 170px;
        }
        .suggestionname
        {
            line-height: 20px;
            margin-top: 5px;
        }
        .steps {
            line-height: 20px !important;
            margin-left: 27px;
            padding-top: 15px;
        }

        .stepsactive {
            background: #c4996c none repeat scroll 0 0;
            color: white;
        }
        .breakingline {
            width: 525px;
        }
    </style>
    <div class="loader" style="display: none">
        <img src="images/spin.gif" class="spinloader"/>
    </div>
    <div class="col-md-12">
        <div class="col-md-6" style="text-align: center;margin: 75px 0" id="imageloader">
            <div class="col-md-1">
                <label onclick="rotateImage('left')" id="rotateLeft" style="margin-top: 350px"><i class="fa fa-share fa-2x"></i></label>
            </div>
            <div class="col-md-10" style="padding-top: 50px;position: relative;z-index: -100">
                <img src="" class="suggestion" id="shirtImage"/>
            </div>
            <div class="col-md-1">
                <label onclick="rotateImage('right')" id="rotateRight" style="margin-top: 350px"><i class="fa fa-reply fa-2x"></i></label>
            </div>
        </div>
        <div class="col-md-6" id="objectloader" style="display:none">
            hello
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="padding:0">
                <div class="steps stepsactive" id="collar">Shirt Collar</div>
                <div class="steps" id="cuff">Shirt <br>Cuff</div>
                <div class="steps" id="cuff">Shirt Placket</div>
                <div class="steps" id="cuff">Shirt Pocket</div>
                <div class="steps" id="cuff">Shirt <br>Back</div>
                <div class="steps" id="shirtcolor">Shirt <br>Color</div>
                <div class="breakingline"></div>
            </div>
            <!--
            ------
            -----collar div
            ------
            -------->
            <div class="col-md-12" id="collars" style="display: block;padding-left:20px">
                <p class="stylecaption">Shirt Collar Style</p>
                <div class="primarysuggestion active" id="collar_smallroundedpointscollar">
                    <img src="images/shirts/smallroundedpointscollar.png"/>
                    <p class="suggestionname">Small Rounded Points Collar</p>
                </div>
                <div class="primarysuggestion" id="collar_smallpointedcollard">
                    <img src="images/shirts/smallpointedcollard.png"/>
                    <p class="suggestionname">Small Pointed Collar D</p>
                </div>
                <div class="primarysuggestion" id="collar_middlepointedcollard">
                    <img src="images/shirts/middlepointedcollard.png"/>
                    <p class="suggestionname">Middle Pointed Collar D</p>
                </div>
                <div class="primarysuggestion" id="collar_largepointedcollard">
                    <img src="images/shirts/largepointedcollard.png"/>
                    <p class="suggestionname">Large Pointed Collar D</p>
                </div>
                <div class="primarysuggestion" id="collar_smallpointedcollare">
                    <img src="images/shirts/smallpointedcollare.png"/>
                    <p class="suggestionname">Small Pointed Collar E</p>
                </div>
            </div>
            <!--
           ------
           -----cuff div
           ------
           -------->
            <div class="col-md-12" id="cuffs" style="display:none;padding-left:80px">
                <p class="stylecaption">Collar Style</p>
                <div class="primarysuggestion active" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
                <div class="primarysuggestion" title="Large Rounded Cuff One Button and One Button Hole" id="cuff_largeroundedcuffonebuttonandonebuttonhole">
                    <img src="images/shirts/largeroundedcuffonebuttonandonebuttonhole.png"/>
                    <p class="suggestionname">Large Rounded Cuff...</p>
                </div>
            </div>
            <!--
          ------
          -----Color div
          ------
          -------->
            <div class="col-md-12" id="colors" style="display:none" >
                <p class="stylecaption">Collar Style</p>
                <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0">
                    <?php
                    $query = "select * from wp_colors where type = 'shirt'and status = '1'";
                    $result = mysql_query($query);
                    if($result)
                    {
                        $num = mysql_num_rows($result);
                        if($num>0)
                        {
                            $status = "done";
                            $a=0;
                            while($rows = mysql_fetch_array($result))
                            {
                                $a++;
                                $colorName = $rows['colorName'];
                                $displayImage = $rows['displayImage'];
                                $objImage = $rows['objImage'];
                                ?>
                                <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' id='color_<?php echo $colorName ?>'>
                                    <img src='images/shirts/<?php echo $displayImage ?>' />
                                </div>
                                <?php
                            }
                        }
                        else
                        {
                            echo "No Color Found";
                        }
                    }
                    else
                    {
                        echo mysql_error();
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="text-align:center">
        <label id="previous" onclick="navigation('previous')" class="prevnextbtn" >Previous</label>
        <label id="next" onclick="navigation('next')" class="prevnextbtn" >Next</label>
    </div>
<?php
include("footer.php");
?>
    <script>
        var currPage="";
        var collar="smallroundedpointscollar";
        var cuff="largeroundedcuffonebuttonandonebuttonhole";
        var shirtcolor="sda351a";
        function loadSessionData() {
            $(".loader").fadeIn("slow");
            var prevIndex = $(".stepsactive").attr("id");
            currPage = prevIndex + "s";
            var url = "admin/webserver/selectionData.php?type=getsession&" + prevIndex + "=" + prevIndex;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    var img = "";
                    var item = json.items;
                    var name = json.items;
                    item = prevIndex + "_" + item;
                    $(".primarysuggestion").removeClass("active");
                    $("#" + item).addClass("active");
                    if (prevIndex == "collar") {
                        collar = name;
                    }
                    else if (prevIndex == "cuff") {
                        cuff = name;
                    }
                    else if (prevIndex == "shirtcolor") {
                        shirtcolor = name;
                    }
                    if (currPage == "buttons") {
                        img = button;
                    }
                    else if (currPage == "vents") {
                        img = button + "_" + vent;
                    }
                    else if (currPage == "collars") {
                        img = button + "_" + vent + "_" + collar;
                    }
                    if(currPage == "colors") {
                        $("#imageloader").hide();
                        console.log(button+"_"+vent+"_"+collar);
                        $("#objectloader").html("<iframe class='iframe' src='objectLoader.php?filename="+button+"_"+vent+"_"+collar+
                            "&color="+color+"'></iframe>");
                        $("#objectloader").show();
                        setTimeout(function()
                        {
                            $(".loader").fadeOut("slow");
                        },6000);
                    }
                    else{
                        $(".suggestion").attr("src", "images/sugg/white/" + img + ".jpg");
                        setTimeout(function()
                        {
                            $(".loader").fadeOut("slow");
                        },2000);
                    }
                }
                else {
                    var url = "";
                    if (currPage == "buttons") {
                        img = button;
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + button;
                    }
                    else if (currPage == "vents") {
                        img = button + "_" + vent;
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + vent;
                    }
                    else if (currPage == "collars") {
                        img = button + "_" + vent + "_" + collar;
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + collar;
                    }
                    else if (currPage == "colors") {

                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + color;
                    }
                    $.get(url, function (data) {
                        var json = $.parseJSON(data);
                        var status = json.status;
                        if (status == "done") {
                            if(currPage == "colors") {
                                $("#imageloader").hide();
                                console.log(button+"_"+vent+"_"+collar);
                                $("#objectloader").html("<iframe class='iframe' src='objectLoader.php?filename="+button+"_"+vent+"_"+collar+
                                    "&color="+color+"'></iframe>");
                                $("#objectloader").show();
                                setTimeout(function()
                                {
                                    $(".loader").fadeOut("slow");
                                },6000);
                            }
                            else{
                                $(".suggestion").attr("src", "images/sugg/white/" + img + ".jpg");
                                setTimeout(function()
                                {
                                    $(".loader").fadeOut("slow");
                                },2000);
                            }
                        }
                    }).fail(function () {
                        alert("page is not available");
                    });
                }
            });
        }
        $(".primarysuggestion").click(function()
        {
            var Img = "";
            $(".loader").fadeIn("slow");
            $(".primarysuggestion").removeClass("active");
            var value = this.id;
            var type = value.split("_");
            var selection = type[1];
            $("#"+value).addClass("active");
            var currpage = type[0]+"s";
            var url = "admin/webserver/selectionData.php?"+type[0]+"="+selection;
            $.get(url,function(data)
            {
                var json = $.parseJSON(data);
                var status = json.status;
                if(status == "done")
                {
                    if (type[0] == "button") {
                        button = selection;
                    }
                    else if (type[0] == "vent") {
                        vent = selection;
                    }
                    else if (type[0] == "collar") {
                        collar = selection;
                    }
                    else if (type[0] == "color") {
                        color = selection;
                    }
                    if (currpage == "buttons") {
                        Img = button;
                    }
                    else if (currpage == "vents") {
                        Img = button + "_" + vent;
                    }
                    else if (currpage == "collars") {
                        Img = button + "_" + vent + "_" + collar;
                    }
                    if(currpage == "colors") {
                        $("#imageloader").hide();
                        $("#objectloader").html("<iframe class='iframe' src='objectLoader.php?filename="+button+"_"+vent+"_"+collar+
                            "&color="+color+"'></iframe>");
                        $("#objectloader").show();
                        setTimeout(function()
                        {
                            $(".loader").fadeOut("slow");
                        },6000);

                    }
                    else{
                        $(".suggestion").attr("src", "images/sugg/white/" + Img + ".jpg");
                        setTimeout(function()
                        {
                            $(".loader").fadeOut("slow");
                        },2000);
                    }
                }
                else
                {
                    alert(status);
                }
            }).fail(function()
            {
                alert("page is not available");
            });
        });
        function navigation(switcher) {
            $("#" + currPage).fadeOut("slow");
            if(switcher == "next") {
                if (currPage == "buttons") {
                    $(".steps").removeClass("stepsactive");
                    $("#vents").fadeIn("slow");
                    $("#vent").addClass("stepsactive");
                    loadSessionData();
                }
                else if(currPage == "vents")
                {
                    $(".steps").removeClass("stepsactive");
                    $("#collars").fadeIn("slow");
                    $("#collar").addClass("stepsactive");
                    loadSessionData();
                }
                else if(currPage == "collars")
                {
                    $(".steps").removeClass("stepsactive");
                    $("#colors").fadeIn("slow");
                    $("#color").addClass("stepsactive");
                    $("#next").html("Add to Cart");
                    loadSessionData();
                }
                else if(currPage == "colors")
                {
                    var user_id = $("#user_id").val();
                    //alert(user_id);
                    if(user_id == "")
                    {
                        window.location="login.php?redurl=rgvsjuq45745sttpx";
                    }
                    else {
                        var randomnum = Math.floor((Math.random() * 600000) + 1);
                        var product_name = "CustomSuit"+randomnum;
                        var url = "admin/webserver/addto_cart.php?product_type=Custom Suit&product_name="+product_name+
                            "&product_button="+button+"&product_vent="+vent+"&product_collar="+collar+"&product_color="+color
                            +"&product_price="+179.32+"&user_id="+user_id+"&quantity="+1+"&cart_product_total_amount="+179.32
                            +"&product_image="+button+"_"+vent+"_"+collar;
                        $.get(url, function (data) {
                            var json = $.parseJSON(data)
                            var status = json.status;
                            if (status == "done") {
                                window.location="cart.php";
                            }
                            else {
                                alert(status);
                            }
                        });
                    }
                }
            }
            else if(switcher == "previous")
            {
                if (currPage == "buttons") {
                    window.location="customize.php";
                }
                else if(currPage == "vents")
                {
                    $(".steps").removeClass("stepsactive");
                    $("#buttons").fadeIn("slow");
                    $("#button").addClass("stepsactive");
                    loadSessionData();
                }
                else if(currPage == "collars")
                {
                    $(".steps").removeClass("stepsactive");
                    $("#vents").fadeIn("slow");
                    $("#vent").addClass("stepsactive");
                    loadSessionData();
                }
                else if(currPage == "colors")
                {
                    $(".steps").removeClass("stepsactive");
                    $("#collars").fadeIn("slow");
                    $("#collar").addClass("stepsactive");
                    $("#next").html("Next");
                    $("#imageloader").show();
                    $("#objectloader").hide();

                    loadSessionData();
                }
            }
        }
        loadSessionData();
        function showcolordiv()
        {
            $(".loader").fadeIn("slow");
            $("#buttons").hide();
            $("#vents").hide();
            $("#collars").hide();
            $("#colors").show();
            $(".steps").removeClass("stepsactive");
            $("#color").addClass("stepsactive");
            currPage = "colors";
            $("#next").html("Add to Cart");
            $("#imageloader").hide();
            var url="admin/webserver/selectionData.php?type=getParicularSession";
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                button = json.button;
                vent = json.vent;
                collar = json.collar;
                color = json.color;
                console.log(button+"_"+vent+"_"+collar);
                $("#objectloader").html("<iframe class='iframe' src='objectLoader.php?filename="+button+"_"+vent+"_"+collar+
                    "&color="+color+"'></iframe>");
                $("#objectloader").show();
                setTimeout(function()
                {
                    $(".loader").fadeOut("slow");
                },6000);
            });
        }
    </script>
<?php
if(isset($_REQUEST['fromtwo']))
{
    ?>
    <script>
        showcolordiv();
    </script>
    <?php
}
?>