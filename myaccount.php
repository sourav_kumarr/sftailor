<?php
include('admin/webserver/database/db.php');
include('header.php');
?>
<div class="about-bottom wthree-3">
    <link rel="stylesheet" href="css/myaccount.css">
    <link rel="stylesheet" href="css/bootstrap-datepicker.css">
    <script src="js/location.js"></script>
    <script src="js/moment.js"></script>
    <style>
        #cityfeild #cityId {
            margin-bottom: 11px;
            width: 254px;
        }
        img {
            width: 160px;
        }
        .cf {
            margin-bottom: 11px;
            width: 254px;
        }
        .sb-icon-search, .sb-search-submit {
            right: -58px;
        }
    </style>
    <div class="loader" style="display: none">
        <img src="http://scan2fit.com/scan2tailor/images/preloader.gif" class="spinloader"/>
    </div>
    <div class="col-md-12"
         style="background: rgb(34, 34, 34) none repeat scroll 0% 0%; margin-top: 80px; margin-bottom: 85px; opacity: 0.94; padding-bottom: 50px;">
        <?php
        $userId = $_SESSION['sftuser_id'];
        $query = mysql_query("SELECT * FROM `wp_customers` WHERE `user_id` = $userId");
        if ($query) {
            $num = mysql_num_rows($query);
            if ($num > 0) {
                $status = "done";
                $rows = mysql_fetch_assoc($query);
                $user_id = $rows['user_id'];
                $user_name = $rows['name'];
                $address = $rows['address'];
                $b_country = $rows['b_country'];
                $b_state = $rows['b_state'];
                $b_city = $rows['b_city'];
                $dob = $rows['dob'];
                if ($b_country == "United States") {
                    $retrieved = $dob;
                    $date = DateTime::createFromFormat('d/m/Y', $retrieved);
                    $dob = $date->format('m/d/Y');
                } else {
                    $dob = $rows['dob'];
                }
                $driver_licence = $rows['driver_licence'];
                $face_login = $rows['face_login'];
                $height = $rows['height'];
                $profile_pic = $rows['profile_pic'];
                $email = $rows['email'];
                $password = $rows['password'];
                $email_receive = $rows['email_receive'];
                $stylist = $rows['stylist'];
                $gender = $rows['gender'];
                $lang = $rows['lang'];
                $weight = $rows['weight'];
                $app_type = $rows['app_type'];
                $mobile = $rows['mobile'];
                $pincode = $rows['pincode'];
            } else {
                $status = "error" . mysql_error();
            }
        }
        if (isset($_POST['update'])) {
            $user_name = $_POST['name'];
            $dob = $_POST['dob'];
            $b_country = $_POST['b_country'];
            $b_state = $_POST['b_state'];
            $b_city = $_POST['b_city'];
            $address = $_POST['address'];
            $mobile = $_POST['mobile'];
            $gender = $_POST['gender'];
            $stylist = $_POST['stylist'];
            $pincode = $_POST['pincode'];
            if ($_FILES['file0']['name']) {
                $target_path = "admin/webserver/upload/";
                $file_ext = strtolower(end(explode('.', $_FILES['file0']['name'])));
                $tmp_name = $_FILES['file0']['tmp_name'];
                $file_name = "user" . rand(000000, 999999) . "." . $file_ext;
                $target_path = $target_path . $file_name;
                if (move_uploaded_file($tmp_name, $target_path)) {
                    $target_path = "http://scan2fit.com/sftailor/" . $target_path;
                }
            } else {
                $target_path = $profile_pic;
            }
            $update = "UPDATE `wp_customers` SET `name`='$user_name',`dob`='$dob',`b_country`='$b_country',
		    `b_state`='$b_state',`b_city`='$b_city',`address`='$address',`mobile`='$mobile',
		    `gender`='$gender',`stylist`='$stylist',`profile_pic`='$target_path',pincode='$pincode' where user_id=" . $user_id;
            $query = mysql_query($update);
            if ($query) {
                echo "<script>document.location.href='myaccount.php'</script>";
            } else {
                echo "<script>alert('server error please wait..')</script>";
            }
        }
        ?>
        <div id="container">
            <div id="content" data-easytabs="true">
                <div class="menu">
                    <ul class="tabs">
                        <li class="profile active"><a href="javascript:void(0)" onclick="pro_tab()"
                        class="profile-tab">Profile</a></li>
                        <li class="edit"><a href="javascript:void(0)" onclick="edit_tab()" class="connect-tab">Edit
                        Profile</a></li>
                    </ul>
                </div>
                <form action="" method="post" enctype="multipart/form-data" id="update_info">
                    <div id="profile_add" class="active">
                        <div class="about">
                            <div class="profile-photo"><img src="<?php echo $profile_pic; ?>" width="160" height="160"></div>
                            <h1><?php echo $user_name; ?></h1>
                            <h3><i><?php echo $email; ?></i></h3>
                            <p>
                                <span style="color:#FDEE78;">Full Address</span></br><?php echo $address . "," . $b_city . "," . $b_state . ",</br>" . $b_country; ?>
                            </p>
                            <p>
                                <span style="color:#FDEE78;">ZipCode</span></br><?php echo $pincode ?>
                            </p>
                        </div>
                        <ul class="basic-info">
                            <li><h3>BASIC INFO </h3></li>
                            <li><label>Name</label><span><?php echo $user_name; ?></span></li>
                            <li><label>Birthdate</label><span><?php echo $dob; ?></span></li>
                            <li><label>Stylist</label><span><?php echo $stylist; ?></span></li>
                            <li><label>Phone</label><span><?php echo $mobile; ?></span></li>
                            <li><label>Email</label><span><?php echo $email; ?></span></li>
                            <li><label>Gender</label><span><?php echo $gender; ?></span></li>
                        </ul>
                    </div>
                    <div id="connect_add" style="display: none;" class="">
                        <div class="about">
                            <div class="profile-photo"><img src="<?php echo $profile_pic; ?>" width="160" height="160">
                                <div class="edit-pic">Edit Photo</div>
                                <input name="file0" id="file0" class="custom_signup_feild form-control edit-photo" type="file">
                            </div>
                            <h1><?php echo $user_name; ?></h1>
                            <h3><i><?php echo $email; ?></i></h3>
                            <span style="color: rgb(253, 238, 120); font-weight: bold;">Full Address</span></br>
                            <div class="full-addresses">
							
							
                                <input type="text" name="address" value="<?php echo $address; ?>">
								
								
                                <select name="b_country" class="countries form-control" id="countryId" style="margin-bottom: 11px; width: 254px;"></select>
								
                                <select name="b_state" class="states form-control" id="stateId" style="margin-bottom: 11px; width: 254px;">
                                    <option value="<?php echo $b_state; ?>"><?php echo $b_state; ?></option>
                                </select>
								
								
								
                                <div id="cityfeild">
                                    <select name="b_city" class="cities form-control" id="cityId" style="margin-bottom: 11px; width: 254px !important;">
                                        <option value="<?php echo $b_city; ?>"><?php echo $b_city; ?></option>
                                    </select>
                                </div>
								
								
								
								
								
                            </div>
                        </div>
                        <div class="ee">
                            <ul class="basic-info">
                                <li><h3>BASIC INFO </h3></li>
                                <li><label>Name</label><span style="color:black;"><input type="text" name="name"
                                value="<?php echo $user_name; ?>"></span></li>
                                <li><label>Birthdate</label><span style="color:black;"><input type="text" name="dob" id="dob"
                                class="form-control frm_ctrl" value="<?php echo $dob; ?>"></span></li>
                                <li><label>Stylist</label><span style="color:black;"><input type="text" name="stylist"
                                value="<?php echo $stylist; ?>"></span></li>
                                <li><label>Phone</label><span style="color:black;"><input type="text" name="mobile"
                                value="<?php echo $mobile; ?>"></span></li>
                                <li><label>Gender</label><span style="color:black;"><input type="text" name="gender"
                                value="<?php echo $gender; ?>"></span></li>
                                <li><label>Zipcode</label><span style="color:black;"><input type="text" name="pincode"
                                value="<?php echo $pincode; ?>"></span></li>
                                <input type="hidden" value="<?php echo $user_id ?>" name="user_id">
                                <input type="submit" id="update" name="update" value="Update Info"
                                class="custombttn2 index-lern edit-infos" style="margin-left:0;"/>
                            </ul>
                        </div>
                    </div>
                </form>
            </div>
            <script>
                function edit_tab() {
                    $("#profile_add").hide();
                    $("#connect_add").show();
                    $(".edit").addClass("active");
                    $(".profile").removeClass("active");
                }
                function pro_tab() {
                    $("#connect_add").hide();
                    $("#profile_add").show();
                    $(".profile").addClass("active");
                    $(".edit").removeClass("active");
                }
                $('#update_info').submit(function (e) {
                    $(".loader").fadeIn("slow");
                });
                $(function () {
                    $('#dob').datepicker({
                        format: 'dd/mm/yyyy'
                    });
                    $("#dob").on("dp.change", function (e) {
                        $('#dob').data("DateTimePicker").hide();
                    });
                });
				userPersonal();
			  function userPersonal() {
				var user_id = $("#user_id").val();
				var url = "admin/webserver/register.php?type=get_billing&user_id=" + user_id;
				$.get(url, function (data) {
					var json = $.parseJSON(data);
					var status = json.status;
					var items = json.items;
					if (status == "done") {
						var item = items[0];
						$("#countryId").append("<option selected='selected'>" + item.country + "</option>");
						$("#stateId").html("<option selected='selected'>" + item.state + "</option>");
						$("#cityId").html("<option selected='selected'>" + item.city + "</option>")
					}
				});
			}
            </script>
        </div>
    </div>
    <div class="clear"></div>
</div>
<?php
    include('footer.php');
?>