<?php
error_reporting(0);
$user_id = "";
if(isset($_REQUEST['userId']) != ""){
    $user_id = $_REQUEST['userId'];
}
echo "<input type='hidden' value='".$user_id."' id='user_id' />";
?>
<style>
    .homeIcon{
        background: #811215 none repeat scroll 0 0;
        border-radius: 50%;
        color: white;
        font-size: 30px;
        font-weight: bold;
        height: 42px;
        line-height: 38px;
        margin: -4px;
        position: absolute;
        text-align: center;
        width: 42px;
    }
    #homeIcon_box{
        position: absolute;
        backface-visibility: hidden;
        float: right;
        min-width: 42px;
        transition: width 0.3s ease 0s;
        z-index: 999;
        right: 150px !important;
        top: 62px !important;
        cursor: pointer;
        margin-top: 15px;
    }
    .sb-search{
        margin-top: 18px!important;
    }
</style>
<script>
    function pageRedirect(redirectTo){
        window.location=redirectTo;
    }
    function getCart(user_id) {
        var url = "../admin/webserver/getcart.php?user_id=" + user_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                var cartshow = "";
                if (status == "done") {
                    var items = json.items;
                    var count = items.length;
                    $(".bagde").html(count);
                }
                else {
                    //alert('error');
                }
            }
        });
    }
</script>
<link rel="stylesheet" href="css/front/mystyle.css" type="text/css" media="all">
<link rel="stylesheet" href="css/front/style.css" type="text/css" media="all">
<div class="search-box" onclick="pageRedirect('order_cart.php?userId=<?php echo $user_id; ?>')">
    <div id="sb-search" class="sb-search">
        <span class="sb-icon-search"><span class="bagde">0</span> </span>
    </div>
</div>
<div class="search-box" onclick="pageRedirect('order_customize.php?userId=<?php echo $user_id; ?>')">
    <div id="homeIcon_box">
        <i class="fa fa-home homeIcon" aria-hidden="true"></i>
    </div>
</div>