<?php
require_once ('Classes/SUPPLIER.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$supClass = new \Classes\SUPPLIER();
$requiredfields = array('type');

($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $supClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addSupplier")
{
    $requiredfields = array('company_name','company_address1','contact_first_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $company_name = trim($_POST['company_name']);
    $company_address1 = trim($_POST['company_address1']);
    $company_address2 = trim($_POST['company_address2']);
    $company_work_phone = trim($_POST['company_work_phone']);
    $company_mobile_no = trim($_POST['company_mobile_no']);
    $company_fax = trim($_POST['company_fax']);
    $contact_first_name = trim($_POST['contact_first_name']);
    $family_name = trim($_POST['family_name']);
    $contact_email = trim($_POST['contact_email']);
    $response = $supClass->addSupplier($company_name,$company_address1,$company_address2,$company_work_phone,$company_mobile_no,$company_fax,$contact_first_name,$family_name,$contact_email);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "updateSupplier"){
    $requiredfields = array('supplier_id','company_name','company_address1','contact_first_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $company_name = trim($_POST['company_name']);
    $company_address1 = trim($_POST['company_address1']);
    $company_address2 = trim($_POST['company_address2']);
    $company_work_phone = trim($_POST['company_work_phone']);
    $company_mobile_no = trim($_POST['company_mobile_no']);
    $company_fax = trim($_POST['company_fax']);
    $contact_first_name = trim($_POST['contact_first_name']);
    $family_name = trim($_POST['family_name']);
    $contact_email = trim($_POST['contact_email']);
    $supplier_id = trim($_POST['supplier_id']);

    $response = $supClass->updateSupplier($company_name,$company_address1,$company_address2,$company_work_phone,$company_mobile_no,$company_fax,$contact_first_name,$family_name,$contact_email,$supplier_id);
    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}

else if($type == "deleteSupplier")
{
    $requiredfields = array('supplier_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $sup_id = $_REQUEST['supplier_id'];
    $response = $supClass->deleteSupplier($sup_id);
    if($response[STATUS] == Error) {
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if ($type == "singleUserEmail") {
    $requiredfields = array('supplier_email');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $sup_Email = $_REQUEST['supplier_email'];
    $orderid = $_REQUEST['orderid'];
    $response = $supClass->sendMaillSupplier($sup_Email,$orderid);
    if($response[STATUS] == Error) {
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if ($type == "supplierEmail") {
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $sup_Email = $_REQUEST['supplier_email'];
    $orderid = $_REQUEST['order_id'];
    $admin_mail = $_REQUEST['admin_mail'];
    $add_cc = $_REQUEST['add_cc'];
    $message_text = $_REQUEST['message_text'];
    if($admin_mail =="blank1@gmail.com"){
        $response = $supClass->sendMaillSupplier($sup_Email,$orderid,"blank@gmail.com",$add_cc,$message_text);
    }
    else {
        $response = $supClass->sendMaillSupplier($sup_Email,$orderid,$admin_mail,$add_cc,$message_text);
    }

    if($response[STATUS] == Error) {
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "updateterms"){

    $requiredfields = array('editor1');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $editor1 = trim($_POST['editor1']);

    $response = $supClass->updateSalesTerms($editor1);
    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "newTask")
{
    $requiredfields = array('activity_type');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $activity_type = $_REQUEST['activity_type'];
    $newtask_assing = $_REQUEST['assignTo'];
    $newtask_subject = $_REQUEST['subject'];
    $newtask_duedate = $_REQUEST['duedate'];
    $newtask_comment = $_REQUEST['comment'];
    $related = $_REQUEST['related'];
    $user_id = $_REQUEST['user_id'];
    $response = $supClass->createTask($newtask_assing,$newtask_subject,$newtask_duedate,$newtask_comment,$related,$activity_type,$user_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "newTaskEmail")
{
    $requiredfields = array('activity_type');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $activity_type = $_REQUEST['activity_type'];
    $newtask_subject = $_REQUEST['subject'];
    $newtask_comment = $_REQUEST['comment'];
    $related = $_REQUEST['related'];
    $user_id = $_REQUEST['user_id'];
    $response = $supClass->createTaskEmail($newtask_subject,$newtask_comment,$related,$activity_type,$user_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}

else if($type == "emailTask")
{
    $requiredfields = array('activity_type');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $activity_type = $_REQUEST['activity_type'];
    $email_to = $_REQUEST['email_to'];
    $email_cc = $_REQUEST['email_cc'];
    $email_bcc = $_REQUEST['email_bcc'];
    $email_subject = $_REQUEST['email_subject'];
    $email_comment = $_REQUEST['email_comment'];
    $email_related = $_REQUEST['email_related'];
    $user_id = $_REQUEST['user_id'];
    $time_zone_offset = $_REQUEST['time_zone_offset'];
    $response = $supClass->createEmailTask($email_to,$email_cc,$email_bcc,$email_subject,$email_comment,$email_related,$activity_type,$user_id,$time_zone_offset);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "deleteActivity")
{
    $requiredfields = array('activity_id');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $activity_id = $_REQUEST['activity_id'];
    $response = $supClass->deleteActivity($activity_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}

else if($type == "newevent")
{
    $requiredfields = array('addevent');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $newevent = $_REQUEST['addevent'];
    $activity_type = $_REQUEST['activity_type'];
    $response = $supClass->addevent($newevent,$activity_type);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "updateNewEvent")
{
    $requiredfields = array('event_id');
    $response = RequiredFields($_REQUEST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $event_id = $_REQUEST['event_id'];
    $updateevent = $_REQUEST['updateevent'];
    $response = $supClass->updateNewEvent($event_id,$updateevent);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}


else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $supClass->apiResponse($response);
}
?>