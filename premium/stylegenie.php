<?php
    include('header.php');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Style Genie <small></small></h2>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of User's Having Style Genie Used
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>User Profile</th>
                                <th>User Name</th>
                                <th>User E-Mail</th>
                                <th>User Contact</th>
                                <th>User Gender</th>
                                <th>User City</th>
                                <th>Stylist</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="userData"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    function loadUsersData(){
        var storeName = $("#store_name").val();
        if(storeName == ""){
            showError("Session Timed Out !!! Please Login Again","red");
            return false;
        }
        var url = "api/userProcess.php";
        $.post(url,{"type":"getAllUsersData","store_name":storeName},function(data){
            var Status = data.Status;
            var dataShow = "";
            var ids = [];
            if(Status == "Success"){
                var userData = data.UserData;
                for(var i=0;i<userData.length;i++){
                    if(ids.indexOf(userData[i].user_id)>-1){
                        //alert("data already exist");
                    }else {
                        ids.push(userData[i].user_id);
                        dataShow = dataShow + "<tr><td><img alt='Image' src='" + userData[i].profile_pic + "' " +
                        "style='height:40px !important;width:40px;border:2px solid #333'/></td><td>" + userData[i].name + "</td>" +
                        "<td>" + userData[i].email + "</td><td>" + userData[i].mobile + "</td><td>" + userData[i].gender + "</td>" +
                        "<td>" + userData[i].b_city + "</td><td>"+storeName+"</td><td><button class='btn btn-sm btn-info' " +
                        "onclick=viewSaved('" + userData[i].user_id + "')>View Saved Measurements</button></td></tr>";
                    }
                }
            }else{
                dataShow = "<tr><td colspan=8 style='text-align:center'>"+data.Message+"</td></tr>";
            }
            $("#userData").html(dataShow);
        }).fail(function(){
            showError("Server Error !!! Please Try After Some Time...","red");
        });
    }
    function viewSaved(user_id){
        var url = "../api/registerUser.php";
        $.post(url,{"type":"getAllMeasurement","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                var measurements = data.measurements;
                var datashow = "<div class='row'><div class='container-fluid'><table class='table table-bordered'><tr>" +
                "<th>S.No</th><th>Measurement Name</th><th>Added On</th><th>Action</th>";
                for(var i=0;i<measurements.length;i++){
                    var date = new Date(measurements[i].added_on*1000);
                    var year    = date.getFullYear();
                    var month   = date.getMonth()+1;
                    var dated     = date.getDate();
                    var added_on = dated+"/"+month+"/"+year;
                    datashow += "<tr><td>"+(i+1)+"</td><td>"+measurements[i].meas_name+"</td><td>"+added_on+"</td>" +
                    "<td><input type='button' value='View Measurement' class='btn btn-danger btn-sm' " +
                    "onclick=finalSelection('"+measurements[i].measurement_id+"','"+user_id+"') /></td></tr>";
                }
                datashow+="</table></div></div>";
                $(".modal-title").html("<span style='color:green'>Select Measurement File</span>");
                $(".modal-body").html(datashow);
                $(".modal-footer").css("display","none");
                $("#myModal").modal("show");
            }
            else{
                showMessage("No measurements saved yet","red");
            }
        }).fail(function(){
            alert("Server Error !!! Please Try After Some Time");
        });
    }
    function finalSelection(measurement_id,user_id){
        window.location="sdet.php?_="+user_id+"&m="+measurement_id;
    }
    loadUsersData();
</script>
