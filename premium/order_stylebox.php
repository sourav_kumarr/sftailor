<?php
include("header.php");
include("front_header.php");
include_once "api/Constants/dbConfig.php";
include_once "api/Classes/CONNECT.php";
$conn = new Classes\CONNECT();
?>
<style>
    .nav_menu{
        margin-bottom: 0px;
    }
    .navicon {
        background: transparent;
        border: 1px solid #666;
        height: 50px !important;
        width: 50px !important;
        line-height: 44px;
        color:#666 !important;
        text-align: center;
        font-size:50px;
    }
    .labelofstylebox {
        background: white;
        margin-top: 30px;
        padding: 15px 70px;
        color: #666;
        font-size: 30px;
        font-weight: normal;
        text-transform: full-width;
        font-family: monospace;
    }
    .descriptiondiv{
        margin-top:20px;
    }
    .divbox{
        height:350px;
        border:2px solid #efefef;
        width:24%;
        margin-right:1%;
        padding:0;
        cursor:pointer;
    }
    .imageBox{
        height:300px;
        overflow: hidden;
        border:2px solid #efefef;
    }
    .divbox p{
        text-align:center;
    }
    .carousel {
        position: relative;
    }
    .carousel-inner {
        position: relative;
        width: 100%;
        overflow: hidden;
    }
    .carousel-control {
        position: absolute;
        top: 47%;
        left: 25px;
        opacity: 0.5;
        background: transparent!important;
    }
    .navicon {
        background: transparent;
        border: 1px solid #666;
        height: 50px !important;
        width: 50px !important;
        line-height: 44px;
        color: #666 !important;
        text-align: center;
        font-size: 50px;
    }


</style>

<div class="right_col" role="main" style="overflow-y: auto">
    <div class="about-bottom wthree-3" style="">
        <div class="container">
            <h2 class="tittle">A LOOK AT OUR STYLE BOXES</h2>
        </div>
        <div class="container">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <?php
                    if($conn->connect()){
                        $query = "select * from stylebox order by u_id desc";
                        $result = mysqli_query($conn->connect(),$query);
                        if($result){
                            $num = mysqli_num_rows($result);
                            if($num>0){
                                $a=0;
                                while($rows = mysqli_fetch_assoc($result)){
                                    $a++;
                                    if($a == 1){
                                        $class='active';
                                    }else{
                                        $class = '';
                                    }
                                    ?>
                                    <div class="item <?php echo $class ?>" id="<?php echo $rows['u_id'] ?>">
                                        <div class='col-md-12 itemdiv'>
                                            <div style="width:40%;height:600px;background:#E1E6EA;float:left;text-align:center">
                                                <label class="labelofstylebox"><?php echo $rows['title'] ?></label>
                                                <div class="descriptiondiv"><?php echo $rows['description']?></div>
                                            </div>
                                            <div style="width:60%;height:600px;float:left;background:url(../admins/api/Files/images/stylebox/<?php echo $rows['image'] ?>) no-repeat;background-size:100% auto;"></div>
                                        </div>
                                        <!--<div class="carousel-caption"></div>-->
                                        <button style="cursor:pointer;margin:10px 0 0 40%;width:20%;border-radius:0"
                                                class="btn btn-info btn-lg"
                                                onclick=getBox('<?php echo $rows['u_id'] ?>','<?php echo $rows['price'] ?>','<?php echo urlencode($rows['image']) ?>')>Get My Box</button>
                                    </div>
                                    <?php
                                }
                            }else{
                                echo "Not Any Style Box Found...";
                            }
                        }else{
                            echo mysqli_error($conn->connect());
                        }
                    }else{
                        echo "No Connection With DataBase";
                    }
                    ?>
                </div>
                <!-- Controls -->
                <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
                    <span class="fa fa-angle-left navicon" aria-hidden="true"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="fa fa-angle-right navicon" aria-hidden="true"></span>
                </a>
            </div>
            <div class="recombox" style="padding:0 15px;margin-top:50px">
                <h2 class="tittle">SHOP THE STORE</h2>
                <?php
                if($conn->connect()){
                    $query = "select * from stylebox order by u_id desc";
                    $result = mysqli_query($conn->connect(),$query);
                    if($result){
                        $num = mysqli_num_rows($result);
                        if($num>0){
                            $a=0;
                            while($rows = mysqli_fetch_assoc($result)){
                                ?>
                                <div class="col-md-3 divbox" onclick=showinslider(<?php echo $rows['u_id'] ?>)>
                                    <div class="imageBox">
                                        <img src="../admins/api/Files/images/stylebox/<?php echo $rows['image']?>" class="img-responsive" />
                                    </div>
                                    <p><?php echo $rows['title']?></p>
                                </div>
                                <?php
                            }

                        }else{
                            echo "Not Any Style Box Found...";
                        }

                    }else{
                        echo mysqli_error($conn->connect());
                    }
                }else{
                    echo "No Connection With DataBase";
                }
                ?>
            </div>
        </div>
    </div>
</div>
<?php include("footer.php");?>
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<script>
    var user_id = $("#user_id").val();
    function showinslider(u_id){
        $(".item").removeClass("active");
        $("#"+u_id).addClass("active");
    }
    function getBox(box_id,price,image){
        var user_id = $("#user_id").val();
        image = decodeURIComponent(image);
        window.location="../styleboxpaynow.php?u_id="+box_id+"&rate="+price+"&user_id="+user_id+"&img="+image;
    }
    getCart(user_id);
</script>