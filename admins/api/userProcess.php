<?php
require_once ('Classes/USERCLASS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $userClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if($type == "register")
{
    $requiredfields = array('username','email','contactNumber','address','socialType');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $username = trim($_POST['username']);
    $email = trim($_POST['email']);
    $contactNumber = trim($_POST['contactNumber']);
    $address = trim($_POST['address']);
    $socialType = trim($_POST['socialType']);
    $image_url = trim($_POST['image_url']);
    $response = $userClass->registerUser($username, $email, $contactNumber, $address,$socialType,$image_url);
    $userClass->apiResponse($response);
}
else if($type == "getParticularUserData")
{
    $requiredfields = array('userId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $response = $userClass->getParticularUserData($userId);
    $userClass->apiResponse($response);
}
else if($type == "getAllUsers") {
    $userResponse = $userClass->getAllUsers();
    if($userResponse[STATUS] == Error) {
        $userClass->apiResponse($userResponse);
        return false;
    }
    $userClass->apiResponse($userResponse);
}
else if($type == "getAllUsersMeasure") {
    $userResponse = $userClass->getAllUsersMeasure();
    if($userResponse[STATUS] == Error) {
        $userClass->apiResponse($userResponse);
        return false;
    }
    $userClass->apiResponse($userResponse);
}
else if($type == "statusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->statusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "paymentIdAdd")
{
    $requiredfields = array('paymentId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $paymentId = $_REQUEST['paymentId'];
    $response = $userClass->paymentIdAdd($paymentId);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "renewalStatusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->renewalStatusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteUser")
{
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->deleteUser($user_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
///////////////////////////////////////////////////*/
else if($type == "updatePlan")
{
    $requiredfields = array('userId','plan_id','renewal_type','auto_renewal');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $plan_id = $_POST['plan_id'];
    $renewal_type = $_POST['renewal_type'];
    $auto_renewal = $_POST['auto_renewal'];
    $response = $userClass->updatePlan($userId, $plan_id, $renewal_type, $auto_renewal);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else if($type == "feedback")
{
    $requiredfields = array('name','from','feedback_text','feedback_description');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $name = trim($_POST['name']);
    $from = trim($_POST['from']);
    $feedback_text = trim($_POST['feedback_text']);
    $feedback_description = trim($_POST['feedback_description']);
    $response = $userClass->addFeedback($name, $from, $feedback_text,$feedback_description);
    $userClass->apiResponse($response);
}
else if($type == "getFeedbackData")
{
    $response = $userClass->getFeedbackData();
    $userClass->apiResponse($response);
}
else if($type == "getFaqData")
{
    ($response = $userClass->getFaqData());
    $userClass->apiResponse($response);
}
else if($type == "getStyleboxData"){
    ($response = $userClass->getStyleboxData());
    $userClass->apiResponse($response);
}
else if($type == "addStylebox"){
    $requiredfields = array('title','description','price');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $title = $_POST['title'];
    $description = $_POST['description'];
    $price = $_POST['price'];
    ($response = $userClass->addStylebox($title,$description,$price));
    $userClass->apiResponse($response);
}
else if($type == "getPartFeedback")
{
    $requiredfields = array('feedId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $feedId = $_POST['feedId'];
    $response = $userClass->getPartFeedback($feedId);
    $userClass->apiResponse($response);
}
else if($type == "deleteFeedback")
{
    $requiredfields = array('feedId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $feedId = $_REQUEST['feedId'];
    $response = $userClass->deleteFeedback($feedId);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteStylebox")
{
    $requiredfields = array('u_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $u_id = $_REQUEST['u_id'];
    $response = $userClass->deleteStylebox($u_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteFaq"){
    $requiredfields = array('u_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $u_id = $_REQUEST['u_id'];
    $response = $userClass->deleteFaq($u_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "addNewFaq"){
    $requiredfields = array('category','question','answer');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $category = $_REQUEST['category'];
    $question = $_REQUEST['question'];
    $answer = $_REQUEST['answer'];
    $response = $userClass->addNewFaq($category,$question,$answer);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "editStyleData")
{
    $requiredfields = array('u_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $u_id = $_REQUEST['u_id'];
    $response = $userClass->editStyleData($u_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "editFaqData")
{
    $requiredfields = array('u_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $u_id = $_REQUEST['u_id'];
    $response = $userClass->editFaqData($u_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "updateFaq")
{
    $requiredfields = array('u_id','category','question','answer');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $u_id = $_REQUEST['u_id'];
    $category = $_REQUEST['category'];
    $question = $_REQUEST['question'];
    $answer = $_REQUEST['answer'];
    $response = $userClass->updateFaq($u_id,$category,$question,$answer);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "updateFeedBack")
{
    $requiredfields = array('name','from','feedback_text','feedback_description','feedId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $feedId = trim($_POST['feedId']);
    $name = trim($_POST['name']);
    $from = trim($_POST['from']);
    $feedback_text = trim($_POST['feedback_text']);
    $feedback_description = trim($_POST['feedback_description']);
    $response = $userClass->updateFeedBack($feedId,$name, $from, $feedback_text,$feedback_description);
    $userClass->apiResponse($response);
}
else if($type == "updateStylebox")
{
    $requiredfields = array('title','description','price','u_id','ImageChanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $title = trim($_POST['title']);
    $description = trim($_POST['description']);
    $price = trim($_POST['price']);
    $u_id = trim($_POST['u_id']);
    $ImageChanged = trim($_POST['ImageChanged']);
    $response = $userClass->updateStylebox($title,$description, $price, $u_id,$ImageChanged);
    $userClass->apiResponse($response);
}
else if($type == "updateFeedStatus")
{
    $requiredfields = array('feedId','feedValue');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $feedId = $_REQUEST['feedId'];
    $feedValue = $_REQUEST['feedValue'];
    $response = $userClass->feddStatusChange($feedId,$feedValue);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "updatePayStatus")
{
    $requiredfields = array('payId','payValue');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $payId = $_REQUEST['payId'];
    $payValue = $_REQUEST['payValue'];
    $response = $userClass->payStatusChange($payId,$payValue);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "updatePaymentId")
{
    $requiredfields = array('paymentId','payId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $paymentId = trim($_POST['paymentId']);
    $payId = trim($_POST['payId']);
    $response = $userClass->updatePaymentId($paymentId,$payId);
    $userClass->apiResponse($response);
}
else if($type == "delPaymentId")
{
    $requiredfields = array('payId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $payId = $_REQUEST['payId'];
    $response = $userClass->deletePaymentId($payId);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "getActivePaymentId")
{
    $response = $userClass->getActivePaymentId();
    $userClass->apiResponse($response);
}
else if($type == "getPartPay")
{
    $requiredfields = array('payId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $payId = $_POST['payId'];
    $response = $userClass->getPartPaymentData($payId);
    $userClass->apiResponse($response);
}
else if($type == "getPaymentData")
{
    $response = $userClass->getPaymentData();
    $userClass->apiResponse($response);
}
else if($type == "getSalesData")
{
    $response = $userClass->getSalesData();
    $userClass->apiResponse($response);
}
else if($type == "getSalesPay")
{
    $requiredfields = array('payId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $payId = $_POST['payId'];
    $response = $userClass->getPartSalesData($payId);
    $userClass->apiResponse($response);
}
else if($type == "updateSalesId")
{
    $requiredfields = array('paymentId','payId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $paymentId = trim($_POST['paymentId']);
    $payId = trim($_POST['payId']);
    $response = $userClass->updateSalesId($paymentId,$payId);
    $userClass->apiResponse($response);
}
else if($type === 'editCategoryPrice') {
    $requiredfields = array('priceId','oldcategoryImagechanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $priceId = $_REQUEST['priceId'];
    $oldcategoryImagechanged = $_REQUEST['oldcategoryImagechanged'];

    $oldcategoryImage = $_REQUEST['oldcategoryImage'];
    $logoResponse = $userClass->editPriceCat($priceId,$oldcategoryImagechanged);
    $userClass->apiResponse($logoResponse);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $userClass->apiResponse($response);
}
?>