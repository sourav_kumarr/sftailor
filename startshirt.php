<?php
include "header.php";
include "api/Constants/dbConfig.php";
include "api/Classes/CONNECT.php";
?>
<style>
    .headingstartshirt{
        font-size: 19px;
        font-weight: bold;
        margin-bottom: 20px;
        margin-left: 15px;
        text-align: left;
        text-decoration: underline;
    }
    .col-md-6 > li {
        font-size: 18px;
        list-style: square;
    }
</style>
<?php
$conn = new \Modals\CONNECT();
$link = $conn->Connect();
?>
<div class="col-md-12" style="margin-top:150px;text-align:center;">
    <p style="font-size:30px">Scan2Tailor's Custom-Made Shirts </p>
    <hr>
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p class="headingstartshirt">CUSTOM SHIRTING</p>
        <div class="col-md-6">
            <label style="text-align:left">Our custom shirt offering combines the best craftsmanship with world class fabrics and
            scientific precision.<br><br>Each shirt pattern is individually drafted for each client to shape the
            torso seamlessly and provide ease of movement.<br><br>Each shirt comes with a complementary custom monogram.<br><br>
            Turnaround time is 2-3 weeks.<br><br>
            </label>
        </div>
        <div class="col-md-6">
            <img src="images/two.jpg" class="img-responsive" />
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
    <div class="col-md-12" style="margin-bottom:50px">
        <hr>
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <p class="headingstartshirt">RETAIL PRICE</p>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories where cat_type='Shirts' group by cat_color order by cat_id asc");
                if($query){
                    $count=1;
                    while($rows = mysqli_fetch_array($query)){
                        if ($rows['cat_type'] == "Shirts" && $rows['cat_color']!='') {
                            if($count <= 5) {
                                echo "<label>" . $rows['cat_description'] . "</label><br>";
                                $catgName = "";
                                switch (ucfirst($rows['cat_color'])){
                                    case "Bronze":
                                        $catgName = " Shirt, Bronze label";
                                        break;
                                    case "Silver":
                                        $catgName = " Shirt, Silver label";
                                        break;
                                    case "Gold":
                                        $catgName = " Shirt, Gold label";
                                        break;
                                    case "Black":
                                        $catgName = " Shirt, Black label";
                                        break;
                                }
                                echo $catgName ." $".$rows['cat_price']. "<br><br>";
                                $count++;
                            }
                        }
                    }
                }
                ?>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories where cat_type='Shirts' group by cat_color order by cat_id asc" );
                if($query){
                    $count=1;
                    while($rows = mysqli_fetch_array($query)){
                        if ($rows['cat_type'] == "Shirts") {
                            $count++;
                            if($count >6) {
                                echo "<label>" . $rows['cat_description'] . "</label><br>";
                                echo '1 shirt with fabric of fabric code '.ucfirst($rows['cat_color'])  . " $" . $rows['cat_price'] . "<br><br>";
                            }
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
<div class="col-md-12" style="margin-bottom:50px">
    <hr>
    <div class="col-md-1"></div>
    <div class="col-md-10">
        <p class="headingstartshirt">Specifications</p>
        <div class="col-md-8">
            <div class="col-md-6">
                <li>Over one thousand fabric options</li>
                <li>Nine monogram position options</li>
                <li>Thomas Mason fabrics</li>
                <li>Sixty Collar options</li>
                <li>Canclini fabrics</li>
            </div>
            <div class="col-md-6">
                <li>Genuine mother of pearl buttons</li>
                <li>Soft or stiff cuff & collar interlining</li>
                <li>First button position choice</li>
                <li>Seventeen button options</li>
                <li>Forty cuff options</li>
            </div>
        </div>
        <div class="col-md-4">
            <input type="button" value="Order Custom Shirt >>" onclick="window.location='custom_shirts.php'" class="btn btn-danger btn-lg" style="margin:50px" />
        </div>
    </div>
    <div class="col-md-1"></div>
</div>
<?php
    include "footer.php";
?>