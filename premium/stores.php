<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if(isset($_POST['filterButton'])){
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query="select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
}else{
    $query="select * from orders order by order_id DESC";
}
?>

<style>.shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .hideloader {
        display: none;
    }
    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
#create_stores{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>
<!-- page content -->
<div id="loadd" class="loaderr hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Orders <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <button style="margin-top:5px;float: right;" onclick="creatStore()" class="btn btn-info btn-sm">Create Stylist</button>


              <?php if($_REQUEST["type"] =="edit"){

                  ?>

                <div class="shadoows" onclick="close_diolog()" style="display: block;"></div>
                    <div class="col-md-12 create_stores" id="create_stores" style="padding: 0;display: block;" >
                        <?php


                        $link = $conn->connect();//for scan2tailor
                        //$link2 = $conn->connect2();//for tailormade
                        if ($link) {
                            $storeId = $_REQUEST['storeid'];
                            $query = "select * from wp_store where store_id='$storeId'";
                            $result = mysqli_query($link, $query);
                            if ($result) {

                                $num = mysqli_num_rows($result);
                                if ($num > 0) {
                                    $j = 0;
                                    $orderDatas = mysqli_fetch_assoc($result);
                                }
                            }
                        }
                        ?>
                        <form method="Post" enctype="multipart/form-data" id="update_store">
						<div class="form-group col-md-4">
                            <label>First Name</label>
                            <input type="text" disabled name="user_name" class="form-control" value="<?php echo $orderDatas['user_name'];?>" placeholder="Enter Your Name" required="true"  />
                        </div>
						<div class="form-group col-md-4">
                            <label>Last Name</label>
                            <input type="text" disabled name="last_name" class="form-control" value="<?php echo $orderDatas['last_name'];?>" placeholder="Enter Your Name" required="true"  />
                        </div>
                            <input type="hidden" name="store_id" value="<?php echo $storeId;?>">
                            <div class="form-group col-md-4">
                                <label>Stylist E-Mail</label>
                                <input type="text" name="user_email" class="form-control" value="<?php echo $orderDatas['user_email'];?>" placeholder="Enter Your E-Mail Address" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Stylist Phone Number</label>
                                <input type="text" name="store_phone" class="form-control" value="<?php echo $orderDatas['store_phone'];?>" placeholder="Enter Your Phone"required="true" />
                                <input type="hidden" name="store_status" value="0">
                            </div>
                            <div class="form-group col-md-4">
                                <label>Password</label>
                                <input type="password" name="store_password" class="form-control" value="<?php echo $orderDatas['store_password'];?>" placeholder="Enter Your Password" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Company Name</label>
                                <input type="text" disabled name="company_name" class="form-control" value="<?php echo $orderDatas['store_name'];?>"   placeholder="Enter Your company Name" />
                            </div>
                            <div class="form-group col-md-4" id="cityfeild">
                                <label>Enter D.O.B</label>
                                <input id="inputdob" name="dob" placeholder="Enter D.O.B" class=" custom_signup_feild form-control frm_ctrl" type="text" value="<?php echo $orderDatas['dob'];?>">
                            </div>
                            <div class="form-group col-md-4">
                                <label>About Company</label>
                                <textarea type="text" name="store_about" class="form-control"  placeholder="Company description"/><?php echo $orderDatas['store_about'];?></textarea>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Stylist Address</label>
                                <textarea type="text" name="store_contact_address" class="form-control" value=""  placeholder="Contact description"/><?php echo $orderDatas['store_contact_address'];?></textarea>
                            </div>
                            <div class="form-group col-md-4" style="margin-bottom: 40px; margin-top: 12px;">
                                <label>Zip Code</label>
                                <input type="text" name="zip_code" class="form-control" value="<?php echo $orderDatas['zip_code'];?>"  placeholder="Enter Your Zip Code" required="true"/>
                            </div>
							<div class="form-group col-md-4" style="margin-bottom: 40px; margin-top: 12px;">
								<label>Mother</label>
									<select class="form-control" name="mother">
									<option selected="" value="">Select Mother</option>
									 <?php
										$link = $conn->connect();
										if ($link) {
											$query = "select * from wp_store order by store_id DESC";
											$result = mysqli_query($link, $query);
											if ($result) {
												$num = mysqli_num_rows($result);
												if ($num > 0) {
													while ($orderData = mysqli_fetch_array($result)) {
														?>
									<option value="<?php echo $orderData['user_name']; ?>" ><?php echo $orderData['user_name']; ?></option>
									 <?php
												}
											}
										}
									}
									?>
									</select>
								</div>
                            <div style='clear:both' ></div>

                            <div class="form-group">
                                <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Update Stylist</button>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
                    <div class="shadoows" onclick="close_diolog()"></div>
                        <div class="col-md-12 create_stores" id="create_stores" style="padding: 0;display: none;" >
                            <form method="Post" enctype="multipart/form-data" id="create_store">
                                <div class="form-group col-md-4">
                                    <label>First Name</label>
                                    <input type="text" name="user_name" class="form-control" value="" placeholder="Enter Your Name" required="true"  />
                                </div>
								<div class="form-group col-md-4">
                                    <label>Last Name</label>
                                    <input type="text" name="last_name" class="form-control" value="" placeholder="Enter Your Last Name" required="true"  />
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Stylist E-Mail</label>
                                    <input type="text" name="user_email" class="form-control" value="" placeholder="Enter Your E-Mail Address" required="true"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Stylist Phone Number</label>
                                    <input type="text" name="store_phone" class="form-control" value="" placeholder="Enter Your Phone"required="true" />
                                    <input type="hidden" name="store_status" value="0">
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Password</label>
                                    <input type="password" name="store_password" class="form-control" value="" placeholder="Enter Your Password" required="true"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Company Name</label>
                                    <input type="text" name="company_name" class="form-control" value=""  placeholder="Enter Your company Name" />
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Profile Picture</label>
                                    <input type="file" name="store_logo" class="form-control" value="Upload" required="true"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>About Company</label>
                                    <textarea type="text" name="store_about" class="form-control" value=""  placeholder="Company description"/></textarea>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Stylist Address</label>
                                    <textarea type="text" name="store_contact_address" class="form-control" value=""  placeholder="Contact description"/></textarea>
                                </div>
                                <div class="form-group col-md-4" style="margin-bottom: 40px; margin-top: 12px;">
                                    <label>Zip Code</label>
                                    <input type="text" name="zip_code" class="form-control" value=""  placeholder="Enter Your Zip Code" required="true"/>
                                </div>
								
								 <div class="form-group col-md-4" style="margin-bottom: 40px; margin-top: 12px;">
								<label>Mother</label>
									<select class="form-control" name="mother">
									<option selected="" value="">Select Mother</option>
									 <?php
										$link = $conn->connect();
										if ($link) {
											$query = "select * from wp_store order by store_id DESC";
											$result = mysqli_query($link, $query);
											if ($result) {
												$num = mysqli_num_rows($result);
												if ($num > 0) {
													while ($orderData = mysqli_fetch_array($result)) {
														?>
									<option value="<?php echo $orderData['user_name']; ?>" ><?php echo $orderData['user_name']; ?></option>
									 <?php
												}
											}
										}
									}
									?>
									</select>
								</div>
								
                                <div style='clear:both' ></div>
                                <div class="form-group col-md-4">
                                    <label>Select Country</label>
                                    <select name="b_country" class="custom_signup_feild countries form-control" id="countryId" onchange='datedate()'>
                                        <option value="">Select Country</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Select State</label>
                                    <select name="b_state" class=" custom_signup_feild states form-control" id="stateId">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Select City</label>
                                    <div id="cityfeild">
                                        <select name="b_city" class=" custom_signup_feild cities form-control" id="cityId">
                                            <option value="">Select City</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-md-4" id="cityfeild">
                                    <label>Enter D.O.B</label>
                                    <input id="inputdob" name="dob" placeholder="Enter D.O.B" class=" custom_signup_feild form-control frm_ctrl" type="text">
                                </div>
                                <div class="form-group col-md-4" id="cityfeild">
                                    <label>Reference( optional )</label>
                                    <input id="referal" name="referal" placeholder="Enter Reference Store Code"
                                           class="custom_signup_feild form-control frm_ctrl" type="text" />
                                </div>
                                <div class="form-group">
                                    <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Create Stylist</button>
                                </div>
                            </form>
                        </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Orders Generated
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Stylist Name</th>
                                <th>Stylist Email</th>
                                <th>Phone</th>
                                <th>Refferal Code</th>
                                <th>No.of Refferals</th>
                                <th>Mother</th>
                                <th>Designation</th>
                                <!--<th>Stylist Website</th>-->
                                <th>Stylist Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $link = $conn->connect();//for scan2tailor
                            //$link2 = $conn->connect2();//for tailormade
							
                            if ($link) {
                                $query = "select * from wp_store order by store_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {

                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($orderData = mysqli_fetch_array($result)) {

                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td><?php echo $orderData['user_name']; ?> <?php echo $orderData['last_name']; ?></td>
                                                <td><?php echo $orderData['user_email']; ?></td>
                                                <td><?php echo $orderData['store_phone']; ?></td>
                                                <td><?php echo $orderData['store_refferal_code']; ?></td>
                                                <td><?php echo $orderData['referal_count']; ?></td>
                                                <td><?php echo $orderData['mother']; ?></td>
                                                <td><?php echo $orderData['designation']; ?></td>
                                                <!--<td>http://scan2tailor.com/<?php /*echo $orderData['store_name']; */?>.com</td>-->
												<?php if($orderData['store_status'] =="0"){
												 $statusStore = "Not Active";
												 $realStatus = "1";
												 $color ="red";
												}
												else
												{
													$statusStore = "Active";
													$realStatus = "0";
													$color ="green";
												}
												?>
                                                <td style="width: 158px;"><div onclick=storeStatus('<?php echo $realStatus;?>','<?php echo $orderData['store_id'];?>') style="background: <?php echo $color;?>;
                                                        padding: 7px 2px;font-size:12px; float:left;text-align: center;width: 70px;color: white; font-weight: bold;cursor:pointer;"><?php echo $statusStore; ?></div><a href='stores.php?type=edit&storeid=<?php echo $orderData['store_id'];?>' style="float: right; font-size: 18px; padding: 2px 4px; background: tomato none repeat scroll 0% 0%; color: white;"><i class="fa fa-pencil"></i> </a>
														&nbsp;&nbsp;&nbsp;
														<a href='#' onclick=delStore('<?php echo $orderData['store_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a> </td>
                                                </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
$(document).ready(function () {
	$('#orderTable').DataTable({});
});
function storeStatus(status,store_id){
	var url = "update_store.php?store_status=" + status +"&store_id="+store_id +"&type=status";
	$.get(url, function (data) {
		var json = $.parseJSON(data)
		{
			var status = json.status;
			if (status == "done") {
					alert("Company status update successfully...");
					window.location="";
			}
			else {
				//alert('error');
			}
		}
	});
}
function delStore(delid){
	   var url = "update_store.php?type=delitem&delid="+delid;
		$.get(url, function (data) {
		var json = $.parseJSON(data)
		{
			var status = json.status;
			if (status == "done") {
					window.location="stores.php";
			}
			else {
				//alert('error');
			}
		}
	});
}
function creatStore(){
$("#create_stores").show();
$(".shadoows").show();
	$("#create_store").submit(function(e)
	{
		e.preventDefault();
		$('.loader').fadeIn("slow");
		$.ajax({
			url: "../admin/webserver/create_store.php?type=storeinsert",
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				$('.loader').fadeOut("slow");
				var json = $.parseJSON(data)
				{
					var status = json.status;
					var message = json.message;
					if(status == "done"){
						alert(message);
						window.location="stores.php";
					}
					else{
						alert(message);
					}
				}
			}
		});
	});
}

$("#update_store").submit(function(e)
{
	e.preventDefault();
	$('.loader').fadeIn("slow");
	$.ajax({
		url: "update_store.php?type=update",
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data)
		{
			$('.loader').fadeOut("slow");
			var json = $.parseJSON(data)
			{
				var status = json.status;
				var message = json.message;
				if(status == "done"){
					alert(message);
					window.location="stores.php";
				}
				else{
					alert(message);
				}
			}
		}
	});
});

$(".menuitems").removeClass("activemenuitem");
$("#contact").addClass("activemenuitem");
function datedate(){
	var country = $("#countryId").val();
	if(country == "United States"){
		$('#inputdob').datetimepicker({
			minView : 2,
			format:"mm/dd/yyyy",
			autoclose: true
		});
	}else{
		$('#inputdob').datetimepicker({
			minView : 2,
			format:"dd/mm/yyyy",
			autoclose: true
		});
	}
}

function close_diolog() {
	window.location = "stores.php"
}
function editstore(store_id){
	//window.location = "stores.php?store_id="+store_id;
	$("#create_stores").show();
	$(".shadoows").show();
}
</script>
