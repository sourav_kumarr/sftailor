<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\STORECONNECT();
$userClass = new \Classes\USERCLASS();
$query="SELECT * FROM wp_customers,wp_orders WHERE (wp_customers.stylist = '$store_name' or wp_orders.store_name = '$store_name')";
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Style Genie <small></small></h2>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of User's Having Style Genie Used
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>User Profile</th>
                                <th>User Name</th>
                                <th>User E-Mail</th>
                                <th>User Contact</th>
                                <th>User Gender</th>
                                <th>User City</th>
                                <th>Stylist</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="userData"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    function loadUsersData(){
        var storeName = $("#store_name").val();
        if(storeName == ""){
            showError("Session Timed Out !!! Please Login Again","red");
            return false;
        }
        var url = "api/userProcess.php";
        $.post(url,{"type":"getAllUsersData","store_name":storeName},function(data){
            var Status = data.Status;
            var dataShow = "";
            var ids = [];
            if(Status == "Success"){
                var userData = data.UserData;
                for(var i=0;i<userData.length;i++){
                    if(ids.indexOf(userData[i].user_id)>-1){
                        //alert("data already exist");
                    }else {
                        ids.push(userData[i].user_id);
                        dataShow = dataShow + "<tr><td><img alt='Image' src='" + userData[i].profile_pic + "' " +
                        "style='height:40px !important;width:40px;border:2px solid #333'/></td><td>" + userData[i].name + "</td>" +
                        "<td>" + userData[i].email + "</td><td>" + userData[i].mobile + "</td><td>" + userData[i].gender + "</td>" +
                        "<td>" + userData[i].b_city + "</td><td>"+storeName+"</td><td><button class='btn btn-sm btn-info' " +
                        "onclick=viewBodyStyle('" + userData[i].user_id + "')>Show Body Style</button></td></tr>";
                    }
                }
            }else{
                dataShow = "<tr><td colspan=8 style='text-align:center'>"+data.Message+"</td></tr>";
            }
            $("#userData").html(dataShow);
        }).fail(function(){
            showError("Server Error !!! Please Try After Some Time...","red");
        });
    }
    function viewBodyStyle(user_id){
        var url = "../api/registerUser.php";
        $.post(url,{"type":"getStyleGenieData","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                window.location='styledetail.php?user_id='+user_id;
            }
            else{
                showError("No Body Style Data Available","red");
            }
        }).fail(function(){
            alert("Server Error !!! Please Try After Some Time");
        });
    }
    loadUsersData();
</script>
