<?php
namespace Modals;
require_once "CONNECT.php";
require_once('PHPExcel.php');
class USERS{
    private $link = null;
    private $link2 = null;
    public $CurrentDateTime = null;
    function __construct(){
        $this->link = new CONNECT();
        $this->link2 = new CONNECT();
        $this->CurrentDateTime = date("Y-m-d h:i:s");
        $this->objPHPExcel = new \PHPExcel();
        $this->objPHPExcel->getDefaultStyle()->getFont()->setName('Calibri');
        $this->objPHPExcel->getDefaultStyle()->getFont()->setSize(10);
        $this->objWriter = \PHPExcel_IOFactory::createWriter($this->objPHPExcel, "Excel2007");
        $currencyFormat = '#,#0.## \€;[Red]-#,#0.## \€';
        $numberFormat = '#,#0.##;[Red]-#,#0.##';
        $this->objSheet = $this->objPHPExcel->getActiveSheet();
    }
    public function getParticularUserData($userId){
        if($this->link->Connect()){
            $query = "select * from users where user_id = '$userId'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $userData = mysqli_fetch_assoc($result);
                    $query2 = "select * from brands";
                    $result2 = mysqli_query($this->link->Connect(),$query2);
                    $brands = array();
                    while($rows2 = mysqli_fetch_array($result2)){
                        $brands[] = array("brand_name"=>$rows2['brand_name']);
                    }
                    $qq = mysqli_query($this->link->Connect(),"select * from wp_customers where user_id='$userId'");
                    $rr = mysqli_fetch_assoc($qq);
                    $this->link->response[Status] = Success;
                    $this->link->response[Message] = "User Data Found";
                    $this->link->response['userData'] = $userData;
                    $this->link->response['country'] = $rr['b_country'];
                    $this->link->response['brands'] = $brands;
                }
                else{
                    $this->link->response[Status] = Error;
                    $this->link->response[Message] = "UnAuthorized User";
                }
            }
            else{
                $this->link->response[Status] = Error;
                $this->link->response[Message] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[Status] = Error;
            $this->link->response[Message] = $this->link->sqlError();
        }
        return $this->link->response;
    }
    public function getAllMatchingData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from sizematching";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $sizeMatchingData = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $sizeMatchingData[] = $rows;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Size Matching Found SuccessFully";
                    $this->link->response['sizeMatchingData'] = $sizeMatchingData;
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid Size Matching Identification";
                }
            }else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getSize($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id = '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rows = mysqli_fetch_assoc($result);
                    $height = $rows['height_in'];
                    $height = $height*2.54; ///coversion inches to cm
                    $min_height = $height-3;
                    $max_height = $height+3;
                    $weight = $rows['weight'];
                    $weight = $weight*0.453592; ///conversion of lbs to kilogram
                    $min_weight = $weight-3;
                    $max_weight = $weight+3;
                    $gender = $rows['gender'];
                    $neck = $rows['neck'];
                    $waist = $rows['waist'];
                    $waist = $waist*2.54; ///conversion inches to cm
                    $sizeresponse = $this->getsizeData($min_height,$max_height,$min_weight,$max_weight,$neck,$waist,$user_id);
                    if($sizeresponse[Status] == Error){
                        $this->link->response[Status]=Error;
                        $this->link->response[Message]="No Twin Found";
                    }else{
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Size Found";
                        $this->link->response['sizeData_in'] = $sizeresponse['sizeData_in'];
                        $this->link->response['sizeData_cm'] = $sizeresponse['sizeData_cm'];
                        $this->createOrdFile($sizeresponse['sizeData_in'],$rows,"in",$user_id);
                        $this->createOrdFile($sizeresponse['sizeData_cm'],$rows,"cm",$user_id);
                    }
                } else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid User";
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getDatafromOrd($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from ord_files where user_id = '$user_id' order by _id DESC limit 1";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rows = mysqli_fetch_assoc($result);
                    $file_name = $rows['file_name'];
                    $myfile = fopen(getcwd()."/Files/users/user".$user_id."/".$file_name, "r") or die("Unable to open file!");
                    $file = fread($myfile,filesize(getcwd()."/Files/users/user".$user_id."/".$file_name));
                    $newarray = explode("MEASURE",$file);
                    $temp = explode("OPTION UNITS=",$newarray[0]);
                    $unit = explode("#",$temp[1]);
                    $unit = trim($unit[0]);	/// successfully got unit type here
                    if($unit == "inch"){
                        //conversion from inch to cm////////////////////
                        $Height = explode("=",$newarray[1]);
                        $Height = $Height[1]*2.54;
                        $Height = round($Height);
                        $Waist = explode("=",$newarray[12]);
                        $Waist = $Waist[1]*2.54;
                        $Waist = round($Waist);
                        $Weight = explode("=",$newarray[145]);
                        $Weight = $Weight[1];
                        $Weight = round($Weight);
                        $Neck = explode("=",$newarray[2]);
                        $Neck = $Neck[1];///no conversion required inches
                        $Neck = round($Neck);
                    }
                    else if($unit == "mm"){
                        //now convert mm to cm by multiply by 0.1////////////////////
                        $Height = explode("=",$newarray[1]);
                        $Height = $Height[1]*0.1;
                        $Height = round($Height);
                        $Waist = explode("=",$newarray[12]);
                        $Waist = $Waist[1]*0.1;
                        $Waist = round($Waist);
                        $Weight = explode("=",$newarray[145]);
                        $Weight = $Weight[1];
                        $Weight = round($Weight);
                        $Neck = explode("=",$newarray[2]);
                        $Neck = $Neck[1]*0.0393701; //conversion to inch
                        $Neck = round($Neck);
                    }
                    else{
                        //no conversion required cm////////////////////
                        $Height = explode("=",$newarray[1]);
                        $Height = $Height[1];
                        $Height = round($Height).";";
                        $Waist = explode("=",$newarray[13]);
                        $Waist = $Waist[1];
                        $Waist = round($Waist).";";
                        $Weight = explode("=",$newarray[146]);
                        $Weight = $Weight[1];
                        $Weight = round($Weight).";";
                        $Neck = explode("=",$newarray[2]);
                        $Neck = $Neck[1]/2.54; ///required in inches
                        $Neck = round($Neck).";";
                    }
                    $min_height = $Height-3;
                    $max_height = $Height+3;
                    $min_weight = $Weight-3;
                    $max_weight = $Weight+3;
                    $sizeresponse = $this->getsizeData($min_height,$max_height,$min_weight,$max_weight,$Neck,$Waist,$user_id);
                    if($sizeresponse[Status] == Error){
                        $this->link->response[Status]=Error;
                        $this->link->response[Message]="No Twin Found";
                    }else{
                        $this->link->response[Status] = Success;
                        $this->link->response[Message] = "Size Found";
                        $this->link->response['sizeData_in'] = $sizeresponse['sizeData_in'];
                        $this->link->response['sizeData_cm'] = $sizeresponse['sizeData_cm'];
                        $this->createOrdFile($sizeresponse['sizeData_in'],$rows,"in",$user_id);
                        $this->createOrdFile($sizeresponse['sizeData_cm'],$rows,"cm",$user_id);
                    }
                } else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid User";
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function createOrdFile($sizeData,$userData,$ut,$user_id){
        if(!file_exists("Files/users/ord/".$ut."ord".$user_id.".ord")){
            $myfile = fopen("Files/users/ord/".$ut."ord".$user_id.".ord", "w") or die("Unable to open file!");
            $content = "\n\nUser Detail\n\n";
            foreach ($userData as $key=>$val) {
                $content = $content.$key." : ".$val."\n";
            }
            $content = $content."\n\nMeasurements are\n\n";
            foreach ($sizeData as $key=>$val) {
                $content = $content.$key." : ".$val."\n";
            }
            fwrite($myfile, $content);
            fclose($myfile);
        }else{
            unlink("Files/users/ord/".$ut."ord".$user_id.".ord");
            $myfile = fopen("Files/users/ord/".$ut."ord".$user_id.".ord", "w") or die("Unable to open file!");
            $content = "\n\nUser Detail\n\n";
            foreach ($userData as $key=>$val) {
                $content = $content.$key." : ".$val."\n";
            }
            $content = $content."\n\nMeasurements are\n\n";
            foreach ($sizeData as $key=>$val) {
                $content = $content.$key." : ".$val."\n";
            }
            fwrite($myfile, $content);
            fclose($myfile);
        }
        /*if(!file_exists("Files/users/ord/".$ut."ord".$user_id.".xls")){
            $this->objSheet->mergeCells('A1:C1');
            $this->objSheet->getStyle('A1')->getFont()->setSize(12);
            $this->objSheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->objSheet->getCell('A1')->setValue('Size Found Data');
            $this->objSheet->getStyle('A3:F3')->getFont()->setBold(true)->setSize(10);
            $this->objSheet->getCell('A3')->setValue('S.No');
            $this->objSheet->getCell('B3')->setValue('Parameters');
            $this->objSheet->getCell('C3')->setValue('Values');
            // autosize the columns
            $this->objSheet->getColumnDimension('A')->setAutoSize(true);
            $this->objSheet->getColumnDimension('B')->setAutoSize(true);
            $this->objSheet->getColumnDimension('C')->setAutoSize(true);
            $this->objSheet->getStyle('A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->objSheet->getStyle('B')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->objSheet->getStyle('C')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            // we could get this data from database, but here we are writing for simplicity
            $this->objSheet->mergeCells('A5:C5');
            $this->objSheet->getStyle('A5:C5')->getFont()->setBold(true)->setSize(10);
            $this->objSheet->getCell('A5')->setValue('User Detail');
            $i=6;
            $j=0;
            foreach ($userData as $key=>$val) {
                $j++;
                $this->objSheet->getCell("A$i")->setValue($j);
                $this->objSheet->getCell("B$i")->setValue($key);
                $this->objSheet->getCell("C$i")->setValue($val);
                $i++;
            }
            $i=$i+2;
            $this->objSheet->mergeCells("A$i:C$i");
            $this->objSheet->getStyle("A$i:C$i")->getFont()->setBold(true)->setSize(10);
            $this->objSheet->getCell("A$i")->setValue('Measurements');
            $i=$i+1;
            foreach ($sizeData as $key=>$val) {
                $j++;
                $this->objSheet->getCell("A$i")->setValue($j);
                $this->objSheet->getCell("B$i")->setValue($key);
                $this->objSheet->getCell("C$i")->setValue($val);
                $i++;
            }
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=ORD'.date("hmis").'.xls');
            header('Cache-Control: max-age=0');
            $val = "new part ".$this->objWriter->save("Files/users/ord/".$ut."ord".$user_id.".xls");
        }else{
            unlink("Files/users/ord/".$ut."ord".$user_id.".xls");
            $this->objSheet->mergeCells('A1:C1');
            $this->objSheet->getStyle('A1')->getFont()->setSize(12);
            $this->objSheet->getStyle('A1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            $this->objSheet->getCell('A1')->setValue('Size Found Data');
            $this->objSheet->getStyle('A3:F3')->getFont()->setBold(true)->setSize(10);
            $this->objSheet->getCell('A3')->setValue('S.No');
            $this->objSheet->getCell('B3')->setValue('Parameters');
            $this->objSheet->getCell('C3')->setValue('Values');
            // autosize the columns
            $this->objSheet->getColumnDimension('A')->setAutoSize(true);
            $this->objSheet->getColumnDimension('B')->setAutoSize(true);
            $this->objSheet->getColumnDimension('C')->setAutoSize(true);
            $this->objSheet->getStyle('A')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->objSheet->getStyle('B')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            $this->objSheet->getStyle('C')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
            // we could get this data from database, but here we are writing for simplicity
            $this->objSheet->mergeCells('A5:C5');
            $this->objSheet->getStyle('A5:C5')->getFont()->setBold(true)->setSize(10);
            $this->objSheet->getCell('A5')->setValue('User Detail');
            $i=6;
            $j=0;
            foreach ($userData as $key=>$val) {
                $j++;
                $this->objSheet->getCell("A$i")->setValue($j);
                $this->objSheet->getCell("B$i")->setValue($key);
                $this->objSheet->getCell("C$i")->setValue($val);
                $i++;
            }
            $i=$i+2;
            $this->objSheet->mergeCells("A$i:C$i");
            $this->objSheet->getStyle("A$i:C$i")->getFont()->setBold(true)->setSize(10);
            $this->objSheet->getCell("A$i")->setValue('Measurements');
            $i=$i+1;
            foreach ($sizeData as $key=>$val) {
                $j++;
                $this->objSheet->getCell("A$i")->setValue($j);
                $this->objSheet->getCell("B$i")->setValue($key);
                $this->objSheet->getCell("C$i")->setValue($val);
                $i++;
            }
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename=ORD'.date("hmis").'.xls');
            header('Cache-Control: max-age=0');
            $val =  "old part ".$this->objWriter->save("Files/users/ord/".$ut."ord".$user_id.".xls");
        }*/
    }
    public function getSizeData($min_height,$max_height,$min_weight,$max_weight,$neck,$waist,$user_id){
        $link = $this->link->connect();
        if($link) {
            $min_neck = $neck-3;
            $max_neck = $neck+3;
            $min_waist = $waist-3;
            $max_waist = $waist+3;
            $query = "select * from size_data where Height>='$min_height' and Height<='$max_height' and 
            Weight_in_kg>='$min_weight' and Weight_in_kg<='$max_weight' and Neck_Width>='$min_neck' and
            Neck_Width<='$max_neck' and Waist >= '$min_waist' and Waist <= '$max_waist'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $name = $row['Name'];
                    $sizeQuery = "select * from size_datav3 where Name = '$name'";
                    $sizeresult = mysqli_query($link,$sizeQuery);
                    if($sizeresult){
                        $numsize = mysqli_num_rows($sizeresult);
                        if($numsize>0){
                            $this->link->response[STATUS] = Success;
                            $this->link->response[MESSAGE] = "Data Found";
                            $sizerows = mysqli_fetch_assoc($sizeresult);
                            $userResponse = $this->getParticularUserData($user_id);
                            $userData = $userResponse['userData'];
                            $sizeData_in = array(
                                "Unit_Type" => "Inches",
                                "Gender" => $userData['gender'],
                                "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                                "Size" =>$userData['size'],
                                "Waist" =>$sizerows['Waist']/2.54,//conversion in inch;
                                "Neck_Width" =>$userData['neck'],//already in inch
                                "Race" =>$userData['race'],
                                "Weight_in_kg" =>$userData['weight'],
                                "Name"=>$sizerows['Name'],
                                "Height"=>$sizerows['Height']*0.393701,
                                "Neck"=>$sizerows['Neck']*0.393701,
                                "t2_Chest"=>$sizerows['2_Chest']*0.393701,
                                "t3_Stomach"=>$sizerows['3_Stomach']*0.393701,
                                "t4_Seat"=>$sizerows['4_Seat']*0.393701,
                                "Stomach"=>$sizerows['Stomach']*0.393701,
                                "Seat"=>$sizerows['Seat']*0.393701,
                                "Bicep"=>$sizerows['Bicep']*0.393701,
                                "Back_Shoulder_Width"=>$sizerows['Back_Shoulder_Width']*0.393701,
                                "Front_Shoulder_Width"=>$sizerows['Front_Shoulder_Width']*0.393701,
                                //"Shoulder"=>$sizerows['Shoulder']*0.393701,
                                "Sleeve_Length"=>$sizerows['Sleeve_Length']*0.393701,
                                //"Shoulder_front"=>$sizerows['Shoulder_front']*0.393701,
                                "Nape_to_Waist"=>$sizerows['Nape_to_Waist']*0.393701,
                                "Back_Jacket_Length"=>$sizerows['Back_Jacket_Length']*0.393701,
                                "Front_Waist_Length"=>$sizerows['Front_Waist_Length']*0.393701,
                                "Thigh"=>$sizerows['Thigh']*0.393701,
                                "U_rise"=>$sizerows['U_rise']*0.393701,
                                "Back_Waist_Height"=>$sizerows['Back_Waist_Height']*0.393701,
                                "Front_Waist_Height"=>$sizerows['Front_Waist_Height']*0.393701,
                                "Pant_Length"=>$sizerows['Pant_Length']*0.393701,
                                "Wrist"=>$sizerows['Wrist']*0.393701,
                                "Calf"=>$sizerows['Calf']*0.393701,
                                "Neck_finished_measurements_regular"=>$sizerows['Neck_finished_measurements_regular']*0.393701,
                                "Sleeve_L_finished_measurements_regular"=>$sizerows['Sleeve_L_finished_measurements_regular']*0.393701,
                                "Biceps_Shirt_finished_measurements_regular"=>$sizerows['Biceps_Shirt_finished_measurements_regular']*0.393701,
                                "Shoulders_finished_measurements_regular"=>$sizerows['Shoulders_finished_measurements_regular']*0.393701,
                                "Stomach_finished_measurements_regular"=>$sizerows['Stomach_finished_measurements_regular']*0.393701,
                                "Sleeve_R_finished_measurements_regular"=>$sizerows['Sleeve_R_finished_measurements_regular']*0.393701,
                                "Chest_finished_measurements__Regular"=>$sizerows['Chest_finished_measurements__Regular']*0.393701,
                                "Left_Cuff_finished_measurements_regular"=>$sizerows['Left_Cuff_finished_measurements_regular']*0.393701,
                                "Back_Lenght_Finished_measurements_regular"=>$sizerows['Back_Lenght_Finished_measurements_regular']*0.393701,
                                //"Seat_lessthen_2cm_Finished_measurements_regular"=>$sizerows['Seat_lessthen_2cm_Finished_measurements_regular']*0.393701,
                                "Righ_Cuff_Finished_measurements_regular"=>$sizerows['Righ_Cuff_Finished_measurements_regular']*0.393701,
                                "Bicept_Jacket_finished_measurements"=>$sizerows['Bicept_Jacket_finished_measurements']*0.393701,
                                "Sleeve_L_Jacket_Regular"=>$sizerows['Sleeve_L_Jacket_Regular']*0.393701,
                                "Shoulder_Jacket_Regular"=>$sizerows['Shoulder_Jacket_Regular']*0.393701,
                                "Stomach_Jacket_Regular"=>$sizerows['Stomach_Jacket_Regular']*0.393701,
                                "Sleeve_R_Jacket_Regular"=>$sizerows['Sleeve_R_Jacket_Regular']*0.393701,
                                "Chest_Jacket_Regular"=>$sizerows['Chest_Jacket_Regular']*0.393701,
                                "Back_Length_Jacket_Regular"=>$sizerows['Back_Length_Jacket_Regular']*0.393701,
                                "Seat__Jacket_Regular"=>$sizerows['Seat__Jacket_Regular']*0.393701,
                                "Waist_pants"=>$sizerows['Waist_pants']*0.393701,
                                "U_rise_finished_regal_fit"=>$sizerows['U_rise_finished_regal_fit']*0.393701,
                                "Pants_Tight_finished_measurement"=>$sizerows['Pants_Tight_finished_measurement']*0.393701,
                                "Outseam_L_pants"=>$sizerows['Outseam_L_pants']*0.393701,
                                "Outseam_R_Pants"=>$sizerows['Outseam_R_Pants']*0.393701,
                                "Knee_Pants"=>$sizerows['Knee_Pants']*0.393701,
                                "Seat_Pants"=>$sizerows['Seat_Pants']*0.393701,
                                "Bottom_for_trousers"=>$sizerows['Bottom_for_trousers']*0.393701,
                                "Body_Position_Forward__Backward"=>$sizerows['Body_Position_Forward__Backward']*0.393701,
                                "Shoulder_position_Forward__Backward"=>$sizerows['Shoulder_position_Forward__Backward']*0.393701,
                                "Waist_line_High"=>$sizerows['Waist_line_High']*0.393701,
                                "Waist_line_Low"=>$sizerows['Waist_line_Low']*0.393701,
                                "shoulder_based_on_Overarm_full_Back"=>$sizerows['shoulder_based_on_Overarm_full_Back']*0.393701,
                                "shoulder_based_on_Overarm_full_Front"=>$sizerows['shoulder_based_on_Overarm_full_Front']*0.393701,
                                "Collar"=>$sizerows['Collar']*0.393701,
                                "Bust"=>$sizerows['Bust']*0.393701,
                                "Chest"=>$sizerows['Chest']*0.393701,
                                "Chest_Front"=>$sizerows['Chest_Front']*0.393701,
                                "Chest_Back"=>$sizerows['Chest_Back']*0.393701,
                                "Chest_Height"=>$sizerows['Chest_Height']*0.393701,
                                "Hips"=>$sizerows['Hips']*0.393701,
                                "NecktoHips"=>$sizerows['NecktoHips']*0.393701,
                                "Shoulder2Shoulder_45"=>$sizerows['Shoulder2Shoulder_45']*0.393701,
                                "Neck2Shoulderblade_Horizontal"=>$sizerows['Neck2Shoulderblade_Horizontal']*0.393701,
                                "Schoulder_overarm_Full"=>$sizerows['Schoulder_overarm_Full']*0.393701,
                                "Chest2_schoulder_front_Full"=>$sizerows['Chest2_schoulder_front_Full']*0.393701,
                                "Shoulder2Shoulder_thru_CBN"=>$sizerows['Shoulder2Shoulder_thru_CBN']*0.393701,
                                "Bust2Bust_Horizontal"=>$sizerows['Bust2Bust_Horizontal']*0.393701,
                                "underwear_Waist"=>$sizerows['underwear_Waist']*0.393701,
                                "pants_Waist_rcmtm_Full"=>$sizerows['pants_Waist_rcmtm_Full']*0.393701,
                                "underwear_Waist_Front"=>$sizerows['underwear_Waist_Front']*0.393701,
                                "underwear_Waist_Back"=>$sizerows['underwear_Waist_Back']*0.393701,
                                "underwear_Waist_Height_Front"=>$sizerows['underwear_Waist_Height_Front']*0.393701,
                                "underwear_Waist_Height_Back"=>$sizerows['underwear_Waist_Height_Back']*0.393701,
                                "underwear_Waist_Height_Left"=>$sizerows['underwear_Waist_Height_Left']*0.393701,
                                "underwear_Waist_Height_Right"=>$sizerows['underwear_Waist_Height_Right']*0.393701,
                                "Hips_Front"=>$sizerows['Hips_Front']*0.393701,
                                "Hips_Back"=>$sizerows['Hips_Back']*0.393701,
                                "Outseam_Left"=>$sizerows['Outseam_Left']*0.393701,
                                "Outseam_Right"=>$sizerows['Outseam_Right']*0.393701,
                                "Inseam_Left"=>$sizerows['Inseam_Left']*0.393701,
                                "Inseam_Right"=>$sizerows['Inseam_Right']*0.393701,
                                "Overarm"=>$sizerows['Overarm']*0.393701,
                                "Shirt_Sleeve_Left"=>$sizerows['Shirt_Sleeve_Left']*0.393701,
                                "Shirt_Sleeve_Right"=>$sizerows['Shirt_Sleeve_Right']*0.393701,
                                "Across_Chest"=>$sizerows['Across_Chest']*0.393701,
                                "Shoulder_Length_Left"=>$sizerows['Shoulder_Length_Left']*0.393701,
                                "Shoulder_Length_Right"=>$sizerows['Shoulder_Length_Right']*0.393701,
                                "Underbust"=>$sizerows['Underbust']*0.393701,
                                "SideNeck2Bust_Left"=>$sizerows['SideNeck2Bust_Left']*0.393701,
                                "SideNeck2Bust_Right"=>$sizerows['SideNeck2Bust_Right']*0.393701,
                                "Neck2Waist_Front"=>$sizerows['Neck2Waist_Front']*0.393701,
                                "Neck2Waist_Back"=>$sizerows['Neck2Waist_Back']*0.393701,
                                "Shoulder_Slope_Left"=>$sizerows['Shoulder_Slope_Left']*0.393701,
                                "Shoulder_Slope_Right"=>$sizerows['Shoulder_Slope_Right']*0.393701,
                                "Hips_Height"=>$sizerows['Hips_Height']*0.393701,
                                "Knee_Height"=>$sizerows['Knee_Height']*0.393701,
                                "CrotchLength"=>$sizerows['CrotchLength']*0.393701,
                                "CrotchLength_Front"=>$sizerows['CrotchLength_Front']*0.393701,
                                "CrotchLength_Back"=>$sizerows['CrotchLength_Back']*0.393701,
                                "ArmLength_Left"=>$sizerows['ArmLength_Left']*0.393701,
                                "ArmLength_Right"=>$sizerows['ArmLength_Right']*0.393701,
                                "SlvIn_L"=>$sizerows['SlvIn_L']*0.393701,
                                "SlvIn_R"=>$sizerows['SlvIn_R']*0.393701,
                                "Biceps_Left"=>$sizerows['Biceps_Left']*0.393701,
                                "Biceps_Right"=>$sizerows['Biceps_Right']*0.393701,
                                "Max_Biceps"=>$sizerows['Max_Biceps']*0.393701,
                                "Wrist_Left"=>$sizerows['Wrist_Left']*0.393701,
                                "Wrist_Right"=>$sizerows['Wrist_Right']*0.393701,
                                "Neck_Front_Height"=>$sizerows['Neck_Front_Height']*0.393701,
                                "Neck_Back_Height"=>$sizerows['Neck_Back_Height']*0.393701,
                                "Neck_Left_Height"=>$sizerows['Neck_Left_Height']*0.393701,
                                "Neck_Right_Height"=>$sizerows['Neck_Right_Height']*0.393701,
                                "BustToWaist_Left"=>$sizerows['BustToWaist_Left']*0.393701,
                                "BustToWaist_Right"=>$sizerows['BustToWaist_Right']*0.393701,
                                "Shoulder2Shoulder_Horiz"=>$sizerows['Shoulder2Shoulder_Horiz']*0.393701,
                                "Coat_Outsleeve_Left"=>$sizerows['Coat_Outsleeve_Left']*0.393701,
                                "Coat_Outsleeve_Right"=>$sizerows['Coat_Outsleeve_Right']*0.393701,
                                "Stomach_Height"=>$sizerows['Stomach_Height']*0.393701,
                                "Abdomen"=>$sizerows['Abdomen']*0.393701,
                                "Abdomen_Height"=>$sizerows['Abdomen_Height']*0.393701,
                                "Shoulder_Height_Left"=>$sizerows['Shoulder_Height_Left']*0.393701,
                                "Shoulder_Height_Right"=>$sizerows['Shoulder_Height_Right']*0.393701,
                                "Armpit_Avg_Height"=>$sizerows['Armpit_Avg_Height']*0.393701,
                                "Overarm_50"=>$sizerows['Overarm_50']*0.393701,
                                "Overarm_70"=>$sizerows['Overarm_70']*0.393701,
                                "Thigh_Length_Left"=>$sizerows['Thigh_Length_Left']*0.393701,
                                "Thigh_Length_Right"=>$sizerows['Thigh_Length_Right']*0.393701,
                                "Thigh_Max"=>$sizerows['Thigh_Max']*0.393701,
                                "Knee_Left"=>$sizerows['Knee_Left']*0.393701,
                                "Knee_Right"=>$sizerows['Knee_Right']*0.393701,
                                "Ankle_Girth_Left"=>$sizerows['Ankle_Girth_Left']*0.393701,
                                "Ankle_Girth_Right"=>$sizerows['Ankle_Girth_Right']*0.393701,
                                "Foot_Width_Left"=>$sizerows['Foot_Width_Left']*0.393701,
                                "Foot_Width_Right"=>$sizerows['Foot_Width_Right']*0.393701,
                                "Calf_Left"=>$sizerows['Calf_Left']*0.393701,
                                "Calf_Right"=>$sizerows['Calf_Right']*0.393701,
                                "Thigh_Left"=>$sizerows['Thigh_Left']*0.393701,
                                "Thigh_Right"=>$sizerows['Thigh_Right']*0.393701,
                                "Seat_Height"=>$sizerows['Seat_Height']*0.393701,
                                "Seat_Width"=>$sizerows['Seat_Width']*0.393701,
                                "Seat_Back_Angle"=>$sizerows['Seat_Back_Angle']*0.393701,
                                "Waist_to_Seat_Back"=>$sizerows['Waist_to_Seat_Back']*0.393701,
                                "Waist_to_Hips_Back"=>$sizerows['Waist_to_Hips_Back']*0.393701,
                                "Bulk_Volume"=>$sizerows['Bulk_Volume']*0.393701,
                                "high_hips_height"=>$sizerows['high_hips_height']*0.393701,
                                "high_hips_Full"=>$sizerows['high_hips_Full']*0.393701,
                                "Upper_Hip_Girth_Height"=>$sizerows['Upper_Hip_Girth_Height']*0.393701,
                                "Upper_Hips_Girth_Full"=>$sizerows['Upper_Hips_Girth_Full']*0.393701,
                                "Neck_to_High_Hips"=>$sizerows['Neck_to_High_Hips']*0.393701,
                                "Armscye_Circumference_Left"=>$sizerows['Armscye_Circumference_Left']*0.393701,
                                "Armscye_Circumference_Right"=>$sizerows['Armscye_Circumference_Right']*0.393701,
                                "Armscye_Width_Left_Tape"=>$sizerows['Armscye_Width_Left_Tape']*0.393701,
                                "Armscye_Width_Right_Tape"=>$sizerows['Armscye_Width_Right_Tape']*0.393701,
                                "Center_Trunk_Circumference_Full"=>$sizerows['Center_Trunk_Circumference_Full']*0.393701,
                                "Shoulder_2_Shoulder_Caliper_straight_line"=>$sizerows['Shoulder_2_Shoulder_Caliper_straight_line']*0.393701,
                                "Shoulder_2_Shoulder_Shortest_Path"=>$sizerows['Shoulder_2_Shoulder_Shortest_Path']*0.393701,
                                "LeftBkWaist2MidShldr"=>$sizerows['LeftBkWaist2MidShldr']*0.393701,
                                "LeftMidShldr2Bust"=>$sizerows['LeftMidShldr2Bust']*0.393701,
                                "LeftBustPt2underbustPt"=>$sizerows['LeftBustPt2underbustPt']*0.393701,
                                "LeftUnderbust2FrontWaist"=>$sizerows['LeftUnderbust2FrontWaist']*0.393701,
                                "LeftBackWaist2Shoulder"=>$sizerows['LeftBackWaist2Shoulder']*0.393701,
                                "LeftShoulder2FrontWaist"=>$sizerows['LeftShoulder2FrontWaist']*0.393701,
                                "LeftBackWaist2SideNeck"=>$sizerows['LeftBackWaist2SideNeck']*0.393701,
                                "LeftSideNeck2FrontWaist"=>$sizerows['LeftSideNeck2FrontWaist']*0.393701,
                                "CrownHeight_Left"=>$sizerows['CrownHeight_Left']*0.393701,
                                "CrownHeight_Right"=>$sizerows['CrownHeight_Right']*0.393701,
                                "Across_Chest_Normal"=>$sizerows['Across_Chest_Normal']*0.393701,
                                "Across_Back_Caliper"=>$sizerows['Across_Back_Caliper']*0.393701,
                                "Across_Chest_Caliper"=>$sizerows['Across_Chest_Caliper']*0.393701,
                                "SideNeck_2ArmscyeLevelLeftFront"=>$sizerows['SideNeck_2ArmscyeLevelLeftFront']*0.393701,
                                "SideNeck_2ArmscyeLevelRightFront"=>$sizerows['SideNeck_2ArmscyeLevelRightFront']*0.393701,
                                "SideNeck_2ArmscyeLevelLeftBack"=>$sizerows['SideNeck_2ArmscyeLevelLeftBack']*0.393701,
                                "SideNeck_2ArmscyeLevelRightBack"=>$sizerows['SideNeck_2ArmscyeLevelRightBack']*0.393701,
                                "SideNeck_2Bust2SmallOfBackLevelLeftFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelLeftFront']*0.393701,
                                "SideNeck_2Bust2SmallOfBackLevelRightFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelRightFront']*0.393701,
                                "Center_Trunk_Circumference_Front"=>$sizerows['Center_Trunk_Circumference_Front']*0.393701,
                                "Center_Trunk_Circumference_Back"=>$sizerows['Center_Trunk_Circumference_Back']*0.393701,
                                "Center_Trunk_Circumference_Full_Coutoured"=>$sizerows['Center_Trunk_Circumference_Full_Coutoured']*0.393701,
                                "Center_Trunk_Circumference_Front_Coutoured"=>$sizerows['Center_Trunk_Circumference_Front_Coutoured']*0.393701,
                                "Center_Trunk_Circumference_Back_Coutoured"=>$sizerows['Center_Trunk_Circumference_Back_Coutoured']*0.393701,
                                "Center_Trunk_Circumference_Over_Left_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Left_Shoulder']*0.393701,
                                "Center_Trunk_Circumference_Over_Right_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Right_Shoulder']*0.393701,
                                "Neck2Shoulderblade_Distance"=>$sizerows['Neck2Shoulderblade_Distance']*0.393701,
                                "Neck2Shoulderblade_Vertical"=>$sizerows['Neck2Shoulderblade_Vertical']*0.393701,
                                "Nape_to_Waist_Front"=>$sizerows['Nape_to_Waist_Front']*0.393701,
                                "Nape_to_Waist_Back"=>$sizerows['Nape_to_Waist_Back']*0.393701,
                                "rcmtm_Waist_to_Seat2_Front"=>$sizerows['rcmtm_Waist_to_Seat2_Front']*0.393701,
                                "rcmtm_Waist_to_Seat2_Back"=>$sizerows['rcmtm_Waist_to_Seat2_Back']*0.393701,
                                "rcmtm_Waist_to_Hips2_Front"=>$sizerows['rcmtm_Waist_to_Hips2_Front']*0.393701,
                                "rcmtm_Waist_to_Hips2_Back"=>$sizerows['rcmtm_Waist_to_Hips2_Back']*0.393701,
                                "Back_Side_Waist_to_Under_Buttocks_Right"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Right']*0.393701,
                                "Back_Side_Waist_to_Under_Buttocks_Left"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Left']*0.393701,
                                "Back_Side_Waist_to_Crotch_Level_Right"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Right']*0.393701,
                                "Back_Side_Waist_to_Crotch_Level_Left"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Left']*0.393701,
                                "Neck2Waist_rcmtm_Front"=>$sizerows['Neck2Waist_rcmtm_Front']*0.393701,
                                "CrotchLength_rcmtm_Full"=>$sizerows['CrotchLength_rcmtm_Full']*0.393701,
                                "CrotchLength_rcmtm_Front"=>$sizerows['CrotchLength_rcmtm_Front']*0.393701,
                                "CrotchLength_rcmtm_Back"=>$sizerows['CrotchLength_rcmtm_Back']*0.393701,
                                "pants_Waist_rcmtm_Back"=>$sizerows['pants_Waist_rcmtm_Back']*0.393701,
                                "pants_Waist_rcmtm_Height_Front"=>$sizerows['pants_Waist_rcmtm_Height_Front']*0.393701,
                                "pants_Waist_rcmtm_Height_Back"=>$sizerows['pants_Waist_rcmtm_Height_Back']*0.393701,
                                "Outseam_Max"=>$sizerows['Outseam_Max']*0.393701,
                                "Outseam_rcmtm_Left"=>$sizerows['Outseam_rcmtm_Left']*0.393701,
                                "Outseam_rcmtm_Right"=>$sizerows['Outseam_rcmtm_Right']*0.393701,
                                "Outseam_rcmtm_Max"=>$sizerows['Outseam_rcmtm_Max']*0.393701,
                                "Waist_small_back_middle_Full"=>$sizerows['Waist_small_back_middle_Full']*0.393701,
                                "Waist_small_back_middle_Front"=>$sizerows['Waist_small_back_middle_Front']*0.393701,
                                "Waist_small_back_middle_Back"=>$sizerows['Waist_small_back_middle_Back']*0.393701,
                                "Waist_smal_back_middle_Height_Front"=>$sizerows['Waist_smal_back_middle_Height_Front']*0.393701,
                                "Waist_smal_back_middle_Height_Back"=>$sizerows['Waist_smal_back_middle_Height_Back']*0.393701,
                                "Waist_smal_back_middle_Height_Left"=>$sizerows['Waist_smal_back_middle_Height_Left']*0.393701,
                                "Waist_smal_back_middle_Height_Right"=>$sizerows['Waist_smal_back_middle_Height_Right']*0.393701,
                                "Neck2Waist_middle_horizontal_line_Front"=>$sizerows['Neck2Waist_middle_horizontal_line_Front']*0.393701,
                                "Neck2Waist_middle_horizontal_line_Back"=>$sizerows['Neck2Waist_middle_horizontal_line_Back']*0.393701,
                                "Stomach_belt_to_measure_Full"=>$sizerows['Stomach_belt_to_measure_Full']*0.393701,
                                "Stomach_belt_to_measure_Height_Front"=>$sizerows['Stomach_belt_to_measure_Height_Front']*0.393701,
                                "Stomach_belt_to_measure_Height_Back"=>$sizerows['Stomach_belt_to_measure_Height_Back']*0.393701,
                                "Schoulder_overarm_Height"=>$sizerows['Schoulder_overarm_Height']*0.393701,
                                "waist_belly_botton_Full"=>$sizerows['waist_belly_botton_Full']*0.393701,
                                "waist_belly_botton_Height_Front"=>$sizerows['waist_belly_botton_Height_Front']*0.393701,
                                "waist_belly_botton_Height_Back"=>$sizerows['waist_belly_botton_Height_Back']*0.393701
                            );
                            $sizeData_cm = array(
                                "Unit_Type" => "Centimeters",
                                "Gender" => $userData['gender'],
                                "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                                "Size" =>$userData['size'],
                                "Waist" =>$sizerows['Waist'],//already in cm;
                                "Race" =>$userData['race'],
                                "Neck_Width" =>$userData['neck']*2.54,
                                "Weight_in_kg" =>$userData['weight'],
                                "Name"=>$sizerows['Name'],
                                "Height"=>$sizerows['Height'],
                                "Neck"=>$sizerows['Neck'],
                                "t2_Chest"=>$sizerows['2_Chest'],
                                "t3_Stomach"=>$sizerows['3_Stomach'],
                                "t4_Seat"=>$sizerows['4_Seat'],
                                "Stomach"=>$sizerows['Stomach'],
                                "Seat"=>$sizerows['Seat'],
                                "Bicep"=>$sizerows['Bicep'],
                                "Back_Shoulder_Width"=>$sizerows['Back_Shoulder_Width'],
                                "Front_Shoulder_Width"=>$sizerows['Front_Shoulder_Width'],
                                //"Shoulder"=>$sizerows['Shoulder'],
                                "Sleeve_Length"=>$sizerows['Sleeve_Length'],
                                //"Shoulder_front"=>$sizerows['Shoulder_front'],
                                "Nape_to_Waist"=>$sizerows['Nape_to_Waist'],
                                "Back_Jacket_Length"=>$sizerows['Back_Jacket_Length'],
                                "Front_Waist_Length"=>$sizerows['Front_Waist_Length'],
                                "Thigh"=>$sizerows['Thigh'],
                                "U_rise"=>$sizerows['U_rise'],
                                "Back_Waist_Height"=>$sizerows['Back_Waist_Height'],
                                "Front_Waist_Height"=>$sizerows['Front_Waist_Height'],
                                "Pant_Length"=>$sizerows['Pant_Length'],
                                "Wrist"=>$sizerows['Wrist'],
                                "Calf"=>$sizerows['Calf'],
                                "Neck_finished_measurements_regular"=>$sizerows['Neck_finished_measurements_regular'],
                                "Sleeve_L_finished_measurements_regular"=>$sizerows['Sleeve_L_finished_measurements_regular'],
                                "Biceps_Shirt_finished_measurements_regular"=>$sizerows['Biceps_Shirt_finished_measurements_regular'],
                                "Shoulders_finished_measurements_regular"=>$sizerows['Shoulders_finished_measurements_regular'],
                                "Stomach_finished_measurements_regular"=>$sizerows['Stomach_finished_measurements_regular'],
                                "Sleeve_R_finished_measurements_regular"=>$sizerows['Sleeve_R_finished_measurements_regular'],
                                "Chest_finished_measurements__Regular"=>$sizerows['Chest_finished_measurements__Regular'],
                                "Left_Cuff_finished_measurements_regular"=>$sizerows['Left_Cuff_finished_measurements_regular'],
                                "Back_Lenght_Finished_measurements_regular"=>$sizerows['Back_Lenght_Finished_measurements_regular'],
                                //"Seat_lessthen_2cm_Finished_measurements_regular"=>$sizerows['Seat_lessthen_2cm_Finished_measurements_regular'],
                                "Righ_Cuff_Finished_measurements_regular"=>$sizerows['Righ_Cuff_Finished_measurements_regular'],
                                "Bicept_Jacket_finished_measurements"=>$sizerows['Bicept_Jacket_finished_measurements'],
                                "Sleeve_L_Jacket_Regular"=>$sizerows['Sleeve_L_Jacket_Regular'],
                                "Shoulder_Jacket_Regular"=>$sizerows['Shoulder_Jacket_Regular'],
                                "Stomach_Jacket_Regular"=>$sizerows['Stomach_Jacket_Regular'],
                                "Sleeve_R_Jacket_Regular"=>$sizerows['Sleeve_R_Jacket_Regular'],
                                "Chest_Jacket_Regular"=>$sizerows['Chest_Jacket_Regular'],
                                "Back_Length_Jacket_Regular"=>$sizerows['Back_Length_Jacket_Regular'],
                                "Seat__Jacket_Regular"=>$sizerows['Seat__Jacket_Regular'],
                                "Waist_pants"=>$sizerows['Waist_pants'],
                                "U_rise_finished_regal_fit"=>$sizerows['U_rise_finished_regal_fit'],
                                "Pants_Tight_finished_measurement"=>$sizerows['Pants_Tight_finished_measurement'],
                                "Outseam_L_pants"=>$sizerows['Outseam_L_pants'],
                                "Outseam_R_Pants"=>$sizerows['Outseam_R_Pants'],
                                "Knee_Pants"=>$sizerows['Knee_Pants'],
                                "Seat_Pants"=>$sizerows['Seat_Pants'],
                                "Bottom_for_trousers"=>$sizerows['Bottom_for_trousers'],
                                "Body_Position_Forward__Backward"=>$sizerows['Body_Position_Forward__Backward'],
                                "Shoulder_position_Forward__Backward"=>$sizerows['Shoulder_position_Forward__Backward'],
                                "Waist_line_High"=>$sizerows['Waist_line_High'],
                                "Waist_line_Low"=>$sizerows['Waist_line_Low'],
                                "shoulder_based_on_Overarm_full_Back"=>$sizerows['shoulder_based_on_Overarm_full_Back'],
                                "shoulder_based_on_Overarm_full_Front"=>$sizerows['shoulder_based_on_Overarm_full_Front'],
                                "Collar"=>$sizerows['Collar'],
                                "Bust"=>$sizerows['Bust'],
                                "Chest"=>$sizerows['Chest'],
                                "Chest_Front"=>$sizerows['Chest_Front'],
                                "Chest_Back"=>$sizerows['Chest_Back'],
                                "Chest_Height"=>$sizerows['Chest_Height'],
                                "Hips"=>$sizerows['Hips'],
                                "NecktoHips"=>$sizerows['NecktoHips'],
                                "Shoulder2Shoulder_45"=>$sizerows['Shoulder2Shoulder_45'],
                                "Neck2Shoulderblade_Horizontal"=>$sizerows['Neck2Shoulderblade_Horizontal'],
                                "Schoulder_overarm_Full"=>$sizerows['Schoulder_overarm_Full'],
                                "Chest2_schoulder_front_Full"=>$sizerows['Chest2_schoulder_front_Full'],
                                "Shoulder2Shoulder_thru_CBN"=>$sizerows['Shoulder2Shoulder_thru_CBN'],
                                "Bust2Bust_Horizontal"=>$sizerows['Bust2Bust_Horizontal'],
                                "underwear_Waist"=>$sizerows['underwear_Waist'],
                                "pants_Waist_rcmtm_Full"=>$sizerows['pants_Waist_rcmtm_Full'],
                                "underwear_Waist_Front"=>$sizerows['underwear_Waist_Front'],
                                "underwear_Waist_Back"=>$sizerows['underwear_Waist_Back'],
                                "underwear_Waist_Height_Front"=>$sizerows['underwear_Waist_Height_Front'],
                                "underwear_Waist_Height_Back"=>$sizerows['underwear_Waist_Height_Back'],
                                "underwear_Waist_Height_Left"=>$sizerows['underwear_Waist_Height_Left'],
                                "underwear_Waist_Height_Right"=>$sizerows['underwear_Waist_Height_Right'],
                                "Hips_Front"=>$sizerows['Hips_Front'],
                                "Hips_Back"=>$sizerows['Hips_Back'],
                                "Outseam_Left"=>$sizerows['Outseam_Left'],
                                "Outseam_Right"=>$sizerows['Outseam_Right'],
                                "Inseam_Left"=>$sizerows['Inseam_Left'],
                                "Inseam_Right"=>$sizerows['Inseam_Right'],
                                "Overarm"=>$sizerows['Overarm'],
                                "Shirt_Sleeve_Left"=>$sizerows['Shirt_Sleeve_Left'],
                                "Shirt_Sleeve_Right"=>$sizerows['Shirt_Sleeve_Right'],
                                "Across_Chest"=>$sizerows['Across_Chest'],
                                "Shoulder_Length_Left"=>$sizerows['Shoulder_Length_Left'],
                                "Shoulder_Length_Right"=>$sizerows['Shoulder_Length_Right'],
                                "Underbust"=>$sizerows['Underbust'],
                                "SideNeck2Bust_Left"=>$sizerows['SideNeck2Bust_Left'],
                                "SideNeck2Bust_Right"=>$sizerows['SideNeck2Bust_Right'],
                                "Neck2Waist_Front"=>$sizerows['Neck2Waist_Front'],
                                "Neck2Waist_Back"=>$sizerows['Neck2Waist_Back'],
                                "Shoulder_Slope_Left"=>$sizerows['Shoulder_Slope_Left'],
                                "Shoulder_Slope_Right"=>$sizerows['Shoulder_Slope_Right'],
                                "Hips_Height"=>$sizerows['Hips_Height'],
                                "Knee_Height"=>$sizerows['Knee_Height'],
                                "CrotchLength"=>$sizerows['CrotchLength'],
                                "CrotchLength_Front"=>$sizerows['CrotchLength_Front'],
                                "CrotchLength_Back"=>$sizerows['CrotchLength_Back'],
                                "ArmLength_Left"=>$sizerows['ArmLength_Left'],
                                "ArmLength_Right"=>$sizerows['ArmLength_Right'],
                                "SlvIn_L"=>$sizerows['SlvIn_L'],
                                "SlvIn_R"=>$sizerows['SlvIn_R'],
                                "Biceps_Left"=>$sizerows['Biceps_Left'],
                                "Biceps_Right"=>$sizerows['Biceps_Right'],
                                "Max_Biceps"=>$sizerows['Max_Biceps'],
                                "Wrist_Left"=>$sizerows['Wrist_Left'],
                                "Wrist_Right"=>$sizerows['Wrist_Right'],
                                "Neck_Front_Height"=>$sizerows['Neck_Front_Height'],
                                "Neck_Back_Height"=>$sizerows['Neck_Back_Height'],
                                "Neck_Left_Height"=>$sizerows['Neck_Left_Height'],
                                "Neck_Right_Height"=>$sizerows['Neck_Right_Height'],
                                "BustToWaist_Left"=>$sizerows['BustToWaist_Left'],
                                "BustToWaist_Right"=>$sizerows['BustToWaist_Right'],
                                "Shoulder2Shoulder_Horiz"=>$sizerows['Shoulder2Shoulder_Horiz'],
                                "Coat_Outsleeve_Left"=>$sizerows['Coat_Outsleeve_Left'],
                                "Coat_Outsleeve_Right"=>$sizerows['Coat_Outsleeve_Right'],
                                "Stomach_Height"=>$sizerows['Stomach_Height'],
                                "Abdomen"=>$sizerows['Abdomen'],
                                "Abdomen_Height"=>$sizerows['Abdomen_Height'],
                                "Shoulder_Height_Left"=>$sizerows['Shoulder_Height_Left'],
                                "Shoulder_Height_Right"=>$sizerows['Shoulder_Height_Right'],
                                "Armpit_Avg_Height"=>$sizerows['Armpit_Avg_Height'],
                                "Overarm_50"=>$sizerows['Overarm_50'],
                                "Overarm_70"=>$sizerows['Overarm_70'],
                                "Thigh_Length_Left"=>$sizerows['Thigh_Length_Left'],
                                "Thigh_Length_Right"=>$sizerows['Thigh_Length_Right'],
                                "Thigh_Max"=>$sizerows['Thigh_Max'],
                                "Knee_Left"=>$sizerows['Knee_Left'],
                                "Knee_Right"=>$sizerows['Knee_Right'],
                                "Ankle_Girth_Left"=>$sizerows['Ankle_Girth_Left'],
                                "Ankle_Girth_Right"=>$sizerows['Ankle_Girth_Right'],
                                "Foot_Width_Left"=>$sizerows['Foot_Width_Left'],
                                "Foot_Width_Right"=>$sizerows['Foot_Width_Right'],
                                "Calf_Left"=>$sizerows['Calf_Left'],
                                "Calf_Right"=>$sizerows['Calf_Right'],
                                "Thigh_Left"=>$sizerows['Thigh_Left'],
                                "Thigh_Right"=>$sizerows['Thigh_Right'],
                                "Seat_Height"=>$sizerows['Seat_Height'],
                                "Seat_Width"=>$sizerows['Seat_Width'],
                                "Seat_Back_Angle"=>$sizerows['Seat_Back_Angle'],
                                "Waist_to_Seat_Back"=>$sizerows['Waist_to_Seat_Back'],
                                "Waist_to_Hips_Back"=>$sizerows['Waist_to_Hips_Back'],
                                "Bulk_Volume"=>$sizerows['Bulk_Volume'],
                                "high_hips_height"=>$sizerows['high_hips_height'],
                                "high_hips_Full"=>$sizerows['high_hips_Full'],
                                "Upper_Hip_Girth_Height"=>$sizerows['Upper_Hip_Girth_Height'],
                                "Upper_Hips_Girth_Full"=>$sizerows['Upper_Hips_Girth_Full'],
                                "Neck_to_High_Hips"=>$sizerows['Neck_to_High_Hips'],
                                "Armscye_Circumference_Left"=>$sizerows['Armscye_Circumference_Left'],
                                "Armscye_Circumference_Right"=>$sizerows['Armscye_Circumference_Right'],
                                "Armscye_Width_Left_Tape"=>$sizerows['Armscye_Width_Left_Tape'],
                                "Armscye_Width_Right_Tape"=>$sizerows['Armscye_Width_Right_Tape'],
                                "Center_Trunk_Circumference_Full"=>$sizerows['Center_Trunk_Circumference_Full'],
                                "Shoulder_2_Shoulder_Caliper_straight_line"=>$sizerows['Shoulder_2_Shoulder_Caliper_straight_line'],
                                "Shoulder_2_Shoulder_Shortest_Path"=>$sizerows['Shoulder_2_Shoulder_Shortest_Path'],
                                "LeftBkWaist2MidShldr"=>$sizerows['LeftBkWaist2MidShldr'],
                                "LeftMidShldr2Bust"=>$sizerows['LeftMidShldr2Bust'],
                                "LeftBustPt2underbustPt"=>$sizerows['LeftBustPt2underbustPt'],
                                "LeftUnderbust2FrontWaist"=>$sizerows['LeftUnderbust2FrontWaist'],
                                "LeftBackWaist2Shoulder"=>$sizerows['LeftBackWaist2Shoulder'],
                                "LeftShoulder2FrontWaist"=>$sizerows['LeftShoulder2FrontWaist'],
                                "LeftBackWaist2SideNeck"=>$sizerows['LeftBackWaist2SideNeck'],
                                "LeftSideNeck2FrontWaist"=>$sizerows['LeftSideNeck2FrontWaist'],
                                "CrownHeight_Left"=>$sizerows['CrownHeight_Left'],
                                "CrownHeight_Right"=>$sizerows['CrownHeight_Right'],
                                "Across_Chest_Normal"=>$sizerows['Across_Chest_Normal'],
                                "Across_Back_Caliper"=>$sizerows['Across_Back_Caliper'],
                                "Across_Chest_Caliper"=>$sizerows['Across_Chest_Caliper'],
                                "SideNeck_2ArmscyeLevelLeftFront"=>$sizerows['SideNeck_2ArmscyeLevelLeftFront'],
                                "SideNeck_2ArmscyeLevelRightFront"=>$sizerows['SideNeck_2ArmscyeLevelRightFront'],
                                "SideNeck_2ArmscyeLevelLeftBack"=>$sizerows['SideNeck_2ArmscyeLevelLeftBack'],
                                "SideNeck_2ArmscyeLevelRightBack"=>$sizerows['SideNeck_2ArmscyeLevelRightBack'],
                                "SideNeck_2Bust2SmallOfBackLevelLeftFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelLeftFront'],
                                "SideNeck_2Bust2SmallOfBackLevelRightFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelRightFront'],
                                "Center_Trunk_Circumference_Front"=>$sizerows['Center_Trunk_Circumference_Front'],
                                "Center_Trunk_Circumference_Back"=>$sizerows['Center_Trunk_Circumference_Back'],
                                "Center_Trunk_Circumference_Full_Coutoured"=>$sizerows['Center_Trunk_Circumference_Full_Coutoured'],
                                "Center_Trunk_Circumference_Front_Coutoured"=>$sizerows['Center_Trunk_Circumference_Front_Coutoured'],
                                "Center_Trunk_Circumference_Back_Coutoured"=>$sizerows['Center_Trunk_Circumference_Back_Coutoured'],
                                "Center_Trunk_Circumference_Over_Left_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Left_Shoulder'],
                                "Center_Trunk_Circumference_Over_Right_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Right_Shoulder'],
                                "Neck2Shoulderblade_Distance"=>$sizerows['Neck2Shoulderblade_Distance'],
                                "Neck2Shoulderblade_Vertical"=>$sizerows['Neck2Shoulderblade_Vertical'],
                                "Nape_to_Waist_Front"=>$sizerows['Nape_to_Waist_Front'],
                                "Nape_to_Waist_Back"=>$sizerows['Nape_to_Waist_Back'],
                                "rcmtm_Waist_to_Seat2_Front"=>$sizerows['rcmtm_Waist_to_Seat2_Front'],
                                "rcmtm_Waist_to_Seat2_Back"=>$sizerows['rcmtm_Waist_to_Seat2_Back'],
                                "rcmtm_Waist_to_Hips2_Front"=>$sizerows['rcmtm_Waist_to_Hips2_Front'],
                                "rcmtm_Waist_to_Hips2_Back"=>$sizerows['rcmtm_Waist_to_Hips2_Back'],
                                "Back_Side_Waist_to_Under_Buttocks_Right"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Right'],
                                "Back_Side_Waist_to_Under_Buttocks_Left"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Left'],
                                "Back_Side_Waist_to_Crotch_Level_Right"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Right'],
                                "Back_Side_Waist_to_Crotch_Level_Left"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Left'],
                                "Neck2Waist_rcmtm_Front"=>$sizerows['Neck2Waist_rcmtm_Front'],
                                "CrotchLength_rcmtm_Full"=>$sizerows['CrotchLength_rcmtm_Full'],
                                "CrotchLength_rcmtm_Front"=>$sizerows['CrotchLength_rcmtm_Front'],
                                "CrotchLength_rcmtm_Back"=>$sizerows['CrotchLength_rcmtm_Back'],
                                "pants_Waist_rcmtm_Back"=>$sizerows['pants_Waist_rcmtm_Back'],
                                "pants_Waist_rcmtm_Height_Front"=>$sizerows['pants_Waist_rcmtm_Height_Front'],
                                "pants_Waist_rcmtm_Height_Back"=>$sizerows['pants_Waist_rcmtm_Height_Back'],
                                "Outseam_Max"=>$sizerows['Outseam_Max'],
                                "Outseam_rcmtm_Left"=>$sizerows['Outseam_rcmtm_Left'],
                                "Outseam_rcmtm_Right"=>$sizerows['Outseam_rcmtm_Right'],
                                "Outseam_rcmtm_Max"=>$sizerows['Outseam_rcmtm_Max'],
                                "Waist_small_back_middle_Full"=>$sizerows['Waist_small_back_middle_Full'],
                                "Waist_small_back_middle_Front"=>$sizerows['Waist_small_back_middle_Front'],
                                "Waist_small_back_middle_Back"=>$sizerows['Waist_small_back_middle_Back'],
                                "Waist_smal_back_middle_Height_Front"=>$sizerows['Waist_smal_back_middle_Height_Front'],
                                "Waist_smal_back_middle_Height_Back"=>$sizerows['Waist_smal_back_middle_Height_Back'],
                                "Waist_smal_back_middle_Height_Left"=>$sizerows['Waist_smal_back_middle_Height_Left'],
                                "Waist_smal_back_middle_Height_Right"=>$sizerows['Waist_smal_back_middle_Height_Right'],
                                "Neck2Waist_middle_horizontal_line_Front"=>$sizerows['Neck2Waist_middle_horizontal_line_Front'],
                                "Neck2Waist_middle_horizontal_line_Back"=>$sizerows['Neck2Waist_middle_horizontal_line_Back'],
                                "Stomach_belt_to_measure_Full"=>$sizerows['Stomach_belt_to_measure_Full'],
                                "Stomach_belt_to_measure_Height_Front"=>$sizerows['Stomach_belt_to_measure_Height_Front'],
                                "Stomach_belt_to_measure_Height_Back"=>$sizerows['Stomach_belt_to_measure_Height_Back'],
                                "Schoulder_overarm_Height"=>$sizerows['Schoulder_overarm_Height'],
                                "waist_belly_botton_Full"=>$sizerows['waist_belly_botton_Full'],
                                "waist_belly_botton_Height_Front"=>$sizerows['waist_belly_botton_Height_Front'],
                                "waist_belly_botton_Height_Back"=>$sizerows['waist_belly_botton_Height_Back']
                            );
                            $this->link->response['sizeData_in'] = $sizeData_in;
                            $this->link->response['sizeData_cm'] = $sizeData_cm;
                        }else{
                            $this->link->response[STATUS] = Error;
                            $this->link->response[MESSAGE] = "No Twin Found";
                        }
                    }else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else
                {
                    $query = "select * from size_data where Height>='$min_height' and Height<='$max_height' and 
                    Weight_in_kg>='$min_weight' and Weight_in_kg<='$max_weight' and Neck_Width>='$min_neck' and 
                    Neck_Width<='$max_neck'";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $num = mysqli_num_rows($result);
                        if ($num > 0) {
                            $row = mysqli_fetch_assoc($result);
                            $name = $row['Name'];
                            $sizeQuery = "select * from size_datav3 where Name = '$name'";
                            $sizeresult = mysqli_query($link,$sizeQuery);
                            if($sizeresult){
                                $numsize = mysqli_num_rows($sizeresult);
                                if($numsize>0){
                                    $this->link->response[STATUS] = Success;
                                    $this->link->response[MESSAGE] = "Data Found";
                                    $sizerows = mysqli_fetch_assoc($sizeresult);
                                    $userResponse = $this->getParticularUserData($user_id);
                                    $userData = $userResponse['userData'];
                                    $sizeData_in = array(
                                        "Unit_Type" => "Inches",
                                        "Gender" => $userData['gender'],
                                        "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                                        "Size" =>$userData['size'],
                                        "Waist" =>$sizerows['Waist']/2.54,//conversion in inch;
                                        "Neck_Width" =>$userData['neck'],//already in inch
                                        "Race" =>$userData['race'],
                                        "Weight_in_kg" =>$userData['weight'],
                                        "Name"=>$sizerows['Name'],
                                        "Height"=>$sizerows['Height']*0.393701,
                                        "Neck"=>$sizerows['Neck']*0.393701,
                                        "t2_Chest"=>$sizerows['2_Chest']*0.393701,
                                        "t3_Stomach"=>$sizerows['3_Stomach']*0.393701,
                                        "t4_Seat"=>$sizerows['4_Seat']*0.393701,
                                        "Stomach"=>$sizerows['Stomach']*0.393701,
                                        "Seat"=>$sizerows['Seat']*0.393701,
                                        "Bicep"=>$sizerows['Bicep']*0.393701,
                                        "Back_Shoulder_Width"=>$sizerows['Back_Shoulder_Width']*0.393701,
                                        "Front_Shoulder_Width"=>$sizerows['Front_Shoulder_Width']*0.393701,
                                        //"Shoulder"=>$sizerows['Shoulder']*0.393701,
                                        "Sleeve_Length"=>$sizerows['Sleeve_Length']*0.393701,
                                        //"Shoulder_front"=>$sizerows['Shoulder_front']*0.393701,
                                        "Nape_to_Waist"=>$sizerows['Nape_to_Waist']*0.393701,
                                        "Back_Jacket_Length"=>$sizerows['Back_Jacket_Length']*0.393701,
                                        "Front_Waist_Length"=>$sizerows['Front_Waist_Length']*0.393701,
                                        "Thigh"=>$sizerows['Thigh']*0.393701,
                                        "U_rise"=>$sizerows['U_rise']*0.393701,
                                        "Back_Waist_Height"=>$sizerows['Back_Waist_Height']*0.393701,
                                        "Front_Waist_Height"=>$sizerows['Front_Waist_Height']*0.393701,
                                        "Pant_Length"=>$sizerows['Pant_Length']*0.393701,
                                        "Wrist"=>$sizerows['Wrist']*0.393701,
                                        "Calf"=>$sizerows['Calf']*0.393701,
                                        "Neck_finished_measurements_regular"=>$sizerows['Neck_finished_measurements_regular']*0.393701,
                                        "Sleeve_L_finished_measurements_regular"=>$sizerows['Sleeve_L_finished_measurements_regular']*0.393701,
                                        "Biceps_Shirt_finished_measurements_regular"=>$sizerows['Biceps_Shirt_finished_measurements_regular']*0.393701,
                                        "Shoulders_finished_measurements_regular"=>$sizerows['Shoulders_finished_measurements_regular']*0.393701,
                                        "Stomach_finished_measurements_regular"=>$sizerows['Stomach_finished_measurements_regular']*0.393701,
                                        "Sleeve_R_finished_measurements_regular"=>$sizerows['Sleeve_R_finished_measurements_regular']*0.393701,
                                        "Chest_finished_measurements__Regular"=>$sizerows['Chest_finished_measurements__Regular']*0.393701,
                                        "Left_Cuff_finished_measurements_regular"=>$sizerows['Left_Cuff_finished_measurements_regular']*0.393701,
                                        "Back_Lenght_Finished_measurements_regular"=>$sizerows['Back_Lenght_Finished_measurements_regular']*0.393701,
                                        //"Seat_lessthen_2cm_Finished_measurements_regular"=>$sizerows['Seat_lessthen_2cm_Finished_measurements_regular']*0.393701,
                                        "Righ_Cuff_Finished_measurements_regular"=>$sizerows['Righ_Cuff_Finished_measurements_regular']*0.393701,
                                        "Bicept_Jacket_finished_measurements"=>$sizerows['Bicept_Jacket_finished_measurements']*0.393701,
                                        "Sleeve_L_Jacket_Regular"=>$sizerows['Sleeve_L_Jacket_Regular']*0.393701,
                                        "Shoulder_Jacket_Regular"=>$sizerows['Shoulder_Jacket_Regular']*0.393701,
                                        "Stomach_Jacket_Regular"=>$sizerows['Stomach_Jacket_Regular']*0.393701,
                                        "Sleeve_R_Jacket_Regular"=>$sizerows['Sleeve_R_Jacket_Regular']*0.393701,
                                        "Chest_Jacket_Regular"=>$sizerows['Chest_Jacket_Regular']*0.393701,
                                        "Back_Length_Jacket_Regular"=>$sizerows['Back_Length_Jacket_Regular']*0.393701,
                                        "Seat__Jacket_Regular"=>$sizerows['Seat__Jacket_Regular']*0.393701,
                                        "Waist_pants"=>$sizerows['Waist_pants']*0.393701,
                                        "U_rise_finished_regal_fit"=>$sizerows['U_rise_finished_regal_fit']*0.393701,
                                        "Pants_Tight_finished_measurement"=>$sizerows['Pants_Tight_finished_measurement']*0.393701,
                                        "Outseam_L_pants"=>$sizerows['Outseam_L_pants']*0.393701,
                                        "Outseam_R_Pants"=>$sizerows['Outseam_R_Pants']*0.393701,
                                        "Knee_Pants"=>$sizerows['Knee_Pants']*0.393701,
                                        "Seat_Pants"=>$sizerows['Seat_Pants']*0.393701,
                                        "Bottom_for_trousers"=>$sizerows['Bottom_for_trousers']*0.393701,
                                        "Body_Position_Forward__Backward"=>$sizerows['Body_Position_Forward__Backward']*0.393701,
                                        "Shoulder_position_Forward__Backward"=>$sizerows['Shoulder_position_Forward__Backward']*0.393701,
                                        "Waist_line_High"=>$sizerows['Waist_line_High']*0.393701,
                                        "Waist_line_Low"=>$sizerows['Waist_line_Low']*0.393701,
                                        "shoulder_based_on_Overarm_full_Back"=>$sizerows['shoulder_based_on_Overarm_full_Back']*0.393701,
                                        "shoulder_based_on_Overarm_full_Front"=>$sizerows['shoulder_based_on_Overarm_full_Front']*0.393701,
                                        "Collar"=>$sizerows['Collar']*0.393701,
                                        "Bust"=>$sizerows['Bust']*0.393701,
                                        "Chest"=>$sizerows['Chest']*0.393701,
                                        "Chest_Front"=>$sizerows['Chest_Front']*0.393701,
                                        "Chest_Back"=>$sizerows['Chest_Back']*0.393701,
                                        "Chest_Height"=>$sizerows['Chest_Height']*0.393701,
                                        "Hips"=>$sizerows['Hips']*0.393701,
                                        "NecktoHips"=>$sizerows['NecktoHips']*0.393701,
                                        "Shoulder2Shoulder_45"=>$sizerows['Shoulder2Shoulder_45']*0.393701,
                                        "Neck2Shoulderblade_Horizontal"=>$sizerows['Neck2Shoulderblade_Horizontal']*0.393701,
                                        "Schoulder_overarm_Full"=>$sizerows['Schoulder_overarm_Full']*0.393701,
                                        "Chest2_schoulder_front_Full"=>$sizerows['Chest2_schoulder_front_Full']*0.393701,
                                        "Shoulder2Shoulder_thru_CBN"=>$sizerows['Shoulder2Shoulder_thru_CBN']*0.393701,
                                        "Bust2Bust_Horizontal"=>$sizerows['Bust2Bust_Horizontal']*0.393701,
                                        "underwear_Waist"=>$sizerows['underwear_Waist']*0.393701,
                                        "pants_Waist_rcmtm_Full"=>$sizerows['pants_Waist_rcmtm_Full']*0.393701,
                                        "underwear_Waist_Front"=>$sizerows['underwear_Waist_Front']*0.393701,
                                        "underwear_Waist_Back"=>$sizerows['underwear_Waist_Back']*0.393701,
                                        "underwear_Waist_Height_Front"=>$sizerows['underwear_Waist_Height_Front']*0.393701,
                                        "underwear_Waist_Height_Back"=>$sizerows['underwear_Waist_Height_Back']*0.393701,
                                        "underwear_Waist_Height_Left"=>$sizerows['underwear_Waist_Height_Left']*0.393701,
                                        "underwear_Waist_Height_Right"=>$sizerows['underwear_Waist_Height_Right']*0.393701,
                                        "Hips_Front"=>$sizerows['Hips_Front']*0.393701,
                                        "Hips_Back"=>$sizerows['Hips_Back']*0.393701,
                                        "Outseam_Left"=>$sizerows['Outseam_Left']*0.393701,
                                        "Outseam_Right"=>$sizerows['Outseam_Right']*0.393701,
                                        "Inseam_Left"=>$sizerows['Inseam_Left']*0.393701,
                                        "Inseam_Right"=>$sizerows['Inseam_Right']*0.393701,
                                        "Overarm"=>$sizerows['Overarm']*0.393701,
                                        "Shirt_Sleeve_Left"=>$sizerows['Shirt_Sleeve_Left']*0.393701,
                                        "Shirt_Sleeve_Right"=>$sizerows['Shirt_Sleeve_Right']*0.393701,
                                        "Across_Chest"=>$sizerows['Across_Chest']*0.393701,
                                        "Shoulder_Length_Left"=>$sizerows['Shoulder_Length_Left']*0.393701,
                                        "Shoulder_Length_Right"=>$sizerows['Shoulder_Length_Right']*0.393701,
                                        "Underbust"=>$sizerows['Underbust']*0.393701,
                                        "SideNeck2Bust_Left"=>$sizerows['SideNeck2Bust_Left']*0.393701,
                                        "SideNeck2Bust_Right"=>$sizerows['SideNeck2Bust_Right']*0.393701,
                                        "Neck2Waist_Front"=>$sizerows['Neck2Waist_Front']*0.393701,
                                        "Neck2Waist_Back"=>$sizerows['Neck2Waist_Back']*0.393701,
                                        "Shoulder_Slope_Left"=>$sizerows['Shoulder_Slope_Left']*0.393701,
                                        "Shoulder_Slope_Right"=>$sizerows['Shoulder_Slope_Right']*0.393701,
                                        "Hips_Height"=>$sizerows['Hips_Height']*0.393701,
                                        "Knee_Height"=>$sizerows['Knee_Height']*0.393701,
                                        "CrotchLength"=>$sizerows['CrotchLength']*0.393701,
                                        "CrotchLength_Front"=>$sizerows['CrotchLength_Front']*0.393701,
                                        "CrotchLength_Back"=>$sizerows['CrotchLength_Back']*0.393701,
                                        "ArmLength_Left"=>$sizerows['ArmLength_Left']*0.393701,
                                        "ArmLength_Right"=>$sizerows['ArmLength_Right']*0.393701,
                                        "SlvIn_L"=>$sizerows['SlvIn_L']*0.393701,
                                        "SlvIn_R"=>$sizerows['SlvIn_R']*0.393701,
                                        "Biceps_Left"=>$sizerows['Biceps_Left']*0.393701,
                                        "Biceps_Right"=>$sizerows['Biceps_Right']*0.393701,
                                        "Max_Biceps"=>$sizerows['Max_Biceps']*0.393701,
                                        "Wrist_Left"=>$sizerows['Wrist_Left']*0.393701,
                                        "Wrist_Right"=>$sizerows['Wrist_Right']*0.393701,
                                        "Neck_Front_Height"=>$sizerows['Neck_Front_Height']*0.393701,
                                        "Neck_Back_Height"=>$sizerows['Neck_Back_Height']*0.393701,
                                        "Neck_Left_Height"=>$sizerows['Neck_Left_Height']*0.393701,
                                        "Neck_Right_Height"=>$sizerows['Neck_Right_Height']*0.393701,
                                        "BustToWaist_Left"=>$sizerows['BustToWaist_Left']*0.393701,
                                        "BustToWaist_Right"=>$sizerows['BustToWaist_Right']*0.393701,
                                        "Shoulder2Shoulder_Horiz"=>$sizerows['Shoulder2Shoulder_Horiz']*0.393701,
                                        "Coat_Outsleeve_Left"=>$sizerows['Coat_Outsleeve_Left']*0.393701,
                                        "Coat_Outsleeve_Right"=>$sizerows['Coat_Outsleeve_Right']*0.393701,
                                        "Stomach_Height"=>$sizerows['Stomach_Height']*0.393701,
                                        "Abdomen"=>$sizerows['Abdomen']*0.393701,
                                        "Abdomen_Height"=>$sizerows['Abdomen_Height']*0.393701,
                                        "Shoulder_Height_Left"=>$sizerows['Shoulder_Height_Left']*0.393701,
                                        "Shoulder_Height_Right"=>$sizerows['Shoulder_Height_Right']*0.393701,
                                        "Armpit_Avg_Height"=>$sizerows['Armpit_Avg_Height']*0.393701,
                                        "Overarm_50"=>$sizerows['Overarm_50']*0.393701,
                                        "Overarm_70"=>$sizerows['Overarm_70']*0.393701,
                                        "Thigh_Length_Left"=>$sizerows['Thigh_Length_Left']*0.393701,
                                        "Thigh_Length_Right"=>$sizerows['Thigh_Length_Right']*0.393701,
                                        "Thigh_Max"=>$sizerows['Thigh_Max']*0.393701,
                                        "Knee_Left"=>$sizerows['Knee_Left']*0.393701,
                                        "Knee_Right"=>$sizerows['Knee_Right']*0.393701,
                                        "Ankle_Girth_Left"=>$sizerows['Ankle_Girth_Left']*0.393701,
                                        "Ankle_Girth_Right"=>$sizerows['Ankle_Girth_Right']*0.393701,
                                        "Foot_Width_Left"=>$sizerows['Foot_Width_Left']*0.393701,
                                        "Foot_Width_Right"=>$sizerows['Foot_Width_Right']*0.393701,
                                        "Calf_Left"=>$sizerows['Calf_Left']*0.393701,
                                        "Calf_Right"=>$sizerows['Calf_Right']*0.393701,
                                        "Thigh_Left"=>$sizerows['Thigh_Left']*0.393701,
                                        "Thigh_Right"=>$sizerows['Thigh_Right']*0.393701,
                                        "Seat_Height"=>$sizerows['Seat_Height']*0.393701,
                                        "Seat_Width"=>$sizerows['Seat_Width']*0.393701,
                                        "Seat_Back_Angle"=>$sizerows['Seat_Back_Angle']*0.393701,
                                        "Waist_to_Seat_Back"=>$sizerows['Waist_to_Seat_Back']*0.393701,
                                        "Waist_to_Hips_Back"=>$sizerows['Waist_to_Hips_Back']*0.393701,
                                        "Bulk_Volume"=>$sizerows['Bulk_Volume']*0.393701,
                                        "high_hips_height"=>$sizerows['high_hips_height']*0.393701,
                                        "high_hips_Full"=>$sizerows['high_hips_Full']*0.393701,
                                        "Upper_Hip_Girth_Height"=>$sizerows['Upper_Hip_Girth_Height']*0.393701,
                                        "Upper_Hips_Girth_Full"=>$sizerows['Upper_Hips_Girth_Full']*0.393701,
                                        "Neck_to_High_Hips"=>$sizerows['Neck_to_High_Hips']*0.393701,
                                        "Armscye_Circumference_Left"=>$sizerows['Armscye_Circumference_Left']*0.393701,
                                        "Armscye_Circumference_Right"=>$sizerows['Armscye_Circumference_Right']*0.393701,
                                        "Armscye_Width_Left_Tape"=>$sizerows['Armscye_Width_Left_Tape']*0.393701,
                                        "Armscye_Width_Right_Tape"=>$sizerows['Armscye_Width_Right_Tape']*0.393701,
                                        "Center_Trunk_Circumference_Full"=>$sizerows['Center_Trunk_Circumference_Full']*0.393701,
                                        "Shoulder_2_Shoulder_Caliper_straight_line"=>$sizerows['Shoulder_2_Shoulder_Caliper_straight_line']*0.393701,
                                        "Shoulder_2_Shoulder_Shortest_Path"=>$sizerows['Shoulder_2_Shoulder_Shortest_Path']*0.393701,
                                        "LeftBkWaist2MidShldr"=>$sizerows['LeftBkWaist2MidShldr']*0.393701,
                                        "LeftMidShldr2Bust"=>$sizerows['LeftMidShldr2Bust']*0.393701,
                                        "LeftBustPt2underbustPt"=>$sizerows['LeftBustPt2underbustPt']*0.393701,
                                        "LeftUnderbust2FrontWaist"=>$sizerows['LeftUnderbust2FrontWaist']*0.393701,
                                        "LeftBackWaist2Shoulder"=>$sizerows['LeftBackWaist2Shoulder']*0.393701,
                                        "LeftShoulder2FrontWaist"=>$sizerows['LeftShoulder2FrontWaist']*0.393701,
                                        "LeftBackWaist2SideNeck"=>$sizerows['LeftBackWaist2SideNeck']*0.393701,
                                        "LeftSideNeck2FrontWaist"=>$sizerows['LeftSideNeck2FrontWaist']*0.393701,
                                        "CrownHeight_Left"=>$sizerows['CrownHeight_Left']*0.393701,
                                        "CrownHeight_Right"=>$sizerows['CrownHeight_Right']*0.393701,
                                        "Across_Chest_Normal"=>$sizerows['Across_Chest_Normal']*0.393701,
                                        "Across_Back_Caliper"=>$sizerows['Across_Back_Caliper']*0.393701,
                                        "Across_Chest_Caliper"=>$sizerows['Across_Chest_Caliper']*0.393701,
                                        "SideNeck_2ArmscyeLevelLeftFront"=>$sizerows['SideNeck_2ArmscyeLevelLeftFront']*0.393701,
                                        "SideNeck_2ArmscyeLevelRightFront"=>$sizerows['SideNeck_2ArmscyeLevelRightFront']*0.393701,
                                        "SideNeck_2ArmscyeLevelLeftBack"=>$sizerows['SideNeck_2ArmscyeLevelLeftBack']*0.393701,
                                        "SideNeck_2ArmscyeLevelRightBack"=>$sizerows['SideNeck_2ArmscyeLevelRightBack']*0.393701,
                                        "SideNeck_2Bust2SmallOfBackLevelLeftFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelLeftFront']*0.393701,
                                        "SideNeck_2Bust2SmallOfBackLevelRightFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelRightFront']*0.393701,
                                        "Center_Trunk_Circumference_Front"=>$sizerows['Center_Trunk_Circumference_Front']*0.393701,
                                        "Center_Trunk_Circumference_Back"=>$sizerows['Center_Trunk_Circumference_Back']*0.393701,
                                        "Center_Trunk_Circumference_Full_Coutoured"=>$sizerows['Center_Trunk_Circumference_Full_Coutoured']*0.393701,
                                        "Center_Trunk_Circumference_Front_Coutoured"=>$sizerows['Center_Trunk_Circumference_Front_Coutoured']*0.393701,
                                        "Center_Trunk_Circumference_Back_Coutoured"=>$sizerows['Center_Trunk_Circumference_Back_Coutoured']*0.393701,
                                        "Center_Trunk_Circumference_Over_Left_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Left_Shoulder']*0.393701,
                                        "Center_Trunk_Circumference_Over_Right_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Right_Shoulder']*0.393701,
                                        "Neck2Shoulderblade_Distance"=>$sizerows['Neck2Shoulderblade_Distance']*0.393701,
                                        "Neck2Shoulderblade_Vertical"=>$sizerows['Neck2Shoulderblade_Vertical']*0.393701,
                                        "Nape_to_Waist_Front"=>$sizerows['Nape_to_Waist_Front']*0.393701,
                                        "Nape_to_Waist_Back"=>$sizerows['Nape_to_Waist_Back']*0.393701,
                                        "rcmtm_Waist_to_Seat2_Front"=>$sizerows['rcmtm_Waist_to_Seat2_Front']*0.393701,
                                        "rcmtm_Waist_to_Seat2_Back"=>$sizerows['rcmtm_Waist_to_Seat2_Back']*0.393701,
                                        "rcmtm_Waist_to_Hips2_Front"=>$sizerows['rcmtm_Waist_to_Hips2_Front']*0.393701,
                                        "rcmtm_Waist_to_Hips2_Back"=>$sizerows['rcmtm_Waist_to_Hips2_Back']*0.393701,
                                        "Back_Side_Waist_to_Under_Buttocks_Right"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Right']*0.393701,
                                        "Back_Side_Waist_to_Under_Buttocks_Left"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Left']*0.393701,
                                        "Back_Side_Waist_to_Crotch_Level_Right"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Right']*0.393701,
                                        "Back_Side_Waist_to_Crotch_Level_Left"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Left']*0.393701,
                                        "Neck2Waist_rcmtm_Front"=>$sizerows['Neck2Waist_rcmtm_Front']*0.393701,
                                        "CrotchLength_rcmtm_Full"=>$sizerows['CrotchLength_rcmtm_Full']*0.393701,
                                        "CrotchLength_rcmtm_Front"=>$sizerows['CrotchLength_rcmtm_Front']*0.393701,
                                        "CrotchLength_rcmtm_Back"=>$sizerows['CrotchLength_rcmtm_Back']*0.393701,
                                        "pants_Waist_rcmtm_Back"=>$sizerows['pants_Waist_rcmtm_Back']*0.393701,
                                        "pants_Waist_rcmtm_Height_Front"=>$sizerows['pants_Waist_rcmtm_Height_Front']*0.393701,
                                        "pants_Waist_rcmtm_Height_Back"=>$sizerows['pants_Waist_rcmtm_Height_Back']*0.393701,
                                        "Outseam_Max"=>$sizerows['Outseam_Max']*0.393701,
                                        "Outseam_rcmtm_Left"=>$sizerows['Outseam_rcmtm_Left']*0.393701,
                                        "Outseam_rcmtm_Right"=>$sizerows['Outseam_rcmtm_Right']*0.393701,
                                        "Outseam_rcmtm_Max"=>$sizerows['Outseam_rcmtm_Max']*0.393701,
                                        "Waist_small_back_middle_Full"=>$sizerows['Waist_small_back_middle_Full']*0.393701,
                                        "Waist_small_back_middle_Front"=>$sizerows['Waist_small_back_middle_Front']*0.393701,
                                        "Waist_small_back_middle_Back"=>$sizerows['Waist_small_back_middle_Back']*0.393701,
                                        "Waist_smal_back_middle_Height_Front"=>$sizerows['Waist_smal_back_middle_Height_Front']*0.393701,
                                        "Waist_smal_back_middle_Height_Back"=>$sizerows['Waist_smal_back_middle_Height_Back']*0.393701,
                                        "Waist_smal_back_middle_Height_Left"=>$sizerows['Waist_smal_back_middle_Height_Left']*0.393701,
                                        "Waist_smal_back_middle_Height_Right"=>$sizerows['Waist_smal_back_middle_Height_Right']*0.393701,
                                        "Neck2Waist_middle_horizontal_line_Front"=>$sizerows['Neck2Waist_middle_horizontal_line_Front']*0.393701,
                                        "Neck2Waist_middle_horizontal_line_Back"=>$sizerows['Neck2Waist_middle_horizontal_line_Back']*0.393701,
                                        "Stomach_belt_to_measure_Full"=>$sizerows['Stomach_belt_to_measure_Full']*0.393701,
                                        "Stomach_belt_to_measure_Height_Front"=>$sizerows['Stomach_belt_to_measure_Height_Front']*0.393701,
                                        "Stomach_belt_to_measure_Height_Back"=>$sizerows['Stomach_belt_to_measure_Height_Back']*0.393701,
                                        "Schoulder_overarm_Height"=>$sizerows['Schoulder_overarm_Height']*0.393701,
                                        "waist_belly_botton_Full"=>$sizerows['waist_belly_botton_Full']*0.393701,
                                        "waist_belly_botton_Height_Front"=>$sizerows['waist_belly_botton_Height_Front']*0.393701,
                                        "waist_belly_botton_Height_Back"=>$sizerows['waist_belly_botton_Height_Back']*0.393701
                                    );
                                    $sizeData_cm = array(
                                        "Unit_Type" => "Centimeters",
                                        "Gender" => $userData['gender'],
                                        "AgeRange" => ($userData['age'] - 3) . "-" . ($userData['age'] + 3),
                                        "Size" =>$userData['size'],
                                        "Waist" =>$sizerows['Waist'],
                                        "Race" =>$userData['race'],
                                        "Neck_Width" =>$userData['neck']*2.54,
                                        "Weight_in_kg" =>$userData['weight'],
                                        "Name"=>$sizerows['Name'],
                                        "Height"=>$sizerows['Height'],
                                        "Neck"=>$sizerows['Neck'],
                                        "t2_Chest"=>$sizerows['2_Chest'],
                                        "t3_Stomach"=>$sizerows['3_Stomach'],
                                        "t4_Seat"=>$sizerows['4_Seat'],
                                        "Stomach"=>$sizerows['Stomach'],
                                        "Seat"=>$sizerows['Seat'],
                                        "Bicep"=>$sizerows['Bicep'],
                                        "Back_Shoulder_Width"=>$sizerows['Back_Shoulder_Width'],
                                        "Front_Shoulder_Width"=>$sizerows['Front_Shoulder_Width'],
                                        //"Shoulder"=>$sizerows['Shoulder'],
                                        "Sleeve_Length"=>$sizerows['Sleeve_Length'],
                                        //"Shoulder_front"=>$sizerows['Shoulder_front'],
                                        "Nape_to_Waist"=>$sizerows['Nape_to_Waist'],
                                        "Back_Jacket_Length"=>$sizerows['Back_Jacket_Length'],
                                        "Front_Waist_Length"=>$sizerows['Front_Waist_Length'],
                                        "Thigh"=>$sizerows['Thigh'],
                                        "U_rise"=>$sizerows['U_rise'],
                                        "Back_Waist_Height"=>$sizerows['Back_Waist_Height'],
                                        "Front_Waist_Height"=>$sizerows['Front_Waist_Height'],
                                        "Pant_Length"=>$sizerows['Pant_Length'],
                                        "Wrist"=>$sizerows['Wrist'],
                                        "Calf"=>$sizerows['Calf'],
                                        "Neck_finished_measurements_regular"=>$sizerows['Neck_finished_measurements_regular'],
                                        "Sleeve_L_finished_measurements_regular"=>$sizerows['Sleeve_L_finished_measurements_regular'],
                                        "Biceps_Shirt_finished_measurements_regular"=>$sizerows['Biceps_Shirt_finished_measurements_regular'],
                                        "Shoulders_finished_measurements_regular"=>$sizerows['Shoulders_finished_measurements_regular'],
                                        "Stomach_finished_measurements_regular"=>$sizerows['Stomach_finished_measurements_regular'],
                                        "Sleeve_R_finished_measurements_regular"=>$sizerows['Sleeve_R_finished_measurements_regular'],
                                        "Chest_finished_measurements__Regular"=>$sizerows['Chest_finished_measurements__Regular'],
                                        "Left_Cuff_finished_measurements_regular"=>$sizerows['Left_Cuff_finished_measurements_regular'],
                                        "Back_Lenght_Finished_measurements_regular"=>$sizerows['Back_Lenght_Finished_measurements_regular'],
                                        //"Seat_lessthen_2cm_Finished_measurements_regular"=>$sizerows['Seat_lessthen_2cm_Finished_measurements_regular'],
                                        "Righ_Cuff_Finished_measurements_regular"=>$sizerows['Righ_Cuff_Finished_measurements_regular'],
                                        "Bicept_Jacket_finished_measurements"=>$sizerows['Bicept_Jacket_finished_measurements'],
                                        "Sleeve_L_Jacket_Regular"=>$sizerows['Sleeve_L_Jacket_Regular'],
                                        "Shoulder_Jacket_Regular"=>$sizerows['Shoulder_Jacket_Regular'],
                                        "Stomach_Jacket_Regular"=>$sizerows['Stomach_Jacket_Regular'],
                                        "Sleeve_R_Jacket_Regular"=>$sizerows['Sleeve_R_Jacket_Regular'],
                                        "Chest_Jacket_Regular"=>$sizerows['Chest_Jacket_Regular'],
                                        "Back_Length_Jacket_Regular"=>$sizerows['Back_Length_Jacket_Regular'],
                                        "Seat__Jacket_Regular"=>$sizerows['Seat__Jacket_Regular'],
                                        "Waist_pants"=>$sizerows['Waist_pants'],
                                        "U_rise_finished_regal_fit"=>$sizerows['U_rise_finished_regal_fit'],
                                        "Pants_Tight_finished_measurement"=>$sizerows['Pants_Tight_finished_measurement'],
                                        "Outseam_L_pants"=>$sizerows['Outseam_L_pants'],
                                        "Outseam_R_Pants"=>$sizerows['Outseam_R_Pants'],
                                        "Knee_Pants"=>$sizerows['Knee_Pants'],
                                        "Seat_Pants"=>$sizerows['Seat_Pants'],
                                        "Bottom_for_trousers"=>$sizerows['Bottom_for_trousers'],
                                        "Body_Position_Forward__Backward"=>$sizerows['Body_Position_Forward__Backward'],
                                        "Shoulder_position_Forward__Backward"=>$sizerows['Shoulder_position_Forward__Backward'],
                                        "Waist_line_High"=>$sizerows['Waist_line_High'],
                                        "Waist_line_Low"=>$sizerows['Waist_line_Low'],
                                        "shoulder_based_on_Overarm_full_Back"=>$sizerows['shoulder_based_on_Overarm_full_Back'],
                                        "shoulder_based_on_Overarm_full_Front"=>$sizerows['shoulder_based_on_Overarm_full_Front'],
                                        "Collar"=>$sizerows['Collar'],
                                        "Bust"=>$sizerows['Bust'],
                                        "Chest"=>$sizerows['Chest'],
                                        "Chest_Front"=>$sizerows['Chest_Front'],
                                        "Chest_Back"=>$sizerows['Chest_Back'],
                                        "Chest_Height"=>$sizerows['Chest_Height'],
                                        "Hips"=>$sizerows['Hips'],
                                        "NecktoHips"=>$sizerows['NecktoHips'],
                                        "Shoulder2Shoulder_45"=>$sizerows['Shoulder2Shoulder_45'],
                                        "Neck2Shoulderblade_Horizontal"=>$sizerows['Neck2Shoulderblade_Horizontal'],
                                        "Schoulder_overarm_Full"=>$sizerows['Schoulder_overarm_Full'],
                                        "Chest2_schoulder_front_Full"=>$sizerows['Chest2_schoulder_front_Full'],
                                        "Shoulder2Shoulder_thru_CBN"=>$sizerows['Shoulder2Shoulder_thru_CBN'],
                                        "Bust2Bust_Horizontal"=>$sizerows['Bust2Bust_Horizontal'],
                                        "underwear_Waist"=>$sizerows['underwear_Waist'],
                                        "pants_Waist_rcmtm_Full"=>$sizerows['pants_Waist_rcmtm_Full'],
                                        "underwear_Waist_Front"=>$sizerows['underwear_Waist_Front'],
                                        "underwear_Waist_Back"=>$sizerows['underwear_Waist_Back'],
                                        "underwear_Waist_Height_Front"=>$sizerows['underwear_Waist_Height_Front'],
                                        "underwear_Waist_Height_Back"=>$sizerows['underwear_Waist_Height_Back'],
                                        "underwear_Waist_Height_Left"=>$sizerows['underwear_Waist_Height_Left'],
                                        "underwear_Waist_Height_Right"=>$sizerows['underwear_Waist_Height_Right'],
                                        "Hips_Front"=>$sizerows['Hips_Front'],
                                        "Hips_Back"=>$sizerows['Hips_Back'],
                                        "Outseam_Left"=>$sizerows['Outseam_Left'],
                                        "Outseam_Right"=>$sizerows['Outseam_Right'],
                                        "Inseam_Left"=>$sizerows['Inseam_Left'],
                                        "Inseam_Right"=>$sizerows['Inseam_Right'],
                                        "Overarm"=>$sizerows['Overarm'],
                                        "Shirt_Sleeve_Left"=>$sizerows['Shirt_Sleeve_Left'],
                                        "Shirt_Sleeve_Right"=>$sizerows['Shirt_Sleeve_Right'],
                                        "Across_Chest"=>$sizerows['Across_Chest'],
                                        "Shoulder_Length_Left"=>$sizerows['Shoulder_Length_Left'],
                                        "Shoulder_Length_Right"=>$sizerows['Shoulder_Length_Right'],
                                        "Underbust"=>$sizerows['Underbust'],
                                        "SideNeck2Bust_Left"=>$sizerows['SideNeck2Bust_Left'],
                                        "SideNeck2Bust_Right"=>$sizerows['SideNeck2Bust_Right'],
                                        "Neck2Waist_Front"=>$sizerows['Neck2Waist_Front'],
                                        "Neck2Waist_Back"=>$sizerows['Neck2Waist_Back'],
                                        "Shoulder_Slope_Left"=>$sizerows['Shoulder_Slope_Left'],
                                        "Shoulder_Slope_Right"=>$sizerows['Shoulder_Slope_Right'],
                                        "Hips_Height"=>$sizerows['Hips_Height'],
                                        "Knee_Height"=>$sizerows['Knee_Height'],
                                        "CrotchLength"=>$sizerows['CrotchLength'],
                                        "CrotchLength_Front"=>$sizerows['CrotchLength_Front'],
                                        "CrotchLength_Back"=>$sizerows['CrotchLength_Back'],
                                        "ArmLength_Left"=>$sizerows['ArmLength_Left'],
                                        "ArmLength_Right"=>$sizerows['ArmLength_Right'],
                                        "SlvIn_L"=>$sizerows['SlvIn_L'],
                                        "SlvIn_R"=>$sizerows['SlvIn_R'],
                                        "Biceps_Left"=>$sizerows['Biceps_Left'],
                                        "Biceps_Right"=>$sizerows['Biceps_Right'],
                                        "Max_Biceps"=>$sizerows['Max_Biceps'],
                                        "Wrist_Left"=>$sizerows['Wrist_Left'],
                                        "Wrist_Right"=>$sizerows['Wrist_Right'],
                                        "Neck_Front_Height"=>$sizerows['Neck_Front_Height'],
                                        "Neck_Back_Height"=>$sizerows['Neck_Back_Height'],
                                        "Neck_Left_Height"=>$sizerows['Neck_Left_Height'],
                                        "Neck_Right_Height"=>$sizerows['Neck_Right_Height'],
                                        "BustToWaist_Left"=>$sizerows['BustToWaist_Left'],
                                        "BustToWaist_Right"=>$sizerows['BustToWaist_Right'],
                                        "Shoulder2Shoulder_Horiz"=>$sizerows['Shoulder2Shoulder_Horiz'],
                                        "Coat_Outsleeve_Left"=>$sizerows['Coat_Outsleeve_Left'],
                                        "Coat_Outsleeve_Right"=>$sizerows['Coat_Outsleeve_Right'],
                                        "Stomach_Height"=>$sizerows['Stomach_Height'],
                                        "Abdomen"=>$sizerows['Abdomen'],
                                        "Abdomen_Height"=>$sizerows['Abdomen_Height'],
                                        "Shoulder_Height_Left"=>$sizerows['Shoulder_Height_Left'],
                                        "Shoulder_Height_Right"=>$sizerows['Shoulder_Height_Right'],
                                        "Armpit_Avg_Height"=>$sizerows['Armpit_Avg_Height'],
                                        "Overarm_50"=>$sizerows['Overarm_50'],
                                        "Overarm_70"=>$sizerows['Overarm_70'],
                                        "Thigh_Length_Left"=>$sizerows['Thigh_Length_Left'],
                                        "Thigh_Length_Right"=>$sizerows['Thigh_Length_Right'],
                                        "Thigh_Max"=>$sizerows['Thigh_Max'],
                                        "Knee_Left"=>$sizerows['Knee_Left'],
                                        "Knee_Right"=>$sizerows['Knee_Right'],
                                        "Ankle_Girth_Left"=>$sizerows['Ankle_Girth_Left'],
                                        "Ankle_Girth_Right"=>$sizerows['Ankle_Girth_Right'],
                                        "Foot_Width_Left"=>$sizerows['Foot_Width_Left'],
                                        "Foot_Width_Right"=>$sizerows['Foot_Width_Right'],
                                        "Calf_Left"=>$sizerows['Calf_Left'],
                                        "Calf_Right"=>$sizerows['Calf_Right'],
                                        "Thigh_Left"=>$sizerows['Thigh_Left'],
                                        "Thigh_Right"=>$sizerows['Thigh_Right'],
                                        "Seat_Height"=>$sizerows['Seat_Height'],
                                        "Seat_Width"=>$sizerows['Seat_Width'],
                                        "Seat_Back_Angle"=>$sizerows['Seat_Back_Angle'],
                                        "Waist_to_Seat_Back"=>$sizerows['Waist_to_Seat_Back'],
                                        "Waist_to_Hips_Back"=>$sizerows['Waist_to_Hips_Back'],
                                        "Bulk_Volume"=>$sizerows['Bulk_Volume'],
                                        "high_hips_height"=>$sizerows['high_hips_height'],
                                        "high_hips_Full"=>$sizerows['high_hips_Full'],
                                        "Upper_Hip_Girth_Height"=>$sizerows['Upper_Hip_Girth_Height'],
                                        "Upper_Hips_Girth_Full"=>$sizerows['Upper_Hips_Girth_Full'],
                                        "Neck_to_High_Hips"=>$sizerows['Neck_to_High_Hips'],
                                        "Armscye_Circumference_Left"=>$sizerows['Armscye_Circumference_Left'],
                                        "Armscye_Circumference_Right"=>$sizerows['Armscye_Circumference_Right'],
                                        "Armscye_Width_Left_Tape"=>$sizerows['Armscye_Width_Left_Tape'],
                                        "Armscye_Width_Right_Tape"=>$sizerows['Armscye_Width_Right_Tape'],
                                        "Center_Trunk_Circumference_Full"=>$sizerows['Center_Trunk_Circumference_Full'],
                                        "Shoulder_2_Shoulder_Caliper_straight_line"=>$sizerows['Shoulder_2_Shoulder_Caliper_straight_line'],
                                        "Shoulder_2_Shoulder_Shortest_Path"=>$sizerows['Shoulder_2_Shoulder_Shortest_Path'],
                                        "LeftBkWaist2MidShldr"=>$sizerows['LeftBkWaist2MidShldr'],
                                        "LeftMidShldr2Bust"=>$sizerows['LeftMidShldr2Bust'],
                                        "LeftBustPt2underbustPt"=>$sizerows['LeftBustPt2underbustPt'],
                                        "LeftUnderbust2FrontWaist"=>$sizerows['LeftUnderbust2FrontWaist'],
                                        "LeftBackWaist2Shoulder"=>$sizerows['LeftBackWaist2Shoulder'],
                                        "LeftShoulder2FrontWaist"=>$sizerows['LeftShoulder2FrontWaist'],
                                        "LeftBackWaist2SideNeck"=>$sizerows['LeftBackWaist2SideNeck'],
                                        "LeftSideNeck2FrontWaist"=>$sizerows['LeftSideNeck2FrontWaist'],
                                        "CrownHeight_Left"=>$sizerows['CrownHeight_Left'],
                                        "CrownHeight_Right"=>$sizerows['CrownHeight_Right'],
                                        "Across_Chest_Normal"=>$sizerows['Across_Chest_Normal'],
                                        "Across_Back_Caliper"=>$sizerows['Across_Back_Caliper'],
                                        "Across_Chest_Caliper"=>$sizerows['Across_Chest_Caliper'],
                                        "SideNeck_2ArmscyeLevelLeftFront"=>$sizerows['SideNeck_2ArmscyeLevelLeftFront'],
                                        "SideNeck_2ArmscyeLevelRightFront"=>$sizerows['SideNeck_2ArmscyeLevelRightFront'],
                                        "SideNeck_2ArmscyeLevelLeftBack"=>$sizerows['SideNeck_2ArmscyeLevelLeftBack'],
                                        "SideNeck_2ArmscyeLevelRightBack"=>$sizerows['SideNeck_2ArmscyeLevelRightBack'],
                                        "SideNeck_2Bust2SmallOfBackLevelLeftFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelLeftFront'],
                                        "SideNeck_2Bust2SmallOfBackLevelRightFront"=>$sizerows['SideNeck_2Bust2SmallOfBackLevelRightFront'],
                                        "Center_Trunk_Circumference_Front"=>$sizerows['Center_Trunk_Circumference_Front'],
                                        "Center_Trunk_Circumference_Back"=>$sizerows['Center_Trunk_Circumference_Back'],
                                        "Center_Trunk_Circumference_Full_Coutoured"=>$sizerows['Center_Trunk_Circumference_Full_Coutoured'],
                                        "Center_Trunk_Circumference_Front_Coutoured"=>$sizerows['Center_Trunk_Circumference_Front_Coutoured'],
                                        "Center_Trunk_Circumference_Back_Coutoured"=>$sizerows['Center_Trunk_Circumference_Back_Coutoured'],
                                        "Center_Trunk_Circumference_Over_Left_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Left_Shoulder'],
                                        "Center_Trunk_Circumference_Over_Right_Shoulder"=>$sizerows['Center_Trunk_Circumference_Over_Right_Shoulder'],
                                        "Neck2Shoulderblade_Distance"=>$sizerows['Neck2Shoulderblade_Distance'],
                                        "Neck2Shoulderblade_Vertical"=>$sizerows['Neck2Shoulderblade_Vertical'],
                                        "Nape_to_Waist_Front"=>$sizerows['Nape_to_Waist_Front'],
                                        "Nape_to_Waist_Back"=>$sizerows['Nape_to_Waist_Back'],
                                        "rcmtm_Waist_to_Seat2_Front"=>$sizerows['rcmtm_Waist_to_Seat2_Front'],
                                        "rcmtm_Waist_to_Seat2_Back"=>$sizerows['rcmtm_Waist_to_Seat2_Back'],
                                        "rcmtm_Waist_to_Hips2_Front"=>$sizerows['rcmtm_Waist_to_Hips2_Front'],
                                        "rcmtm_Waist_to_Hips2_Back"=>$sizerows['rcmtm_Waist_to_Hips2_Back'],
                                        "Back_Side_Waist_to_Under_Buttocks_Right"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Right'],
                                        "Back_Side_Waist_to_Under_Buttocks_Left"=>$sizerows['Back_Side_Waist_to_Under_Buttocks_Left'],
                                        "Back_Side_Waist_to_Crotch_Level_Right"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Right'],
                                        "Back_Side_Waist_to_Crotch_Level_Left"=>$sizerows['Back_Side_Waist_to_Crotch_Level_Left'],
                                        "Neck2Waist_rcmtm_Front"=>$sizerows['Neck2Waist_rcmtm_Front'],
                                        "CrotchLength_rcmtm_Full"=>$sizerows['CrotchLength_rcmtm_Full'],
                                        "CrotchLength_rcmtm_Front"=>$sizerows['CrotchLength_rcmtm_Front'],
                                        "CrotchLength_rcmtm_Back"=>$sizerows['CrotchLength_rcmtm_Back'],
                                        "pants_Waist_rcmtm_Back"=>$sizerows['pants_Waist_rcmtm_Back'],
                                        "pants_Waist_rcmtm_Height_Front"=>$sizerows['pants_Waist_rcmtm_Height_Front'],
                                        "pants_Waist_rcmtm_Height_Back"=>$sizerows['pants_Waist_rcmtm_Height_Back'],
                                        "Outseam_Max"=>$sizerows['Outseam_Max'],
                                        "Outseam_rcmtm_Left"=>$sizerows['Outseam_rcmtm_Left'],
                                        "Outseam_rcmtm_Right"=>$sizerows['Outseam_rcmtm_Right'],
                                        "Outseam_rcmtm_Max"=>$sizerows['Outseam_rcmtm_Max'],
                                        "Waist_small_back_middle_Full"=>$sizerows['Waist_small_back_middle_Full'],
                                        "Waist_small_back_middle_Front"=>$sizerows['Waist_small_back_middle_Front'],
                                        "Waist_small_back_middle_Back"=>$sizerows['Waist_small_back_middle_Back'],
                                        "Waist_smal_back_middle_Height_Front"=>$sizerows['Waist_smal_back_middle_Height_Front'],
                                        "Waist_smal_back_middle_Height_Back"=>$sizerows['Waist_smal_back_middle_Height_Back'],
                                        "Waist_smal_back_middle_Height_Left"=>$sizerows['Waist_smal_back_middle_Height_Left'],
                                        "Waist_smal_back_middle_Height_Right"=>$sizerows['Waist_smal_back_middle_Height_Right'],
                                        "Neck2Waist_middle_horizontal_line_Front"=>$sizerows['Neck2Waist_middle_horizontal_line_Front'],
                                        "Neck2Waist_middle_horizontal_line_Back"=>$sizerows['Neck2Waist_middle_horizontal_line_Back'],
                                        "Stomach_belt_to_measure_Full"=>$sizerows['Stomach_belt_to_measure_Full'],
                                        "Stomach_belt_to_measure_Height_Front"=>$sizerows['Stomach_belt_to_measure_Height_Front'],
                                        "Stomach_belt_to_measure_Height_Back"=>$sizerows['Stomach_belt_to_measure_Height_Back'],
                                        "Schoulder_overarm_Height"=>$sizerows['Schoulder_overarm_Height'],
                                        "waist_belly_botton_Full"=>$sizerows['waist_belly_botton_Full'],
                                        "waist_belly_botton_Height_Front"=>$sizerows['waist_belly_botton_Height_Front'],
                                        "waist_belly_botton_Height_Back"=>$sizerows['waist_belly_botton_Height_Back']
                                    );
                                    $this->link->response['sizeData_in'] = $sizeData_in;
                                    $this->link->response['sizeData_cm'] = $sizeData_cm;
                                }else{
                                    $this->link->response[STATUS] = Error;
                                    $this->link->response[MESSAGE] = "No Twin Found";
                                }
                            }else{
                                $this->link->response[STATUS] = Error;
                                $this->link->response[MESSAGE] = $this->link->sqlError();
                            }
                        } else {
                            $this->link->response[STATUS] = Error;
                            $this->link->response[MESSAGE] = "No Twin Found";
                        }
                    }
                    else {
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function resetData($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "update users set race = '',neck='',brand='',size='',waist='' where user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                mysqli_query($link,"delete from style_genie where user_id='$user_id'");
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Data Reset Success";
            }else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function storeDatatoTable($userData)
    {
        $link = $this->link->connect();
        if($link) {
            $email = $userData['email'];
            $query = "select * from users where email = '$email'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                $user_id = $userData['user_id'];
                $name = explode(" ",$userData['name']);
                $fname = $name[0];
                $lname = $name[1];
                $password = $userData['password'];
                $contact = $userData['mobile'];
                $gender = $userData['gender'];
                $dob = $userData['dob'];
                $height_in = explode(" ",$userData['height'])[0];
                $weight = explode(" ",$userData['weight'])[0];
                $user_profile = $userData['profile_pic'];
                $status = '1';
                if ($num > 0) {
                    //update my table data
                    $query = "update users set user_id='$user_id',fname='$fname',lname='$lname',
                    password='$password',contact='$contact',gender='$gender',dob='$dob',height_in='$height_in',
                    weight='$weight',user_profile='$user_profile',status='$status' where email = '$email'";
                } else {
                    //insert new row in my table
                    $query = "insert into users (user_id,fname,lname,password,contact,gender,dob,height_in,weight,
                    user_profile,status,email) values('$user_id','$fname','$lname','$password','$contact','$gender',
                    '$dob','$height_in','$weight','$user_profile','$status','$email')";
                }
                $result = mysqli_query($this->link->Connect(),$query);
                if($result){
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Login Success";
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function updateDimensions($user_id, $height, $weight, $age, $gender, $race, $neck, $brand,$size,$waist)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update users set height_in='$height',weight='$weight',age='$age',
            gender='$gender',race='$race',neck='$neck',brand='$brand',waist = '$waist',size = '$size'
            where user_id = '$user_id'";
            $result = mysqli_query($this->link->Connect(),$query);
            if($result){
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Data Updated Successfully";
            }
            else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function uploadOrd($user_id){
        $link = $this->link->connect();
        if($link) {
            $stat = "";
            $sourcePath = $_FILES['file']['tmp_name'];
            $ordname = urlencode($_POST['ordname']);
            $dated = date("Y-m-d");
            if(!is_dir(getcwd()."/Files/users/user".$user_id)){
                mkdir(getcwd()."/Files/users/user".$user_id);
            }
            $targetPath = $dated.".ord";
            if($ordname == ""){
                if(move_uploaded_file($sourcePath,getcwd()."/Files/users/user".$user_id."/".$targetPath)){
                    $filepath = getcwd()."/Files/users/user".$user_id."/".$targetPath;
                    $myfile = fopen($filepath, "r") or die("Unable to open file!");
                    $file = fread($myfile,filesize($filepath));
                    $show = explode("#DATE=",$file);
                    $date =  explode("MEASURE",$show[1]);
                    $requiredfilename = trim(str_replace(", ","-",$date[0])).".ord";
                    fclose($myfile);
                    rename(getcwd()."/Files/users/user$user_id/$targetPath" ,getcwd()."/Files/users/user$user_id/$requiredfilename");
                    $stat="uploaded";
                }
            }
            else{
                $requiredfilename = $ordname.".ord";
                move_uploaded_file($sourcePath,getcwd()."/Files/users/user".$user_id."/".$requiredfilename);
                $stat="uploaded";
            }
            $timestamp = strtotime($this->CurrentDateTime);
            if($stat == "uploaded"){
                $query = "insert into ord_files (user_id,file_name,added_on) values('$user_id','$requiredfilename',
                '$timestamp')";
                $result = mysqli_query($link,$query);
                if($result){
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "File Uploaded Success";
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Error File Uploading...";
                }
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function isOrdExist($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from ord_files where user_id='$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "File Available";
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Not Any Ord File Found";
                }
            }else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function isStyleGenieUsed($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from style_genie where user_id='$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $rows = mysqli_fetch_assoc($result);
                    if($rows['shirt_que_1'] != "" && $rows['shirt_que_2'] != "" && $rows['shirt_que_3'] != "" &&
                    $rows['shirt_que_4'] != "" && $rows['shirt_que_5'] != "" && $rows['shirt_que_6'] != ""){
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Data Available";
                    }else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = "New in Style Genie";
                    }
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "New in Style Genie";
                }
            }else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getAllMeasurement($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from final_measurements where user_id='$user_id' order by measurement_id DESC";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $measurements = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $measurements[] = $rows;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Measurement Available";
                    $this->link->response['measurements'] = $measurements;
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No measurements saved yet";
                }
            }
            else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getParticularMeasurement($meas_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from final_measurements where measurement_id='$meas_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $measurements = mysqli_fetch_assoc($result);
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Measurement Available";
                    $this->link->response['measurements'] = $measurements;
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Not Any Measurement Found";
                }
            }
            else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function deleteMeasurement($meas_id){
        $link = $this->link->connect();
        if($link) {
            $query = "delete from final_measurements where measurement_id='$meas_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Measurement Available";
            }
            else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getMyFiles($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from ord_files where user_id='$user_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $filesData = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $filesData[] = $rows;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "File Available";
                    $this->link->response['filesData'] = $filesData;
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Not Any Ord File Found";
                }
            }else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function deleteFile($file_id){
        $link = $this->link->connect();
        if($link) {
            $query = "delete from ord_files where _id='$file_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "File Available";
            }else{
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getAllQuesData($userId){
        $link = $this->link->connect();
        if($link) {
            $usersData = array();
            $query = "select * from style_genie where user_id='$userId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData=$rows;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $this->link->response['Count'] = $num;
                    $this->link->response['quesData'] = $usersData;
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function storeTwinData($collar,$chest,$stomach,$seat,$bicep,$back_shoulder,$front_shoulder,$sleeve_length_right,
    $sleeve_length_left,$thigh,$nape_to_waist,$front_waist_length,$wrist,$waist,$calf,$back_waist_height,$front_waist_height,
    $knee,$back_jacket_length,$u_rise,$pant_left_outseam,$pant_right_outseam,$bottom,$user_id,$unit_type,$shoulder_slope_left,
    $shoulder_slope_right){
        $link = $this->link->connect();
        if($link) {
            $usersData = array();
            $query = "select * from style_genie where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    //update twin_geneie_data here//////
                    $tg_query = "update style_genie set collar='$collar',chest='$chest',stomach='$stomach',seat='$seat',
                    bicep='$bicep',back_sholuder='$back_shoulder',front_shoulder='$front_shoulder',sleeve_length_left=
                    '$sleeve_length_left',sleeve_length_right='$sleeve_length_right',thigh='$thigh',nape_to_waist=
                    '$nape_to_waist',front_waist_length='$front_waist_length',wrist='$wrist',waist='$waist',calf='$calf',
                    back_waist_height='$back_waist_height',front_waist_height='$front_waist_height',knee='$knee',
                    back_jacket_length='$back_jacket_length',u_rise='$u_rise',pant_left_outseam='$pant_left_outseam',
                    pant_right_outseam='$pant_right_outseam',bottom='$bottom',unit_type='$unit_type',shoulder_slope_left
                    ='$shoulder_slope_left',shoulder_slope_right='$shoulder_slope_right' where user_id='$user_id'";
                    $tg_result = mysqli_query($link, $tg_query);
                    if ($tg_result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Data Saved Success";
                    }else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else{
                    //insert twin_geneie_data here//////
                    $tg_query = "INSERT INTO style_genie(collar,chest,stomach,seat,bicep,back_sholuder,front_shoulder,
                    sleeve_length_left,sleeve_length_right,thigh,nape_to_waist,front_waist_length,wrist,waist,calf,
                    back_waist_height,front_waist_height,knee,back_jacket_length,u_rise,pant_left_outseam,
                    pant_right_outseam,bottom,user_id,unit_type,shoulder_slope_left,shoulder_slope_right)
                    values('$collar','$chest','$stomach','$seat','$bicep','$back_shoulder','$front_shoulder',
                    '$sleeve_length_left','$sleeve_length_right','$thigh','$nape_to_waist','$front_waist_length',
                    '$wrist','$waist','$calf','$back_waist_height','$front_waist_height','$knee','$back_jacket_length',
                    '$u_rise','$pant_left_outseam','$pant_right_outseam','$bottom','$user_id','$unit_type',
                    '$shoulder_slope_left','$shoulder_slope_right')";
                    $tg_result = mysqli_query($link, $tg_query);
                    if ($tg_result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Data Saved Success";
                    }else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function styleGenieUpdate($first_ques,$second_ques,$third_ques,$forth_ques,$fifth_ques,$sixth_ques,$shirt_que_1,
    $shirt_que_2,$shirt_que_3,$shirt_que_4,$shirt_que_5,$shirt_que_6,$suit_que_1,$suit_que_2,$suit_que_3,$suit_que_4,
    $suit_que_5,$vest_que_1,$pant_que_1,$user_id,$sixth_ques_stoop){
        $link = $this->link->connect();
        if($link) {
            $usersData = array();
            $query = "select * from style_genie where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    //update twin_geneie_data here//////
                    $tg_query = "update style_genie set shoulder_forward='$first_ques',body_forward='$second_ques',
                    shoulder_slope='$third_ques',belly_shape='$forth_ques',style_arms='$fifth_ques',style_seat='$sixth_ques',
                    shirt_que_1='$shirt_que_1',shirt_que_2='$shirt_que_2',shirt_que_3='$shirt_que_3',shirt_que_4='$shirt_que_4',
                    shirt_que_5='$shirt_que_5',shirt_que_6='$shirt_que_6',suit_que_1='$suit_que_1',suit_que_2='$suit_que_2',
                    suit_que_3='$suit_que_3',suit_que_4='$suit_que_4',suit_que_5='$suit_que_5',vest_que_1='$vest_que_1'
                    ,pant_que_1='$pant_que_1',sixth_ques_stoop='$sixth_ques_stoop' where user_id='$user_id'";
                    $tg_result = mysqli_query($link, $tg_query);
                    if ($tg_result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Data Saved Success";
                    }else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else{
                    //insert twin_geneie_data here//////
                    $tg_query = "INSERT INTO style_genie(shoulder_forward,body_forward,shoulder_slope,belly_shape,style_arms,
                    style_seat,shirt_que_1,shirt_que_2,shirt_que_3,shirt_que_4,shirt_que_5,shirt_que_6,suit_que_1,suit_que_2,
                    suit_que_3,suit_que_4,suit_que_5,vest_que_1,pant_que_1,user_id,sixth_ques_stoop) values('$first_ques','$second_ques','$third_ques',
                    '$forth_ques','$fifth_ques','$sixth_ques','$shirt_que_1','$shirt_que_2','$shirt_que_3','$shirt_que_4',
                    '$shirt_que_5','$shirt_que_6','$suit_que_1','$suit_que_2','$suit_que_3','$suit_que_4','$suit_que_5',
                    '$vest_que_1','$pant_que_1','$user_id','$sixth_ques_stoop')";
                    $tg_result = mysqli_query($link, $tg_query);
                    if ($tg_result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Data Saved Success";
                    }else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            } else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getStyleGenieData($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from style_genie where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $rows = mysqli_fetch_assoc($result);
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $this->link->response['userData'] = $rows;
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function queryProcess($garment_type,$body_param,$question_type,$answer){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from allowance_table where garment_type='$garment_type' and body_params = '$body_param' 
            and question_type='$question_type' and answers = '$answer'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $rows = mysqli_fetch_assoc($result);
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $this->link->response['value'] = $rows['value'];
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }

    public function queryProcess2($body_param,$ques_answer){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from allowance_table where body_params = '$body_param'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $ques_answer = explode("@",$ques_answer);
                    $ques1 = explode(":",$ques_answer[0])[0];
                    $ans1 = explode(":",$ques_answer[0])[1];
                    $ques2 = explode(":",$ques_answer[1])[0];
                    $ans2 = explode(":",$ques_answer[1])[1];
                    $ques3 = explode(":",$ques_answer[2])[0];
                    $ans3 = explode(":",$ques_answer[2])[1];
                    $ques4 = explode(":",$ques_answer[3])[0];
                    $ans4 = explode(":",$ques_answer[3])[1];
                    $ques5 = explode(":",$ques_answer[4])[0];
                    $ans5 = explode(":",$ques_answer[4])[1];
                    $ques6 = explode(":",$ques_answer[5])[0];
                    $ans6 = explode(":",$ques_answer[5])[1];
                    $jques1 = explode(":",$ques_answer[6])[0];
                    $jans1 = explode(":",$ques_answer[6])[1];
                    $jques2 = explode(":",$ques_answer[7])[0];
                    $jans2 = explode(":",$ques_answer[7])[1];
                    $jques3 = explode(":",$ques_answer[8])[0];
                    $jans3 = explode(":",$ques_answer[8])[1];
                    $vques1 = explode(":",$ques_answer[9])[0];
                    $vans1 = explode(":",$ques_answer[9])[1];
                    $pques1 = explode(":",$ques_answer[10])[0];
                    $pans1 = explode(":",$ques_answer[10])[1];
                    while($rows = mysqli_fetch_assoc($result)){
                        if($rows['garment_type'] == "shirt" && $rows['question_type'] == $ques1 && $rows['answers'] == $ans1){
                            $ans1 = $rows['value'];
                        }else if($rows['garment_type'] == "shirt" && $rows['question_type'] == $ques2 && $rows['answers'] == $ans2){
                            $ans2 = $rows['value'];
                        }else if($rows['garment_type'] == "shirt" && $rows['question_type'] == $ques3 && $rows['answers'] == $ans3){
                            $ans3 = $rows['value'];
                        }else if($rows['garment_type'] == "shirt" && $rows['question_type'] == $ques4 && $rows['answers'] == $ans4){
                            $ans4 = $rows['value'];
                        }else if($rows['garment_type'] == "shirt" && $rows['question_type'] == $ques5 && $rows['answers'] == $ans5){
                            $ans5 = $rows['value'];
                        }else if($rows['garment_type'] == "shirt" && $rows['question_type'] == $ques6 && $rows['answers'] == $ans6){
                            $ans6 = $rows['value'];
                        }else if($rows['garment_type'] == "jacket" && $rows['question_type'] == $jques1 && $rows['answers'] == $jans1){
                            $jans1 = $rows['value'];
                        }else if($rows['garment_type'] == "jacket" && $rows['question_type'] == $jques2 && $rows['answers'] == $jans2) {
                            $jans2 = $rows['value'];
                        }else if($rows['garment_type'] == "jacket" && $rows['question_type'] == $jques3 && $rows['answers'] == $jans3){
                            $jans3 = $rows['value'];
                        }else if($rows['garment_type'] == "vest" && $rows['question_type'] == $vques1 && $rows['answers'] == $vans1){
                            $vans1 = $rows['value'];
                        }else if($rows['garment_type'] == "pant" && $rows['question_type'] == $pques1 && $rows['answers'] == $pans1){
                            $pans1 = $rows['value'];
                        }
                    }
                    if($body_param == "bicep"){
                        $svalue = $ans2;
                        $jvalue = $jans2;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "wrist"){
                        $svalue = $ans1+$ans5;
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "right_sleeve_length"){
                        $svalue = $ans4;
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "left_sleeve_length"){
                        $svalue = $ans4;
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "collar"){
                        $svalue = (($ans1+$ans2)/2)+$ans3+$ans4+(($ans5+$ans6)/2);
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "chest"){
                        $svalue = $ans1;
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "stomach"){
                        $svalue = (($ans1+$ans2)/2)+$ans3+$ans4+(($ans5+$ans6)/2);
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "seat"){
                        $svalue = (($ans1+$ans2)/2)+$ans3+$ans4+(($ans5+$ans6)/2);
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    elseif($body_param == "back_jacket_length"){
                        $svalue = (($ans1+$ans2)/2)+$ans3+$ans4+(($ans5+$ans6)/2);
                        $jvalue = $jans3;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    else{
                        $svalue = (($ans1+$ans2)/2)+$ans3+$ans4+(($ans5+$ans6)/2);
                        $jvalue = $jans1;
                        $vvalue = $vans1;
                        $pvalue = $pans1;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $this->link->response['svalue'] = $svalue;
                    $this->link->response['jvalue'] = $jvalue;
                    $this->link->response['vvalue'] = $vvalue;
                    $this->link->response['pvalue'] = $pvalue;
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function pantpleat($pant_answer){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from pleat_option where dress_style = '$pant_answer'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $pleatData = mysqli_fetch_assoc($result);
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $this->link->response['pleatData'] = $pleatData;
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function storeFinalMeasurement($shirt_collar,$jacket_collar,$shirt_chest,$jacket_chest,$shirt_stomach,
    $jacket_stomach,$shirt_seat,$jacket_seat,$shirt_bicep,$jacket_bicep,$shirt_back_shoulder,$jacket_back_shoulder,
    $shirt_front_shoulder,$jacket_front_shoulder,$shirt_sleeve_left,$jacket_sleeve_left,$shirt_sleeve_right,
    $jacket_sleeve_right,$shirt_thigh,$jacket_thigh,$shirt_nape_to_waist,$jacket_nape_to_waist,$shirt_front_waist_length,
    $jacket_front_waist_length,$shirt_wrist,$jacket_wrist,$shirt_waist,$jacket_waist,$shirt_calf,$jacket_calf,$shirt_back_waist_height,
    $jacket_back_waist_height,$shirt_front_waist_height,$jacket_front_waist_height,$shirt_knee,$jacket_knee,
    $shirt_back_jacket_length,$jacket_back_jacket_length,$shirt_u_rise,$jacket_u_rise,$shirt_pant_left_outseam,
    $jacket_pant_left_outseam,$shirt_pant_right_outseam,$jacket_pant_right_outseam,$shirt_bottom,$jacket_bottom,
    $meas_name,$user_id,$unit_type,$vest_collar,$pant_collar,$vest_chest,$pant_chest,$vest_stomach,$pant_stomach,
    $vest_seat,$pant_seat,$vest_bicep,$pant_bicep,$vest_back_shoulder,$pant_back_shoulder,$vest_front_shoulder,
    $pant_front_shoulder,$vest_sleeve_left,$pant_sleeve_left,$vest_sleeve_right,$pant_sleeve_right,$vest_thigh,
    $pant_thigh,$vest_nape_to_waist,$pant_nape_to_waist,$vest_front_waist_length,$pant_front_waist_length,$vest_wrist,
    $pant_wrist,$vest_waist,$pant_waist,$vest_calf,$pant_calf,$vest_back_waist_height,$pant_back_waist_height,$vest_front_waist_height,
    $pant_front_waist_height,$vest_knee,$pant_knee,$vest_back_jacket_length,$pant_back_jacket_length,$vest_u_rise,
    $pant_u_rise,$vest_pant_left_outseam,$pant_pant_left_outseam,$vest_pant_right_outseam,$pant_pant_right_outseam,
    $vest_bottom,$pant_bottom,$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from final_measurements where meas_name = '$meas_name' and user_id = '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Measurement Name Already Exist";
                }else{
                    $timestamp = strtotime($this->CurrentDateTime);
                    $query = "insert into final_measurements (shirt_collar,jacket_collar,shirt_chest,jacket_chest,
                    shirt_stomach,jacket_stomach,shirt_seat,jacket_seat,shirt_bicep,jacket_bicep,shirt_back_shoulder,
                    jacket_back_shoulder,shirt_front_shoulder,jacket_front_shoulder,shirt_sleeve_left,jacket_sleeve_left,
                    shirt_sleeve_right,jacket_sleeve_right,shirt_thigh,jacket_thigh,shirt_nape_to_waist,jacket_nape_to_waist,
                    shirt_front_waist_length,jacket_front_waist_length,shirt_wrist,jacket_wrist,shirt_waist,jacket_waist,shirt_calf,jacket_calf,
                    shirt_back_waist_height,jacket_back_waist_height,shirt_front_waist_height,jacket_front_waist_height,
                    shirt_knee,jacket_knee,shirt_back_jacket_length,jacket_back_jacket_length,shirt_u_rise,jacket_u_rise,
                    shirt_pant_left_outseam,jacket_pant_left_outseam,shirt_pant_right_outseam,jacket_pant_right_outseam,
                    shirt_bottom,jacket_bottom,meas_name,user_id,unit_type,added_on,vest_collar,pant_collar,vest_chest,
                    pant_chest,vest_stomach,pant_stomach,vest_seat,pant_seat,vest_bicep,pant_bicep,vest_back_shoulder,
                    pant_back_shoulder,vest_front_shoulder,pant_front_shoulder,vest_sleeve_left,pant_sleeve_left,
                    vest_sleeve_right,pant_sleeve_right,vest_thigh,pant_thigh,vest_nape_to_waist,pant_nape_to_waist,
                    vest_front_waist_length,pant_front_waist_length,vest_wrist,pant_wrist,vest_waist,pant_waist,vest_calf,pant_calf,
                    vest_back_waist_height,pant_back_waist_height,vest_front_waist_height,
                    pant_front_waist_height,vest_knee,pant_knee,vest_back_jacket_length,pant_back_jacket_length,
                    vest_u_rise,pant_u_rise,vest_pant_left_outseam,pant_pant_left_outseam,vest_pant_right_outseam,
                    pant_pant_right_outseam,vest_bottom,pant_bottom,pant_seat1,pant_seat11,pant_seat2,pant_seat3)
                    values ('$shirt_collar','$jacket_collar','$shirt_chest',
                    '$jacket_chest','$shirt_stomach','$jacket_stomach','$shirt_seat','$jacket_seat','$shirt_bicep','$jacket_bicep',
                    '$shirt_back_shoulder','$jacket_back_shoulder','$shirt_front_shoulder','$jacket_front_shoulder',
                    '$shirt_sleeve_left','$jacket_sleeve_left','$shirt_sleeve_right','$jacket_sleeve_right','$shirt_thigh',
                    '$jacket_thigh','$shirt_nape_to_waist','$jacket_nape_to_waist','$shirt_front_waist_length',
                    '$jacket_front_waist_length','$shirt_wrist','$jacket_wrist','$shirt_waist','$jacket_waist','$shirt_calf','$jacket_calf','$shirt_back_waist_height',
                    '$jacket_back_waist_height','$shirt_front_waist_height','$jacket_front_waist_height','$shirt_knee','$jacket_knee',
                    '$shirt_back_jacket_length','$jacket_back_jacket_length','$shirt_u_rise','$jacket_u_rise','$shirt_pant_left_outseam',
                    '$jacket_pant_left_outseam','$shirt_pant_right_outseam','$jacket_pant_right_outseam','$shirt_bottom','$jacket_bottom',
                    '$meas_name','$user_id','$unit_type','$timestamp','$vest_collar','$pant_collar','$vest_chest','$pant_chest',
                    '$vest_stomach','$pant_stomach','$vest_seat','$pant_seat','$vest_bicep','$pant_bicep','$vest_back_shoulder',
                    '$pant_back_shoulder','$vest_front_shoulder','$pant_front_shoulder','$vest_sleeve_left','$pant_sleeve_left',
                    '$vest_sleeve_right','$pant_sleeve_right','$vest_thigh','$pant_thigh','$vest_nape_to_waist','$pant_nape_to_waist',
                    '$vest_front_waist_length','$pant_front_waist_length','$vest_wrist','$pant_wrist','$vest_waist','$pant_waist','$vest_calf','$pant_calf',
                    '$vest_back_waist_height','$pant_back_waist_height','$vest_front_waist_height','$pant_front_waist_height',
                    '$vest_knee','$pant_knee','$vest_back_jacket_length','$pant_back_jacket_length','$vest_u_rise','$pant_u_rise',
                    '$vest_pant_left_outseam','$pant_pant_left_outseam','$vest_pant_right_outseam','$pant_pant_right_outseam',
                    '$vest_bottom','$pant_bottom',$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat)";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Measurement Saved SuccessFully";
                    }
                    else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function updateFinalMeasurement($shirt_collar,$jacket_collar,$shirt_chest,$jacket_chest,$shirt_stomach,
    $jacket_stomach,$shirt_seat,$jacket_seat,$shirt_bicep,$jacket_bicep,$shirt_back_shoulder,$jacket_back_shoulder,
    $shirt_front_shoulder,$jacket_front_shoulder,$shirt_sleeve_left,$jacket_sleeve_left,$shirt_sleeve_right,
    $jacket_sleeve_right,$shirt_thigh,$jacket_thigh,$shirt_nape_to_waist,$jacket_nape_to_waist,$shirt_front_waist_length,
    $jacket_front_waist_length,$shirt_wrist,$jacket_wrist,$shirt_waist,$jacket_waist,$shirt_calf,$jacket_calf,$shirt_back_waist_height,
    $jacket_back_waist_height,$shirt_front_waist_height,$jacket_front_waist_height,$shirt_knee,$jacket_knee,
    $shirt_back_jacket_length,$jacket_back_jacket_length,$shirt_u_rise,$jacket_u_rise,$shirt_pant_left_outseam,
    $jacket_pant_left_outseam,$shirt_pant_right_outseam,$jacket_pant_right_outseam,$shirt_bottom,$jacket_bottom,
    $meas_id,$unit_type,$vest_collar,$pant_collar,$vest_chest,$pant_chest,$vest_stomach,$pant_stomach,
    $vest_seat,$pant_seat,$vest_bicep,$pant_bicep,$vest_back_shoulder,$pant_back_shoulder,$vest_front_shoulder,
    $pant_front_shoulder,$vest_sleeve_left,$pant_sleeve_left,$vest_sleeve_right,$pant_sleeve_right,$vest_thigh,
    $pant_thigh,$vest_nape_to_waist,$pant_nape_to_waist,$vest_front_waist_length,$pant_front_waist_length,$vest_wrist,
    $pant_wrist,$vest_waist,$pant_waist,$vest_calf,$pant_calf,$vest_back_waist_height,$pant_back_waist_height,$vest_front_waist_height,
    $pant_front_waist_height,$vest_knee,$pant_knee,$vest_back_jacket_length,$pant_back_jacket_length,$vest_u_rise,
    $pant_u_rise,$vest_pant_left_outseam,$pant_pant_left_outseam,$vest_pant_right_outseam,$pant_pant_right_outseam,
    $vest_bottom,$pant_bottom/*,$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat*/){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from final_measurements where measurement_id = '$meas_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $timestamp = strtotime($this->CurrentDateTime);
                    $query = "update final_measurements set shirt_collar='$shirt_collar',jacket_collar='$jacket_collar',
                    shirt_chest='$shirt_chest',jacket_chest='$jacket_chest',shirt_stomach='$shirt_stomach',jacket_stomach
                    ='$jacket_stomach',shirt_seat='$shirt_seat',jacket_seat='$jacket_seat',shirt_bicep='$shirt_bicep',
                    jacket_bicep='$jacket_bicep',shirt_back_shoulder='$shirt_back_shoulder',jacket_back_shoulder=
                    '$jacket_back_shoulder',shirt_front_shoulder='$shirt_front_shoulder',jacket_front_shoulder
                    ='$jacket_front_shoulder',shirt_sleeve_left='$shirt_sleeve_left',jacket_sleeve_left='$jacket_sleeve_left',
                    shirt_sleeve_right='$shirt_sleeve_right',jacket_sleeve_right='$jacket_sleeve_right',shirt_thigh
                    ='$shirt_thigh',jacket_thigh='$jacket_thigh',shirt_nape_to_waist='$shirt_nape_to_waist',
                    jacket_nape_to_waist='$jacket_nape_to_waist',shirt_front_waist_length='$shirt_front_waist_length',
                    jacket_front_waist_length='$jacket_front_waist_length',shirt_wrist='$shirt_wrist',jacket_wrist
                    ='$jacket_wrist',shirt_waist='$shirt_waist',jacket_waist='$jacket_waist',shirt_calf='$shirt_calf',jacket_calf='$jacket_calf',shirt_back_waist_height=
                    '$shirt_back_waist_height',jacket_back_waist_height='$jacket_back_waist_height',shirt_front_waist_height
                    ='$shirt_front_waist_height',jacket_front_waist_height='$jacket_front_waist_height',shirt_knee=
                    '$shirt_knee',jacket_knee='$jacket_knee',shirt_back_jacket_length='$shirt_back_jacket_length',
                    jacket_back_jacket_length='$jacket_back_jacket_length',shirt_u_rise='$shirt_u_rise',jacket_u_rise=
                    '$jacket_u_rise',shirt_pant_left_outseam='$shirt_pant_left_outseam',jacket_pant_left_outseam=
                    '$jacket_pant_left_outseam',shirt_pant_right_outseam='$shirt_pant_right_outseam',
                    jacket_pant_right_outseam='$jacket_pant_right_outseam',shirt_bottom='$shirt_bottom',jacket_bottom=
                    '$jacket_bottom',unit_type='$unit_type',vest_collar='$vest_collar',pant_collar='$pant_collar',
                    vest_chest='$vest_chest',pant_chest='$pant_chest',vest_stomach='$vest_stomach',pant_stomach='$pant_stomach'
                    ,vest_seat='$vest_seat',pant_seat='$pant_seat',vest_bicep='$vest_bicep',pant_bicep='$pant_bicep',
                    vest_back_shoulder='$vest_back_shoulder',pant_back_shoulder='$pant_back_shoulder',vest_front_shoulder=
                    '$vest_front_shoulder',pant_front_shoulder='$pant_front_shoulder',vest_sleeve_left='$vest_sleeve_left',
                    pant_sleeve_left='$pant_sleeve_left',vest_sleeve_right='$vest_sleeve_right',pant_sleeve_right='$pant_sleeve_right'
                    ,vest_thigh='$vest_thigh',pant_thigh='$pant_thigh',vest_nape_to_waist='$vest_nape_to_waist',
                    pant_nape_to_waist='$pant_nape_to_waist',vest_front_waist_length='$vest_front_waist_length',
                    pant_front_waist_length='$pant_front_waist_length',vest_wrist='$vest_wrist',pant_wrist='$pant_wrist'
                    ,vest_waist='$vest_waist',pant_waist='$pant_waist',vest_calf='$vest_calf',pant_calf='$pant_calf',vest_back_waist_height='$vest_back_waist_height',
                    pant_back_waist_height='$pant_back_waist_height',vest_front_waist_height='$vest_front_waist_height',
                    pant_front_waist_height='$pant_front_waist_height',vest_knee='$vest_knee',pant_knee='$pant_knee',
                    vest_back_jacket_length='$vest_back_jacket_length',pant_back_jacket_length='$pant_back_jacket_length',
                    vest_u_rise='$vest_u_rise',pant_u_rise='$pant_u_rise',vest_pant_left_outseam='$vest_pant_left_outseam'
                    ,pant_pant_left_outseam='$pant_pant_left_outseam',vest_pant_right_outseam='$vest_pant_right_outseam',
                    pant_pant_right_outseam='$pant_pant_right_outseam',vest_bottom='$vest_bottom',pant_bottom='$pant_bottom'
                     where measurement_id='$meas_id'";
                    /*,pant_seat1='$pant1_seat',pant_seat11='$pant11_seat',pant_seat2='$pant2_seat',pant_seat3='$pant3_seat'*/
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Measurement Saved SuccessFully";
                    }
                    else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid Measurement Identification";
                }
            }else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }

    public function updateFinalMeasurementName($shirt_collar,$jacket_collar,$shirt_chest,$jacket_chest,$shirt_stomach,
    $jacket_stomach,$shirt_seat,$jacket_seat,$shirt_bicep,$jacket_bicep,$shirt_back_shoulder,$jacket_back_shoulder,
    $shirt_front_shoulder,$jacket_front_shoulder,$shirt_sleeve_left,$jacket_sleeve_left,$shirt_sleeve_right,
    $jacket_sleeve_right,$shirt_thigh,$jacket_thigh,$shirt_nape_to_waist,$jacket_nape_to_waist,$shirt_front_waist_length,
    $jacket_front_waist_length,$shirt_wrist,$jacket_wrist,$shirt_waist,$jacket_waist,$shirt_calf,$jacket_calf,$shirt_back_waist_height,
    $jacket_back_waist_height,$shirt_front_waist_height,$jacket_front_waist_height,$shirt_knee,$jacket_knee,
    $shirt_back_jacket_length,$jacket_back_jacket_length,$shirt_u_rise,$jacket_u_rise,$shirt_pant_left_outseam,
    $jacket_pant_left_outseam,$shirt_pant_right_outseam,$jacket_pant_right_outseam,$shirt_bottom,$jacket_bottom,
    $meas_name,$unit_type,$vest_collar,$pant_collar,$vest_chest,$pant_chest,$vest_stomach,$pant_stomach,
    $vest_seat,$pant_seat,$vest_bicep,$pant_bicep,$vest_back_shoulder,$pant_back_shoulder,$vest_front_shoulder,
    $pant_front_shoulder,$vest_sleeve_left,$pant_sleeve_left,$vest_sleeve_right,$pant_sleeve_right,$vest_thigh,
    $pant_thigh,$vest_nape_to_waist,$pant_nape_to_waist,$vest_front_waist_length,$pant_front_waist_length,$vest_wrist,
    $pant_wrist,$vest_waist,$pant_waist,$vest_calf,$pant_calf,$vest_back_waist_height,$pant_back_waist_height,$vest_front_waist_height,
    $pant_front_waist_height,$vest_knee,$pant_knee,$vest_back_jacket_length,$pant_back_jacket_length,$vest_u_rise,
    $pant_u_rise,$vest_pant_left_outseam,$pant_pant_left_outseam,$vest_pant_right_outseam,$pant_pant_right_outseam,
    $vest_bottom,$pant_bottom,$user_id/*,$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat*/){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from final_measurements where meas_name = '$meas_name' and user_id= '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $timestamp = strtotime($this->CurrentDateTime);
                    $query = "update final_measurements set shirt_collar='$shirt_collar',jacket_collar='$jacket_collar',
                    shirt_chest='$shirt_chest',jacket_chest='$jacket_chest',shirt_stomach='$shirt_stomach',jacket_stomach
                    ='$jacket_stomach',shirt_seat='$shirt_seat',jacket_seat='$jacket_seat',shirt_bicep='$shirt_bicep',
                    jacket_bicep='$jacket_bicep',shirt_back_shoulder='$shirt_back_shoulder',jacket_back_shoulder=
                    '$jacket_back_shoulder',shirt_front_shoulder='$shirt_front_shoulder',jacket_front_shoulder
                    ='$jacket_front_shoulder',shirt_sleeve_left='$shirt_sleeve_left',jacket_sleeve_left='$jacket_sleeve_left',
                    shirt_sleeve_right='$shirt_sleeve_right',jacket_sleeve_right='$jacket_sleeve_right',shirt_thigh
                    ='$shirt_thigh',jacket_thigh='$jacket_thigh',shirt_nape_to_waist='$shirt_nape_to_waist',
                    jacket_nape_to_waist='$jacket_nape_to_waist',shirt_front_waist_length='$shirt_front_waist_length',
                    jacket_front_waist_length='$jacket_front_waist_length',shirt_wrist='$shirt_wrist',jacket_wrist
                    ='$jacket_wrist',shirt_waist='$shirt_waist',jacket_waist='$jacket_waist',shirt_calf='$shirt_calf',jacket_calf='$jacket_calf',shirt_back_waist_height=
                    '$shirt_back_waist_height',jacket_back_waist_height='$jacket_back_waist_height',shirt_front_waist_height
                    ='$shirt_front_waist_height',jacket_front_waist_height='$jacket_front_waist_height',shirt_knee=
                    '$shirt_knee',jacket_knee='$jacket_knee',shirt_back_jacket_length='$shirt_back_jacket_length',
                    jacket_back_jacket_length='$jacket_back_jacket_length',shirt_u_rise='$shirt_u_rise',jacket_u_rise=
                    '$jacket_u_rise',shirt_pant_left_outseam='$shirt_pant_left_outseam',jacket_pant_left_outseam=
                    '$jacket_pant_left_outseam',shirt_pant_right_outseam='$shirt_pant_right_outseam',
                    jacket_pant_right_outseam='$jacket_pant_right_outseam',shirt_bottom='$shirt_bottom',jacket_bottom=
                    '$jacket_bottom',unit_type='$unit_type',vest_collar='$vest_collar',pant_collar='$pant_collar',
                    vest_chest='$vest_chest',pant_chest='$pant_chest',vest_stomach='$vest_stomach',pant_stomach='$pant_stomach'
                    ,vest_seat='$vest_seat',pant_seat='$pant_seat',vest_bicep='$vest_bicep',pant_bicep='$pant_bicep',
                    vest_back_shoulder='$vest_back_shoulder',pant_back_shoulder='$pant_back_shoulder',vest_front_shoulder=
                    '$vest_front_shoulder',pant_front_shoulder='$pant_front_shoulder',vest_sleeve_left='$vest_sleeve_left',
                    pant_sleeve_left='$pant_sleeve_left',vest_sleeve_right='$vest_sleeve_right',pant_sleeve_right='$pant_sleeve_right'
                    ,vest_thigh='$vest_thigh',pant_thigh='$pant_thigh',vest_nape_to_waist='$vest_nape_to_waist',
                    pant_nape_to_waist='$pant_nape_to_waist',vest_front_waist_length='$vest_front_waist_length',
                    pant_front_waist_length='$pant_front_waist_length',vest_wrist='$vest_wrist',pant_wrist='$pant_wrist'
                    ,vest_waist='$vest_waist',pant_waist='$pant_waist',vest_calf='$vest_calf',pant_calf='$pant_calf',vest_back_waist_height='$vest_back_waist_height',
                    pant_back_waist_height='$pant_back_waist_height',vest_front_waist_height='$vest_front_waist_height',
                    pant_front_waist_height='$pant_front_waist_height',vest_knee='$vest_knee',pant_knee='$pant_knee',
                    vest_back_jacket_length='$vest_back_jacket_length',pant_back_jacket_length='$pant_back_jacket_length',
                    vest_u_rise='$vest_u_rise',pant_u_rise='$pant_u_rise',vest_pant_left_outseam='$vest_pant_left_outseam'
                    ,pant_pant_left_outseam='$pant_pant_left_outseam',vest_pant_right_outseam='$vest_pant_right_outseam',
                    pant_pant_right_outseam='$pant_pant_right_outseam',vest_bottom='$vest_bottom',pant_bottom='$pant_bottom'
                     where meas_name = '$meas_name' and user_id= '$user_id'";
                    /*,pant_seat1='$pant1_seat',pant_seat11='$pant11_seat',pant_seat2='$pant2_seat',pant_seat3='$pant3_seat'*/
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Measurement Saved SuccessFully";
                    }
                    else{
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid Measurement Identification";
                }
            }else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }

    public function getAllowanceData($garment_type){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from allowance_table where garment_type = '$garment_type'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $allowanceData = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $allowanceData[] = $rows;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Measurement Saved SuccessFully";
                    $this->link->response['allowanceData'] = $allowanceData;
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid Measurement Identification";
                }
            }else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }



    public function updateAllowance($id,$value){
        $link = $this->link->connect();
        if($link) {
            $temp = explode("_",$id);
            $garment_type = $temp[0];
            $body_params  = $temp[1];
            $question_type  = $temp[2];
            $answer = $temp[3];
            if(sizeof($temp)>4){
                $answer = $answer." ".$temp[4];
            }
            if(sizeof($temp)>5){
                $answer = $answer." ".$temp[5];
            }
            $query = "update allowance_table set value='$value' where garment_type = '$garment_type' and 
            body_params='$body_params' and question_type = '$question_type' and answers = '$answer'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Allowance Saved SuccessFully";
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }public function getBodyForwardvalues($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from style_genie where user_id = '$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $row = mysqli_fetch_assoc($result);
                    $front_waist_length = $row['front_waist_length'];
                    $nape_to_waist = $row['nape_to_waist'];
                    $unit_type = $row['unit_type'];
                    if($unit_type == "Inches"){
                        $front_waist_length = $front_waist_length*2.54;
                        $nape_to_waist = $nape_to_waist*2.54;
                    }
                    $value = $front_waist_length-$nape_to_waist;
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Allowance Saved SuccessFully";
                    $this->link->response['value'] = $value;
                }else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Style Genie Data Found";
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function addCartMeasurement($cart_id,$meas_id,$meas_name){
        $link = $this->link->connect();
        if($link) {
            $query = "update wp_cart set product_meas = '$meas_id',meas_name='$meas_name' where cart_id = '$cart_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Cart Updated Successfully";
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getBodyStyle($left_side,$right_side,$combination){
        $link = $this->link->connect();
        if($link) {
            if($combination == "both"){
                $query = "select * from bodystyle where option_name = '$left_side'";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $row = mysqli_fetch_assoc($result);
                    $front = $row['combination'];
                    $back = $row['combination'];
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Values Found Successfully";
                    $this->link->response['front'] = $front;
                    $this->link->response['back'] = $back;
                }
                else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = $this->link->sqlError();
                }
            }else if($combination == "single"){
                $query = "select * from bodystyle where option_name = '$left_side'";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $row = mysqli_fetch_assoc($result);
                    $front = $row['front_shoulder'];
                    $query2 = "select * from bodystyle where option_name = '$right_side'";
                    $result2 = mysqli_query($link, $query2);
                    if ($result2) {
                        $row2 = mysqli_fetch_assoc($result2);
                        $back = $row2['back_shoulder'];
                        $this->link->response[STATUS] = Success;
                        $this->link->response[MESSAGE] = "Values Found Successfully";
                        $this->link->response['front'] = $front;
                        $this->link->response['back'] = $back;
                    }
                    else {
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getBodyStyleData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from bodystyle";
            $result = mysqli_query($link, $query);
            if ($result) {
                $styleData = array();
                while($row = mysqli_fetch_assoc($result)) {
                    $styleData[] = $row;
                }
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Found Successfully";
                $this->link->response['styleData'] = $styleData;
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }public function minmaxdata(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from minmaxdata";
            $result = mysqli_query($link, $query);
            if ($result) {
                $minmaxData = array();
                while($row = mysqli_fetch_assoc($result)) {
                    $minmaxData[] = $row;
                }
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Found Successfully";
                $this->link->response['minmaxData'] = $minmaxData;
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getpleatstyledata(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from pleat_option";
            $result = mysqli_query($link, $query);
            if ($result) {
                $pleatData = array();
                while($row = mysqli_fetch_assoc($result)) {
                    $pleatData[] = $row;
                }
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Found Successfully";
                $this->link->response['pleatData'] = $pleatData;
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function updateBodyStyle($row_id,$value,$option_name){
        $link = $this->link->connect();
        if($link) {
            $query = "update bodystyle set $option_name = '$value' where row_id = '$row_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Found Successfully";
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function updatePleatData($row_id,$value,$option_name){
        $link = $this->link->connect();
        if($link) {
            $query = "update pleat_option set $option_name = '$value' where row_id = '$row_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Updated Successfully";
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }public function updateminmaxData($row_id,$value,$option_name){
        $link = $this->link->connect();
        if($link) {
            $query = "update minmaxdata set $option_name = '$value' where row_id = '$row_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Updated Successfully";
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getMatchingData($size_type){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from sizematching where size_type = '$size_type'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $sizeMatchingData = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $sizeMatchingData[] = $rows;
                    }
                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Size Matching Found SuccessFully";
                    $this->link->response['sizeMatchingData'] = $sizeMatchingData;
                }
                else{
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "Invalid Size Matching Identification";
                }
            }else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function checkFabricStatus($fabricCode){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from fabric where fabric_cameo = '$fabricCode' and stock_status ='Discontinued'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){

                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "This Fabric are not in the stock so please choose another option.";
                }
                else{
                    $query = "select * from fabric where fabric_rc_code = '$fabricCode' and stock_status ='Discontinued'";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $num = mysqli_num_rows($result);
                        if($num>0){

                            $this->link->response[STATUS] = Error;
                            $this->link->response[MESSAGE] = "This Fabric are not in the stock so please choose another option.";
                        }
                        else{
                            $this->link->response[STATUS] = Success;
                            $this->link->response[MESSAGE] = "continue shopping";
                        }
                    }
                    else {
                        $this->link->response[STATUS] = Error;
                        $this->link->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function updateSizeMatching($row_id,$value,$colm_val){
        $link = $this->link->connect();
        if($link) {
            $query = "update sizematching set $colm_val = '$value' where id = '$row_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->link->response[STATUS] = Success;
                $this->link->response[MESSAGE] = "Values Updated Successfully";
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getMatchingRange($chest,$stomach,$bicep,$waist,$u_rise,$thigh,$collar,$shoulder,$back_jacket_length){
        $link = $this->link->connect();
        if($link) {
            $query = "SELECT * FROM sizematching" ;
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num > 0) {
                    $sizematching = array();
                    while ($row = mysqli_fetch_assoc($result)) {
                        $sizematching[] = $row;
                    }
                    $chestArray = array();
                    for($l = 0; $l < count($sizematching); $l++){
                        $sizeType = $sizematching[$l]['size_type'];
                        $sizeTitle = $sizematching[$l]['title'];
                        if($sizeType == "jacket"){
                            if($sizeTitle == "Chest")
                            {
                                $chestArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($chestArray);
                            }
                            else if($sizeTitle == "Stomach")
                            {
                                $stomachArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($stomachArray);
                            }
                        }
                        else if($sizeType == "pant"){
                            if($sizeTitle == "Waist") {
                                $waistArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($waistArray);
                            }
                            else if ($sizeTitle == "Thigh")
                            {
                                $thighArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($thighArray);
                            }
                            else if ($sizeTitle == "U-rise")
                            {
                                $urise = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($urise);
                            }
                        }

                        else if($sizeType == "shirt"){
                            if($sizeTitle == "Chest") {
                                $shirtChestArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($shirtChestArray);
                            }
                            else if ($sizeTitle == "Stomach")
                            {
                                $shirtStomachArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($shirtStomachArray);
                            }
                            else if ($sizeTitle == "Shoulder")
                            {
                                $shirtShoulderArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($shirtShoulderArray);
                            }
                            else if ($sizeTitle == "Bicep")
                            {
                                $shirtBicepArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($shirtBicepArray);
                            }
                            else if ($sizeTitle == "Neck")
                            {
                                $shirtNeckArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($shirtNeckArray);
                            }
                            else if ($sizeTitle == "Back length")
                            {
                                $shirtBackArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($shirtBackArray);
                            }
                        }
                        else if($sizeType == "vest"){
                            if($sizeTitle == "Chest") {
                                $vestChestArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($vestChestArray);
                            }
                            else if ($sizeTitle == "Stomach")
                            {
                                $vestStomachArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($vestStomachArray);
                            }
                            else if ($sizeTitle == "Back length")
                            {
                                $vestBackArray = array(
                                    "reading_1"=>$sizematching[$l]['reading_1'],
                                    "reading_2"=>$sizematching[$l]['reading_2'],
                                    "reading_3"=>$sizematching[$l]['reading_3'],
                                    "reading_4"=>$sizematching[$l]['reading_4'],
                                    "reading_5"=>$sizematching[$l]['reading_5'],
                                    "reading_6"=>$sizematching[$l]['reading_6'],
                                    "reading_7"=>$sizematching[$l]['reading_7'],
                                    "reading_8"=>$sizematching[$l]['reading_8'],
                                    "reading_9"=>$sizematching[$l]['reading_9'],
                                    "reading_10"=>$sizematching[$l]['reading_10'],
                                    "reading_11"=>$sizematching[$l]['reading_11'],
                                    "reading_12"=>$sizematching[$l]['reading_12']
                                );
                                asort($vestBackArray);
                            }
                        }
                    }



                    /*for shirt specification start here */
                    foreach($shirtChestArray as $key => $value)
                    {

                        if($value >= $chest)
                        {
                            $shirtChestRange = $key;
                            $shirtChestRange = $this->getSizeRange("shirt_specification",$shirtChestRange);
                            break;
                        }
                        else
                        {
                            $shirtChestRange = array_keys($shirtChestArray, max($shirtChestArray));
                            $shirtChestRange = array_shift($shirtChestRange);
                            $shirtChestRange = $this->getSizeRange("shirt_specification",$shirtChestRange);
                        }
                    }
                    foreach($shirtStomachArray as $key => $value)
                    {
                        if($value >= $stomach)
                        {
                            $shirtStomachRange = $key;
                            $shirtStomachRange = $this->getSizeRange("shirt_specification",$shirtStomachRange);
                            break;
                        }
                        else
                        {
                            $shirtStomachRange = array_keys($shirtStomachArray, max($shirtStomachArray));
                            $shirtStomachRange = array_shift($shirtStomachRange);
                            $shirtStomachRange = $this->getSizeRange("shirt_specification",$shirtStomachRange);
                        }
                    }
                    foreach($shirtShoulderArray as $key => $value)
                    {
                        if($value >= $shoulder)
                        {
                            $shirtShoulderRange = $key;
                            $shirtShoulderRange = $this->getSizeRange("shirt_specification",$shirtShoulderRange);
                            break;
                        }
                        else
                        {
                            $shirtShoulderRange = array_keys($shirtShoulderArray, max($shirtShoulderArray));
                            $shirtShoulderRange = array_shift($shirtShoulderRange);
                            $shirtShoulderRange = $this->getSizeRange("shirt_specification",$shirtShoulderRange);
                        }
                    }
                    foreach($shirtBicepArray as $key => $value)
                    {
                        if($value >= $bicep)
                        {
                            $shirtBicepRange = $key;
                            $shirtBicepRange = $this->getSizeRange("shirt_specification",$shirtBicepRange);
                            break;
                        }
                        else
                        {
                            $shirtBicepRange = array_keys($shirtBicepArray, max($shirtBicepArray));
                            $shirtBicepRange = array_shift($shirtBicepRange);
                            $shirtBicepRange = $this->getSizeRange("shirt_specification",$shirtBicepRange);
                        }
                    }
                    foreach($shirtNeckArray as $key => $value)
                    {
                        if($value >= $collar)
                        {
                            $shirtNeckRange = $key;
                            $shirtNeckRange = $this->getSizeRange("shirt_specification",$shirtNeckRange);
                            break;
                        }
                        else
                        {
                            $shirtNeckRange = array_keys($shirtNeckArray, max($shirtNeckArray));
                            $shirtNeckRange = array_shift($shirtNeckRange);
                            $shirtNeckRange = $this->getSizeRange("shirt_specification",$shirtNeckRange);
                        }
                    }
                    foreach($shirtBackArray as $key => $value)
                    {
                        if($value >= $back_jacket_length)
                        {
                            $shirtBackRange = $key;
                            $shirtBackRange = $this->getSizeRange("shirt_specification",$shirtBackRange);
                            break;
                        }
                        else
                        {
                            $shirtBackRange = array_keys($shirtBackArray, max($shirtBackArray));
                            $shirtBackRange = array_shift($shirtBackRange);
                            $shirtBackRange = $this->getSizeRange("shirt_specification",$shirtBackRange);
                        }
                    }
                    $shirtRange =array($shirtChestRange,$shirtStomachRange,$shirtShoulderRange,$shirtBicepRange,$shirtNeckRange,
                        $shirtBackRange);
                    $shirtRange = max($shirtRange);
                    /*for shirt specification end here */

                    /*for vest specification start here */
                    foreach($vestChestArray as $key => $value)
                    {
                        if($value >= $chest)
                        {
                            $vestChestRange = $key;
                            $vestChestRange = $this->getSizeRange("vest_specification",$vestChestRange);
                            break;
                        }
                        else
                        {
                            $vestChestRange = array_keys($vestChestArray, max($vestChestArray));
                            $vestChestRange = array_shift($vestChestRange);
                            $vestChestRange = $this->getSizeRange("vest_specification",$vestChestRange);
                        }
                    }
                    foreach($vestStomachArray as $key => $value)
                    {
                        if($value >= $stomach)
                        {
                            $vestStomachRange = $key;
                            $vestStomachRange = $this->getSizeRange("vest_specification",$vestStomachRange);
                            break;
                        }
                        else
                        {
                            $vestStomachRange = array_keys($vestStomachArray, max($vestStomachArray));
                            $vestStomachRange = array_shift($vestStomachRange);
                            $vestStomachRange = $this->getSizeRange("vest_specification",$vestStomachRange);
                        }
                    }
                    foreach($vestBackArray as $key => $value)
                    {
                        if($value >= $back_jacket_length)
                        {
                            $vestBackRange = $key;
                            $vestBackRange = $this->getSizeRange("vest_specification",$vestBackRange);
                            break;
                        }
                        else
                        {
                            $vestBackRange = array_keys($vestBackArray, max($vestBackArray));
                            $vestBackRange = array_shift($vestBackRange);
                            $vestBackRange = $this->getSizeRange("vest_specification",$vestBackRange);
                        }
                    }
                    $vestRange =array($vestChestRange,$vestStomachRange,$vestBackRange);
                    $vestRange = max($vestRange);
                    /*for vest specification end here */

                    /*for jacket specification start here */
                    foreach($chestArray as $key => $value)
                    {
                        if($value >= $chest)
                        {
                            $chestRange = $key;
                            $chestRange = $this->getSizeRange("jacket_specification",$chestRange);
                            break;
                        }
                        else
                        {
                            $chestRange = array_keys($chestArray, max($chestArray));
                            $chestRange = array_shift($chestRange);
                            $chestRange = $this->getSizeRange("jacket_specification",$chestRange);
                        }
                    }
                    foreach($stomachArray as $key => $value)
                    {
                        if($value >= $stomach)
                        {
                            $stomachRange = $key;
                            $stomachRange = $this->getSizeRange("jacket_specification",$stomachRange);
                            break;
                        }
                        else
                        {
                            $stomachRange = array_keys($stomachArray, max($stomachArray));
                            $stomachRange = array_shift($stomachRange);
                            $stomachRange = $this->getSizeRange("jacket_specification",$stomachRange);
                        }
                    }
                    $jacketRange =array($chestRange,$stomachRange);
                    $jacketRange = max($jacketRange);
                    /*for jacket specification end here */


                    /*for pant specification start here */
                    foreach($waistArray as $key => $value)
                    {
                        if($value >= $waist)
                        {
                            $waistRange = $key;
                            $waistRange = $this->getSizeRange("pant_specification",$waistRange);
                            break;
                        }
                        else
                        {
                            $waistRange = array_keys($waistArray, max($waistArray));
                            $waistRange = array_shift($waistRange);
                            $waistRange = $this->getSizeRange("pant_specification",$waistRange);
                        }
                    }
                    foreach($urise as $key => $value)
                    {
                        if($value >= $u_rise)
                        {
                            $uriseRange = $key;
                            $uriseRange = $this->getSizeRange("pant_specification",$uriseRange);
                            break;
                        }
                        else
                        {
                            $uriseRange = array_keys($urise, max($urise));
                            $uriseRange = array_shift($uriseRange);
                            $uriseRange = $this->getSizeRange("pant_specification",$uriseRange);
                        }
                    }
                    foreach($thighArray as $key => $value)
                    {
                        if($value >= $thigh)
                        {
                            $thighRange = $key;
                            $thighRange = $this->getSizeRange("pant_specification",$thighRange);
                            break;
                        }
                        else
                        {
                            $thighRange = array_keys($thighArray, max($thighArray));
                            $thighRange = array_shift($thighRange);
                            $thighRange = $this->getSizeRange("pant_specification",$thighRange);
                        }
                    }
                    $pantRange =array($waistRange,$uriseRange,$thighRange);
                    $pantRange = max($pantRange);
                    /*for jacket specification end here */


                    $this->link->response[STATUS] = Success;
                    $this->link->response[MESSAGE] = "Data Found";
                    $this->link->response['jacketRange'] = $jacketRange;
                    $this->link->response['vestRange'] = $vestRange;
                    $this->link->response['shirtRange'] = $shirtRange;
                    $this->link->response['pantRange'] = $pantRange;
                    $this->link->response['shirtNeckRange'] = $shirtNeckRange;
                    //$this->link->response['chestArray'] = $chestArray;
                    //$this->link->response['stomachArray'] = $stomachArray;
                }
                else
                {
                    $this->link->response[STATUS] = Error;
                    $this->link->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->link->response[STATUS] = Error;
                $this->link->response[MESSAGE] = $this->link->sqlError();
            }

        }
        else{
            $this->link->response[STATUS] = Error;
            $this->link->response[MESSAGE] = "Connection Error";
        }
        return $this->link->response;
    }
    public function getSizeRange($specification,$colmVal){
        $link = $this->link->connect();
        if($link) {
            $query = "SELECT * FROM sizematching where title = '$specification'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_assoc($result);
                    $range = $row[$colmVal];
                    return $range;
                }
                else
                {
                    return "Data Found";
                }
            }
            else
            {
                $this->link->sqlError();
            }
        }
        else
        {
            return "Connection Error";
        }
    }

    public function apiResponse($response){
        header("Content-Type:application/json");
        echo json_encode($response);
    }
}