<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 10/24/2017
 * Time: 11:21 AM
 */
namespace Classes;
use SquareConnect\Model\Error;

require_once('CONNECT.php');
require_once('USERCLASS.php');
class BUTTON{
    private $validData = array();
    public $link = null;
    public $userClass = null;
    public $response = array();
    private $btnFor;
    private $btnName;
    private $btnSize;
    private $btnPrice;
    public function __construct(){
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->validData = array('No.','Button For','Button Name','Button Size','Button Price');
    }
    public function validExcelSheet($data) {
        $response = array();
        if(count($data->sheets)>1) {
            $response[Status] = Error;
            $response[Message] = "Excel sheet should contain only one sheet";
            return $response;

        }
        if(count($data->sheets[0]['cells'])>0) // checking sheet not empty
        {
            $header = $data->sheets[0]['cells'][1];

            if(count($header) == 0 || count($header)< count($this->validData)) {
                $response[Status] = Error;
                $response[Message] = "Please attach valid params to your excel sheet ";
                $response["validParams"] = implode(',',$this->validData);
                return $response;
            }
            for($j=1;$j<=count($data->sheets[0]['cells']);$j++) // loop used to get each row of the sheet
            {

                for($k=1;$k<count($header);$k++) {
                    if($j == 1 )  {
                        $item = $header[$k];

                        if(!in_array(trim($item),$this->validData)) {
                            $response[Status] = Error;
                            $response[Message] = "Excel sheet should contain valid params. Invalid param found in excel : ".$item;
                            $response["validParams"] = implode(',',$this->validData);
                            $response["items"] = $item;

                            return $response;
                        }
                    }
                    else{
                        break;
                    }
                }
            }
        }
        $response[Status] = Success;
        $response[Message] = "Excel sheet is ok.";

        return $response;
    }
    public function setButtonFor($value)
    {
        $this->btnFor = $value;
    }
    public function setButtonName($value)
    {
        $this->btnName = $value;
    }
    public function setButtonSize($value)
    {
        $this->btnSize = $value;
    }
    public function setButtonPrice($value)
    {
        $this->btnPrice = $value;
    }
/*add btn data with csv file*/
    public function save() {
        $link = $this->link->connect();
        if($link) {
                $checkRcm= "select * from button where btn_type='".$this->btnFor."' and btn_name ='".$this->btnName."' ";
                $resCheckRcm = mysqli_query($link,$checkRcm);
                if($resCheckRcm){
                    if(mysqli_num_rows($resCheckRcm) > 0){
                        $query = "update button set btn_size='".$this->btnSize."', btn_price='".$this->btnPrice."' where
                         btn_type='".$this->btnFor."' and btn_name ='".$this->btnName."' ";
                        $res = mysqli_query($link,$query);
                        if($res) {
                            $response[Status] = Success;
                            $response[Message] = "button price update successfully ";
                            return $response;
                        }
                        else{
                            $response[Status] = Error;
                            $response[Message] = "unable to upload data ".$query;
                        }
                    }
                    else{
                        $imgFilePath ="http://www.scan2fit.com/sftailor/admins/api/Files/images/buttons/changeBtn.png";
                        $query = "insert into button (btn_type,btn_name,btn_size,btn_price,btn_image,btn_file_path,status) 
                        values('$this->btnFor','$this->btnName','$this->btnSize','$this->btnPrice','changeBtn.png','$imgFilePath','1')";
                        $res = mysqli_query($link,$query);
                        if($res) {
                            $num = mysqli_affected_rows($link);
                            if($num>0) {
                                $response[Status] = Success;
                                $response[Message] = "Button added successfully ";
                            }
                            else{
                                $response[Status] = Error;
                                $response[Message] = "unable to upload data ".$query;
                            }
                            return $response;
                        }
                    }
                }
                else{
                    $response[Status] = Error;
                    $response[Message] = mysqli_error($link);
                    return $response;
                }
             $response[Status] = Error;
             $response[Message] = mysqli_error($link);
             return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }




    public function addButtonImg($button_for,$button_name,$button_size,$button_price){
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        if($this->buttonExist($button_for, $button_name)){
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "This button name is already exist.";

        }
        else{
            if(isset($_FILES['btn_image'])) {
                $btnfile_tmp = $_FILES['btn_image']['tmp_name'];
                $btn_extensions = array("jpg", "png", "gif", "jpeg");
                $btn_ext = strtolower(end(explode('.', $_FILES['btn_image']['name'])));
                if (in_array($btn_ext, $btn_extensions) === true) {
                    $btn_db_file_name = $button_name."_".rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$btn_ext;
                    if (move_uploaded_file($btnfile_tmp, getcwd() . "/Files/images/buttons/" . $btn_db_file_name)) {
                        $file_path = "http://scan2fit.com/sftailor/admins/api/Files/images/buttons/". $btn_db_file_name;
                        $query = "insert into button (btn_type,btn_name,btn_size,btn_price,btn_image,btn_file_path,status)
                        values ('$button_for','$button_name','$button_size','$button_price','$btn_db_file_name',
                        '$file_path','1')";
                        $result = mysqli_query($link, $query);
                        if (!$result) {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                            return $this->response;
                        }
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "File uploaded successfully";
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "error in uploading file";
                    }
                }
            }
        }
        return $this->response;
    }
    public function buttonExist($button_for, $button_name){
        $link = $this->link->connect();
        if(!$link) {
            return false;
        }
        else{
            $query = "select * from button where btn_type= '$button_for' and btn_name ='$button_name'";
            $result = mysqli_query($link,$query);
            $count = mysqli_num_rows($result);
            return $count;
        }
    }
    public function getButtons() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from button ORDER BY id DESC ";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;
    }
    public function btnStatusChange($btnId,$btnValue){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from button where id='$btnId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE button SET status='$btnValue' WHERE id='$btnId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteBtnId($btnId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from button where id='$btnId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $rowData = mysqli_fetch_assoc($result);
                    /*unlink("Files/images/buttons/".$rowData['btn_image']);*/
                    $update = mysqli_query($link, "delete from button WHERE id='$btnId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Button Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function isFileExists($file_name) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from gallery where gallery_file='$file_name'";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $this->response[STATUS] = Error;
        $this->response[MESSAGE] = "File already exists with this name ".$file_name;
        $this->response["data"] = $data;
        return $this->response;
    }


    public function getPartBtn($btnId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from button where id = '$btnId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $btnData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['btnData'] = $btnData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Data Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function editButton($btnId,$button_for,$button_name,$button_size,$button_price,$oldBtnImagechanged,$oldBtnImage) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldBtnImagechanged == "yes") {
            if(isset($_FILES['new_btn_img'])) {
                $file_tmp = $_FILES['new_btn_img']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['new_btn_img']['name'])));
                if (in_array($file_ext, $extensions) === true) {
                    $db_file_name = $button_name."_".rand(0000, 9999) . strtotime(date("d M Y h:i:s A")) . "." . $file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/buttons/".$db_file_name)) {
                        $btnImg = $db_file_name;
                        $pathImg = "https://scan2fit.com/sftailor/admins/api/Files/buttons/".$db_file_name;
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Server Error to Upload File";
                        return $this->response;
                    }
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }
        else{
            $btnImg = $oldBtnImage;
        }
        $query = "update button set btn_type='$button_for', btn_name = '$button_name',btn_size = '$button_size',
        btn_price='$button_price',btn_image='$btnImg', btn_file_path='$pathImg' where id = '$btnId'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }
    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}