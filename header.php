<!--<!doctype html>-->
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js">
<head>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel='stylesheet' id='main-css' href='css/main1.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='main-css' href='css/font-awesome.css' type='text/css' media='all'/>
    <script type="text/javascript" src="js/raven.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/font-awesome.css" type="text/css" media="all">
    <link rel="stylesheet" href="css/mystyle.css" type="text/css" media="all">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800" type="text/css">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Montserrat:400,700" type="text/css">
    <link rel="stylesheet" href="css/jquery-ui.css" type="text/css">



    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.2.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script src="js/responsiveslides.min.js"></script>
    <script src="js/ordFile.js"></script>
    <script src="js/jquery-ui.js"></script>

    <title>sftailor</title>
</head>
<?php
error_reporting(0);
include('admins/api/Classes/CONNECT.php');
include('admins/api/Constants/DbConfig.php');
include('admins/api/Constants/configuration.php');
include('admins/api/Classes/ADMIN.php');
$conn = new \Classes\CONNECT();
session_start();
$user_id = "";
if(isset($_SESSION['sftuser_id'])) {
    $user_id = $_SESSION['sftuser_id'];
}
echo "<input type = 'hidden' id='user_id' value=" . $user_id . " >";
?>
<input type="hidden" value="" id="sustain">


<style>
    .loaderr {
        background: #fff;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }

    .loaderimg {
        background-attachment: fixed;
        margin-top: 20%;
        margin-left: 49%;
        height: 50px;
        width: 50px;
    }

    .hideloader {
        display: none !important;
    }
    .showloader{
        display: block !important;
    }
    .ui-autocomplete {
    max-height: 100px;
    overflow-y: auto;
    /* prevent horizontal scrollbar */
    overflow-x: hidden;
  }
  /* IE 6 doesn't support max-height
   * we use height instead, but this forces the menu to always be this tall
   */
   .ui-autocomplete {
    height: 100px;
  }
</style>
<style>
@media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) {
        .sb-search {
            margin-top: -80px!important;
        }
    }
    .header__logo {
        width: 170px;
    }

    p.copyright {
        width: 582px;
    }

    .errormsg {
        color: red;
        text-align: center;
    }

    .sb-search.sb-search-open, .no-js .sb-search {
        width: 0%;
    }

    .header-ancor {
        cursor: pointer;
    }

    .submenu-items-ul {
        list-style-type: none;
        display: inline-block !important;
        line-height: 1 !important;
        position: absolute;

        width: 200px;
        background: #fff;
        padding-left: 3px;
        letter-spacing: .8;

    }

    .submenu-items-a {
        text-decoration: none !important;
    }

    .submenu-items-li {
        line-height: 1 !important;
    }

    .search-box {
        float: right;
        margin-top: 13px;
    }

    .sb-search {
        backface-visibility: hidden;
        float: right;
        height: 40px;
        margin-top: 14px;
        min-width: 42px;
        position: absolute;
        right: 0;
        transition: width 0.3s ease 0s;
        width: 0;
        z-index: 999;
    }

    .header a {
        color: #404040 !important;
    }

    p, .p {
        font-family: "fontscorecomttwcenmt" !important;
    }

    .footer-bottom h3 {
        text-transform: uppercase;
    }

    .sb-icon-search {
        background: #811215 url("images/search.png") no-repeat scroll 7px 9px !important;
        border-radius: 50px;
        height: 40px;
        width: 40px;
        z-index: 90;
    }

    .sb-icon-search, .sb-search-submit {
        cursor: pointer;
        display: block;
        height: 41px;
        line-height: 71px;
        margin: 0;
        padding: 0;
        position: absolute;
        right: 79px;
        text-align: center;
        top: 0;
        width: 42px;
    }

    .bagde {
        background: red none repeat scroll 0 0;
        border-radius: 50%;
        color: white;
        font-size: 12px;
        font-weight: bold;
        height: 24px;
        line-height: 24px;
        margin: -4px;
        position: absolute;
        text-align: center;
        width: 24px;
        font-family: fantasy;
    }

    .header-ancor {
        cursor: pointer;
    }

    .submenu-items2-ul {
        list-style-type: none;
        display: inline-block !important;
        line-height: 5rem !important;
        position: absolute;

        width: 238px;
        background: #fff;
        padding-left: 3px;
        letter-spacing: .8;
        height: auto !important;
        display: none !important;

    }

    .advantage-li:hover .submenu-items2-ul {
        display: inline-block !important;
    }

    .submenu-items2-a {
        text-decoration: none !important;
        cursor: pointer;
        line-height: 5rem !important;
    }

    .submenu-items2-li {
        width: 100%;
        line-height: 2rem !important;
    }

  

    .profile_img {
        border: 1px solid #ccc;
        border-radius: 50%;
        height: 40px;
        margin-top: -10px;
        margin-bottom: -13px;
        width: 40px;
    }

    .header__menu {
        margin-left: 3rem;
    }

    @media screen and (-webkit-min-device-pixel-ratio: 0)
    and (min-resolution: .001dpcm) {
        .sb-icon-search, .sb-search-submit {
            top: 102px !important;
        }
    }
    .hover-show{display: none!important;}
    .submenu-items2-li-ul{
        display: none;
        line-height: 5rem !important;
        position: absolute;
        width: 238px;
        background: #fff;
        padding-left: 3px;
        list-style-type: none;
        height: auto !important;
        top: 50%;
        margin-left: 90%;
    }
    .submenu-items2-li:hover .submenu-items2-li-ul{
        display: inline-block !important;
    }
    .loaderr{
        display: none;
    }
</style>
<body>
<div class="loaderr" id="loadd">
    <img src="images/762.gif" class="loaderimg"/>
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<header class="site-header header" role="banner">
    <div class="header__container">
        <a href="index.php" class="header__logo">
            <img src="images/logo/logo.png" style="height: 60px; margin-top: 17px;width:60px;"/>
        </a>

        <div class="header__toggler">
            <a href="#">
                <svg viewBox="0 0 16 13" xmlns="http://www.w3.org/2000/svg">
                    <g>
                        <rect id="top-line" x="0" y="0" width="16" height="1"></rect>
                        <rect id="middle-line" x="0" y="6" width="16" height="1"></rect>
                        <rect id="bottom-line" x="0" y="12" width="16" height="1"></rect>
                    </g>
                </svg>
            </a>
        </div>
        <div class="header__list">
            <nav class="header__menu header__menu--main">
                <ul id="menu-main-en" class="menu">

                    <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
                        <a onclick="pageRedirect('index')" class="header-ancor">HOME</a>
                    </li>
                    <li id="menu-item-1042"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042 advantage-li">
                        <a onclick="pageRedirect('about')" class="header-ancor">ABOUT US</a>
                        <ul class="submenu-items2-ul">

                            <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                <a onclick="pageRedirect('about')" class="submenu-items2-a">BESPOKE</a>
                            </li>
                            <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                <a onclick="pageRedirect('suits')" class="submenu-items2-a">SUITS</a>
                            </li>

                            <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                <a onclick="pageRedirect('shirts')" class="submenu-items2-a">SHIRTS</a>
                            </li>
                            <li  class="submenu-items2-li" style="line-height: 5rem!important;">
                                <a onclick="pageRedirect('fabric')" class="submenu-items2-a">Fabric</a>
                                <ul class="submenu-items2-li-ul" style="display: none">
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('fabric')" class="submenu-items2-a">Fabric Descriptions</a>
                                    </li>

                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('topbrands')" class="submenu-items2-a">TOP Fabrics brands</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                <a onclick="pageRedirect('fullbespoke')" class="submenu-items2-a">ADVANTAGE</a>
                                <ul class="submenu-items2-li-ul" style="display: none">
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('fullbespoke')" class="submenu-items2-a">FULL BESPOKE
                                            ADVANTAGES</a>
                                    </li>

                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('customfit')" class="submenu-items2-a">CUSTOM FIT</a>
                                    </li>

                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('canvas')" class="submenu-items2-a">CANVAS</a>
                                    </li>

                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('pricelist')" class="submenu-items2-a">PRICE LIST</a>
                                    </li>

                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('workprocess')" class="submenu-items2-a">Work Process</a>
                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('highenddetails')" class="submenu-items2-a">High end
                                            Details</a>
                                    </li>
                                </ul>
                            </li>
                            <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                <a onclick="pageRedirect('faq')" class="submenu-items2-a">FAQ</a>
                            </li>
                        </ul>
                    </li>
                    <li id="menu-item-1042"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042">
                        <a onclick="pageRedirect('stylish')" class="header-ancor">Join Our Team</a>
                    </li>
					<li id="menu-item-1043"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1043">
                            <a href="http://www.sftailorsblog.com" target="_blank" class="header-ancor">Our Blog</a>
                    </li>
                    <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
                        <a onclick="pageRedirect('gallery')" class="header-ancor">GALLERY</a>
                    </li>
                    <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
                        <a onclick="pageRedirect('partners')" class="header-ancor">PARTNERS</a>
                    </li>
                    <li id="menu-item-129" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-129">
                        <a onclick="pageRedirect('book_appointment')" class="header-ancor">Book Appointment</a>
                    </li>
                    <li id="menu-item-291" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291">
                        <a onclick="pageRedirect('feedback')" class="header-ancor">FEEDBACK</a>
                    </li>
                    <li id="menu-item-26"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 advantage-li">
                        <a onclick="pageRedirect('customize')" class="header-ancor" style="padding: 0px 10px 0px 8px;color: #c4996c !important;font-size: 16px;font-weight: bold !important;letter-spacing: -1px;margin-top: -2px;">ORDER NOW</a>

                    </li>





                    <li id="menu-item-25" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                        <a onclick="pageRedirect('contact')" class="header-ancor">CONTACT US</a></li>
                    <?php
                    if ($user_id != "") {
                        ?>

                        <div class="search-box" onclick="pageRedirect('cart')">
                            <div id="sb-search" class="sb-search">
                                <span class="sb-icon-search"><span class="bagde">0</span> </span>
                            </div>
                        </div>
                    <?php }
                    ?>
                    <?php
                    if ($user_id != "") {
                        ?>
                        <li><img src="" class="profile_img" id="profile" alt="profile photo"/>
                        </li>
                        <li style="margin-left:-5px" id="menu-item-27"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27 advantage-li">
                            <a class="header-ancor" id="username">User Name</a>
                            <ul class="submenu-items2-ul">

                                <li id="menu-item-1042"
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042"
                                    style="height: 78px;">
                                    <a onclick="uploadOrd()" class="header-ancor">Upload ORD</a>

                                </li>
                                <br>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('sizematching')" class=" submenu-items2-a ">Twin Genie</a>
                                </li>
                                <!--<li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('orders')" class="submenu-items2-a">MY ORDERS</a>
                                </li>-->
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="bodyStyle()" class="submenu-items2-a">Body Style</a>
                                </li>
                               <!-- <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('sdet')" class="submenu-items2-a">Style Genie</a>
                                </li>-->
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="checkMeasurement()" class="submenu-items2-a">Check Measurement</a>
                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="myfiles()" class="submenu-items2-a">My ORD Files</a>
                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="viewRecommends()" class="submenu-items2-a">Reorder Product</a>
                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="viewSaved()" class="submenu-items2-a">Saved Measurements</a>
                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('myaccount')" class="submenu-items2-a">MY ACCOUNT</a>
                                </li>
                                <li id="menu-item-26"
                                    class="submenu-items2-li">
                                    <a onclick="pageRedirect('customize')" class="submenu-items2-a" >ORDER NOW</a>

                                </li>
                                <li id="menu-item-26"
                                    class="submenu-items2-li">
                                    <a onclick="pageRedirect('orderStatus')" class="submenu-items2-a" >ORDER STATUS</a>

                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('logout')" class="submenu-items2-a">LOGOUT</a>
                                </li>
                            </ul>
                        </li>


                        <?php
                    } else {
                        ?>
                        <li id="menu-item-25"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                            <a class="header-ancor" onclick="pageRedirect('login')">LOGIN/REGISTER</a></li>
                        <?php
                    }
                    ?>

                </ul>
            </nav>
        </div>
    </div>
</header>
<script>
    function userPersonal() {
        var user_id = $("#user_id").val();
        if (user_id != "") {
            var url = "admin/webserver/register.php?type=get_billing&user_id=" + user_id;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    var items = json.items;
                    var item = items[0];
                    var email = item.email;

                    var name = item.first_name +" "+ item.last_name;
                    if(item.first_name == "" && item.last_name == ""){
                         name = email.substr(0,5);
                    }
                    $("#username").html("&nbsp;" + name);
                    $("#profile").attr("src", item.profile_pic);
                    getCart(user_id);
                }
            }).fail(function () {
                alert("failed");
            });
        }
    }
    function getCart(user_id) {
        var url = "admin/webserver/getcart.php?user_id=" + user_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                var cartshow = "";
                if (status == "done") {
                    var items = json.items;
                    var count = items.length;
                    $(".bagde").html(count);
                }
                else {
                    //alert('error');
                }
            }
        });
    }
    userPersonal();
    function pageRedirect(pageName) {
        if (pageName == "sdet") {
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url, {"type": "isStyleGenieUsed", "user_id": user_id}, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    window.location = 'sdet.php';
                } else {
                    window.location = 'sizematching.php';
                }
            });
        } else {
            window.location = pageName + ".php";
        }
    }
    function viewRecommends(){
        window.location = "recommends.php";
    }
    function viewSaved(){
        var user_id = $("#user_id").val();
        var url = "api/registerUser.php";
        $.post(url,{"type":"getAllMeasurement","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                var measurements = data.measurements;
                var datashow = "<div class='row'><div class='container-fluid'><table class='table table-bordered'><tr>" +
                    "<th>S.No</th><th>Measurement Name</th><th>Added On</th><th>Action</th>";
                for(var i=0;i<measurements.length;i++){
                    var date = new Date(measurements[i].added_on*1000);
                    var year    = date.getFullYear();
                    var month   = date.getMonth()+1;
                    var dated     = date.getDate();
                    var added_on = dated+"/"+month+"/"+year;
                    datashow += "<tr><td>"+(i+1)+"</td><td>"+measurements[i].meas_name+"</td><td>"+added_on+"</td>" +
                        "<td><input type='button' value='View Measurement' class='btn btn-danger btn-sm' " +
                        "onclick=finalSelection('"+measurements[i].measurement_id+"','"+user_id+"') /></td></tr>";
                }
                datashow+="</table></div></div>";
                $(".modal-title").html("<span style='color:green'>Select Measurement File</span>");
                $(".modal-body").html(datashow);
                $(".modal-footer").css("display","none");
                $("#modal").modal("show");
            }
            else{
                showMessage("Not Any Measurement Saved Yet","red");
            }
        }).fail(function(){
            alert("Server Error !!! Please Try After Some Time");
        });
    }
    function finalSelection(measurement_id,user_id){
        window.location="m_detail.php?_="+user_id+"&m="+measurement_id;
    }
    function showMessage(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
        $(".modal-footer").css({"display": "none"});
        $("#modal").modal("show");
        setTimeout(function () {
            $("#modal").modal("hide");
        }, 3000);
    }
    function orderRedirect(pageName) {
        window.location = pageName + ".php?order";
    }

    /*check twin ginny complete or not*/
    function bodyStyle(){
        window.location= "sizematching.php?bodyStyle"
    }
    function checkMeasurement(){
        window.location="check_measurement.php"
    }

</script>
<!-- header end here -->

