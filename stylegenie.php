<?php
include('header.php');
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/USERS.php');
echo "<input type='hidden' value='".$_REQUEST['user_id']."' id='user_id' />";
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
$getAllQuesData = $userClass->getAllQuesData($_REQUEST['user_id']);

$quesData = $getAllQuesData['quesData'];
$userID = $quesData[0]['user_id'];
if($userID ==""){ ?>
<script>
    alert("This user cannot create own style genie");
    window.location="style-genie.php";
</script>
<?php }
$shirt_ques_1 = $quesData[0]['shirt_que_1'];
$suit_ques_1 = $quesData[0]['suit_que_1'];

?>
<style>
    .boxes{
        background: white;
        min-height:100px;
        border:1px solid #ddd;
        margin-bottom: 10px;
    }
    .Review{
        box-shadow: 3px 3px 2px #ccc;
        margin-bottom: 20px;
    }
    .boxes .tabdiv {
        background: #eee none repeat scroll 0 0;
        border: 1px solid;
        font-weight: bold;
        margin-top: 22px;
        padding: 8px;
        text-align: center;
    }

</style>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row" role="main">
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2 style="cursor:pointer" onclick="back()"><i class="fa fa-arrow-circle-left"></i> Measurement Details<small></small></h2>
                            <ul class="nav navbar-right panel_toolbox" style="display: none;"></ul>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col-md-12 boxes">
                        <hr>
                        <div class="col-md-12">
                            <div class="col-md-12" id="dimes_ul">
                                <table class="table table-bordered">
                                    <select class="form-control paramselect" onchange="getData()" style="width:200px" id="params">
                                        <option value="Inches" selected="selected">Inches</option>
                                        <option value="CM">CM</option>
                                    </select>
                                    <thead>
                                        <th colspan="4" style="text-align: center">Body Measurement</th>
                                        <th colspan="2" style="text-align: center">Extra Allowance</th>
                                        <th colspan="2" style="text-align: center">Final Measurement</th>
                                    </thead>
                                        <tr>
                                            <th>S.No.</th>
                                            <th>Parameters</th>
                                            <th>Edit Values</th>
                                            <th>Values</th>
                                            <th>Shirt</th>
                                            <th>Jacket</th>
                                            <th>Shirt</th>
                                            <th>Jacket</th>
                                        </tr>
                                        <tr>
                                            <td>1.</td>
                                            <td><span>Collar</span></td>
                                            <td>
                                                <div class="">
                                                    <i style='margin-top:10px;cursor:pointer' onclick="changeVal('collar_ip','minus')" class="fa fa-minus pull-left"></i>
                                                    <input id="collar_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                    <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('collar_ip','plus')" class="fa fa-plus pull-left"></i>
                                                </div>
                                        </td>
                                        <td id="collar"></td>
                                        <?php
                                            if($shirt_ques_1 =="Very Fitted (european)"){
                                                $defValue = 0;
                                            }
                                            else if($shirt_ques_1 =="Tailored to the body not too tight (tailored)"){
                                                $defValue = 0;
                                            }
                                            else if($shirt_ques_1 =="Roomy (Classic)"){
                                                $defValue = 0;
                                            }
                                        ?>
                                        <td><?php echo $defValue;?></td>
                                        <?php
                                        if($suit_ques_1 =="Very Fitted (european)"){
                                            $defValue = 0;
                                        }
                                        else if($suit_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = 0;
                                        }
                                        else if($suit_ques_1 =="Roomy (Classic)"){
                                            $defValue = 0;
                                        }
                                        ?>
                                        <td><?php echo $defValue;?></td>
                                        <td>0</td>
                                        <td>0</td>

                                    </tr>
                                    <tr>
                                        <td>2.</td>
                                        <td><span>Chest</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('chest_front_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="chest_front_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('chest_front_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="chest_front"></td>
                                        <?php
                                        if($shirt_ques_1 =="Very Fitted (european)"){
                                            $defValue = "6";
                                        }
                                        else if($shirt_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "8";
                                        }
                                        else if($shirt_ques_1 =="Roomy (Classic)"){
                                            $defValue = "10";
                                        }
                                        ?>
                                        <td id="collar-extra-shirt"><?php echo $defValue;?></td>
                                        <?php
                                        if($suit_ques_1 =="Very Fitted (european)"){
                                            $defValue = "6";
                                        }
                                        else if($suit_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "8";
                                        }
                                        else if($suit_ques_1 =="Roomy (Classic)"){
                                            $defValue = "12";
                                        }
                                        ?>
                                        <td id="collar-extra-jacket"><?php echo $defValue;?></td>
                                        <td id="collar-final-shirt"></td>
                                        <td id="collar-final-jacket"></td>

                                    </tr>
                                    <tr>
                                        <td>3.</td>
                                        <td><span>Waist Horizontal Line (stomach)</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('stomach_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="stomach_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('stomach_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="stomach"></td>
                                        <?php
                                        if($shirt_ques_1 =="Very Fitted (european)"){
                                            $defValue = "8";
                                        }
                                        else if($shirt_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "9";
                                        }
                                        else if($shirt_ques_1 =="Roomy (Classic)"){
                                            $defValue = "9";
                                        }
                                        ?>
                                        <td id="stomach-extra-shirt"><?php echo $defValue;?></td>

                                        <?php
                                        if($suit_ques_1 =="Very Fitted (european)"){
                                            $defValue = "3.5";
                                        }
                                        else if($suit_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "5.5";
                                        }
                                        else if($suit_ques_1 =="Roomy (Classic)"){
                                            $defValue = "9.5";
                                        }
                                        ?>
                                        <td id="stomach-extra-jacket"><?php echo $defValue;?></td>
                                        <td id="stomach-final-shirt"></td>
                                        <td id="stomach-final-jacket"></td>
                                    </tr>
                                    <tr>
                                        <td>4.</td>
                                        <td><span>Seat</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('seat_front_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="seat_front_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('seat_front_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="seat_front"></td>
                                        <?php
                                        if($shirt_ques_1 =="Very Fitted (european)"){
                                            $defValue = "6.6";
                                        }
                                        else if($shirt_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "6.6";
                                        }
                                        else if($shirt_ques_1 =="Roomy (Classic)"){
                                            $defValue = "6.6";
                                        }
                                        ?>
                                        <td id="seat-extra-shirt"><?php echo $defValue;?></td>

                                        <?php
                                        if($suit_ques_1 =="Very Fitted (european)"){
                                            $defValue = "7.2";
                                        }
                                        else if($suit_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "9.2";
                                        }
                                        else if($suit_ques_1 =="Roomy (Classic)"){
                                            $defValue = "13.2";
                                        }
                                        ?>
                                        <td id="seat-extra-jacket"><?php echo $defValue;?></td>
                                        <td id="seat-final-shirt"></td>
                                        <td id="seat-final-jacket"></td>
                                    </tr>
                                    <tr>
                                        <td>5.</td>
                                        <td><span>Bicep</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('bicep_front_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="bicep_front_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('bicep_front_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="bicep_front"></td>
                                        <?php
                                        if($shirt_ques_1 =="Very Fitted (european)"){
                                            $defValue = "8";
                                        }
                                        else if($shirt_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "8.5";
                                        }
                                        else if($shirt_ques_1 =="Roomy (Classic)"){
                                            $defValue = "9";
                                        }
                                        ?>
                                        <td id="bicep-extra-shirt"><?php echo $defValue;?></td>
                                        <?php
                                        if($suit_ques_1 =="Very Fitted (european)"){
                                            $defValue = "8.2";
                                        }
                                        else if($suit_ques_1 =="Tailored to the body not too tight (tailored)"){
                                            $defValue = "8.7";
                                        }
                                        else if($suit_ques_1 =="Roomy (Classic)"){
                                            $defValue = "9.7";
                                        }
                                        ?>
                                        <td id="bicep-extra-jacket"><?php echo $defValue;?></td>
                                        <td id="bicep-final-shirt"></td>
                                        <td id="bicep-final-jacket"></td>
                                    </tr>
                                    <tr>
                                        <td>6.</td>
                                        <td><span>Back Shoulder</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('shoulder_back_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="shoulder_back_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('shoulder_back_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="shoulder_back"></td>
                                    </tr>
                                    <tr>
                                        <td>7.</td>
                                        <td><span>Front Shoulder</span><td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('shoulder_front_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="shoulder_front_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('shoulder_front_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="shoulder_front"></td>
                                    </tr>
                                    <tr>
                                        <td>8.</td>
                                        <td><span>Sleeve length (right)</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('shoulder_sleeve_right_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="shoulder_sleeve_right_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('shoulder_sleeve_right_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="shoulder_sleeve_right"></td>
                                    </tr>
                                    <tr>
                                        <td>9.</td>
                                        <td><span>Sleeve length (left)</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('shoulder_sleeve_left_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="shoulder_sleeve_left_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('shoulder_sleeve_left_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="shoulder_sleeve_left"></td>
                                    </tr>

                                    <tr>
                                        <td>10.</td>
                                        <td><span>Thigh</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('thigh_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="thigh_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('thigh_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="thigh"></td>
                                    </tr>

                                    <tr>
                                        <td>11.</td>
                                        <td><span>Nape to waist (back)</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('napetowaist_back_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="napetowaist_back_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('napetowaist_back_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="napetowaist_back"></td>
                                    </tr>
                                    <tr>
                                        <td>12.</td>
                                        <td><span>Front waist length</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('front_waist_length_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="front_waist_length_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('front_waist_length_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="front_waist_length"></td>
                                    </tr>
                                    <tr>
                                        <td>13.</td>
                                        <td><span>Wrist</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('wrist_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="wrist_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('wrist_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="wrist"></td>
                                    </tr>
                                    <tr>
                                        <td>14.</td>
                                        <td><span>Waist</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('waist_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="waist_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('waist_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="waist"></td>
                                    </tr>
                                    <tr>
                                        <td>15.</td>
                                        <td><span>Calf</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('calf_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="calf_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('calf_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="calf"></td>
                                    </tr>

                                    <tr>
                                        <td>16.</td>
                                        <td><span>Back Waist Height</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('back_waist_height_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="back_waist_height_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('back_waist_height_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="back_waist_height"></td>
                                    </tr>
                                    <tr>
                                        <td>17.</td>
                                        <td><span>Front Waist Height</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('front_waist_height_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="front_waist_height_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('front_waist_height_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="front_waist_height"></td>
                                    </tr>
                                    <tr>
                                        <td>18.</td>
                                        <td><span>Knee</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('knee_right_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="knee_right_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('knee_right_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td><span id="knee_right"></td>
                                    </tr>
                                    <tr>
                                        <td>19.</td>
                                        <td><span>Back_Jacket_Length</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('back_jacket_length_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="back_jacket_length_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('back_jacket_length_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="back_jacket_length"></td>
                                    </tr>
                                    <tr>
                                        <td>20.</td>
                                        <td><span>U-rise</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('u_rise_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="u_rise_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('u_rise_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="u_rise"></td>
                                    </tr>
                                    <tr>
                                        <td>21.</td>
                                        <td><span>Pant Left Outseam</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('outseam_l_pants_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="outseam_l_pants_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('outseam_l_pants_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="outseam_l_pants"></td>
                                    </tr>
                                    <tr>
                                        <td>22.</td>
                                        <td><span>Pant Right Outseam</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('outseam_r_pants_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="outseam_r_pants_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('outseam_r_pants_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="outseam_r_pants"></td>
                                    </tr>
                                    <tr>
                                        <td>23.</td>
                                        <td><span>Bottom</span></td>
                                        <td>
                                            <div class="">
                                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('bottom_ip','minus')" class="fa fa-minus pull-left"></i>
                                                <input id="bottom_ip" type="text" class="form-control pull-left" style="width:52%" />
                                                <i style='margin-top:10px;cursor:pointer;margin-left:5px' onclick="changeVal('bottom_ip','plus')" class="fa fa-plus pull-left"></i>
                                            </div>
                                        </td>
                                        <td id="bottom"></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function getMeasureData(){
        //for chest
        var chestip = $("#chest_front_ip").val();
        var collarExtraShirt = $("#collar-extra-shirt").text();
        var collarExtraJacket = $("#collar-extra-jacket").text();
        var finalMeasShirt = parseInt(chestip)+parseInt(collarExtraShirt);
        var finalMeasJacket = parseInt(chestip)+parseInt(collarExtraJacket);
        $("#collar-final-shirt").text(finalMeasShirt);
        $("#collar-final-jacket").text(finalMeasJacket);

        //for Waist Horizontal Line (stomach)
        var stomachip = $("#stomach_ip").val();
        var stomachExtraShirt = $("#stomach-extra-shirt").text();
        var stomachExtraJacket = $("#stomach-extra-jacket").text();
        var finalMeasShirt = parseInt(stomachip)+parseInt(stomachExtraShirt);
        var finalMeasJacket = parseInt(stomachip)+parseInt(stomachExtraJacket);
        $("#stomach-final-shirt").text(finalMeasShirt);
        $("#stomach-final-jacket").text(finalMeasJacket);

        //for seat
        var seatip = $("#seat_front_ip").val();
        var seatExtraShirt = $("#seat-extra-shirt").text();
        var seatExtraJacket = $("#seat-extra-jacket").text();
        var finalMeasShirt = parseInt(seatip)+parseInt(seatExtraShirt);
        var finalMeasJacket = parseInt(stomachip)+parseInt(seatExtraJacket);
        $("#seat-final-shirt").text(finalMeasShirt);
        $("#seat-final-jacket").text(finalMeasJacket);

        //for bicep
        var bicepip = $("#bicep_front_ip").val();
        var bicepExtraShirt = $("#bicep-extra-shirt").text();
        var bicepExtraJacket = $("#bicep-extra-jacket").text();
        var finalMeasShirt = parseInt(bicepip)+parseInt(bicepExtraShirt);
        var finalMeasJacket = parseInt(bicepip)+parseInt(bicepExtraJacket);
        $("#bicep-final-shirt").text(finalMeasShirt);
        $("#bicep-final-jacket").text(finalMeasJacket);
    }
    function changeVal(_id,operation){
        var val = $("#" + _id).val();
        var ut = $("#params").val();
        if(ut == "Inches") {
            if (operation == "minus") {
                if (val >= 1) {
                    val = parseFloat(val) - 0.25;
                }
            } else {
                val = parseFloat(val) + 0.25;
            }
        }else{
            if (operation == "minus") {
                if (val >= 1) {
                    val = parseFloat(val) - 0.5;
                }
            } else {
                val = parseFloat(val) + 0.5;
            }
        }
        $("#" + _id).val(val);
    }
    function getData() {
        var ut = $("#params").val();
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getSize"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                if (ut == "Inches") {
                    var sizeData = data.sizeData_in;
                    $("#collar").html(Math.round(sizeData.Collar) + " Inch");
                    $("#collar_ip").val(Math.round(sizeData.Collar));
                    $("#chest_front").html(Math.round(sizeData.t2_Chest) + " Inch");
                    $("#chest_front_ip").val(Math.round(sizeData.t2_Chest));
                    $("#stomach").html(Math.round(sizeData.waist_belly_botton_Full) + " Inch");
                    $("#stomach_ip").val(Math.round(sizeData.waist_belly_botton_Full));
                    $("#seat_front").html(Math.round(sizeData.t4_Seat) + " Inch");
                    $("#seat_front_ip").val(Math.round(sizeData.t4_Seat));
                    $("#bicep_front").html(Math.round(sizeData.Bicep) + " Inch");
                    $("#bicep_front_ip").val(Math.round(sizeData.Bicep));
                    $("#shoulder_back").html(Math.round(sizeData.Back_Shoulder_Width ) + " Inch");
                    $("#shoulder_back_ip").val(Math.round(sizeData.Back_Shoulder_Width ));
                    $("#shoulder_front").html(Math.round(sizeData.Front_Shoulder_Width ) + " Inch");
                    $("#shoulder_front_ip").val(Math.round(sizeData.Front_Shoulder_Width ));
                    $("#shoulder_sleeve_right").html(Math.round(sizeData.Sleeve_Length) + " Inch");
                    $("#shoulder_sleeve_right_ip").val(Math.round(sizeData.Sleeve_Length));
                    $("#shoulder_sleeve_left").html(Math.round(sizeData.Sleeve_Length) + " Inch");
                    $("#shoulder_sleeve_left_ip").val(Math.round(sizeData.Sleeve_Length));
                    $("#thigh").html(Math.round(sizeData.Thigh) + " Inch");
                    $("#thigh_ip").val(Math.round(sizeData.Thigh));
                    $("#thigh_right").html(Math.round(sizeData.Thigh_Right) + " Inch");
                    $("#thigh_right_ip").val(Math.round(sizeData.Thigh_Right));
                    $("#napetowaist_back").html(Math.round(sizeData.Nape_to_Waist_Back) + " Inch");
                    $("#napetowaist_back_ip").val(Math.round(sizeData.Nape_to_Waist_Back));
                    $("#front_waist_length").html(Math.round(sizeData.Front_Waist_Length) + " Inch");
                    $("#front_waist_length_ip").val(Math.round(sizeData.Front_Waist_Length));
                    $("#wrist").html(Math.round(sizeData.Wrist) + " Inch");
                    $("#wrist_ip").val(Math.round(sizeData.Wrist));
                    $("#waist").html(Math.round(sizeData.Waist) + " Inch");
                    $("#waist_ip").val(Math.round(sizeData.Waist));
                    $("#calf").html(Math.round((sizeData.Calf_Left+sizeData.Calf_Right)/2) + " Inch");
                    $("#calf_ip").val(Math.round((sizeData.Calf_Left+sizeData.Calf_Right)/2));
                    $("#back_waist_height").html(Math.round(sizeData.Back_Waist_Height) + " Inch");
                    $("#back_waist_height_ip").val(Math.round(sizeData.Back_Waist_Height));
                    $("#front_waist_height").html(Math.round(sizeData.Front_Waist_Height) + " Inch");
                    $("#front_waist_height_ip").val(Math.round(sizeData.Front_Waist_Height));
                    $("#knee_right").html(Math.round((sizeData.Knee_Left+sizeData.Knee_Right)/2) + " Inch");
                    $("#knee_right_ip").val(Math.round(Math.round((sizeData.Knee_Left+sizeData.Knee_Right)/2)));
                    $("#back_jacket_length").html(Math.round(sizeData.Back_Jacket_Length)+" Inch");
                    $("#back_jacket_length_ip").val(Math.round(sizeData.Back_Jacket_Length));
                    $("#u_rise").html(Math.round(sizeData.U_rise)+" Inch");
                    $("#u_rise_ip").val(Math.round(sizeData.U_rise));
                    $("#outseam_l_pants").html(Math.round(sizeData.Outseam_L_pants)+" Inch");
                    $("#outseam_l_pants_ip").val(Math.round(sizeData.Outseam_L_pants));
                    $("#outseam_r_pants").html(Math.round(sizeData.Outseam_R_Pants)+" Inch");
                    $("#outseam_r_pants_ip").val(Math.round(sizeData.Outseam_R_Pants));
                    $("#bottom").html(Math.round(sizeData.Bottom_for_trousers)+" Inch");
                    $("#bottom_ip").val(Math.round(sizeData.Bottom_for_trousers));
                    //getMeasureData();
                } else {
                    var sizeData = data.sizeData_cm;
                    $("#collar").html(Math.round(sizeData.Collar) + " Cm");
                    $("#collar_ip").val(Math.round(sizeData.Collar));
                    $("#chest_front").html(Math.round(sizeData.t2_Chest) + " Cm");
                    $("#chest_front_ip").val(Math.round(sizeData.t2_Chest));
                    $("#stomach").html(Math.round(sizeData.waist_belly_botton_Full) + " Cm");
                    $("#stomach_ip").val(Math.round(sizeData.waist_belly_botton_Full));
                    $("#seat_front").html(Math.round(sizeData.t4_Seat) + " Cm");
                    $("#seat_front_ip").val(Math.round(sizeData.t4_Seat));
                    $("#bicep_front").html(Math.round(sizeData.Bicep) + " Cm");
                    $("#bicep_front_ip").val(Math.round(sizeData.Bicep));
                    $("#shoulder_back").html(Math.round(sizeData.Back_Shoulder_Width ) + " Cm");
                    $("#shoulder_back_ip").val(Math.round(sizeData.Back_Shoulder_Width ));
                    $("#shoulder_front").html(Math.round(sizeData.Front_Shoulder_Width ) + " Cm");
                    $("#shoulder_front_ip").val(Math.round(sizeData.Front_Shoulder_Width ));
                    $("#shoulder_sleeve_right").html(Math.round(sizeData.Sleeve_Length) + " Cm");
                    $("#shoulder_sleeve_right_ip").val(Math.round(sizeData.Sleeve_Length));
                    $("#shoulder_sleeve_left").html(Math.round(sizeData.Sleeve_Length) + " Cm");
                    $("#shoulder_sleeve_left_ip").val(Math.round(sizeData.Sleeve_Length));
                    $("#thigh").html(Math.round(sizeData.Thigh) + " Cm");
                    $("#thigh_ip").val(Math.round(sizeData.Thigh));
                    $("#thigh_right").html(Math.round(sizeData.Thigh_Right) + " Cm");
                    $("#thigh_right_ip").val(Math.round(sizeData.Thigh_Right));
                    $("#napetowaist_back").html(Math.round(sizeData.Nape_to_Waist_Back) + " Cm");
                    $("#napetowaist_back_ip").val(Math.round(sizeData.Nape_to_Waist_Back));
                    $("#front_waist_length").html(Math.round(sizeData.Front_Waist_Length) + " Cm");
                    $("#front_waist_length_ip").val(Math.round(sizeData.Front_Waist_Length));
                    $("#wrist").html(Math.round(sizeData.Wrist) + " Cm");
                    $("#wrist_ip").val(Math.round(sizeData.Wrist));
                    $("#waist").html(Math.round(sizeData.Waist) + " Cm");
                    $("#waist_ip").val(Math.round(sizeData.Waist));
                    $("#calf").html(Math.round((sizeData.Calf_Left+sizeData.Calf_Right)/2) + " Cm");
                    $("#calf_ip").val(Math.round((sizeData.Calf_Left+sizeData.Calf_Right)/2));
                    $("#back_waist_height").html(Math.round(sizeData.Back_Waist_Height) + " Cm");
                    $("#back_waist_height_ip").val(Math.round(sizeData.Back_Waist_Height));
                    $("#front_waist_height").html(Math.round(sizeData.Front_Waist_Height) + " Cm");
                    $("#front_waist_height_ip").val(Math.round(sizeData.Front_Waist_Height));
                    $("#knee_right").html(Math.round((sizeData.Knee_Left+sizeData.Knee_Right)/2) + " Cm");
                    $("#knee_right_ip").val(Math.round(Math.round((sizeData.Knee_Left+sizeData.Knee_Right)/2)));
                    $("#back_jacket_length").html(Math.round(sizeData.Back_Jacket_Length)+" Cm");
                    $("#back_jacket_length_ip").val(Math.round(sizeData.Back_Jacket_Length));
                    $("#u_rise").html(Math.round(sizeData.U_rise)+" Cm");
                    $("#u_rise_ip").val(Math.round(sizeData.U_rise));
                    $("#outseam_l_pants").html(Math.round(sizeData.Outseam_L_pants)+" Cm");
                    $("#outseam_l_pants_ip").val(Math.round(sizeData.Outseam_L_pants));
                    $("#outseam_r_pants").html(Math.round(sizeData.Outseam_R_Pants)+" Cm");
                    $("#outseam_r_pants_ip").val(Math.round(sizeData.Outseam_R_Pants));
                    $("#bottom").html(Math.round(sizeData.Bottom_for_trousers)+" Cm");
                    $("#bottom_ip").val(Math.round(sizeData.Bottom_for_trousers));
                    //getMeasureData();
                }
                $("#size").html(sizeData.Size);
                $("#size_ip").val(sizeData.Size);
                $("#dimes_ul").show();
                $(".humanbody").show();
                $(".error").css("display", "none");
            }
            else {
                showMessage(data.Message, 'red');
                $("#dimes_ul").hide();
                $(".humanbody").hide();
                $("#downloadbtn").css("display", "none");
                $(".error").css("display", "block");
            }
        }).fail(function () {
            showMessage("Server Error !!! Please Try After Some Time....", 'red');
        });

    }
    getData();
</script>
