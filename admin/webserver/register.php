<?php
require_once 'errorCodes.php';
require_once 'functions.php';
require_once 'database/db.php';
$status="error";
$items=array();
$message="unable to get data";
$mail_status= "";
$requiredfields = array('type');
if(!RequiredFields($_REQUEST, $requiredfields)){
	return false;
}
$type = $_REQUEST['type'];
if($type=="billing"){
	$requiredfields = array('user_id', 'billing_fname','billing_lname','billing_address','pincode','city','state','country','phone');
	if(!RequiredFields($_REQUEST, $requiredfields)){
		return false;
	}
	$user_id = $_REQUEST['user_id'];
	$qq = "select * from wp_customers where user_id = '$user_id'";
	$rr = mysql_query($qq);
	$num = mysql_num_rows($rr);
	if($num>0)
	{
		$billing_fname = $_REQUEST['billing_fname'];
		$billing_lname = $_REQUEST['billing_lname'];
		$name = $billing_fname." ".$billing_lname;
		$address = $_REQUEST['billing_address'];
		$pincode = $_REQUEST['pincode'];
		$city = $_REQUEST['city'];
		$state = $_REQUEST['state'];
		$country = $_REQUEST['country'];
		$phone = $_REQUEST['phone'];
		$update_data = "update wp_customers set name='$name',address='$address',pincode='$pincode',b_city='$city',
		b_state='$state',b_country='$country',mobile='$phone' where user_id='$user_id'";
		$update_res = mysql_query($update_data);
		if($update_res){
			$status=  "done";
			$message = "Billing Address Updated Successfully";
		}
		else{
			$status=  "error";
			$message=mysql_error();
		}
	}
	else{
		$status=  "error";
		$message="Invalid User Id";
	}
	echo json_encode(array("status"=>$status,"message"=>$message));
}
else if($type=="get_billing") {
	$requiredfields = array('user_id');
	if(!RequiredFields($_REQUEST, $requiredfields)){
		return false;
	}
	$user_id = $_REQUEST['user_id'];
	$select_data = "select * from wp_customers where user_id='$user_id'";
	$select_res = mysql_query($select_data);
	if($select_res){
		$num=mysql_num_rows($select_res);
		if($num>0){
			$status="done";
			$message="data found";
			$row=mysql_fetch_array($select_res);
			$user_id=$row['user_id'];
			$name= explode(" ",$row['name']);
			$first_name=$name[0];
			$last_name=$name[1];
			$address=$row['address'];
			$pincode=$row['pincode'];
			$city=$row['b_city'];
			$state=$row['b_state'];
			$country=$row['b_country'];
			$mobile=$row['mobile'];
			$profile_pic=$row['profile_pic'];
			$email = $row['email'];
			$query = "select * from wp_cart where user_id = '$user_id'";
			$result = mysql_query($query);
			$total_amount = 0;
			while ($rows = mysql_fetch_array($result))
			{
				$total_amount=$total_amount+$rows['cart_product_total_amount'];
			}
			$items[]=array("user_id"=>$user_id,"first_name"=>$first_name,"last_name"=>$last_name,"address"=>$address,
			"pincode"=>$pincode,"city"=>$city,"state"=>$state,"country"=>$country,"mobile"=>$mobile,
			"total_amount"=>$total_amount,"profile_pic"=>$profile_pic,"email"=>$email);
		}
		else{
			$status = "error";
			$message = "no data found";
		}
	}
	else{
		$status="error";
		$message="unable to update data..".mysql_error();
	}
	echo json_encode(array("status"=>$status,"message"=>$message,"items"=>$items));
}
?>