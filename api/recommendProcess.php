<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 9/25/2017
 * Time: 12:17 PM
 */

error_reporting(0);
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/USERS.php";
require_once "Classes/Recommends.php";

$userClass = new Modals\USERS();
$recommendClass = new \Classes\Recommends();

$requiredFeilds = array("type");
$response = RequiredFields($_REQUEST, $requiredFeilds);
if ($response[Status] === Error) {
    $userClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];

if($type === 'addRecommends') {

    $requiredFeilds = array("data","userId");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] === Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $data = $_REQUEST['data'];
    $user_id = $_REQUEST['userId'];
    $data = json_decode($data,true);
    $recommendClass->setData($data);
    $recommendClass->setUserId($user_id);
    $recommendResponse = $recommendClass->save();
    $userClass->apiResponse($recommendResponse);

}
else if($type === 'getRecommends') {
    $requiredFeilds = array("userId");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] === Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $recommendResponse = $recommendClass->getRecommends();
    $userClass->apiResponse($recommendResponse);
}
else if($type === 'getUserRecommends') {
    $requiredFeilds = array("userId");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] === Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['userId'];
    $recommendClass->setUserId($user_id);

    $recommendResponse = $recommendClass->getRecommends();
    $userClass->apiResponse($recommendResponse);
}
else if($type === 'searchRecommends') {
    $requiredFeilds = array("userId","searchText","searchKey");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] === Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['userId'];
    $searchText = $_REQUEST['searchText'];
    $searchKey = $_REQUEST['searchKey'];

    $recommendClass->setUserId($user_id);
    $recommendResponse = $recommendClass->searchRecommends($searchText,$searchKey);
    $userClass->apiResponse($recommendResponse);

}

else if($type === "addToCart") {
    $requiredFeilds = array("data","userId");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] === Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $data = $_REQUEST['data'];
    $user_id = $_REQUEST['userId'];

    $data = json_decode($data,true);
    $recommendClass->setData($data);
    $recommendClass->setUserId($user_id);
    $cartResponse = $recommendClass->addToCart();
    $userClass->apiResponse($cartResponse);
}


?>