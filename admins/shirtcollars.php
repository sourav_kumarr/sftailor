<?php
include('header.php');
require_once('api/Classes/BRAND_LOGO.php');
$conn = new \Classes\CONNECT();
$collar_data = new \Classes\BRAND_LOGO();
$getCollars = $collar_data->getCollars();

?>
<style>
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .main-grid-box {
        text-align: center;
        margin-left: 5%;
        margin-bottom: 2%;
        border: 1px solid #999;
        /*width:80%*/
    }
    .ext-img {
        height: 200px;
        /*height: 50px;*/
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
    }
    #logoBox{
        max-height: 900px;
        overflow-y: auto;
    }
    .old_btn_image{
        position: absolute;
        top: 22px;
        height: 100px;
        width: 125px;
        opacity: 0;
        image-orientation: from-image;
    }
    .oldbtnimg{
        height:100px;
        width:125px;
        image-orientation :from-image;
    }
</style>
<link href = "js/jquery-ui.css" rel = "stylesheet">
<script src="js/jquery-ui.js"></script>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<!--  page content  -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Collar<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <form method="post" class="form-inline">
                                <div class="form-group form-inline">
                                    <label id="message" style="color: red"></label>
                                    <input type="file" class="form-control" name="fabricFile" id="logoFile" />
                                    <input type="button" Value="+ Add Collar" class="btn btn-primary btn-sm" name="filterButton"
                                           onclick="addCollar()" style="margin-bottom: -1px"/>
                                </div>
                            </form>
                        </li>
                    </ul>
                    <?php include('footer.php'); ?>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Available Users
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Collar Image</th>
                                <th>Collar Name</th>
                                <th>Description</th>
                                <th>Visibility</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $j=1;
                            $collarData = $getCollars['data'];
                            for($i=0;$i<count($collarData);$i++){
                                ?>
                                <tr>
                                    <td data-title='#'><?php echo $j ?></td>
                                    <td><img src="api/Files/images/collar/<?php echo $collarData[$i]['collar_img_path']; ?>" style="height: 50px;"></td>
                                    <td><?php echo $collarData[$i]['collar_name']; ?></td>
                                    <td><?php echo $collarData[$i]['collar_description']; ?></td>
                                    <td>
                                        <?php if($collarData[$i]['status'] =="1") { ?>
                                        <input type="hidden" id="visible_status_<?php echo $collarData[$i]['id']; ?>" value="<?php echo $collarData[$i]['id']; ?>">
                                        <select id="visiblity_<?php echo $collarData[$i]['id']; ?>">
                                            <option selected value="1">Visible</option>
                                            <option value="0">Not Visible</option>
                                        </select>
                                        <?php } else { ?>
                                        <input type="hidden" id="visible_status_<?php echo $collarData[$i]['id']; ?>" value="<?php echo $collarData[$i]['id']; ?>">
                                        <select id="visiblity_<?php echo $collarData[$i]['id']; ?>">
                                            <option  value="1">Visible</option>
                                            <option selected value="0">Not Visible</option>
                                        </select>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <a href='#' onclick=deleteCollar('<?php echo $collarData[$i]['id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>
                                        <a href='#' onclick=getCollarPart('<?php echo $collarData[$i]['id'];?>','<?php echo $collarData[$i]['collar_img_path'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-pencil"></i> </a>
                                    </td>
                                </tr>
                                <script>
                                    $('#visiblity_<?php echo $collarData[$i]['id']; ?>').on('change', function() {
                                        var clrStatus = this.value;
                                        var clrId = $("#visible_status_<?php echo $collarData[$i]['id']; ?>").val();
                                        var url = "api/brandLogoProcess.php";
                                        $(".loaderdiv").removeClass("loaderhidden");
                                        $.post(url, {"type": "updateClrStatus", "clrId": clrId, "clrStatus": clrStatus}, function (data) {
                                            var Status = data.Status;
                                            var Message = data.Message;
                                            if (Status == "Success") {
                                                window.location.reload();
                                            }
                                            else{
                                                showMessage(Message,"red");
                                                $(".loaderdiv").addClass("loaderhidden");
                                            }
                                        });
                                    })
                                </script>
                                <?php $j++ ;}  ?>
                            </tbody>
                        </table>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <!--body part start here-->

</div>

<!-- /page content -->

<!-- script -->
<script>
    var oldClrImagechanged = "no";

    function addCollar() {
        var logoFile = $("#logoFile").val();
        if(logoFile == ""){
            $("#message").html("Please select image file");
            return false;
        }
        var data = new FormData();
        $("#message").html("");
        $(".loaderdiv").removeClass("loaderhidden");
        var _file = document.getElementById('logoFile');
        var image_ext = logoFile.split(".");
        image_ext = image_ext[image_ext.length - 1];
        image_ext = image_ext.toLowerCase();
        if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
            data.append('type', "addCollar");
            data.append('file_', _file.files[0]);
        }
        else{
            $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
            $(".loaderdiv").addClass("loaderhidden");
            return false;
        }
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#message").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else
                {
                    $("#message").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/brandLogoProcess.php');
        request.send(data);
    }


    function getCollarPart(clrId,oldImage){
        var url = "api/brandLogoProcess.php";
        $.post(url, {"type": "getPartCollar", "clrId": clrId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var btnData = data.btnData;
            if (Status == "Success") {
                $(".modal-title").html("<label style='color: green'>Update Collar</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='message'" +
                    " style='color:red'></p>" +
                    "<div class='col-md-6 form-group'><label>Collar Name</label><input type='text' name='collar_name' " +
                    "id='collar_name' class='form-control' placeholder='Collar Name' value='"+btnData.collar_name+"' /></div>" +
                    "<div class='col-md-6 form-group'><label>Collar Description</label><input type='text' name='collar_description' " +
                    "id='collar_description' class='form-control' placeholder='Collar Description' value='"+btnData.collar_description+"' /></div>" +
                    "<div class='col-md-6 form-group'><label>Collar Image</label><br><img src='api/Files/images/collar/"+btnData.collar_img_path+"' class='oldbtnimg' />" +
                    "<input type='file' name='collarFile' id='new_clr_img' class='form-control old_btn_image'  " +
                    "onchange=readURL(this,'oldclrimg');  /></div><div class='col-md-12'>" +
                    "<input type='button' class='btn btn-danger pull-right' style='margin-top:30px' onclick=updateClr('"+btnData.id+"','"+oldImage+"');" +
                    " id='subbtn' value='Update'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }

    function deleteCollar(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>'+
            '<button type="button" class="btn btn-danger" onclick=deleteData("'+id+'")>Delete</button>');
        $("#myModal").modal("show");
    }

    function deleteData(id) {
        var url ='api/brandLogoProcess.php';
        $.post(url,{"type":'deleteCollar','id':id},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                location.reload();
            }
            else{
                $("#myModal").modal("hide");
                showMessage(message,"green");
                getBrandData();
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }

    function updateClr(clrId,oldImage) {
        oldImage = decodeURI(oldImage);
        var collar_name = $("#collar_name").val();
        var collar_description = $("#collar_description").val();
        var new_clr_img = $("#new_clr_img").val();
        var data = new FormData();
        if (new_clr_img !== "") {
            var file_new_clr_img = document.getElementById('new_clr_img');
            var new_clr_image_ext = new_clr_img.split(".");
            new_clr_image_ext = new_clr_image_ext[new_clr_image_ext.length - 1];
            new_clr_image_ext = new_clr_image_ext.toLowerCase();
            if (new_clr_image_ext == "jpg" || new_clr_image_ext == "png" || new_clr_image_ext == "gif" || new_clr_image_ext == "jpeg") {
                data.append('new_clr_img', file_new_clr_img.files[0]);
                oldClrImagechanged = "yes";
            }
            else {
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color", "red");
                $("#subbtn").attr("disabled", false);
                return false;
            }
        }

        if (collar_name !== "" || collar_description !== "") {
            $("#message").html("");
            $("#subbtn").attr("disabled", true);
            $(".loaderdiv").removeClass("loaderhidden");
            data.append('type', 'editCollars');
            data.append('clrId', clrId);
            data.append('collar_name', collar_name);
            data.append('collar_description', collar_description);
            data.append('oldClrImage', oldImage);
            data.append('oldClrImagechanged', oldClrImagechanged);
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState == 4) {
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if (status == "Success") {
                        location.reload();
                        $("#myModal").modal("hide");
                    }
                    else {
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                        $("#message").css("color", "red");
                        $("#subbtn").attr("disabled", false);
                        return false;
                    }
                }
            };
            request.open('POST', 'api/brandLogoProcess.php');
            request.send(data);
        }
        else {
            $("#message").html("please fill required fields");
            $("#message").css("color", "red");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }






</script>
<!-- / script -->
