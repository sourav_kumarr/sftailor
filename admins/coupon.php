<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
    <style>
        .cop-price {
            float: left;
            height: 28px;
            width: 55px;
        }

        .couponForCheck {
            margin: 0 10px !important;
        }
    </style>
    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">

        </div>
        <div class="row" role="main">
            <div class="">
                <!--<div class="page-title">
                    <div class="title_left"></div>@f1*2C3AmY7+
                </div>-->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <?php if ($_REQUEST['type'] == 'price_list') { ?>

                            <?php } else { ?>

                                <div class="x_title">
                                    <h2><span style="color:#1ABB9C;"></span><?php echo $_REQUEST['type']; ?> Price List
                                        <small></small>
                                    </h2>
                                    <?php if ($_REQUEST['type'] == 'Coupons') { ?>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="btn btn-info"
                                                   onclick="addNewSuitPrice('<?php echo $_REQUEST['type']; ?>')"
                                                   style="color: white"><i class="fa fa-plus"></i> Add Coupon</a></li>
                                        </ul>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                    <p class="text-muted font-13 m-b-30">
                                        From here admin can manage/modify the content of the catalogs
                                    </p>
                                    <table id="catTable" class="table table-striped table-bordered display">
                                        <thead>
                                        <?php if ($_REQUEST['type'] == 'Coupons') { ?>
                                            <tr>
                                                <th>#</th>
                                                <th>Image</th>

                                                <th>Coupon Name</th>
                                                <th>Condition</th>
                                                <th>Discount %</th>
                                                <th>Coupon Uses</th>
                                                <th>Coupon For</th>
                                                <th>Price</th>
                                                <th>Action</th>
                                            </tr>
                                        <?php } ?>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $link = $conn->connect();
                                        if ($link) {
                                            $query = "select * from categories where cat_type= '" . $_REQUEST['type'] . "' 
                                        order by cat_id asc";
                                            $result = mysqli_query($link, $query);
                                            if ($result) {
                                                $num = mysqli_num_rows($result);
                                                if ($num > 0) {
                                                    $j = 0;
                                                    while ($catData = mysqli_fetch_array($result)) {
                                                        $j++;
                                                        if ($catData['cat_type'] == "Coupons") {

                                                            ?>
                                                            <tr>
                                                                <td data-title='#'><?php echo $j ?></td>

                                                                <td data-title='Image'>
                                                                    <img src='images/twopicesuit.jpg'
                                                                         style='height:35px'
                                                                         class='img-thumbnail'>
                                                                </td>

                                                                <!--Coupon name start here-->
                                                                <td data-title='Category' style="font-weight: 600;">
                                                                    <input id="cat_name_<?php echo $catData['cat_id'] ?>"
                                                                           class="form-control cop-price" maxlength="50"
                                                                           pattern="\d*" maxlength="4"
                                                                           value="<?php echo $catData['cat_name'] ?>"
                                                                           required="" type="text"
                                                                           style="display: none;width: 195px;">
                                                                    <span id="staticname_<?php echo $catData['cat_id'] ?>">
                                                                        <?php echo $catData['cat_name'] ?></span>
                                                                    <input type='hidden' value='editCategory'
                                                                           id='type'/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                    <span id='editname_<?php echo $catData['cat_id'] ?>'
                                                                          onclick=editname('<?php echo $catData['cat_id'] ?>')>
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"
                                                                   style="color: rgb(208, 94, 97);cursor: pointer; font-size: 19px; float: right; margin-top: 6px; font-weight: bold; margin-right: 14px;"></i>
                                                                </span>
                                                                    <span id="editsave_name_<?php echo $catData['cat_id'] ?>"
                                                                          style="display:none;"
                                                                          onclick=updateCouponData('<?php echo $catData['cat_id'] ?>')>
                                                                    <i class="fa fa-floppy-o" aria-hidden="true"
                                                                       style="color: green; margin-top: 3px; margin-left: -10px; font-size: 23px;cursor: pointer;"></i> </span>
                                                                    <span id="canclesave_name_<?php echo $catData['cat_id'] ?>"
                                                                          style="display:none;"
                                                                          onclick="canclesave_name('<?php echo $catData['cat_id'] ?>')">
                                                                    <i class="fa fa-times" aria-hidden="true"
                                                                       style="color: rgb(208, 94, 97); float: right; font-weight: bold; font-size: 24px; margin-right: -4px; margin-top: 1px; cursor: pointer;"></i>
                                                                </span>
                                                                </td>
                                                                <!--coupon name end here-->


                                                                <td data-title='Type'>
                                                                    <?php echo $catData['coupon_condition'] ?>
                                                                </td>

                                                                <td data-title='Price' style="font-weight: 600;">
                                                                    <input id="cat_price_<?php echo $catData['cat_id'] ?>"
                                                                           class="form-control cop-price" maxlength="50"
                                                                           pattern="\d*" maxlength="4"
                                                                           value="<?php echo $catData['cat_price'] ?>"
                                                                           required="" type="text"
                                                                           style="display: none;">
                                                                    <span id="staticprice_<?php echo $catData['cat_id'] ?>">
                                                                        <?php echo $catData['cat_price'] ?>
                                                                    </span>

                                                                    <input type='hidden' value='editCategory'
                                                                            id='type'/>

                                                                    <!--For edit price start here-->
                                                                    <?php if($catData['coupon_condition'] == "discount"){
                                                                        ?>
                                                                        <span id='editprice_<?php echo $catData['cat_id'] ?>'
                                                                              onclick=editprice('<?php echo $catData['cat_id'] ?>')
                                                                        <i class="fa fa-pencil-square-o" aria-hidden="true"
                                                                           style="color: rgb(208, 94, 97);cursor: pointer; font-size: 19px; float: right; margin-top: 6px; font-weight: bold; margin-right: 14px;"></i>
                                                                        </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <span
                                                                                id="editsave_<?php echo $catData['cat_id'] ?>"
                                                                                style="display:none;"
                                                                                onclick=updateCouponData('<?php echo $catData['cat_id'] ?>')><i
                                                                                    class="fa fa-floppy-o" aria-hidden="true"
                                                                                    style="color: green; margin-top: 3px; margin-left: -10px; font-size: 23px;cursor: pointer;"></i>
                                                                        </span>
                                                                        <span id="canclesave_<?php echo $catData['cat_id'] ?>"
                                                                              style="display:none;"
                                                                              onclick="canclesave('<?php echo $catData['cat_id'] ?>')"><i
                                                                                    class="fa fa-times" aria-hidden="true"
                                                                                    style="color: rgb(208, 94, 97); float: right; font-weight: bold; font-size: 24px; margin-right: -4px; margin-top: 1px; cursor: pointer;"></i>
                                                                        </span>
                                                                <?php

                                                                    } ?>
                                                                    <!--For edit price end here-->
                                                                </td>

                                                                <!--Coupon uses start here-->
                                                                <?php $infinityArr = array("once", "infinity"); ?>
                                                                <td data-title='uses'>
                                                                    <select id="infinty<?php echo $j; ?>"
                                                                            onchange=couponInfinity('<?php echo $catData['cat_id'] ?>','<?php echo $j ?>')
                                                                            style='width: 108px; height: 28px; margin-top: 3px;'>
                                                                        <?php
                                                                        for ($k = 0; $k < count($infinityArr); $k++) {
                                                                            if ($catData['coupon_infinity'] == $infinityArr[$k]) {
                                                                                ?>
                                                                                <option selected
                                                                                        value="<?php echo $infinityArr[$k]; ?>"><?php echo $infinityArr[$k]; ?></option>
                                                                                <?php
                                                                            } else {
                                                                                ?>
                                                                                <option value="<?php echo $infinityArr[$k]; ?>"><?php echo $infinityArr[$k]; ?></option>
                                                                                <?php
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </td>
                                                                <!--Coupon uses start here-->

                                                                <!--coupon for start here-->
                                                                <?php
                                                                if($catData['coupon_condition'] == "freebies"){
                                                                    ?>
                                                                    <td data-title='uses'>
                                                                        <?php
                                                                        echo substr($catData['freebies_item'], 0, 12) . '...';
                                                                        ?>
                                                                        <a onclick=editCouponFor('<?php echo $catData['cat_id'] ?>','<?php echo rawurlencode($catData['freebies_item_with']) ?>','<?php echo rawurlencode($catData['freebies_item']) ?>',"freebies",'<?php echo rawurlencode($catData['cat_name'])?>')
                                                                           style="float: right;font-size: 18px; padding: 2px 4px;
                                                                            background: tomato none repeat scroll 0% 0%; color: white;">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </a>
                                                                    </td>
                                                                    <?php
                                                                }
                                                                else if($catData['coupon_condition'] == "discount"){
                                                                    $couponsForArr = array("All", "Custom Suit", "Custom Shirt", "Custom Pant",
                                                                        "Custom 2Pc Suit", "Custom Vest", "Accessories", "Custom Overcoat", "Collection");

                                                                    ?>
                                                                    <td data-title='uses'>
                                                                        <?php
                                                                        echo substr($catData['coupon_for'], 0, 12) . '...';
                                                                        ?>
                                                                        <a onclick=editCouponFor('<?php echo $catData['cat_id'] ?>','<?php echo rawurlencode($catData['cat_price']) ?>','<?php echo rawurlencode($catData['coupon_for']) ?>','discount','<?php echo rawurlencode($catData['cat_name'])?>')
                                                                           style="float: right;font-size: 18px; padding: 2px 4px;
                                                                            background: tomato none repeat scroll 0% 0%; color: white;">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </a>
                                                                    </td>

                                                                    <?php
                                                                }
                                                                  ?>
                                                                <td data-title='Added On'><?php echo $catData['added_on'] ?></td>
                                                                <td data-title='Delete'><a href='#' onclick=delCoupon('<?php echo $catData['cat_id']; ?>')
                                                                                           style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i
                                                                                class="fa fa-trash-o"></i> </a></td>
                                                            </tr>


                                                            <?php
                                                        }
                                                        ?>


                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function delCoupon(cat_id) {
            var url = "api/categoryProcess.php";
            $.post(url, {"type": "deleteCategory", "cat_id": cat_id}, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    location.reload();
                }
            });
        }
    </script>
    <!-- /page content -->
<?php
include('footer.php');
?>