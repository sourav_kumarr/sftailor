
<?php
include("header.php");
require_once('admins/api/Classes/PARTNER.php');
$conn = new \Classes\CONNECT();
$partnerClass = new \Classes\PARTNER();
$getAllParnters = $partnerClass->getPartner();
$getPartnerCategories = $partnerClass->getCategories();


?>
<style>
    .main-grid-box {
        text-align: center;
        margin-left: 5%;
        border: 1px solid #999;
        margin-bottom: 2%;
    }
    .ext-img {
        height: 240px;
        /* width: 272px;*/
        margin-bottom: 15px;
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
    }
    #wonderplugingridgallery-1 .wonderplugin-gridgallery-item-text {
        background-color: #fff;
        color: #333;
        text-align: center;
        font: 14px "open sans", Arial, Helvetica, sans-serif;
        padding: 8px 0px;
        position: absolute;
        left: 0px;
        bottom: 0px;
        width: 100%;
        height: 100%;
        box-sizing: border-box;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
    }

    #wonderplugingridgallery-1 .wonderplugin-gridgallery-item-wrapper {
        display: block;
        width: 100%;
        height: auto;
        position: absolute;
        left: 0;
        top: 50%;
        transform: translateY(-50%);
        backface-visibility: hidden;
    }

    #wonderplugingridgallery-1 .wonderplugin-gridgallery-item-title {
        font-size: 14px;
    }

    #wonderplugingridgallery-1 .wonderplugin-gridgallery-item-description {
        font-size: 12px;
        margin-top: 10px;
    }

    #wonderplugingridgallery-1 .wonderplugin-gridgallery-item-button {
        margin-top: 10px;
    }

    .wpgridgallery-socialmedia-button {
        display: inline-block;
        margin: 4px;
    }

    .wpgridgallery-socialmedia-button a {
        box-shadow: none;
    }

    .wpgridgallery-socialmedia-icon {
        display: table-cell;
        width: 32px;
        height: 32px;
        font-size: 18px;
        border-radius: 50%;
        color: #fff;
        vertical-align: middle;
        text-align: center;
        cursor: pointer;
        padding: 0;
    }

    .wpgridgallery-socialmedia-rotate {
        transition: transform .4s ease-in;
    }

    .wpgridgallery-socialmedia-rotate:hover {
        transform: rotate(360deg);
    }
    #wonderplugingridgallery-1 .wpp-category-greybutton .wonderplugin-gridgallery-tag {
        border: none;
        background: #404040;
        color: #fff;
        padding: 6px 21px;
        font-size: 12px;
        font-weight: normal;
        font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif, Arial;
        box-sizing: border-box;
        cursor: pointer;
        text-align: center;
        text-decoration: none;
        text-shadow: none;
        text-transform: none;
        white-space: nowrap;
        -webkit-font-smoothing: antialiased;
        border-radius: 2px;
        transition: background-color 0.3s ease;
    }

    #wonderplugingridgallery-1 .wpp-category-greybutton .wonderplugin-gridgallery-tag:hover, #wonderplugingridgallery-1 .wpp-category-greybutton .wonderplugin-gridgallery-tag-selected {
        border: none;
        background: #D5504F;
        color: #fff;
    }
    .wonderplugin-gridgallery-item{border: 1px solid #45AA3D;}
    .wonderplugin-gridgallery-tags-topcenter {
        margin-bottom: 52px;
        width: 883px;
    }
    .wonderplugin-gridgallery-item-container{padding: 8px!important;}
</style>
<!--content -->
<link rel="stylesheet"  href="css/wonderplugingridgalleryengine.css" type="text/css" media="all"/>
<div style="margin-top: 140px">
    <div class="container">
        <div class="row">
            <h2 class="tittle">Partners <span style="color:#000;"></span></h2>
        </div>
        <div class="wonderplugingridgallery" id="wonderplugingridgallery-1" data-gridgalleryid="1" data-width="260"
             data-height="260" data-skin="feature" data-style="flip"
             data-categorylist="[{&quot;slug&quot;:&quot;all&quot;,&quot;caption&quot;:&quot;Show All&quot;},{&quot;slug&quot;:&quot;photo&quot;,&quot;caption&quot;:&quot;Photo&quot;},{&quot;slug&quot;:&quot;video&quot;,&quot;caption&quot;:&quot;Video&quot;},{&quot;slug&quot;:&quot;sea&quot;,&quot;caption&quot;:&quot;Sea&quot;},{&quot;slug&quot;:&quot;landscape&quot;,&quot;caption&quot;:&quot;Landscape&quot;}]"
             data-enabletabindex="false" data-masonrymode="false" data-random="false" data-shownavigation="true"
             data-shownavcontrol="true" data-hidenavdefault="false" data-nohoverontouchscreen="false"
             data-hoverzoomin="false" data-hoverzoominimageonly="true" data-hoverzoominimagecenter="false"
             data-hoverfade="false" data-responsive="true" data-mediumscreen="true" data-smallscreen="true"
             data-showtitle="true" data-showtexttitle="true" data-showtextdescription="true"
             data-showtextbutton="true" data-overlaylink="true" data-donotaddtext="false"
             data-titlebottomcss="{color:#333; font-size:14px; font-family:Armata,sans-serif,Arial; overflow:hidden; text-align:left;}"
             data-descriptionbottomcss="{color:#333; font-size:12px; font-family:Arial,Helvetica,sans-serif; overflow:hidden; text-align:left; margin:4px 0px 0px; padding: 0px;}"
             data-googleanalyticsaccount="" data-gap="30" data-margin="0" data-borderradius="0"
             data-videoplaybutton="playvideo-64-64-0.png" data-column="3" data-mediumcolumn="3"
             data-mediumscreensize="800" data-smallcolumn="2" data-smallscreensize="600" data-titlemode="mouseover"
             data-titleeffect="flipy" data-titleeffectduration="500" data-titleheight="0" data-scalemode="fill"
             data-lightboxbgcolor="#fff" data-lightboxoverlaybgcolor="#000" data-lightboxoverlayopacity="0.8"
             data-navbgcolor="rgba(0,0,0,0.2)" data-categoryposition="topcenter"
             data-categorystyle="wpp-category-greybutton" data-categorydefault="all"
             data-verticalcategorysmallscreenwidth="480" data-triggerresizedelay="100"
             data-skinsfoldername="skins/default/"
             style="display:none;position:relative;margin:0 auto;width:100%;max-width:812px;">
            <div class="wonderplugin-gridgallery-tags wonderplugin-gridgallery-tags-topcenter wpp-category-greybutton">
                <div class="wonderplugin-gridgallery-tag" data-slug="all">All</div>
                <?php
                $partnersC = $getPartnerCategories['data'];
               
                for($p=0;$p<count($partnersC);$p++){
                ?>
                <div class="wonderplugin-gridgallery-tag" data-slug="<?php echo $partnersC[$p]['category'];?>"><?php echo $partnersC[$p]['category'];?></div>
                <?php } ?>
            </div></br></br>
            <div class="wonderplugin-gridgallery-list"  style="display:block;position:relative;max-width:100%;margin:0 auto;">
                <?php
                $partners = $getAllParnters['data'];

                for($k=0;$k<count($partners);$k++){
                ?>
                    <div style="cursor: pointer" onclick=partnerLink('<?php echo $partners[$k]['fairLink'];?>') class="wonderplugin-gridgallery-item" data-category="<?php echo $partners[$k]['category'];?>" data-row="1" data-col="1">
                        <div class="wonderplugin-gridgallery-item-container">
                            <a class="wpgridlightbox" href=""  data-title="<?php echo $partners[$k]['fairTitle'];?>">
                                <img class="wonderplugin-gridgallery-item-img<?php echo $k;?>" src="images/trans.png" alt="Forest"/>
                            </a>
                        </div>
                    </div>
<style>.wonderplugin-gridgallery-item-img<?php echo $k;?>{background: url("http://scan2fit.com/sftailor/admins/api/Files/images/partner/<?php echo $partners[$k]['fairLogo'];?>");background-position: center center;
        background-size: 100% auto;background-repeat:no-repeat;height: 100%}</style>

                <?php } ?>
            </div>

        </div>
    </div>
</div>
<!-- / content -->



<script type='text/javascript' src='js/wonderplugingridlightbox.js'></script>
<script type='text/javascript' src='js/wonderplugingridgallery.js?ver=11.3C'></script>
<?php
include("footer.php"); ?>
<script>

    function partnerLink(link) {
        if(link.indexOf("http:") > 0){

        }
        else{
            link = "http://"+link;
        }
        window.open(link,'_blank');
    }
    /*function getPartnerData(){
        var url = "admins/api/partnerProcess.php";
        $.post(url,{"type":"getPartner"},function(data){
            var status = data.Status;
            //var partnerData = data.data;
            if(status == "Success"){
                var partnerBox = "";
                    partnerBox = partnerBox+'';

                $("#partners").html(partnerBox);
            }
            else{
                showMessage(data.Message, "red");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
        });
    }
    getPartnerData();*/
</script>
