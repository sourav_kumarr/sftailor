<?php
    include('header.php');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Check Measurement<small></small></h2>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of User's Having Check Measurement Used.
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>User Profile</th>
                                    <th>User Name</th>
                                    <th>User E-Mail</th>
                                    <th>User Contact</th>
                                    <th>User Gender</th>
                                    <th>User City</th>
                                    <th>Stylist</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="userData"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function loadUsersData(){
        var storeName = $("#store_name").val();
        if(storeName == ""){
            showError("Session Timed Out !!! Please Login Again","red");
            return false;
        }
        var url = "api/userProcess.php";
        $.post(url,{"type":"getAllUsersData","store_name":storeName},function(data){
            var Status = data.Status;
            var dataShow = "";
            var ids = [];
            if(Status == "Success"){
                var userData = data.UserData;
                for(var i=0;i<userData.length;i++){
                    if(ids.indexOf(userData[i].user_id)>-1){
                        //alert("data already exist");
                    }else {
                        ids.push(userData[i].user_id);
                        dataShow = dataShow + "<tr><td><img alt='Image' src='" + userData[i].profile_pic + "' " +
                            "style='height:40px !important;width:40px;border:2px solid #333'/></td><td>" + userData[i].name + "</td>" +
                            "<td>" + userData[i].email + "</td><td>" + userData[i].mobile + "</td><td>" + userData[i].gender + "</td>" +
                            "<td>" + userData[i].b_city + "</td><td>"+storeName+"</td><td><button class='btn btn-sm btn-info' " +
                            "onclick=checkMeasurement('" + userData[i].user_id + "')>Check Measurement</button></td></tr>";
                    }
                }
            }else{
                dataShow = "<tr><td colspan=8 style='text-align:center'>"+data.Message+"</td></tr>";
            }
            $("#userData").html(dataShow);
        }).fail(function(){
            showError("Server Error !!! Please Try After Some Time...","red");
        });
    }
    function checkMeasurement(user_id){
        window.location='check_measurement_det.php?_='+user_id;
    }
    loadUsersData();
</script>
