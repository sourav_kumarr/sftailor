<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 11-12-2017
 * Time: 12:39
 */

namespace Classes;


class BROADCAST
{
    public $link = null;
    public $broad_id = null;
    public $broad_msg_id = null;
    public $broad_users = null;
    public $broad_created_at = null;
    public $broad_updated = null;
    public $response = array();

    public function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    /**
     * @param false|int $currentDateTimeStamp
     */
    public function setCurrentDateTimeStamp($currentDateTimeStamp)
    {
        $this->currentDateTimeStamp = $currentDateTimeStamp;
    }

    /**
     * @param null $broad_created_at
     */
    public function setBroadCreatedAt($broad_created_at)
    {
        $this->broad_created_at = $broad_created_at;
    }

    /**
     * @param false|string $currentDateTime
     */
    public function setCurrentDateTime($currentDateTime)
    {
        $this->currentDateTime = $currentDateTime;
    }

    /**
     * @param null $broad_id
     */
    public function setBroadId($broad_id)
    {
        $this->broad_id = $broad_id;
    }

    /**
     * @param null $broad_msg_id
     */
    public function setBroadMsgId($broad_msg_id)
    {
        $this->broad_msg_id = $broad_msg_id;
    }

    /**
     * @param null $broad_updated
     */
    public function setBroadUpdated($broad_updated)
    {
        $this->broad_updated = $broad_updated;
    }

    /**
     * @param null $broad_users
     */
    public function setBroadUsers($broad_users)
    {
        $this->broad_users = $broad_users;
    }

    public function save()
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $msgExists = $this->isMsgExists();
        if ($msgExists[Status] === Success) {
            $this->response = $this->updateMsgId();
            return $this->response;
        }

            $query = "insert into wp_broadcasts(broad_msg_id, broad_users, broad_created_at,broad_updated)values('$this->broad_msg_id','$this->broad_users','$this->broad_created_at','$this->broad_updated')";
            $result = mysqli_query($link, $query);
            if (!$result) {
                $this->response[Status] = Error;
                $this->response[Message] = "Unable to insert record " . $this->link->sqlError();
                return $this->response;
            }
            $broad_id = mysqli_insert_id($link);
            if ($broad_id > 0) {
                $this->response[Status] = Success;
                $this->response[Message] = "Record inserted successfully ";
                return $this->response;
            }
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to insert record " . $this->link->sqlError();
            return $this->response;


    }

    public function isMsgExists()
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "select * from wp_broadcasts where broad_msg_id='$this->broad_msg_id'";
        $result = mysqli_query($link, $query);
        if (!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to insert record " . $this->link->sqlError();
            return $this->response;
        }
        $count = mysqli_num_rows($result);
        if ($count > 0) {
            $this->response[Status] = Success;
            $this->response[Message] = "Msg id found";
            return $this->response;
        }
        $this->response[Status] = Error;
        $this->response[Message] = "No data found ";
        return $this->response;
    }

    public function updateMsgId()
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "update wp_broadcasts set broad_updated='$this->broad_updated',broad_users='$this->broad_users' where broad_msg_id='$this->broad_msg_id'";

        $result = mysqli_query($link, $query);
        if (!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to update record " . $this->link->sqlError();
            return $this->response;

        }
        $id_ = mysqli_affected_rows($link);
        if ($id_ === 0) {
            $this->response[Status] = Error;
            $this->response[Message] = "No new changes found";
            return $this->response;
        }
        $this->response[Status] = Success;
        $this->response[Message] = "Broadcast updated successfully";
//        $this->response["msg_id"] = $id_;

        return $this->response;

    }

    public function getAllBroadCast() {
        $link = $this->link->connect();
        $data = array();
        if (!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "select msg_id,msg_desc,msg_desc,msg_text,broad_id,broad_users,broad_updated,broad_created_at from wp_broadcasts,wp_messages where wp_broadcasts.broad_msg_id = wp_messages.msg_id";
        $result = mysqli_query($link, $query);
        if (!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to insert record " . $this->link->sqlError();
            return $this->response;
        }
        $count = mysqli_num_rows($result);
        if ($count > 0) {
            while($rows = mysqli_fetch_assoc($result)) {
                $data[] = $rows;
            }
            $this->response[Status] = Success;
            $this->response[Message] = "Msg data found";
            $this->response["data"] = $data;

            return $this->response;
        }
        $this->response[Status] = Error;
        $this->response[Message] = "No data found ";
        return $this->response;
    }

    public function removeSchedule() {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to connect with database";
            return $this->response;
        }
        $query = "delete from wp_broadcasts where broad_id='$this->broad_id'";
        $result = mysqli_query($link, $query);
        if (!$result) {
            $this->response[Status] = Error;
            $this->response[Message] = "Unable to remove record " . $this->link->sqlError();
            return $this->response;
        }
        $count = mysqli_affected_rows($link);
        if ($count > 0) {
            $this->response[Status] = Success;
            $this->response[Message] = "Unschedule done successfuly";

            return $this->response;
        }
        $this->response[Status] = Error;
        $this->response[Message] = "Record is removed already";
        return $this->response;
    }
}