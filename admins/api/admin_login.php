<?Php
require_once ('Classes/ADMIN.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$adminClass = new \Classes\ADMIN();
if(isset($_REQUEST["_"])){
    $_SESSION['capd'] = $_REQUEST['_'];
    header("Location:../login?_=cpd");
}
else {
    $requiredfields = array('type');
    ($response = RequiredFields($_POST, $requiredfields));
    if ($response['Status'] == 'Failure') {
        $adminClass->apiResponse($response);
        return false;
    }
    error_reporting(0);
    $type = $_POST['type'];
    if ($type == "admin_login") {
        $requiredfields = array('email', 'password');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $adminClass->apiResponse($response);
            return false;
        }
        $username = trim($_POST['email']);
        $password = MD5(trim($_POST['password']));
        $response = $adminClass->adminLogin($username, $password);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $adminClass->apiResponse($response);
    }
    else if ($type == "changeAdminPassword") {
        $requiredfields = array('old_password', 'new_password', 'admin_id');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $adminClass->apiResponse($response);
            return false;
        }
        $adminId = trim($_POST['admin_id']);
        $old_password = MD5(trim($_POST['old_password']));
        $new_password = MD5(trim($_POST['new_password']));
        $response = $adminClass->changeAdminPassword($old_password, $new_password, $adminId);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $adminClass->apiResponse($response);
    }
    else if ($type == "changeForgotPassword") {
        $requiredfields = array('new_password');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $adminClass->apiResponse($response);
            return false;
        }
        $admin_email = trim($_SESSION['capd']);
        $new_password = MD5(trim($_POST['new_password']));
        $response = $adminClass->changeForgotPassword($new_password, $admin_email);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $adminClass->apiResponse($response);
    }
    else if ($type == "forgotPassword") {
        $response = $adminClass->getAdminEmail();
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $adminEmail = $response['admin_email'];
        $response = $adminClass->sendMaill($adminEmail);
        $adminClass->apiResponse($response);
    }
    else if ($type == "addAdmins") {

		$adminName 	 = $_POST['admin_name'];
		$email 		 = $_POST['email'];
		$password 	 = MD5(trim($_POST['password']));
		$password2 	 = $_POST['password'];
		$access_menu = $_POST['access_name'];
		$menus 		 = implode(',', $access_menu);
        $requiredfields = array($email,$password);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $response = $adminClass->addAdministratorData($adminName,$email,$password,$menus,$password2);
		
        $adminClass->apiResponse($response); 
		
    }
	else if ($type == "updateAdministrator") {
		$admin_id 	 = $_POST['adminId'];
		$adminName 	 = $_POST['admin_name'];
		$email 		 = $_POST['email'];
		$password 	 = MD5(trim($_POST['password']));
		$password2 	 = $_POST['password'];
		$access_menu = $_POST['access_name'];
		$menus 		 = implode(',', $access_menu);
        $requiredfields = array($admin_id);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $response = $adminClass->updateAdministratorData($adminName,$email,$password,$menus,$admin_id,$password2);
        $adminClass->apiResponse($response); 
    }
    else if ($type == "updateSocial") {
        $s_id 	 = $_POST['s_id'];
        $admin_name 	 = $_POST['admin_name'];
        $social_link 	 = $_POST['social_link'];
        $requiredfields = array($s_id);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $response = $adminClass->updateSocialData($admin_name,$social_link,$s_id);
        $adminClass->apiResponse($response);
    }
	 else if ($type == "deleteAdmin") {
		$admin_id 	 = $_POST['admin_id'];
        $requiredfields = array($admin_id);
        if ($response[STATUS] == Error) {
            $adminClass->apiResponse($response);
            return false;
        }
        $response = $adminClass->delAdministratorData($admin_id);
        $adminClass->apiResponse($response); 
    }
    else {
        $response[STATUS] = Error;
        $response[MESSAGE] = "502 UnAuthorised Request";
        $adminClass->apiResponse($response);
    }
}
?>