<meta content="text/html;charset=utf-8" http-equiv="Content-Type">

<meta content="utf-8" http-equiv="encoding">

<?php

include('header.php');

include("admin/webserver/database/db.php");

?>

</div>

<div class="about-bottom wthree-3">

    <script>

        if ($("#user_id").val() == "") {

            window.location = "login.php";

        }

    </script>

    <style>

        .quantity_ip{
            width:182px !important;
        }
        .search-boxx{
            width: 130px;
            -webkit-transition: width 0.4s ease-in-out;
            transition: width 0.4s ease-in-out;
        }


        .search-boxx:focus {
            width: 80%;
        }


        #search {
            float: right;
            margin-top: 9px;
            width: 250px;
        }

        .search-boxx {
            padding: 5px 0;
            width: 230px;
            height: 30px;
            position: relative;
            left: 10px;
            float: left;
            line-height: 22px;
        }

        .search-boxx input {
            position: absolute;
            width: 0px;
            float: Left;
            margin-left: 210px;
            -webkit-transition: all 0.7s ease-in-out;
            -moz-transition: all 0.7s ease-in-out;
            -o-transition: all 0.7s ease-in-out;
            transition: all 0.7s ease-in-out;
            height: 30px;
            line-height: 18px;
            padding: 0 2px 0 2px;
            border-radius:1px;
        }

        .search-boxx:hover input, .search-boxx input:focus {
            width: 200px;
            margin-left: 0px;
        }

        .btnn {
            height: 30px !important;
            position: absolute !important;;
            right: 0 !important;
            top: 5px !important;
            border-radius:1px !important;
            background: #d9534f;
            border-color: snow;
        }

        .top-bar {

            background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;

        }

        .img {

            height: 150px;

            position: absolute;

            width:auto !important;

            margin-top:20px;

        }

        .nodata {

            color: #002856;

            font-size: 19px;

            font-weight: bold;

            text-align: center;

            text-shadow: 10px 9px 3px #ccc;

        }

        .active {

            /*border: 3px solid #000;*/

        }

        table {

            font-family: arial, sans-serif;

            border-collapse: collapse;

            width: 100%;

        }

        td{

            max-width: 100px;

            overflow-wrap: break-word;

        }

        td, th {

            border: 1px solid #dddddd;

            font-size: 12px;

            letter-spacing: 1.2px;

            padding: 1.4px 16px;

            text-align: left;

        }

        tr:nth-child(even) {

            background-color: #eee;

        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
            background: #f7f7f7;
        }
        .total_ip{
            width:109%;
        }
        .clearcartbtn{

            margin-bottom: 24px;

            margin-top: 0px;

            text-transform: uppercase;

            margin-right: 15px;

            border-radius: 4px;

            padding: 8px 25px;

            background: rgb(212, 80, 79) none repeat scroll 0% 0%;

            border: 1px solid rgb(212, 80, 79); color: white;

            display:none;

        }
        .quantity_inc{
            display:none;
        }
        .search-container{
            display: flex;
            flex-direction: row;

        }
        .cartcaption{
            text-align: left;
        }
        .order_quan{
            margin-top: 0px;
        }
        .paymentStatus{
            text-align: left;
            margin-top: 20px;
        }
        .paymentStatus span{
            color:#811215;
        }
    </style>

    <script src="js/location.js"></script>

    <div class="loader">

        <img src="images/spin.gif" class="spinloader"/>

    </div>

    <div class="col-md-12" style="margin-bottom: 30px">

        <div class="col-md-12">
            <div class="col-md-12" style="padding: 0px; margin-top: 40px;text-align: center">
               <!-- <p class="cartcaption" >My Orders</p>-->
                <h2 class="tittle">Order Status <span style="color:#000;"></span></h2>
            </div>

            <div class="clear"></div>

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab" onclick="getRecommendsData('orderStatus')">Order Status</a>
                </li>
            </ul>
            <div class="tab-content" style="background-color:#f7f7f7;padding:7px 5px 7px 5px;height: 700px;overflow: auto;overflow-x: hidden">
                <div id="cart" class="row"></div>
            </div>
        </div>

    </div>

    <input type="hidden" id="selectedMeasurement" />

    <script>

        var user_id = $("#user_id").val();
        var selectedMeasurement="";
        var stock=10;
        var recommend_data = [];
        $(".nav a").removeClass("active");

        function getRecommendsData(type) {
            var reqType = type;
            if(type == "orderStatus"){
                type = "getUserRecommends";
                reqType = "orderStatus";
            }
            var url = "api/recommendProcess.php";
            $(".loader").show();
            $.post(url, {userId: user_id, type: type}, function (data) {


                $(".loaderr").hide();
                var status = data.Status;
                var message = data.Message;

                var button = "";

                var cartshow = "";

                if (status === "Success") {

                    $("#clear_cart").css("display", "block");

                    var items = data.data;

                    var totalamount = 0;

                    if (items.length > 0) {
                        recommend_data = [];
                        for (var i = 0; i < items.length; i++) {

                            totalamount = parseFloat(totalamount) + parseFloat(items[i].cart_product_total_amount);
                            recommend_data.push(items[i]);
                            var prod_name = items[i].product_name;

                            var product_type = items[i].product_type;

                            var prod_image = items[i].product_image;

                            var meas_name = "Add To Cart";

                            if (items[i].meas_name != "") {

                                meas_name = items[i].meas_name;

                            }
                            if(reqType != "orderStatus"){
                                if (product_type == "Custom Suit") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img alt='Custom Fabric' src='" + prod_image[0] + "' class='img' style='z-index:0' />" +

                                        "<img alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' /><img  alt='' src='" + prod_image[2] +

                                        "' class='img' style='z-index:3' /><img alt=''  src='" + prod_image[3] + "' class='img' style='z-index:3' />" +

                                        "<img  alt='' src='" + prod_image[4] + "' class='img' style='z-index:4' /><img alt=''  src='" + prod_image[5] +

                                        "' class='img' style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +

                                        "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[8] +

                                        "' class='img' style='z-index:2' /></div><div class='col-md-10'><table style='font-family: " +

                                        "trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>Product Name </th><td>" +

                                        prod_name + "</td></tr> <tr><th>Selected Buttons </th><td>" + items[i].j_frontbutton + "</td></tr>" +

                                        "<tr><th>Selected Vents</th><td>" + items[i].j_backvent + "</td></tr><tr><th>Breast Pocket</th><td>" +

                                        items[i].j_breastpocket + "</td></tr><tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td>" +

                                        "</tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption + "</td></tr></table></div>" +

                                        "</div> <div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +

                                        "readonly id='quantity" + i + "' value='" + items[i].quantity + "'/> <div class='quantity_inc'>" +

                                        "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Jacket','" + escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i +

                                        "','" + items[i].cart_id + "') ></i><i class='fa fa-plus-circle increment_quantity' " +

                                        "onclick=incrementValue('" + items[i].user_id + "','Jacket','" + escape(items[i].j_canvasoption) +

                                        "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')></i></div><input type='text'" +

                                        " id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  />" +

                                        "<div class='clear'></div><input class='form-control total_ip' type='text' readonly " +

                                        "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text' " +

                                        "hidden id='stock_value' value='" + stock + "' /><input type='button' value='" + meas_name + "' " +

                                        "class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin-top:20px;width:100%' " +

                                        " onclick='addToCart(" + i + ")'/></div></div>";

                                }
                                else if (product_type == "Custom 2Pc Suit") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' " +

                                        "style='z-index:0' /><img  alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' />" +

                                        "<img alt=''  src='" + prod_image[2] + "' class='img' style='z-index:3' /><img  alt='' src='" +

                                        prod_image[3] + "' class='img' style='z-index:3' /><img  alt='' src='" + prod_image[4] +

                                        "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[5] + "' class='img' " +

                                        "style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +

                                        "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" +

                                        prod_image[8] + "' class='img' style='z-index:2' /></div><div class='col-md-10'>" +

                                        "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr>" +

                                        "<th class=''>Suit Name </th><td>" + prod_name + "</td><th>Suit Type</th><td>Custom 2Pc Suit</td>" +

                                        "</tr><tr><th>Suit Buttons </th><td>" + items[i].j_frontbutton + "</td><th>Pant Category</th><td>" +

                                        items[i].p_category + "</td></tr><tr><th>Suit Vents</th><td>" + items[i].j_backvent + "</td><th>" +

                                        "Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Breast Pocket</th><td>" +

                                        items[i].j_breastpocket + "</td><th>Back Pocket</th><td>" + items[i].p_backpocket + "</td></tr>" +

                                        "<tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td><th>Belt Loop</th><td>" +

                                        items[i].p_beltloop + "</td></tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption +

                                        "</td><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +

                                        "readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +

                                        "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','2PcSuit','" + escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i + "','" +

                                        items[i].cart_id + "')></i><i class='fa fa-plus-circle increment_quantity' " +

                                        "onclick=incrementValue('" + items[i].user_id + "','2PcSuit','" + escape(items[i].j_canvasoption) +

                                        "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')></i></div><input type='text'" +

                                        "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  />" +
                                        "<div class='clear'></div><input class='form-control total_ip' type='text' readonly " +

                                        "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text'" +

                                        "id='stock_value' value='" + stock + "' hidden  /><input type='button' value='" + meas_name +

                                        "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin-top:20px;width:100%'" +

                                        " onclick='addToCart(" + i + ")'/></div></div>";

                                }

                                else if (product_type == "Custom Shirt") {

                                    stock = 5;

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'" +

                                        "><div class='col-md-2'><img  alt='Custom Fabric' style='margin-top:5px' src='" +

                                        prod_image[0] + "' class='img' /><img  alt='' src='" + prod_image[1] + "' class='img' " +

                                        "style='margin-top:5px'/><img src='" + prod_image[2] + "' class='img' " +

                                        "style='z-index:2;margin-top:5px' /><img src='" + prod_image[3] + "' class='img' " +

                                        "style='margin-top:5px' /><img  alt='' src='" + prod_image[4] + "' class='img' style='margin-top:5px' />" +

                                        "<img  alt='' src='" + prod_image[5] + "' class='img' style='margin-top:5px' /><img alt=''  src='" +

                                        prod_image[6] + "' class='img' style='margin-top:5px' /><img  alt='' src='" + prod_image[7] + "' " +

                                        "class='img' style='margin-top:5px' /><img alt=''  src='" + prod_image[8] + "' class='img' " +

                                        "style='margin-top:5px' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +

                                        "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" + prod_name + "</td>" +

                                        "</tr> <tr><th>Shirt Category</th><td>" + items[i].s_category + "</td></tr><tr><th>Shirt Color</th>" +

                                        "<td>" + items[i].s_shirtcolor + "</td></tr><tr><th>Button Style</th><td>" + items[i].s_buttoningstyle + "</td>" +

                                        "</tr></table></div></div><div class='col-md-2 productpricing'><input type='text' " +

                                        "class='form-control quantity_ip' readonly id='quantity" + i + "' value='" + items[i].quantity + "'/>" +

                                        "<div class='quantity_inc'><i class='fa fa-minus-circle increment_quantity' " +

                                        "onclick=incrementValue('" + items[i].user_id + "','Shirt','" + stock + "','quantity" + i + "','minus','" +

                                        i + "','" + items[i].cart_id + "')></i><i class='fa fa-plus-circle increment_quantity' " +

                                        "onclick=incrementValue('" + items[i].user_id + "','Shirt','" + stock + "','quantity" + i +

                                        "','plus','" + i + "','" + items[i].cart_id + "') ></i></div><input type='text' " +

                                        "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  />" +

                                        "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                        items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='button' value='" +

                                        meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin-top:20px;" +

                                        "width:100%' onclick='addToCart(" + i + ")'/></div></div>";

                                }

                                else if (product_type == "Custom Pant") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' />" +

                                        "<img alt=''  src='" + prod_image[1] + "' class='img'/><img  alt='' src='" + prod_image[2] +

                                        "' class='img' /><img alt=''  src='" + prod_image[3] + "' class='img'/><img alt=''  src='" +

                                        prod_image[4] + "' class='img' /></div><div class='col-md-10'>" +

                                        "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                        "Product Name </th><td>" + prod_name + "</td></tr> <tr><th>Pant Category</th><td>" + items[i].p_category +

                                        "</td></tr><tr><th>Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Back Pocket</th>" +

                                        "<td>" + items[i].p_backpocket + "</td></tr><tr><th>Belt Loop</th><td>" + items[i].p_beltloop + "</td>" +

                                        "</tr><tr><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +

                                        " readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +

                                        "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Pant','" + stock + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "')" +

                                        "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Pant','" + stock + "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')" +

                                        "></i></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden />" +
                                        " <div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                        items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='button' value='" +

                                        meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin-top:20px;" +

                                        "width:100%' onclick='addToCart(" + i + ")';/></div></div>";

                                    $("#cart").html(button + cartshow);

                                }

                                else if (product_type == "Custom Vest") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +

                                        "' class='img' /><img alt='' style='z-index:6' src='" + prod_image[1] + "' class='img'/>" +

                                        "<img style='z-index:6' alt='' src='" + prod_image[2] + "' class='img' /><img alt='' " +

                                        "style='z-index:4' src='" + prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +

                                        prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" + prod_image[5] + "' " +

                                        "class='img'/><img alt='' style='z-index:1' src='" + prod_image[6] + "' class='img'/></div>" +

                                        "<div class='col-md-10'>" +

                                        "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                        "Product Name </th><td>" + prod_name + "</td></tr> <tr><th>Vest Category</th><td>" + items[i].v_category +

                                        "</td></tr><tr><th>Vest Button</th><td>" + items[i].v_buttonstyle + "</td></tr><tr><th>Vest Bottom</th>" +

                                        "<td>" + items[i].v_bottomstyle + "</td></tr><tr><th>Collar Style</th><td>" + items[i].v_collarstyle + "</td>" +

                                        "</tr><tr><th>Lower Pocket</th><td>" + items[i].v_lowerpocket + "</td></tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +

                                        " readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +

                                        "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Vest','" + stock + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "')" +

                                        "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Vest','" + stock + "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')" +

                                        "></i></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden />" +
                                        "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                        items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='button' value='" +

                                        meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin-top:20px;" +

                                        "width:100%' onclick='addToCart(" + i + ")'/></div></div>";

                                    $("#cart").html(button + cartshow);

                                }

                                else if (product_type == "Custom Overcoat") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +

                                        "' class='img' /><img alt='' style='z-index:1' src='" + prod_image[1] + "' class='img'/>" +

                                        "<img style='z-index:2' alt='' src='" + prod_image[2] + "' class='img' /><img alt='' " +

                                        "style='z-index:3' src='" + prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +

                                        prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" + prod_image[5] + "' " +

                                        "class='img'/><img alt='' style='z-index:7' src='" + prod_image[6] + "' class='img'/></div><div class='col-md-10'>" +

                                        "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                        "Product Name </th><td>&nbsp;&nbsp;: " + prod_name +

                                        "</td></tr> <tr><th>Product Type</th><td>&nbsp;&nbsp;: " + items[i].product_type +

                                        "</td></tr><tr><th>Overcoat Category</th><td>&nbsp;&nbsp;: " + items[i].o_category + "</td>" +

                                        "</tr><tr><th>Overcoat Lapel</th><td>&nbsp;&nbsp;: " + items[i].o_lapelstyle + "</td>" +

                                        "</tr><tr><th>Overcoat Elbow Pad</th><td>&nbsp;&nbsp;: " + items[i].o_elbowpadstyle + "</td>" +

                                        "</tr><tr><th>Overcoat Canvas</th><td>&nbsp;&nbsp;: " + items[i].o_canvasstyle + "</td>" +

                                        "</tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +

                                        " readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +

                                        "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Overcoat','" + escape(items[i].o_canvasstyle) + "','quantity" + i + "','minus','" + i +

                                        "','" + items[i].cart_id + "')" +

                                        "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Overcoat','" + escape(items[i].o_canvasstyle) + "','quantity" + i + "','plus','" +

                                        i + "','" + items[i].cart_id + "')" +

                                        "></i></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden />" +

                                        "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                        items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='button' value='" +

                                        meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin-top:20px;" +

                                        "width:100%' onclick='addToCart(" + i + ")'/></div></div>";

                                    $("#cart").html(button + cartshow);

                                }

                                else if (product_type == "Accessories") {

                                    prod_image = items[i].product_image;

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +

                                        "accessinfo'><div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image + "' " +

                                        " class='accessinfoimg' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +

                                        "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" +

                                        prod_name + "</td></tr> <tr><th>Product Category</th><td>" + items[i].product_type +

                                        "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +

                                        "</tr><tr><th>RC Number</th><td>" + items[i].a_rc_no + "</td></tr></table></div>" +

                                        "<div class='col-md-1' style='text-align:right;padding-right:26px'></div></div><div " +

                                        "class='col-md-2'><input type='text' class='form-control quantity_ip' readonly id='quantity" + i

                                        + "' value='" + items[i].quantity + "'/><div class='quantity_inc'><i class='fa fa-minus-circle " +

                                        "increment_quantity' onclick=incrementValue('" + items[i].user_id + "','Accessories','" +

                                        escape(items[i].product_name) + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id +

                                        "')></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Accessories','" + escape(items[i].product_name) + "','quantity" + i + "','plus','" + i +

                                        "','" + items[i].cart_id + "')></i></div><input type='text' id='limit_reach" + i + "' value='" +

                                        items[i].quantity + "' hidden  /><div class='clear'></div><input class='form-control total_ip' " +

                                        "type='text' readonly value='" + items[i].cart_product_total_amount + "' id='total" + i + "'/>" +

                                        "<input type='button' value='" + meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "'" +

                                        "style='margin-top:20px;width:100%' onclick='addToCart(" + i + ")'/></div></div>";

                                    $("#cart").html(cartshow);

                                }
                                else if (product_type == "Collection") {

                                    prod_image = items[i].product_image;

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +

                                        "accessinfo'><div class='col-md-2' style='height:150px;overflow:hidden'><img  alt='Custom Fabric'" +

                                        " src='" + prod_image + "' style='width:150px' /></div><div class='col-md-10'>" +

                                        "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:38px;'><tr>" +

                                        "<th>Product Name </th><td>" + prod_name + "</td></tr><tr><th>Product Category</th><td>" +

                                        items[i].product_type + "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +

                                        "</tr><tr><th>RC Number</th><td>" + items[i].a_rc_no + "</td></tr></table></div></div><div " +

                                        "class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' readonly" +

                                        " id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'><i class='fa " +

                                        "fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +

                                        "','Collection','" + escape(items[i].a_category) + "','quantity" + i + "','minus','" + i + "','" +

                                        items[i].cart_id + "') ></i><i class='fa fa-plus-circle increment_quantity' " +

                                        "onclick=incrementValue('" + items[i].user_id + "','Collection','" + escape(items[i].a_category) +

                                        "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')></i></div> <div class='clear'></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity +

                                        "' hidden /><input class='form-control total_ip' type='text' readonly value='" + items[i].cart_product_total_amount + "' id='total" + i + "' />" +

                                        "<input type='button' value='" + meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "'" +

                                        "style='margin-top:20px;width:100%' onclick='addToCart(" + i + ")'/></div></div>";

                                    $("#cart").html(cartshow);

                                }
                            }
                            else if(reqType == "orderStatus"){
                                var order_status = items[i].order_status;
                                if(order_status == ""){
                                    order_status = "Pending";
                                }
                                var paymentStatus = "<p class='paymentStatus'>Payment Status: <span>"+order_status+
                                    "</span></p>";

                                if (product_type == "Custom Suit") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img alt='Custom Fabric' src='" + prod_image[0] + "' class='img' style='z-index:0' />" +

                                        "<img alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' /><img  alt='' src='" + prod_image[2] +

                                        "' class='img' style='z-index:3' /><img alt=''  src='" + prod_image[3] + "' class='img' style='z-index:3' />" +

                                        "<img  alt='' src='" + prod_image[4] + "' class='img' style='z-index:4' /><img alt=''  src='" + prod_image[5] +

                                        "' class='img' style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +

                                        "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[8] +

                                        "' class='img' style='z-index:2' /></div><div class='col-md-10'><table style='font-family: " +

                                        "trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>Product Name </th><td>" +

                                        prod_name + "</td></tr> <tr><th>Selected Buttons </th><td>" + items[i].j_frontbutton + "</td></tr>" +

                                        "<tr><th>Selected Vents</th><td>" + items[i].j_backvent + "</td></tr><tr><th>Breast Pocket</th><td>" +

                                        items[i].j_breastpocket + "</td></tr><tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td>" +

                                        "</tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption + "</td></tr></table></div>" +

                                        "</div> <div class='col-md-2 productpricing'>" +paymentStatus+
                                        "</div></div>";

                                }
                                else if (product_type == "Custom 2Pc Suit") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' " +

                                        "style='z-index:0' /><img  alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' />" +

                                        "<img alt=''  src='" + prod_image[2] + "' class='img' style='z-index:3' /><img  alt='' src='" +

                                        prod_image[3] + "' class='img' style='z-index:3' /><img  alt='' src='" + prod_image[4] +

                                        "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[5] + "' class='img' " +

                                        "style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +

                                        "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" +

                                        prod_image[8] + "' class='img' style='z-index:2' /></div><div class='col-md-10'>" +

                                        "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr>" +

                                        "<th class=''>Suit Name </th><td>" + prod_name + "</td><th>Suit Type</th><td>Custom 2Pc Suit</td>" +

                                        "</tr><tr><th>Suit Buttons </th><td>" + items[i].j_frontbutton + "</td><th>Pant Category</th><td>" +

                                        items[i].p_category + "</td></tr><tr><th>Suit Vents</th><td>" + items[i].j_backvent + "</td><th>" +

                                        "Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Breast Pocket</th><td>" +

                                        items[i].j_breastpocket + "</td><th>Back Pocket</th><td>" + items[i].p_backpocket + "</td></tr>" +

                                        "<tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td><th>Belt Loop</th><td>" +

                                        items[i].p_beltloop + "</td></tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption +

                                        "</td><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'>"+paymentStatus +
                                        "</div></div>";

                                }

                                else if (product_type == "Custom Shirt") {

                                    stock = 5;

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'" +

                                        "><div class='col-md-2'><img  alt='Custom Fabric' style='margin-top:5px' src='" +

                                        prod_image[0] + "' class='img' /><img  alt='' src='" + prod_image[1] + "' class='img' " +

                                        "style='margin-top:5px'/><img src='" + prod_image[2] + "' class='img' " +

                                        "style='z-index:2;margin-top:5px' /><img src='" + prod_image[3] + "' class='img' " +

                                        "style='margin-top:5px' /><img  alt='' src='" + prod_image[4] + "' class='img' style='margin-top:5px' />" +

                                        "<img  alt='' src='" + prod_image[5] + "' class='img' style='margin-top:5px' /><img alt=''  src='" +

                                        prod_image[6] + "' class='img' style='margin-top:5px' /><img  alt='' src='" + prod_image[7] + "' " +

                                        "class='img' style='margin-top:5px' /><img alt=''  src='" + prod_image[8] + "' class='img' " +

                                        "style='margin-top:5px' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +

                                        "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" + prod_name + "</td>" +

                                        "</tr> <tr><th>Shirt Category</th><td>" + items[i].s_category + "</td></tr><tr><th>Shirt Color</th>" +

                                        "<td>" + items[i].s_shirtcolor + "</td></tr><tr><th>Button Style</th><td>" + items[i].s_buttoningstyle + "</td>" +

                                        "</tr></table></div></div><div class='col-md-2 productpricing'>" +paymentStatus+
                                        "</div></div>";

                                }

                                else if (product_type == "Custom Pant") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' />" +

                                        "<img alt=''  src='" + prod_image[1] + "' class='img'/><img  alt='' src='" + prod_image[2] +

                                        "' class='img' /><img alt=''  src='" + prod_image[3] + "' class='img'/><img alt=''  src='" +

                                        prod_image[4] + "' class='img' /></div><div class='col-md-10'>" +

                                        "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                        "Product Name </th><td>" + prod_name + "</td></tr> <tr><th>Pant Category</th><td>" + items[i].p_category +

                                        "</td></tr><tr><th>Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Back Pocket</th>" +

                                        "<td>" + items[i].p_backpocket + "</td></tr><tr><th>Belt Loop</th><td>" + items[i].p_beltloop + "</td>" +

                                        "</tr><tr><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'>" +paymentStatus+
                                        "</div></div>";

                                    $("#cart").html(button + cartshow);

                                }

                                else if (product_type == "Custom Vest") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +

                                        "' class='img' /><img alt='' style='z-index:6' src='" + prod_image[1] + "' class='img'/>" +

                                        "<img style='z-index:6' alt='' src='" + prod_image[2] + "' class='img' /><img alt='' " +

                                        "style='z-index:4' src='" + prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +

                                        prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" + prod_image[5] + "' " +

                                        "class='img'/><img alt='' style='z-index:1' src='" + prod_image[6] + "' class='img'/></div>" +

                                        "<div class='col-md-10'>" +

                                        "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                        "Product Name </th><td>" + prod_name + "</td></tr> <tr><th>Vest Category</th><td>" + items[i].v_category +

                                        "</td></tr><tr><th>Vest Button</th><td>" + items[i].v_buttonstyle + "</td></tr><tr><th>Vest Bottom</th>" +

                                        "<td>" + items[i].v_bottomstyle + "</td></tr><tr><th>Collar Style</th><td>" + items[i].v_collarstyle + "</td>" +

                                        "</tr><tr><th>Lower Pocket</th><td>" + items[i].v_lowerpocket + "</td></tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'>" +paymentStatus+
                                        "</div></div>";

                                    $("#cart").html(button + cartshow);

                                }

                                else if (product_type == "Custom Overcoat") {

                                    prod_image = prod_image.split(",");

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                        "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +

                                        "' class='img' /><img alt='' style='z-index:1' src='" + prod_image[1] + "' class='img'/>" +

                                        "<img style='z-index:2' alt='' src='" + prod_image[2] + "' class='img' /><img alt='' " +

                                        "style='z-index:3' src='" + prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +

                                        prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" + prod_image[5] + "' " +

                                        "class='img'/><img alt='' style='z-index:7' src='" + prod_image[6] + "' class='img'/></div><div class='col-md-10'>" +

                                        "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                        "Product Name </th><td>&nbsp;&nbsp;: " + prod_name +

                                        "</td></tr> <tr><th>Product Type</th><td>&nbsp;&nbsp;: " + items[i].product_type +

                                        "</td></tr><tr><th>Overcoat Category</th><td>&nbsp;&nbsp;: " + items[i].o_category + "</td>" +

                                        "</tr><tr><th>Overcoat Lapel</th><td>&nbsp;&nbsp;: " + items[i].o_lapelstyle + "</td>" +

                                        "</tr><tr><th>Overcoat Elbow Pad</th><td>&nbsp;&nbsp;: " + items[i].o_elbowpadstyle + "</td>" +

                                        "</tr><tr><th>Overcoat Canvas</th><td>&nbsp;&nbsp;: " + items[i].o_canvasstyle + "</td>" +

                                        "</tr></table></div>" +

                                        "</div><div class='col-md-2 productpricing'>" +paymentStatus+
                                        "</div></div>";

                                    $("#cart").html(button + cartshow);

                                }

                                else if (product_type == "Accessories") {

                                    prod_image = items[i].product_image;

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +

                                        "accessinfo'><div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image + "' " +

                                        " class='accessinfoimg' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +

                                        "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" +

                                        prod_name + "</td></tr> <tr><th>Product Category</th><td>" + items[i].product_type +

                                        "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +

                                        "</tr><tr><th>RC Number</th><td>" + items[i].a_rc_no + "</td></tr></table></div>" +

                                        "<div class='col-md-1' style='text-align:right;padding-right:26px'></div></div><div " +

                                        "class='col-md-2'>" +paymentStatus+
                                        "</div></div>";

                                    $("#cart").html(cartshow);

                                }
                                else if (product_type == "Collection") {

                                    prod_image = items[i].product_image;

                                    cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +

                                        "accessinfo'><div class='col-md-2' style='height:150px;overflow:hidden'><img  alt='Custom Fabric'" +

                                        " src='" + prod_image + "' style='width:150px' /></div><div class='col-md-10'>" +

                                        "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:38px;'><tr>" +

                                        "<th>Product Name </th><td>" + prod_name + "</td></tr><tr><th>Product Category</th><td>" +

                                        items[i].product_type + "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +

                                        "</tr><tr><th>RC Number</th><td>" + items[i].a_rc_no + "</td></tr></table></div></div><div " +

                                        "class='col-md-2 productpricing'>" + paymentStatus+
                                        "</div></div>";

                                    $("#cart").html(cartshow);

                                }
                            }

                        }

                        $("#cart").html(cartshow);

                        totalamount = totalamount.toFixed(1);

                        var url = "admin/webserver/apply_coupon.php?type=isCouponApplied&user_id=" + user_id;

                        $.get(url, function (data) {

                            var json = $.parseJSON(data);

                            var status = json.status;

                            if (status == "done") {

                                var resp = json.message;

                                totalamount = totalamount - ((totalamount * resp) / 100)

                            }

                            $("#finalcartsubtotal").val("$" + totalamount);

                            $(".finalpaymentcartlabel2").val("$" + ((parseFloat(totalamount) * 7.25) / 100).toFixed(2));

                            $("#totalcartsubtotal").val("$" + (parseFloat(totalamount) + ((parseFloat(totalamount) * 7.25) / 100)).toFixed(2));

                            $("#paymentconfirm").html("$" + totalamount);

                            $("#totalpaymentconfirm").html("$" + (parseFloat(totalamount) + ((parseFloat(totalamount) * 7.25) / 100)).toFixed(2));

                            $(".loader").fadeOut();

                        });

                    }

                    else {

//                    document.getElementById("applycoupondiv").style.display = "none";

//                    document.getElementById("finalpaymentcart").style.display = "none";

                        cartshow = cartshow + "<p class='nodata'>Cart Is Empty !!!!</p>";

                        document.getElementById("totalpaymentcart").style.display = "none";

                        document.getElementById("placeorder").style.display = "none";

                        $("#placeorder").attr("disabled", true);

                        $("#placeorder").css({"opacity": "0.5"});

                        $(".loader").fadeOut();

                        $("#cart").html(cartshow);

                    }

                }

                else {

//                document.getElementById("applycoupondiv").style.display = "none";

//                document.getElementById("finalpaymentcart").style.display = "none";

                    cartshow = cartshow + "<p class='nodata'>"+message+"</p>";

//                    document.getElementById("totalpaymentcart").style.display = "none";

//                    document.getElementById("placeorder").style.display = "none";
                    $(".loader").hide();
                    $("#cart").html(cartshow);

                }


            });
        }


        function searchData (key) {
            var url = 'api/recommendProcess.php';
            var search_txt = $("#search_txt").val();
            if(key !=='text') {
                search_txt = $("#search-type").val();
                key = search_txt;
                if(search_txt === 'Accessories') {
                    search_txt = $("#search-type :selected").text();
                }
            }


            if(search_txt === '') {
                $(".modal-title").html("<label style='color:green'>Search Error !!!</label>");

                $(".modal-body").html("<h5>Please enter product name to search</h5>");

                $("#modal").modal("show");
                return false;
            }

            $(".loader").show();
            $.post(url,{userId:user_id,type:"searchRecommends","searchText":search_txt,"searchKey":key}, function(data) {
                $(".loader").hide();
                var status = data.Status;

                var button = "";

                var cartshow = "";

                if (status === "Success") {

                    $("#clear_cart").css("display","block");

                    var items = data.data;

                    var totalamount = 0;

                    if(items.length > 0) {
                        recommend_data = [];
                        for (var i = 0; i < items.length; i++) {
                            recommend_data.push(items[i]);
                            totalamount = parseFloat(totalamount) + parseFloat(items[i].cart_product_total_amount);

                            var prod_name = items[i].product_name;

                            var product_type = items[i].product_type;

                            var prod_image = items[i].product_image;

                            var meas_name = "Add To Cart";

                            if(items[i].meas_name != ""){

                                meas_name = items[i].meas_name;

                            }

                            if (product_type == "Custom Suit") {

                                prod_image = prod_image.split(",");

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                    "<div class='col-md-2'><img alt='Custom Fabric' src='" + prod_image[0] + "' class='img' style='z-index:0' />" +

                                    "<img alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' /><img  alt='' src='" + prod_image[2] +

                                    "' class='img' style='z-index:3' /><img alt=''  src='" + prod_image[3] + "' class='img' style='z-index:3' />" +

                                    "<img  alt='' src='" + prod_image[4] + "' class='img' style='z-index:4' /><img alt=''  src='" + prod_image[5] +

                                    "' class='img' style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +

                                    "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[8] +

                                    "' class='img' style='z-index:2' /></div><div class='col-md-10'><table style='font-family: " +

                                    "trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>Product Name </th><td>" +

                                    prod_name + "</td></tr> <tr><th>Selected Buttons </th><td>"+items[i].j_frontbutton + "</td></tr>" +

                                    "<tr><th>Selected Vents</th><td>"+items[i].j_backvent + "</td></tr><tr><th>Breast Pocket</th><td>" +

                                    items[i].j_breastpocket + "</td></tr><tr><th>Lower Pocket</th><td>"+items[i].j_lowerpocket + "</td>" +

                                    "</tr><tr><th>Suit Canvas</th><td>"+items[i].j_canvasoption + "</td></tr></table></div>" +

                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +

                                    "readonly id='quantity"+i+"' value='"+items[i].quantity+"'/><div class='quantity_inc'>" +

                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Jacket','" +escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i +

                                    "','" + items[i].cart_id + "') ></i><i class='fa fa-plus-circle increment_quantity' " +

                                    "onclick=incrementValue('"+items[i].user_id+"','Jacket','" + escape(items[i].j_canvasoption) +

                                    "','quantity" + i + "','plus','"+i + "','" + items[i].cart_id + "')></i></div><input type='text'" +

                                    " id='limit_reach"+i+"' value='"+items[i].quantity + "' hidden  />" +

                                    "<div class='clear'></div><input class='form-control total_ip' type='text' readonly " +

                                    "value='"+items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text' " +

                                    "hidden id='stock_value' value='" + stock + "' /><input type='button' value='"+meas_name+"' " +

                                    "class='btn btn-danger' id='cartmeas"+items[i].cart_id+"' style='margin-top:20px;width:100%' " +

                                    " onclick='addToCart("+i+")' /></div></div>";

                            }else if (product_type == "Custom 2Pc Suit") {

                                prod_image = prod_image.split(",");

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>"+

                                    "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' " +

                                    "style='z-index:0' /><img  alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' />" +

                                    "<img alt=''  src='" + prod_image[2] +"' class='img' style='z-index:3' /><img  alt='' src='" +

                                    prod_image[3] + "' class='img' style='z-index:3' /><img  alt='' src='" + prod_image[4] +

                                    "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[5] +"' class='img' " +

                                    "style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +

                                    "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" +

                                    prod_image[8] +"' class='img' style='z-index:2' /></div><div class='col-md-10'>" +

                                    "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr>" +

                                    "<th class=''>Suit Name </th><td>"+prod_name+"</td><th>Suit Type</th><td>Custom 2Pc Suit</td>" +

                                    "</tr><tr><th>Suit Buttons </th><td>"+items[i].j_frontbutton + "</td><th>Pant Category</th><td>"+

                                    items[i].p_category+ "</td></tr><tr><th>Suit Vents</th><td>" +items[i].j_backvent + "</td><th>" +

                                    "Front Pocket</th><td>" + items[i].p_frontpocket+"</td></tr><tr><th>Breast Pocket</th><td>" +

                                    items[i].j_breastpocket + "</td><th>Back Pocket</th><td>"+items[i].p_backpocket +"</td></tr>" +

                                    "<tr><th>Lower Pocket</th><td>"+items[i].j_lowerpocket + "</td><th>Belt Loop</th><td>" +

                                    items[i].p_beltloop + "</td></tr><tr><th>Suit Canvas</th><td>"+items[i].j_canvasoption +

                                    "</td><th>Pant Bottom</th><td>"+items[i].p_pantbottom + "</td></tr></table></div>" +

                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +

                                    "readonly id='quantity"+i+"' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +

                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','2PcSuit','"+escape(items[i].j_canvasoption)+"','quantity"+i + "','minus','" + i + "','" +

                                    items[i].cart_id + "')></i><i class='fa fa-plus-circle increment_quantity' " +

                                    "onclick=incrementValue('"+items[i].user_id+"','2PcSuit','" + escape(items[i].j_canvasoption) +

                                    "','quantity" + i + "','plus','" + i + "','"+items[i].cart_id+"')></i></div><input type='text'" +

                                    "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  />"+
                                    "<div class='clear'></div><input class='form-control total_ip' type='text' readonly " +

                                    "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text'" +

                                    "id='stock_value' value='" + stock + "' hidden  /><input type='button' value='"+meas_name+

                                    "' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"' style='margin-top:20px;width:100%'" +

                                    " onclick='addToCart("+i+")' /></div></div>";

                            }

                            else if (product_type == "Custom Shirt") {

                                stock = 5;

                                prod_image = prod_image.split(",");

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'" +

                                    "><div class='col-md-2'><img  alt='Custom Fabric' style='margin-top:5px' src='" +

                                    prod_image[0] + "' class='img' /><img  alt='' src='" + prod_image[1] + "' class='img' " +

                                    "style='margin-top:5px'/><img src='" + prod_image[2] + "' class='img' " +

                                    "style='z-index:2;margin-top:5px' /><img src='" + prod_image[3] + "' class='img' " +

                                    "style='margin-top:5px' /><img  alt='' src='" + prod_image[4]+"' class='img' style='margin-top:5px' />" +

                                    "<img  alt='' src='" + prod_image[5] + "' class='img' style='margin-top:5px' /><img alt=''  src='" +

                                    prod_image[6]+"' class='img' style='margin-top:5px' /><img  alt='' src='" + prod_image[7] + "' " +

                                    "class='img' style='margin-top:5px' /><img alt=''  src='" + prod_image[8]+"' class='img' " +

                                    "style='margin-top:5px' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +

                                    "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>"+prod_name+"</td>" +

                                    "</tr> <tr><th>Shirt Category</th><td>"+items[i].s_category+"</td></tr><tr><th>Shirt Color</th>" +

                                    "<td>"+items[i].s_shirtcolor+"</td></tr><tr><th>Button Style</th><td>"+items[i].s_buttoningstyle+"</td>" +

                                    "</tr></table></div></div><div class='col-md-2 productpricing'><input type='text' " +

                                    "class='form-control quantity_ip' readonly id='quantity"+i+"' value='"+items[i].quantity+"'/>" +

                                    "<div class='quantity_inc'><i class='fa fa-minus-circle increment_quantity' " +

                                    "onclick=incrementValue('"+items[i].user_id+"','Shirt','"+stock + "','quantity" + i + "','minus','" +

                                    i + "','" + items[i].cart_id+"')></i><i class='fa fa-plus-circle increment_quantity' " +

                                    "onclick=incrementValue('"+items[i].user_id+"','Shirt','" + stock+"','quantity" + i +

                                    "','plus','" + i + "','" + items[i].cart_id + "') ></i></div><input type='text' " +

                                    "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  />" +

                                    "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                    items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='button' value='"+

                                    meas_name+"' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"' style='margin-top:20px;" +

                                    "width:100%' onclick='addToCart("+i+")' /></div></div>";

                            }

                            else if (product_type == "Custom Pant") {

                                prod_image = prod_image.split(",");

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                    "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' />" +

                                    "<img alt=''  src='"+prod_image[1] + "' class='img'/><img  alt='' src='" + prod_image[2] +

                                    "' class='img' /><img alt=''  src='" +prod_image[3] + "' class='img'/><img alt=''  src='" +

                                    prod_image[4] + "' class='img' /></div><div class='col-md-10'>" +

                                    "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                    "Product Name </th><td>"+prod_name+"</td></tr> <tr><th>Pant Category</th><td>"+items[i].p_category+

                                    "</td></tr><tr><th>Front Pocket</th><td>"+items[i].p_frontpocket+"</td></tr><tr><th>Back Pocket</th>" +

                                    "<td>"+items[i].p_backpocket+"</td></tr><tr><th>Belt Loop</th><td>"+items[i].p_beltloop+"</td>"+

                                    "</tr><tr><th>Pant Bottom</th><td>"+items[i].p_pantbottom+"</td></tr></table></div>" +

                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +

                                    " readonly id='quantity"+i+"' value='"+items[i].quantity+"'/><div class='quantity_inc'>" +

                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Pant','" + stock + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "')" +

                                    "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Pant','" + stock + "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')" +

                                    "></i></div><input type='text' id='limit_reach"+i+"' value='"+items[i].quantity+"' hidden />" +
                                    " <div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                    items[i].cart_product_total_amount + "' id='total"+i+"' /><input type='button' value='"+

                                    meas_name+"' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"' style='margin-top:20px;" +

                                    "width:100%' onclick='addToCart("+i+")'/></div></div>";

                                $("#cart").html(button + cartshow);

                            }

                            else if (product_type == "Custom Vest") {

                                prod_image = prod_image.split(",");

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                    "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +

                                    "' class='img' /><img alt='' style='z-index:6' src='"+prod_image[1] + "' class='img'/>" +

                                    "<img style='z-index:6' alt='' src='" + prod_image[2] +"' class='img' /><img alt='' " +

                                    "style='z-index:4' src='" +prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +

                                    prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" +prod_image[5] + "' " +

                                    "class='img'/><img alt='' style='z-index:1' src='" +prod_image[6] + "' class='img'/></div>" +

                                    "<div class='col-md-10'>" +

                                    "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                    "Product Name </th><td>"+prod_name+"</td></tr> <tr><th>Vest Category</th><td>"+items[i].v_category+

                                    "</td></tr><tr><th>Vest Button</th><td>"+items[i].v_buttonstyle+"</td></tr><tr><th>Vest Bottom</th>" +

                                    "<td>"+items[i].v_bottomstyle+"</td></tr><tr><th>Collar Style</th><td>"+items[i].v_collarstyle+"</td>"+

                                    "</tr><tr><th>Lower Pocket</th><td>"+items[i].v_lowerpocket+"</td></tr></table></div>" +

                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +

                                    " readonly id='quantity"+i+"' value='"+items[i].quantity+"'/><div class='quantity_inc'>" +

                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Vest','" + stock + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "')" +

                                    "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Vest','" + stock + "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "')" +

                                    "></i></div><input type='text' id='limit_reach"+i+"' value='"+items[i].quantity+"' hidden />" +
                                    "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                    items[i].cart_product_total_amount + "' id='total"+i+"' /><input type='button' value='"+

                                    meas_name+"' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"' style='margin-top:20px;" +

                                    "width:100%' onclick='addToCart("+i+")' /></div></div>";

                                $("#cart").html(button + cartshow);

                            }

                            else if (product_type == "Custom Overcoat") {

                                prod_image = prod_image.split(",");

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +

                                    "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +

                                    "' class='img' /><img alt='' style='z-index:1' src='"+prod_image[1] + "' class='img'/>" +

                                    "<img style='z-index:2' alt='' src='" + prod_image[2] +"' class='img' /><img alt='' " +

                                    "style='z-index:3' src='" +prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +

                                    prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" +prod_image[5] + "' " +

                                    "class='img'/><img alt='' style='z-index:7' src='" +prod_image[6] + "' class='img'/></div><div class='col-md-10'>" +

                                    "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +

                                    "Product Name </th><td>&nbsp;&nbsp;: " + prod_name +

                                    "</td></tr> <tr><th>Product Type</th><td>&nbsp;&nbsp;: " + items[i].product_type+

                                    "</td></tr><tr><th>Overcoat Category</th><td>&nbsp;&nbsp;: " + items[i].o_category+ "</td>" +

                                    "</tr><tr><th>Overcoat Lapel</th><td>&nbsp;&nbsp;: " + items[i].o_lapelstyle + "</td>" +

                                    "</tr><tr><th>Overcoat Elbow Pad</th><td>&nbsp;&nbsp;: " + items[i].o_elbowpadstyle + "</td>" +

                                    "</tr><tr><th>Overcoat Canvas</th><td>&nbsp;&nbsp;: " + items[i].o_canvasstyle + "</td>" +

                                    "</tr></table></div>" +

                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +

                                    " readonly id='quantity"+i+"' value='"+items[i].quantity+"'/><div class='quantity_inc'>" +

                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Overcoat','" + escape(items[i].o_canvasstyle) + "','quantity" + i + "','minus','" + i +

                                    "','" + items[i].cart_id + "')" +

                                    "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Overcoat','" + escape(items[i].o_canvasstyle) + "','quantity" + i + "','plus','" +

                                    i + "','" + items[i].cart_id + "')" +

                                    "></i></div><input type='text' id='limit_reach"+i+"' value='"+items[i].quantity+"' hidden />" +

                                    "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +

                                    items[i].cart_product_total_amount + "' id='total"+i+"' /><input type='button' value='"+

                                    meas_name+"' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"' style='margin-top:20px;" +

                                    "width:100%' onclick='addToCart("+i+")' /></div></div>";

                                $("#cart").html(button + cartshow);

                            }

                            else if (product_type == "Accessories") {

                                prod_image = items[i].product_image;

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +

                                    "accessinfo'><div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image + "' " +

                                    " class='accessinfoimg' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +

                                    "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" +

                                    prod_name + "</td></tr> <tr><th>Product Category</th><td>" + items[i].product_type +

                                    "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +

                                    "</tr><tr><th>RC Number</th><td>"+items[i].a_rc_no + "</td></tr></table></div>" +

                                    "<div class='col-md-1' style='text-align:right;padding-right:26px'></div></div><div " +

                                    "class='col-md-2'><input type='text' class='form-control quantity_ip' readonly id='quantity"+i

                                    +"' value='"+items[i].quantity+"'/><div class='quantity_inc'><i class='fa fa-minus-circle " +

                                    "increment_quantity' onclick=incrementValue('"+items[i].user_id+"','Accessories','"+

                                    escape(items[i].product_name)+"','quantity" + i + "','minus','" + i + "','" + items[i].cart_id +

                                    "')></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Accessories','" +escape(items[i].product_name) + "','quantity" + i + "','plus','" + i +

                                    "','" + items[i].cart_id +"')></i></div><input type='text' id='limit_reach" + i + "' value='" +

                                    items[i].quantity +"' hidden  /><div class='clear'></div><input class='form-control total_ip' " +

                                    "type='text' readonly value='" + items[i].cart_product_total_amount + "' id='total"+i+"'/>" +

                                    "<input type='button' value='"+meas_name+"' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"'" +

                                    "style='margin-top:20px;width:100%' onclick='addToCart("+i+")' /></div></div>";

                                $("#cart").html(cartshow);

                            }else if (product_type == "Collection") {

                                prod_image = items[i].product_image;

                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +

                                    "accessinfo'><div class='col-md-2' style='height:150px;overflow:hidden'><img  alt='Custom Fabric'" +

                                    " src='" + prod_image + "' style='width:150px' /></div><div class='col-md-10'>" +

                                    "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:38px;'><tr>" +

                                    "<th>Product Name </th><td>"+prod_name + "</td></tr><tr><th>Product Category</th><td>"+

                                    items[i].product_type+"</td></tr><tr><th>Specification</th><td>"+items[i].a_specification+"</td>" +

                                    "</tr><tr><th>RC Number</th><td>"+items[i].a_rc_no+"</td></tr></table></div></div><div " +

                                    "class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' readonly" +

                                    " id='quantity"+i+"' value='"+items[i].quantity+"'/><div class='quantity_inc'><i class='fa " +

                                    "fa-minus-circle increment_quantity' onclick=incrementValue('"+items[i].user_id+

                                    "','Collection','" + escape(items[i].a_category) +"','quantity" + i + "','minus','" + i + "','" +

                                    items[i].cart_id + "') ></i><i class='fa fa-plus-circle increment_quantity' " +

                                    "onclick=incrementValue('"+items[i].user_id+"','Collection','" +escape(items[i].a_category) +

                                    "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id +"')></i></div> <div class='clear'></div><input type='text' id='limit_reach"+i+"' value='" +items[i].quantity +

                                    "' hidden /><input class='form-control total_ip' type='text' readonly value='" + items[i].cart_product_total_amount + "' id='total" + i + "' />" +

                                    "<input type='button' value='"+meas_name+"' class='btn btn-danger' id='cartmeas"+items[i].cart_id+"'" +

                                    "style='margin-top:20px;width:100%' onclick='addToCart("+i+")' /></div></div>";

                                $("#cart").html(cartshow);

                            }

                        }

                        $("#cart").html(cartshow);

                        totalamount = totalamount.toFixed(1);

                        var url = "admin/webserver/apply_coupon.php?type=isCouponApplied&user_id="+user_id;

                        $.get(url, function (data) {

                            var json = $.parseJSON(data);

                            var status = json.status;

                            if (status == "done") {

                                var resp = json.message;

                                totalamount = totalamount - ((totalamount * resp) / 100)

                            }

                            $("#finalcartsubtotal").val("$" + totalamount);

                            $(".finalpaymentcartlabel2").val("$"+((parseFloat(totalamount)*7.25)/100).toFixed(2));

                            $("#totalcartsubtotal").val("$" + (parseFloat(totalamount) + ((parseFloat(totalamount)*7.25)/100)).toFixed(2));

                            $("#paymentconfirm").html("$" + totalamount);

                            $("#totalpaymentconfirm").html("$" + (parseFloat(totalamount) + ((parseFloat(totalamount)*7.25)/100)).toFixed(2));

                            $(".loader").fadeOut();

                        });

                    }

                    else {

//                        document.getElementById("applycoupondiv").style.display = "none";

//                        document.getElementById("finalpaymentcart").style.display = "none";

                        cartshow = cartshow+"<p class='nodata'>No searched result found !!!!</p>";

//                        document.getElementById("totalpaymentcart").style.display = "none";

//                        document.getElementById("placeorder").style.display = "none";

                        $("#placeorder").attr("disabled", true);

                        $("#placeorder").css({"opacity": "0.5"});

                        $(".loader").fadeOut();

                        $("#cart").html(cartshow);

                    }

                }

                else{

//                    document.getElementById("applycoupondiv").style.display = "none";

//                    document.getElementById("finalpaymentcart").style.display = "none";

                    cartshow = cartshow+"<p class='nodata'>No searched result found !!!!</p>";

//                    document.getElementById("totalpaymentcart").style.display = "none";

//                    document.getElementById("placeorder").style.display = "none";

                    $("#placeorder").attr("disabled", true);

                    $("#placeorder").css({"opacity": "0.5"});

                    $(".loader").fadeOut();

                    $("#cart").html(cartshow);

                }


            });
        }

        function backon_1() {

            var path = window.location.href;

            if (path.indexOf("?") >= 1) {

                path = path.split("?");

                var temp = path[1].split("=");

                if (temp[0] == "stp" && temp[1] == "2" || temp[0] == "stp" && temp[1] == "3") {

                    window.location = "cart.php?stp=1";

                }

            }

        }

        function backon_2() {

            var path = window.location.href;

            if (path.indexOf("?") >= 2) {

                path = path.split("?");

                var temp = path[1].split("=");

                if (temp[0] == "stp" && temp[1] == "3") {

                    window.location = "cart.php?stp=2";

                }

            }

        }

        function continue_shoping() {

            window.location = "customize.php";

        }

        function addToCart(position) {

            console.log('cart id --- '+recommend_data[position].cart_id);
            var user_id = $("#user_id").val();
            var url = "api/recommendProcess.php";
            $.post(url,{'type':'addToCart','data':JSON.stringify(recommend_data[position]),'userId':user_id},function(data){
                console.log('data ---- '+JSON.stringify(data));
                var status = data.Status;
                if(status === "Success") {
                    window.location = 'cart.php';
                }
                else if(status === 'Failure') {
                    $(".modal-title").html("<label style='color:green'>Cart Error !!!</label>");

                    $(".modal-body").html("<h5>"+data.Message+"</h5>");

                    $("#modal").modal("show");
                }
            });
        }
        function incrementValue(user_id,type,stock,cart_qid,opt,row_id,cart_id) {

            if(opt == "plus") {

                if (type == "Shirt") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    if (quantity > stock) {

                        $(".modal-title").html("<label style='color:green'>Quantity Limit Reach !!!</label>");

                        $(".modal-body").html("<h5>We have only " + stock + " Items Available in Stock.</h5>");

                        $("#modal").modal("show");

                        return false;

                    }

                    var cat_name = "Shirt, " + quantity + " piece";

                    var url = "admin/webserver/get_price.php?type=shirt&cat_name=" + cat_name;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Jacket") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var cat_name = "";

                    switch(unescape(stock)){

                        case "C-000A:full_canvas":

                            cat_name = "Full Canvas Jacket";

                            break;

                        case "C-000B:half_canvas":

                            cat_name = "Half Canvas Jacket";

                            break;

                        case "C-00C1:fused":

                            cat_name = "Fused Canvas Jacket";

                            break;

                        case "C-00C3:deconstructed_suits_or_soft_suit":

                            cat_name = "Half Canvas Jacket";

                            break;

                    }

                    var url = "admin/webserver/get_price.php?type=jacket&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "2PcSuit") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var cat_name = unescape(stock);

                    var cat_name = "";

                    switch(unescape(stock)){

                        case "C-000A:full_canvas":

                            cat_name = "Full Canvas 2pc suit";

                            break;

                        case "C-000B:half_canvas":

                            cat_name = "Half Canvas 2pcs suit";

                            break;

                        case "C-00C1:fused":

                            cat_name = "Fused suit 2pcd suit";

                            break;

                        default :

                            cat_name = "Half Canvas 2pcs suit";

                            break;

                    }

                    var url = "admin/webserver/get_price.php?type=2pcsuit&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Pant") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var cat_name = "Pants";

                    var url = "admin/webserver/get_price.php?type=pant&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Vest") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var url = "admin/webserver/get_price.php?type=pant&cat_name=High-end vest&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Overcoat") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var cat_name = "";

                    stock = unescape(stock);

                    switch(stock){

                        case "D-000A:full_canvas":

                            cat_name = "Full canvas Overcoat";

                            break;

                        case "D-000B:half_canvas":

                            cat_name = "Half canvas overcoat";

                            break;

                        case "D-00C1:fused":

                            cat_name = "Fused overcoat";

                            break;

                    }

                    var url = "admin/webserver/get_price.php?type=overcoat&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Accessories") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var accessory_name = stock;

                    var cat_name = accessory_name.split("_")[0];

                    var url = "admin/webserver/get_price.php?type=accessories&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#"+cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Collection") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity + 1;

                    var cat_name = stock;

                    var url = "admin/webserver/get_price.php?type=accessories&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#"+cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

            }

            else{

                if (type == "Shirt") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity - 1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = "Shirt, " + quantity + " piece";

                    var url = "admin/webserver/get_price.php?type=shirt&cat_name="+cat_name;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Jacket") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity-1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = "";

                    switch(unescape(stock)){

                        case "C-000A:full_canvas":

                            cat_name = "Full Canvas Jacket";

                            break;

                        case "C-000B:half_canvas":

                            cat_name = "Half Canvas Jacket";

                            break;

                        case "C-00C1:fused":

                            cat_name = "Fused Canvas Jacket";

                            break;

                        case "C-00C3:deconstructed_suits_or_soft_suit":

                            cat_name = "Half Canvas Jacket";

                            break;

                    }

                    var url = "admin/webserver/get_price.php?type=jacket&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "2PcSuit") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity-1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = "";

                    switch(unescape(stock)){

                        case "C-000A:full_canvas":

                            cat_name = "Full Canvas 2pc suit";

                            break;

                        case "C-000B:half_canvas":

                            cat_name = "Half Canvas 2pcs suit";

                            break;

                        case "C-00C1:fused":

                            cat_name = "Fused suit 2pcd suit";

                            break;

                        default :

                            cat_name = "Half Canvas 2pcs suit";

                            break;

                    }



                    var url = "admin/webserver/get_price.php?type=2pcsuit&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Pant") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity-1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = "Pants";

                    var url = "admin/webserver/get_price.php?type=pant&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Vest") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity-1;

                    if (quantity < 1) {

                        return false;

                    }

                    var url = "admin/webserver/get_price.php?type=pant&cat_name=High-end vest&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Overcoat") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity-1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = "";

                    stock = unescape(stock);

                    switch(stock){

                        case "D-000A:full_canvas":

                            cat_name = "Full canvas Overcoat";

                            break;

                        case "D-000B:half_canvas":

                            cat_name = "Half canvas overcoat";

                            break;

                        case "D-00C1:fused":

                            cat_name = "Fused overcoat";

                            break;

                    }

                    var url = "admin/webserver/get_price.php?type=overcoat&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }else if (type == "Accessories") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity - 1;

                    if (quantity < 1) {

                        return false;

                    }

                    var accessory_name = stock;

                    var cat_name = accessory_name.split("_")[0];

                    var url = "admin/webserver/get_price.php?type=accessories&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Collection") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity - 1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = stock;

                    var url = "admin/webserver/get_price.php?type=accessories&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

                else if (type == "Custom Overcoat") {

                    var quantity = parseInt($("#" + cart_qid).val());

                    quantity = quantity - 1;

                    if (quantity < 1) {

                        return false;

                    }

                    var cat_name = stock;

                    var url = "admin/webserver/get_price.php?type=customOvercoat&cat_name="+cat_name+"&quantity="+quantity;

                    $.get(url, function (datas) {

                        var json = $.parseJSON(datas);

                        var status = json.status;

                        if (status == "done") {

                            var amounts = json.amount;

                            var prod_total = amounts.total_amount;

                            $("#" + cart_qid).val(quantity);

                            $("#total" + row_id).val(prod_total);

                            increment_cart_value(quantity, prod_total, cart_id,user_id);

                        }

                    });

                }

            }

        }

        function increment_cart_value(quantity,prod_total,cart_id,user_id){

            var url = "admin/webserver/update_cart.php?cart_id="+cart_id+"&product_quantity="+quantity+"&product_total="+prod_total;

            $.get(url, function (data) {

                var json = $.parseJSON(data)

                var status = json.status;

                if (status == "done") {

                    var url = "admin/webserver/apply_coupon.php?type=getfullcartcount&user_id="+user_id;

                    $.get(url, function (data) {

                        var json = $.parseJSON(data)

                        var status = json.status;

                        if (status == "done") {

                            var subtotal = json.message;

                            var url3 = "admin/webserver/apply_coupon.php?type=isCouponApplied&user_id="+user_id;

                            $.get(url3, function (data3) {

                                var json3 = $.parseJSON(data3);

                                var status3 = json3.status;

                                if(status3 == "done"){

                                    var resp = json3.message;

                                    subtotal = subtotal - ((subtotal * resp) / 100)

                                }

                                $("#finalcartsubtotal").val("$" + subtotal);

                                var currentTax = (parseFloat(subtotal) * parseFloat(7.25))/100;

                                var finalwithtax = (parseFloat(subtotal) + parseFloat(currentTax)).toFixed(2);

                                $(".finalpaymentcartlabel2").val("$" + currentTax.toFixed(2));

                                $("#totalcartsubtotal").val("$" + finalwithtax);

                            });

                        }else{

                            $(".modal-title").html("<label style='color:red'>Error Occured</label>");

                            $(".modal-body").html("Server Error !!! Please Try After Some Time");

                            $("#modal").modal("show");

                        }

                    });

                }

                else{

                    $(".modal-title").html("<label style='color:red'>Error Occured</label>");

                    $(".modal-body").html(json.message);

                    $("#modal").modal("show");

                }

            }).fail(function () {

                $(".modal-title").html("<label style='color:red'>Error Occured</label>");

                $(".modal-body").html("Something Went Wrong !!! Please Try After Some Time");

                $("#modal").modal("show");

            });

        }

        var path = window.location.href;

        if (path.indexOf("?") >= 0) {

            path = path.split("?");

            var temp = path[1].split("=");

            if(temp[0] == "stp" && temp[1] == "2"){

                /*$("#selectedMeasurement").val(meas[1]);*/

                $("#optioncart").removeClass("optionnumboxactive");

                $("#paymentcart").addClass("optionnumboxactive");

                $("#cart").css({"display": "none"});

                $("#applycoupondiv").css({"display": "none"});

                $("#finalpaymentcart").css({"display": "none"});

                $("#totalpaymentcart").css({"display": "none"});

                $("#stp1_buttons").css({"display": "none"});

                $("#step2_div").css({"display": "block"});

                $("#make_payment").css({"display": "block"});

                userPersonal();

            }

            else{

                $("#paymentcart").removeClass("optionnumboxactive");

                $("#optioncart").addClass("optionnumboxactive");

                $("#make_payment").css({"display": "none"});

            }

        }

        else {

            $("#paymentcart").removeClass("optionnumboxactive");

            $("#optioncart").addClass("optionnumboxactive");

            $("#make_payment").css({"display": "none"});

        }

        function step_2_task() {

            window.location = "cart.php?stp=2";

        }

        function userPersonal() {

            var user_id = $("#user_id").val();

            var url = "admin/webserver/register.php?type=get_billing&user_id=" + user_id;

            $.get(url, function (data) {

                var json = $.parseJSON(data);

                var status = json.status;

                var items = json.items;

                if (status == "done") {

                    var item = items[0];

                    $("#countryId").append("<option selected='selected'>" + item.country + "</option>");

                    $("#user_id").val(item.user_id);

                    $("#first_name").val(item.first_name);

                    $("#last_name").val(item.last_name);

                    $("#address").val(item.address);

                    $("#stateId").html("<option selected='selected'>" + item.state + "</option>");

                    $("#postalcode").val(item.pincode);

                    $("#phonenumber").val(item.mobile);

                    setTimeout(function(){

                        $("#total_am").html(parseFloat($("#finalcartsubtotal").val().split("$")[1]));

                        $("#tax").html("$"+((parseFloat($("#finalcartsubtotal").val().split("$")[1]) *7.25)/100).toFixed(2));

                        $("#grand_total").html(parseFloat(parseFloat($("#finalcartsubtotal").val().split("$")[1])+ ((parseFloat($("#finalcartsubtotal").val().split("$")[1]) *7.25)/100)).toFixed(2));

                    },500);

                    $("#cityId").append("<option selected='selected'>" + item.city + "</option>")

                }

            });

        }

        //shipping and billing process end here

        //payment confirmation process start here

        function selectcartMeasurement(cart_id){

            $(".loader").fadeIn("slow");

            var url = "api/registerUser.php";

            $.post(url,{"type":"getAllMeasurement","user_id":user_id},function(data){

                var Status = data.Status;

                if(Status == "Success"){

                    var measurements = data.measurements;

                    var datashow = "<div class='row'><div class='container-fluid'><table class='table table-bordered'><tr><th>S.No</th><th>Measurement Name</th>" +

                        "<th>Added On</th><th>Action</th>";

                    for(var i=0;i<measurements.length;i++){

                        var date = new Date(measurements[i].added_on * 1000);

                        var year    = date.getFullYear();

                        var month   = date.getMonth()+1;

                        var day     = date.getDate();

                        var added_on = day+"/"+month+"/"+year;

                        datashow += "<tr><td>"+(i+1)+"</td><td>"+measurements[i].meas_name+"</td><td>"+added_on+"</td>" +

                            "<td><input type='button' value='Select' class='btn btn-danger btn-sm' " +

                            "onclick=finalSelection('"+cart_id+"','"+measurements[i].measurement_id+"','"+escape(measurements[i].meas_name)+"') />" +

                            "<i class='fa fa-trash' onclick=deletemeas('"+measurements[i].measurement_id+"') " +

                            "style='cursor:pointer;margin-left:20px; color:#CC3834;font-size:17px;position:absolute;margin-top:6px'>" +

                            "</i></td></tr>";

                    }

                    datashow+="</table></div></div>";

                    $(".modal-title").html("<span style='color:green'>Select Measurement File</span>");

                    $(".modal-body").html(datashow);

                    $(".modal-footer").css("display","none");

                    $("#modal").modal("show");

                }

                else{

                    showMessage("Not Any Measurement Available Yet !!! Please Save Atleast One Measurement First","red");

                    setTimeout(function(){

                        window.location="sizematching.php";

                    },3000);

                }

            }).fail(function(){

                alert("Server Error !!! Please Try After Some Time");

            });

            $(".loader").fadeOut("slow");

        }

        function deletemeas(measurement_id){

            var url = "api/registerUser.php";

            $.post(url,{"type":"deleteMeasurement","meas_id":measurement_id},function(data){

                var Status = data.Status;

                if(Status == "Success"){

                    $("#modal").modal("hide");

                    setTimeout(function(){

                        selectMeasurement();

                    },500);

                }else{

                    alert("Server Error !!! Please Try After Some Time");

                }

            }).fail(function(){

                alert("Server Error !!! Please Try After Some Time");

            });

        }

        function finalSelection(cart_id,measurement_id,meas_name){

            $("#selectedMeasurement").val(measurement_id);

            var url='api/registerUser.php';

            meas_name = unescape(meas_name);

            $.post(url,{"type":"addCartMeasurement","meas_id":measurement_id,"cart_id":cart_id,"meas_name":meas_name},function(data){

                var Status = data.Status;

                if(Status == "Success"){

                    $("#modal").modal("hide");

                    $("#cartmeas"+cart_id).val(meas_name);

                }else{

                    $("#modal").modal("hide");

                    setTimeout(function(){

                        showMessage(data.Message,"red");

                    },1000);

                }

            }).fail(function(){

                $("#modal").modal("hide");

                setTimeout(function(){

                    showMessage("Server Error !!! Please Try After Some Time","red");

                },1000)

            });

            //step_2_task(measurement_id);

        }

        //update_shipping_address();

        function update_shipping_address() {

            $(".loader").fadeIn("slow");

            var user_id = $("#user_id").val();

            var billing_fname = $("#first_name").val();

            var billing_lname = $("#last_name").val();

            var address = $("#address").val();

            var country = $("#countryId").val();

            var state = $("#stateId").val();

            var city = $("#cityId").val();

            var pincode = $("#postalcode").val();

            var phone = $("#phonenumber").val();

            var billing_address = address;

            if (billing_fname == "" || billing_lname == "" || address == "" || country == "" || state == ""

                || city == "" || pincode == "" || phone == "") {

                $(".loader").fadeOut("slow");

                showMessage("Please Fill All the Required Feilds...","red");

            }else {

                var url = "api/registerUser.php";

                $.post(url,{"type":"getAllMeasurement","user_id":user_id},function(data) {

                    var Status = data.Status;

                    if (Status == "Success") {

                        var url="admin/webserver/register.php?type=billing&user_id="+user_id+"&billing_fname="+billing_fname+

                            "&billing_lname="+billing_lname+"&billing_address="+billing_address+"&country="+country+"&state="+

                            state+"&city="+city+"&pincode="+pincode+"&phone="+phone;

                        $.get(url, function (data) {

                            var json = $.parseJSON(data);

                            var status = json.status;

                            if (status == "done") {

                                var subtotal = parseFloat($("#finalcartsubtotal").val().split("$")[1]);

                                var coupon_code='';

                                var url3 = "admin/webserver/apply_coupon.php?type=isCouponApplied&user_id="+user_id;

                                $.get(url3, function (data3) {

                                    var json3 = $.parseJSON(data3);

                                    var status3 = json3.status;

                                    if (status3 == "done") {

                                        coupon_code = json3.coupon_code;

                                    }

                                    subtotal = (subtotal + (((subtotal * 7.25))/100)).toFixed(2);

                                    order_detail(user_id, subtotal,escape(coupon_code));

                                });

                            }

                        });

                    } else {

                        showMessage("Not Any Measurement Available Yet !!! Please Save Atleast One Measurement First","red");

                        setTimeout(function(){

                            window.location="sizematching.php";

                        },3000);

                    }

                });



            }

        }

        function order_detail(user_id, total, couponCode) {

            couponCode = unescape(couponCode);

            var json = '{"user_id":"' + user_id + '","couponCode":"' + couponCode + '","total":"' + total + '"}';

            var url = "admin/webserver/get_orders.php?id=3&json=" + json;

            $.get(url, function (data) {

                var json = $.parseJSON(data);

                var status = json.status;

                if (status == "done") {

                    $(".loader").fadeOut("slow");

                    var order_id = json.order_id;

                    window.open("paynow.php?order_id="+order_id+"&order_amount="+total);

                    window.location="index.php";

                }

                else {

                    alert(json.message);

                }

            });

        }

        function deletecart(cart_id) {

            $(".modal-title").html("<label style='color:green'>Confirm Status !!!</label>");

            $(".modal-body").html("<h5>Are you sure want to delete this item.</h5>");

            $(".modal-footer").css("display","block");

            $(".modal-footer").html("<input type='button' class='btn btn-warning' value='Confirm' onclick=confirm_deletecart('" + cart_id + "') />");

            $("#modal").modal("show");

        }

        function confirm_deletecart(cart_id) {

            var url = "admin/webserver/clear_cart.php?type=remove&cart_id=" + cart_id;

            $.get(url, function (data) {

                var json = $.parseJSON(data);

                var status = json.status;

                if (status == "done") {

                    window.location = "cart.php?stp=1";

                }

                else {

                    $(".modal-title").html("<label style='color:red'>Error Occured</label>");

                    $(".modal-body").html(json.message);

                    $("#modal").modal("show");

                }

            });

        }

        //clear all cart items

        function clear_cart(user_id) {

            $(".modal-title").html("<label style='color:green'>Confirm Status !!!</label>");

            $(".modal-body").html("<h5>Are you sure want to delete all items in the cart.</h5>");

            $(".modal-footer").html("<input type='button' class='btn btn-warning' value='Confirm' onclick=confirm_clear_cart('" + user_id + "') />");

            $("#modal").modal("show");

        }

        function confirm_clear_cart(user_id) {

            var url = "admin/webserver/clear_cart.php?type=clear&user_id=" + user_id;

            $.get(url, function (data) {

                var json = $.parseJSON(data);

                var status = json.status;

                if (status == "done") {

                    window.location = "cart.php?stp=1";

                }

                else {

                    $(".modal-title").html("<label style='color:red'>Error Occured</label>");

                    $(".modal-body").html(json.message);

                    $("#modal").modal("show");

                }

            });

        }

        setTimeout(getCart(), 3000);

        function applyCoupon(){

            var couponCode = $("#validcoupon").val();

            var user_id = $("#user_id").val();

            var url = "admin/webserver/apply_coupon.php?couponCode=" + couponCode +"&user_id="+user_id;

            $.get(url, function (data) {

                var json = $.parseJSON(data);

                var status = json.status;

                if (status == "done") {

                    var price = json.message;

                    var total = $("#finalcartsubtotal").val().split("$")[1];

                    var discount = (total*price)/100;

                    total = total-discount;

                    $("#finalcartsubtotal").val("$"+total);

                    var totalwithtax = (total+((total*7.25)/100)).toFixed(2);

                    $("#totalcartsubtotal").val("$"+totalwithtax);

                    $(".modal-title").html("<label style='color:green'>Success</label>");

                    $(".modal-body").html("Congratulations !!! you Got "+price+"% Discount by Applying Code "+couponCode.toUpperCase());

                    $("#modal").modal("show");

                }

                else {

                    $(".modal-title").html("<label style='color:red'>Error Occured</label>");

                    $(".modal-body").html(json.status);

                    $("#modal").modal("show");

                }

            });

        }

        function isCouponApplied(user_id){

            var url = "admin/webserver/apply_coupon.php?type=isCouponApplied&user_id="+user_id;

            $.get(url, function (data) {

                var json = $.parseJSON(data);

                var status = json.status;

                var resp = "false";

                if(status == "done"){

                    resp = json.message;

                }

                return resp;

            });

        }

        getRecommendsData('orderStatus');
    </script>

</div>

<div class="clearfix"></div>

<?php

include('footer.php');

?>