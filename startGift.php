<?php
include "header.php";
include "api/Constants/dbConfig.php";
include "api/Classes/CONNECT.php";
?>
    <style>
        .headingstartshirt{
            font-size: 19px;
            font-weight: bold;
            margin-bottom: 20px;
            margin-left: 15px;
            text-align: left;
            text-decoration: underline;
        }
        .col-md-6 > li {
            font-size: 18px;
            list-style: square;
        }
        html.js {
            opacity: 1;
        }
        .grid li {
            width: 350px !important;
            padding: 10px !important;
            margin:2px!important;
        }
        .cs-style-4 figcaption {
            width: 70% !important;
        }
    </style>
    <?php
    $conn = new \Modals\CONNECT();
    $link = $conn->Connect();
    $paypal_url = 'https://www.sandbox.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
    ?>
    <link rel="stylesheet" type="text/css" href="css/component1.css" />
    <script src="js/modernizr.custom1.js"></script>

    <div class="col-md-12" style="margin-top:150px;text-align:center;">
        <p style="color: #555;font-size:30px;background: wheat;">SFTAILOR GIFT CERTIFICATE &nbsp; <img src="admins/images/gift_marvel.gif" style="height:68px;width:84px;margin-top: -16px;">
        </p>
        <hr>
            <div class="container demo-3">
                <ul class="grid cs-style-4">
                    <?php
                    $query = mysqli_query($link,"select * from wp_gift_certificate where status = '1'");
                    if($query){
                        while($rows = mysqli_fetch_assoc($query)) { ?>
                    <form id="myForm" name="myForm" action="<?php echo $paypal_url; ?>" method="post">

                    <li style="background: url('images/gift_pattern.jpg');height: 245px;background-repeat: no-repeat;">
                        <figure>
                            <div style="background: url(admins/api/Files/images/giftbox/<?php echo $rows['gift_image'];?>);
                                    background-position: center center;background-size: 100% auto;height: 100%;width: 100%;background-repeat: no-repeat"></div>
                            <figcaption>
                                <h3>Gift For : <?php echo $rows['gift_for'];?></h3>
                                <div style="min-height: 100px;">Specification : <?php echo substr($rows['gift_description'],0,110);?></div>
                                <div style="width: 200px;">
                                    <h2 style="color: white;float: left;font-size: 24px;">PRICE :$<?php echo $rows['gift_price'];?></h2>
                                    <input style="background: #ed4e6e;border: 1px solid #ed4e6e;color: white;margin-left: 6%;border-radius: 2px;padding: 4px 8px;" name="submit" value="BUY NOW" id="paybtn" type="submit">

                                </div>
                            </figcaption>
                        </figure>
                    </li>
                        <input name="business" value="daisyvc@sftailors.com" type="hidden">
                        <!-- Specify a Buy Now button. -->
                        <input name="cmd" value="_xclick" type="hidden">
                        <!--    <input type="hidden" name="cmd" value="_cart">-->
                        <!-- Specify details about the item that buyers will purchase. -->
                        <input name="item_name" value="Orders" type="hidden">
                        <input name="item_number" value="<?php echo $rows['g_id'];?>" type="hidden">
                        <input name="amount" value="<?php echo $rows['gift_price'];?> " type="hidden">
                        <input name="currency_code" value="USD" type="hidden">
                        <!-- Specify URLs -->
                        <input type='hidden' name='cancel_return' value="http://scan2fit.com/sftailor/admin/webserver/payment_gift.php?payment_status=Failed&order_id=<?php echo $rows['g_id'];?>&paid_amount=<?php echo $rows['gift_price'];?>" />
                        <input name="return" value="http://scan2fit.com/sftailor/admin/webserver/payment_gift.php?payment_status=Success&order_id=<?php echo $rows['g_id'];?>&paid_amount=<?php echo $rows['gift_price'];?>" type="hidden"/>
                        <!-- Display the payment button. -->
                    </form>
                    <?php } } ?>

                </ul>
            </div><!-- /container -->
            <script src="js/toucheffects1.js"></script>
        <div class="col-md-1"></div>
    </div>



        <!-- Identify your business so that you can collect the payments. -->

<?php
include "footer.php";
?>