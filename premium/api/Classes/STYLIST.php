<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/23/2017
 * Time: 3:46 PM
 */
namespace Classes;
require_once('STORECONNECT.php');
class STYLIST{
    public $link = null;
    public $response = array();
    function __construct(){
        $this->link = new STORECONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
        session_start();
    }
    public function storeLogin($user_email,$password){
        $link = $this->link->connect();
        if ($link){
            $query = "SELECT * FROM wp_store WHERE user_email='$user_email' AND store_password='$password'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){  //To check if the row exists
                    $this->response[STATUS]=Success;
                    $this->response[MESSAGE]="Login Success";
                    $rows = mysqli_fetch_assoc($result);
                    $_SESSION['SFT_Store_Id']=$rows['store_id'];
                    $_SESSION['SFT_Store_Name']=$rows['store_name'];
                    $_SESSION['SFT_Admin_Email']=$rows['user_email'];
                }
                else{
                    $this->response[STATUS]=Error;
                    $this->response[MESSAGE]="Invalid Credentials";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }////ok
    public function getAdminEmail(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from admin_login";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_array($result);
                    $adminEmail = $row['admin_email'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Email Found";
                    $this->response['admin_email'] = $adminEmail;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function changeAdminPassword($oldPassword,$newPassword,$adminId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from admin_login where admin_id = '$adminId' and admin_password='$oldPassword'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE admin_login SET admin_password='$newPassword' WHERE 
                    admin_password='$oldPassword' and admin_id='$adminId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Old Password";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function changeForgotPassword($newPassword,$admin_email){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from admin_login where admin_email = '$admin_email'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE admin_login SET admin_password='$newPassword' WHERE admin_email='$admin_email'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Old Password";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function sendMaill($admin_email)
    {
        require 'SMTP/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'md-in-68.webhostbox.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@stsmentor.com';
        $mail->Password = 'noreply@sts';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom('noreply@stsmentor.com', 'Bolti Kitaab');   // sender
        $mail->addAddress($admin_email);
        //$mail->addCC('sbsunilbhatia9@gmail.com');				//to add cc
        $mail->addAttachment('');         // Add attachments
        $mail->addAttachment('');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'noreply@Bolti Kitaab';
        $mail->Body='<div style="width:100%;background:#ccc;padding-bottom:36px"><div style="width:80%;margin: 0 10%">
        <div style="background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px"><img style="float:left;
        height:50px" src="" alt="Bolti Kitaab" />
        <p style="float: left;color:#666;font-size:15px;font-weight:bold;margin-left:10px">Hello ! Admin</p>
        </div><div style="background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px">
        <p style="color:green;line-height:40px;font-weight:bold;font-size:15px">Greetings of the Day,</p>
        <p>Welcome To STS Mentor Family</p><p>Here Is The Change Password link . Click here to Change your Account Password.</p>
        <a style="text-decoration:none" href="'.MainServer.'/admin/api/admin_login.php?_='.$admin_email.'" >
        <input type="button" style="background:#d14130;border-radius:3px;color: white;font-weight:600;margin: 10px 0;
        padding: 10px 20px" value="Change Password" /></a>
        <div style="background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px">
        <p style="font-weight:bold;color:#666">Thanks & Regards<br><br>Bolti Kitaab Team</p>
        </div></div></div>';
        $mail->AltBody = '';
        if(!$mail->send())
        {
            return $mail->ErrorInfo;
        }
        else {
            return "Email has been sent Successfully to your registered email address";
        }
    }
	
	public function getAllMenus(){
		$link = $this->link->connect();
		if ($link){
			$query = "select * from wp_menu";
			$result = mysqli_query($link, $query);
			if ($result) {
				$num = mysqli_num_rows($result);
				if ($num > 0) {
						 while($rows = mysqli_fetch_array($result)){
							$menuData[]=array(
							  "menu_id"=>$rows["menu_id"],
							  "menu_name"=>$rows["menu_name"],
							  "menu_link"=>$rows["menu_link"]
							);
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Menu Found";
                    $this->response['menuData'] = $menuData;
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = $this->link->sqlError();
			}
		}
		else{
			$this->response[STATUS] = Error;
			$this->response[MESSAGE] = $this->link->sqlError();
		}
		return $this->response;
	}
	public function getPerticularAdminData($admin_id){
		$link = $this->link->connect();
		if ($link){
			$query = "select * from admin_login where status='1' AND admin_id='$admin_id'";
			$result = mysqli_query($link, $query);
			if ($result) {
				$num = mysqli_num_rows($result);
				if ($num > 0) {
						$adminData = mysqli_fetch_assoc($result);
						$this->response[STATUS] = Success;
						$this->response[MESSAGE] = "Admin User";
						$this->response['adminData'] = $adminData;
                    }
                    
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = $this->link->sqlError();
			}
		
		return $this->response;
	}
	public function getAllAdminData(){
		$link = $this->link->connect();
		if ($link){
			$query = "select * from admin_login where status='1' AND admin_level !='Super Admin'";
			$result = mysqli_query($link, $query);
			if ($result) {
				$num = mysqli_num_rows($result);
				if ($num > 0) {
						 while($rows = mysqli_fetch_array($result)){
							$menuData[]=array(
							  "admin_id"=>$rows["admin_id"],
							  "admin_name"=>$rows["admin_name"],
							  "admin_email"=>$rows["admin_email"],
							  "admin_password"=>$rows["admin_password"],
							  "password2"=>$rows["password2"],
							  "admin_access_menu"=>$rows["admin_access_menu"],
							  "status"=>$rows["status"]
							);
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Admin Found";
                    $this->response['adminData'] = $menuData;
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = $this->link->sqlError();
			}
		}
		else{
			$this->response[STATUS] = Error;
			$this->response[MESSAGE] = $this->link->sqlError();
		}
		return $this->response;
	}
	public function addAdministratorData($adminName,$Email,$password,$addminAccess,$password2){
		$link = $this->link->connect();
		if ($link){
			 $query = "insert into admin_login (admin_name,admin_email,admin_password,admin_level,admin_access_menu,status,password2) values('$adminName','$Email','$password','simple admin','$addminAccess','1','$password2')";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Administrator Added successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Data";
			}
           
        return $this->response;
	}
	public function updateAdministratorData($adminName,$Email,$password,$addminAccess,$adminId,$password2){
		$link = $this->link->connect();
		if ($link){
			 $query = "update admin_login set admin_name='$adminName',admin_email='$Email',admin_password='$password',admin_access_menu='$addminAccess',password2='$password2' where admin_id='$adminId'";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Administrator Info updated successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Admin Data";
			}
           
        return $this->response;
	}
	
	public function delAdministratorData($adminId){
		$link = $this->link->connect();
		if ($link){
			 $query = "delete from admin_login where admin_id='$adminId'";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Administrator Deleted successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Admin Data";
			}
           
        return $this->response;
	}
	
	
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}
?>