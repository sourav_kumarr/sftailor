<?php
include('header.php');
?>
<style>
    .shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    #create_supplier{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 9999999;}
    #send_supplier_data{ background: white none repeat scroll 0 0;
        border-radius: 4px;
        left: 182px;
        padding: 37px 25px!important;
        top: 19px;
        z-index: 999999;}
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
    #emails {
        width:100%
    }
    .select2-selection.select2-selection--multiple {
        border-color: silver!important;
        border-radius: 0!important;
        height: auto!important;
        width: 565px!important;
    }
    .select2-results {
        width: 565px!important;
    }
    .select2-search__field {
        width: 180px !important;
    }
</style>
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<link rel="stylesheet" href="css/select2.min.css" type="text/css" media="all">

<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">

    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Email Manager <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <style>
                        #tags {
                            width:100%
                        }
                    </style>

                    <button style="float: right; margin-top: 40px;" class="btn btn-info btn-sm email" data-target="#create_template" data-toggle="modal">Template</button>
                    <button style="float: right; margin-top: 40px;" class="btn btn-info btn-sm email vieews" id="vieews" data-target="#view_template" data-toggle="modal">View</button>
                    <button style="float: right; margin-top: 40px;" class="btn btn-info btn-sm email" data-target="#addemployee" data-toggle="modal">Stylish</button>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Orders Generated
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <script src="js/jquery.min.js"></script>
                            <script src="js/location.js"></script>
                            <script src="js/bootstrap.min.js"></script>
                            <script src="js/moment.min.js"></script>
                            <script src="js/custom.min.js"></script>
                            <script src="js/jquery.dataTables.min.js"></script>
                            <script src="js/bootstrap-toggle.min.js"></script>
                            <script src="js/bootstrap-datetimepicker.js"></script>
                            <script src="js/myscript.js"></script>
                            <script src="../js/jquery-ui.js" type="text/javascript"></script>
                            <script type="text/javascript" src="js/select2.min.js"></script>
                            <?php

                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $query = "select * from wp_email_manager where category='employee' order by e_id desc";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($orderData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='Name'><?php echo $orderData['employee_name'];?></td>
                                                <td data-title='Price'><?php echo $orderData['employee_email'];?></td>
                                                <td data-title='Name'><?php echo $orderData['employee_contact'];?></td>
                                                <td data-title='Order Dated'>

                                                    <?php
                                                    if($orderData['employee_status'] =="Active"){
                                                        ?>
                                                        <span style="background: green;padding: 5px 10px;color: white;cursor: pointer;" onclick=selectEmployee('<?php echo $orderData['e_id'];?>','De-Active'); ><?php echo $orderData['employee_status'];?></span>
                                                    <?php } else { ?>
                                                        <span style="background: tomato;padding: 5px 10px;color: white;cursor: pointer;" onclick=selectEmployee('<?php echo $orderData['e_id'];?>','Active'); ><?php echo $orderData['employee_status'];?></span>
                                                    <?php } ?>


                                                </td>
                                                <td data-title='Action'>
                                                    <a href='#' onclick=editEmployee('<?php echo $orderData['e_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: forestgreen none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-pencil"></i> </a>
                                                    <a href='#' onclick=deleteRecord('<?php echo $orderData['e_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>







             <!--           <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Admin Name</th>
                                <th>Email</th>
                                <th>Contact</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
/*
                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $query = "select * from wp_email_manager where category='admin' order by e_id desc";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($orderData = mysqli_fetch_array($result)) {
                                            $j++;
                                            */?>
                                            <tr>
                                                <td data-title='#'><?php /*echo $j */?></td>
                                                <td data-title='Name'><?php /*echo $orderData['employee_name'];*/?></td>
                                                <td data-title='Price'><?php /*echo $orderData['employee_email'];*/?></td>
                                                <td data-title='Name'><?php /*echo $orderData['employee_contact'];*/?></td>
                                                <td data-title='Action'>
                                                    <a href='#' onclick=editEmployee('<?php /*echo $orderData['e_id'];*/?>') style="float: right; font-size: 18px; padding: 2px 4px; background: forestgreen none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-pencil"></i> </a>
                                                </td>
                                            </tr>
                                            <?php
/*                                        }
                                    }
                                }
                            }
                            */?>
                            </tbody>
                        </table>-->









                        <!-- Modal employee -->
                        <div class="modal fade" id="addemployee" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                        <h5 class="modal-title" id="exampleModalLabel">Add New Employee</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Employee Name</label></br>
                                            <input type="text" value="" id="employee_name" class="form-control" style="width:100%">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Add Email</label></br>
                                            <input type="text" value="" id="employee_email" class="form-control" multiple="multiple" style="width:100%">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Contact</label></br>
                                            <input type="text" value="" id="contact" class="form-control" multiple="multiple" style="width:100%">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Status</label></br>
                                            <select id="status" name="status" style="width: 35%;height: 33px;">
                                                <option value="Active">Active</option>
                                                <option value="De-Active">De-Active</option>
                                            </select>
                                        </div>


                                    </div></br></br>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="addEmployee()">Add</button>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Modal Template-->
                        <div class="modal fade" id="create_template" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                        <h5 class="modal-title" id="exampleModalLabel">Add New Template</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">

                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Template Name</label></br>
                                            <input type="text" value="" id="template_name" class="form-control" style="width:100%">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Subject</label></br>
                                            <input type="text" value="" id="template_subject" class="form-control" multiple="multiple" style="width:100%">
                                        </div>
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Message Box</label></br>
                                            <textarea id="message_box_template" id="message_box_template" style="width:100%"></textarea>
                                        </div>


                                    </div></br></br>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="createTemplate()">Create</button>
                                    </div>
                                </div>
                            </div>
                        </div>
        <!--edit template manager-->
                        <?php if($_REQUEST['id']) {
                            $id = $_REQUEST['id'];
                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $query = "select * from wp_email_manager where e_id='$id'";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $orderData = mysqli_fetch_array($result);

                            ?>
                            <div style="background: black;height: 100%;position: fixed;width: 100%;margin: 0;padding: 0;opacity: 0.7;left: 0;right: 0;top: 0;bottom: 0;z-index: 1;"></div>
                        <div class="modal-dialog fade-in" role="document" style="z-index: 2;top: -205px;">
                        <div class="modal-content">
                            <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                <h5 class="modal-title" id="exampleModalLabel">Update Template</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Template Name</label></br>
                                    <input type="text" value="<?php echo $orderData['template_name'];?>" id="template_name_up" class="form-control" style="width:100%">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Subject</label></br>
                                    <input type="text" value="<?php echo $orderData['template_subject'];?>" id="template_subject_up" class="form-control" multiple="multiple" style="width:100%">
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Message Box</label></br>
                                    <textarea id="template_message_up" style="width:100%"><?php echo $orderData['template_message'];?></textarea>
                                </div>
                                <input type="hidden" id="template_status1" value="<?php echo $orderData['template_status'];?>">


                            </div></br></br>
                            <div class="modal-footer">
                                <button type="button" onclick="redirects()" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" onclick=editTemplates('<?php echo $orderData['e_id'];?>')>Update</button>
                            </div>
                        </div>
                        </div>
                    <?php } } } } ?>

<!--edit employe manager-->
                        <?php if($_REQUEST['eml_id']) {
                            $id = $_REQUEST['eml_id'];
                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $query = "select * from wp_email_manager where e_id='$id'";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $orderData = mysqli_fetch_array($result);
                                        ?>
                                        ?>
                                        <div style="background: black;height: 100%;position: fixed;width: 100%;margin: 0;padding: 0;opacity: 0.7;left: 0;right: 0;top: 0;bottom: 0;z-index: 1;"></div>
                                        <div class="modal-dialog fade-in" role="document" style="z-index: 2;top: -305px;">
                                            <div class="modal-content">
                                                <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                                    <h5 class="modal-title" id="exampleModalLabel">Update Employee Record</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="redirects()">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">

                                                    <div class="form-group">
                                                        <label for="recipient-name" class="form-control-label">Employee Name</label></br>
                                                        <input type="text" value="<?php echo $orderData['employee_name'];?>" id="employee_name_up" class="form-control" style="width:100%">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="form-control-label">Email</label></br>
                                                        <input type="text" value="<?php echo $orderData['employee_email'];?>" id="employee_email_up" class="form-control" multiple="multiple" style="width:100%">
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="recipient-name" class="form-control-label">Contact</label></br>
                                                        <input type="text" value="<?php echo $orderData['employee_contact'];?>" id="contact_up" class="form-control" multiple="multiple" style="width:100%">
                                                    </div>
                                                    <input type="hidden" id="employee_status_up" value="<?php echo $orderData['employee_status'];?>">


                                                </div></br></br>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" onclick="redirects()">Close</button>
                                                    <button type="button" class="btn btn-primary" onclick=updateEmployee('<?php echo $orderData['e_id'];?>')>Update</button>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } } } } ?>


                        <!-- All Templates-->
                        <div class="modal fade" id="view_template" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                            <div class="modal-dialog" role="document" style="width: 60%;">
                                <div class="modal-content">
                                    <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                        <h5 class="modal-title" id="exampleModalLabel">Select One Template</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">


                                        <table id="orderTable" class="table table-striped table-bordered">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Template Name</th>
                                                <th>Template Subject</th>
                                                <th>Template Message</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php

                                            $link = $conn->connect();//for sftailor
                                            if ($link) {
                                                $query = "select * from wp_email_manager where category='templates' order by e_id desc";
                                                $result = mysqli_query($link, $query);
                                                if ($result) {
                                                    $num = mysqli_num_rows($result);
                                                    if ($num > 0) {
                                                        $j = 0;
                                                        while ($orderData = mysqli_fetch_array($result)) {
                                                            $j++;
                                                            ?>
                                                            <tr>
                                                                <td data-title='#'><?php echo $j ?></td>
                                                                <td data-title='Name'><?php echo $orderData['template_name'];?></td>
                                                                <td data-title='Price'><?php echo $orderData['template_subject'];?></td>
                                                                <td data-title='Name'><?php echo $orderData['template_message'];?></td>
                                                                <td data-title='Order Dated'>
                                                                    <?php
                                                                    if($orderData['template_status'] =="Active"){
                                                                    ?>
                                                                        <span style="background: green;padding: 5px 10px;color: white;cursor: pointer;" onclick=selectTemplate('<?php echo $orderData['e_id'];?>'); ><?php echo $orderData['template_status'];?></span>
                                                                     <?php } else { ?>
                                                                        <span style="background: tomato;padding: 5px 10px;color: white;cursor: pointer;" onclick=selectTemplate('<?php echo $orderData['e_id'];?>'); ><?php echo $orderData['template_status'];?></span>
                                                                     <?php } ?>

                                                                </td>
                                                                <td data-title='Action'>
                                                                    <a href='#' onclick=editTemplate('<?php echo $orderData['e_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: forestgreen none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-pencil"></i> </a>
                                                                    <a href='#' onclick=deleteRecord('<?php echo $orderData['e_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>
                                                                </td>
                                                            </tr>
                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                            ?>
                                            </tbody>
                                        </table>




                                    </div></br></br>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary" onclick="updateTemplate_Status()">Select</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="gwc_box" id="cartList" data-price="￥">
    <table class="gwc_tb productitem-view-table" width="100%" cellspacing="0"
           cellpadding="0" border="0"></table>
</div>

<!-- /page content -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css" rel="stylesheet" type="text/css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
<script>
    CKEDITOR.replace( 'message_box_template' );
    CKEDITOR.replace( 'template_message_up' );
    function addEmployee() {
        $(".loader").show();
        var employee_name = $("#employee_name").val();
        var employee_email = $("#employee_email").val();
        var contact = $("#contact").val();
        var status = $("#status").val();
        var url = "api/emanagerProcess.php";
        $.post(url,{"type":"addEmployee","employee_name":employee_name,"employee_email":employee_email,"contact":contact,"status":status}, function (data) {
            $(".loader").hide();
            window.location="emailmanager.php";
        });
    }
    function createTemplate() {
        $(".loader").show();
        var template_name = $("#template_name").val();
        var template_subject = $("#template_subject").val();
        var message_box_template = CKEDITOR.instances['message_box_template'].getData();
        var url = "api/emanagerProcess.php";
        $.post(url,{"type":"addTemplate","template_name":template_name,"template_subject":template_subject,"message_box_template":message_box_template,"status_template":"De-Active"}, function (data) {
            $(".loader").hide();
            window.location="emailmanager.php";
        });
    }
    function selectTemplate(template_id) {
        $(".loader").show();
        var url = "api/emanagerProcess.php";
        $.post(url,{"type":"selectTemplate","template_id":template_id}, function (data) {
            $(".loader").hide();
            window.location="emailmanager.php";
        });
    }
    function deleteRecord(e_id) {
        $(".loader").show();
        var url = "api/emanagerProcess.php";
        $.post(url,{"type":"deleteRecord","e_id":e_id}, function (data) {
            $(".loader").hide();
            window.location="emailmanager.php";
        });
    }
    function selectEmployee(employee_id,status) {
        $(".loader").show();
        var url = "api/emanagerProcess.php";
        $.post(url,{"type":"selectEmployee","employee_id":employee_id,"status":status}, function (data) {
            $(".loader").hide();
            window.location="emailmanager.php";
        });
    }
     function editTemplates(template_id) {
         $(".loader").show();
         var template_name = $("#template_name_up").val();
         var template_subject = $("#template_subject_up").val();
         var message_box_template = CKEDITOR.instances['template_message_up'].getData();
         var template_status1 = $("#template_status1").val();
         var url = "api/emanagerProcess.php";
         $.post(url,{"type":"updateTemplate","template_name":template_name,"template_subject":template_subject,"message_box_template":message_box_template,"status_template":template_status1,"template_id":template_id}, function (data) {
             $(".loader").hide();
             window.location="emailmanager.php";
         });
     }
    function updateEmployee(employee_id) {
        $(".loader").show();
        var employee_name_up = $("#employee_name_up").val();
        var employee_email_up = $("#employee_email_up").val();
        var contact_up = $("#contact_up").val();
        var employee_status_up = $("#employee_status_up").val();
        var url = "api/emanagerProcess.php";
        $.post(url,{"type":"updateEmployee","employee_name_up":employee_name_up,"employee_email_up":employee_email_up,"contact_up":contact_up,"employee_status_up":employee_status_up,"employee_id":employee_id}, function (data) {
            $(".loader").hide();
            window.location="emailmanager.php";
        });
    }
    orderListData();
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });


    function close_diolog() {
        $(".shadoows").hide();
        $("#send_supplier_data").hide();
    }
    function editTemplate(id) {
        window.location="emailmanager.php?id="+id;
    }
    function editEmployee(id) {
        window.location="emailmanager.php?eml_id="+id;
    }
    function redirects() {
        window.location="emailmanager.php";
    }
</script>
