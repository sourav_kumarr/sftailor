<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');

class EMAILMANAGERS
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }


    public function addEmployee($employee_name,$employee_email,$contact,$status)
    {
        $link = $this->link->connect();
        if ($link) {

             $query = "insert into wp_email_manager (employee_name,employee_email,employee_contact,employee_status,category) VALUES ('$employee_name','$employee_email','$contact','$status','employee')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Employee Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function addTemplate($template_name,$template_subject,$message_box_template,$status_template)
    {
        $link = $this->link->connect();
        if ($link) {

             $query = "insert into wp_email_manager (template_name,template_subject,template_message,template_status,category) VALUES ('$template_name','$template_subject','$message_box_template','$status_template','templates')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Template Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getEmailData($type)
    {
        $link = $this->link->connect();
        $data = array();
        if ($link) {

            $query = "select * from wp_email_manager where category='$type'";

            $result = mysqli_query($link, $query);
            if ($result) {
                $count = mysqli_num_rows($result);
                if($count === 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "email data not found ".$query;

                }
                else{
                  while($rows = mysqli_fetch_assoc($result)) {
                      $data[] = $rows;
                  }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "email data found";
                    $this->response["data"] = $data;

                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function updateTemplate($template_name,$template_subject,$message_box_template,$status_template,$tempId)
    {
        $link = $this->link->connect();
        if ($link) {

            $query = "update wp_email_manager set template_name='$template_name',template_subject='$template_subject',template_message='$message_box_template',template_status='$status_template' where e_id='$tempId' ";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Template update SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateEmployee($employee_name_up,$employee_email_up,$contact_up,$employee_status_up,$employee_id)
    {
        $link = $this->link->connect();
        if ($link) {

            $query = "update wp_email_manager set employee_name='$employee_name_up',employee_email='$employee_email_up',employee_contact='$contact_up',employee_status='$employee_status_up' where e_id='$employee_id' ";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Employee status update SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function selectTemplate($template_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update wp_email_manager set template_status='De-Active'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $query1 = "update wp_email_manager set template_status='Active' where e_id='$template_id'";
                mysqli_query($link, $query1);
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Template Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function selectEmployee($employee_id,$status)
    {
        $link = $this->link->connect();
        if ($link) {
            $query1 = "update wp_email_manager set employee_status='$status' where e_id='$employee_id'";
            mysqli_query($link, $query1);
            if ($result) {

                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Template Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function deleteRecord($e_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query1 = "DELETE FROM `wp_email_manager` WHERE e_id='$e_id'";
            $result = mysqli_query($link, $query1);
            if ($result) {

                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Successfully deleted";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}