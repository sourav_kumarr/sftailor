<?php
include_once 'header.php';
require_once('admins/api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
$getFeedbackData = $userClass->getFeedbackData();
?>
<!DOCTYPE html>
<html>
<style>

    .container{
        width:75%;
        margin:0 auto;
    }
    .left{
        width: 35%;
        float: left;
        margin-left: 10%;
        background: rgba(0,0,0,0.65);
        padding: 9px 3% 37px;
    }
    .left .fa{
        color:#fff;
        font-size: 20px;
        margin-right: 15px;
    }
    .name {
        margin-top: 10px;
    }
    .feedbackFields{
        padding:10px;
        background:transparent;
        border:none;
        border:1px solid #fff;
        outline:none;
        width:100%;
        color:#fff;
    }
    textarea {
        background: transparent;
        border: 1px solid white;
        height: 8rem;
        color: white;
        overflow: hidden;
        margin-top: 15px;
    }

    ::-webkit-input-placeholder { /* WebKit, Blink, Edge */
        color:    #fff;
    }
    :-moz-placeholder { /* Mozilla Firefox 4 to 18 */
        color:    #fff;
        opacity:  1;
    }
    ::-moz-placeholder { /* Mozilla Firefox 19+ */
        color:    #fff;
        opacity:  1;
    }
    :-ms-input-placeholder { /* Internet Explorer 10-11 */
        color:    #fff;
    }
    ::-ms-input-placeholder { /* Microsoft Edge */
        color:    #fff;
    }
    input[type="submit"] {
        padding: 13px 80px;
        background: #a07650;
        border: none;
        outline: none;

        color:#fff;
        transition: 0.5s all;
        -webkit-transition: 0.5s all;
        -moz-transition: 0.5s all;
        -o-transition: 0.5s all;
        -ms-transition: 0.5s all;
        cursor:pointer;
        font-family: 'open sans',sans-serif;
    }
    input[type="submit"]:hover{
        background:#000;
    }
    .email{
        margin:15px 0px;
    }
    .right-w3{
        width: 60%;
        float: left;
        /*background: rgba(0, 0, 0, 0.60);*/
        padding: 36px 2.5%;
        margin-left: -6%;
        /*overflow-y: auto;
        height: 475px;*/
    }
    .right-w3 p{
        color:#d6d5d5;
        font-size:16px;
        line-height:1.5px;
    }
    .right-w3 a{
        color: #d6d5d5;
        font-size: 14px;
    }
    .right-w3  h2{
        font-size:17px;
        margin-bottom:20px;
    }
    .fromUser{
        border-left: 1px solid #ccc;
        border-right: 1px solid #ccc;
        padding: 0 10px;
    }
    .nameUser{
        padding-right: 10px;
    }

    p.para {
        color:#333;
    }
    /*-- responsive --*/
    @media (max-width: 1440px){
        input[type="submit"] {
        }
    }
    @media (max-width: 1366px){
        input[type="submit"] {
        }
    }
    @media (max-width: 1280px){
        h1 {
            margin: 50px 0px 100px 0px;
        }
        input[type="submit"] {
        }
        .footer-w3l {
            margin-top: 135px;
            margin-bottom:20px;
        }
    }
    @media (max-width: 1080px){
        .container {
            width: 68%;
            margin: 0 auto;
        }
        input[type="text"] {
            width: 80%;
        }
    }
    @media (max-width: 1024px){
        .container {
            width: 71%;
            margin: 0 auto;
        }
        input[type="submit"] {
            padding: 13px 70px;
        }
    }
    @media (max-width: 991px){
        h1 {
            font-size: 45px;
        }
        .container {
            width: 73%;
            margin: 0 auto;
        }

    }
    @media (max-width: 966px){
        .container {
            width: 75%;
            margin: 0 auto;
        }
        input[type="text"] {
            width: 81%;
        }
    }
    @media (max-width: 900px){
        .container {
            width: 81%;
            margin: 0 auto;
        }
    }
    @media (max-width: 800px){
        input[type="text"] {
            width: 79%;
        }
        .right-w3 {
            padding: 20px 2.5%;
        }
        input[type="submit"] {
            padding: 13px 70px;
        }
    }
    @media (max-width: 768px){
        .left .fa {
            margin-right: 13px;
        }
        input[type="submit"] {
            margin: 50px 0 0 101px;
        }
    }
    @media (max-width: 736px){
        h1 {
            margin: 45px 0px 50px 0px;
        }
        .left .fa {
            margin-right: 10px;
        }
        .footer-w3l {
            margin-top: 50px;
        }
        input[type="submit"] {
            padding: 13px 61px;
        }
    }
    @media (max-width:667px){
        .left .fa {
            margin-right: 5px;
        }
        input[type="submit"] {
            padding: 13px 51px;
        }
    }
    @media (max-width:640px){
        .left .fa {
            font-size:19px;
        }
        h1 {
            margin: 45px 0px 45px 0px;
            font-size: 40px;
        }
        .right-w3 h2 {
            font-size: 25px;
        }
        input[type="submit"] {
            padding: 13px 46px;
        }
    }
    @media (max-width:600px){
        .left .fa {
            font-size: 16.8px;
        }
        input[type="submit"] {
            padding: 12px 40px;
        }
    }
    @media (max-width: 568px){
        h1 {
            margin: 39px 0px 40px 0px;
            font-size: 35px;
        }
        .right-w3 h2 {
            font-size: 23px;
        }
        .container {
            width: 86%;
            margin: 0 auto;
        }
    }
    @media (max-width: 480px){
        .right-w3 {
            padding: 30px 5%;
            width:90%;
        }
        .left {
            width: 100%;
            margin-left: 5%;
        }
        input[type="submit"] {
            padding: 12px 50px;
            margin: 50px 0 0 221px;
        }
    }
    @media (max-width: 414px){
        h1 {
            margin: 39px 0px 50px 0px;
            font-size: 31px;
        }
        .right-w3 h2 {
            font-size: 21px;
        }
        .left .fa {
            font-size: 18px;
        }
        input[type="submit"] {
        }
    }
    @media (max-width: 384px){
        h1 {
            font-size: 29px;
        }
        .right-w3 h2 {
            font-size: 19px;
        }
        input[type="submit"] {

        }
        .footer-w3l p {
            font-size: 13px;
        }
    }
    @media (max-width: 375px){
        h1 {
            font-size: 28px;
        }
    }
    @media (max-width: 320px){
        h1 {
            font-size: 24.3px;
            margin: 39px 0px 40px 0px;
        }
        input[type="text"] {
            width: 72%;
        }
        input[type="submit"] {
            padding: 10px 35px;
        }
    }
    #af-submit-image-1418525566 {
        margin-left: 1px;
        margin-top: 1px;
    }
    .feedbackHeading{
        font-size: 24px!important;
        text-transform: uppercase;
        font-weight: 600;
    }
    .feedbackDescription{
        line-height: 1.5;
        color: #333!important;
        font-size: 20px;
    }
    .feedbackDescription {
        line-height: 1.5 !important;
        font-size: 20px;
    }
    input[type="submit"]{
        margin: 5px 5px!important;
    }
</style>
<body>
<link rel="stylesheet" href="css/font-awesome.min.css">
<div class="container">
    <div class="contact" style="margin-top: 14%;">
        <h2 class="tittle">Feedback</h2>
        <div class="right-w3">

            <?php
                $feed = $getFeedbackData['feedData'];
                for($i=0;$i<count($feed);$i++){
                    if($feed[$i]['feedback_status'] == '1'){
                    ?>
                    <div style="border-bottom: 1px solid #555;padding: 18px;">
                        <h2 class="feedbackHeading"><?php echo $feed[$i]['feedback_text'];?></h2>
                        <p class="feedbackDescription"><?php echo $feed[$i]['feedback_description'];?></p>
                        <p class="para">
                            <span class="para nameUser" style="line-height: 1.5">NAME : <?php echo $feed[$i]['f_name'];?></span>
                            <span class="para fromUser" style="line-height: 1.5">FROM: <?php echo $feed[$i]['f_from'];?></span>
                            <span class="para fromDate" style="line-height: 1.5"><?php $date =  date_create($feed[$i]['created_date']);
                            echo $feed[$i]['created_date'];?></span>
                        </p>


                    </div>
             <?php   }}  ?>

        </div>
        <form action="#" method="post" id="submit_feedback">
            <div class="left">
                <div class="name">
                    <i class="fa fa-user" aria-hidden="true"></i>
                    <input type="text" name="name" class="name feedbackFields" placeholder="Your Name" required="">
                </div>
                <div class="email">
                    <i class="fa fa-globe" aria-hidden="true"></i>
                    <input type="text" name="from" class="email feedbackFields" placeholder="From" required="">
                </div>
                <div class="phone">
                    <i class="fa fa-feed" aria-hidden="true"></i>
                    <textarea type="text" name="feedback_text" class="phone feedbackFields" placeholder="Your Feedback Title" required=""></textarea>
                </div>
                <div class="phone" style="margin-top: 10px">
                    <i class="fa fa-feed" aria-hidden="true"></i>
                    <textarea type="text" name="feedback_description" class="phone feedbackFields" placeholder="Your Feedback Description" required=""></textarea>
                </div>
              <!--  <div class="phone">
                    <i class="fa fa-feed" aria-hidden="true"></i>
                    <textarea type="text" name="feedback_description" class="phone" placeholder="Your Feedback Description" required=""></textarea>
                </div>-->
                <input type="hidden" name="type" value="feedback">
                <div style="text-align: center;margin:20px">
                    <input type="submit" value="Send">
                </div>

            </div>
        </form>
        <div class="clear"></div>
    </div>
</div>
<?php
include_once 'footer.php';
?>
</body>
</html>
<script>
    $("#submit_feedback").submit(function(e){
        e.preventDefault();
        $('.loader').fadeIn("slow");
        $.ajax({
            url: "admins/api/userProcess.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                var Status = data.Status;
                var Message = data.Message;
                if (Status == "Success"){
                    alert(Message);
                    window.location="feedback.php";
                }
            }
        });
    });

</script>