<html lang="en">
<?php
error_reporting(0);
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/ADMIN.php');
$conn = new \Classes\CONNECT();
$adminClass = new \Classes\ADMIN();
if($_SESSION['SFTAdminId'] ==""){
    echo "<script>document.location.href='login.php'</script>";
}
$allAdminData = $adminClass->getPerticularAdminData($_SESSION['SFTAdminId']);
$getMenuData = $adminClass->getAllMenus();
echo "<pre>";
print_r($getMenuData);
echo "</pre>";
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SFTailor || Admin</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
</head>
<style>
    .active a {

        color: white;

    }</style>
<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            <div class="left_col scroll-view">
                <div class="navbar nav_title" style="border: 0;">
                    <a href="index" class="site_title"><img src="images/logo.png" style="height: 51px;"></a>
                </div>
                <div class="clearfix"></div>
                <!-- menu profile quick info -->
                <div class="profile clearfix">
                    <div class="profile_pic">
                        <img src="images/img.png" alt="..." class="img-circle profile_img">
                    </div>
                    <div class="profile_info">
                        <span>Welcome</span>
                        <h2><?php echo $_SESSION['SFTAdminName'];?></h2>
                    </div>
                </div>
                <!-- /menu profile quick info -->

                <br />

                <!-- sidebar menu -->
                <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                    <div class="menu_section">
                        <h3>General</h3>
                        <ul class="nav side-menu">




                            <li class="active treeview">
                                <a href="#">
                                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                                    <span class="pull-right-container">
                                          <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                </a>
                                <ul class="treeview-menu">
                                    <li class="active">
                                        <a href="index.html">
                                            <i class="fa fa-caret-right"></i> Dashboard v1
                                        </a>
                                    </li>
                                </ul>
                            </li>




                            <?php
                            //for simple admin
                            $checkAdmin = $allAdminData['adminData'];
                            $admin_type = $checkAdmin['admin_level'];
                            if($admin_type !='Super Admin'){
                                $admin_access_menu = $checkAdmin['admin_access_menu'];
                                $simpmenu =  explode(",",$admin_access_menu);
                                for($i=0;$i<count($simpmenu);$i++){
                                    $menuNameS = $simpmenu[$i];
                                    $mainMenu = $getMenuData['menuData'];
                                    foreach($mainMenu as $mMenu){
                                        $menuNameP = $mMenu['menu_name'];
                                        if($menuNameS == $menuNameP){
                                            $menuNameL = $mMenu['menu_link'];
                                            ?>
                                            <li><a href="<?php echo $menuNameL;?>"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i><?php echo $menuNameS;?></a></li>
                                        <?php } } }	}
                            //for super admin
                            if($admin_type =='Super Admin') {
                                $mainMenu = $getMenuData['menuData'];
                                for($i=0;$i<count($mainMenu);$i++){
                                    $menuName = $mainMenu[$i]['menu_name'];
                                    $menuLink = $mainMenu[$i]['menu_link'];
                                    ?>
                                    <li><a href="<?php echo $menuLink;?>"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i><?php echo $menuName;?></a></li>
                                <?php } } ?>
                        </ul>
                    </div>
                </div>
                <!-- /sidebar menu -->
            </div>
        </div>
        <!-- top navigation -->
        <div class="top_nav">
            <div class="nav_menu">
                <nav>
                    <div class="nav toggle">
                        <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                    </div>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="">
                            <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="images/img.png" alt=""><?php echo $_SESSION['SFTAdminName'];?>
                                <span class=" fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu dropdown-usermenu pull-right">
                                <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                <li>
                                    <a onclick=changeAdminPassword('<?php echo $_SESSION['SFTAdminId'] ?>')>
                                        <i class="fa fa-lock pull-right"></i> Change Password
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
