<?php include("header.php");?>
	<div class="banner top-fullbespokadvantage">
		<div class="">
		</div>
	</div>
	<!-- //Slider -->
</div>
	<!-- //Header -->
<div class="about-bottom wthree-3">
	<div class="container">
		<h2 class="tittle">Full bespoke <span style="color:#000;">advantages</span></h2>
		
		<div class="bs-docs-example">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>No</th>
							<th>Item</th>
							<th>Full bespoke suit advantages</th>
							<th>Normal MTM suit disadvantage</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Pattern</td>
							<td>Platform design exclusive pattern for every customer</td>
							<td>Simple amendment on standard pattern</td>
						</tr>
						<tr>
							<td>2</td>
							<td>Lead time</td>
							<td>7 working days</td>
							<td>Can not promise to delivery in a short time.</td>
						</tr>
						<tr>
							<td>3</td>
							<td>Individual designs</td>
							<td>Could meet all the customers individual needs</td>
							<td>Individual designs are not available.</td>
						</tr>
						<tr>
							<td>4</td>
							<td>Top making crafts</td>
							<td>Full handmade & full canvas</td>
							<td>Simple machine-made crafts</td>
						</tr>
						<tr>
							<td>5</td>
							<td>Eco-friendly design</td>
							<td>No lining, no shoulder pad; shirt fabric; silk fabric; textile suit.</td>
							<td>Airtight, heavy, old mishmash, stiffness, uncomfortable.</td>
						</tr>
						<tr>
							<td>6</td>
							<td>Measuring method</td>
							<td>Enjoy patent measuring method, accurate and efficient.</td>
							<td>Try on fit garments and adjust based on the standard size.</td>
						</tr>
						<tr>
							<td>7</td>
							<td>Try on</td>
							<td>No Need to try on, make the garment perfect-fit all at one time.</td>
							<td>Need many try-ons and amendments</td>
						</tr>
						<tr>
							<td>8</td>
							<td>Ordering method</td>
							<td>Place orders via on-line ordering system, submit order with one click.</td>
							<td>Place orders manually, inefficiently and error prone</td>
						</tr>
						<tr>
							<td>9</td>
							<td>Manufacturing advantage</td>
							<td>World-wide high-end informationalized & specialized factory.</td>
							<td>Unprofessional MTM factory</td>
						</tr>
						<tr>
							<td>10</td>
							<td>Full bespoke</td>
							<td>The symbol of your status, the show of your personality, and could bring you a healthy and comfortable dressing experience.</td>
							<td>Normal suit, you can buy it anywhere.</td>
						</tr>
					</tbody>
				</table>
			</div>
			
				<div class="grid_3 grid_5 wthree">
				<h3>FULL BESPOKE Advantages</h3>
				<div class="col-md-12 agileits-w3layouts">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="text-align: center;"><img src="images/logo/logo.png" style="width:20%;">Suit</th>
								<th style="text-align: center;">Normal Suit</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>Collar
									<ul>
									<li class="list-images"><img src="images/basepoke/collar1.png"></li>
									<li>The collar outline appears to be a natural curvature from above to bottom</li>
									<li>The back neck is more fit to the neck which shall increase your noble temperament</li>
									<li>Making your neck more slender</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/collar2.png"></li>
									<li>The collar is not natural enough.</li>
									<li>The back neck is not fit with the body</li>
									<li>Unsmooth by observation</li>
									</ul>
								</td>
							</tr>
							<tr>
								<td>Shoulder
									<ul>
									<li class="list-images"><img src="images/basepoke/shoulder1.png"></li>
									<li>With the smooth line of the shoulder</li>
									<li>Overall uprising the collar, revealing one's noble temperament</li>
									<li>No shoulder pad or light shoulder pad is with a better breathability and less pressure on one's shoulder</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/shoulder2.png"></li>
									<li>The line of the shoulder is not smooth, the shoulder breath is too large and the proportion is not coordinating.</li>
									<li>Lowing the collar of the suit which appears to be too fat</li>
									<li>Shoulder pad is too thick and too stressful</li>
									</ul>
								</td>
							</tr>
							
							<tr>
								<td>Breast
									<ul>
									<li class="list-images"><img src="images/basepoke/breast1.png"></li>
									<li>The breast would appears to be more smoothly and straight</li>
									<li>Chest is more fit and makes one more muscular</li>
									<li>The overall dressing has a slim effect and makes one more masculine</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/breast2.png"></li>
									<li>The breast is not smooth and straight enough</li>
									<li>With airtight, uncomfortable and huge chest lining has made one looks stiff</li>
									<li>The front panel of the suit is too heavy that makes one moves uncomfortably</li>
									</ul>
								</td>
							</tr>
							
							<tr>
								<td>Waist
									<ul>
									<li class="list-images"><img src="images/basepoke/waist1.png"></li>
									<li>With quite fair shaped and natural waist line</li>
									<li>A more disctinct waist narrowing effect</li>
									<li>Moving more comfortably</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/waist2.png"></li>
									<li>Without waist shape</li>
									<li>Looks fat without waist narrowing</li>
									<li>Inconvenient when making movements</li>
									</ul>
								</td>
							</tr>
							
							<tr>
								<td>Armhole
									<ul>
									<li class="list-images"><img src="images/basepoke/armhole1.jpg"></li>
									<li>The armhole is more fitand makes one looks more flexible as a whole</li>
									<li>Makes one move more comfortably and the appearance looks more light and handy</li>
									<li>The design is more ergonomic</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/armhole2.jpg"></li>
									<li>The whole armhole looks fat</li>
									<li>The appearance is not smooth and straight enough</li>
									<li>Inconvenient when making movements</li>
									</ul>
								</td>
							</tr>
							
							<tr>
								<td>Back
									<ul>
									<li class="list-images"><img src="images/basepoke/back1.jpg"></li>
									<li>The shoulder appears t be stylish.And with the waist narrowing, the outlineof one's back appears to be perfect</li>
									<li>The slim outline makes one looks more handsome</li>
									<li>The back is more fit and smooths</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/back2.jpg"></li>
									<li>Without waist shape, the suit is barrel-shaped</li>
									<li>Without waist narrowing,the suit looks fat</li>
									<li>Inconvenient when making movements</li>
									</ul>
								</td>
							</tr>
							
							<tr>
								<td>Side Panel
									<ul>
									<li class="list-images"><img src="images/basepoke/sidepanel1.jpg"></li>
									<li>Flat and fair shaped</li>
									<li>Chest is more fit and makes one more muscular</li>
									<li>The overall dressing has a slim effect and makes one more musculine</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/sidepanel2.jpg"></li>
									<li>Not flat and fair enough</li>
									<li>Chest lining is too stiff that made the side panel has no sense of line</li>
									</ul>
								</td>
							</tr>
							
							<tr>
								<td>Back Neck
									<ul>
									<li class="list-images"><img src="images/basepoke/backneck1.jpg"></li>
									<li>More fit and natural overall</li>
									<li>No redundancy below the back neck</li>
									<li>Without limitation when making movements</li>
									</ul>
								</td>
								<td>
									<ul>
									<li class="list-images"><img src="images/basepoke/backneck2.jpg"></li>
									<li>With redundancy below the back neck</li>
									<li>Not smooth enough</li>
									<li>Restricted when making movements</li>
									</ul>
								</td>
							</tr>
							
						</tbody>
					</table>                    
				</div>
			   <div class="clearfix"> </div>
			</div>	
			
	</div>
</div>
<?php include("footer.php");?>