<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 10/24/2017
 * Time: 11:21 AM
 */


namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class GALLERY
{
    private $pdf_id;
    public $link = null;
    public $userClass = null;
    public $response = array();

    public function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
    }
    public function addGalleryImg($event,$iseventimg,$oldeventimg){
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $event_img = "";
        if ($iseventimg == "yes") {
            if(isset($_FILES['eventimg'])){
                $evefile_tmp = $_FILES['eventimg']['tmp_name'];
                $eve_extensions = array("jpg", "png", "gif", "jpeg");
                $evefile_ext = strtolower(end(explode('.', $_FILES['eventimg']['name'])));
                if (in_array($evefile_ext, $eve_extensions) === true) {
                    $eve_db_file_name = rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$evefile_ext;
                    if (move_uploaded_file($evefile_tmp, getcwd() . "/Files/images/gallery/" . $eve_db_file_name)) {
                        $event_img = $eve_db_file_name;
                    }
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Please Select Event Image";
                return $this->response;
            }
        }
        else {
            $event_img = $oldeventimg;
        }
        foreach($_FILES['file_']['tmp_name'] as $key => $tmp_name ) {
            $file_name = $_FILES['file_']['name'][$key];
            if (strpos($file_name, "'") >= 0) {
                $file_name = str_replace("'", "", $file_name);
            }
            $file_tmp = $_FILES['file_']['tmp_name'][$key];
            $extensions = array("jpg", "png", "gif", "jpeg");
            $file_ext = strtolower(end(explode('.', $_FILES['file_']['name'][$key])));
            if (in_array($file_ext, $extensions) === true) {
                $db_file_name = rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$file_ext;
                if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/gallery/" . $db_file_name)) {
                    $gallery_title = explode('.',$file_name)[0];
                    $getCat =  "SELECT * FROM gallery where event = '$event'";
                    $getCat_result = mysqli_query($link, $getCat);
                    if($getCat_result){
                        $getCat_num = mysqli_num_rows($getCat_result);
                        if($getCat_num > 0){
                            $getCat_row = mysqli_fetch_assoc($getCat_result);
                            $sort_order_cat = $getCat_row['sort_order_cat'];
                        }
                        else{
                            $getMaxCatSort = "SELECT MAX(sort_order_cat) AS sort_order_cat FROM gallery";
                            $getMaxCatSort_result = mysqli_query($link, $getMaxCatSort);
                            if ($getMaxCatSort_result) {
                                $sort_order_cat = 0;
                                $getMaxCatSort_num = mysqli_num_rows($getMaxCatSort_result);
                                if($getMaxCatSort_num > 0){
                                    $sort_row = mysqli_fetch_assoc($getMaxCatSort_result);
                                    $sort_order_cat = $sort_row["sort_order_cat"];
                                    $sort_order_cat++;
                                }
                            }
                            else{
                                $this->response[STATUS] = Error;
                                $this->response[MESSAGE] = $this->link->sqlError();
                                return false;
                            }
                        }
                        $getMaxSort = "SELECT  MAX(`sort_order`) AS sort_order FROM gallery";
                        $getMaxSort_result = mysqli_query($link, $getMaxSort);
                        if ($getMaxSort_result) {
                            $sort_order = 0;
                            $sortNum = mysqli_num_rows($getMaxSort_result);
                            if ($sortNum > 0) {
                                $sort_row = mysqli_fetch_assoc($getMaxSort_result);
                                $sort_order = $sort_row["sort_order"];
                                $sort_order++;
                            }
                            $query = "insert into gallery (gallery_title, gallery_file,status,event,event_img,sort_order_cat,sort_order)
                            values ('$gallery_title','$db_file_name','1','$event','$event_img','$sort_order_cat','$sort_order')";
                            $result = mysqli_query($link, $query);
                            if (!$result) {
                                $this->response[STATUS] = Error;
                                $this->response[MESSAGE] = $this->link->sqlError();
                                return $this->response;
                            }
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "File uploaded successfully";
                        } else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                        return false;
                    }

                }else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = " getcwd is  ".getcwd();
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Wrong File Selected";
            }
        }
        return $this->response;
    }
    public function isFileExists($file_name) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from gallery where gallery_file='$file_name'";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $this->response[STATUS] = Error;
        $this->response[MESSAGE] = "File already exists with this name ".$file_name;
        $this->response["data"] = $data;
        return $this->response;
    }
    public function getGallery() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from gallery ORDER BY sort_order ASC";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;

    }
    public function getDistinctEvent() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select distinct(event) from gallery  ORDER BY sort_order_cat ASC";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $event = $rows['event'];
            $qq = "select * from gallery where event = '$event' limit 1";
            $rr = mysqli_query($link,$qq);
            $roww = mysqli_fetch_assoc($rr);
            $data[] = $roww;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;
    }
    public function updateSortOrder($sortOrder,$idOrder) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        for($j = 0;$j < count($idOrder); $j++){
            $update_sort = "update gallery set sort_order= '$sortOrder[$j]' where id = '$idOrder[$j]' ";
            $result = mysqli_query($link,$update_sort);
            $count = mysqli_affected_rows($link);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update_sort;

                return $this->response;
            }
        }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }
    public function updateCatSortOrder($sortOrder,$idOrder) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        for($j = 0;$j < count($idOrder); $j++){
            $update_sort = "update gallery set sort_order_cat= '$sortOrder[$j]' where event = '$idOrder[$j]' ";
            $result = mysqli_query($link,$update_sort);
            $count = mysqli_affected_rows($link);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update_sort;

                return $this->response;
            }
        }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }
    public function deleteGallery($id,$path) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from gallery where id='$id'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        $this->response["data"] = $data;
        $path = rawurldecode($path);
        unlink("Files/images/".$path);
        return $this->response;

    }
    public function deleteEvent($event) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }

        $query = "select * from gallery where event='$event'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }
        $eventfilepath = "";
        while($rows = mysqli_fetch_assoc($result)){
            $gallery_file = $rows['gallery_file'];
            $event_img = $rows['event_img'];
            $filepath = getcwd()."/Files/images/gallery/".$gallery_file;
            $eventfilepath = getcwd()."/Files/images/gallery/".$event_img;
            unlink($filepath);
        }
        $query = "delete from gallery where event='$event'";
        $result = mysqli_query($link,$query);
        if($result) {
            unlink($eventfilepath);
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "Data deleted successfully";
            return $this->response;
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable To Delete Event";
            return $this->response;
        }
    }
    public function editEvent($new_event,$old_event,$oldEventImagechanged,$oldEventImage) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldEventImagechanged == "yes") {
            $file_tmp = $_FILES['neweventimg']['tmp_name'];
            $eveimg = $_FILES['neweventimg']['name'];
            $extensions = array("jpg", "png", "gif", "jpeg");
            $file_ext = strtolower(end(explode('.', $_FILES['neweventimg']['name'])));
            if (in_array($file_ext, $extensions) === true) {
                $eveimg = rand(0000, 9999) . strtotime(date("d M Y h:i:s A")) . "." . $file_ext;
                if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/gallery/" . $eveimg)) {
                    $gallery_title = explode('.', $eveimg)[0];
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Wrong File Selected";
                return false;
            }
        }else{
            $eveimg = $oldEventImage;
        }
        $query = "update gallery set event='$new_event',event_img = '$eveimg' where event = '$old_event'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }
    public function editGallery($gallery_id,$gallery_title,$oldGalleryImagechanged) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldGalleryImagechanged == "yes") {
            if(isset($_FILES['newgalleryimg']['tmp_name'])) {
                $file_name = $_FILES['newgalleryimg']['name'];
                if (strpos($file_name, "'") >= 0) {
                    $file_name = str_replace("'", "", $file_name);
                }
                $file_tmp = $_FILES['newgalleryimg']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['newgalleryimg']['name'])));
                if (in_array($file_ext, $extensions) === true) {
                    $db_file_name = rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/gallery/" . $db_file_name)) {
                        $query = "update gallery set gallery_title='$gallery_title',gallery_file='$db_file_name'
                        where id = '$gallery_id'";
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = " getcwd is  ".getcwd();
                        return $this->response;
                    }
                }
                else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }else{
            $query = "update gallery set gallery_title='$gallery_title' where id = '$gallery_id'";
        }
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }
    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}