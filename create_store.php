<?php
require_once 'errorCodes.php';
require_once 'functions.php';
require_once 'database/db.php';
$status="error";
$items=array();
$message="unable to get data";
$mail_status= "";
$type = $_REQUEST['type'];
$user_name = $_REQUEST['user_name'];
$user_email = $_REQUEST['user_email'];
$store_phone = $_REQUEST['store_phone'];
$store_status = $_REQUEST['store_status'];
$store_password = $_REQUEST['store_password'];
$store_name = $_REQUEST['store_name'];
$store_contact_address = $_REQUEST['store_contact_address'];
$store_about = $_REQUEST['store_about'];
$zip_code = $_REQUEST['zip_code'];
$b_country = $_REQUEST['b_country'];
$b_state = $_REQUEST['b_state'];
$b_city = $_REQUEST['b_city'];
$dob = $_REQUEST['dob'];


if($type=="storeinsert"){
    $requiredfields = array('user_name', 'user_email','store_phone','store_password','store_name');
    if(!RequiredFields($_REQUEST, $requiredfields)){
        return false;
    }

    $alr = "select * from wp_store where store_name='$store_name'";
    $rcm = mysql_query($alr);
    $num = mysql_num_rows($rcm);
    if($num>0)
    {
        $status=  "done";
        $message = "Store name already Exist Please Choose Another";
    }
    else {
        $makeDir = mkdir('../../'.$store_name.'.com');
        if($makeDir) {
             $indexcopy = copy('../../demo/index.php', '../../'.$store_name.'.com/index.php');
            if($indexcopy){
                file_put_contents('../../'.$store_name.'.com/index.php',"<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
            }
             $headcopy = copy('../../demo/header.php', '../../'.$store_name.'.com/header.php');
            if($headcopy){
                file_put_contents('../../'.$store_name.'.com/header.php',"<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
            }
            $copycontact=  copy('../../demo/contact.php', '../../'.$store_name.'.com/contact.php');
            if($copycontact){
                file_put_contents('../../'.$store_name.'.com/contact.php',"<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
            }
             copy('../../demo/about.php', '../../'.$store_name.'.com/about.php');
             copy('../../demo/canvas.php', '../../'.$store_name.'.com/canvas.php');
             copy('../../demo/accessories.php', '../../'.$store_name.'.com/accessories.php');
             copy('../../demo/androidObject.php', '../../'.$store_name.'.com/androidObject.php');
             copy('../../demo/cart.php', '../../'.$store_name.'.com/cart.php');
             copy('../../demo/colorSwitcher.php', '../../'.$store_name.'.com/colorSwitcher.php');
             copy('../../demo/custom_configure.php', '../../'.$store_name.'.com/custom_configure.php');
             copy('../../demo/custom_configure_overcoat.php', '../../'.$store_name.'.com/custom_configure_overcoat.php');
             copy('../../demo/custom_overcoat.php', '../../'.$store_name.'.com/custom_overcoat.php');
             copy('../../demo/custom_pants.php', '../../'.$store_name.'.com/custom_pants.php');
             copy('../../demo/custom_shirts.php', '../../'.$store_name.'.com/custom_shirts.php');
             copy('../../demo/custom_suit.php', '../../'.$store_name.'.com/custom_suit.php');
             copy('../../demo/customfit.php', '../../'.$store_name.'.com/customfit.php');
             copy('../../demo/customize.php', '../../'.$store_name.'.com/customize.php');
             copy('../../demo/fabric.php', '../../'.$store_name.'.com/fabric.php');
             copy('../../demo/fullbespoke.php', '../../'.$store_name.'.com/fullbespoke.php');
             copy('../../demo/highend.php', '../../'.$store_name.'.com/highend.php');
             copy('../../demo/highenddetails.php', '../../'.$store_name.'.com/highenddetails.php');
             copy('../../demo/login.php', '../../'.$store_name.'.com/login.php');
             copy('../../demo/logout.php', '../../'.$store_name.'.com/logout.php');
             copy('../../demo/myaccount.php', '../../'.$store_name.'.com/myaccount.php');
             copy('../../demo/objectLoader.php', '../../'.$store_name.'.com/objectLoader.php');
             copy('../../demo/orders.php', '../../'.$store_name.'.com/orders.php');
             copy('../../demo/pricelist.php', '../../'.$store_name.'.com/pricelist.php');
             copy('../../demo/suit_old.php', '../../'.$store_name.'.com/suit_old.php');
             copy('../../demo/topbrands.php', '../../'.$store_name.'.com/topbrands.php');
             copy('../../demo/workprocess.php', '../../'.$store_name.'.com/workprocess.php');
        }
        $target_path = "upload/storelogo/";
        $file_ext=strtolower(end(explode('.',$_FILES['store_logo']['name'])));
        $tmp_name = $_FILES['store_logo']['tmp_name'];
        $file_name = "logo".$store_name.".".$file_ext;
        $target_path = $target_path.$file_name;
        if(move_uploaded_file($tmp_name, $target_path)) {
            $store_logo = "admin/webserver/" . $target_path;
            $insert_data = "insert into wp_store (user_name,user_email,store_phone,store_password,store_name,store_logo,store_about,store_contact_address,zip_code,b_country,b_state,b_city,dob,store_status,created_date)
             values('$user_name','$user_email','$store_phone','$store_password','$store_name','$store_logo','$store_about','$store_contact_address','$zip_code','$b_country','$b_state','$b_city','$dob','$store_status',NOW())";
            $insert_res = mysql_query($insert_data);
            if ($insert_res) {
                $status = "done";
                $message = "your store has been created successfully.";
            } else {
                $status = "error";
                $message = mysql_error();
            }
        }
    }

    echo json_encode(array("status"=>$status,"message"=>$message));
}
if($type=="storeupdate"){
    $store_id = $_REQUEST['store_id'];
    $qq = "select * from wp_store where store_id = '$store_id'";
    $rr = mysql_query($qq);
    $num = mysql_num_rows($rr);
    if($num>0)
    {

        $update_data = "update wp_store set user_name='$user_name',store_phone='$store_phone',store_contact_address='$store_contact_address' ,store_password='$store_password' where store_id='$store_id'";
        $update_res = mysql_query($update_data);
        if($update_res){
            $status=  "done";
            $message = "Store information Updated Successfully";
        }
        else{
            $status=  "error";
            $message=mysql_error();
        }
    }
    echo json_encode(array("status"=>$status,"message"=>$message));
}


?>