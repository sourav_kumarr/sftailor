<body>
<script src="js/three.js"></script>
<script src="js/DDSLoader.js"></script>
<script src="js/MTLLoader.js"></script>
<script src="js/OBJLoader.js"></script>
<script src="js/Detector.js"></script>
<script src="js/TrackballControls.js"></script>
<script src="js/jquery-2.2.1.min.js"></script>
<script>
    var container, stats;
    var camera, scene, renderer;
    var mouseX = 0, mouseY = 0;
    var windowHalfX = window.innerWidth / 3;
    var windowHalfY = window.innerHeight / 3;
    var filename="";
    var color="";
    load_data();
    function init() {
        container = document.createElement('div');
        document.body.appendChild(container);
        camera = new THREE.PerspectiveCamera(10, window.innerWidth / window.innerHeight, 1, 1000);
        camera.position.z = 90;
        // scene
        scene = new THREE.Scene();
        var ambient = new THREE.AmbientLight(0x999999);
        scene.add(ambient);
        var directionalLight = new THREE.DirectionalLight(0x999999);
        directionalLight.position.set(1, -1, 0.5).normalize();
        scene.add(directionalLight);
        // model
        var onProgress = function (xhr) {
            if (xhr.lengthComputable) {
                var percentComplete = xhr.loaded / xhr.total * 100;
                console.log(Math.round(percentComplete, 2) + '% downloaded');
            }
        };
        var onError = function (xhr) {
        };
        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio(window.devicePixelRatio);
        renderer.setSize(500, 650);
        renderer.setClearColor(0xffffff);
        container.appendChild(renderer.domElement);
        controls = new THREE.TrackballControls(camera, renderer.domElement);
        controls.zoomSpeed = 0.9;
        controls.rotateSpeed = 4.0;
        controls.noZoom = true;
        controls.panSpeed = 0;
        THREE.Loader.Handlers.add(/\.dds$/i, new THREE.DDSLoader());
        var mtlLoader = new THREE.MTLLoader();
        mtlLoader.setPath("object_files/"+filename+"/");
        mtlLoader.load(filename+".mtl", function (materials) {
            var objLoader = new THREE.OBJLoader();
            objLoader.setMaterials(materials);
            objLoader.setPath("object_files/"+filename+"/");
            objLoader.load(filename+".obj", function (object) {
                object.position.x = 0;
                object.position.y = 0;
                object.position.z = 0;
                object.scale.set(1, 1, 1);
                scene.add(object);
            }, onProgress, onError);
        });
    }
    function animate() {
        requestAnimationFrame(animate);
        renderer.render(scene, camera);
        controls.update();
    }
    function render() {
        camera.lookAt(scene.position);
        renderer.render(scene, camera);
    }
    function getQueryStringParameter(paramToRetrieve) {
        var params = document.URL.split("?")[1].split("&");
        var strParams = "";
        for (var i = 0; i < params.length; i = i + 1) {
            var singleParam = params[i].split("=");
            if (singleParam[0] == paramToRetrieve)
                return singleParam[1];
        }
    }
    function load_data() {
        filename = getQueryStringParameter("filename");
        color = getQueryStringParameter("color");
        var url = "colorSwitcher.php?filename="+filename+"&color="+color;
        $.get(url,function(data)
        {
            var json = $.parseJSON(data);
            var status = json.status;
            if(status == "done")
            {
                init();
                animate();
            }
            else
            {
                alert("error");
            }
        });
    }
</script>
</body>
</html>