<?php include("header.php");?>
<div class="banner top-aboutbannersuits">
    <div class="">
    </div>
</div>
</div>
<div class="about-bottom wthree-3">
    <div class="container">
        <h2 class="tittle">LET YOUR SUIT DO THE TALKING</h2>
        <div class="agileinfo_about_bottom_grids">
           <div class="col-md-12 agileinfo_about_bottom_grid">
                <p>Your suit is a blank canvas waiting to be designed by you. The fabric is the obvious choice to start with, but there are many more details that can be personalized. 
                </p><br>
				<img src="images/slider/customize_your_suitcopy.jpg" alt=" " class="img-responsive">
            </div>
          
        </div>
        <h2 class="tittle" style="font-size:2em;">FASHION FADES, BUT STYLE IS ETERNAL.</br> COCO CHANEL</h2>
    </div>
</div>

<!-- //team -->
<?php include("footer.php");?>
