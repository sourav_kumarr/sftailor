<?php
include('database/db.php');
session_start();
$status = "init";
$items=array();

error_reporting(0);
$curr_timezone = date_default_timezone_get();
date_default_timezone_set($curr_timezone);
$dated = date("d/M/Y h:i:s A");
$user_id = $_REQUEST['user_id'];

function getProductPrice ($colorCode) {
    $price = 169;

    $query ="select * from fabric where fabric_cameo='$colorCode' or fabric_rc_code='$colorCode'";

    $result = mysql_query($query);
    if($result) {
        $num = mysql_num_rows($result);
        if($num>0) {
            $row = mysql_fetch_array($result);
            $fabric_price = floatval(substr($row['fabric_price'],1,strlen($row['fabric_price'])));
            if($fabric_price<= 13) {
                $cat_price = "select * from categories where $fabric_price>=price_range_start and $fabric_price<=price_range_end";
                $resultt = mysql_query($cat_price);
                if($resultt){

                    $rows = mysql_fetch_array($resultt);
                    $count = mysql_num_rows($resultt);

                    if($count>0){
                        $product_price = $rows['cat_price'];
//                      echo $product_price;
                        return $product_price;
                    }
                }
            }
            else{
                $prodPrice = isFabricColorExists($colorCode,'Custom Shirt');
                if($prodPrice['status']) {
                    $price = $prodPrice['price'];
                    return $price;
                }
            }
        }
    }
    return $price;
}

function isFabricColorExists($colorCode,$prod_type) {
    $response = array();
    $price_query = "select * from wp_prices where price_type='$prod_type'";
    $price_result = mysql_query($price_query);
    if($price_result) {
        $price_count = mysql_num_rows($price_result);
        if($price_count == 0) {
            $response['status'] = false;
            return $response;
        }
        $row = mysql_fetch_array($price_result);
        $price = $row['price_amount'];
        $unit = $row['price_per_meter'];

        $query ="select * from fabric where fabric_cameo='$colorCode' or fabric_rc_code='$colorCode'";
        $result = mysql_query($query);
        if($result) {
            $num = mysql_num_rows($result);
            if($num>0) {
                $row = mysql_fetch_array($result);
                $fabric_price = $row['fabric_price'];

                if($fabric_price == '') {
                    $response['status'] = false;
                    return $response;
                }

                /*if($prod_type === 'Custom Shirt') {
                    $retail_price = intval(substr($row['fabric_retail_price'],1,strlen($row['fabric_retail_price'])));

                    $fabric_price = intval(substr($fabric_price,1,strlen($fabric_price)));
                    $price = intval($price);
                    $unit = floatval($unit);
                    $response['status'] = true;
                    if($fabric_price>13) {
                        $extra_price = ($fabric_price-$price)*$unit;
                        $response['price'] = $extra_price;
                     }
                     else{
                         $response['price'] = $retail_price;
                     }
                    return $response;
                }*/

                $fabric_price = intval(substr($fabric_price,1,strlen($fabric_price)));
                $price = intval($price);
                $unit = floatval($unit);
                if($fabric_price>$price) {
                    $extra_price = ($fabric_price-$price)*$unit;
                    $response['status'] = true;
                    $response['price'] = $extra_price;
                    return $response;
                }
            }
        }


    }
    $response['status'] = false;
    return $response;

}

function getButtonPrice($btnFor,$btnName){
    $reqType = "jacket";
    switch ($btnFor){
        case "Custom Shirt":
            $reqType = "shirt";
            break;
        case "Custom Jacket":
            $reqType = "jacket";
            break;
        case "Custom Suit":
            $reqType = "jacket";
            break;
    }
    $defaultPrice = 0;
    $btnPrice = 0;
    $defaultPriceQry = "select * from button_price_process where price_type ='$btnFor'";
    $resPrice = mysql_query($defaultPriceQry);
    if($resPrice){
        if(mysql_num_rows($resPrice) > 0){
            $defaultPriceRow = mysql_fetch_assoc($resPrice);
            $uplifts = $defaultPriceRow['uplifts'];
            $price_amount = $defaultPriceRow['price_amount'];
            $no_of_buttons = $defaultPriceRow['no_of_buttons'];
            $defaultPrice =  $no_of_buttons * $price_amount * $uplifts;
        }
    }
    $btnPriceQry = "select * from button where btn_name = '$btnName' and btn_type='$reqType' ";
    $resBtnPrice = mysql_query($btnPriceQry);
    if($btnPriceQry){
        if(mysql_num_rows($resBtnPrice) > 0){
            $btnPriceRow = mysql_fetch_assoc($resBtnPrice);
            $btnPrice = $btnPriceRow['btn_price'];
            if($btnPrice <= $defaultPrice){
                $btnPrice = $defaultPrice;
            }
        }
    }
    /*echo "btn_name => ".$btn_name." btnPrice = > ".$btnPrice." defaultPrice=> ".$defaultPrice;*/
    return $btnPrice;
}

if($_REQUEST)
{
    $type = $_REQUEST['type'];
    $query = "select * from categories";
    $result = mysql_query($query);
    if($result){
        $num = mysql_num_rows($result);
        if($num>0){
            $status = "done";
            $product_priced = 0;
            $extra_price = 0;
            $cat_name = $_REQUEST['cat_name'];
            $btnColor = $_REQUEST['btnColor'];
            $quantity = $_REQUEST['quantity'];
            while($row= mysql_fetch_array($result)) {
//                echo 'row catname '.$row['cat_name'].' request type '.$type."\n";
                if ($type == "shirt" && $row['cat_name'] == $cat_name) {

                    if(isset($_REQUEST['prod_color'])) {

                        $color_code = $_REQUEST['prod_color'];
//                        $product_priced = getProductPrice($color_code);
//                        echo $quantity;
                        if(intval($quantity) === 1) {
                            $product_priced = getProductPrice($color_code);
                        }
                        else
                         $product_priced = $row['cat_price'];
                        /*if(isFabricColorExists($color_code,"Custom Shirt")) {
                            $sub_total = 2.1*16;
                            $product_priced = $product_priced+$sub_total;
                        }*/

                        $priceResponse = isFabricColorExists($color_code,"Custom Shirt");

                        if($priceResponse["status"]) {
                            if(strpos($priceResponse["status"], '$') !== false)
                             $extra_price = substr($priceResponse["price"],1,strlen($priceResponse["price"]));
                            else
                              $extra_price  = $priceResponse["price"];
                        }


//                        $product_priced = intval($product_priced)*$quantity;
                        $buttonswatchPrice = getButtonPrice('Custom Shirt',''.$btnColor.'');
                        $product_priced = $product_priced + (floatval($buttonswatchPrice) * $quantity);

                        $extra_price = $extra_price*$quantity;

                    }
                }
                else if($type == "jacket" && $row['cat_name'] == $cat_name){
                    $product_priced = $row['cat_price'];
                    if(isset($_REQUEST['prod_color'])) {
                        $color_code = explode(":",$_REQUEST['prod_color'])[0];
                        /*if(isFabricColorExists($color_code)) {
                           $sub_total = 3.5*30;
                           $product_priced = $product_priced+$sub_total;
                        }*/

                        $priceResponse = isFabricColorExists($color_code,"Custom Suit");
                        if($priceResponse["status"]) {
                            $extra_price = $priceResponse["price"];
                        }
                    }
                    $quantity = $_REQUEST['quantity'];
                    $product_priced = $product_priced*$quantity;

                    $buttonswatchPrice = getButtonPrice('Custom Jacket',''.$btnColor.'');
                    $product_priced = $product_priced + (floatval($buttonswatchPrice) * $quantity);

                    $extra_price = $extra_price*$quantity;
                }
                else if($type == "pant" && $row['cat_name'] == $cat_name) {
                    $product_priced = $row['cat_price'];
                    $quantity = $_REQUEST['quantity'];

                    if(isset($_REQUEST['prod_color'])) {
                        $color_code = $_REQUEST['prod_color'];
                        /*if(isFabricColorExists($color_code)) {
                            $sub_total = 1.5*30;
                            $product_priced = $product_priced+$sub_total;
                        }*/

                        $priceResponse = isFabricColorExists($color_code,"Custom Pant");
                        if($priceResponse["status"]) {
                            $extra_price = $priceResponse["price"];
                        }
                    }

                    $product_priced = $product_priced*$quantity;
                    $extra_price = $extra_price*$quantity;
                }

                else if($type == "vest" && $row['cat_name'] === $cat_name) {
                    $product_priced = $row['cat_price'];
                    $quantity = $_REQUEST['quantity'];

                    if(isset($_REQUEST['prod_color'])) {
                        $color_code = $_REQUEST['prod_color'];
                        /*if(isFabricColorExists($color_code)) {
                            $sub_total = 1*30;
                            $product_priced = $product_priced+$sub_total;
                        }*/

                        $priceResponse = isFabricColorExists($color_code,"Custom Vest");
                        if($priceResponse["status"]) {
                            $extra_price = $priceResponse["price"];
                        }
                    }

                    $product_priced = $product_priced*$quantity;
                    $extra_price = $extra_price*$quantity;
                }

                else if($type == "accessories" && $row['cat_name'] == $cat_name) {
                    $product_priced = $row['cat_price'];
                    $quantity = $_REQUEST['quantity'];
                    $product_priced = $product_priced * $quantity;
                }
                else if($type == "2pcsuit" && $row['cat_name'] == $cat_name) {
                    $product_priced = $row['cat_price'];
                    $quantity = $_REQUEST['quantity'];
                    if(isset($_REQUEST['prod_color'])) {
                        $color_code = explode(":",$_REQUEST['prod_color'])[0];
                        /*if(isFabricColorExists($color_code)) {
                            $sub_total = 3.5*30;
                            $product_priced = $product_priced+$sub_total;
                        }*/

                        $priceResponse = isFabricColorExists($color_code,"Custom Suit");
                        if($priceResponse["status"]) {
                            $extra_price = $priceResponse["price"];
                        }
                    }
                    $product_priced = $product_priced * $quantity;
                    $buttonswatchPrice = getButtonPrice('Custom Suit',''.$btnColor.'');
                    $product_priced = $product_priced + (floatval($buttonswatchPrice) * $quantity);
                    $extra_price = $extra_price*$quantity;
                }
                else if($type == "overcoat" && $row['cat_name'] == $cat_name) {
                    $product_priced = $row['cat_price'];
                    $quantity = $_REQUEST['quantity'];
                    if(isset($_REQUEST['prod_color'])) {
                        $color_code = $_REQUEST['prod_color'];
                        /*if(isFabricColorExists($color_code)) {
                            $sub_total = 3.5*30;
                            $product_priced = $product_priced+$sub_total;
                        }*/
                        $priceResponse = isFabricColorExists($color_code,"Custom Overcoat");
                        if($priceResponse["status"]) {
                            $extra_price = $priceResponse["price"];
                        }
                    }
                    $product_priced = $product_priced * $quantity;
                    $extra_price = $extra_price*$quantity;
                }
                /*if($quantity =='1' && $product_name=="Shirt, 1 piece"){
                    $product_priced = $product_price;
                }
                else if($quantity =='2' && $product_name=="Shirt, 2 pieces"){
                    $product_priced = $product_price;
                }
                else if($quantity =='3' && $product_name=="Shirt, 3 pieces"){
                    $product_priced = $product_price;
                }
                else if($quantity =='4' && $product_name=="Shirt, 4 pieces"){
                    $product_priced = $product_price;
                }
                else if($quantity =='5' && $product_name=="Shirt, 5 pieces"){
                    $product_priced = $product_price;
                }
                else if($product_name=="Full Canvas 2pc suit"){
                    $product_priced = $product_price;
                }
                if($product_name=="Full Canvas Jacket"){
                    $product_priced = $product_price;
                }
                if($product_name=="Pants"){
                    $product_priced = $product_price;
                }*/
            }
            $items = array("total_amount" => $product_priced,"extra_price"=>$extra_price);
        }
        else
        {
            $status = "no data found";
        }
    }
    else
    {
        $status = "Error".Mysql_error();
    }
}
else
{
    $status = "Missing Params";
}
$response['status']=$status;
$response['amount']=$items;
echo json_encode($response);
?>