<?php include("header.php");?>
	<div class="banner top-fabricbrands">
		<div class="">
		</div>
	</div>
	<!-- //Slider -->
</div>
	<!-- //Header -->
<div class="about-bottom wthree-3">
	<div class="container">
		<h2 class="tittle">TOP Fabrics <span style="color:#000;">brands</span></h2>
		<p>We agree that every man should own a classic blue and grey suit, but our fabrics invite you to make some more adventurous choices as well. Our in-house brand gives you high quality for an affordable price.</p></br>
		
		<p>Looking for something even more exclusive? We also collaborate with top European brands like the British Holland & Sherry, the French Dormeuil, the Belgian Scabal and the Italian Loro Piana, Ermenegildo Zegna and Tollegno, to name a few.</p></br>
		 <div class="gallery agileinfo" id="partner">
				<div class="container">
					<ul id="flexiselDemo3">
						<li><img src="images/topbrands/alumo.jpg" /></li>
						<li><img src="images/topbrands/botonificio.jpg" /></li>
						<li><img src="images/topbrands/canclini.jpg" /></li>
						<li><img src="images/topbrands/dormeuil.jpg" /></li>
						<li><img src="images/topbrands/ermenegildo.png" /></li>
						<li><img src="images/topbrands/hollend.jpg" /></li>
						<li><img src="images/topbrands/lord.jpg" /></li>
						<li><img src="images/topbrands/scabal.jpg" /></li>
						<li><img src="images/topbrands/vbc.jpg" /></li>
						<li><img src="images/topbrands/tessituramonti.jpg" /></li>
						<li><img src="images/topbrands/tollgno.jpg" /></li>
						
						
						
					</ul>
					<script type="text/javascript">
						$(window).load(function() {
							$("#flexiselDemo3").flexisel({
								visibleItems: 5,
								animationSpeed: 1000,
								autoPlay: true,
								autoPlaySpeed: 3000,    		
								pauseOnHover: true,
								enableResponsiveBreakpoints: true,
								responsiveBreakpoints: { 
									portrait: { 
										changePoint:480,
										visibleItems: 1
									}, 
									landscape: { 
										changePoint:640,
										visibleItems: 2
									},
									tablet: { 
										changePoint:768,
										visibleItems: 3
									}
								}
							});
							
						});
					</script>
<script type="text/javascript" src="js/jquery.flexisel.js"></script>
</div>
</div>
		
		
			
	</div>
</div>
<?php include("footer.php");?>