<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/22/2017
 * Time: 9:18 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class ORDERS
{
    public $link = null;
    public $bookClass = null;
    public $userClass = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function getParticularOrderData($order_id)
    {
        $link = $this->link->connect();
        $order_detail=array();
        if($link) {
            $query="select * from wp_orders where order_id='$order_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $orderData = mysqli_fetch_assoc($result);
                    $order_id = $orderData['order_id'];
                    $user_id = $orderData['order_user_id'];
                    $userData = $this->getParticularUserData($user_id);
                    $query2 = "select * from wp_orders_detail where det_order_id = '$order_id'";
                    $result2 = mysqli_query($link,$query2);
                    if($result2){
                        $num = mysqli_num_rows($result2);
                        if($num>0){
                            while($detail = mysqli_fetch_assoc($result2)){
                                $order_detail[] = $detail;
                            }
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Order Data Exist";
                        }
                        else{
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "Empty Order Details";
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                    $orderData['orderDetail'] = $order_detail;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $orderData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from wp_customers where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getFinalMeasurementData($meas_id){
        $link = $this->link->connect();
        $finalMeasurement   =   array();
        if($link) {
            $query="select * from final_measurements where measurement_id='$meas_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $finalMeasurement = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Measurement Data Find";
                    $this->response['finalMeasurement'] = $finalMeasurement;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Measurement Data not Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularOrderDetailData($orderRowId)
    {
        $link = $this->link->connect();
        $orderDetRowData   =   array();
        if($link) {
            $query="select * from wp_orders_detail where det_id='$orderRowId'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                        $detail = mysqli_fetch_assoc($result);
                        $orderDetRowData[]=$detail;

                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Detail Data Found";
                    $this->response['orderDetRowData'] = $orderDetRowData;
                    $this->getSupplierDetails();
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Order Detail Data not Found";
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getSupplierDetails()
    {
        $catArray = array();
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from wp_supplier order by sup_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while ($supData = mysqli_fetch_array($result)) {
                        $supArray[] = array(
                            "sup_id" => $supData['sup_id'],
                            "company_name" => $supData['company_name'],
                            "company_address1" => $supData['company_address1'],
                            "company_address2" => $supData['company_address2'],
                            "company_work_phone" => $supData['company_work_phone'],
                            "company_mobile_no" => $supData['company_mobile_no'],
                            "company_fax" => $supData['company_fax'],
                            "contact_first_name" => $supData['contact_first_name'],
                            "family_name" => $supData['family_name'],
                            "contact_email" => $supData['contact_email']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['supplierData'] = $supArray;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Supplier Found";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllOrders()
    {
        $link = $this->link->connect();
        $order_detail=array();
        $order=array();
        if($link) {
            $query="select * from orders order by order_id desc";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($row = mysqli_fetch_assoc($result)) {
                        $order_id = $row['order_id'];
                        $query2 = "select * from orderdetail where order_id = '$order_id'";
                        $result2 = mysqli_query($link, $query2);
                        if ($result2) {
                            $num = mysqli_num_rows($result2);
                            if ($num > 0) {
                                unset($order_detail);
                                while ($detail = mysqli_fetch_array($result2)) {
                                    $detail_id = $detail['item_id'];
                                    $temp = $this->bookClass->getParticularBookData($detail_id);
                                    $temp = $temp['bookData'];
                                    $order_detail[] = array(
                                        "detail_id" => $detail['detail_id'],
                                        "book_name" => $temp['book_name'],
                                        "book_author" => $temp['book_author'],
                                        "book_narrator" => $temp['book_narrator'],
                                        "play_time" => $temp['play_time'],
                                        "front_look" => $temp['front_look'],
                                        "audio_file" => $temp['audio_file'],
                                        "item_price" => $detail['item_price']
                                    );
                                }
                                $user_id = $row['order_userId'];
                                $userData = $this->userClass->getParticularUserData($user_id);
                                $userData = $userData['UserData'];
                                $order[] = array(
                                    "order_id" => $row['order_id'],
                                    "order_number" => $row['order_number'],
                                    "transaction_id" => $row['transaction_id'],
                                    "amount" => $row['amount'],
                                    "payment_status" => $row['payment_status'],
                                    "order_dated" => $row['order_dated'],
                                    "order_user_id" => $row['order_userId'],
                                    "order_user_name" => $userData['user_name'],
                                    "order_user_profile" => $userData['user_profile'],
                                    "order_detail" => $order_detail
                                );
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Order Data Exist";
                            } else {
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Empty Order Details";
                            }
                        } else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Order Found";
                    $this->response['order'] = $order;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Order Identification";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");

        echo json_encode($response);
    }
    public function cancelOrder($order_id)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from orders where order_id='$order_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE orders SET order_status='Cancelled' WHERE order_id='$order_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Order Has Been Cancelled Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function updateOrderData($order_id,$all_fields,$user_id){
        $array = explode(',', $all_fields);
        $OrderDate = $array[1].','.$array[2];
        $Status = $array[3];
        $Type = $array[4];
        $name = $array[5];
        $phone = $array[6];
        $weight = $array[7];
        $height = $array[8];
        $link = $this->link->connect();
        if($link) {
                $update = mysqli_query($link, "UPDATE wp_orders_detail SET det_product_name='$Type', WHERE det_id='$order_id'");
                $update = mysqli_query($link, "UPDATE wp_orders SET order_date='$OrderDate', order_payment_status='$Status' WHERE order_id='$order_id'");
                $update = mysqli_query($link, "UPDATE wp_customers SET name='$name', mobile='$phone',weight='$weight',height='$height' WHERE user_id='$user_id'");
            if ($update) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Order Has Been update Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }

        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function updateEditDetail($order_detail_id,$column,$value,$table){
        $link = $this->link->connect();
        if($link) {
            $update = mysqli_query($link, "UPDATE $table SET $column = '$value' WHERE det_id='$order_detail_id'");
            if($table =="final_measurements"){
                $update = mysqli_query($link, "UPDATE $table SET $column = '$value' WHERE measurement_id='$order_detail_id'");
            }
            if ($update) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Order Has Been update Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function searchOrder($text) {
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_orders_detail,wp_orders where wp_orders_detail.det_product_name like '%$text%' 
            and wp_orders.order_id=wp_orders_detail.det_order_id";
            $result = mysqli_query($link, $query);
            if($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $orderData = array();
                    while ($rows = mysqli_fetch_assoc($result)) {
                        $userResp = $this->userClass->getParticularUserData($rows['order_user_id']);
                        $userName = $userResp['UserData']['name'];
                        $rows['user_name'] = $userName;
                        $orderData [] = $rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $orderData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Data Found";
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function searchOrder2($type) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
            return $this->response;
        }
        $query ="";
        if($type === 'jacket') {
            $query ="select * from wp_orders_detail,wp_orders where product_type='Custom Suit' and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else if($type === 'trousers') {
            $query ="select * from wp_orders_detail,wp_orders where product_type='Custom Pant' and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else if($type === 'overcoat') {
            $query ="select * from wp_orders_detail,wp_orders where product_type='Collection' and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else if($type === 'suit') {
            $query ="select * from wp_orders_detail,wp_orders where (product_type='Custom Suit' or product_type='Custom 2Pc Suit') and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else if($type == 'shirt') {
            $query ="select * from wp_orders_detail,wp_orders where product_type='Custom Shirt' and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else if($type === 'vest') {
            $query ="select * from wp_orders_detail,wp_orders where product_type='Custom Vest' and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else if($type === 'all') {
            $query ="select * from wp_orders_detail,wp_orders where wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        else{
            $query ="select * from wp_orders_detail,wp_orders where det_product_name like '%$type%' and wp_orders.order_id=wp_orders_detail.det_order_id";
        }
        $result = mysqli_query($link,$query);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $query;
            return $this->response;
        }
        $data = array();
        $count = mysqli_num_rows($result);
        if($count == 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $userResp = $this->userClass->getParticularUserData($rows['order_user_id']);
            $userName = $userResp['UserData']['name'];
            $rows['user_name'] = $userName;
            $data [] = $rows;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found ".$query;
        $this->response["data"] = $data;
        return $this->response;
    }

    public function updateManualCode($det_id,$manual_code){
        $link = $this->link->connect();
        if($link) {
            $update = mysqli_query($link, "UPDATE wp_orders_detail SET manual_code = '$manual_code' WHERE det_id='$det_id'");
            if ($update) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Code Has Been update Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
}
?>