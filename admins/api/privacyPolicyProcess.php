<?php
require_once ('Classes/PRIVACY_POLICY.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$priv = new \Classes\PRIVACY_POLICY();
$requiredfields = array('type');

($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $priv->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "updatePrivacyPolicy"){

    $requiredfields = array('editor1');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $priv->apiResponse($response);
        return false;
    }
    $editor1 = trim($_POST['editor1']);

    $response = $priv->updateSalesTerms($editor1);
    if($response[STATUS] == Error){
        $priv->apiResponse($response);
        return false;
    }
    $priv->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $priv->apiResponse($response);
}
?>