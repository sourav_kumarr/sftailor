<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/USERCLASS.php');
require_once('api/Classes/SUPPLIER.php');

$conn = new \Classes\CONNECT();
$supClass = new \Classes\SUPPLIER();
$userClass = new \Classes\USERCLASS();
$allUsersData = $userClass->getAllUsersData();
$gettermsData = $supClass->getTermsData();
$user_id = $_REQUEST['uid'];
echo "<input type='hidden' id='user_id' value='" . $user_id . "'>";
?>
<style>.shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }

    .hideloader {
        display: none;
    }

    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }

    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }

    #create_stores {
        background: white none repeat scroll 0% 0%;
        padding: 37px 25px !important;
        border-radius: 4px;
        width: 1062px;
        height: auto;
        min-height: 525px;
        position: inherit;
        z-index: 9999;
        left: -224px;
        top: -102px;
    }

    @media print {
        body * {
            visibility: hidden;
        }

        #section-to-print, #section-to-print * {
            visibility: visible;
        }

        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }

    #send_supplier_data {
        background: white none repeat scroll 0 0;
        border-radius: 4px;
        left: 182px;
        padding: 37px 25px !important;
        top: 19px;
        z-index: 999999;
    }

    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top: 0;
    }

    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }

    #emails {
        width: 100%
    }

    .select2-selection.select2-selection--multiple {
        border-color: silver !important;
        border-radius: 0 !important;
        height: auto !important;
        width: 565px !important;
    }

    .select2-results {
        width: 565px !important;
    }

    .select2-search__field {
        width: 180px !important;
    }

    .bootstrap-dialog {
        z-index: 9999 !important;
    }

    .ddkfxx ul li {
        border: 1px solid #ccc;
        width: 100% !important;
    }

    .ddkfxx ul li .p1 {
        border-right: 1px solid #ccc;
        padding-left: 4px;
        width: 30% !important;
    }

    .ddkfxx ul li .p1 img {
        height: 37px;
    }

    .ddkfxx ul li .p2 {
        border-bottom: medium none !important;
    }

    .ddkfxx ul li {
        border-left: none !important;
        margin-bottom: 0 !important;
    }

    .ddkfxx ul {
        border-left: 1px solid;
    }

    .nextg li {
        border-bottom: medium none !important;
    }

    .layui-layer-shade {
        height: 100vh;
        width: 221vh;
    }

    .ddkfxx ul li .p2 {
        width: auto !important;
    }

    .layui-layer-title {
        overflow: inherit !important;
    }

    .ipfield {
        width: 400px;
    }

    .ipfields {
        width: 90px;
    }

    .cartimg {
        height: 60px !important;
    }

    .nav.side-menu > li > a, .nav.child_menu > li > a {
        font-size: 13px;
    }

    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top: 0;
    }

    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }

    #orderTable_filter {
        display: none;
    }

    .search-container {
        display: flex;
        flex-direction: row;
    }

    .search-remove {
        display: none;
    }

    .no-gutter {
        margin: 0px;
        padding: 0px;
    }

    #searchdataa_filter label input {
        border: 1px solid #ccc;
        border-radius: 2px;
        height: 35px;
    }

    #searchdataa_filter {
        margin-top: -35px;
    !important;
        width: 25%;
    }

    #searchdata_length {
        display: none;
    }

    .view-detail {
        border-bottom: 1px solid blue;
        padding-bottom: 2px;
        cursor: pointer;
    }

    label {
        font-weight: 500;
    }

    #openActivity_length {
        display: none
    }

    #activityHistory_length {
        display: none
    }

    #openActivity {
        border-top: 4px solid thistle;
        border-bottom: 1px solid aliceblue;
    }

    #activityHistory {
        border-top: 4px solid #a5dba5;
        border-bottom: 1px solid aliceblue;
    }

    #activityHistory_info {
        display: none
    }

    #openActivity_info {
        display: none
    }

    .text-muted {
        margin-bottom: -15px;
    }

    #orderTable_length {
        display: none
    }

    #related_to_length {
        display: none
    }

    #related_to_filter {
        display: none
    }

    #related_to_info {
        display: none
    }

    #related_to_info {
        display: none
    }

    #related_to_paginate {
        display: none
    }

    #related_to_wrapper {
        float: right;
        width: 29%;
        margin-right: 62px;
        margin-top: 34px;
        min-height: 100px !important;
        overflow-y: auto;
        height: 226px;
        overflow-x: hidden;
    }

    .header_strip {
        height: 40px;
        background: lightblue;
        padding: 9px;
    }
</style>
<!-- page content -->


<div style="display: none;" id="terms-message">
    <?php echo $gettermsData['termsData'][0]['message']; ?>
</div>
<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Customer relationship Manager
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px"
                                        onclick="window.location='api/excelProcess.php?dataType=allOrders'"
                                        class="btn btn-info btn-sm">Download Excel File
                                </button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control"
                                               name="startDate" id="startFilter"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate"
                                               id="endFilter"/>
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton"
                                           style="margin-top: 5px"/>
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

                    <!--Message Box-->
                    <link rel="stylesheet" href="css/select2.min.css" type="text/css" media="all">
                    <script src="js/jquery.min.js"></script>
                    <script src="js/location.js"></script>
                    <!--<script src="js/bootstrap.min.js"></script>-->
                    <script src="js/moment.min.js"></script>
                    <script src="js/custom.min.js"></script>
                    <script src="js/jquery.dataTables.min.js"></script>
                    <script src="js/bootstrap-toggle.min.js"></script>
                    <script src="js/bootstrap-datetimepicker.js"></script>
                    <script src="js/myscript.js"></script>
                    <script src="../js/jquery-ui.js" type="text/javascript"></script>
                    <script type="text/javascript" src="js/select2.min.js"></script>

                    <link rel="stylesheet" href="css/select2.min.css" type="text/css" media="all">
                    <script type="text/javascript" src="js/select2.min.js"></script>

                    <?php if($_REQUEST['t']=="u"){ ?>
                <button type="button" style="opacity: 0;" id="mymodal" class="btn btn-info btn-lg email" data-toggle="modal" data-target="#exampleModal">Send mail</button>
                <!-- Modal email person -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false" >
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">

                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Recipient To :</label></br>
                                    <input type="text" id="emailsd" value="" class="form-control" multiple="multiple" style="width:100%"></input>
                                </div>
                                <div class="form-group">
                                    <label for="recipient-name" class="form-control-label">Add CC :</label></br>
                                    <input type="text" id="cccs" value="" class="form-control" multiple="multiple" style="width:100%"></input>
                                </div>

                                <div class="form-group">
                                    <label for="message-text" class="form-control-label">Message:</label>
                                    <textarea class="form-control" id="message-text"></textarea>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="message-text" style="padding-right: 20px;" class="form-control-label">Send copy to</br><?php echo $_SESSION['SFTAdminEmail'];?>:</label>
                                    <input type="checkbox" checked class="" name="copy" id="copy" style="position: absolute; display: contents;">
                                </div>

                            </div></br></br>
                            <div class="modal-footer">
                                <input type="hidden" id="oid" value="">
                               <button type="button" class="btn btn-secondary" onclick="reloaded()">Close</button>
                                <button type="button" class="btn btn-primary" onclick=sendMultiEmail('<?php echo $_REQUEST['_'];?>')>Send Mail</button>
                            </div>
                        </div>
                    </div>
                </div>
                    <!--Call a log model-->

                    <div class="modal fade" id="newTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false" >
                        <div class="modal-dialog" role="document" style="width: 70%;">
                            <div class="modal-content">
                                <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                    <h5 class="modal-title" id="exampleModalLabel">New Task</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group" style="float: right;width: 35%;">
                                        <label for="recipient-name" class="form-control-label">Related To :</label></br>
                                        <select id="newtask_related" style="width: 80%;height: 32px;border: 1px solid #ddd;">
                                            <?php
                                            $link = $conn->connect();
                                            //for sftailor
                                            if ($link) {
                                            $query99 = "select * from wp_crm_activities where activity_type='event_a' order by c_id desc";
                                            $result99 = mysqli_query($link, $query99);
                                            if ($result99) {
                                            $num99 = mysqli_num_rows($result99);
                                            if ($num99 > 0) {
                                            while ($usersData99 = mysqli_fetch_array($result99)) {
                                            ?>
                                            <option value="<?php echo $usersData99['related_to'];?>"><?php echo $usersData99['related_to'];?></option>
                                            <?php } } } } ?>
                                             <option value="Add Event">Add Event</option>

                                        </select>
                                         <div class="form-group" id="newtask_event" style="float: right; width: 100%;margin-top: 32px;display: none">
                                        <label for="recipient-name" class="form-control-label">Add Event</label></br>
                                        <input type="text" id="event_1" value="" class="form-control" style="width: 80%;height: 32px;border: 1px solid #ddd;"></input>
                                        <button type="button" class="btn btn-primary" onclick=addEvent('event_a','1')>Save</button>
                                    </div>
                                    </div>


                                 <div class="form-group" style="width: 100%;margin-top: 32px;">
                                    <table id="related_to" class="table table-striped table-bordered">
                                    <thead>
                                    <tr id="relate_to" style="" class="relate-td">
                                        <th>#</th>
                                        <th>Related to</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                     <tbody>
                                    <?php
                                    $link = $conn->connect();//for sftailor
                                    if ($link) {
                                    $query4 = "select * from wp_crm_activities where activity_type='event_a' order by c_id desc";
                                    $result4 = mysqli_query($link, $query4);
                                    if ($result4) {
                                    $num4 = mysqli_num_rows($result4);
                                    if ($num4 > 0) {
                                    $o=0;
                                    while ($usersData4 = mysqli_fetch_array($result4)) {
                                    $o++;
                                    ?>
                                    <tr>
                                        <td data-title='#'><?php echo $o; ?></td>
                                        <td data-title='#'><input type="text" id="new_event_edit_<?php echo$usersData4['c_id']; ?>_a" value="<?php echo$usersData4['related_to']; ?>" style="display: none;width: 100px;"> <span id="new_event_r_<?php echo $usersData4['c_id']; ?>_a"> <?php echo$usersData4['related_to']; ?></span></td>
                                        <td data-title='#'>
                                            <i style="color: orange;cursor: pointer" id="edit_div_<?php echo $usersData4['c_id']; ?>_a" class="fa fa-pencil 2x" onclick=editEventNew('<?php echo $usersData4['c_id']; ?>','a'); > </i>
                                            <i style="color: green;cursor: pointer;display:none;" id="update_div_<?php echo $usersData4['c_id']; ?>_a" class="fa fa-floppy-o 2x" onclick=updateEventNew('<?php echo $usersData4['c_id']; ?>','a'); > </i>
                                            <i style="color: red;cursor: pointer;float:right" class="fa fa-trash 2x" onclick=deleteActivity('<?php echo$usersData4['c_id']; ?>'); > </i>
                                        </td>
                                    </tr>

                                <?php } } } } ?>
                                     </tbody>
                                     </table>
                                </div>


                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Assign To :</label></br>
                                        <input type="text" id="newtask_assing" value="" class="form-control" multiple="multiple" style="width:50%"></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Subject :</label></br>
                                        <input type="text" id="newtask_subject" value="" class="form-control" multiple="multiple" style="width:50%"></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Due Date :</label></br>
                                        <input type="text" id="newtask_duedate" value="" class="form-control" multiple="multiple" style="width:50%"><span style="margin-top: -28px;position: absolute;left: 51%;font-weight: bold;color: green;"> | <?php echo date("m/d/Y");?></span>
                                    </div>

                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">Comment:</label>
                                        <textarea class="form-control" id="newtask_comment" style="width: 50%"></textarea>
                                    </div>
                                </div></br></br>
                                <div class="modal-footer">
                                    <input type="hidden" id="oid" value="">
                                    <button type="button" class="btn btn-secondary" onclick="reloaded()">Close</button>
                                    <button type="button" class="btn btn-primary" onclick=saveTask('taskevent')>Save</button>
                                </div>
                            </div>
                        </div>
                    </div>

                <!--Open Activities table-->

                <div class="x_content">
                    <p class="text-muted font-15 m-b-30" style="font-weight: bold;color: #202020;font-size: 14px;">
                        Open Activities <button type="button" style="margin-left: 97px;" id="mymodal" class="btn btn-info btn-sm email" data-toggle="modal" data-target="#newTask">Log to do</button>
                    </p>
                    <table id="openActivity" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Subject</th>
                            <th>Assign To</th>
                            <th>Task</th>
                            <th>Due Date</th>
                            <th>Status</th>
                            <th>Related To</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $link = $conn->connect();//for sftailor
                        if ($link) {
                            $query7 = "select * from wp_crm_activities where activity_type='taskevent' and user_id='$user_id' order by c_id desc";
                            $result7 = mysqli_query($link, $query7);
                            if ($result7) {
                                $num7 = mysqli_num_rows($result7);
                                if ($num7 > 0) {
                                    $j=0;
                                    while ($usersData7 = mysqli_fetch_array($result7)) {
                                        $j++;
                                        ?>
                                        <tr>
                                            <td data-title='#'><?php echo $j ?></td>
                                            <td><?php echo $usersData7['subject'];?></td>
                                            <td><?php echo $usersData7['assign_to'];?></td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><?php
                                              $date = date_create($usersData7['due_date']);
                                              echo date_format($date,"m/d/Y");
                                             ?></td>
                                            <td>Not Started</td>
                                            <td><?php echo $usersData7['related_to'];?></td>
                                            <td style="text-align: center" onclick=deleteActivity('<?php echo $usersData7['c_id']; ?>'); ?> <i class="fa fa-trash" style="font-size: 18px;color: tomato;cursor: pointer;"></i> </td>
                                        </tr>
                                    <?php     }
                                }
                            }
                        }  ?>
                        </tbody>
                    </table>
                </div>

                 <div class="modal fade" id="mailnewTask" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false" >
                    <div class="modal-dialog" role="document" style="width: 70%;">
                        <div class="modal-content">
                            <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                <h5 class="modal-title" id="exampleModalLabel">New Call</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <p class="header_strip">
                                        <label class="pull-left"><strong>Task Information</strong></label>
                                        <label class="pull-right" style="color:red">*Required Fields</label>
                                    </p>
                                    <div class="col-md-6">
                                       <!-- <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Assign To :</label>
                                            <input type="text" id="newtask_assing" value="" class="form-control"/>
                                        </div>-->
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Subject :</label>
                                            <input title ="text" type="text" id="newtask_subject_email" value="" class="form-control" />
                                        </div>
                                       <!-- <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Due Date :</label>
                                            <input type="text" id="newtask_duedate" value="" class="form-control" >
                                            <span style="margin-top:-26px;position:absolute;right:20px;font-weight:bold;color: green"> | <?php /*echo date("m/d/Y");*/?></span>
                                        </div>-->
                                        <div class="form-group">
                                            <label for="message-text" class="form-control-label">Comment:</label>
                                            <textarea class="form-control" id="newtask_comment_email"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="recipient-name" class="form-control-label">Related To :</label>
                                            <select id="newtask_related_email" class="form-control" >
                                                <?php
                                                $link = $conn->connect();//for sftailor
                                                if ($link) {
                                                    $query99 = "select * from wp_crm_activities where activity_type='event_a' order by c_id desc";
                                                    $result99 = mysqli_query($link, $query99);
                                                    if ($result99) {
                                                        $num99 = mysqli_num_rows($result99);
                                                        if ($num99 > 0) {
                                                            while ($usersData99 = mysqli_fetch_array($result99)) {
                                                                ?>
                                                                <option value="<?php echo $usersData99['related_to'];?>"><?php echo $usersData99['related_to'];?></option>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                <option value="Add Event">Add Event</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="newtask_event">
                                            <label for="recipient-name" class="form-control-label">Add Event</label></br>
                                            <input type="text" id="event_1" value="" class="form-control"/>
                                            <button type="button" class="btn btn-primary pull-right" style="margin-top:10px" onclick=addEvent('event_a','1')>Save</button>
                                        </div>
                                        <div class="clearfix"></div>
                                        <table id="related_to" style="margin-top:10px" class="table table-striped table-bordered">
                                            <thead>
                                            <tr id="relate_to" class="relate-td">
                                                <th>#</th>
                                                <th>Related to</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php
                                            $link = $conn->connect();//for sftailor
                                            if ($link) {
                                                $query4 = "select * from wp_crm_activities where activity_type='event_a' order by c_id desc";
                                                $result4 = mysqli_query($link, $query4);
                                                if ($result4) {
                                                    $num4 = mysqli_num_rows($result4);
                                                    if ($num4 > 0) {
                                                        $o=0;
                                                        while ($usersData4 = mysqli_fetch_array($result4)) {
                                                            $o++;
                                                            ?>
                                                            <tr>
                                                                <td data-title='#'><?php echo $o; ?></td>
                                                                <td data-title='#'><input title="text" type="text" id="new_event_edit_<?php echo$usersData4['c_id']; ?>_a" value="<?php echo$usersData4['related_to']; ?>" style="display: none;width: 100px;"> <span id="new_event_r_<?php echo $usersData4['c_id']; ?>_a"> <?php echo$usersData4['related_to']; ?></span></td>
                                                                <td data-title='#'>
                                                                    <i style="color: orange;cursor: pointer" id="edit_div_<?php echo $usersData4['c_id']; ?>_a" class="fa fa-pencil 2x" onclick=editEventNew('<?php echo $usersData4['c_id']; ?>','a'); > </i>
                                                                    <i style="color: green;cursor: pointer;display:none;" id="update_div_<?php echo $usersData4['c_id']; ?>_a" class="fa fa-floppy-o 2x" onclick=updateEventNew('<?php echo $usersData4['c_id']; ?>','a'); > </i>
                                                                    <i style="color: red;cursor: pointer;float:right" class="fa fa-trash 2x" onclick=deleteActivity('<?php echo$usersData4['c_id']; ?>'); > </i>
                                                                </td>
                                                            </tr>
                                                        <?php } } } } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="oid" value="">
                                <input type="hidden" id="tasktype" value="">
                                <button type="button" class="btn btn-secondary" onclick="reloaded()">Close</button>
                                <button type="button" class="btn btn-primary" onclick=saveTaskEmail()>Save</button>
                            </div>
                        </div>
                    </div>
                </div>



            <!--send and email task modal-->

                    <div class="modal fade" id="sendanEmail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="false" >
                        <div class="modal-dialog" role="document" style="width: 50%;">
                            <div class="modal-content">
                                <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                    <h5 class="modal-title" id="exampleModalLabel">Send an Email</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="padding: 30px;">
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">To :</label></br>
                                        <input type="text" id="email_to" value="" class="form-control" multiple="multiple" style="width:50%"></input>
                                    </div>

                                    <div class="form-group" style="width: 35%;">
                                        <label for="recipient-name" class="form-control-label">Related To :</label></br>
                                        <select id="email_related" style="width: 100%;height: 32px;border: 1px solid #ddd;">
                                            <?php
                                            $link = $conn->connect();//for sftailor
                                            if ($link) {
                                            $query99 = "select * from wp_crm_activities where activity_type='event_b'  order by c_id desc";
                                            $result99 = mysqli_query($link, $query99);
                                            if ($result99) {
                                            $num99 = mysqli_num_rows($result99);
                                            if ($num99 > 0) {
                                            while ($usersData99 = mysqli_fetch_array($result99)) {
                                            ?>
                                            <option value="<?php echo $usersData99['related_to'];?>"><?php echo $usersData99['related_to'];?></option>
                                            <?php } } } } ?>
                                             <option value="Add Event">Add Event</option>

                                        </select>

                                         <div class="form-group" id="emails_events" style="float: right; width: 100%;margin-top: 32px;display: none">
                                            <label for="recipient-name" class="form-control-label">Add Event</label></br>
                                            <input type="text" id="event_2" value="" class="form-control" style="width: 80%;height: 32px;border: 1px solid #ddd;"></input>
                                            <button type="button" class="btn btn-primary" onclick=addEvent('event_b','2')>Save</button>
                                         </div>
                                    </div>

                                    <div class="form-group" style="width: 40%;margin-top: -121px;float: right;overflow-y: auto;height: 192px;">
                                        <table id="related_to" class="table table-striped table-bordered">
                                        <thead>
                                        <tr id="relate_to" style="" class="relate-td">
                                            <th>#</th>
                                            <th>Related to</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                         <tbody>
                                        <?php
                                        $link = $conn->connect();//for sftailor
                                        if ($link) {
                                        $query4 = "select * from wp_crm_activities where activity_type='event_b' order by c_id desc";
                                        $result4 = mysqli_query($link, $query4);
                                        if ($result4) {
                                        $num4 = mysqli_num_rows($result4);
                                        if ($num4 > 0) {
                                        $o=0;
                                        while ($usersData4 = mysqli_fetch_array($result4)) {
                                        $o++;
                                        ?>
                                        <tr>
                                            <td data-title='#'><?php echo $o; ?></td>
                                            <td data-title='#'><input type="text" id="new_event_edit_<?php echo$usersData4['c_id']; ?>_e" value="<?php echo$usersData4['related_to']; ?>" style="display: none;width: 100px;"> <span id="new_event_r_<?php echo $usersData4['c_id']; ?>_e"> <?php echo$usersData4['related_to']; ?></span></td>
                                            <td data-title='#'>
                                                <i style="color: orange;cursor: pointer" id="edit_div_<?php echo $usersData4['c_id']; ?>_e" class="fa fa-pencil 2x" onclick=editEventNew('<?php echo $usersData4['c_id']; ?>','e'); > </i>
                                                <i style="color: green;cursor: pointer;display:none;" id="update_div_<?php echo $usersData4['c_id']; ?>_e" class="fa fa-floppy-o 2x" onclick=updateEventNew('<?php echo $usersData4['c_id']; ?>','e'); > </i>
                                                <i style="color: red;cursor: pointer;float:right" class="fa fa-trash 2x" onclick=deleteActivity('<?php echo$usersData4['c_id']; ?>'); > </i>
                                            </td>
                                        </tr>

                                    <?php } } } } ?>
                                         </tbody>
                                         </table>
                                    </div>

                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">CC :</label></br>
                                        <input type="text" id="email_cc" value="" class="form-control" multiple="multiple" style="width:50%"></input>
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">BCC :</label></br>
                                        <input type="text" id="email_bcc" value="" class="form-control" multiple="multiple" style="width:100%">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Subject :</label></br>
                                        <input type="text" id="email_subject" value="" class="form-control" multiple="multiple" style="width:100%">
                                    </div>
                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">Body:</label>
                                        <textarea class="form-control" id="email_comment" style="min-height: 150px"></textarea>
                                    </div>
                                </div></br></br>
                                <div class="modal-footer">
                                    <input type="hidden" id="oid" value="">
                                    <button type="button" class="btn btn-secondary" onclick="reloaded()">Close</button>
                                    <button type="button" class="btn btn-primary" onclick=sendanEmail('sendademail')>Send</button>
                                </div>
                            </div>
                        </div>
                    </div>

            <!--Activity History table-->

                <div class="x_content">
                    <p class="text-muted font-15 m-b-30" style="font-weight: bold;color: #202020;font-size: 14px;">
                        Activity History
                        <button type="button" style="margin-left:50px" id="mymodal" class="btn btn-info btn-sm email" data-toggle="modal" data-target="#mailnewTask" >Log Call</button>
                        <button type="button" style="margin-left: 97px;" id="mymodal" class="btn btn-info btn-sm email" data-toggle="modal" data-target="#sendanEmail">Send an Email</button>
                    </p>
                    <table id="activityHistory" class="table table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Assign To</th>
                            <th>Subject</th>
                            <th>Related To</th>
                            <th>Message</th>
                            <th>Last Modified</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php
                        $link = $conn->connect();//for sftailor
                        if ($link) {
                            $query4 = "select * from wp_crm_activities where activity_type='sendademail' and user_id='$user_id' order by c_id desc";
                            $result4 = mysqli_query($link, $query4);
                            if ($result4) {
                                $num4 = mysqli_num_rows($result4);
                                if ($num4 > 0) {
                                    $j=0;
                                    while ($usersData4 = mysqli_fetch_array($result4)) {
                                        $j++;
                                        ?>
                                        <tr>
                                            <td data-title='#'><?php echo $j ?></td>
                                            <td><?php echo $usersData4['assign_to'];?></td>
                                            <td><?php echo $usersData4['due_date'];?></td>
                                            <td><?php echo $usersData4['related_to'];?></td>
                                            <td><?php echo substr($usersData4['comment'],0,15);?></td>
                                            <td><?php
                                              $date = date_create($usersData4['created_date']);
                                              echo date_format($date,"m/d/Y H:i:s");
                                            ?></td>
                                            <td style="text-align: center" onclick=deleteActivity('<?php echo $usersData4['c_id']; ?>'); ?> <i class="fa fa-trash" style="font-size: 18px;color: tomato;cursor: pointer;"></i> </td>
                                        </tr>
                                    <?php     }
                                }
                            }
                        }  ?>
                        </tbody>
                    </table>
                    </div>


                    <div class="x_content">
                        <p class="text-muted font-15 m-b-30" style="font-weight: bold;color: #202020;font-size: 14px;margin-bottom: 30px;">
                            Order History
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered" style="border-top: 4px solid cadetblue">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Product Name</th>
                                <th>Image</th>
                                <th>Price</th>
                                <th>Customer</th>
                                <th>Last Order Activity</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php

                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $storeName = $_SESSION['SFT_Store_Name'];
                            $user_id = $_REQUEST['uid'];
                            $query = "select * from wp_orders_detail,wp_orders where wp_orders.order_id=wp_orders_detail.det_order_id and wp_orders.order_user_id = wp_orders_detail.det_user_id and wp_orders.order_user_id = '$user_id' and store_name='$storeName' order by det_id desc";
                            $result = mysqli_query($link, $query);
                            if ($result) {
                            $num = mysqli_num_rows($result);
                            if ($num > 0) {
                            $j = 0;
                            while ($orderData = mysqli_fetch_array($result)) {
                            $j++;
                            $amount = $orderData['det_price'];
                            $user_id = $orderData['det_user_id'];
                            $det_id = $orderData['det_id'];
                            $prod_type = $orderData['product_type'];
                            $order_number = $orderData['order_number'];
                            $order_date = $orderData['order_date'];
                            $det_quantity = $orderData['det_quantity'];
                            $det_price = $orderData['det_price'];
                            $det_prod_name = $orderData['det_product_name'];
                            $store_name = $orderData['store_name'];
                            $payment_status = $orderData['order_payment_status'];
                            $product_image = $orderData['product_image'];
                            $access_category = $orderData['access_category'];
                            $email_status = $orderData['email_status'];

                            if($amount=="") {
                                continue;
                            }
                            ?>
                            <tr>
                                <td data-title='#'><?php echo $j ?></td>

                                <td data-title='Name'><?php echo $orderData['det_product_name']." | ".$orderData['order_number']; ?><i onclick=expend('<?php echo $det_id;?>') class="fa fa-bars expends_<?php echo $orderData['order_id'];?>" aria-hidden="true" style="float: right;cursor: pointer;color: salmon;font-size: 19px;"></i>
                                </td>
                                <td data-title='Image'>
                                    <?php
                                    $order_product_image = "";
                                    switch ($prod_type) {
                                        case "Custom Suit":
                                            $order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                                            break;
                                        case "Custom 2Pc Suit":
                                            $order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                                            break;
                                        case "Custom Shirt":
                                            $order_product_image = "http://scan2fit.com/sftailor/images/shirts/57bandcollar.png";
                                            break;
                                        case "Custom Pant":
                                            $order_product_image = "http://scan2fit.com/sftailor/images/pants/formal.jpg";
                                            break;
                                        case "Collection":
                                            $order_product_image = $product_image;
                                            break;
                                        case "Accessories":
                                            $order_product_image = $product_image;
                                            break;
                                    }
                                    echo "<img src='$order_product_image' style='width: 35px;height: 35px;'/>";
                                    ?>
                                </td>
                                <td data-title='Price'>$<?php echo $amount*$det_quantity ?></td>
                                <td data-title='Customer' >
                                    <?php
                                    $userResp = $userClass->getParticularUserData($orderData['order_user_id']);
                                    $userName = $userResp['UserData']['name'];
                                    $useremail = $userResp['UserData']['email'];
                                    echo $userName;
                                    ?><input type="hidden" id="customerName" value="<?php echo $userName;?>"><input type="hidden" id="customeremail" value="<?php echo $useremail;?>"> </td>
                                <td data-title='Action'>
                                    <?php echo $order_date;?>
                                </td>
                                <td data-title='Action'><span  class="view-detail" onclick="get_product_d('<?php echo $user_id?>','<?php echo $det_id?>','<?php echo $prod_type?>', '<?php echo $order_number?>', '<?php echo $order_date?>', '<?php echo $det_quantity?>', '<?php echo $det_price?>',
                                            '<?php echo $det_prod_name?>', '<?php echo $store_name?>', '<?php echo $payment_status?>', '<?php echo $orderData['order_id']?>')">View Details</span></td>
                            </tr>

                            <?php if($email_status ==1){ ?>
                            <div>
                                <?php if($j % 2 ==0)
                                    $color ="rgb(112, 192, 118) none repeat scroll 0% 0%";
                                else $color="rgb(125, 162, 192) none repeat scroll 0% 0%";
                                ?>
                            <thead class="expand_<?php echo $det_id;?>" style="background: <?php echo $color;?>">
                            <tr>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Serial No. </th>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Email Sent</th>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Order No.</th>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Date</th>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Time</th>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Customer Name</th>
                                <th style="padding: 11px 0px 8px 13px;border-top: 22px solid white;font-size: 12px;border-right: 1px solid #fff;border-bottom: none;">Email Again</th>
                            </tr>
                            </thead>
                            <tbody class="expand_<?php echo $det_id;?>">
                            <?php if($j % 2 ==0)
                                $color ="rgb(165, 216, 169) none repeat scroll 0% 0%";
                            else $color="rgb(165, 197, 222) none repeat scroll 0% 0%";
                            ?>
                            <?php
                            $orderId = $orderData['order_id'];
                            $query1 = "select * from wp_crm_emails where order_id='$orderId' order by crm_id desc";
                            $result1 = mysqli_query($link, $query1);
                            if ($result1) {
                                $num1 = mysqli_num_rows($result1);
                                if ($num1 > 0) {
                                    $k = 0;
                                    while ($emailData = mysqli_fetch_array($result1)) {
                                        $k++;
                                        $email_person = $emailData['email_person'];
                                        $email_time = $emailData['email_time'];
                                        $date_time = (explode(" ",$email_time));
                                        $date = $date_time[0];
                                        $time = $date_time[1];
                                        $email_subject = $emailData['email_subject'];

                                        ?>
                                        <tr style="background:<?php echo $color;?>">
                                            <td data-title='#'      style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><?php echo $k;?></td>
                                            <td data-title='email'  style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><?php echo $email_person;?></td>
                                            <td data-title='email'  style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><?php echo $order_number; ?></td>
                                            <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><?php echo $date;?></td>
                                            <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><?php echo $time;?></td>
                                            <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><?php echo $userName;?></td>
                                            <td data-title='email'  style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal_<?php echo $orderData['order_id'];?>" data-whatever="@mdo" style="margin-left: -1px;height: 19px;padding: 10px;line-height: 0;border-radius: 2px;" onclick=openboxE('<?php echo $orderData['order_id'];?>')>Emails</button></td>
                                        </tr>
                                        <?php
                                    }
                                }
                            }
                            ?>
                            </tbody>
                    </div>

                    <?php } else { ?>

                        <div>
                            <?php if($j % 2 ==0)
                                $color ="rgb(112, 192, 118) none repeat scroll 0% 0%";
                            else $color="rgb(125, 162, 192) none repeat scroll 0% 0%";
                            ?>

                            <tbody class="expand_<?php echo $det_id;?>">
                            <?php if($j % 2 ==0)
                                $color ="rgb(165, 216, 169) none repeat scroll 0% 0%";
                            else $color="rgb(165, 197, 222) none repeat scroll 0% 0%";
                            ?>
                            <tr style="background:<?php echo $color;?>">
                                <td data-title='#'      style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;"></td>
                                <td data-title='email'  style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;"></td>
                                <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;">No Records Found</td>
                                <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;"></td>
                                <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;"></td>
                                <td data-title='time'   style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;"></td>
                                <td data-title='email'  style="padding: 5px 1px 5px 11px;border-bottom: 22px solid white;border-left: none;border-right: 1px solid #fff;"><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal_<?php echo $orderData['order_id'];?>" data-whatever="@mdo" style="margin-left: -1px;height: 19px;padding: 10px;line-height: 0;border-radius: 2px;" onclick=openboxE('<?php echo $orderData['order_id'];?>')>Emails</button></td>
                            </tr>
                            </tbody>
                        </div>

                    <?php } ?>


                    <div class="layui-layer-shade" id="layui-layer-shade5" times="5"
                         style="z-index:19891018; display:none;background-color:#000; opacity:0.3; filter:alpha(opacity=30);"
                         onclick="close_diolog()"></div>

                    <div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5"
                         type="page" times="5"
                         showtime="0" contype="string"
                         style="z-index: 19891019; top: 25px; height: 500px; left: 5%; width:90%;display:none;">

                        <div style="float: right; text-align: center; width: 87px; height: 26px; margin-top: 6px; border-radius: 2px; line-height: 1.8; font-weight: 600; background: darksalmon none repeat scroll 0% 0%; color: rgb(238, 238, 238); margin-right: 19px;">
                            <input type='button' id='btn' value='Print' onclick='printDiv();'></div>

                        <div>
                            <button style="float: right; margin-right: 31px; line-height: 1; margin-top: 6px; height: 27px;" id="suppEmail" onclick=sendEmail('<?php echo $_REQUEST['_'];?>') class="btn btn-info btn-sm email">Send Mail</button></div>
                        <div class="layui-layer-title" style="cursor: move;" move="ok">Order Detail</div>

                        <div class="layui-layer-content order-detail-modal" style="height: 556px;background:#fff"></div>
                        <span class="layui-layer-setwin">
                                    <a class="layui-layer-ico layui-layer-close layui-layer-close1"
                                       href="javascript:;"></a>
                                </span>
                    </div>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal_<?php echo $orderData['order_id'];?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" >
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background: rgb(238, 238, 238) none repeat scroll 0% 0%; text-align: center; font-weight: bold; height: 61px;border-radius: 4px;">
                                    <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Recipient To :</label></br>
                                        <input type="text" value="" id="emails_<?php echo $orderData['order_id'];?>" class="form-control" style="width:100%">
                                    </div>
                                    <div class="form-group">
                                        <label for="recipient-name" class="form-control-label">Add CC :</label></br>
                                        <input type="text" value="" id="cc_<?php echo $orderData['order_id'];?>" class="form-control" multiple="multiple" style="width:100%">
                                    </div>

                                    <div class="form-group">
                                        <label for="message-text" class="form-control-label">Message:</label>
                                        <textarea class="form-control" id="message-text_<?php echo $orderData['order_id'];?>"></textarea>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="message-text" style="padding-right: 20px;" class="form-control-label">Send copy to</br><?php echo $_SESSION['SFTAdminEmail'];?>:</label>
                                        <input type="checkbox" class="" name="copy" id="copy" style="position: absolute; display: contents;">
                                    </div>

                                </div></br></br>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary" onclick=sendMultiEmail('<?php echo $orderData['order_id'];?>')>Send Mail</button>
                                </div>
                            </div>
                        </div>
                    </div>


                    <style>.expand_<?php echo $det_id;?> { display: none; }</style>
                    <!-- Modal -->

                    <script>
                        function expend(id){
                            $(".expand_"+id).toggle("slow");
                        }
                        //multiple email send
                        $('#emails_<?php echo $orderData['order_id'];?>').select2({
                            tags: true,
                            tokenSeparators: [','],
                            placeholder: "Add your email here"
                        });
                        $('#cc_<?php echo $orderData['order_id'];?>').select2({
                            tags: true,
                            tokenSeparators: [','],
                            placeholder: "Add your CC here"
                        });
                    </script>
                        <?php  } } } }  ?>
                        </tbody>
                    </table>

                <?php } else { ?>

                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Profile Pic</th>
                                <th>Customer Name</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Contact</th>
                                <th>Gender</th>
                                <th>Stylist</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $storeName = $_SESSION['SFT_Store_Name'];
                                $query4 = "select * from wp_customers where stylist = '$storeName' ORDER By user_id DESC";
                                $result4 = mysqli_query($link, $query4);
                                if ($result4) {
                                    $num4 = mysqli_num_rows($result4);
                                    if ($num4 > 0) {
                                        $j=0;
                                        while ($usersData4 = mysqli_fetch_array($result4)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td><img src="<?php echo $usersData4['profile_pic']; ?>" style="width:20px;"></td>
                                                <td><a href='crm.php?t=u&uid=<?php echo $usersData4['user_id'];?>' style="border-bottom: 1px solid #0480be;"><?php echo $usersData4['name']; ?></a></td>
                                                <td><?php echo $usersData4['email']; ?></td>
                                                <td><?php echo $usersData4['password']; ?></td>
                                                <td><?php echo $usersData4['mobile']; ?></td>
                                                <td><?php echo $usersData4['gender']; ?></td>
                                                <td><?php echo $usersData4['stylist']; ?></td>
                                            </tr>
                                        <?php     }
                                    }
                                    else{
                                        echo "";
                                    }
                                }
                            }  ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div id="loadcss"></div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css"/>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/css/bootstrap-dialog.min.css"
      rel="stylesheet" type="text/css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap3-dialog/1.34.5/js/bootstrap-dialog.min.js"></script>
<!-- /page content -->
<?php
include('footer.php');
?>

<script>
    function saveTaskEmail() {
        $(".loader").show();
        var user_id = $("#user_id").val();
        var activity_type = "sendademail";
        var newtask_subject = $("#newtask_subject_email").val();
        var newtask_comment = $("#newtask_comment_email").val();
        var newtask_related = $("#newtask_related_email").val();
        var url = "api/supplierProcess.php";
        $.post(url, {
            "type": "newTaskEmail", "activity_type": activity_type, "subject": newtask_subject,
            "comment": newtask_comment, "related": newtask_related, "user_id": user_id
        }, function (datas) {
            window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
        });

    }

    $(".expends_<?php echo $_REQUEST['o'];?>").trigger('click');

    function openboxE(order_id) {
        var url = "api/orderProcess.php";
        $.post(url, {"type": "getOrder", "order_id": +order_id}, function (datas) {
            var status = datas.Status;
            var Message = datas.Message;
            var order = datas.order;
            var UserData = datas.UserData;
            var orderData = order.orderDetail;
            var datashow = "";
            if (status == "Success") {
                for (var a = 0; a < orderData.length; a++) {
                    var access_category = orderData[a].access_category;
                    if (access_category == "neck_tie_two" || access_category == "bow_tie_two") {
                        var emails_sup = $("#supids_2").val();
                        $("#emails_" + order_id).val(emails_sup);
                    }
                    if (access_category == "") {
                        var emails_sup = $("#supids_3").val();
                        $("#emails_" + order_id).val(emails_sup);
                    }
                }
            }
        });
    }

    function sendMultiEmail(order_id) {

        if (order_id == "") {
            var order_id = $("#oid").val();
            var emails = $("#emailsd").val();
        }
        else {
            var emails = $("#emails_" + order_id).val();
            var addcc = $("#cc_" + order_id).val();
            var message_text = $("#message-text_" + order_id).val();
        }

        if (document.getElementById('copy').checked) {
            var adminmail = "<?php echo $_SESSION['SFTAdminEmail'];?>";
        }
        else {
            var adminmail = "blank1@gmail.com";
        }
        $(".loader").show();
        var url = "api/supplierProcess.php";
        $.post(url, {
            "type": "supplierEmail",
            "supplier_email": emails,
            "order_id": order_id,
            "admin_mail": adminmail,
            "add_cc": addcc,
            "message_text": message_text
        }, function (data) {
            $(".loader").hide();
            $(".modal").hide();
            $(".modal-backdrop").hide();
            var $textAndPic = $('<div style="text-align: center;"></div>');
            $textAndPic.append('<img src="images/sendmail.gif" style="margin-bottom: 63px; margin-top: 47px; height: 112px;" />');
            BootstrapDialog.show({
                title: 'Your email has been sent',
                message: $textAndPic,
                buttons: [{
                    label: 'Close',
                    action: function (dialogRef) {
                        dialogRef.close();
                        window.location = "crm.php?type=view&userId=<?php echo $_REQUEST['userId'];?>&o=" + order_id;
                    }
                }]
            });
        });
    }

    function printDiv() {
        var divToPrint = document.getElementById('layui-layer5');
        var newWin = window.open('', 'Print-Window');
        newWin.document.open();
        newWin.document.write('<html><style>.ddkfxx ul li .p2{width:auto!important;}</style><link rel="stylesheet" type="text/css" href="css/defaulten.css">' +
            '<body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
        newWin.document.close();
        setTimeout(function () {
            newWin.close();
        }, 1000);
    }

    function orderListData() {
        var urlData = window.location.href;
        var url = "api/orderProcess.php";
        var oderIds = "";
        $.post(url, {"type": "getOrder", "order_id": +oderIds}, function (datas) {
            var status = datas.Status;
            var Message = datas.Message;
            var order = datas.order;
            var UserData = datas.UserData;
            var orderData = order.orderDetail;
            var datashow = "";
            if (status == "Success") {
                for (var a = 0; a < orderData.length; a++) {
                    var det_row_id = orderData[a].det_id;
                    var det_user_id = orderData[a].det_user_id;
                    var det_order_id = orderData[a].det_order_id;
                    var product_image = orderData[a].product_image;
                    var product_type = orderData[a].product_type;
                    meas_id = orderData[a].meas_id;
                    var det_quantity = orderData[a].det_quantity;
                    var det_price = orderData[a].cart_product_total_amount;
                    var det_product_name = orderData[a].det_product_name;
                    var order_number = order.order_number;
                    var order_date = order.order_date;
                    var store_name = order.store_name;
                    var payment_status = order.order_payment_status;
                    var userName = UserData.name;
                    var order_product_image = "";
                    switch (product_type) {
                        case "Custom Suit":
                            order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                            break;
                        case "Accessories":
                            order_product_image = product_image;
                            break;
                        case "Collection":
                            order_product_image = product_image;
                            break;
                        case "Custom 2Pc Suit":
                            order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                            break;
                        case "Custom Shirt":
                            order_product_image = "http://scan2fit.com/sftailor/images/shirts/57bandcollar.png";
                            break;
                        case "Custom Pant":
                            order_product_image = "http://scan2fit.com/sftailor/images/pants/formal.jpg";
                            break;
                        case "Custom Overcoat":
                            order_product_image = "http://scan2fit.com/sftailor/images/overcoat/overcoat_default.png";
                            break;
                    }
                    datashow = datashow + '<tbody><tr style="position:relative;"><td class="yh" style="height: 110px;" ' +
                        'width="7%">1</td><td class="spxq" style="height: 110px;" width="18%"><div class="middle">' +
                        '<p class="p1" style="display: block;"><a class="showDetail"><img class="cartimg" src="' +
                        order_product_image + '"></a></p><div><p class="p2" style="text-align: left; width: 100px;">' +
                        '<a class="showDetail">' + product_type + '</a></p><p class="p3" style="text-align: left; width: 100px;">'
                        + order_number + '</p></div></div></td><td class="fail" style="height: 110px;width:12%">' +
                        '<a class="showFormulainfo">' + order_number + '</a><p></p></td><td class="date" style="height: 110px; width:10%"><div class="datez"><p>' + order_date + '</p><p></p></div></td><td title="' + userName + '"' +
                        'class="customername" style="height: 110px; max-height: 110px; width:10%"><p style="max-height:110px;">' +
                        userName + '</p></td><td class="onlyspace" style="height: 110px; width:10%"><div class="jiajia">' +
                        det_quantity + '</div></td><td class="endtd" style="height: 110px; width:10%"><input type="hidden"><p>' +
                        '<a id="clickonButton"  onclick=get_product_d("' + det_user_id + '","' + det_row_id + '","' + encodeURI(product_type) +
                        '","' + order_number + '","' + encodeURI(order_date) + '","' + det_quantity + '","' + encodeURI(det_price) + '",' +
                        '"' + encodeURI(det_product_name) + '","' + encodeURI(store_name) + '","' + payment_status + '") ' +
                        'class="buyAgain" >View Details</a></p></td> </tr></tbody>';
                }
                ///* <td class="states" style="height: 110px;" width="10%">$' + det_price + '</td>'*/
                $(".productitem-view-table").html(datashow);
            }
        });
    }

    orderListData();

    function get_product_d(userId, orderRowId, product_type, order_number, order_date, det_quantity, det_price,
                           det_product_name, store_name, payment_status, orderID) {
        // window.location.href="crm.php?type=view&userId=<?php echo $_REQUEST['userId'];?>&od="+orderID;
        $(".loader").show();
        $("#oid").val(orderID);
        var loadcss = "";
        loadcss = loadcss + '<link rel="stylesheet" type="text/css" href="css/defaulten.css">';
        $("#loadcss").html(loadcss);
        product_type = decodeURI(product_type);
        order_number = decodeURI(order_number);
        order_date = decodeURI(order_date);
        det_quantity = decodeURI(det_quantity);
        det_price = decodeURI(det_price);
        det_product_name = decodeURI(det_product_name);
        store_name = decodeURI(store_name);
        payment_status = decodeURI(payment_status);
        userId = decodeURI(userId);
        if (store_name == "") {
            store_name = "SFTailor (Main)";
        }
        var url = "api/orderProcess.php";
        $.post(url, {"type": "getAllOrderData", "orderRowId": +orderRowId, "user_id": +userId}, function (data) {
            var status = data.Status;
            var Message = data.Message;
            var UserData = data.UserData;
            var orderDetRowData = data.orderDetRowData[0];
            var access_category = data.orderDetRowData[0].access_category;
            var datashow = "";
            if (status == "Success") {
                var userName = UserData.name;
                var userEmail = UserData.email;
                var userTelephone = UserData.mobile;
                var userWeight = UserData.weight;
                userWeight = userWeight.split(" ")[0];
                var userHeight = UserData.height;
                userHeight = userHeight.split(" ")[0];
                var meas_id = orderDetRowData.meas_id;
                var measurementData = "";
                var measurementsitemData = "";
                var customizationInformation = "";
                switch (product_type) {
                    case "Custom Suit":
                        customizationInformation = '<div class="ddkfxx nextg"><ul>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Made In China Label</p>' +
                            '<p class="p2">088E no made in china label in jacket</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Category</p>' +
                            '<p class="p2">' +
                            '<input id="j_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_category",this) class="ipfield" value="' + orderDetRowData.j_category + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/frontbutton.png" />Front Button</p>' +
                            '<p class="p2">' +
                            '<input id="j_frontbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_frontbutton",this) class="ipfield" value="' + orderDetRowData.j_frontbutton + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Style</p>' +
                            '<p class="p2">' +
                            '<input id="j_lapelstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Button Holes</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelbuttonholes" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholes",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholes + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Lapel Button Holes Style</p>' +
                            '<p class="p2">' +
                            '<input id="j_lapelbuttonholesstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesstyle + '" /> ' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonholethread.png" />Lapel Button Holes Thread</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelbuttonholesthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesthread",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesthread + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Ingredient</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelingredient" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelingredient",this) class="ipfield" value="' + orderDetRowData.j_lapelingredient + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Satin</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelsatin",this) class="ipfield" value="' + orderDetRowData.j_lapelsatin + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Chest Dart</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_chestdart",this) class="ipfield" value="' + orderDetRowData.j_chestdart + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Felt Collar</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_feltcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_feltcollar",this) class="ipfield" value="' + orderDetRowData.j_feltcollar + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Inner Flap</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_innerflap" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_innerflap",this) class="ipfield" value="' + orderDetRowData.j_innerflap + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Facing Style</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_facingstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_facingstyle",this) class="ipfield" value="' + orderDetRowData.j_facingstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_sleeveslit" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslit",this) class="ipfield" value="' + orderDetRowData.j_sleeveslit + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit Thread</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_sleeveslitthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslitthread",this) class="ipfield" value="' + orderDetRowData.j_sleeveslitthread + '" />' +
                            ' </p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/breastpocket.png" />Breast Pocket</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_breastpocket",this) class="ipfield" value="' + orderDetRowData.j_breastpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lowerpocket.png" />Lower Pocket</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lowerpocket",this) class="ipfield" value="' + orderDetRowData.j_lowerpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/coinpocket.png" />Coin Pocket</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_coinpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_coinpocket",this) class="ipfield" value="' + orderDetRowData.j_coinpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/backvent.png" />Back Vent</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_backvent" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_backvent",this) class="ipfield" value="' + orderDetRowData.j_backvent + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Style</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_liningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningstyle",this) class="ipfield" value="' + orderDetRowData.j_liningstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_liningoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningoption",this) class="ipfield" value="' + orderDetRowData.j_liningoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lining" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lining",this) class="ipfield" value="' + orderDetRowData.j_lining + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Style</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_shoulderstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderstyle",this) class="ipfield" value="' + orderDetRowData.j_shoulderstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Padding</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_shoulderpadding" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderpadding",this) class="ipfield" value="' + orderDetRowData.j_shoulderpadding + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_buttonoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonoption",this) class="ipfield" value="' + orderDetRowData.j_buttonoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Swatch</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_buttonswatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonswatch",this) class="ipfield" value="' + orderDetRowData.j_buttonswatch + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Thread Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_threadoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_threadoption",this) class="ipfield" value="' + orderDetRowData.j_threadoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_elbowpatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatch",this) class="ipfield" value="' + orderDetRowData.j_elbowpatch + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch Color</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_elbowpatchcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatchcolor",this) class="ipfield" value="' + orderDetRowData.j_elbowpatchcolor + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/canvas.png" />Canvas Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_canvasoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_canvasoption",this) class="ipfield" value="' + orderDetRowData.j_canvasoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Suit Color</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_suitcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_suitcolor",this) class="ipfield" value="' + orderDetRowData.j_suitcolor + '" />' + '' +
                            '</p></li>' +
                            '</ul> </div>';
                        $.post(url, {"type": "getUserMeasurement", "meas_id": +meas_id}, function (data) {
                            var Status = data.Status;
                            if (Status == "Success") {
                                measurementData = data.finalMeasurement;
                                measurementsitemData += "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Back length):&nbsp;&nbsp;" + '<input id="jacket_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_jacket_length",this) class="ipfields" value="' + measurementData.jacket_back_jacket_length + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Back shoulder):&nbsp;&nbsp;" + '<input id="jacket_back_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_shoulder",this) class="ipfields" value="' + measurementData.jacket_back_shoulder + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Back waist height):" + measurementData.jacket_back_waist_height + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Bicep):&nbsp;&nbsp;" + '<input id="jacket_bicep" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_bicep",this) class="ipfields" value="' + measurementData.jacket_bicep + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Bottom):" + measurementData.jacket_bottom + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Calf):" + measurementData.jacket_calf + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Chest):&nbsp;&nbsp;" + '<input id="jacket_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_chest",this) class="ipfields" value="' + measurementData.jacket_chest + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Collar):" + measurementData.jacket_collar + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Front shoulder):&nbsp;&nbsp;" + '<input id="jacket_front_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_front_shoulder",this) class="ipfields" value="' + measurementData.jacket_front_shoulder + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Front waist height):" + measurementData.jacket_front_waist_height + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Front waist length):" + measurementData.jacket_front_waist_length + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Knee):" + measurementData.jacket_knee + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Nape to waist):" + measurementData.jacket_nape_to_waist + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Pant left outsteam):" + measurementData.jacket_pant_left_outseam + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Pant right outsteam):" + measurementData.jacket_pant_right_outseam + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Seat):&nbsp;&nbsp;" + '<input id="jacket_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_seat",this) class="ipfields" value="' + measurementData.jacket_seat + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve left):&nbsp;&nbsp;" + '<input id="jacket_sleeve_left" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_left",this) class="ipfields" value="' + measurementData.jacket_sleeve_left + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve right):&nbsp;&nbsp;" + '<input id="jacket_sleeve_right" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_right",this) class="ipfields" value="' + measurementData.jacket_sleeve_right + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Stomach):&nbsp;&nbsp;" + '<input id="jacket_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_stomach",this) class="ipfields" value="' + measurementData.jacket_stomach + '" /> ' + "        </dd>";
                                //"<dd title= \"Seat(Accessory):\"> Jacket(Thigh):" + measurementData.jacket_thigh + "        </dd>" +
                                //"<dd title= \"Seat(Accessory):\"> Jacket(U-rise):" + measurementData.jacket_u_rise + "        </dd>" +
                                //"<dd title= \"Seat(Accessory):\"> Jacket(Wrist):" + measurementData.jacket_wrist + "        </dd>";
                            } else {
                                measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                            }
                        });
                        break;
                    case "Custom 2Pc Suit":
                        customizationInformation = '<div class="ddkfxx nextg"><ul>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Made In China Label</p>' +
                            '<p class="p2">088E no made in china label in 2Pc Suit</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Category</p>' +
                            '<p class="p2">' +
                            '<input id="j_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_category",this) class="ipfield" value="' + orderDetRowData.j_category + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/frontbutton.png" />Front Button</p>' +
                            '<p class="p2">' +
                            '<input id="j_frontbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_frontbutton",this) class="ipfield" value="' + orderDetRowData.j_frontbutton + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Style</p>' +
                            '<p class="p2">' +
                            '<input id="j_lapelstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Button Holes</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelbuttonholes" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholes",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholes + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Lapel Button Holes Style</p>' +
                            '<p class="p2">' +
                            '<input id="j_lapelbuttonholesstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesstyle + '" /> ' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonholethread.png" />Lapel Button Holes Thread</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelbuttonholesthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesthread",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesthread + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Ingredient</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelingredient" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelingredient",this) class="ipfield" value="' + orderDetRowData.j_lapelingredient + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Satin</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lapelsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelsatin",this) class="ipfield" value="' + orderDetRowData.j_lapelsatin + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Chest Dart</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_chestdart",this) class="ipfield" value="' + orderDetRowData.j_chestdart + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Felt Collar</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_feltcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_feltcollar",this) class="ipfield" value="' + orderDetRowData.j_feltcollar + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Inner Flap</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_innerflap" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_innerflap",this) class="ipfield" value="' + orderDetRowData.j_innerflap + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Facing Style</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_facingstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_facingstyle",this) class="ipfield" value="' + orderDetRowData.j_facingstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_sleeveslit" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslit",this) class="ipfield" value="' + orderDetRowData.j_sleeveslit + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit Thread</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_sleeveslitthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslitthread",this) class="ipfield" value="' + orderDetRowData.j_sleeveslitthread + '" />' +
                            ' </p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/breastpocket.png" />Breast Pocket</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_breastpocket",this) class="ipfield" value="' + orderDetRowData.j_breastpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lowerpocket.png" />Lower Pocket</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lowerpocket",this) class="ipfield" value="' + orderDetRowData.j_lowerpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/coinpocket.png" />Coin Pocket</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_coinpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_coinpocket",this) class="ipfield" value="' + orderDetRowData.j_coinpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/backvent.png" />Back Vent</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_backvent" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_backvent",this) class="ipfield" value="' + orderDetRowData.j_backvent + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Style</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_liningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningstyle",this) class="ipfield" value="' + orderDetRowData.j_liningstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_liningoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningoption",this) class="ipfield" value="' + orderDetRowData.j_liningoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_lining" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lining",this) class="ipfield" value="' + orderDetRowData.j_lining + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Style</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_shoulderstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderstyle",this) class="ipfield" value="' + orderDetRowData.j_shoulderstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Padding</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_shoulderpadding" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderpadding",this) class="ipfield" value="' + orderDetRowData.j_shoulderpadding + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_buttonoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonoption",this) class="ipfield" value="' + orderDetRowData.j_buttonoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Swatch</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_buttonswatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonswatch",this) class="ipfield" value="' + orderDetRowData.j_buttonswatch + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Thread Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_threadoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_threadoption",this) class="ipfield" value="' + orderDetRowData.j_threadoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_elbowpatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatch",this) class="ipfield" value="' + orderDetRowData.j_elbowpatch + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch Color</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_elbowpatchcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatchcolor",this) class="ipfield" value="' + orderDetRowData.j_elbowpatchcolor + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/canvas.png" />Canvas Option</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_canvasoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_canvasoption",this) class="ipfield" value="' + orderDetRowData.j_canvasoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Suit Color</p>' +
                            '<p class="p2"> ' +
                            '<input id="j_suitcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_suitcolor",this) class="ipfield" value="' + orderDetRowData.j_suitcolor + '" />' + '' +
                            '</p></li><li>' +
                            '<p class="p1"><img src="images/pantIcon/pant.png" />Category</p>' +
                            '<p class="p2">' +
                            '<input id="p_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_category",this) class="ipfield" value="' + orderDetRowData.p_category + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/sidepocket.png" />Front Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="p_frontpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_frontpocket",this) class="ipfield" value="' + orderDetRowData.p_frontpocket + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/category.png" />Pleat Style </p>' +
                            '<p class="p2">' +
                            '<input id="p_pleatstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleatstyle",this) class="ipfield" value="' + orderDetRowData.p_pleatstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/watch.png" />Watch Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="p_watchpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_watchpocket",this) class="ipfield" value="' + orderDetRowData.p_watchpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket </p>' +
                            '<p class="p2">' +
                            '<input id="p_backpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocket",this) class="ipfield" value="' + orderDetRowData.p_backpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt</p>' +
                            '<p class="p2">' +
                            '<input id="p_belt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_belt",this) class="ipfield" value="' + orderDetRowData.p_belt + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/category.png" />Pleat</p>' +
                            '<p class="p2">' +
                            '<input id="p_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleat",this) class="ipfield" value="' + orderDetRowData.p_pleat + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt Loop</p>' +
                            '<p class="p2">' +
                            '<input id="p_beltloop" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_beltloop",this) class="ipfield" value="' + orderDetRowData.p_beltloop + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/cuff.png" />Pant Bottom </p>' +
                            '<p class="p2">' +
                            '<input id="p_pantbottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantbottom",this) class="ipfield" value="' + orderDetRowData.p_pantbottom + '" />' +
                            '</p> </li>' +
                            '<li style="border-bottom: 1px solid !important;">' +
                            '<p class="p1"><img src="images/pantIcon/pant.png" />Pant Color</p>' +
                            '<p class="p2">' +
                            '<input id="p_pantcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantcolor",this) class="ipfield" value="' + orderDetRowData.p_pantcolor + '" />' +
                            '</p>' +
                            '</li></ul></div>';
                        $.post(url, {"type": "getUserMeasurement", "meas_id": +meas_id}, function (data) {
                            var Status = data.Status;
                            if (Status == "Success") {
                                measurementData = data.finalMeasurement;
                                measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Back length):&nbsp;&nbsp;" + '<input id="jacket_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_jacket_length",this) class="ipfields" value="' + measurementData.jacket_back_jacket_length + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Back shoulder):&nbsp;&nbsp;" + '<input id="jacket_back_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_shoulder",this) class="ipfields" value="' + measurementData.jacket_back_shoulder + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Bicep):&nbsp;&nbsp;" + '<input id="jacket_bicep" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_bicep",this) class="ipfields" value="' + measurementData.jacket_bicep + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Chest):&nbsp;&nbsp;" + '<input id="jacket_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_chest",this) class="ipfields" value="' + measurementData.jacket_chest + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Front shoulder):&nbsp;&nbsp;" + '<input id="jacket_front_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_front_shoulder",this) class="ipfields" value="' + measurementData.jacket_front_shoulder + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Seat):&nbsp;&nbsp;" + '<input id="jacket_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_seat",this) class="ipfields" value="' + measurementData.jacket_seat + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve left):&nbsp;&nbsp;" + '<input id="jacket_sleeve_left" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_left",this) class="ipfields" value="' + measurementData.jacket_sleeve_left + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve right):&nbsp;&nbsp;" + '<input id="jacket_sleeve_right" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_right",this) class="ipfields" value="' + measurementData.jacket_sleeve_right + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Jacket(Stomach):&nbsp;&nbsp;" + '<input id="jacket_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_stomach",this) class="ipfields" value="' + measurementData.jacket_stomach + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):&nbsp;&nbsp;" + '<input id="pant_back_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_back_waist_height",this) class="ipfields" value="' + measurementData.pant_back_waist_height + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):&nbsp;&nbsp;" + '<input id="pant_front_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_front_waist_height",this) class="ipfields" value="' + measurementData.pant_front_waist_height + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Bottom):&nbsp;&nbsp;" + '<input id="pant_bottom" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_bottom",this) class="ipfields" value="' + measurementData.pant_bottom + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Calf):&nbsp;&nbsp;" + '<input id="pant_calf" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_calf",this) class="ipfields" value="' + measurementData.pant_calf + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Knee):&nbsp;&nbsp;" + '<input id="pant_knee" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_knee",this) class="ipfields" value="' + measurementData.pant_knee + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):&nbsp;&nbsp;" + '<input id="pant_pant_left_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_left_outseam",this) class="ipfields" value="' + measurementData.pant_pant_left_outseam + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):&nbsp;&nbsp;" + '<input id="pant_pant_right_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_right_outseam",this) class="ipfields" value="' + measurementData.pant_pant_right_outseam + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Seat):&nbsp;&nbsp;" + '<input id="pant_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_seat",this) class="ipfields" value="' + measurementData.pant_seat + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Thigh):&nbsp;&nbsp;" + '<input id="pant_thigh" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_thigh",this) class="ipfields" value="' + measurementData.pant_thigh + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Waist):&nbsp;&nbsp;" + '<input id="pant_waist" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_waist",this) class="ipfields" value="' + measurementData.pant_waist + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(U-rise):&nbsp;&nbsp;" + '<input id="pant_u_rise" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_u_rise",this) class="ipfields" value="' + measurementData.pant_u_rise + '" /> ' + "        </dd>";
                            } else {
                                measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                            }
                        });
                        break;
                    case "Collection":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                            '<li>' +
                            '<p class="p1">Made In China</p>' +
                            '<p class="p2">No</p></li><li>' +
                            '<p class="p1">Category#</p>' +
                            '<p class="p2"><input readonly type="text" class="ipfield" value="Accessory" />' +
                            '</p></li> ' +
                            '<li>' +
                            '<p class="p1">Rc-Number</p>' +
                            '<p class="p2"><input id="a_rc_no" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_rc_no",this) class="ipfield" value="' + orderDetRowData.a_rc_no + '" />' +
                            '</p></li>' +
                            '<li style="border-bottom: 1px solid !important;">' +
                            '<p class="p1">Specification</p>' +
                            '<p class="p2"><input id="a_specification" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_specification",this) class="ipfield" value="' + orderDetRowData.a_specification + '" />' +
                            '</p></li>' +
                            '</ul> </div>';
                        break;
                    case "Accessories":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                            '<li>' +
                            '<p class="p1">Made In China</p>' +
                            '<p class="p2">No</p></li><li>' +
                            '<p class="p1">Category#</p>' +
                            '<p class="p2"><input readonly type="text" class="ipfield" value="Accessory" />' +
                            '</p></li> ' +
                            '<li>' +
                            '<p class="p1">Rc-Number</p>' +
                            '<p class="p2"><input id="a_rc_no" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_rc_no",this) class="ipfield" value="' + orderDetRowData.a_rc_no + '" />' +
                            '</p></li>' +
                            '<li style="border-bottom: 1px solid !important;">' +
                            '<p class="p1">Specification</p>' +
                            '<p class="p2"><input id="a_specification" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_specification",this) class="ipfield" value="' + orderDetRowData.a_specification + '" />' +
                            '</p></li>' +
                            '</ul> </div>';
                        break;
                    case "Custom Shirt":
                        customizationInformation = '<div class="ddkfxx nextg"><ul><li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" />Made In China Label</p>' +
                            '<p class="p2">58S9 no made in china label in shirt</p></li><li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" />Category</p>' +
                            '<p class="p2"><input id="s_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_category",this) class="ipfield" value="' + orderDetRowData.s_category + '" />' +
                            '</p></li> ' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar</p>' +
                            '<p class="p2"><input id="s_collar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collar",this) class="ipfield" value="' + orderDetRowData.s_collar + '" />' + '' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar Button</p>' +
                            '<p class="p2"><input id="s_collarbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbutton",this) class="ipfield" value="' + orderDetRowData.s_collarbutton + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/collarbuttondown.png" />Collar Button Down</p>' +
                            '<p class="p2"><input id="s_collarbuttondown" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbuttondown",this) class="ipfield" value="' + orderDetRowData.s_collarbuttondown + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/collarbuttondown.png" />Collar Layer Option</p>' +
                            '<p class="p2"><input id="s_collarlayeroption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarlayeroption",this) class="ipfield" value="' + orderDetRowData.s_collarlayeroption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/collar.png" />Front Collar</p>' +
                            '<p class="p2"><input id="s_frontcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_frontcollar",this) class="ipfield" value="' + orderDetRowData.s_frontcollar + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar Belt</p>' +
                            '<p class="p2"><input id="s_collarbelt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbelt",this) class="ipfield" value="' + orderDetRowData.s_collarbelt + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/cuff.png" />Cuff</p>' +
                            '<p class="p2"><input id="s_cuff" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_cuff",this) class="ipfield" value="' + orderDetRowData.s_cuff + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/cuffwidth.png" />Cuff Width</p>' +
                            '<p class="p2"><input id="s_cuffwidth" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_cuffwidth",this) class="ipfield" value="' + orderDetRowData.s_cuffwidth + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" />Placket</p>' +
                            '<p class="p2"><input id="s_placket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_placket",this) class="ipfield" value="' + orderDetRowData.s_placket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" />Placket Button</p>' +
                            '<p class="p2"><input id="s_placketbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_placketbutton",this) class="ipfield" value="' + orderDetRowData.s_placketbutton + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/pocket.png" />Pocket</p>' +
                            '<p class="p2"><input id="s_pocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_pocket",this) class="ipfield" value="' + orderDetRowData.s_pocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/pleat.png" />Pleat</p>' +
                            '<p class="p2"><input id="s_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_pleat",this) class="ipfield" value="' + orderDetRowData.s_pleat + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/bottom.png" />Bottom</p>' +
                            '<p class="p2"><input id="s_bottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_bottom",this) class="ipfield" value="' + orderDetRowData.s_bottom + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" />Shirt Color</p>' +
                            '<p class="p2"><input id="s_shirtcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_shirtcolor",this) class="ipfield" value="' + orderDetRowData.s_shirtcolor + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" />Button Color</p>' +
                            '<p class="p2"><input id="s_buttoncolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoncolor",this) class="ipfield" value="' + orderDetRowData.s_buttoncolor + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Buttoning Style</p>' +
                            '<p class="p2"><input id="s_buttoningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoningstyle",this) class="ipfield" value="' + orderDetRowData.s_buttoningstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Button Cut Thread</p>' +
                            '<p class="p2"><input id="s_buttoncutthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoncutthread",this) class="ipfield" value="' + orderDetRowData.s_buttoncutthread + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Hole Stich Thread</p>' +
                            '<p class="p2"><input id="s_holestichthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_holestichthread",this) class="ipfield" value="' + orderDetRowData.s_holestichthread + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" />Mono Gram Code</p>' +
                            '<p class="p2"><input id="s_monogramtext" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramtext",this) class="ipfield" value="' + orderDetRowData.s_monogramtext + '" />' +
                            '</p> </li>' + '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" />Mono Gram Text</p>' +
                            '<p class="p2"><input id="s_monogramcode" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramcode",this) class="ipfield" value="' + orderDetRowData.s_monogramcode + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" alt="font" />Mono Gram Font</p>' +
                            '<p class="p2"><input id="s_monogramfont" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramfont",this) class="ipfield" value="' + orderDetRowData.s_monogramfont + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Color" />Mono Gram Color</p>' +
                            '<p class="p2"><input id="s_monogramcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramcolor",this) class="ipfield" value="' + orderDetRowData.s_monogramcolor + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Position" />Mono Gram Position</p>' +
                            '<p class="p2"><input id="s_monogramposition" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramposition",this) class="ipfield" value="' + orderDetRowData.s_monogramposition + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Option" />Mono Gram Option</p>' +
                            '<p class="p2"><input id="s_monogramoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramoption",this) class="ipfield" value="' + orderDetRowData.s_monogramoption + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast Fabric</p>' +
                            '<p class="p2"><input id="s_contrastfabric" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrastfabric",this) class="ipfield" value="' + orderDetRowData.s_contrastfabric + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast</p>' +
                            '<p class="p2"><input id="s_contrast" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrast",this) class="ipfield" value="' + orderDetRowData.s_contrast + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast Position</p>' +
                            '<p class="p2"><input id="s_contrastposition" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrastposition",this) class="ipfield" value="' + orderDetRowData.s_contrastposition + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/shirtIcon/cuffwidth.png" alt="" />Sleeve</p>' +
                            '<p class="p2"><input id="s_sleeve" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_sleeve",this) class="ipfield" value="' + orderDetRowData.s_sleeve + '" />' +
                            '</p> </li>' +
                            '<li style="border-bottom: 1px solid !important;">' +
                            '<p class="p1"><img src="images/shirtIcon/placket.png" alt="" />Shoulder</p>' +
                            '<p class="p2"><input id="s_shoulder" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_shoulder",this) class="ipfield" value="' + orderDetRowData.s_shoulder + '" />' +
                            '</p></li>' +
                            '</ul> </div>';
                        $.post(url, {"type": "getUserMeasurement", "meas_id": +meas_id}, function (data) {
                            var Status = data.Status;
                            if (Status == "Success") {
                                measurementData = data.finalMeasurement;
                                measurementsitemData = "" +
                                    "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Back shoulder):" + '<input id="shirt_back_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_back_shoulder",this) class="ipfields" value="' + measurementData.shirt_back_shoulder + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Back Jacket Length):" + '<input id="shirt_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_back_jacket_length",this) class="ipfields" value="' + measurementData.shirt_back_jacket_length + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Back waist height):" + measurementData.shirt_back_waist_height + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Bicep):" + '<input id="shirt_bicep" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_bicep",this) class="ipfields" value="' + measurementData.shirt_bicep + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Bottom):" + measurementData.shirt_bottom + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Calf):" + measurementData.shirt_calf + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Chest):" + '<input id="shirt_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_chest",this) class="ipfields" value="' + measurementData.shirt_chest + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Collar):" + '<input id="shirt_collar" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_collar",this) class="ipfields" value="' + measurementData.shirt_collar + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Front shoulder):" + '<input id="shirt_front_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_front_shoulder",this) class="ipfields" value="' + measurementData.shirt_front_shoulder + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Front waist height):" + measurementData.shirt_front_waist_height + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Front waist length):" + measurementData.shirt_front_waist_length + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Knee):" + measurementData.shirt_knee + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Nape to waist):" + measurementData.shirt_nape_to_waist + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Left-outsteam):" + measurementData.shirt_pant_left_outseam + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Right-outsteam):" + measurementData.shirt_pant_right_outseam + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Seat):" + '<input id="shirt_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_seat",this) class="ipfields" value="' + measurementData.shirt_seat + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Sleeve left):" + '<input id="shirt_sleeve_left" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_sleeve_left",this) class="ipfields" value="' + measurementData.shirt_sleeve_left + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Sleeve right):" + '<input id="shirt_sleeve_right" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_sleeve_right",this) class="ipfields" value="' + measurementData.shirt_sleeve_right + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Shirt(Stomach):" + '<input id="shirt_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_stomach",this) class="ipfields" value="' + measurementData.shirt_stomach + '" /> ' + "        </dd>";
                                //"<dd title= \"Seat(Accessory):\"> Shirt(Thigh):" + measurementData.shirt_thigh + "        </dd>" +
                                //"<dd title= \"Seat(Accessory):\"> Shirt(U-rise):" + measurementData.shirt_u_rise + "        </dd>" +
                                //"<dd title= \"Seat(Accessory):\"> Shirt(Wrist):" + measurementData.shirt_wrist + "        </dd>";
                            } else {
                                measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                            }
                        });
                        break;
                    case "Custom Pant":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/pant.png" />Made In China Label</p>' +
                            '<p class="p2">3662 no made in china label in pants</p></li><li>' +
                            '<p class="p1"><img src="images/pantIcon/pant.png" />Category</p>' +
                            '<p class="p2">' +
                            '<input id="p_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_category",this) class="ipfield" value="' + orderDetRowData.p_category + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/sidepocket.png" />Front Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="p_frontpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_frontpocket",this) class="ipfield" value="' + orderDetRowData.p_frontpocket + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/category.png" />Pleat Style </p>' +
                            '<p class="p2">' +
                            '<input id="p_pleatstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleatstyle",this) class="ipfield" value="' + orderDetRowData.p_pleatstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/watch.png" />Watch Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="p_watchpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_watchpocket",this) class="ipfield" value="' + orderDetRowData.p_watchpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket </p>' +
                            '<p class="p2">' +
                            '<input id="p_backpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocket",this) class="ipfield" value="' + orderDetRowData.p_backpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket Satin</p>' +
                            '<p class="p2">' +
                            '<input id="p_backpocketsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocketsatin",this) class="ipfield" value="' + orderDetRowData.p_backpocketsatin + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt</p>' +
                            '<p class="p2">' +
                            '<input id="p_belt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_belt",this) class="ipfield" value="' + orderDetRowData.p_belt + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/category.png" />Pleat</p>' +
                            '<p class="p2">' +
                            '<input id="p_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleat",this) class="ipfield" value="' + orderDetRowData.p_pleat + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt Loop</p>' +
                            '<p class="p2">' +
                            '<input id="p_beltloop" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_beltloop",this) class="ipfield" value="' + orderDetRowData.p_beltloop + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="images/pantIcon/cuff.png" />Pant Bottom </p>' +
                            '<p class="p2">' +
                            '<input id="p_pantbottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantbottom",this) class="ipfield" value="' + orderDetRowData.p_pantbottom + '" />' +
                            '</p> </li>' +
                            '<li style="border-bottom: 1px solid !important;">' +
                            '<p class="p1"><img src="images/pantIcon/pant.png" />Pant Color</p>' +
                            '<p class="p2">' +
                            '<input id="p_pantcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantcolor",this) class="ipfield" value="' + orderDetRowData.p_pantcolor + '" />' +
                            '</p>' +
                            '</li></ul> </div>';
                        $.post(url, {"type": "getUserMeasurement", "meas_id": +meas_id}, function (data) {
                            var Status = data.Status;
                            if (Status == "Success") {
                                measurementData = data.finalMeasurement;
                                measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Back length):" + measurementData.pant_back_jacket_length + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Back shoulder):" + measurementData.pant_back_shoulder + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):" + '<input id="pant_back_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_back_waist_height",this) class="ipfields" value="' + measurementData.pant_back_waist_height + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):" + '<input id="pant_front_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_front_waist_height",this) class="ipfields" value="' + measurementData.pant_front_waist_height + '" /> ' + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Bicep):" + measurementData.pant_bicep + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Bottom):" + '<input id="pant_bottom" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_bottom",this) class="ipfields" value="' + measurementData.pant_bottom + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Calf):" + '<input id="pant_calf" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_calf",this) class="ipfields" value="' + measurementData.pant_calf + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Chest):" + measurementData.pant_chest + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Collar):" + measurementData.pant_collar + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Front shoulder):" + measurementData.pant_front_shoulder + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Front waist length):" + measurementData.pant_front_waist_length + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Knee):" + '<input id="pant_knee" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_knee",this) class="ipfields" value="' + measurementData.pant_knee + '" /> ' + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Nape to waist):" + measurementData.pant_nape_to_waist + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):" + '<input id="pant_pant_left_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_left_outseam",this) class="ipfields" value="' + measurementData.pant_pant_left_outseam + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):" + '<input id="pant_pant_right_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_right_outseam",this) class="ipfields" value="' + measurementData.pant_pant_right_outseam + '" /> ' + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Seat):" + '<input id="pant_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_seat",this) class="ipfields" value="' + measurementData.pant_seat + '" /> ' + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve left):" + measurementData.pant_sleeve_left + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve right):" + measurementData.pant_sleeve_right + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Stomach):" + measurementData.pant_stomach + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Thigh):" + '<input id="pant_thigh" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_thigh",this) class="ipfields" value="' + measurementData.pant_thigh + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(Waist):" + '<input id="pant_waist" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_waist",this) class="ipfields" value="' + measurementData.pant_waist + '" /> ' + "</dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Pant(U-rise):" + '<input id="pant_u_rise" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_u_rise",this) class="ipfields" value="' + measurementData.pant_u_rise + '" /> ' + "        </dd>";
                                //"<dd title= \"Seat(Accessory):\"> Pant(Wrist):" + measurementData.pant_wrist + "        </dd>";
                            } else {
                                measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                            }
                        });
                        break;
                    case "Custom Vest":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                            '<li>' +
                            '<p class="p1"><img src="" />Made In China Label</p>' +
                            '<p class="p2">4219 no made in china label in vest</p></li><li>' +
                            '<p class="p1"><img src="" />Category</p>' +
                            '<p class="p2">' +
                            '<input id="v_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_category",this) class="ipfield" value="' + orderDetRowData.v_category + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Collar Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_collarstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_collarstyle",this) class="ipfield" value="' + orderDetRowData.v_collarstyle + '" />' +
                            '</p></li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Lapel Button Hole</p>' +
                            '<p class="p2">' +
                            '<input id="v_lapelbuttonhole" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_lapelbuttonhole",this) class="ipfield" value="' + orderDetRowData.v_lapelbuttonhole + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Button Hole Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_buttonholestyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_buttonholestyle",this) class="ipfield" value="' + orderDetRowData.v_buttonholestyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Button Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_buttonstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_buttonstyle",this) class="ipfield" value="' + orderDetRowData.v_buttonstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Bottom Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_bottomstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_bottomstyle",this) class="ipfield" value="' + orderDetRowData.v_bottomstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Lower Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="v_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_lowerpocket",this) class="ipfield" value="' + orderDetRowData.v_lowerpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Ticket Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="v_ticketpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_ticketpocket",this) class="ipfield" value="' + orderDetRowData.v_ticketpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Back Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_backstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_backstyle",this) class="ipfield" value="' + orderDetRowData.v_backstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Chest Dart</p>' +
                            '<p class="p2">' +
                            '<input id="v_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_chestdart",this) class="ipfield" value="' + orderDetRowData.v_chestdart + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Top Stitch</p>' +
                            '<p class="p2">' +
                            '<input id="v_topstitch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_topstitch",this) class="ipfield" value="' + orderDetRowData.v_topstitch + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Placket Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_placketstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_placketstyle",this) class="ipfield" value="' + orderDetRowData.v_placketstyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Tuxedo Style</p>' +
                            '<p class="p2">' +
                            '<input id="v_tuxedostyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_tuxedostyle",this) class="ipfield" value="' + orderDetRowData.v_tuxedostyle + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Breast Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="v_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_breastpocket",this) class="ipfield" value="' + orderDetRowData.v_breastpocket + '" />' +
                            '</p> </li>' +
                            '<li>' +
                            '<p class="p1"><img src="" />Chest Pocket</p>' +
                            '<p class="p2">' +
                            '<input id="v_chestpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_chestpocket",this) class="ipfield" value="' + orderDetRowData.v_chestpocket + '" />' +
                            '</p> </li>' +
                            '<li style="border-bottom: 1px solid !important;">' +
                            '<p class="p1"><img src="" />Vest Fabric</p>' +
                            '<p class="p2">' +
                            '<input id="v_vestfabric" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_vestfabric",this) class="ipfield" value="' + orderDetRowData.v_vestfabric + '" />' +
                            '</p>' +
                            '</li></ul> </div>';
                        $.post(url, {"type": "getUserMeasurement", "meas_id": +meas_id}, function (data) {
                            var Status = data.Status;
                            if (Status == "Success") {
                                measurementData = data.finalMeasurement;
                                measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                    "<dd title= \"Seat(Accessory):\"> Vest(Back Jacket length):" + '<input id="vest_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","vest_back_jacket_length",this) class="ipfields" value="' + measurementData.vest_back_jacket_length + '" /> ' + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Back shoulder):" + measurementData.pant_back_shoulder + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):" + measurementData.pant_back_waist_height + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):" + measurementData.pant_front_waist_height + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Bicep):" + measurementData.pant_bicep + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Bottom):" + measurementData.pant_bottom + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Calf):" + measurementData.pant_calf + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Vest(Chest):" + '<input id="vest_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","vest_chest",this) class="ipfields" value="' + measurementData.vest_chest + '" /> ' + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Collar):" + measurementData.pant_collar + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Front shoulder):" + measurementData.pant_front_shoulder + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Front waist length):" + measurementData.pant_front_waist_length + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Knee):" + measurementData.pant_knee + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Nape to waist):" + measurementData.pant_nape_to_waist + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):" + measurementData.pant_pant_left_outseam + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):" + measurementData.pant_pant_right_outseam + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Seat):" + measurementData.pant_seat + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve left):" + measurementData.pant_sleeve_left + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve right):" + measurementData.pant_sleeve_right + "        </dd>" +
                                    "<dd title= \"Seat(Accessory):\"> Vest(Stomach):" + '<input id="vest_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","vest_stomach",this) class="ipfields" value="' + measurementData.vest_stomach + '" /> ' + "        </dd>";
                                //"<dd title= \"Seat(Accessory):\"> Pant(Thigh):" + measurementData.pant_thigh + "</dd>" +
                                ///"<dd title= \"Seat(Accessory):\"> Pant(Waist):" + measurementData.pant_waist + "</dd>" +
                                //"<dd title= \"Seat(Accessory):\"> Pant(U-rise):" + measurementData.pant_u_rise + "        </dd>";
                                //"<dd title= \"Seat(Accessory):\"> Pant(Wrist):" + measurementData.pant_wrist + "        </dd>";
                            } else {
                                measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                            }
                        });
                        break;
                    case "Custom Overcoat":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                            '<li><p class="p1">  category</p> <p class="p2" title="' + orderDetRowData.o_category + '"> ' + orderDetRowData.o_category + '    </p></li> ' +
                            '<li><p class="p1">  canvasstyle</p> <p class="p2" title="' + orderDetRowData.o_canvasstyle + '"> ' + orderDetRowData.o_canvasstyle + '    </p></li>' +
                            '<li><p class="p1">  lapelstyle</p> <p class="p2" title="' + orderDetRowData.o_lapelstyle + '"> ' + orderDetRowData.o_lapelstyle + '    </p> </li>' +
                            '<li><p class="p1">  pocketstype</p> <p class="p2" title="' + orderDetRowData.o_pocketstype + '"> ' + orderDetRowData.o_pocketstype + '    </p> </li>' +
                            '<li><p class="p1">  buttonstyle</p> <p class="p2" title="' + orderDetRowData.o_buttonstyle + '"> ' + orderDetRowData.o_buttonstyle + '    </p> </li>' +
                            '<li><p class="p1">  sweatpadstyle</p> <p class="p2" title="' + orderDetRowData.o_sweatpadstyle + '"> ' + orderDetRowData.o_sweatpadstyle + '    </p> </li>' +
                            '<li><p class="p1">  elbowpadstyle</p> <p class="p2" title="' + orderDetRowData.o_elbowpadstyle + '"> ' + orderDetRowData.o_elbowpadstyle + '    </p> </li>' +
                            '<li><p class="p1">  shoulderstyle</p> <p class="p2" title="' + orderDetRowData.o_shoulderstyle + '"> ' + orderDetRowData.o_shoulderstyle + '    </p> </li>' +
                            '<li><p class="p1">  shoulderpatchstyle</p> <p class="p2" title="' + orderDetRowData.o_shoulderpatchstyle + '"> ' + orderDetRowData.o_shoulderpatchstyle + '    </p> </li>' +
                            '<li><p class="p1">  shouldertabstyle</p> <p class="p2" title="' + orderDetRowData.o_shouldertabstyle + '"> ' + orderDetRowData.o_shouldertabstyle + '    </p> </li>' +
                            '<li><p class="p1">  sleeveslitstyle</p> <p class="p2" title="' + orderDetRowData.o_sleeveslitstyle + '"> ' + orderDetRowData.o_sleeveslitstyle + '    </p> </li>' +
                            '<li><p class="p1">  sleevebuttonstyle</p> <p class="p2" title="' + orderDetRowData.o_sleevebuttonstyle + '"> ' + orderDetRowData.o_sleevebuttonstyle + '    </p> </li>' +
                            '<li><p class="p1">  backvent</p> <p class="p2" title="' + orderDetRowData.o_backvent + '"> ' + orderDetRowData.o_backvent + '    </p> </li>' +
                            '<li><p class="p1">  stitchstyle</p> <p class="p2" title="' + orderDetRowData.o_stitchstyle + '"> ' + orderDetRowData.o_stitchstyle + '    </p> </li>' +
                            '<li><p class="p1">  backsideofarmhole</p> <p class="p2" title="' + orderDetRowData.o_backsideofarmhole + '"> ' + orderDetRowData.o_backsideofarmhole + '    </p> </li>' +
                            '<li><p class="p1">  topstichstyle</p> <p class="p2" title="' + orderDetRowData.o_topstichstyle + '"> ' + orderDetRowData.o_topstichstyle + '    </p> </li>' +
                            '<li><p class="p1">  breastpocket</p> <p class="p2" title="' + orderDetRowData.o_breastpocket + '"> ' + orderDetRowData.o_breastpocket + '    </p> </li>' +
                            '<li><p class="p1">  facingstyle</p> <p class="p2" title="' + orderDetRowData.o_facingstyle + '"> ' + orderDetRowData.o_facingstyle + '    </p> </li>' +
                            '<li><p class="p1">  pocketstyle</p> <p class="p2" title="' + orderDetRowData.o_pocketstyle + '"> ' + orderDetRowData.o_pocketstyle + '    </p> </li>' +
                            '<li><p class="p1">  collarstyle</p> <p class="p2" title="' + orderDetRowData.o_collarstyle + '"> ' + orderDetRowData.o_collarstyle + '    </p> </li>' +
                            '<li style="border-bottom: 1px solid !important;"><p class="p1">  liningstyle</p> <p class="p2" title="' + orderDetRowData.o_liningstyle + '"> ' + orderDetRowData.o_liningstyle + '    </p> </li>' +
                            '</ul> </div>';
                        break;
                }
            }
            setTimeout(function () {
                var supplierRowData1 = data.supplierData;
                for (var i = 0; i < supplierRowData1.length; i++) {
                    var sup_id = supplierRowData1[i].sup_id;
                    if (sup_id == "2" && (access_category == "neck_tie_two" || access_category == "bow_tie_two")) {
                        var supplier_name = supplierRowData1[i].company_name;
                        var company_address1 = supplierRowData1[i].company_address1;
                        var company_address2 = supplierRowData1[i].company_address2;
                        var company_work_phone = supplierRowData1[i].company_work_phone;
                        var company_mobile_no = supplierRowData1[i].company_mobile_no;
                        var contact_first_name = supplierRowData1[i].contact_first_name;
                        var family_name = supplierRowData1[i].family_name;
                        var contact_email = supplierRowData1[i].contact_email;
                        var city = 'Los Angles';
                        var country = 'USA';
                    }
                    if (sup_id == "3") {
                        var supplier_name = supplierRowData1[i].company_name;
                        var company_address1 = supplierRowData1[i].company_address1;
                        var company_address2 = supplierRowData1[i].company_address2;
                        var company_work_phone = supplierRowData1[i].company_work_phone;
                        var company_mobile_no = supplierRowData1[i].company_mobile_no;
                        var contact_first_name = supplierRowData1[i].contact_first_name;
                        var family_name = supplierRowData1[i].family_name;
                        var contact_email = supplierRowData1[i].contact_email;
                        var city = 'Jimo Qingdao';
                        var country = 'CHINA';
                    }
                }

                var url = "../api/registerUser.php";
                $.post(url, {"type": "getStyleGenieData", "user_id": userId}, function (data) {
                    data = data.userData;
                    var shoulder_slope = data.shoulder_slope;
                    var shoulder_slope = shoulder_slope.split(",");
                    var shoulder_slope_left = shoulder_slope[0];
                    var shoulder_slope_right = shoulder_slope[1];
                    var shoulder_forward = data.shoulder_forward;
                    var body_forward = data.body_forward;
                    var shoulder_slope_left = shoulder_slope_left;
                    var shoulder_slope_right = shoulder_slope_right;
                    var belly_shape = data.belly_shape;
                    var style_arms = data.style_arms;
                    var style_seat = data.style_seat;
                    var sixth_ques_stoop = data.sixth_ques_stoop;
                    var shirt_que_1 = data.shirt_que_1;
                    var shirt_que_2 = data.shirt_que_2;
                    var shirt_que_3 = data.shirt_que_3;
                    var shirt_que_4 = data.shirt_que_4;
                    var shirt_que_5 = data.shirt_que_5;
                    var shirt_que_6 = data.shirt_que_6;
                    var suit_que_1 = data.suit_que_1;
                    var suit_que_2 = data.suit_que_2;
                    var suit_que_3 = data.suit_que_3;
                    var suit_que_4 = data.suit_que_4;
                    var suit_que_5 = data.suit_que_5;
                    var vest_que_1 = data.vest_que_1;
                    var pant_que_1 = data.pant_que_1;
                    //try on specification

                    var chest = (data.chest);
                    var collar = (data.collar);
                    var stomach = (data.stomach);
                    var bicep = (data.bicep);
                    var waist = (data.waist);
                    var u_rise = (data.u_rise);
                    var thigh = (data.thigh);
                    var back_jacket_length = (data.back_jacket_length);
                    var back_shoulder = (data.back_sholuder);
                    var front_shoulder = (data.front_shoulder);
                    var shoulder = ((parseFloat(back_shoulder) + parseFloat(front_shoulder)) / 2);

                    if (data.unit_type == "Inches") {
                        var chest = (data.chest * 2.56).toFixed(2);
                        var collar = (data.collar * 2.56).toFixed(2);
                        var stomach = (data.stomach * 2.56).toFixed(2);
                        var bicep = (data.bicep * 2.56).toFixed(2);
                        var waist = (data.waist * 2.56).toFixed(2);
                        var u_rise = (data.u_rise * 2.56).toFixed(2);
                        var thigh = (data.thigh * 2.56).toFixed(2);
                        var back_jacket_length = (data.back_jacket_length * 2.56).toFixed(2);
                        var back_shoulder = (data.back_sholuder * 2.56).toFixed(2);
                        var front_shoulder = (data.front_shoulder * 2.56).toFixed(2);
                        var shoulder = ((parseFloat(back_shoulder) + parseFloat(front_shoulder)) / 2).toFixed(2);

                    }
                    $.post(url, {
                        "type": "getMatchingRange", "chest": chest, "stomach": stomach, "bicep": bicep,
                        "waist": waist, "u_rise": u_rise, "thigh": thigh, "collar": collar, "shoulder": shoulder,
                        "back_jacket_length": back_jacket_length
                    }, function (data) {
                        var status = data.Status;
                        if (status == "Success") {

                            var jacketRange = data.jacketRange;
                            var pantRange = data.pantRange;
                            var vestRange = data.vestRange;
                            var shirtRange = data.shirtRange;
                            var shirtNeckRange = data.shirtNeckRange;
                            var specification_li = "<li>Your Jacket specification is : " + jacketRange + " R</li>" +
                                "<li>Your Vest specification is : " + vestRange + " R</li>" +
                                "<li>Your Shirt specification is : " + shirtRange + "</li>" +
                                "<li>Your Pant specification is : " + pantRange + " R</li>" +
                                "<li>Your Shirt Specification : " + shirtNeckRange + " ( size based on the neck measurement only )</li>";
                            //$(".size_specification").html(specification_li);
                            var termsMessage = $("#terms-message").html();
                            // alert(termsMessage);

                            var orderModalData = '' +
                                '<div class="ddxq_tc"><h2>Customer Information</h2><div class="ddkfxx"><ul><li style="border-bottom: medium none;"><p class="p1">' +
                                'Order#</p><p class="p2">' + order_number + '</p></li><li style="border-bottom: medium none;"><p class="p1">Customer Name</p>' +
                                '<p class="p2">' + userName + '</p></li><li class="gender" style="border-bottom: medium none;"><p class="p1">Email</p>' +
                                '<p class="p2" id="user_email">' + userEmail + '</p></li><li style="border-bottom: medium none;"><p class="p1">Phone</p><p class="p2">' + userTelephone + '</p></li><li style="border-bottom: medium none;"><p class="p1">Weight</p><p class="p2">' + userWeight + ' Pounds</p></li><li style="border-bottom: medium none"><p class="p1">Height</p><p class="p2">' + userHeight + ' Inches</p></li><li><p class="p1">Salesman</p><p class="p2">' + store_name + '</p></li></ul>' +
                                '</div><h2>Delivery address</h2><div class="ddkfxx"><ul><li style=""><p class="p1">' +
                                'Address</p><p class="p2">SF Tailors 272 Hogans, Valley Way, Cary NC 27513 USA</p></li></ul></div><div class="ddkfxx"><h2>Order Details</h2><ul><li style="border-bottom: medium none;"><p class="p1">OrderNO.</p>' +
                                '<p class="p2">' + order_number + '</p></li><li style="border-bottom: medium none;"><p class="p1">User</p>' +
                                '<p class="p2">' + store_name + '</p></li><li style="border-bottom: medium none;"><p class="p1">OrderDate</p>' +
                                '<p class="p2">' + order_date + '</p></li><li style="border-bottom: medium none;"><p class="p1">Payment Status</p>' +
                                '<p class="p2">' + payment_status + '</p></li><li style="border-bottom: medium none;"><p class="p1">Product Type</p>' +
                                '<p class="p2">' + product_type + '</p></li><li><p class="p1" style="overflow:hidden;text-overflow:ellipsis">Product Name</p>' +
                                '<p class="p2"><input class="ipfield" type="text" id="det_product_name" ' +
                                'onchange=updateData("' + orderRowId + '","wp_orders_detail","det_product_name",this) value="' + det_product_name + '"></p></li>' +
                                '</ul></div><h2 class="img">Customization Information<span></span></h2><div class="ddkfxx">' +
                                '<div class="leftimg"></div>' + customizationInformation + '</div><h2>Monogram</h2><div class="ddkfxx"><ul><li style=""><p class="p1">' +
                                'Mono Gram Code</p><p class="p2">' + orderDetRowData.s_monogramcode + '</p></li><li style=""><p class="p1">' +
                                'Mono Gram Text</p><p class="p2">' + orderDetRowData.s_monogramtext + '</p></li><li style=""><p class="p1">' +
                                'Mono Gram Font</p><p class="p2">' + orderDetRowData.s_monogramfont + '</p></li><li style=""><p class="p1">' +
                                'Mono Gram Color</p><p class="p2">' + orderDetRowData.s_monogramcolor + '</p></li><li style=""><p class="p1">' +
                                'Mono Gram Position</p><p class="p2">' + orderDetRowData.s_monogramposition + '</p></li><li style=""><p class="p1">' +
                                'Mono Gram Option</p><p class="p2">' + orderDetRowData.s_monogramoption + '</p></li></ul></div><h2>Body Style</h2><div class="ddkfxx"><ul><li style=""><p class="p1">' +
                                'Shoulder Forward</p><p class="p2">' + shoulder_forward + '</p></li><li style=""><p class="p1">' +
                                'Body Forward</p><p class="p2">' + body_forward + '</p></li><li style=""><p class="p1">' +
                                'Shoulder Slope Left</p><p class="p2">' + shoulder_slope_left + '</p></li><li style=""><p class="p1">' +
                                'Shoulder Slope Right</p><p class="p2">' + shoulder_slope_right + '</p></li><li style=""><p class="p1">' +
                                'Belly Shape</p><p class="p2">' + belly_shape + '</p></li><li style=""><p class="p1">' +
                                'Style Arms</p><p class="p2">' + style_arms + '</p></li><li style=""><p class="p1">' +
                                'Style Seat</p><p class="p2">' + style_seat + '</p></li><li style=""><p class="p1">' +
                                'Sixth Question Slope</p><p class="p2">' + sixth_ques_stoop + '</p></li><li style=""><p class="p1">' +
                                'Shirt Question 1</p><p class="p2">' + shirt_que_1 + '</p></li><li style=""><p class="p1">' +
                                'Shirt Question 2</p><p class="p2">' + shirt_que_2 + '</p></li><li style=""><p class="p1">' +
                                'Shirt Question 3</p><p class="p2">' + shirt_que_3 + '</p></li><li style=""><p class="p1">' +
                                'Shirt Question 4</p><p class="p2">' + shirt_que_4 + '</p></li><li style=""><p class="p1">' +
                                'Shirt Question 5</p><p class="p2">' + shirt_que_5 + '</p></li><li style=""><p class="p1">' +
                                'Shirt Question 6</p><p class="p2">' + shirt_que_6 + '</p></li><li style=""><p class="p1">' +
                                'Suit Question 1</p><p class="p2">' + suit_que_1 + '</p></li><li style=""><p class="p1">' +
                                'Suit Question 2</p><p class="p2">' + suit_que_2 + '</p></li><li style=""><p class="p1">' +
                                'Suit Question 3</p><p class="p2">' + suit_que_3 + '</p></li><li style=""><p class="p1">' +
                                'Suit Question 4</p><p class="p2">' + suit_que_4 + '</p></li><li style=""><p class="p1">' +
                                'Suit Question 5</p><p class="p2">' + suit_que_5 + '</p></li><li style=""><p class="p1">' +
                                'Vest Question 1</p><p class="p2">' + vest_que_1 + '</p></li><li style=""><p class="p1">' +
                                'Pant Question 1</p><p class="p2">' + pant_que_1 + '</p></li></ul></div><h2>Measurements</h2><div class="size_xx">' +
                                measurementsitemData + '</dl></div><h2 style="font-weight:bold;">Try-on specifitations</h2><div class="size_xx"><dl style="color:#666;font-size:12px;">' +
                                specification_li + '</dl></div><h2 style="font-weight:bold;">Sales Terms</h2><div class="size_xx"><dl style="color:#666;font-size:12px;">' +
                                termsMessage + '</dl></div></div>';
                            $(".order-detail-modal").html(orderModalData);
                            $(".loader").hide();
                        }
                    });
                });
            }, 1000);
        });

        $("#layui-layer-shade5").css("display", "block");
        $("#layui-layer5").css("display", "block");

    }

    function sendEmail() {
        var SupplierEmail = $("#user_email").text();
        $("#emails").val(SupplierEmail);
        $("#mymodal").trigger("click");
        $("#layui-layer5").hide();
        $("#layui-layer-shade5").hide();
        $("#create_stores").hide();
        $(".shadoows").hide();
    }

    $(document).ready(function () {
        $('#openActivity').DataTable({});
    });
    $(document).ready(function () {
        $('#activityHistory').DataTable({});
    });
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });
    $(document).ready(function () {
        $('#related_to').DataTable({});
    });


    function close_diolog() {
        window.location = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
    }

    function updateData(det_id, table, column, obj) {
        var value = $("#" + obj.id).val();
        var url = "api/orderProcess.php";
        $.post(url, {
            "type": "updateEditDetail",
            "order_detail_id": det_id,
            "column": column,
            "value": value,
            "table": table
        });
    }

    var customname = $("#customerName").val();
    $("#newtask_assing").val(customname);

    function saveTask(activity_type) {
        $(".loader").show();
        var user_id = $("#user_id").val();
        var newtask_assing = $("#newtask_assing").val();
        var newtask_subject = $("#newtask_subject").val();
        var newtask_duedate = $("#newtask_duedate").val();
        var newtask_comment = $("#newtask_comment").val();
        var newtask_related = $("#newtask_related").val();
        var url = "api/supplierProcess.php";
        $.post(url, {
            "type": "newTask", "activity_type": activity_type, "assignTo": newtask_assing, "subject": newtask_subject,
            "duedate": newtask_duedate, "comment": newtask_comment, "related": newtask_related, "user_id": user_id
        }, function (datas) {
            window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
        });

    }

    var customnamed = $("#customeremail").val();
    $("#email_to").val(customnamed);

    $('#newtask_related').on('change', function () {
        var selectval = this.value;
        if (selectval == "Add Event") {
            $("#newtask_event").show();
        }
    });
    $('#email_related').on('change', function () {
        var selectval = this.value;
        if (selectval == "Add Event") {
            $("#emails_events").show();
        }
    });


    function sendanEmail(activity_type) {
        $(".loader").show();
        var user_id = $("#user_id").val();
        var email_to = $("#email_to").val();
        var email_cc = $("#email_cc").val();
        var email_bcc = $("#email_bcc").val();
        var email_subject = $("#email_subject").val();
        var email_comment = $("#email_comment").val();
        var email_related = $("#email_related").val();
        var url = "api/supplierProcess.php";
        $.post(url, {
            "type": "emailTask",
            "activity_type": activity_type,
            "email_to": email_to,
            "email_cc": email_cc,
            "email_bcc": email_bcc,
            "email_subject": email_subject,
            "email_comment": email_comment,
            "email_related": email_related,
            "user_id": user_id
        }, function (datas) {
            window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
        });
    }

    function deleteActivity(id) {
        $(".loader").show();
        var url = "api/supplierProcess.php";
        $.post(url, {"type": "deleteActivity", "activity_id": id}, function (datas) {
            window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
        });
    }


    function addEvent(event, id) {
        $(".loader").show();
        var newevent = $("#event_" + id).val();
        var url = "api/supplierProcess.php";
        $.post(url, {"type": "newevent", "activity_type": event, "addevent": newevent}, function (datas) {
            window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
        });

    }

    function editEventNew(event_id, act) {
        $("#new_event_edit_" + event_id + "_" + act).show();
        $("#update_div_" + event_id + "_" + act).show();
        $("#new_event_r_" + event_id + "_" + act).hide();
        $("#edit_div_" + event_id + "_" + act).hide();
    }

    function updateEventNew(event_id, act) {
        $(".loader").show();
        var updateevent = $("#new_event_edit_" + event_id + "_" + act).val();
        var url = "api/supplierProcess.php";
        $.post(url, {"type": "updateNewEvent", "updateevent": updateevent, "event_id": event_id}, function (datas) {
            window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
        });

    }

    function reloaded() {
        window.location.href = "crm.php?t=u&uid=<?php echo $_REQUEST['uid'];?>";
    }
</script>