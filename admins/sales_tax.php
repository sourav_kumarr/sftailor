<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
<style>
    .cop-price {
        float: left;
        height: 28px;
        width: 55px;
    }

    .couponForCheck {
        margin: 0 10px !important;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
</style>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">

    </div>
    <div class="row" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><span style="color:#1ABB9C;"></span>
                                Sales Tax
                                <small></small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <!-- <li>
                                     <div class="form-group form-inline">
                                         <input type="button" class="btn btn-danger btn-sm" onclick="addPaymentId()"
                                                value=" + Add Payment Id" />
                                     </div>
                                 </li>-->
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                From here admin can manage/modify the content of the Sales Tax
                            </p>
                            <div id="paymentIdData">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<?php
include('footer.php');
?>
<script>


    function getPaymentData(){
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "getSalesData"}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var paymentData = data.paymentData;
            if (Status == "Success") {
                var tbodyData ="";
                var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                    '<thead><tr><th>#</th><th>Amount</th><th> Added on</th><th>Action</th></tr></thead>';
                for(var a=0; a < paymentData.length; a++){
                    tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td>"+paymentData[a].sales_amount+"%</td>" +
                        "<td>"+paymentData[a].created_date+"</td><td><a  onclick=getPayIdPart('"+paymentData[a].id+"'); style='float: right; font-size: 18px;" +
                        "padding: 2px 4px; " +
                        "background: red none repeat scroll 0% 0%; color: white;'><i class='fa fa-pencil'></i>" +
                        "</a></td></tr>";
                }
                $("#paymentIdData").html(table+"<tbody>"+tbodyData+"</tbody></table>");
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
    }
    getPaymentData();
    function getPayIdPart(payId){
        var url = "api/userProcess.php";
        $.post(url, {"type": "getSalesPay", "payId": payId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var payData = data.payData;
            if (Status == "Success") {
                $(".modal-title").html("<label style='color: green'>Update Sales Tax Amount</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='payIdError'" +
                    " style='color:red'></p><div class='col-md-6 form-group'><label>Amount</label>" +
                    "<input type='text' class='form-control' id='paymentId' value='"+payData.sales_amount+"'" +
                    "placeholder='payment id' /></div><div class='col-md-6'>" +
                    "<input type='button' class='btn btn-danger pull-right' style='margin-top:27px' " +
                    "onclick=updatePaymentId('"+payData.id+"'); id='subbtn' value='Update'/></div></div>" +
                    "<p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function updatePaymentId(payId){
        var paymentId = $("#paymentId").val();
        if(paymentId == ""){
            $("#payIdError").html("please fill payment id");
            $("#payIdError").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updateSalesId",'payId':payId,'paymentId':paymentId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status == "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getPaymentData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
        $(".modal-backdrop").modal("hide");
    }
    function updatePayStatus(payId,a){
        var payValue = $("#updatePayStatus_"+a).val();
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updatePayStatus", "payId": payId, "payValue": payValue}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if (Status == "Success") {
                getPaymentData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }

</script>