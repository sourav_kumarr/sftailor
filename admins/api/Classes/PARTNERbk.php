<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 10/25/2017
 * Time: 1:39 PM
 */

namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class PARTNER
{
    private $pdf_id;
    public $link = null;
    public $userClass = null;
    public $response = array();
    public function __construct(){
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
    }
    public function addCategory($catTitle) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $checkEntry = "select * from partner_category  where category_title = '$catTitle' ";
        $entryResult = mysqli_query($link,$checkEntry);
        if($entryResult){
            $entryNum = mysqli_num_rows($entryResult);
            if($entryNum > 0){
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "This category title allready exist";
            }
            else{
                $insert_cat = "insert into partner_category(category_title,status) values('$catTitle','1')";
                $insert_result = mysqli_query($link,$insert_cat);
                if($insert_result){
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Category add successfully";
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }

        return $this->response;
    }
    public function getCategory() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select DISTINCT category from partner ";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["catData"] = $data;
        return $this->response;

    }
    public function addPartner($fairTitle,$fairLink,$catPartner)
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $fileResponse = $this->link->storeImage('file_', 'images');
        if ($fileResponse[Status] === Error) {
            return $fileResponse;
        }
        $file_name = $fileResponse["File_Name"];
        $file_name = rawurlencode($file_name);
        $file_path = $fileResponse["File_Path"];
        $fileExists = $this->isFileExists($file_name);

        if ($fileExists[Status] === Error) {
            return $fileExists;
        }
        $getMaxSort = "select MAX(sort_order)  AS sort_max from partner";
        $getMaxSort_result = mysqli_query($link, $getMaxSort);
        if ($getMaxSort_result) {
            $sort_max = 0;
            $sortNum = mysqli_num_rows($getMaxSort_result);
            if ($sortNum > 0) {
                $sort_row = mysqli_fetch_assoc($getMaxSort_result);
                $sort_max = $sort_row["sort_max"];
                $sort_max++;
            }
            $query = "insert into partner(category,fairTitle,fairLink,fairLogo,fairLogoPath,status)
            values('$catPartner','$fairTitle','$fairLink','$file_name','$file_path','1')";
            $result = mysqli_query($link, $query);
            if (!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                return $this->response;
            }
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "Partner add successfully";
            return $this->response;
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
    }
    public function editPartner($id,$fairTitle,$fairLink,$catPartner,$oldEventImagechanged) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }

        if($oldEventImagechanged == "yes"){
            $fileResponse = $this->link->storeImage('file_','images');
            if($fileResponse[Status] === Error) {
                return $fileResponse;
            }
            $file_name = $fileResponse["File_Name"];
            $file_name = rawurlencode($file_name);
            $file_path = $fileResponse["File_Path"];
            $fileExists = $this->isFileExists($file_name);

            if($fileExists[Status] === Error) {
                return $fileExists;
            }

            $query = "UPDATE `partner` SET `fairTitle`='$catPartner',fairTitle='$fairTitle',fairLink='$fairLink',
            fairLogo='$file_name',fairLogoPath='$file_path',status='1' where id='$id'";
        }
        else{
            $query = "UPDATE `partner` SET `category`='$catPartner',fairTitle='$fairTitle',fairLink='$fairLink'  where id='$id'";
        }




        $result = mysqli_query($link,$query);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Partner update successfully";
        return $this->response;
    }

    public function isFileExists($file_name) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from partner where fairLogo = '$file_name'";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $this->response[STATUS] = Error;
        $this->response[MESSAGE] = "File already exists with this name ".$file_name;
        $this->response["data"] = $data;
        return $this->response;
    }
    public function getPartner() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from partner order by sort_order";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;

    }
    public function getParticularPartner($catName) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from partner where category ='$catName'";
        if($catName == "All"){
            $query = "select * from partner";
        }

        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;

    }
    public function deletePartner($id,$path) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from partner where id='$id'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        $this->response["data"] = $data;
        $path = rawurldecode($path);
        unlink("Files/images/".$path);
        return $this->response;

    }
    public function deleteCategory($catName) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from partner where category ='$catName'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        return $this->response;

    }
    public function updateCategory($newCatName,$oldCatName) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $query = "update partner set category='$newCatName' where category ='$oldCatName'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";
        return $this->response;

    }
    public function updateSortOrder($sortOrder,$idOrder) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        for($j = 0;$j < count($idOrder); $j++){
            $update_sort = "update partner set sort_order= '$sortOrder[$j]' where id = '$idOrder[$j]' ";
            $result = mysqli_query($link,$update_sort);
            $count = mysqli_affected_rows($link);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update_sort;
                return $this->response;
            }
        }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }
    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}