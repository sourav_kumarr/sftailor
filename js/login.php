<!DOCTYPE html>
<html>
<head>
<title>Scan2Tailor</title>
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<script src="js/location.js"></script>
<link href="css/mystyle.css" rel="stylesheet" />
<link href="css/bootstrap.css" rel="stylesheet" />
<script src="js/bootstrap.js" ></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
<script src="js/moment.js"></script>

</head>
<div class="loader" style="display: none">
	<img src="images/spin.gif" class="spinloader"/>
</div>
<style>
.body {
    background: rgba(0, 0, 0, 0) url("./images/slider.jpg") no-repeat scroll 0 0 / 100% auto;
    margin: 0;
}
	.cityID
	{
		-moz-appearance: none;
		font-size: 16px !important;
		height: 41px !important;
		margin-top: 10px;
		opacity: 14;
	}
	.cf
	{
		height: 41px!important;
		margin-top: 10px;
	}
	
	#cityfeild #cityId{
		-moz-appearance: none;
		font-size: 16px !important;
		height: 41px !important;
		margin-top: 10px;
		opacity: 14;
	}
</style>
<?php
$successurl = "";
if(isset($_REQUEST['redurl']))
{
	if($_REQUEST['redurl'] == "rgvsjuq45745sttpx")
	{
		$successurl = "custom_suit.php?fromtwo";
	}
	elseif($_REQUEST['redurl'] == "vidypnosmy178ghf")
	{
		$successurl = "custom_pants.php?fromtwo";
	}
	elseif($_REQUEST['redurl'] == "gnsjktyw12cre")
	{
		$successurl = "custom_shirts.php?fromtwo";
	}
	else
	{
		$successurl = "customize.php";
	}
}
else if($_REQUEST['type'] == "fv")
	{
		$successurl = "sizematching.php";
	}
else
{
	$successurl = "customize.php";
}
echo "<input type='hidden' id='successurl' value=".$successurl." />";
?>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
 <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script>
$(document).ready(function()
{
	///-----------------------------registration from=====------------------------------------------------------------------------
	$("#registerform").submit(function(e) 
	{
		e.preventDefault();
		$('.loader').fadeIn("slow");
		$.ajax({
			url: "admin/webserver/signup.php", 
			type: "POST",             
			data: new FormData(this), 
			contentType: false,       
			cache: false,             
			processData:false,        
			success: function(data)   
			{
				$('.loader').fadeOut("slow");
				var json = $.parseJSON(data)
				{
					var status = json.status;
					if(status == "Registered Successfully")
					{
						alert(status);
						window.location="login.php?type=fv";
					}
					else
					{
						alert("Server Error !!! Please Try Again After Some Time");
					}
				}
			}
		});
	});
	///-----------------------------registration form end here=====------------------------------------------------------------------------
	///-----------------------------sign in form in here=====------------------------------------------------------------------------
	$("#loginform").on('submit',(function(e) 
	{
		e.preventDefault();
		//$('.loader').show();
		$.ajax({
			url: "admin/webserver/signin.php", 
			type: "POST",             
			data: new FormData(this), 
			contentType: false,       
			cache: false,             
			processData:false,        
			success: function(data)   
			{
				//$('.loader').hide();
				var json = $.parseJSON(data)
				{
					var status = json.status;
					if(status == "done")
					{
						var successurl = $("#successurl").val();
						window.location=successurl;
					}
					else
					{
						alert(status);
					}
				}
			}
		});
	}));
});

</script>
<?php
session_start();
error_reporting(0);
?>
<body class="body">
	<div class="col-md-12 black_spot">
		<div class="col-md-12 login_logo_div" style="">
			<a href="index.php"><img src="images/logo/logo.png" class="login_logo"/></a>
		</div>
		<!--  signin form start here    -->
		<div class="col-md-12" style="text-align:center;display:block" id="login_form" >
			<p class="login_caption3">Welcome to <span style="color:#d4504f">Scan2Tailor</span></p>
			<form method="Post" id="loginform">
				<input type="hidden" name="block" value="login" />
				<div class="form-group" >
					<input required style="background: #eee url(images/User-512.png) no-repeat scroll 5px center / 25px 25px" class="custom_login_feild" type="email" value="" name="email" Placeholder="Enter E-Mail" />
				</div>
				<div class="form-group" >
					<input required style="background: #eee url(images/password-512.png) no-repeat scroll 4px center / 24px 24px" class="custom_login_feild" type="password" value="" name="password" Placeholder=" Enter Password" />
				</div>
				<p class="forgot"></p></br>
				<div class="form-group" >
					<input class="custom_login_button" type="submit" value="Login" />
				</div>
				<div class="form-group">
					<p class="create_account">Don't have account? <span style="color:#d4504f;cursor:pointer;font-size:24px" onclick="signup()">Create account</span></p>
				</div>
			</form>
		</div>
		<!--  signin form end here    ------------------------------------>
		
		<!--  signup form start here  ------------------------------------>
		<div class="col-md-1"></div>
		<div class="col-md-10" style="text-align:center;display:none;box-shadow:0 0 10px #333;border-radius:5px" id="signup_form">
			<form method="Post" enctype="multipart/form-data" id="registerform">
				<div class="row cardview" style=""> 
					<div class="col-md-4">
						<p id="errorhere" style="color:red;font-size:16px;padding:10px"></p>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<input placeholder="First Name" name="fname" class="custom_signup_feild form-control" type="text">
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<input placeholder="Last Name" name="lname" class=" custom_signup_feild form-control" type="text">
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<select name="b_country" class="custom_signup_feild countries form-control" id="countryId" onchange='datedate()'>
									<option value="">Select Country</option>
								</select>
							</div>
						</div> 
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<select name="b_state" class=" custom_signup_feild states form-control" id="stateId">
									<option value="">Select State</option>
								</select>
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12 " id="cityfeild">
								<select name="b_city" class=" custom_signup_feild cities form-control" id="cityId">
									<option value="">Select City</option>
								</select>
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<input id="inputdob" name="dob" placeholder="Enter D.O.B" class=" custom_signup_feild form-control frm_ctrl" type="text">
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="row" style="margin-top:31px">
							<div class="col-md-12">
								<input name="file0" class=" custom_signup_feild form-control" type="file">
							</div>
						</div>
						<div class="row" style="margin-top:25px;padding:0">
							<div class="col-md-6" style="padding:0">
								<span style="color:black;font-family:trebuchet MS;font-size:16px">Drivers License:</span>
								<input name="driver_licence" value="true" type="checkbox" class="">
							</div>
							<div class="col-md-6" style="padding:0">
								<span style="color:black;font-family:trebuchet MS;font-size:16px">Enable face Login:</span>
								<input name="face_login" value="true" type="checkbox"  class="">
							</div>
						</div>
						<div class="row" style="margin-top:24px">
							<div class="col-md-6">
								<script language="javascript">
									document.write("<select name='height' class=' custom_signup_feild form-control'><option value='Height'>Select Height</option>")
									for(d=1;d<=300;d++)
									{
										document.write("<option value='"+d+"'>"+d+"</option>");	
									}
									document.write("</select>");
								</script>
							</div>
							<div class="col-md-6">
								<select name="heightparam" class=" custom_signup_feild form-control">
									<option selected="selected" value="">Parameter</option>
									<option value="Centimeters">Centimeters</option>
									<option value="Inches">Inches</option>
									<option value="Feets">Feets</option>
								</select>
							</div>
						</div>
						
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<input placeholder="Email" name="email" class=" custom_signup_feild form-control" type="email">
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<input placeholder="Password" id="password" name="password" class=" custom_signup_feild form-control" type="password">
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<input placeholder="Repeat Password" id="cpassword" class="custom_signup_feild form-control frm_ctrl" type="password">
							</div>
						</div>
					</div>
					
					<div class="col-md-4">
						<div class="row" style="margin-top:31px">
							<div class="col-md-12">
								<input placeholder="Stylist" name="stylist" class="custom_signup_feild form-control frm_ctrl" type="text">
							</div>
						</div>
						<div class="row" style="margin-top:25px">
							<div class="col-md-12">
								<span style="color:black;font-family:trebuchet MS;font-size:16px;float:left;margin-left:5px">Receive Mailings:
								<input name="email_recive" class="" value="true" type="checkbox"></span>
							</div>
						</div>
						<div class="row" style="margin-top:19px">
							<div class="col-md-12">
								<select class="custom_signup_feild form-control frm_ctrl" name="gender">
									<option selected="" value="">Select Gender</option>
									<option value="Male">Male</option>
									<option value="Female">Female</option>
								</select>
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-12">
								<select class="custom_signup_feild form-control frm_ctrl" name="lang">
									<option selected="" value="">Select Language</option>
									<option value="English(US)">English(US)</option>
									<option value="English(UK)">English(UK)</option>
									<option value="French">French</option>
								</select>
							</div>
						</div>
						<div class="row" style="margin-top:10px">
							<div class="col-md-6">
								<script language="javascript">
									document.write("<select name='weight' class='custom_signup_feild form-control'><option value='select weight'>Select Weight</option>")
									for(d=1;d<=250;d++)
									{
										document.write("<option value='"+d+"'>"+d+"</option>");	
									}
									document.write("</select>");
								</script>
							</div>
							<div class="col-md-6">
								<select name="weightparam" class="custom_signup_feild form-control">
									<option selected="" value="">Parameter</option>
									<option value="Kilograms">Kilograms</option>
									<option value="Pounds">Pounds</option>
									<option value="Stones">Stones</option>
								</select>
							</div>
						</div>
						<div class="row" style="margin:10px">
							<input value="Register" class=" custom_signup_button btn" type="submit">
						</div>
						<div class="row" style="margin-top:10px">
							<input value="Scan2Tailor" name="site_name" type="hidden">
							<input value="Website" name="app_type" type="hidden">
						</div>
					</div>
					<div style="clear:both"></div>	
					<p class="create_account" style="text-align:center">Already Registered ? <span style="color:#fff;cursor:pointer;font-size:24px" onclick="loginform()">Login Here</span></p>
				</div>
			</form>
		</div>
		<div class="col-md-1"></div>
		<!--  signup form end here  -->
		
		<div class="col-md-12" style="text-align:center;display:none">
			<br><br><br><br><br>
			<div class="form-group" >
				<input class="custom_login_button" id="loginbtn" type="button" value="Login Here" onclick="loginnow()" />
			</div>
		</div>
	</div>
</body>
<script>
	function signup()
	{
		$("#signup_form").show();
		$("#login_form").hide();
	}
	function loginform()
	{
		$("#login_form").show();
		$("#signup_form").hide();
	}
	//function datedate() {
		//var countryname = $("#countryId").val();
		//$("#dob").val("");
		//if(countryname == "United States") {
	//		$('#dob').datepicker({ dateFormat: 'mm/dd/yy' }).val();
		//}
		//else {
			//$('#dob').datepicker({ dateFormat: 'dd/mm/yy' }).val();
		//}
		//$("#dob").on("dp.change", function (e) {
		//	$('#dob').data("DateTimePicker").hide();
		//});
	//}
	function datedate(){
		var country = $("#countryId").val();
		if(country == "United States"){
			$('#inputdob').datetimepicker({
				minView : 2,
				format:"mm/dd/yyyy",
				autoclose: true
			});
		}else{
			$('#inputdob').datetimepicker({
				minView : 2,
				format:"dd/mm/yyyy",
				autoclose: true
			});
		}
	}
</script>
<?php
if(isset($_REQUEST['type']))
{
	if($_REQUEST['type'] == "reg")
	{
		?>
		<script>
			signup();
		</script>
		<?php
	}
}
?>
</html>