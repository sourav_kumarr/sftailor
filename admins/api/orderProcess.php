<?php
require_once('Classes/ORDERS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$orderClass = new \Classes\ORDERS();

$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $orderClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "getOrders")
{
    $response = $orderClass->getAllOrders();
    $orderClass->apiResponse($response);
}
else if($type == "getOrder"){
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->getParticularOrderData($order_id);
    $orderClass->apiResponse($response);
}
/*for get order detail Data, final measurement data,*/
else if($type == "getAllOrderData"){
    $requiredfields = array('orderRowId','user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $orderRowId = $_REQUEST['orderRowId'];
    $user_id  = $_REQUEST['user_id'];
    $response = $orderClass->getParticularUserData($user_id);
    $response = $orderClass->getParticularOrderDetailData($orderRowId);
    if(!strcasecmp($meas_id, "0")) {
        $response = $orderClass->getFinalMeasurementData($meas_id);
    }
    $orderClass->apiResponse($response);
}else if($type == "getUserMeasurement"){
    $requiredfields = array('meas_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $meas_id  = (string)$_REQUEST['meas_id'];
    $response = $orderClass->getFinalMeasurementData($meas_id);
    $orderClass->apiResponse($response);
}
else if($type == "refundStatus"){
    $requiredfields = array('order_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->refundStatus($order_id,$value);
    if($response[STATUS] == Error) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "cancelOrder"){
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_REQUEST['order_id'];
    $response = $orderClass->cancelOrder($order_id);
    if($response[STATUS] == Error) {
        $orderClass->apiResponse($response);
        return false;
    }
    $orderClass->apiResponse($response);
}
else if($type == "updateOrder"){
    $requiredfields = array('order_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_id = $_POST['order_id'];
    $all_fields = $_POST['all_fields'];
    $user_id = $_POST['user_id'];
    $response = $orderClass->updateOrderData($order_id,$all_fields,$user_id);
    $orderClass->apiResponse($response);
}
else if($type == "updateEditDetail"){
    $requiredfields = array('order_detail_id','column','value','table');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $order_detail_id = $_POST['order_detail_id'];
    $column = $_POST['column'];
    $value = $_POST['value'];
    $table = $_POST['table'];
    $response = $orderClass->updateEditDetail($order_detail_id,$column,$value,$table);
    $orderClass->apiResponse($response);
}
else if($type == "searchOrder") {
    $requiredfields = array('search_text');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $search_text = $_REQUEST['search_text'];
    $searchResponse = $orderClass->searchOrder($search_text);
    $orderClass->apiResponse($searchResponse);
}
else if($type == "searchOrder2") {
    $requiredfields = array('search_type');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $search_type = $_REQUEST['search_type'];
    $searchResponse = $orderClass->searchOrder2($search_type);
    $orderClass->apiResponse($searchResponse);
}
else if($type == "updateManualCode"){
    $requiredfields = array('det_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $orderClass->apiResponse($response);
        return false;
    }
    $det_id = $_POST['det_id'];
    $manual_code = $_POST['manual_code'];
    $response = $orderClass->updateManualCode($det_id,$manual_code);
    $orderClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $orderClass->apiResponse($response);
}
?>