<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
    <style>
        .cop-price {
            float: left;
            height: 28px;
            width: 55px;
        }
        .couponForCheck{
            margin: 0 10px!important;
        }
        .loader {
            background: #eee none repeat scroll 0 0;
            height: 830px;
            opacity: 0.7;
            position: fixed;
            width: 100%;
            z-index: 2147483647;
            top:0;
        }
        .spinloader {
            left: 34%;
            position: absolute;
            top: 14%;
        }
        .priceEdit{

        }
        .oldPriceImg{
            width: 80px;
        }
        #new_price_img {
            position: absolute;
            top: 22px;
            height: 100px;
            width: 125px;
            opacity: 0;
            image-orientation: from-image;
        }
        .loaderdiv {
            background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
            height: 700px;
            position: fixed;
            width: 100%;
            z-index: 99999;
            display: block;
        }
        .block {
            display: block !important;
        }
        .loaderhidden {
            display: none !important;
        }
        .loaderdiv > img {
            height: 50px;
            margin: 300px 50%;
            width: 50px;
        }
    </style>

    <div class="loaderdiv loaderhidden">
        <img src="images/preloader.gif"/>
    </div>
    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">

        </div>
        <div class="row" role="main">
            <div class="">
                <!--<div class="page-title">
                    <div class="title_left"></div>@f1*2C3AmY7+
                </div>-->
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title" style="border-bottom: 1px solid #e6e9ed;">
                                <h2><span style="color:#1ABB9C;"></span>Select Categories
                                    <small></small>
                                </h2>

                                <div class="clearfix"></div>
                            </div>
                            <div class="price_listData">
                                <div class="col-md-12 col-sm-12 col-xs-12 catagrories-suit">
                                    <ul class="priceListUl">

                                    </ul>
                                </div>
                            </div>
                            <?php /*if ($_REQUEST['type'] == 'price_list') { */?><!--

                                <div class="col-md-12 col-sm-12 col-xs-12 catagrories-suit">
                                    <ul>
                                        <li>
                                            <i class="fa fa-edit price_edit" onclick="editPriceList()"></i>
                                            <img src='images/2pcsuit.jpg' style='height:35px' class='img-thumbnail'>Suit
                                            2 Pc
                                        </li>
                                        <a href="index.php?type=Suits3pc">
                                            <li><img src='images/3pcsuit.png' style='height:35px' class='img-thumbnail'>Suit
                                                3 Pc
                                            </li>
                                        </a>
                                        <a href="index.php?type=Jacket">
                                            <li><img src='images/jacket.jpg' style='height:35px' class='img-thumbnail'>Jacket
                                            </li>
                                        </a>
                                        <a href="index.php?type=Pants">
                                            <li><img src='images/pants.jpg' style='height:35px' class='img-thumbnail'>Pants
                                            </li>
                                        </a>
                                        <a href="index.php?type=Vest">
                                            <li><img src='images/vest.jpg' style='height:35px' class='img-thumbnail'>Vest
                                            </li>
                                        </a>
                                        <a href="index.php?type=Shirts">
                                            <li><img src='images/shirts.jpg' style='height:35px' class='img-thumbnail'>Shirts
                                            </li>
                                        </a>
                                        <a href="index.php?type=Accessories">
                                            <li><img src='images/suit-accessories.jpg' style='height:35px'
                                                     class='img-thumbnail'>Accessories
                                            </li>
                                        </a>
                                        <a href="index.php?type=Overcoats">
                                            <li><img src='images/overcoat.jpg' style='height:35px'
                                                     class='img-thumbnail'>Overcoats
                                            </li>
                                        </a>
                                        <a href="index.php?type=LuxeriesSuits">
                                            <li><img src='images/luxerysuits.jpg' style='height:35px'
                                                     class='img-thumbnail'>Luxeries
                                                Suits
                                            </li>
                                        </a>
                                    </ul>
                                </div>

                            --><?php /*} */?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php
include('footer.php');
?>
<script>
    var oldPriceImagechanged = "No";
    showCategories();
    function showCategories(){
        var url = "api/price_cat_process.php";
        $.post(url,{"type":"getCategories"},function(data) {
            var Status = data.Status;
            if (Status === "Success") {
                var categories = data.data;
                var catData = "";
                for (var i = 0; i < categories.length; i++) {
                    catData = catData+ "<a href='#'>" +
                        "<li>" +
                        "<a href='"+categories[i].page_link+"'><img src='api/Files/images/price_cat/"+categories[i].image+"' style='height:35px'" +
                        "class='img-thumbnail'></a>"+categories[i].name+"" +
                        "<p><i class='fa fa-edit priceEdit pull-right' onclick=editPriceList('"+categories[i].id+"')></i>" +
                        "</p></li></a>";
                }
                $(".priceListUl").html(catData);
            }
            else
            {
                $("#galleryBox").css("display","none");
                $("#eventBox").css("display","block");
                $("#eventBox").html("<p class='text-center' style='font-size:22px;margin-top:50px'>No Partner Category Found</p>");
            }
        });
    }

    function editPriceList(priceId)
    {
        var url = "api/price_cat_process.php";
        $.post(url,{"type":"getCategoriesPart","priceId":priceId},function(data) {
            var Status = data.Status;
            if (Status === "Success") {
                var categories = data.data[0];
                var catData = "";
                $(".modal-title").html("<label style='color: green'>Edit Category List!!!</label>");
                $(".modal-body").html("<p id='message'></p><div class='row'><div class='col-md-6'>" +
                    "<div class='form-group'><label>Category Image</label><br><img src='api/Files/images/price_cat/"+categories.image+"'" +
                    "class='oldPriceImg' /><input type='file' onchange='readURL(this,"+'"oldPriceImg"'+")' id='new_price_img'" +
                    " class='old_eve_img'/></div></div><div class='col-md-6'><label>Category Name</label><input type='text' " +
                    "class='form-control' value='"+categories.name+"' placeholder='Enter Image Title' id='new_title' />" +
                    "<div style='clear: both'></div><input type='button' value='Edit Category Info' class='btn btn-danger pull-right'" +
                    " style='margin-top:20px' onclick=updatePriceNow('"+priceId+"') /></div></div>");
                $(".modal-footer").css("display","none");
                $("#myModal").modal("show");
            }
            else
            {
                $("#galleryBox").css("display","none");
                $("#eventBox").css("display","block");
                $("#eventBox").html("<p class='text-center' style='font-size:22px;margin-top:50px'>No Partner Category Found</p>");
            }
        });
    }
    function updatePriceNow(priceId){
        var new_title = $("#new_title").val();
        var new_price_img = $("#new_price_img").val();
        if(new_title == ""){
            $("#message").html("please fill category name");
            $("#message").css("color","red");
            return false;
        }
        var data = new FormData();
        if(oldPriceImagechanged == "yes"){
            if(new_price_img !== ""){
                var new_price_file = $("#new_price_img").val();
                var new_price_img = document.getElementById('new_price_img');
                var new_price_img_ext = new_price_file.split(".");
                new_price_img_ext = new_price_img_ext[new_price_img_ext.length - 1];
                new_price_img_ext = new_price_img_ext.toLowerCase();
                if (new_price_img_ext == "jpg" || new_price_img_ext == "png" || new_price_img_ext == "gif" || new_price_img_ext == "jpeg") {
                    data.append('newcatimg', new_price_img.files[0]);
                    oldPriceImagechanged = "yes";
                }
                else{
                    $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                    $("#message").css("color","red");
                    $("#subbtn").attr("disabled",false);
                    return false;
                }
            }
        }
        $("#message").html("");
        $(".loaderdiv").removeClass("loaderhidden");
        data.append('type', 'editCategory');
        data.append('priceId', priceId);
        data.append('new_title', new_title);
        data.append('oldcategoryImagechanged',oldPriceImagechanged);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success"){
                    oldPriceImagechanged = "No";
                    showCategories();
                    $("#myModal").modal("hide");
                    $(".loaderdiv").addClass("loaderhidden");
                }
                else{
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                    $("#message").css("color","red");
                    return false;
                }
            }
        };
        request.open('POST','api/price_cat_process.php');
        request.send(data);

    }
    function readURL(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                if(targetClass == "oldPriceImg"){
                    oldPriceImagechanged = "yes";
                }else{
                    oldPriceImagechanged = "yes";
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
