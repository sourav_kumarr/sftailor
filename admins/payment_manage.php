<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
<style>
    .cop-price {
        float: left;
        height: 28px;
        width: 55px;
    }

    .couponForCheck {
        margin: 0 10px !important;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
</style>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">

    </div>
    <div class="row" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2><span style="color:#1ABB9C;"></span>
                                PayPal ID
                                <small></small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                               <!-- <li>
                                    <div class="form-group form-inline">
                                        <input type="button" class="btn btn-danger btn-sm" onclick="addPaymentId()"
                                               value=" + Add Payment Id" />
                                    </div>
                                </li>-->
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                From here admin can manage/modify the content of the Payment id
                            </p>
                            <div id="paymentIdData">

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function addPaymentId(){
        $(".modal-title").html("<label style='color: green'>Add New Payment Id</label>");
        $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='payIdError'" +
            " style='color:red'></p><div class='col-md-6 form-group'><label>Payment Id</label>" +
            "<input type='text' class='form-control' id='paymentId' placeholder='payment id' /></div>" +
            "<div class='col-md-6'><input type='button' class='btn btn-danger pull-right' style='margin-top:27px'" +
            " onclick='PaymentIdAdd()'" +
            " id='subbtn' value='Submit'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
        $(".modal-footer").css('display', 'none');
        $("#myModal").modal("show");
    }
    function PaymentIdAdd(){
        var paymentId = $("#paymentId").val();
        if(paymentId == ""){
            $("#payIdError").html("please fill payment id");
            $("#payIdError").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "paymentIdAdd",'paymentId':paymentId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status == "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getPaymentData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
        $(".modal-backdrop").modal("hide");

    }
    function getPaymentData(){
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "getPaymentData"}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var paymentData = data.paymentData;
            if (Status == "Success") {
                var tbodyData ="";
                /*var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                    '<thead><tr><th>#</th><th>Payment id</th><th>status</th><th> Added on</th><th>Action</th></tr></thead>';*/
                var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                    '<thead><tr><th>#</th><th>Payment id</th><th> Added on</th><th>Action</th></tr></thead>';
                for(var a=0; a < paymentData.length; a++){
                    var pay_status = paymentData[a].status;
                    if(pay_status == "1"){
                        var selectStatus = "<select id='updatePayStatus_"+a+"' style='width: 108px; height: 28px;" +
                            " margin-top: 3px;' onchange=updatePayStatus('"+paymentData[a].id+"','"+a+"');>" +
                            "<option selected value='"+pay_status+"'>Active</option>" +
                            "<option value='0'>Deactive</option></select>";
                    }
                    else{
                        var selectStatus = "<select id='updatePayStatus_"+a+"' style='width: 108px; height: 28px;" +
                            " margin-top: 3px;'  onchange=updatePayStatus('"+paymentData[a].id+"','"+a+"');>" +
                            "<option  value='1'>Active</option>" +
                            "<option selected  value='0'>Deactive</option></select>";
                    }
                    tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td>"+paymentData[a].payment_id+"</td>" +
                        "<td>"+paymentData[a].add_on+"</td><td><a  onclick=getPayIdPart('"+paymentData[a].id+"'); style='float: right; font-size: 18px;" +
                        "padding: 2px 4px; " +
                        "background: red none repeat scroll 0% 0%; color: white;'><i class='fa fa-pencil'></i>" +
                        "</a></td></tr>";
                   /* tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td>"+paymentData[a].payment_id+"</td>" +
                        "<td>"+selectStatus+"</td><td>"+paymentData[a].add_on+"</td><td><a  " +
                        "onclick=confirmDelete_payid('"+paymentData[a].id+"'); style='float: right; font-size: 18px;padding: 2px 4px; " +
                        "background: red none repeat scroll 0% 0%; color: white; margin-left: 5px;'><i class='fa fa-trash-o'></i>" +
                        "</a>" + "<a  onclick=getPayIdPart('"+paymentData[a].id+"'); style='float: right; font-size: 18px;" +
                        "padding: 2px 4px; " +
                        "background: red none repeat scroll 0% 0%; color: white;'><i class='fa fa-pencil'></i>" +
                        "</a></td></tr>";*/
                }
                $("#paymentIdData").html(table+"<tbody>"+tbodyData+"</tbody></table>");
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
    }
    getPaymentData();
    function getPayIdPart(payId){
        var url = "api/userProcess.php";
        $.post(url, {"type": "getPartPay", "payId": payId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var payData = data.payData;
            if (Status == "Success") {
                $(".modal-title").html("<label style='color: green'>Update PayId</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='payIdError'" +
                    " style='color:red'></p><div class='col-md-6 form-group'><label>Payment Id</label>" +
                    "<input type='text' class='form-control' id='paymentId' value='"+payData.payment_id+"'" +
                    "placeholder='payment id' /></div><div class='col-md-6'>" +
                    "<input type='button' class='btn btn-danger pull-right' style='margin-top:27px' " +
                    "onclick=updatePaymentId('"+payData.id+"'); id='subbtn' value='Update'/></div></div>" +
                    "<p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function updatePaymentId(payId){
        var paymentId = $("#paymentId").val();
        if(paymentId == ""){
            $("#payIdError").html("please fill payment id");
            $("#payIdError").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updatePaymentId",'payId':payId,'paymentId':paymentId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status == "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getPaymentData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
        $(".modal-backdrop").modal("hide");
    }
    function updatePayStatus(payId,a){
        var payValue = $("#updatePayStatus_"+a).val();
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updatePayStatus", "payId": payId, "payValue": payValue}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if (Status == "Success") {
                getPaymentData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function confirmDelete_payid(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
            'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
            'onclick=delPaymentId("'+id+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function delPaymentId(payId) {
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "delPaymentId", "payId": payId}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                $("#myModal").modal("hide");
                getPaymentData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
</script>