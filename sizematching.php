<?php
include("header.php");
if ($user_id == "") {
    header("Location:login.php");
}
?>
<style>
    .fabric-banner {
        background: rgba(0, 0, 0, 0) url("images/slide4.jpg") no-repeat scroll 0 0 / cover !important;
        min-height: 495px;
    }

    .bs-docs-example {
        margin: 1em -5em;
    }

    .form-group {
        margin-bottom: 0 !important;
    }

    .paramselect {
        float: right;
        margin-right: 20px;
        width: 122px;
    }

    .clear {
        clear: both;
    }

    figure {
        border: 2px solid #ccc;
        margin: 15px 0;
    }

    .tr25 {
        width: 25%;
    }

    .humanbody {
        height: 100%;
    }

    #dimes_ul {
        display: block;
        overflow-x: hidden;
        margin-bottom: 65px;
        border-bottom: 1px solid rgb(221, 221, 221);
        height: 564px;
    }

    .que-images {
        width: 140px;
    }

    .main-ques .col-md-2 {
        background: #eee none repeat scroll 0 0;
        margin-left: 30px;
        padding: 16px 16px 1px;
    }

    .op1{
        background: #eee none repeat scroll 0 0;
        margin-left: 17px;
        padding: 14px 21px 0;
        width: 150px;
    }

    .main-ques2 .col-md-2 p {
        font-size: 14px;
    }

    .main-ques .col-md-2 p {
        font-size: 14px;
    }
</style>
<div class="banner fabric-banner">
    <div class="bann-info"></div>
</div>
<div class="clear"></div>
<div class="about-bottom wthree-3">
    <div class="container">
        <div id="step0" class="col-md-12 agileinfo_about_bottom_grid step" style="text-align: center;">
            <h3>We Found Your Body Measurements According to your Given Info</h3>
            <hr>
            <input type="button" value="Upload ORD" onclick="uploadOrd()" style="margin-left:10px" class="btn btn-danger pull-right"/>
            <input type="button" value="Reset Values" onclick="reset()" class="btn btn-danger pull-right"/>&nbsp;
            <input style="margin-right:20px" type="button" class="btn btn-danger pull-right"
                   onclick="downloadORDFile(<?php echo $user_id ?>)" value="Download Ord File.."/>
            <input style="margin-right:20px" type="button" class="btn btn-danger pull-right"
                   onclick="downloadXLSFile(<?php echo $user_id ?>)" value="Download Excel File.."/>
            <a href="api/Files/users/ord/inord<?php echo $user_id . ".ord" ?>" download style="display:none">
                <button id="inch_ord">Inches</button>
            </a>
            <a href="api/Files/users/ord/cmord<?php echo $user_id . ".ord" ?>" style="display:none" download>
                <button id="cm_ord">Cm</button>
            </a>
            <a href="api/Files/users/ord/inord<?php echo $user_id . ".xls" ?>" download style="display:none">
                <button id="inch_ordxls">Inches</button>
            </a>
            <a href="api/Files/users/ord/cmord<?php echo $user_id . ".xls" ?>" style="display:none" download>
                <button id="cm_ordxls">Cm</button>
            </a>
            <select class="form-control paramselect" onchange="needToShowPopUp()" style="width:200px" id="params">
                <option value="Inches" selected="selected">Inches</option>
                <option value="Cm">Cm</option>
            </select>
            <label style="float: right;">Unit Format : &nbsp;</label>
            <label>Data Source : <span id="data_source"></span></label>
            <div class="clearfix"></div>
            <p class="error" style="display:none;">No Data's Found According to your Measurements</p><br/>
            <div class="col-md-8" id="dimes_ul">
                <table class="table table-bordered">
                    <tr>
                        <th colspan="4" style="text-align: center">Body Measurements Are :-</th>
                    </tr>
                    <tr>
                        <th>S.No.</th>
                        <th>Parameters</th>
                        <th class="tr25">Edit Values</th>
                        <th>Values</th>
                    </tr>
                    <tr>
                        <td>1.</td>
                        <td><span onclick="changeImage('collar_ip')">Collar</span> <i onclick="showDesc('Collar')"
                                                                                      class="fa fa-info-circle"></i>
                        </td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('collar_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="collar_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('collar_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="collar"></td>
                    </tr>
                    <tr>
                        <td>2.</td>
                        <td><span onclick="changeImage('chest_front_ip')">Chest</span> <i onclick="showDesc('Chest')"
                                                                                          class="fa fa-info-circle"></i>
                        </td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('chest_front_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="chest_front_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('chest_front_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="chest_front"></td>
                    </tr>
                    <tr>
                        <td>3.</td>
                        <td><span onclick="changeImage('stomach_ip')">Waist Horizontal Line (stomach)</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Stomach')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('stomach_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="stomach_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('stomach_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="stomach"></td>
                    </tr>
                    <tr>
                        <td>4.</td>
                        <td><span onclick="changeImage('seat_front_ip')">Seat</span> <i class="fa fa-info-circle"
                                                                                        onclick="showDesc('Seat')"></i>
                        </td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('seat_front_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="seat_front_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('seat_front_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="seat_front"></td>
                    </tr>
                    <tr>
                        <td>5.</td>
                        <td><span onclick="changeImage('bicep_front_ip')">Bicep</span> <i class="fa fa-info-circle"
                                                                                          onclick="showDesc('Bicep')"></i>
                        </td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('bicep_front_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="bicep_front_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('bicep_front_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="bicep_front"></td>
                    </tr>
                    <tr>
                        <td>6.</td>
                        <td><span onclick="changeImage('shoulder_back_ip')">Back Shoulder</span> <i
                                    class="fa fa-info-circle"
                                    onclick="showDesc('Back_Shoulder_width')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('shoulder_back_ip','minus')" class="fa fa-minus pull-left"></i>
                                <input id="shoulder_back_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('shoulder_back_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="shoulder_back"></td>
                    </tr>
                    <tr>
                        <td>7.</td>
                        <td><span onclick="changeImage('shoulder_front_ip')">Front Shoulder</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Front Shoulder')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('shoulder_front_ip','minus')" class="fa fa-minus pull-left"></i>
                                <input id="shoulder_front_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('shoulder_front_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="shoulder_front"></td>
                    </tr>

                    <tr>
                        <td>8.</td>
                        <td><span onclick="changeImage('shoulder_sleeve_right_ip')">Sleeve length (right)</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Sleeve length right')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('shoulder_sleeve_right_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="shoulder_sleeve_right_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('shoulder_sleeve_right_ip','plus')"
                                   class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="shoulder_sleeve_right"></td>
                    </tr>
                    <tr>
                        <td>9.</td>
                        <td><span onclick="changeImage('shoulder_sleeve_left_ip')">Sleeve length (left)</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Sleeve length left')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('shoulder_sleeve_left_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="shoulder_sleeve_left_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('shoulder_sleeve_left_ip','plus')"
                                   class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="shoulder_sleeve_left"></td>
                    </tr>

                    <tr>
                        <td>10.</td>
                        <td><span onclick="changeImage('thigh_ip')">Thigh</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Thigh')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('thigh_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="thigh_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('thigh_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="thigh"></td>
                    </tr>
                    <tr>
                        <td>11.</td>
                        <td><span onclick="changeImage('napetowaist_back_ip')">Nape to waist (back)</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Nape to waist')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('napetowaist_back_ip','minus')" class="fa fa-minus pull-left"></i>
                                <input id="napetowaist_back_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('napetowaist_back_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="napetowaist_back"></td>
                    </tr>
                    <tr>
                        <td>12.</td>
                        <td><span onclick="changeImage('front_waist_length_ip')">Front waist length</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Front Waist Length')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('front_waist_length_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="front_waist_length_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('front_waist_length_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="front_waist_length"></td>
                    </tr>
                    <tr>
                        <td>13.</td>
                        <td><span onclick="changeImage('wrist_ip')">Wrist</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Wrist')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('wrist_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="wrist_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('wrist_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="wrist"></td>
                    </tr>
                    <tr>
                        <td>14.</td>
                        <td><span onclick="changeImage('waist_ip')">Waist</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Waist')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('waist_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="waist_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('waist_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="waist"></td>
                    </tr>
                    <tr>
                        <td>15.</td>
                        <td><span onclick="changeImage('calf_ip')">Calf</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Calf')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('calf_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="calf_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('calf_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="calf"></td>
                    </tr>
                    <tr>
                        <td>16.</td>
                        <td><span onclick="changeImage('back_waist_height_ip')">Back Waist Height</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Back Waist Height')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('back_waist_height_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="back_waist_height_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('back_waist_height_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="back_waist_height"></td>
                    </tr>
                    <tr>
                        <td>17.</td>
                        <td><span onclick="changeImage('front_waist_height_ip')">Front Waist Height</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Front Waist Height')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('front_waist_height_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="front_waist_height_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('front_waist_height_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="front_waist_height"></td>
                    </tr>
                    <tr>
                        <td>18.</td>
                        <td><span onclick="changeImage('knee_right_ip')">Knee</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Knee')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('knee_right_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="knee_right_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('knee_right_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td><span id="knee_right"></td>
                    </tr>
                    <tr>
                        <td>19.</td>
                        <td><span onclick="changeImage('back_jacket_length_ip')">Back_Jacket_Length</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Back Jacket Length')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('back_jacket_length_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="back_jacket_length_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('back_jacket_length_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="back_jacket_length"></td>
                    </tr>
                    <tr>
                        <td>20.</td>
                        <td><span onclick="changeImage('u_rise_ip')">U-rise</span>
                            <i class="fa fa-info-circle" onclick="showDesc('U-rise')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('u_rise_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="u_rise_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('u_rise_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="u_rise"></td>
                    </tr>
                    <tr>
                        <td>21.</td>
                        <td><span onclick="changeImage('outseam_l_pants_ip')">Pant Left Outseam</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Pant Left Outseam')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('outseam_l_pants_ip','minus')" class="fa fa-minus pull-left"></i>
                                <input id="outseam_l_pants_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('outseam_l_pants_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="outseam_l_pants"></td>
                    </tr>
                    <tr>
                        <td>22.</td>
                        <td><span onclick="changeImage('outseam_r_pants_ip')">Pant Right Outseam</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Pant Right Outseam')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer'
                                   onclick="changeVal('outseam_r_pants_ip','minus')" class="fa fa-minus pull-left"></i>
                                <input id="outseam_r_pants_ip" type="text" class="form-control pull-left"
                                       style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('outseam_r_pants_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="outseam_r_pants"></td>
                    </tr>
                    <tr>
                        <td>23.</td>
                        <td><span onclick="changeImage('bottom_ip')">Bottom</span>
                            <i class="fa fa-info-circle" onclick="showDesc('Bottom')"></i></td>
                        <td>
                            <div class="">
                                <i style='margin-top:10px;cursor:pointer' onclick="changeVal('bottom_ip','minus')"
                                   class="fa fa-minus pull-left"></i>
                                <input id="bottom_ip" type="text" class="form-control pull-left" style="width:52%"/>
                                <i style='margin-top:10px;cursor:pointer;margin-left:5px'
                                   onclick="changeVal('bottom_ip','plus')" class="fa fa-plus pull-left"></i>
                            </div>
                        </td>
                        <td id="bottom"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-4">
                <img src="images/Human_body.png" class="humanbody"/>
            </div>
            <div class="clear"></div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('0','Next')" class="btn btn-primary pull-right"
                       style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
            </div>
        </div>
        <!-----------Questions Part Start-------->
        <!-----First Question Div ---------->
        <div class="col-md-12 main-ques currPage1 step" id="step1" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px;">Shoulder Forward and Shoulder Backwards </h3>
            <div class="col-md-2">
                <img src="images/shape/verybackward.png" class="quess-images">
                <input type="radio" name="first_ques" id="very_backward" value="Very Backward">
                <p>Very Backward</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/slightbackward.png" class="quess-images">
                <input type="radio" name="first_ques" id="slightly_backward" value="Slight Backward">
                <p>Slight Backward</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/regular.png" class="quess-images">
                <input type="radio" name="first_ques" id="regular" value="Regular">
                <p>Regular</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/slightforward.png" class="quess-images">
                <input type="radio" name="first_ques" id="slightly_forward" value="Slight Forward">
                <p>Slight Forward</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/veryforward.png" class="quess-images">
                <input type="radio" name="first_ques" id="very_forward" value="Very Forward">
                <p>Very Forward</p>
            </div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('1','Next')" class="btn btn-primary pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('1','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
        <!-----Second Question Div ---------->
        <div class="col-md-12 main-ques2 currPage2 step" id="step2" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px;">Body Forward / Body Backwards (Men)</h3>
            <div class="col-md-2">
                <img src="images/shape/bodyforward10.png" class="quess-images">
                <input type="radio" id="body_forward_10" name="second_ques" value="Body Forward 1.0">
                <p>Body Forward 1.0</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bodyforward08.png" class="quess-images">
                <input type="radio" id="body_forward_08" name="second_ques" value="Body Forward 0.8">
                <p>Body Forward 0.8</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bodyforward05.png" class="quess-images">
                <input type="radio" name="second_ques" id="body_forward_05" value="Body Forward 0.5">
                <p>Body Forward 0.5</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bodyforward05.png" class="quess-images">
                <input type="radio" name="second_ques" id="normal" value="normal">
                <p>Normal</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bodybackward05.png" class="quess-images">
                <input type="radio" name="second_ques" id="body_backward_05" value="Body Backward 0.5">
                <p>Body Backward 0.5</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bodybackward08.png" class="quess-images">
                <input type="radio" name="second_ques" id="body_backward_08" value="Body Backward 0.8">
                <p>Body Backward 0.8</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bodybackward10.png" class="quess-images">
                <input type="radio" name="second_ques" id="body_backward_10" value="Body Backward 1.0">
                <p>Body Backward 1.0</p>
            </div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('2','Next')" class="btn btn-primary pull-right"
                       style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('2','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>


        <!-----Third Question Div ---------->
        <div class="col-md-12 currPage step" id="step3" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <div class="col-md-6">
                <h3 style="margin-bottom: 50px;">Shoulder Slope Left </h3>
                <div class="col-md-6">
                    <img src="images/shape/sholderslop_left.png" style="width:250px">
                </div>
                <div class="col-md-6">
                    <p style="text-align: left;">Shoulder can be identified by shoulder device, please refer to our body
                    measuring method to learn the way how to use shoulder device.</p>
                    <h4 style="text-align: left; text-transform: uppercase; margin-top: 24px;">Identity method Left</h4>
                    <select style="float: left; margin-top: 17px; height: 30px; width: 260px; border: 2px solid rgb(68, 68, 68);" name="third_ques" id="thirdQuestion_left">
                        <option value="Very high shoulder-G">Very high shoulder-G</option>
                        <option value="High shoulder-F">High shoulder-F</option>
                        <option value="Slight High E">Slight High E</option>
                        <option value="Regular">Regular</option>
                        <option value="Slight Slope-B">Slight Slope-B</option>
                        <option value="Slope-C">Slope-C</option>
                        <option value="Very Slope-D">Very Slope-D</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <h3 style="margin-bottom: 50px;">Shoulder Slope Right </h3>
                <div class="col-md-6">
                    <img src="images/shape/sholderslop_right.png" style="width:250px">
                </div>
                <div class="col-md-6">
                    <p style="text-align: left;">Shoulder can be identified by shoulder device, please refer to our body
                        measuring method to learn the way how to use shoulder device.</p>
                    <h4 style="text-align: left; text-transform: uppercase; margin-top: 24px;">Identity method Right</h4>
                    <select style="float: left; margin-top: 17px; height: 30px; width: 260px; border: 2px solid rgb(68, 68, 68);" name="third_ques" id="thirdQuestion_right">
                        <option value="Very high shoulder-G">Very high shoulder-G</option>
                        <option value="High shoulder-F">High shoulder-F</option>
                        <option value="Slight High E">Slight High E</option>
                        <option value="Regular">Regular</option>
                        <option value="Slight Slope-B">Slight Slope-B</option>
                        <option value="Slope-C">Slope-C</option>
                        <option value="Very Slope-D">Very Slope-D</option>
                    </select>
                </div>
            </div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('3','Next')" class="btn btn-primary pull-right"
                style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('3','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
        <!-----Fourth Question Div ---------->
        <div class="col-md-12 main-ques2 step" id="step4" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px;">Belly Shape (Visual)</h3>
            <div class="col-md-4"></div>
            <div class="col-md-2">
                <img src="images/shape/bellyregular.png" class="quess-images">
                <input type="radio" name="forth_ques" value="Regular">
                <p>Regular</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/bellyportly.png" class="quess-images">
                <input type="radio" name="forth_ques" value="Portly">
                <p>Portly</p>
            </div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('4','Next')" class="btn btn-primary pull-right"
                       style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('4','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>


        <!-----Fifth Question Div ---------->
        <div class="col-md-12 main-ques2 step" id="step5" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px;">Arms (Visual)</h3>
            <div class="col-md-3"></div>
            <div class="col-md-2">
                <img src="images/shape/armsbackward.png" class="quess-images">
                <input type="radio" name="fifth_ques" value="Backward">
                <p>Backward</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/armsregular.png" class="quess-images">
                <input type="radio" name="fifth_ques" value="Regular">
                <p>Regular</p>
            </div>
            <div class="col-md-2">
                <img src="images/shape/armsforward.png" class="quess-images">
                <input type="radio" name="fifth_ques" value="Forward">
                <p>Forward</p>
            </div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('5','Next')" class="btn btn-primary pull-right"
                       style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('5','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
        <!-----sixth Question Div ---------->
        <div class="col-md-12 main-ques2 step" id="step6" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px;">Seat (Visual)</h3>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="col-md-2 op1">
                    <img src="images/shape/seatnormal.png" class="quess-images">
                    <input type="radio" name="sixth_ques" value="Normal">
                    <p>Normal</p>
                </div>
                <div class="col-md-2 op1">
                    <img src="images/shape/seatfelt.png" class="quess-images">
                    <input type="radio" name="sixth_ques" value="Felt">
                    <p>Felt</p>
                </div>
                <div class="col-md-2 op1">
                    <img src="images/shape/seatprominent.png" class="quess-images">
                    <input type="radio" name="sixth_ques" value="Prominent">
                    <p>Prominent</p>
                </div>
                <div class="col-md-2 op1">
                    <img src="images/shape/seatdrop.png" class="quess-images">
                    <input type="radio" name="sixth_ques" value="Drop">
                    <p>Drop</p>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('6','Next')" class="btn btn-primary pull-right"
                       style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('6','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
        <!-----shirt seventh Question Div ---------->
        <div class="col-md-12 main-ques2 step" id="step6a" style="display:none;text-align: center;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px;">Stoop (Visual)</h3>
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="col-md-2 op1">
                    <img src="images/stoopnormal.jpg" class="quess-images">
                    <input type="radio" name="sixth_ques_stoop" value="Regular">
                    <p>Regular</p>
                </div>
                <div class="col-md-2 op1">
                    <img src="images/stoopnormal.jpg" class="quess-images">
                    <input type="radio" name="sixth_ques_stoop" value="Slight Stoop">
                    <p>Slight Stoop</p>
                </div>
                <div class="col-md-2 op1">
                    <img src="images/stoopl.jpg" class="quess-images">
                    <input type="radio" name="sixth_ques_stoop" value="Moderate Stoop">
                    <p>Moderate Stoop</p>
                </div>
                <div class="col-md-2 op1">
                    <img src="images/stoopl.jpg" class="quess-images">
                    <input type="radio" name="sixth_ques_stoop" value="Very Stoop">
                    <p>Very Stoop</p>
                </div>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-12" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('6a','Next')" class="btn btn-primary pull-right"
                       style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('6a','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>

        <!-----shirt eighth Question Div ---------->
        <div class="col-md-12 step" id="step7" style="display:none;text-align: left;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">
                Shirt</h3>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">1) How do you like your shirt to fit through the body?</h4>
                    <li><input type="radio" name="shirt_ques1" value="European">Very Fitted (european)</li>
                    <li><input type="radio" name="shirt_ques1" value="Tailored">Tailored to the body not too tight (tailored)</li>
                    <li><input type="radio" name="shirt_ques1" value="Classic">Roomy (Classic)</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">2) How do you like your shirt sleeve to fit?</h4>
                    <li><input type="radio" name="shirt_ques2" value="European"> Very Fitted (european)</li>
                    <li><input type="radio" name="shirt_ques2" value="Tailored">Tailored to the body not too tight (tailored)</li>
                    <li><input type="radio" name="shirt_ques2" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                </ul>
            </div>
            <div class="col-md-4" style="margin-bottom: 70px;">
                <ul>
                    <h4 style="margin-bottom:14px;">3) Do you tuck your shirt in?</h4>
                    <li><input type="radio" name="shirt_ques3" value="Always tucked">&nbsp;&nbsp; Always tucked</li>
                    <li><input type="radio" name="shirt_ques3" value="Always untucked">&nbsp;&nbsp; Always untucked</li>
                    <li><input type="radio" name="shirt_ques3" value="Both">&nbsp;&nbsp; Both</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">4) For your short -sleeve shirts, what length do you like</h4>
                    <li><input type="radio" name="shirt_ques4" value="Stylish short">&nbsp;&nbsp; Stylish
                        short, mid biceps
                    </li>
                    <li><input type="radio" name="shirt_ques4" value="Standard">&nbsp;&nbsp; Standard,
                        below biceps
                    </li>
                    <li><input type="radio" name="shirt_ques4" value="Classic">&nbsp;&nbsp;
                        Classic, just above the elbow
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">5) Do you wear a watch</h4>
                    <li><input type="radio" name="shirt_ques5" value="Left wrist">&nbsp;&nbsp; Left Wrist</li>
                    <li><input type="radio" name="shirt_ques5" value="Right wrist">&nbsp;&nbsp; Right Wrist</li>
                    <li><input type="radio" name="shirt_ques5" value="Both">&nbsp;&nbsp; Both</li>
                    <li><input type="radio" name="shirt_ques5" value="None">&nbsp;&nbsp; None</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">6) Size of the watch</h4>
                    <li><input type="radio" name="shirt_ques6" value="Normal">&nbsp;&nbsp; Normal</li>
                    <li><input type="radio" name="shirt_ques6" value="Big">&nbsp;&nbsp; Big</li>
                </ul>
            </div>
            <div class="col-md-8" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Continue" onclick="continueQuestion('7','Next')" class="btn btn-primary pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('7','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>


        <!-----eidght Question Div ---------->
        <div class="col-md-12 step" id="step8" style="display:none;text-align: left;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">
                Suit</h3>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">1) How do you like your jackets to fit through the body?</h4>
                    <li><input type="radio" name="suit_ques1" value="European">&nbsp;&nbsp; Very Fitted
                        (european)
                    </li>
                    <li><input type="radio" name="suit_ques1" value="Tailored">&nbsp;&nbsp;
                        Tailored to the body not too tight (tailored)
                    </li>
                    <li><input type="radio" name="suit_ques1" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">2) How do you like your jacket sleeve to fit?</h4>
                    <li><input type="radio" name="suit_ques2" value="European">&nbsp;&nbsp; Very Fitted
                        (european)
                    </li>
                    <li><input type="radio" name="suit_ques2" value="Tailored">&nbsp;&nbsp;
                        Tailored to the body not too tight (tailored)
                    </li>
                    <li><input type="radio" name="suit_ques2" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                </ul>
            </div>
            <div class="col-md-4" style="margin-bottom:60px;">
                <ul>
                    <h4 style="margin-bottom:14px;">3) How long do you prefer your jacket to be?</h4>
                    <li><input type="radio" name="suit_ques3" value="European">&nbsp;&nbsp; Stylish Short</li>
                    <li><input type="radio" name="suit_ques3" value="Tailored">&nbsp;&nbsp;
                        Standard, covers 2/3 of seat
                    </li>
                    <li><input type="radio" name="suit_ques3" value="Classic">&nbsp;&nbsp; Classic
                        covers entire seat
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">4) How long do you like your jacket sleeves to be jacket to be?</h4>
                    <li><input type="radio" name="suit_ques4" value="Stylish">&nbsp;&nbsp;
                        Stylish shorter, show a bit of cuf (about A 1/2)
                    </li>
                    <li><input type="radio" name="suit_ques4" value="Standard">&nbsp;&nbsp;
                        Standard, show a bit a cuf (about A 1/4)
                    </li>
                    <li><input type="radio" name="suit_ques4" value="Classic">&nbsp;&nbsp; Classic
                        covers cuff entiry
                    </li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                    <h4 style="margin-bottom:14px;">5) Do you have typically have any of these items in your
                        jacket?</h4>
                    <li><input type="radio" name="suit_ques5" value="Wallet">&nbsp;&nbsp; Wallet</li>
                    <li><input type="radio" name="suit_ques5" value="Phone">&nbsp;&nbsp; Phone</li>
                    <li><input type="radio" name="suit_ques5" value="Wallet and phone">&nbsp;&nbsp; Wallet and phone
                    </li>
                    <li><input type="radio" name="suit_ques5" value="No items in my jacket"> No items in my jacket
                    </li>
                </ul>
            </div>
            <div class="col-md-7" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Finish" onclick="continueQuestion('8','Next')" class="btn btn-primary pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('8','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
        <div class="col-md-12 step" id="step9" style="display:none;text-align: left;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">
                Vest</h3>
            <div class="col-md-12">
                <ul>
                    <h4 style="margin-bottom:14px;">1) How do you like your vest to fit through the body?</h4>
                    <li><input type="radio" name="vest_ques1" value="European">&nbsp;&nbsp; Very Fitted
                        (european)
                    </li>
                    <li><input type="radio" name="vest_ques1" value="Tailored">&nbsp;&nbsp;
                        Tailored to the body not too tight (tailored)
                    </li>
                    <li><input type="radio" name="vest_ques1" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                </ul>
            </div>
            <div class="col-md-7" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Finish" onclick="continueQuestion('9','Next')" class="btn btn-primary pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('9','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
        <div class="col-md-12 step" id="step10" style="display:none;text-align: left;">
            <h2 class="tittle">Style <span style="color:#000;">Genie</span></h2>
            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">Pant</h3>
            <div class="col-md-12">
                <ul>
                    <h4 style="margin-bottom:14px;">1) How do you like your pant to fit through the body?</h4>
                    <li><span><input type="radio" name="pant_ques1" value="European"> Very Fitted(european)</span></li>
                    <li><input type="radio" name="pant_ques1" value="Tailored">&nbsp;&nbsp;
                        Tailored to the body not too tight (tailored)
                    </li>
                    <li><input type="radio" name="pant_ques1" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                </ul>
            </div>
            <div class="col-md-7" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Finish" onclick="continueQuestion('10','Next')" class="btn btn-primary pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
                <input type="button" value="Back" onclick="continueQuestion('10','Previous')" class="btn btn-default pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;margin-right:10px"/>
            </div>
        </div>
    </div>
    <?php
    include("footer.php");
    ?>
    <script>
        var urlData =  window.location.href;
        if(urlData.indexOf("bodyStyle") > 0){
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url,{"type":"getStyleGenieData", "user_id":user_id},function(data){
                var status = data.Status;
                if(status == "Success"){
                    parseData(data.userData);
                    continueQuestion('0','Next');
                }
                else
                {
                    window.location = "sizematching.php";
                }
            });
        }
        var shoulder_slope_left = 0;
        var shoulder_slope_right = 0;
        var body_forward_backward = 0;
        var data_source = "";
        function continueQuestion(id,cursor) {
            var user_id = $("#user_id").val();
            if(cursor == "Next") {
                $(".btn-primary").val('Next');
                if (id == "0") {
                    $(".step").fadeOut();
                    $("#step1").fadeIn();
                    getShoulderType();
                    storeValuesOfTwinGenie();
                }
                else if (id == "1") {
                    if ($('input[name=first_ques]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step2").fadeIn();
                        getBodyForwardData();
                    } else {
                        showMessage("Please Select Atleast One Value", 'red');
                    }
                }
                else if (id == "2") {
                    if ($('input[name=second_ques]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step3").fadeIn();
                    } else {
                        showMessage("Please Select Atleast One Value", 'red');
                    }
                }
                else if (id == "3") {
                    $(".step").fadeOut();
                    $("#step4").fadeIn();
                }
                else if (id == "4") {
                    if ($('input[name=forth_ques]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step5").fadeIn();
                    } else {
                        showMessage("Please Select Atleast One Value", 'red');
                    }
                }
                else if (id == "5") {
                    if ($('input[name=fifth_ques]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step6").fadeIn();
                    } else {
                        showMessage("Please Select Atleast One Value", 'red');
                    }
                }
                else if (id == "6") {
                    if ($('input[name=sixth_ques]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step6a").fadeIn();
                    } else {
                        showMessage("Please Select Atleast One Value", 'red');
                    }
                }
                else if (id == "6a") {
                    if ($('input[name=sixth_ques_stoop]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step7").fadeIn();
                    } else {
                        showMessage("Please Select Atleast One Value", 'red');
                    }
                }
                else if (id == "7") {
                    if ($('input[name=shirt_ques1]:checked').val() != undefined && $('input[name=shirt_ques2]:checked').val() != undefined &&
                        $('input[name=shirt_ques3]:checked').val() != undefined && $('input[name=shirt_ques4]:checked').val() != undefined &&
                        $('input[name=shirt_ques5]:checked').val() != undefined && $('input[name=shirt_ques6]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step8").fadeIn();
                    } else {
                        showMessage("Please Fill All Questions", 'red');
                    }
                }
                else if (id == "8") {
                    if ($('input[name=suit_ques1]:checked').val() != undefined && $('input[name=suit_ques2]:checked').val() != undefined &&
                        $('input[name=suit_ques3]:checked').val() != undefined && $('input[name=suit_ques4]:checked').val() != undefined &&
                        $('input[name=suit_ques5]:checked').val() != undefined) {
                        $(".step").fadeOut();
                        $("#step9").fadeIn();
                    } else {
                        showMessage("Please Fill All Questions", 'red');
                    }
                }
                else if (id == "9") {
                    if ($('input[name=vest_ques1]:checked').val() != undefined ){
                        $(".step").fadeOut();
                        $("#step10").fadeIn();
                    } else {
                        showMessage("Please Fill All Questions", 'red');
                    }
                }
                else{
                    if ($('input[name=pant_ques1]:checked').val() != undefined ) {
                        var first_ques = $('input[name=first_ques]:checked').val();
                        var second_ques = $('input[name=second_ques]:checked').val();
                        var third_ques_left = $("#thirdQuestion_left option:selected").text();
                        var third_ques_right = $("#thirdQuestion_right option:selected").text();
                        var forth_ques = $('input[name=forth_ques]:checked').val();
                        var fifth_ques = $('input[name=fifth_ques]:checked').val();
                        var sixth_ques = $('input[name=sixth_ques]:checked').val();
                        var sixth_ques_stoop = $('input[name=sixth_ques_stoop]:checked').val();
                        var shirt_que_1 = $('input[name=shirt_ques1]:checked').val();
                        var shirt_que_2 = $('input[name=shirt_ques2]:checked').val();
                        var shirt_que_3 = $('input[name=shirt_ques3]:checked').val();
                        var shirt_que_4 = $('input[name=shirt_ques4]:checked').val();
                        var shirt_que_5 = $('input[name=shirt_ques5]:checked').val();
                        var shirt_que_6 = $('input[name=shirt_ques6]:checked').val();
                        var suit_que_1 = $('input[name=suit_ques1]:checked').val();
                        var suit_que_2 = $('input[name=suit_ques2]:checked').val();
                        var suit_que_3 = $('input[name=suit_ques3]:checked').val();
                        var suit_que_4 = $('input[name=suit_ques4]:checked').val();
                        var suit_que_5 = $('input[name=suit_ques5]:checked').val();
                        var vest_que_1 = $('input[name=vest_ques1]:checked').val();
                        var pant_que_1 = $('input[name=pant_ques1]:checked').val();
                        var url = "api/registerUser.php";
                        $.post(url, {
                            "type": "styleGenieUpdate",
                            "user_id": user_id,
                            "first_ques": first_ques,
                            "second_ques": second_ques,
                            "third_ques": third_ques_left + "," + third_ques_right,
                            "forth_ques": forth_ques,
                            "fifth_ques": fifth_ques,
                            "sixth_ques": sixth_ques,
                            "sixth_ques_stoop": sixth_ques_stoop,
                            "shirt_que_1": shirt_que_1,
                            "shirt_que_2": shirt_que_2,
                            "shirt_que_3": shirt_que_3,
                            "shirt_que_4": shirt_que_4,
                            "shirt_que_5": shirt_que_5,
                            "shirt_que_6": shirt_que_6,
                            "suit_que_1": suit_que_1,
                            "suit_que_2": suit_que_2,
                            "suit_que_3": suit_que_3,
                            "suit_que_4": suit_que_4,
                            "suit_que_5": suit_que_5,
                            "vest_que_1": vest_que_1,
                            "pant_que_1": pant_que_1
                        },
                        function (data) {
                            var status = data.Status;
                            if (status == "Success") {
                                showMessage("Size Genie Measurements Created Successfully....", 'green');
                                setTimeout(function(){
                                    window.location="sdet.php";
                                },2000);
                            } else {
                                showMessage("Server Error !!! Please Try After Some Time....", 'red');
                            }
                        });
                    } else {
                        showMessage("Please Fill All Questions", 'red');
                    }
                }
            }
            else{
                if (id == "1") {
                    $(".step").fadeOut();
                    $("#step0").fadeIn();
                }
                else if (id == "2") {
                    $(".step").fadeOut();
                    $("#step1").fadeIn();
                }
                else if (id == "3") {
                    $(".step").fadeOut();
                    $("#step2").fadeIn();
                }
                else if (id == "4") {
                    $(".step").fadeOut();
                    $("#step3").fadeIn();
                }
                else if (id == "5") {
                    $(".step").fadeOut();
                    $("#step4").fadeIn();
                }
                else if (id == "6") {
                    $(".step").fadeOut();
                    $("#step5").fadeIn();
                }
                else if (id == "6a") {
                    $(".step").fadeOut();
                    $("#step6").fadeIn();
                }
                else if (id == "7") {
                    $(".step").fadeOut();
                    $("#step6a").fadeIn();
                }
                else if (id == "8") {
                    $(".step").fadeOut();
                    $("#step7").fadeIn();
                }
                else if (id == "9") {
                    $(".step").fadeOut();
                    $("#step8").fadeIn();
                }
                else if (id == "10") {
                    $(".step").fadeOut();
                    $("#step9").fadeIn();
                }
            }
        }
        function getShoulderType(){
            var result = (parseFloat(shoulder_slope_left)+parseFloat(shoulder_slope_right))/2;
            var final_result = "";
          if(result<=1){
                final_result = "very_backward";
            }
            else if(result > 1 && result < 2){
                final_result = "slightly_backward";
            }
            else if(result == 2){
                final_result = "regular";
            }
            else if(result > 2 && result <=3){
                final_result = "slightly_forward";
            }
            else if(result > 3){
                final_result = "very_forward";
            }

            $("#"+final_result).attr("checked",true);
        }
        function body_forward(result){
            var final_result = "";
            if(result<0){
                final_result = "body_forward_1.0";
            }
            else if(result>=0 && result<0.4){
                final_result = "body_forward_1.0";
            }
            else if(result >= 0.4 && result < 1.0){
                final_result = "body_forward_0.8";
            }
            else if(result >= 1.0 && result < 2.5){
                final_result = "body_forward_0.5";
            }
            else if(result >= 2.5 && result < 3.0){
                final_result = "normal";
            }
            else if(result >=3.0 && result < 3.6){
                final_result = "body_backward_0.5";
            }
            else if(result >=3.6 && result < 4.0){
                final_result = "body_backward_0.8";
            }
            else if(result >= 4.0 && result < 6.0) {
                final_result = "body_backward_1.0";
            }
            setTimeout(function(){
                switch(final_result){
                    case "body_forward_0.5":
                        $("#body_forward_05").attr("checked",true);
                        break;
                    case "body_forward_0.8":
                        $("#body_forward_08").attr("checked",true);
                        break;
                    case "body_forward_1.0":
                        $("#body_forward_10").attr("checked",true);
                        break;
                    case "body_backward_0.5":
                        $("#body_backward_05").attr("checked",true);
                        break;
                    case "body_backward_0.8":
                        $("#body_backward_08").attr("checked",true);
                        break;
                    case "body_backward_1.0":
                        $("#body_backward_10").attr("checked",true);
                        break;
                    case "normal":
                        $("#normal").attr("checked",true);
                        break;
                }
            },500);
        }
        function showDesc(type) {
            var desc = "";
            switch (type) {
                case "Collar":
                    desc = "Place the tape below the Adams apple and keep the tape 3cm above the Seventh" +
                        " Cervical Vertebra.<br>Measure one round with one finger allowance.<br>Neither too loose or too tight.";
                    break;
                case "Chest":
                    desc = "Measure the fulest part of the chest on round.<br>(Measure on the height of the nipples.)<br>" +
                        "For Chest give real body measurement with 2 fingers within the tape.<br>Neither too tight or too loose.";
                    break;
                case "Stomach":
                    desc = " Measure on smallest part of the back (or a bit above the belly button).<br>For Waist " +
                        "Horizontal Line give real body measurement with 2 fingers within the tape.<br>Neith too tight or too loose. ";
                    break;
                case "Seat":
                    desc = "Measure the Seat at the fullest part fro one round.<br>For Seat give real body measurement with" +
                        " 2 fingers within the tape.<br>Neither too tight or too lose. ";
                    break;
                case "Bicep":
                    desc = " Measure the Bicep at the plubest part of upper arm.<br>For Bicep give reall body measurement " +
                        "with 1 finger within the tape ";
                    break;
                case "Back_Shoulder_width":
                    desc = "Measure the distance between the Shoulder End Points passing Seventh Cervical Vertebra";
                    break;
                case "Front Shoulder":
                    desc = "Start from right Shoulder End Point ans pass the collarbone position, then to left Shoulder" +
                        " End Point.<br>For Shoulder take real body measurement, we do not add any allowance";
                    break;
                case "Sleeve length right":
                    desc = "Measure from each Shoulder End Point to Knuckles and deducted 4 cm. (for women 3cm)<br>For " +
                        "Sleeves take real body measurement, we do not add any allowance";
                    break;
                case "Sleeve length left":
                    desc = "Measure from each Shoulder End Point to Knuckles and deducted 4 cm. (for women 3cm)<br>For " +
                        "Sleeves take real body measurement, we do not add any allowance";
                    break;
                case "Nape to waist":
                    desc = "Start in the back from the Seventh Cervical Vertebra then mesure along with the back middle" +
                        " and to the to Waist Horizontal Line (Smallest part of the back/above Bellybutton).For Nape to Waist" +
                        " take real body c, we do not add any allowance.";
                    break;
                case "Back Jacket Length":
                    desc = "Start in the back from the Seventh Cervical to under the Bottocks<br>Deducting 2cm will be " +
                        "standard, 4cm is a short style and 6cm is a very short style.<br>For Back Jacket Length take real " +
                        "body measurement, we do not add any allowance";
                    break;
                case "Wrist":
                    desc = "Wrap the tape measure  for one round around the wrist.<br>For Wrist take real body measurement," +
                        " we do not add any allowance.";
                    break;
                case "Waist":
                    desc = "Measure line where customer wears his pants <br>For Waist take real body measurment, we do not " +
                        "add any allowance. ";
                    break;
                case "Thigh":
                    desc = "Measure the Thigh at the plumest part.<br>For Thigh give real body measurement with 2 fingers " +
                        "within the tape.<br>Measure neither too tight or too loose. ";
                    break;
                case "U-rise":
                    desc = "Start from the top center of the back waistband and pull down the tape and let it across the" +
                        " the crotch, then lift it up to the top of the front waistband center.<br>For U-Rise take real body" +
                        " measurement, we do not add any allowance.";
                    break;
                case "Back Waist Height":
                    desc = "Start from the Waist Horizontal Line then measure along with the back middle line to the" +
                        " back top point of the waistband.<br>For Back Waist Height take real body measurement, we do not add " +
                        "any allowance.";
                    break;
                case "Front Waist Length":
                    desc = "Start from the Waist Horizontal Line then measure along with the front middle line to the" +
                        " front top point of the waistband.<br>For Front Waist Height take real body measurement, we do not add " +
                        "any allowance.";
                    break;
                case "Front Waist Height":
                    desc = "Start from the Waist Horizontal Line then measure along with the front middle line to the" +
                        " front top point of the waistband.<br>For Front Waist Height take real body measurement, we do not add " +
                        "any allowance.";
                    break;
                case "Pant Left Outseam":
                    desc = "Start from the top point of the left side waistband and meaure along with the outseam to the " +
                        "ground, deducting 2cm for normal style.<br>For Outseam take real body measurement, we do not add any " +
                        "allowance.";
                    break;
                case "Pant Right Outseam":
                    desc = "Start from the top point of the right side waistband and meaure along with the outseam to the" +
                        " ground, deducting 2cm for normal style.<br>For Outseam take real body measurement, we do not add any " +
                        "allowance. ";
                    break;
                case "Knee":
                    desc = "For Knee take real body measurement, we do not add any allowance. ";
                    break;
                case "Calf":
                    desc = "Measure horizontally around the calf at the fullest point and make it neither loos or " +
                        "tight.<br>For Calk take real body measurement, we do not add any allowance.";
                    break;
                case "Thigh Side":
                    desc = "Measure the Thigh at the plumest part.<br>For Thigh give real body measurement with 2 fingers " +
                        "within the tape.<br>Measure neither too tight or too loose. ";
                    break;
                case "Bottom":
                    desc = "Start from the Waist Horizontal Line then measure along with the front middle line to the" +
                        " front top point of the waistband.<br>For Front Waist Height take real body measurement, we do not add " +
                        "any allowance.";
                    break;

            }
            $(".modal-header").css({"display": "block"});
            $(".modal-title").html("<label style='color:red'>" + type + " Description</label>");
            $(".modal-body").css({"display": "block"});
            $(".modal-body").html("<span>" + desc + "</span>");
            $(".modal-footer").css({"display": "block"});
            $(".modal-footer").html("<input type='button' class='btn btn-default' value='Close' data-dismiss='modal' />");
            $("#modal").modal("show");
        }
        function changeVal(_id, operation) {
            var val = $("#" + _id).val();
            var ut = $("#params").val();
            if (ut == "Inches") {
                if (operation == "minus") {
                    if (val >= 1) {
                        val = parseFloat(val) - 0.25;
                    }
                } else {
                    val = parseFloat(val) + 0.25;
                }
            } else {
                if (operation == "minus") {
                    if (val >= 1) {
                        val = parseFloat(val) - 0.5;
                    }
                } else {
                    val = parseFloat(val) + 0.5;
                }
            }
            $("#" + _id).val(val);
            var disp_id = _id.split("_ip")[0];
            if (ut == "Inches") {
                $("#" + disp_id).html(val + " Inches");
            } else {
                $("#" + disp_id).html(val + " Cm");
            }
            $(".humanbody").attr("src", "images/shape/" + _id + ".png");
            storeValuesOfTwinGenie();
        }
        function changeImage(obj) {
            $(".humanbody").attr("src", "images/shape/" + obj + ".png");
        }
        function getData(waist,neck) {
            var ut = $("#params").val();
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url, {"type": "getSize","user_id":user_id}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    $("#data_source").html(data_source);
                    if (ut == "Inches") {
                        var sizeData = data.sizeData_in;
                        if(waist != "" && neck != ""){
                            $("#collar").html(((parseFloat(neck))+0.79).toFixed(2)+ " Inches");
                            $("#collar_ip").val(((parseFloat(neck))+0.79).toFixed(2));
                            $("#waist").html(parseFloat(waist).toFixed(2) + " Inches");
                            $("#waist_ip").val(parseFloat(waist).toFixed(2));
                        }else{
                            $("#collar").html(parseFloat(sizeData.Collar).toFixed(2)+ " Inches");
                            $("#collar_ip").val(parseFloat(sizeData.Collar).toFixed(2));
                            $("#waist").html(parseFloat(sizeData.Waist).toFixed(2) + " Inches");
                            $("#waist_ip").val(parseFloat(sizeData.Waist).toFixed(2));
                        }
                        $("#chest_front").html(parseFloat(sizeData.t2_Chest).toFixed(2) + " Inches");
                        $("#chest_front_ip").val(parseFloat(sizeData.t2_Chest).toFixed(2));
                        $("#stomach").html(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2) + " Inches");
                        $("#stomach_ip").val(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2));
                        $("#seat_front").html(parseFloat(sizeData.t4_Seat).toFixed(2) + " Inches");
                        $("#seat_front_ip").val(parseFloat(sizeData.t4_Seat).toFixed(2));
                        $("#bicep_front").html(parseFloat(sizeData.Bicep).toFixed(2) + " Inches");
                        $("#bicep_front_ip").val(parseFloat(sizeData.Bicep).toFixed(2));
                        $("#shoulder_back").html(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2) + " Inches");
                        $("#shoulder_back_ip").val(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2));
                        $("#shoulder_front").html(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2) + " Inches");
                        $("#shoulder_front_ip").val(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2));
                        $("#shoulder_sleeve_right").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Inches");
                        $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#shoulder_sleeve_left").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Inches");
                        $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#thigh").html(parseFloat(sizeData.Thigh).toFixed(2) + " Inches");
                        $("#thigh_ip").val(parseFloat(sizeData.Thigh).toFixed(2));
                        $("#thigh_right").html(parseFloat(sizeData.Thigh_Right).toFixed(2) + " Inches");
                        $("#thigh_right_ip").val(parseFloat(sizeData.Thigh_Right).toFixed(2));
                        $("#napetowaist_back").html(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2) + " Inches");
                        $("#napetowaist_back_ip").val(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2));
                        $("#front_waist_length").html(parseFloat(sizeData.Front_Waist_Length).toFixed(2) + " Inches");
                        $("#front_waist_length_ip").val(parseFloat(sizeData.Front_Waist_Length).toFixed(2));
                        $("#wrist").html(parseFloat(sizeData.Wrist).toFixed(2) + " Inches");
                        $("#wrist_ip").val(parseFloat(sizeData.Wrist).toFixed(2));
                        $("#calf").html(((parseFloat(sizeData.Calf_Right)+ parseFloat(sizeData.Calf_Left)) / 2).toFixed(2)+" Inches");
                        $("#calf_ip").val(((parseFloat(sizeData.Calf_Right)+ parseFloat(sizeData.Calf_Left)) / 2).toFixed(2));
                        $("#back_waist_height").html(parseFloat(sizeData.Back_Waist_Height).toFixed(2) + " Inches");
                        $("#back_waist_height_ip").val(parseFloat(sizeData.Back_Waist_Height).toFixed(2));
                        $("#front_waist_height").html(parseFloat(sizeData.Front_Waist_Height).toFixed(2) + " Inches");
                        $("#front_waist_height_ip").val(parseFloat(sizeData.Front_Waist_Height).toFixed(2));
                        $("#knee_right").html(((parseFloat(sizeData.Knee_Right)+ parseFloat(sizeData.Knee_Left))/2).toFixed(2)+" Inches");
                        $("#knee_right_ip").val(((parseFloat(sizeData.Knee_Right)+ parseFloat(sizeData.Knee_Left))/2).toFixed(2));
                        $("#back_jacket_length").html(parseFloat(sizeData.Back_Jacket_Length).toFixed(2) + " Inches");
                        $("#back_jacket_length_ip").val(parseFloat(sizeData.Back_Jacket_Length).toFixed(2));
                        $("#u_rise").html(parseFloat(sizeData.U_rise).toFixed(2) + " Inches");
                        $("#u_rise_ip").val(parseFloat(sizeData.U_rise).toFixed(2));
                        $("#outseam_l_pants").html(parseFloat(sizeData.Outseam_L_pants).toFixed(2) + " Inches");
                        $("#outseam_l_pants_ip").val(parseFloat(sizeData.Outseam_L_pants).toFixed(2));
                        $("#outseam_r_pants").html(parseFloat(sizeData.Outseam_R_Pants).toFixed(2) + " Inches");
                        $("#outseam_r_pants_ip").val(parseFloat(sizeData.Outseam_R_Pants).toFixed(2));
                        $("#bottom").html(parseFloat(sizeData.Bottom_for_trousers).toFixed(2) + " Inches");
                        $("#bottom_ip").val(parseFloat(sizeData.Bottom_for_trousers).toFixed(2));
                        shoulder_slope_left = parseFloat(sizeData.Shoulder_Slope_Left).toFixed(2);
                        shoulder_slope_right = parseFloat(sizeData.Shoulder_Slope_Right).toFixed(2);
                    } else {
                        var sizeData = data.sizeData_cm;
                        if(waist != "" && neck != ""){
                            $("#collar").html(((parseFloat(neck)*2.54)+2).toFixed(2)+ " Cm");
                            $("#collar_ip").val(((parseFloat(neck)*2.54)+2).toFixed(2));
                            $("#waist").html(parseFloat(waist)*2.54.toFixed(2) + " Cm");
                            $("#waist_ip").val(parseFloat(waist)*2.54.toFixed(2));
                        }else{
                            $("#collar").html(parseFloat(sizeData.Collar)*2.54.toFixed(2)+ " Cm");
                            $("#collar_ip").val(parseFloat(sizeData.Collar)*2.54.toFixed(2));
                            $("#waist").html(parseFloat(sizeData.Waist)*2.54.toFixed(2) + " Cm");
                            $("#waist_ip").val(parseFloat(sizeData.Waist)*2.54.toFixed(2));
                        }
                        $("#chest_front").html(parseFloat(sizeData.t2_Chest).toFixed(2) + " Cm");
                        $("#chest_front_ip").val(parseFloat(sizeData.t2_Chest).toFixed(2));
                        $("#stomach").html(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2) + " Cm");
                        $("#stomach_ip").val(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2));
                        $("#seat_front").html(parseFloat(sizeData.t4_Seat).toFixed(2) + " Cm");
                        $("#seat_front_ip").val(parseFloat(sizeData.t4_Seat).toFixed(2));
                        $("#bicep_front").html(parseFloat(sizeData.Bicep).toFixed(2) + " Cm");
                        $("#bicep_front_ip").val(parseFloat(sizeData.Bicep).toFixed(2));
                        $("#shoulder_back").html(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2) + " Cm");
                        $("#shoulder_back_ip").val(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2));
                        $("#shoulder_front").html(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2) + " Cm");
                        $("#shoulder_front_ip").val(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2));
                        $("#shoulder_sleeve_right").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Cm");
                        $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#shoulder_sleeve_left").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Cm");
                        $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#thigh").html(parseFloat(sizeData.Thigh).toFixed(2) + " Cm");
                        $("#thigh_ip").val(parseFloat(sizeData.Thigh).toFixed(2));
                        $("#thigh_right").html(parseFloat(sizeData.Thigh_Right).toFixed(2) + " Cm");
                        $("#thigh_right_ip").val(parseFloat(sizeData.Thigh_Right).toFixed(2));
                        $("#napetowaist_back").html(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2) + " Cm");
                        $("#napetowaist_back_ip").val(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2));
                        $("#front_waist_length").html(parseFloat(sizeData.Front_Waist_Length).toFixed(2) + " Cm");
                        $("#front_waist_length_ip").val(parseFloat(sizeData.Front_Waist_Length).toFixed(2));
                        $("#wrist").html(parseFloat(sizeData.Wrist).toFixed(2) + " Cm");
                        $("#wrist_ip").val(parseFloat(sizeData.Wrist).toFixed(2));
                        $("#calf").html(((parseFloat(sizeData.Calf_Right)+ parseFloat(sizeData.Calf_Left))/2).toFixed(2)+" Cm");
                        $("#calf_ip").val(((parseFloat(sizeData.Calf_Right)+ parseFloat(sizeData.Calf_Left))/2).toFixed(2) );
                        $("#back_waist_height").html(parseFloat(sizeData.Back_Waist_Height).toFixed(2) + " Cm");
                        $("#back_waist_height_ip").val(parseFloat(sizeData.Back_Waist_Height).toFixed(2));
                        $("#front_waist_height").html(parseFloat(sizeData.Front_Waist_Height).toFixed(2) + " Cm");
                        $("#front_waist_height_ip").val(parseFloat(sizeData.Front_Waist_Height).toFixed(2));
                        $("#knee_right").html(((parseFloat(sizeData.Knee_Right)+ parseFloat(sizeData.Knee_Left))/2).toFixed(2)+" Cm");
                        $("#knee_right_ip").val(((parseFloat(sizeData.Knee_Right)+ parseFloat(sizeData.Knee_Left))/2).toFixed(2));
                        $("#back_jacket_length").html(parseFloat(sizeData.Back_Jacket_Length).toFixed(2) + " Cm");
                        $("#back_jacket_length_ip").val(parseFloat(sizeData.Back_Jacket_Length).toFixed(2));
                        $("#u_rise").html(parseFloat(sizeData.U_rise).toFixed(2) + " Cm");
                        $("#u_rise_ip").val(parseFloat(sizeData.U_rise).toFixed(2));
                        $("#outseam_l_pants").html(parseFloat(sizeData.Outseam_L_pants).toFixed(2) + " Cm");
                        $("#outseam_l_pants_ip").val(parseFloat(sizeData.Outseam_L_pants).toFixed(2));
                        $("#outseam_r_pants").html(parseFloat(sizeData.Outseam_R_Pants).toFixed(2) + " Cm");
                        $("#outseam_r_pants_ip").val(parseFloat(sizeData.Outseam_R_Pants).toFixed(2));
                        $("#bottom").html(parseFloat(sizeData.Bottom_for_trousers).toFixed(2) + " Cm");
                        $("#bottom_ip").val(parseFloat(sizeData.Bottom_for_trousers).toFixed(2));
                        shoulder_slope_left = parseFloat(sizeData.Shoulder_Slope_Left).toFixed(2);
                        shoulder_slope_right = parseFloat(sizeData.Shoulder_Slope_Right).toFixed(2);
                    }
                    $("#size").html(sizeData.Size);
                    $("#size_ip").val(sizeData.Size);
                    $("#dimes_ul").show();
                    $(".humanbody").show();
                    $(".error").css("display", "none");
                    storeValuesOfTwinGenie();
                }
                else {
                    showMessage(data.Message, 'red');
                    $("#dimes_ul").hide();
                    $(".humanbody").hide();
                    $("#downloadbtn").css("display", "none");
                    $(".error").css("display", "block");
                }
            }).fail(function () {
                showMessage("Server Error !!! Please Try After Some Time....", 'red');
            });
        }
        function getDatafromOrd() {
            var ut = $("#params").val();
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url, {"type": "getDatafromOrd","user_id":user_id}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    if (ut == "Inches") {
                        var sizeData = data.sizeData_in;
                        $("#collar").html(parseFloat(sizeData.Collar).toFixed(2) + " Inches");
                        $("#collar_ip").val(parseFloat(sizeData.Collar).toFixed(2));
                        $("#chest_front").html(parseFloat(sizeData.t2_Chest).toFixed(2) + " Inches");
                        $("#chest_front_ip").val(parseFloat(sizeData.t2_Chest).toFixed(2));
                        $("#stomach").html(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2) + " Inches");
                        $("#stomach_ip").val(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2));
                        $("#seat_front").html(parseFloat(sizeData.t4_Seat).toFixed(2) + " Inches");
                        $("#seat_front_ip").val(parseFloat(sizeData.t4_Seat).toFixed(2));
                        $("#bicep_front").html(parseFloat(sizeData.Bicep).toFixed(2) + " Inches");
                        $("#bicep_front_ip").val(parseFloat(sizeData.Bicep).toFixed(2));
                        $("#shoulder_back").html(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2) + " Inches");
                        $("#shoulder_back_ip").val(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2));
                        $("#shoulder_front").html(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2) + " Inches");
                        $("#shoulder_front_ip").val(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2));
                        $("#shoulder_sleeve_right").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Inches");
                        $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#shoulder_sleeve_left").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Inches");
                        $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#thigh").html(parseFloat(sizeData.Thigh).toFixed(2) + " Inches");
                        $("#thigh_ip").val(parseFloat(sizeData.Thigh).toFixed(2));
                        $("#thigh_right").html(parseFloat(sizeData.Thigh_Right).toFixed(2) + " Inches");
                        $("#thigh_right_ip").val(parseFloat(sizeData.Thigh_Right).toFixed(2));
                        $("#napetowaist_back").html(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2) + " Inches");
                        $("#napetowaist_back_ip").val(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2));
                        $("#front_waist_length").html(parseFloat(sizeData.Front_Waist_Length).toFixed(2) + " Inches");
                        $("#front_waist_length_ip").val(parseFloat(sizeData.Front_Waist_Length).toFixed(2));
                        $("#wrist").html(parseFloat(sizeData.Wrist).toFixed(2) + " Inches");
                        $("#wrist_ip").val(parseFloat(sizeData.Wrist).toFixed(2));
                        $("#waist").html(parseFloat(sizeData.Waist).toFixed(2) + " Inches");
                        $("#waist_ip").val(parseFloat(sizeData.Waist).toFixed(2));
                        $("#calf").html(((parseFloat(sizeData.Calf_Right)+ parseFloat(sizeData.Calf_Left)) / 2).toFixed(2) + " Inches");
                        $("#calf_ip").val(((parseFloat(sizeData.Calf_Right) + parseFloat(sizeData.Calf_Left)) / 2).toFixed(2));
                        $("#back_waist_height").html(parseFloat(sizeData.Back_Waist_Height).toFixed(2) + " Inches");
                        $("#back_waist_height_ip").val(parseFloat(sizeData.Back_Waist_Height).toFixed(2));
                        $("#front_waist_height").html(parseFloat(sizeData.Front_Waist_Height).toFixed(2) + " Inches");
                        $("#front_waist_height_ip").val(parseFloat(sizeData.Front_Waist_Height).toFixed(2));
                        $("#knee_right").html(((parseFloat(sizeData.Knee_Right) + parseFloat(sizeData.Knee_Left)) / 2).toFixed(2) + " Inches");
                        $("#knee_right_ip").val(((parseFloat(sizeData.Knee_Right)+ parseFloat(sizeData.Knee_Left)) / 2).toFixed(2));
                        $("#back_jacket_length").html(parseFloat(sizeData.Back_Jacket_Length).toFixed(2) + " Inches");
                        $("#back_jacket_length_ip").val(parseFloat(sizeData.Back_Jacket_Length).toFixed(2));
                        $("#u_rise").html(parseFloat(sizeData.U_rise).toFixed(2) + " Inches");
                        $("#u_rise_ip").val(parseFloat(sizeData.U_rise).toFixed(2));
                        $("#outseam_l_pants").html(parseFloat(sizeData.Outseam_L_pants).toFixed(2) + " Inches");
                        $("#outseam_l_pants_ip").val(parseFloat(sizeData.Outseam_L_pants).toFixed(2));
                        $("#outseam_r_pants").html(parseFloat(sizeData.Outseam_R_Pants).toFixed(2) + " Inches");
                        $("#outseam_r_pants_ip").val(parseFloat(sizeData.Outseam_R_Pants).toFixed(2));
                        $("#bottom").html(parseFloat(sizeData.Bottom_for_trousers).toFixed(2) + " Inches");
                        $("#bottom_ip").val(parseFloat(sizeData.Bottom_for_trousers).toFixed(2));
                        shoulder_slope_left = parseFloat(sizeData.Shoulder_Slope_Left).toFixed(2);
                        shoulder_slope_right = parseFloat(sizeData.Shoulder_Slope_Right).toFixed(2);
                    } else {
                        var sizeData = data.sizeData_cm;
                        $("#collar").html(parseFloat(sizeData.Collar).toFixed(2) + " Cm");
                        $("#collar_ip").val(parseFloat(sizeData.Collar).toFixed(2));
                        $("#chest_front").html(parseFloat(sizeData.t2_Chest).toFixed(2) + " Cm");
                        $("#chest_front_ip").val(parseFloat(sizeData.t2_Chest).toFixed(2));
                        $("#stomach").html(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2) + " Cm");
                        $("#stomach_ip").val(parseFloat(sizeData.waist_belly_botton_Full).toFixed(2));
                        $("#seat_front").html(parseFloat(sizeData.t4_Seat).toFixed(2) + " Cm");
                        $("#seat_front_ip").val(parseFloat(sizeData.t4_Seat).toFixed(2));
                        $("#bicep_front").html(parseFloat(sizeData.Bicep).toFixed(2) + " Cm");
                        $("#bicep_front_ip").val(parseFloat(sizeData.Bicep).toFixed(2));
                        $("#shoulder_back").html(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2) + " Cm");
                        $("#shoulder_back_ip").val(parseFloat(sizeData.Back_Shoulder_Width).toFixed(2));
                        $("#shoulder_front").html(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2) + " Cm");
                        $("#shoulder_front_ip").val(parseFloat(sizeData.Front_Shoulder_Width).toFixed(2));
                        $("#shoulder_sleeve_right").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Cm");
                        $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#shoulder_sleeve_left").html(parseFloat(sizeData.Sleeve_Length).toFixed(2) + " Cm");
                        $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.Sleeve_Length).toFixed(2));
                        $("#thigh").html(parseFloat(sizeData.Thigh).toFixed(2) + " Cm");
                        $("#thigh_ip").val(parseFloat(sizeData.Thigh).toFixed(2));
                        $("#thigh_right").html(parseFloat(sizeData.Thigh_Right).toFixed(2) + " Cm");
                        $("#thigh_right_ip").val(parseFloat(sizeData.Thigh_Right).toFixed(2));
                        $("#napetowaist_back").html(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2) + " Cm");
                        $("#napetowaist_back_ip").val(parseFloat(sizeData.Nape_to_Waist_Back).toFixed(2));
                        $("#front_waist_length").html(parseFloat(sizeData.Front_Waist_Length).toFixed(2) + " Cm");
                        $("#front_waist_length_ip").val(parseFloat(sizeData.Front_Waist_Length).toFixed(2));
                        $("#wrist").html(parseFloat(sizeData.Wrist).toFixed(2) + " Cm");
                        $("#wrist_ip").val(parseFloat(sizeData.Wrist).toFixed(2));
                        $("#waist").html(parseFloat(sizeData.Waist).toFixed(2) + " Cm");
                        $("#waist_ip").val(parseFloat(sizeData.Waist).toFixed(2));
                        $("#calf").html(((parseFloat(sizeData.Calf_Right)+ parseFloat(sizeData.Calf_Left))/ 2).toFixed(2) + " Cm");
                        $("#calf_ip").val(((parseFloat(sizeData.Calf_Right).toFixed(2) + parseFloat(sizeData.Calf_Left).toFixed(2)) / 2).toFixed(2));
                        $("#back_waist_height").html(parseFloat(sizeData.Back_Waist_Height).toFixed(2) + " Cm");
                        $("#back_waist_height_ip").val(parseFloat(sizeData.Back_Waist_Height).toFixed(2));
                        $("#front_waist_height").html(parseFloat(sizeData.Front_Waist_Height).toFixed(2) + " Cm");
                        $("#front_waist_height_ip").val(parseFloat(sizeData.Front_Waist_Height).toFixed(2));
                        $("#knee_right").html(((parseFloat(sizeData.Knee_Right)+parseFloat(sizeData.Knee_Left)) / 2).toFixed(2) + " Cm");
                        $("#knee_right_ip").val(((parseFloat(sizeData.Knee_Right)+ parseFloat(sizeData.Knee_Left)) / 2).toFixed(2));
                        $("#back_jacket_length").html(parseFloat(sizeData.Back_Jacket_Length).toFixed(2) + " Cm");
                        $("#back_jacket_length_ip").val(parseFloat(sizeData.Back_Jacket_Length).toFixed(2));
                        $("#u_rise").html(parseFloat(sizeData.U_rise).toFixed(2) + " Cm");
                        $("#u_rise_ip").val(parseFloat(sizeData.U_rise).toFixed(2));
                        $("#outseam_l_pants").html(parseFloat(sizeData.Outseam_L_pants).toFixed(2) + " Cm");
                        $("#outseam_l_pants_ip").val(parseFloat(sizeData.Outseam_L_pants).toFixed(2));
                        $("#outseam_r_pants").html(parseFloat(sizeData.Outseam_R_Pants).toFixed(2) + " Cm");
                        $("#outseam_r_pants_ip").val(parseFloat(sizeData.Outseam_R_Pants).toFixed(2));
                        $("#bottom").html(parseFloat(sizeData.Bottom_for_trousers).toFixed(2) + " Cm");
                        $("#bottom_ip").val(parseFloat(sizeData.Bottom_for_trousers).toFixed(2));
                        shoulder_slope_left = parseFloat(sizeData.Shoulder_Slope_Left).toFixed(2);
                        shoulder_slope_right = parseFloat(sizeData.Shoulder_Slope_Right).toFixed(2);
                    }
                    $("#size").html(sizeData.Size);
                    $("#size_ip").val(sizeData.Size);
                    $("#dimes_ul").show();
                    $(".humanbody").show();
                    $(".error").css("display", "none");
                    storeValuesOfTwinGenie();
                }
                else {
                    showMessage(data.Message, 'red');
                    $("#dimes_ul").hide();
                    $(".humanbody").hide();
                    $("#downloadbtn").css("display", "none");
                    $(".error").css("display", "block");
                }
            }).fail(function () {
                showMessage("Server Error !!! Please Try After Some Time....", 'red');
            });
        }
        function downloadORDFile(user_id) {
            var unit_type = $("#params").val();
            if (unit_type == "Inches") {
                $("#inch_ord").trigger("click");
            } else if (unit_type == "Cm") {
                $("#cm_ord").trigger("click");
            }
        }
        function storeValuesOfTwinGenie() {
            var ut = $("#params").val();
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            var collar = $("#collar_ip").val();
            var chest = $("#chest_front_ip").val();
            var stomach = $("#stomach_ip").val();
            var seat = $("#seat_front_ip").val();
            var bicep = $("#bicep_front_ip").val();
            var back_shoulder = $("#shoulder_back_ip").val();
            var front_shoulder = $("#shoulder_front_ip").val();
            var sleeve_length_right = $("#shoulder_sleeve_right_ip").val();
            var sleeve_length_left = $("#shoulder_sleeve_left_ip").val();
            var thigh = $("#thigh_ip").val();
            var nape_to_waist = $("#napetowaist_back_ip").val();
            var front_waist_length = $("#front_waist_length_ip").val();
            var wrist = $("#wrist_ip").val();
            var waist = $("#waist_ip").val();
            var calf = $("#calf_ip").val();
            var back_waist_height = $("#back_waist_height_ip").val();
            var front_waist_height = $("#front_waist_height_ip").val();
            var knee = $("#knee_right_ip").val();
            var back_jacket_length = $("#back_jacket_length_ip").val();
            var u_rise = $("#u_rise_ip").val();
            var pant_left_outseam = $("#outseam_l_pants_ip").val();
            var pant_right_outseam = $("#outseam_r_pants_ip").val();
            var bottom = $("#bottom_ip").val();
            $.post(url, {
                "type": "storeTwinData",
                "ut": ut,
                "collar": collar,
                "chest": chest,
                "stoamch": stomach,
                "seat": seat,
                "bicep": bicep,
                "back_shoulder": back_shoulder,
                "front_shoulder": front_shoulder,
                "sleeve_length_right": sleeve_length_right,
                "sleeve_length_left": sleeve_length_left,
                "thigh": thigh,
                "nape_to_waist": nape_to_waist,
                "front_waist_length": front_waist_length,
                "wrist": wrist,
                "waist": waist,
                "calf": calf,
                "back_waist_height": back_waist_height,
                "front_waist_height": front_waist_height,
                "knee": knee,
                "back_jacket_length": back_jacket_length,
                "u_rise": u_rise,
                "pant_left_outseam": pant_left_outseam,
                "pant_right_outseam": pant_right_outseam,
                "bottom": bottom,
                "user_id": user_id,
                "shoulder_slope_left":shoulder_slope_left,
                "shoulder_slope_right":shoulder_slope_right
            }, function (data) {
                var Status = data.Status;
                if (Status != "Success") {
                    showMessage(data.Message, "red");
                }
                else if(Status == "Success")
                {

                }
            });
        }
        function downloadXLSFile(user_id) {
            var unit_type = $("#params").val();
            if (unit_type == "Inches") {
                $("#inch_ordxls").trigger("click");
            } else if (unit_type == "Cm") {
                $("#cm_ordxls").trigger("click");
            }
        }
        function parseData(data){
            var ut = $("#params").val();
            var sizeData = data;
            $("#data_source").html("Saved Values");
            if(sizeData.unit_type == "Inches"){
                if (ut == "Inches") {
                    $("#collar").html(parseFloat(sizeData.collar).toFixed(2) + " Inches");
                    $("#collar_ip").val(parseFloat(sizeData.collar).toFixed(2));
                    $("#chest_front").html(parseFloat(sizeData.chest).toFixed(2) + " Inches");
                    $("#chest_front_ip").val(parseFloat(sizeData.chest).toFixed(2));
                    $("#stomach").html(parseFloat(sizeData.stomach).toFixed(2) + " Inches");
                    $("#stomach_ip").val(parseFloat(sizeData.stomach).toFixed(2));
                    $("#seat_front").html(parseFloat(sizeData.seat).toFixed(2) + " Inches");
                    $("#seat_front_ip").val(parseFloat(sizeData.seat).toFixed(2));
                    $("#bicep_front").html(parseFloat(sizeData.bicep).toFixed(2) + " Inches");
                    $("#bicep_front_ip").val(parseFloat(sizeData.bicep).toFixed(2));
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder).toFixed(2) + " Inches");
                    $("#shoulder_back_ip").val(parseFloat(sizeData.back_sholuder).toFixed(2));
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder).toFixed(2) + " Inches");
                    $("#shoulder_front_ip").val(parseFloat(sizeData.front_shoulder).toFixed(2));
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left).toFixed(2) + " Inches");
                    $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.sleeve_length_left).toFixed(2));
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right).toFixed(2) + " Inches");
                    $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.sleeve_length_right).toFixed(2));
                    $("#thigh").html(parseFloat(sizeData.thigh).toFixed(2) + " Inches");
                    $("#thigh_ip").val(parseFloat(sizeData.thigh).toFixed(2));
                    $("#thigh_right").html(parseFloat(sizeData.thigh).toFixed(2) + " Inches");
                    $("#thigh_right_ip").val(parseFloat(sizeData.thigh).toFixed(2));
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist).toFixed(2) + " Inches");
                    $("#napetowaist_back_ip").val(parseFloat(sizeData.nape_to_waist).toFixed(2));
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length).toFixed(2) + " Inches");
                    $("#front_waist_length_ip").val(parseFloat(sizeData.front_waist_length).toFixed(2));
                    $("#wrist").html(parseFloat(sizeData.wrist).toFixed(2) + " Inches");
                    $("#wrist_ip").val(parseFloat(sizeData.wrist).toFixed(2));
                    $("#waist").html(parseFloat(sizeData.waist).toFixed(2) + " Inches");
                    $("#waist_ip").val(parseFloat(sizeData.waist).toFixed(2));
                    $("#calf").html(parseFloat(sizeData.calf).toFixed(2) + " Inches");
                    $("#calf_ip").val(parseFloat(sizeData.calf).toFixed(2));
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height).toFixed(2) + " Inches");
                    $("#back_waist_height_ip").val(parseFloat(sizeData.back_waist_height).toFixed(2));
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height).toFixed(2) + " Inches");
                    $("#front_waist_height_ip").val(parseFloat(sizeData.front_waist_height).toFixed(2));
                    $("#knee_right").html(parseFloat(sizeData.knee).toFixed(2) + " Inches");
                    $("#knee_right_ip").val(parseFloat(sizeData.knee).toFixed(2));
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length).toFixed(2) + " Inches");
                    $("#back_jacket_length_ip").val(parseFloat(sizeData.back_jacket_length).toFixed(2));
                    $("#u_rise").html(parseFloat(sizeData.u_rise).toFixed(2) + " Inches");
                    $("#u_rise_ip").val(parseFloat(sizeData.u_rise).toFixed(2));
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam).toFixed(2) + " Inches");
                    $("#outseam_l_pants_ip").val(parseFloat(sizeData.pant_left_outseam).toFixed(2));
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam).toFixed(2) + " Inches");
                    $("#outseam_r_pants_ip").val(parseFloat(sizeData.pant_right_outseam).toFixed(2));
                    $("#bottom").html(parseFloat(sizeData.bottom).toFixed(2) + " Inches");
                    $("#bottom_ip").val(parseFloat(sizeData.bottom).toFixed(2));
                    shoulder_slope_left = parseFloat(sizeData.shoulder_slope_left).toFixed(2);
                    shoulder_slope_right = parseFloat(sizeData.shoulder_slope_right).toFixed(2);
                } else {
                    $("#collar").html(parseFloat(sizeData.collar*2.54).toFixed(2) + " Cm");
                    $("#collar_ip").val(parseFloat(sizeData.collar*2.54).toFixed(2));
                    $("#chest_front").html(parseFloat(sizeData.chest*2.54).toFixed(2) + " Cm");
                    $("#chest_front_ip").val(parseFloat(sizeData.chest*2.54).toFixed(2));
                    $("#stomach").html(parseFloat(sizeData.stomach*2.54).toFixed(2) + " Cm");
                    $("#stomach_ip").val(parseFloat(sizeData.stomach*2.54).toFixed(2));
                    $("#seat_front").html(parseFloat(sizeData.seat*2.54).toFixed(2) + " Cm");
                    $("#seat_front_ip").val(parseFloat(sizeData.seat*2.54).toFixed(2));
                    $("#bicep_front").html(parseFloat(sizeData.bicep*2.54).toFixed(2) + " Cm");
                    $("#bicep_front_ip").val(parseFloat(sizeData.bicep*2.54).toFixed(2));
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder*2.54).toFixed(2) + " Cm");
                    $("#shoulder_back_ip").val(parseFloat(sizeData.back_sholuder*2.54).toFixed(2));
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder*2.54).toFixed(2) + " Cm");
                    $("#shoulder_front_ip").val(parseFloat(sizeData.front_shoulder*2.54).toFixed(2));
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left*2.54).toFixed(2) + " Cm");
                    $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.sleeve_length_left*2.54).toFixed(2));
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right*2.54).toFixed(2) + " Cm");
                    $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.sleeve_length_right*2.54).toFixed(2));
                    $("#thigh").html(parseFloat(sizeData.thigh*2.54).toFixed(2) + " Cm");
                    $("#thigh_ip").val(parseFloat(sizeData.thigh*2.54).toFixed(2));
                    $("#thigh_right").html(parseFloat(sizeData.thigh*2.54).toFixed(2) + " Cm");
                    $("#thigh_right_ip").val(parseFloat(sizeData.thigh*2.54).toFixed(2));
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist*2.54).toFixed(2) + " Cm");
                    $("#napetowaist_back_ip").val(parseFloat(sizeData.nape_to_waist*2.54).toFixed(2));
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length*2.54).toFixed(2) + " Cm");
                    $("#front_waist_length_ip").val(parseFloat(sizeData.front_waist_length*2.54).toFixed(2));
                    $("#wrist").html(parseFloat(sizeData.wrist*2.54).toFixed(2) + " Cm");
                    $("#wrist_ip").val(parseFloat(sizeData.wrist*2.54).toFixed(2));
                    $("#waist").html(parseFloat(sizeData.waist*2.54).toFixed(2) + " Cm");
                    $("#waist_ip").val(parseFloat(sizeData.waist*2.54).toFixed(2));
                    $("#calf").html(parseFloat(sizeData.calf*2.54).toFixed(2) + " Cm");
                    $("#calf_ip").val(parseFloat(sizeData.calf*2.54).toFixed(2));
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height*2.54).toFixed(2) + " Cm");
                    $("#back_waist_height_ip").val(parseFloat(sizeData.back_waist_height*2.54).toFixed(2));
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height*2.54).toFixed(2) + " Cm");
                    $("#front_waist_height_ip").val(parseFloat(sizeData.front_waist_height*2.54).toFixed(2));
                    $("#knee_right").html(parseFloat(sizeData.knee*2.54).toFixed(2) + " Cm");
                    $("#knee_right_ip").val(parseFloat(sizeData.knee*2.54).toFixed(2));
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length*2.54).toFixed(2) + " Cm");
                    $("#back_jacket_length_ip").val(parseFloat(sizeData.back_jacket_length*2.54).toFixed(2));
                    $("#u_rise").html(parseFloat(sizeData.u_rise*2.54).toFixed(2) + " Cm");
                    $("#u_rise_ip").val(parseFloat(sizeData.u_rise*2.54).toFixed(2));
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam*2.54).toFixed(2) + " Cm");
                    $("#outseam_l_pants_ip").val(parseFloat(sizeData.pant_left_outseam*2.54).toFixed(2));
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam*2.54).toFixed(2) + " Cm");
                    $("#outseam_r_pants_ip").val(parseFloat(sizeData.pant_right_outseam*2.54).toFixed(2));
                    $("#bottom").html(parseFloat(sizeData.bottom*2.54).toFixed(2) + " Cm");
                    $("#bottom_ip").val(parseFloat(sizeData.bottom*2.54).toFixed(2));
                    shoulder_slope_left = parseFloat(sizeData.shoulder_slope_left).toFixed(2);
                    shoulder_slope_right = parseFloat(sizeData.shoulder_slope_right).toFixed(2);
                }
            }else{
                if (ut == "Inches") {
                    $("#collar").html(parseFloat(sizeData.collar/2.54).toFixed(2) + " Inches");
                    $("#collar_ip").val(parseFloat(sizeData.collar/2.54).toFixed(2));
                    $("#chest_front").html(parseFloat(sizeData.chest/2.54).toFixed(2) + " Inches");
                    $("#chest_front_ip").val(parseFloat(sizeData.chest/2.54).toFixed(2));
                    $("#stomach").html(parseFloat(sizeData.stomach/2.54).toFixed(2) + " Inches");
                    $("#stomach_ip").val(parseFloat(sizeData.stomach/2.54).toFixed(2));
                    $("#seat_front").html(parseFloat(sizeData.seat/2.54).toFixed(2) + " Inches");
                    $("#seat_front_ip").val(parseFloat(sizeData.seat/2.54).toFixed(2));
                    $("#bicep_front").html(parseFloat(sizeData.bicep/2.54).toFixed(2) + " Inches");
                    $("#bicep_front_ip").val(parseFloat(sizeData.bicep/2.54).toFixed(2));
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder/2.54).toFixed(2) + " Inches");
                    $("#shoulder_back_ip").val(parseFloat(sizeData.back_sholuder/2.54).toFixed(2));
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder/2.54).toFixed(2) + " Inches");
                    $("#shoulder_front_ip").val(parseFloat(sizeData.front_shoulder/2.54).toFixed(2));
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left/2.54).toFixed(2) + " Inches");
                    $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.sleeve_length_left/2.54).toFixed(2));
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right/2.54).toFixed(2) + " Inches");
                    $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.sleeve_length_right/2.54).toFixed(2));
                    $("#thigh").html(parseFloat(sizeData.thigh/2.54).toFixed(2) + " Inches");
                    $("#thigh_ip").val(parseFloat(sizeData.thigh/2.54).toFixed(2));
                    $("#thigh_right").html(parseFloat(sizeData.thigh/2.54).toFixed(2) + " Inches");
                    $("#thigh_right_ip").val(parseFloat(sizeData.thigh/2.54).toFixed(2));
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist/2.54).toFixed(2) + " Inches");
                    $("#napetowaist_back_ip").val(parseFloat(sizeData.nape_to_waist/2.54).toFixed(2));
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length/2.54).toFixed(2) + " Inches");
                    $("#front_waist_length_ip").val(parseFloat(sizeData.front_waist_length/2.54).toFixed(2));
                    $("#wrist").html(parseFloat(sizeData.wrist/2.54).toFixed(2) + " Inches");
                    $("#wrist_ip").val(parseFloat(sizeData.wrist/2.54).toFixed(2));
                    $("#waist").html(parseFloat(sizeData.waist/2.54).toFixed(2) + " Inches");
                    $("#waist_ip").val(parseFloat(sizeData.waist/2.54).toFixed(2));
                    $("#calf").html(parseFloat(sizeData.calf/2.54).toFixed(2) + " Inches");
                    $("#calf_ip").val(parseFloat(sizeData.calf/2.54).toFixed(2));
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height/2.54).toFixed(2) + " Inches");
                    $("#back_waist_height_ip").val(parseFloat(sizeData.back_waist_height/2.54).toFixed(2));
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height/2.54).toFixed(2) + " Inches");
                    $("#front_waist_height_ip").val(parseFloat(sizeData.front_waist_height/2.54).toFixed(2));
                    $("#knee_right").html(parseFloat(sizeData.knee/2.54).toFixed(2) + " Inches");
                    $("#knee_right_ip").val(parseFloat(sizeData.knee/2.54).toFixed(2));
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length/2.54).toFixed(2) + " Inches");
                    $("#back_jacket_length_ip").val(parseFloat(sizeData.back_jacket_length/2.54).toFixed(2));
                    $("#u_rise").html(parseFloat(sizeData.u_rise/2.54).toFixed(2) + " Inches");
                    $("#u_rise_ip").val(parseFloat(sizeData.u_rise/2.54).toFixed(2));
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam/2.54).toFixed(2) + " Inches");
                    $("#outseam_l_pants_ip").val(parseFloat(sizeData.pant_left_outseam/2.54).toFixed(2));
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam/2.54).toFixed(2) + " Inches");
                    $("#outseam_r_pants_ip").val(parseFloat(sizeData.pant_right_outseam/2.54).toFixed(2));
                    $("#bottom").html(parseFloat(sizeData.bottom/2.54).toFixed(2) + " Inches");
                    $("#bottom_ip").val(parseFloat(sizeData.bottom/2.54).toFixed(2));
                    shoulder_slope_left = parseFloat(sizeData.shoulder_slope_left).toFixed(2);
                    shoulder_slope_right = parseFloat(sizeData.shoulder_slope_right).toFixed(2);
                } else {
                    $("#collar").html(parseFloat(sizeData.collar).toFixed(2) + " Cm");
                    $("#collar_ip").val(parseFloat(sizeData.collar).toFixed(2));
                    $("#chest_front").html(parseFloat(sizeData.chest).toFixed(2) + " Cm");
                    $("#chest_front_ip").val(parseFloat(sizeData.chest).toFixed(2));
                    $("#stomach").html(parseFloat(sizeData.stomach).toFixed(2) + " Cm");
                    $("#stomach_ip").val(parseFloat(sizeData.stomach).toFixed(2));
                    $("#seat_front").html(parseFloat(sizeData.seat).toFixed(2) + " Cm");
                    $("#seat_front_ip").val(parseFloat(sizeData.seat).toFixed(2));
                    $("#bicep_front").html(parseFloat(sizeData.bicep).toFixed(2) + " Cm");
                    $("#bicep_front_ip").val(parseFloat(sizeData.bicep).toFixed(2));
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder).toFixed(2) + " Cm");
                    $("#shoulder_back_ip").val(parseFloat(sizeData.back_sholuder).toFixed(2));
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder).toFixed(2) + " Cm");
                    $("#shoulder_front_ip").val(parseFloat(sizeData.front_shoulder).toFixed(2));
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left).toFixed(2) + " Cm");
                    $("#shoulder_sleeve_right_ip").val(parseFloat(sizeData.sleeve_length_left).toFixed(2));
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right).toFixed(2) + " Cm");
                    $("#shoulder_sleeve_left_ip").val(parseFloat(sizeData.sleeve_length_right).toFixed(2));
                    $("#thigh").html(parseFloat(sizeData.thigh).toFixed(2) + " Cm");
                    $("#thigh_ip").val(parseFloat(sizeData.thigh).toFixed(2));
                    $("#thigh_right").html(parseFloat(sizeData.thigh).toFixed(2) + " Cm");
                    $("#thigh_right_ip").val(parseFloat(sizeData.thigh).toFixed(2));
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist).toFixed(2) + " Cm");
                    $("#napetowaist_back_ip").val(parseFloat(sizeData.nape_to_waist).toFixed(2));
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length).toFixed(2) + " Cm");
                    $("#front_waist_length_ip").val(parseFloat(sizeData.front_waist_length).toFixed(2));
                    $("#wrist").html(parseFloat(sizeData.wrist).toFixed(2) + " Cm");
                    $("#wrist_ip").val(parseFloat(sizeData.wrist).toFixed(2));
                    $("#waist").html(parseFloat(sizeData.waist).toFixed(2) + " Cm");
                    $("#waist_ip").val(parseFloat(sizeData.waist).toFixed(2));
                    $("#calf").html(parseFloat(sizeData.calf).toFixed(2) + " Cm");
                    $("#calf_ip").val(parseFloat(sizeData.calf).toFixed(2));
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height).toFixed(2) + " Cm");
                    $("#back_waist_height_ip").val(parseFloat(sizeData.back_waist_height).toFixed(2));
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height).toFixed(2) + " Cm");
                    $("#front_waist_height_ip").val(parseFloat(sizeData.front_waist_height).toFixed(2));
                    $("#knee_right").html(parseFloat(sizeData.knee).toFixed(2) + " Cm");
                    $("#knee_right_ip").val(parseFloat(sizeData.knee).toFixed(2));
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length).toFixed(2) + " Cm");
                    $("#back_jacket_length_ip").val(parseFloat(sizeData.back_jacket_length).toFixed(2));
                    $("#u_rise").html(parseFloat(sizeData.u_rise).toFixed(2) + " Cm");
                    $("#u_rise_ip").val(parseFloat(sizeData.u_rise).toFixed(2));
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam).toFixed(2) + " Cm");
                    $("#outseam_l_pants_ip").val(parseFloat(sizeData.pant_left_outseam).toFixed(2));
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam).toFixed(2) + " Cm");
                    $("#outseam_r_pants_ip").val(parseFloat(sizeData.pant_right_outseam).toFixed(2));
                    $("#bottom").html(parseFloat(sizeData.bottom).toFixed(2) + " Cm");
                    $("#bottom_ip").val(parseFloat(sizeData.bottom).toFixed(2));
                    shoulder_slope_left = parseFloat(sizeData.shoulder_slope_left).toFixed(2);
                    shoulder_slope_right = parseFloat(sizeData.shoulder_slope_right).toFixed(2);
                }
            }

            /*for auto select data start here*/
            var shoulder_slope = data.shoulder_slope;
            var shoulder_slope = shoulder_slope.split(",");
            var shoulder_slope_left = shoulder_slope[0];
            var shoulder_slope_right = shoulder_slope[1];
            $("input[name='first_ques'][value='"+data.shoulder_forward+"' ]").prop('checked', true);
            $("input[name='second_ques'][value='"+data.body_forward+"' ]").prop('checked', true);
            $('#thirdQuestion_left > option[value="'+shoulder_slope_left+'"]').attr("selected", "selected");
            $('#thirdQuestion_right > option[value="'+shoulder_slope_right+'"]').attr("selected", "selected");
            $("input[name='forth_ques'][value='"+data.belly_shape+"']").prop('checked', true);
            $("input[name='fifth_ques'][value='"+data.style_arms+"']").prop('checked', true);
            $("input[name='sixth_ques'][value='"+data.style_seat+"']").prop('checked', true);
            $("input[name='sixth_ques_stoop'][value='"+data.sixth_ques_stoop+"']").prop('checked', true);
            $("input[name='shirt_ques1'][value='"+data.shirt_que_1+"']").prop('checked', true);
            $("input[name='shirt_ques2'][value='"+data.shirt_que_2+"']").prop('checked', true);
            $("input[name='shirt_ques3'][value='"+data.shirt_que_3+"']").prop('checked', true);
            $("input[name='shirt_ques4'][value='"+data.shirt_que_4+"']").prop('checked', true);
            $("input[name='shirt_ques5'][value='"+data.shirt_que_5+"']").prop('checked', true);
            $("input[name='shirt_ques6'][value='"+data.shirt_que_6+"']").prop('checked', true);
            $("input[name='suit_ques1'][value='"+data.suit_que_1+"']").prop('checked', true);
            $("input[name='suit_ques2'][value='"+data.suit_que_2+"']").prop('checked', true);
            $("input[name='suit_ques3'][value='"+data.suit_que_3+"']").prop('checked', true);
            $("input[name='suit_ques4'][value='"+data.suit_que_4+"']").prop('checked', true);
            $("input[name='suit_ques5'][value='"+data.suit_que_5+"']").prop('checked', true);
            $("input[name='vest_ques1'][value='"+data.vest_que_1+"']").prop('checked', true);
            $("input[name='pant_ques1'][value='"+data.pant_que_1+"']").prop('checked', true);


            /*for auto select data end here*/

            $("#size").html(sizeData.Size);
            $("#size_ip").val(sizeData.Size);
            $("#dimes_ul").show();
            $(".humanbody").show();
            $(".error").css("display", "none");
            storeValuesOfTwinGenie();
        }
        function needToShowPopUp() {
            $(".loaderr").removeClass("showloader");
            $(".loaderr").addClass("hideloader");
            $("#dimes_ul").hide();
            $(".humanbody").hide();
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url, {"type": "getStyleGenieData", "user_id": user_id}, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    parseData(data.userData);
                }
                else{
                    var url = "api/registerUser.php";
                    $.post(url, {"type": "isOrdExist", "user_id": user_id}, function (data) {
                        var Status = data.Status;
                        if (Status == "Success") {
                            data_source = "ORD File";
                            $("#data_source").html(data_source);
                            getDatafromOrd();
                        }
                        else {
                            var url = "api/registerUser.php";
                            $.post(url, {"type": "getParticularUserData", "user_id": user_id}, function (data) {
                                var status = data.Status;
                                if (status == "Success") {
                                    var userData = data.userData;
                                    var brands = data.brands;
                                    var brand_names = "<option value=''>Select Brand</option>";
                                    for (var j = 0; j < brands.length; j++) {
                                        brand_names = brand_names + "<option value='" + brands[j].brand_name + "'>" + brands[j].brand_name + "</option>";
                                    }
                                    var height = userData.height_in;
                                    var weight = userData.weight;
                                    var gender = userData.gender;
                                    if (gender == "Male" || gender == "male") {
                                        var options = "<option value=''>Select Gender</option><option selected = 'selected' value='Male'>Male</option>" +
                                            "<option value='Female'>Female</option>";
                                    } else {
                                        var options = "<option value=''>Select Gender</option><option value='Male'>Male</option>" +
                                            "<option value='Female' selected='selected'>Female</option>";
                                    }
                                    var race = userData.race;
                                    var neck = userData.neck;
                                    var waist = userData.waist;
                                    var size = userData.size;
                                    var dob = userData.dob;
                                    //alert(dob);
                                    var age = userData.age;
                                    var country = data.country;
                                    var month="";
                                    var birthYear = dob.split("/")[2];
                                    var cyear = new Date().getFullYear();
                                    var cmonth = new Date().getMonth()+1;
                                    if(country == "United States"){
                                        month = dob.split("/")[0];
                                    }else{
                                        month = dob.split("/")[1];
                                    }
                                    age = cyear - birthYear;
                                    if(month>cmonth){
                                        age = age-1;
                                    }
                                    //alert(age);
                                    var brand = userData.brand;
                                    if (height == "" || weight == "" || gender == "" || race == "" || age == "" || neck == "" || brand == "" ||
                                        size == "" || waist == "") {
                                        $(".modal-title").html("Please fill your Measurements");
                                        $(".modal-body").html("<p style='display:block;text-align:center;color:red' id='errorr' ></p>" +
                                            "<div class='form-group col-md-6' id='heightfeild'><label>Height</label>" +
                                            "<div class='row'><div class='col-md-4'><select onchange='heightparam()' style='padding-left:0'" +
                                            " class='form-control' id='height_param'><option value='Inches'>Inches</option><option " +
                                            "value='Centimeter'>Centimeter</option><option value='Feet'>Feet</option></select></div>" +
                                            "<div style='padding-left:0' " +
                                            "class='col-md-8'><input type='text' class='form-control' placeholder='Height in Inches' " +
                                            "value='" + height + "' id='pop_height'/>" +
                                            "</div></div></div><div class='form-group col-md-6'><label>Weight</label><div class='row'>" +
                                            "<div class='col-md-4'><select class='form-control' id='weight_param' onchange='weightparam()'>" +
                                            "<option value='Pounds'>Pounds</option><option value='Kilogram'>Kilogram</option>" +
                                            "<option value='Stones'>Stones</option></select></div><div class='col-md-8' style='padding-left:0'><input type='text' " +
                                            "class='form-control' value='" + weight + "' id='pop_weight' placeholder='Weight in Lbs'/></div></div>" +
                                            "</div><div class='" +
                                            "form-group col-md-6'><label>Age in Years</label><input type='text' class='form-control' " +
                                            "value='" + age + "' id='pop_age'/></div><div class='form-group col-md-6'><label>Gender</label><select " +
                                            "class='form-control' id='pop_gender'>" + options + "</select></div><div class='form-group col-md-6'>" +
                                            "<label>Select Race</label><select class='form-control' id='pop_race'><option " +
                                            "value='Non-Hispanic White'>Non-Hispanic White</option><option value='Non-Hispanic Black'>" +
                                            "Non-Hispanic Black</option><option value='Non-Mexican Hispanic'>Non-Mexican Hispanic</option>" +
                                            "<option value='Mexican Hispanic'>Mexican Hispanic</option><option value='Asian'>Asian</option>" +
                                            "<option value='Other'>Other</option><option value='Blank'>Blank</option></select></div>" +
                                            "<div class='form-group col-md-6'><label>Neck</label><div class='row'><div class='col-md-4'>" +
                                            "<select class='form-control' id='neck_param' onchange='neckparam()'><option value='Inches'>" +
                                            "Inches</option><option value='Centimeter'>Centimeter</option></select></div><div class='col-md-8' " +
                                            "style='padding-left:0'><input type='text' class='form-control' placeholder='Neck in Inches' " +
                                            "value='" + neck + "' id='pop_neck'/>" +
                                            "</div></div></div><div class='form-group col-md-6'><label>Waist <i class='fa fa-info-circle' " +
                                            "title='Measure line where customer wears his pants.For Waist take real body measurment, we do not " +
                                            "add any allowance.'></i></label><div class='row'><div class='col-md-4'>" +
                                            "<select class='form-control' id='waist_param' onchange='waistparam()'><option value='Inches'>" +
                                            "Inches</option><option value='Centimeter'>Centimeter</option></select></div><div class='col-md-8' " +
                                            "style='padding-left:0'><input type='text' class='form-control' placeholder='Waist in Inches' " +
                                            "value='" + waist + "' id='pop_waist'/></div></div></div><div class='form-group col-md-6'>" +
                                            "<label>Favourite Brand</label><select class='form-control' id='pop_brand'>" + brand_names + "</select>" +
                                            "</div><div class='clearfix'></div>" +
                                            "<div class='form-group col-md-6'><label>Select Size</label><select onchange='ss()' class='form-control' id='sizes'>" +
                                            "<option value='S'>S</option><option value='M'>M</option><option value='L'>L</option>" +
                                            "<option value='XL'>XL</option><option value='XXL'>XXL</option><option value='XXXL'>XXXL</option>" +
                                            "<option value='Other'>Other</option></select></div>" +
                                            "<div id='otherdiv' class='form-group col-md-6' style='display:none'>" +
                                            "<label>Please Mention Size</label><input class='form-control' id='othersizeval'" +
                                            " type='text' value='' placeholder='Please Mention Size' /></div><div class='clearfix'></div>");
                                        $(".modal-footer").css("display", "block");
                                        $(".modal-footer").html("<input type='button' data-dismiss='modal' class='btn btn-default' " +
                                            "value='Cancel'><input type='button' class='btn btn-danger' value='Submit' " +
                                            "onclick=storeDimensions('" + user_id + "')>");
                                        if (race != "") {
                                            var appendData = "<option selected='selected' value='" + race + "'>" + race + "</option>";
                                            $("#pop_race").append(appendData);
                                        }
                                        if (size != "") {
                                            var sizeData = "<option selected='selected' value='" + size + "'>" + size + "</option>";
                                            $("#sizes").append(sizeData);
                                        }
                                        $("#modal").modal("show");
                                        data_source = "SpreadSheet";
                                    }
                                    else {
                                        // no need to show pop up //
                                        data_source = "SpreadSheet";
                                        getData('','');
                                    }
                                } else {
                                    showMessage("Session Expired !!! Please Login Again to Continue...")
                                    return false;
                                }
                            });
                        }
                    }).fail(function () {
                        showMessage("Server Error !!! Please Try After Some Time....", 'red');
                    });
                }
            });
        }
        function ss() {
            var size = $("#sizes").val();
            if (size == "Other") {
                $("#otherdiv").css("display", "block");
            }
            else {
                $("#otherdiv").css("display", "none");
            }
        }
        function heightparam() {
            var height_param = ($("#height_param").val());
            if (height_param == "Feet") {
                $("#heightfeild").html("<label>Height</label><div class='row'><div class='col-md-4'><select " +
                    "onchange='heightparam()' style='padding-left:0' class='form-control' id='height_param'>" +
                    "<option value='Inches'>Inches</option><option value='Centimeter'>Centimeter</option><option " +
                    "value='Feet' selected='selected' >Feet</option></select></div><div class='col-md-8' style='" +
                    "padding-left:0'><div class='col-md-6' style='padding-left:0'><input type='text' placeholder='Feet'" +
                    " class='form-control' value='' id='pop_height0'/></div><div class='col-md-6' style='padding:0'>" +
                    "<input type='text' placeholder='Inches' class='form-control col-md-6' value='' id='pop_height1'/>" +
                    "</div></div></div>");
            } else if (height_param == "Centimeter") {
                $("#heightfeild").html("<label>Height</label><div class='row'><div class='col-md-4'><select onchange=" +
                    "'heightparam()' style='padding-left:0' class='form-control' id='height_param'><option value='Inches'>Inches" +
                    "</option><option selected='selected' value='Centimeter'>Centimeter</option><option value='Feet'>Feet" +
                    "</option></select></div><div class='col-md-8' style='padding-left:0'><input type='text' class='" +
                    "form-control' placeholder='Height in Centimeters' value='' id='pop_height'/></div></div>");
            } else if (height_param == "Inches") {
                $("#heightfeild").html("<label>Height</label><div class='row'><div class='col-md-4'><select onchange='" +
                    "heightparam()' style='padding-left:0' class='form-control' id='height_param'><option value='Inches' " +
                    "selected='selected'>Inches</option><option value='Centimeter'>Centimeter</option><option value='Feet'>" +
                    "Feet</option></select></div><div class='col-md-8' style='padding-left:0'><input type='text' " +
                    "class='form-control' value='' placeholder='Height in Inches' id='pop_height'/></div></div>");
            }
        }
        function weightparam() {
            var weight_param = ($("#weight_param").val());
            var weight = $("#pop_weight").val("");
            $("#pop_weight").attr("placeholder", "Weight in " + weight_param);
        }
        function neckparam() {
            var neck_param = ($("#neck_param").val());
            var neck = $("#pop_neck").val("");
            $("#pop_neck").attr("placeholder", "Neck in " + neck_param);
        }
        function waistparam() {
            var waist_param = ($("#waist_param").val());
            var waist = $("#pop_waist").val("");
            $("#pop_waist").attr("placeholder", "Waist in " + waist_param);
        }
        function storeDimensions(user_id) {
            var height_param = $("#height_param").val();
            var pop_height = null;
            if (height_param == "Feet") {
                var feet = $("#pop_height0").val();
                var inches = $("#pop_height1").val();
                pop_height = parseFloat(feet * 12) + parseFloat(inches);
            } else if (height_param == "Centimeter") {
                pop_height = $("#pop_height").val() * 0.393701;
            }
            else {
                pop_height = $("#pop_height").val();
            }
            var weight_param = $("#weight_param").val();
            var pop_weight = null;
            if (weight_param == "Kilogram") {
                pop_weight = ($("#pop_weight").val() * 2.20462).toFixed(2);
            } else if (weight_param == "Stones") {
                pop_weight = ($("#pop_weight").val() * 14).toFixed(2);
            }
            else {
                pop_weight = $("#pop_weight").val();
            }
            var pop_age = $("#pop_age").val();
            var pop_gender = $("#pop_gender").val();
            var pop_race = $("#pop_race").val();
            var neck_param = $("#neck_param").val();
            var pop_neck = null;
            if (neck_param == "Centimeter") {
                pop_neck = ($("#pop_neck").val() * 0.393701).toFixed(2);
            }
            else {
                pop_neck = $("#pop_neck").val();
            }
            var pop_brand = $("#pop_brand").val();
            var size = $("#sizes").val();
            if (size == "Other") {
                if ($("#othersizeval").val() == "") {
                    $("#errorr").html("Please Mention Size First");
                    return false;
                } else {
                    size = $("#othersizeval").val();
                }
            }
            var waist_param = $("#waist_param").val();
            var pop_waist = null;
            if (waist_param == "Centimeter") {
                pop_waist = ($("#pop_waist").val() * 0.393701).toFixed(2);
            }
            else {
                pop_waist = $("#pop_waist").val();
            }
            if (height_param == "" || pop_height == "" || weight_param == "" || pop_weight == "" || pop_age == "" || pop_gender == ""
                || pop_race == "" || neck_param == "" || pop_neck == "" || pop_brand == "" || size == "" || waist_param == "" || pop_waist == "") {
                $("#errorr").html("Please Fill all the Required Feilds");
                return false;
            }
            var url = "api/registerUser.php";
            $.post(url, {
                "type": "updateDimensions",
                "user_id": user_id,
                "height": pop_height,
                "weight": pop_weight,
                "age": pop_age,
                "gender": pop_gender,
                "race": pop_race,
                "neck": pop_neck,
                "brand": pop_brand,
                "size": size,
                "waist": pop_waist
            }, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    $("#modal").modal('hide');
                    getData(pop_waist,pop_neck);
                } else {
                    $("#modal").modal('hide');
                    getData(pop_waist,pop_neck);
                    showMessage("Server Error !!! Please Try After Some Time....", 'red');
                }
            });
        }
        function reset() {
            $("#data_source").html("");
            $("#dimes_ul").hide();
            $(".humanbody").hide();
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url, {"type": "resetValues","user_id":user_id}, function (data) {
                $(".loaderr").addClass("showloader");
                var status = data.Status;
                if (status == "Success") {
                    $("#modal").modal('hide');
                    setTimeout(function () {
                        needToShowPopUp();
                    }, 1000);
                } else {
                    $("#modal").modal('hide');
                    $(".loaderr").removeClass("showloader");
                    $(".loaderr").addClass("hideloader");
                    showMessage("Server Error !!! Please Try After Some Time....", 'red');
                }
            });
        }
        function showMessage(message, color) {
            $(".modal-header").css({"display": "none"});
            $(".modal-body").css({"display": "block"});
            $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
            $(".modal-footer").css({"display": "none"});
            $("#modal").modal("show");
            setTimeout(function () {
                $("#modal").modal("hide");
            }, 3000);
        }
        if (window.location.href.indexOf("?up") >= 0) {
            uploadOrd();
        }
        if (window.location.href.indexOf("?mf") >= 0) {
            myfiles();
        }
        if (window.location.href.indexOf("?sm") >= 0) {
            viewSaved();
        }
        function getBodyForwardData(){
            var url = "api/registerUser.php";
            var user_id = $("#user_id").val();
            $.post(url, {"type": "getBodyForwardvalues","user_id":user_id}, function (data) {
                var Status = data.Status;
                if(Status == "Success"){
                    body_forward(data.value);
                }else{
                    body_forward(0);
                }
            });
        }
        needToShowPopUp();
    </script>
