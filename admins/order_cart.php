<meta content="text/html;charset=utf-8" http-equiv="Content-Type">
<meta content="utf-8" http-equiv="encoding">
<?php
include('header.php');
include("front_header.php");
include("../admin/webserver/database/db.php");
?>
<style>
    .nav_menu{
        margin-bottom: 0px;
    }
    .freebies_box > p {
        font-size: 14px;
        text-align: left;
    }

    .freebies_box > .btn {
        width: 100%;
        padding: 6px;
    }

    .top-bar {
        background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
    }

    .img {
        height: 150px;
        position: absolute;
        width: auto !important;
        margin-top: 20px;
    }

    .nodata {
        color: #002856;
        font-size: 19px;
        font-weight: bold;
        text-align: center;
        text-shadow: 10px 9px 3px #ccc;
    }



    table {
        font-family: arial, sans-serif;
        border-collapse: collapse;
        width: 100%;
    }

    td {
        max-width: 100px;
        overflow-wrap: break-word;
    }

    td, th {
        border: 1px solid #dddddd;
        font-size: 12px;
        letter-spacing: 1.2px;
        padding: 1.4px 16px;
        text-align: left;
    }

    tr:nth-child(even) {
        background-color: #eee;
    }

    .clearcartbtn {
        margin-bottom: 24px;
        margin-top: 0px;
        text-transform: uppercase;
        margin-right: 15px;
        border-radius: 4px;
        padding: 8px 25px;
        background: rgb(212, 80, 79) none repeat scroll 0% 0%;
        border: 1px solid rgb(212, 80, 79);
        color: white;
        display: none;
    }

    .coupon_amount {
        display: none;
        margin: 10px 0;
    }

    .coupon_amount_p {
        display: none;
        font-weight: bold;
        font-family: "fontscorecomttwcenmt" !important;
        font-size: 19px;
        margin: 10px 0;
    }
    .nav-md .container.body .right_col{
        background: #fff;
    }
</style>
<div class="right_col" role="main" style="overflow-y: auto;height: 700px!important;">
    <div class="about-bottom wthree-3" style="padding: 0px">
        <script src="../js/location.js"></script>
        <div class="loader">
            <img src="../images/spin.gif" class="spinloader"/>
        </div>
        <div class="col-md-12" style="margin-bottom: 30px">
            <div class="col-md-12">
                <p class="cartcaption">CHECKOUT CART</p>
                <div class="col-md-12">
                    <div class="col-md-6 optionbox">CART<br>
                        <span class="optionnumbox optionnumboxactive" style="cursor:pointer" onclick="backon_1()"
                              id="optioncart">1</span>
                    </div>
                    <div class="col-md-6 optionbox">PAYMENT CONFIRMATION<br>
                        <span class="optionnumbox" id="paymentcart">2</span>
                    </div>
                </div>
                <div class="col-md-12 breakingline2"></div>
                <div class="clear"></div>
                <button class='custommpbtn pull-right clearcartbtn' onclick=clear_cart('<?php echo $user_id ?>')
                        id='clear_cart'>Clear Cart
                </button>
                <div id="cart" class="row"></div>
                <div class='col-md-12 finalpaymentcart' style="padding: 27px;" id="applycoupondiv">
                    <div class='col-md-9'>
                        <input type="text" name="validcoupon" id="validcoupon" placeholder="Enter valid coupon code"
                               style="height: 38px; width: 370px; padding-left: 19px;">
                        <input type="button" onclick="applyCoupon()" name="applycoupon" id="applycoupon"
                               value="Apply Coupon" style="margin-bottom: 24px; margin-top: 0px; text-transform: uppercase;

                     margin-right: 15px; padding: 8px 25px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; border: 1px solid rgb(212, 80, 79); color: white;">
                    </div>
                    <div class='col-md-1'></div>
                    <div class='col-md-2'></div>
                </div>
                <div class='col-md-12 finalpaymentcart' id="finalpaymentcart">
                    <div class='col-md-9' style="text-align: right">
                        <label class="finalpaymentcartlabel1">Cart Subtotal (Without TAX) : </label></br></br>
                        <p class="coupon_amount_p coupon_amount">Coupon Discounted amount : </p>
                        <p class="coupon_amount_p coupon_amount">Cart Subtotal (With Discount Coupon) : </p>
                        <div class="clear"></div>
                        <label class="finalpaymentcartlabel2"><i class="fa fa-barcode"></i> Tax % =  <?php
                        $query = "select * from sales_tax";
                        $result = mysql_query($query);
                            $rows = mysql_fetch_array($result);
                            echo $rows['sales_amount'];
                        ?>% : </label>
                    </div>
                    <div class='col-md-1'></div>
                    <div class='col-md-2'>
                        <input class="finalpaymentcartlabel1" type="text" id="finalcartsubtotal"
                               style="border:none;text-align:left" readonly/><br><br>
                        <div class="clear"></div>
                        <input class="finalpaymentcartlabel1 coupon_amount" type="text" id="coupon_applied_amount"
                               style="border:none;text-align:left" readonly/>
                        <input class="finalpaymentcartlabel1 coupon_amount" type="text" id="cart_coupon_dis_amount"
                               style="border:none;text-align:left" readonly/>
                        <input type="text" id="finalcartsubtotal_actual"
                               style="display: none" readonly/>
                        <input class="finalpaymentcartlabel2" style="border:none;text-align:left" readonly/>
                    </div>
                </div>
                <div class='col-md-12 totalpaymentcart' id="totalpaymentcart">
                    <div class='col-md-9'>
                        <label class="totalpaymentcartlabel1">Order Total (With TAX) : </label>
                    </div>
                    <div class='col-md-1'></div>
                    <div class='col-md-2'>
                        <input class=" totalpaymentcartlabel1 " type="text" id="totalcartsubtotal"
                               style="border:none;text-align:left" readonly/><br><br>
                    </div>
                </div>
                <div class='col-md-12' id='stp1_buttons' style="padding:0">
                    <input type="button" class="custombottomcartbtn" onclick="continue_shoping()"
                           style="float: left;background: white;color: black; border: 1px solid #ccc"
                           value="CONTINUE SHOPPING"/>
                    <input type="button" class="custombottomcartbtn" onclick="step_2_task()" id="placeorder"
                           style="float:right"
                           value="Shipping & Billing"/>
                </div>
                <!------------------------------step 2functionality start here------------------------------------------------>
                <div class="col-md-12" id="step2_div" style="display: none">
                    <h2 class="addressdataheading">Ship to this address</h2>
                    <div class="box" id="box" style="display: none "></div>
                    <hr>
                    <div class="col-md-12" style="padding:20px">
                        <div class="col-md-8" style="line-height:45px;border-right:1px solid #ccc">
                            <div class="col-md-6">
                                <label class="otherradio_label">First name(*)</label>
                                <input class="form-control" id="first_name" placeholder="first name" type="text">
                            </div>
                            <div class="col-md-6">
                                <label class="otherradio_label">Last name(*)</label>
                                <input class="form-control" id="last_name" placeholder="last name" type="text">
                            </div>
                            <div class="col-md-12">
                                <label class="otherradio_label">Address line 1:(*)</label>
                                <textarea class="form-control" id="address"
                                          placeholder="Enter Billing / Shipping Address"></textarea>
                            </div>
                            <div class="col-md-4">
                                <label class="otherradio_label">Country(*)</label>
                                <div id="">
                                    <select class="form-control countries" name="country" id="countryId"></select>
                                </div>
                            </div>
                            <div class="col-md-4 ">
                                <label class="otherradio_label">State(*)</label>
                                <div id="">
                                    <select class="form-control states" name="state" id="stateId"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="otherradio_label">City(*)</label>
                                <div id="cityfeild">
                                    <select class="form-control cities" name="city" id="cityId"></select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="otherradio_label">Postal code(*)</label>
                                <input class="form-control" id="postalcode" placeholder="postal code" type="text">
                            </div>
                            <div class="col-md-4">
                                <label class="otherradio_label">Phone number(*)</label>
                                <input class="form-control" id="phonenumber" placeholder="contact number" type="text">
                            </div>
                        </div>
                        <div class="col-md-4" style="text-align:right">
                            <p class="payment_confirmation">Payment Confirmation</p>
                            <p class="total_am">Total Amount (Without Tax) = $<span id="total_am"></span></p>
                            <p class="tax">Tax : 7.25% = <span id="tax"></span></p>
                            <p class="grand_total">Grand Total (With Tax) = $<span id="grand_total"></span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <!--<input id="make_payment" value="Make Payment" onclick="update_shipping_address()"
                           class="custommpbtn pull-right" type="button">-->
                    <input id="make_payment" value="Make Payment" class="custommpbtn pull-right" type="button" onclick="selectPaymentOption()">
                </div>
                <!------------------------------payment confirmation div end here------------------------------------------------------------------>
            </div>
        </div>
        <input type="hidden" id="selectedMeasurement"/>
    </div>
    <div class="clearfix"></div>
</div>

<?php include("footer.php");?>
<script>

    var user_id = $("#user_id").val();
    function selectPaymentOption(){
        $(".modal-header").show();
        $(".modal-title").show();
        $(".modal-body").show();
        $(".modal-footer").show();
        $(".modal-title").html("<label>Select Payment Option</label>");
        $(".modal-body").html(' <p style="color:red" class="errorMessage"></p><div class="container-fluid"><div class="col-md-10"><div class="col-md-5 ">' +
            '<label class="pull-right">Select Payment Option</label></div><div class="col-md-7"><select class="form-control" ' +
            'id="paymentOption"><option value="">Select Payment Option</option><option value="paypal">PAYPAL</option>' +
            '<option value="square">SQUARE</option></select></div></div></div>' +
            '');
        $(".modal-footer").html("<input type='button' value='Continue' class='btn btn-dafault' onclick='paymentOption()'>");
        $("#myModal").modal("show");
    }
    function paymentOption(){
        var paymentOption = $("#paymentOption").val();
        if(paymentOption == ""){
            $(".errorMessage").html("please select payment option");
        }
        else if(paymentOption == "paypal"){
            update_shipping_address("paypal");
        }
        else if(paymentOption == "square"){
            update_shipping_address("square");
        }
    }


    /*select free item start here*/
    function selectFreeItem(free_item, cartId, rowType) {
        free_item = decodeURI(free_item);
        switch (free_item) {
            case "Custom Shirt":
                window.location = "custom_shirts.php?freebies=Custom_Shirt&pid=" + cartId + "&rowType=" + rowType;
        }
        switch (free_item) {
            case "Custom Pant":
                window.location = "custom_pants.php?freebies=Custom_Pant&pid=" + cartId + "&rowType=" + rowType;
        }
    }

    function viewProductDetail(viewData, freebies_item) {
        viewData = unescape(viewData);
        viewData = JSON.parse(viewData);
        var productViewData = "";
        for (var i in viewData) {
            var value = viewData[i];
            if (value == "") {
                value = "N/A"
            }
            productViewData = productViewData + "<div class='col-md-6'><label style='text-transform: capitalize'>" +
                i.split("_")[1] + "</label><p style='width:100%;font-size:13px;word-wrap:break-word'>" + value + "</p></div>";

        }
        freebies_item = decodeURI(freebies_item);
        $(".modal-title").html("<label style='color:green'>Selected " + freebies_item + "</label>");
        $(".modal-body").html("<div class='row'>" + productViewData + "</div>");
        $(".modal-footer").css("display", "block");
        $(".modal-footer").html("<input type='button' class='btn btn-warning' data-dismiss='modal' value='Cancel' />");
        $("#myModal").modal("show");
    }

    /*select free item end  here*/
    var selectedMeasurement = "";

    /*is apply coupon and fetch cart data start here*/
    var url = "../admin/webserver/apply_coupon.php?type=isCouponApplied&user_id=" + user_id;
    $.get(url, function (data) {
        var json = $.parseJSON(data);
        var status = json.status;
        var coupon_condition = "";
        if (status == "done") {
            coupon_condition = json.coupon_condition;
            var coupon_applyd_on = json.coupon_applyd_on;
            var coupon_code = json.coupon_code;
            if (coupon_condition == "discount") {
                var message = json.message;
                var discount_amount = json.discount_amount;
            }
            if (coupon_condition == "freebiese") {
                alert("Coupon discount = " + coupon_condition);
            }
        }

        /* $(".loader").fadeOut();*/
        /*fetch cart data start here*/
        url = "../admin/webserver/getcart.php?user_id=" + user_id;
        stock = 10;
        $(".nav a").removeClass("active");
        $.get(url, function (data) {
            var data = $.parseJSON(data);
            var status = data.status;
            var button = "";
            var cartshow = "";
            var prod_color = "";
            if (status == "done") {
                $("#clear_cart").css("display", "block");
                var items = data.items;
                var totalamount = 0;
                if (items.length > 0) {
                    for (var i = 0; i < items.length; i++) {
                        totalamount = parseFloat(totalamount) + parseFloat(items[i].cart_product_total_amount) + parseFloat(items[i].cart_extra_price);
                        var cart_id = items[i].cart_id;
                        var prod_name = items[i].product_name;
                        var product_type = items[i].product_type;
                        var prod_image = items[i].product_image;
                        var coupon_applied = items[i].coupon_applied;
                        var freebies_item = items[i].freebies_item;

                        /*for view data of freebies data start here*/
                        var j_category = items[i].j_category;
                        var j_frontbutton = items[i].j_frontbutton;
                        var j_lapelstyle = items[i].j_lapelstyle;
                        var j_lapelbuttonholes = items[i].j_lapelbuttonholes;
                        var j_lapelbuttonholesstyle = items[i].j_lapelbuttonholesstyle;
                        var j_lapelbuttonholesthread = items[i].j_lapelbuttonholesthread;
                        var j_lapelingredient = items[i].j_lapelingredient;
                        var j_lapelsatin = items[i].j_lapelsatin;
                        var j_chestdart = items[i].j_chestdart;
                        var j_feltcollar = items[i].j_feltcollar;
                        var j_innerflap = items[i].j_innerflap;
                        var j_facingstyle = items[i].j_facingstyle;
                        var j_sleeveslit = items[i].j_sleeveslit;
                        var j_sleeveslitthread = items[i].j_sleeveslitthread;
                        var j_breastpocket = items[i].j_breastpocket;
                        var j_lowerpocket = items[i].j_lowerpocket;
                        var j_coinpocket = items[i].j_coinpocket;
                        var j_backvent = items[i].j_backvent;
                        var j_liningstyle = items[i].j_liningstyle;
                        var j_liningoption = items[i].j_liningoption;
                        var j_lining = items[i].j_lining;
                        var j_shoulderstyle = items[i].j_shoulderstyle;
                        var j_shoulderpadding = items[i].j_shoulderpadding;
                        var j_buttonoption = items[i].j_buttonoption;
                        var j_buttonswatch = items[i].j_buttonswatch;
                        var j_threadoption = items[i].j_threadoption;
                        var j_elbowpatch = items[i].j_elbowpatch;
                        var j_elbowpatchcolor = items[i].j_elbowpatchcolor;
                        var j_canvasoption = items[i].j_canvasoption;
                        var j_suitcolor = items[i].j_suitcolor;
                        var j_shirt = items[i].j_shirt;
                        var j_tie = items[i].j_tie;
                        var j_jacket = items[i].j_jacket;
                        var j_armbutton = items[i].j_armbutton;
                        var j_sidearmbutton = items[i].j_sidearmbutton;
                        var j_sidearmbuttoncut = items[i].j_sidearmbuttoncut;
                        var j_sidearmbuttonpoint = items[i].j_sidearmbuttonpoint;
                        var p_category = items[i].p_category;
                        var p_frontpocket = items[i].p_frontpocket;
                        var p_pleatstyle = items[i].p_pleatstyle;
                        var p_watchpocket = items[i].p_watchpocket;
                        var p_backpocket = items[i].p_backpocket;
                        var p_belt = items[i].p_belt;
                        var p_pleat = items[i].p_pleat;
                        var p_beltloop = items[i].p_beltloop;
                        var p_pantbottom = items[i].p_pantbottom;
                        var p_pantcolor = items[i].p_pantcolor;
                        var o_category = items[i].o_category;
                        var o_canvasstyle = items[i].o_canvasstyle;
                        var o_lapelstyle = items[i].o_lapelstyle;
                        var o_pocketstype = items[i].o_pocketstype;
                        var o_buttonstyle = items[i].o_buttonstyle;
                        var o_sweatpadstyle = items[i].o_sweatpadstyle;
                        var o_elbowpadstyle = items[i].o_elbowpadstyle;
                        var o_shoulderstyle = items[i].o_shoulderstyle;
                        var o_shoulderpatchstyle = items[i].o_shoulderpatchstyle;
                        var o_shouldertabstyle = items[i].o_shouldertabstyle;
                        var o_sleeveslitstyle = items[i].o_sleeveslitstyle;
                        var o_sleevebuttonstyle = items[i].o_sleevebuttonstyle;
                        var o_backvent = items[i].o_backvent;
                        var o_stitchstyle = items[i].o_stitchstyle;
                        var o_backsideofarmhole = items[i].o_backsideofarmhole;
                        var o_topstichstyle = items[i].o_topstichstyle;
                        var o_breastpocket = items[i].o_breastpocket;
                        var o_coinpocket = items[i].o_coinpocket;
                        var o_facingstyle = items[i].o_facingstyle;
                        var o_pocketstyle = items[i].o_pocketstyle;
                        var o_collarstyle = items[i].o_collarstyle;
                        var o_liningstyle = items[i].o_liningstyle;
                        var o_liningoption = items[i].o_liningoption;
                        var o_threadcolor = items[i].o_threadcolor;
                        var o_overcoatfabric = items[i].o_overcoatfabric;
                        var s_category = items[i].s_category;
                        var s_collar = items[i].s_collar;
                        var s_collarbutton = items[i].s_collarbutton;
                        var s_collarbuttondown = items[i].s_collarbuttondown;
                        var s_collarlayeroption = items[i].s_collarlayeroption;
                        var s_frontcollar = items[i].s_frontcollar;
                        var s_collarbelt = items[i].s_collarbelt;
                        var s_cuff = items[i].s_cuff;
                        var s_cuffwidth = items[i].s_cuffwidth;
                        var s_placket = items[i].s_placket;
                        var s_placketbutton = items[i].s_placketbutton;
                        var s_pocket = items[i].s_pocket;
                        var s_pleat = items[i].s_pleat;
                        var s_bottom = items[i].s_bottom;
                        var s_shirtcolor = items[i].s_shirtcolor;
                        var s_buttoncolor = items[i].s_buttoncolor;
                        var s_buttoningstyle = items[i].s_buttoningstyle;
                        var s_buttoncutthread = items[i].s_buttoncutthread;
                        var s_holestichthread = items[i].s_holestichthread;
                        var s_monogramtext = items[i].s_monogramtext;
                        var s_monogramfont = items[i].s_monogramfont;
                        var s_monogramcolor = items[i].s_monogramcolor;
                        var s_monogramposition = items[i].s_monogramposition;
                        var s_monogramoption = items[i].s_monogramoption;
                        var s_monogramcode = items[i].s_monogramcode;
                        var s_contrastfabric = items[i].s_contrastfabric;
                        var s_contrast = items[i].s_contrast;
                        var s_contrastposition = items[i].s_contrastposition;
                        var s_sleeve = items[i].s_sleeve;
                        var s_shoulder = items[i].s_shoulder;
                        var a_category = items[i].a_category;
                        var a_specification = items[i].a_specification;
                        var a_rc_no = items[i].a_rc_no;
                        var v_category = items[i].v_category;
                        var v_collarstyle = items[i].v_collarstyle;
                        var v_lapelbuttonhole = items[i].v_lapelbuttonhole;
                        var v_buttonholestyle = items[i].v_buttonholestyle;
                        var v_buttonstyle = items[i].v_buttonstyle;
                        var v_bottomstyle = items[i].v_bottomstyle;
                        var v_lowerpocket = items[i].v_lowerpocket;
                        var v_ticketpocket = items[i].v_ticketpocket;
                        var v_backstyle = items[i].v_backstyle;
                        var v_chestdart = items[i].v_chestdart;
                        var v_topstitch = items[i].v_topstitch;
                        var v_placketstyle = items[i].v_placketstyle;
                        var v_tuxedostyle = items[i].v_tuxedostyle;
                        var v_breastpocket = items[i].v_breastpocket;
                        var v_chestpocket = items[i].v_chestpocket;
                        var v_vestfabric = items[i].v_vestfabric;
                        /*for view data of freebies data end   here*/
                        var jacketCartData = {
                            "j_category": j_category,
                            "j_frontbutton": j_frontbutton,
                            "j_lapelstyle": j_lapelstyle,
                            "j_lapelbuttonholes": j_lapelbuttonholes,
                            "j_lapelbuttonholesstyle": j_lapelbuttonholesstyle,
                            "j_lapelbuttonholesthread": j_lapelbuttonholesthread,
                            "j_lapelingredient": j_lapelingredient,
                            "j_lapelsatin": j_lapelsatin,
                            "j_chestdart": j_chestdart,
                            "j_feltcollar": j_feltcollar,
                            "j_innerflap": j_innerflap,
                            "j_facingstyle": j_facingstyle,
                            "j_sleeveslit": j_sleeveslit,
                            "j_sleeveslitthread": j_sleeveslitthread,
                            "j_breastpocket": j_breastpocket,
                            "j_lowerpocket": j_lowerpocket,
                            "j_coinpocket": j_coinpocket,
                            "j_backvent": j_backvent,
                            "j_liningstyle": j_liningstyle,
                            "j_liningoption": j_liningoption,
                            "j_lining": j_lining,
                            "j_shoulderstyle": j_shoulderstyle,
                            "j_shoulderpadding": j_shoulderpadding,
                            "j_buttonoption": j_buttonoption,
                            "j_buttonswatch": j_buttonswatch,
                            "j_threadoption": j_threadoption,
                            "j_elbowpatch": j_elbowpatch,
                            "j_elbowpatchcolor": j_elbowpatchcolor,
                            "j_canvasoption": j_canvasoption,
                            "j_suitcolor": j_suitcolor,
                            "j_shirt": j_shirt,
                            "j_tie": j_tie,
                            "j_jacket": j_jacket,
                            "j_armbutton": j_armbutton,
                            "j_sidearmbutton": j_sidearmbutton,
                            "j_sidearmbuttoncut": j_sidearmbuttoncut,
                            "j_sidearmbuttonpoint": j_sidearmbuttonpoint
                        };

                        var pantCartData = {
                            "p_category": p_category,
                            "p_frontpocket": p_frontpocket,
                            "p_pleatstyle": p_pleatstyle,
                            "p_watchpocket": p_watchpocket,
                            "p_backpocket": p_backpocket,
                            "p_belt": p_belt,
                            "p_pleat": p_pleat,
                            "p_beltloop": p_beltloop,
                            "p_pantbottom": p_pantbottom,
                            "p_pantcolor": p_pantcolor
                        };
                        var overCoatData = {
                            "o_category": o_category,
                            "o_canvasstyle": o_canvasstyle,
                            "o_lapelstyle": o_lapelstyle,
                            "o_pocketstype": o_pocketstype,
                            "o_buttonstyle": o_buttonstyle,
                            "o_sweatpadstyle": o_sweatpadstyle,
                            "o_elbowpadstyle": o_elbowpadstyle,
                            "o_shoulderstyle": o_shoulderstyle,
                            "o_shoulderpatchstyle": o_shoulderpatchstyle,
                            "o_shouldertabstyle": o_shouldertabstyle,
                            "o_sleeveslitstyle": o_sleeveslitstyle,
                            "o_sleevebuttonstyle": o_sleevebuttonstyle,
                            "o_backvent": o_backvent,
                            "o_stitchstyle": o_stitchstyle,
                            "o_backsideofarmhole": o_backsideofarmhole,
                            "o_topstichstyle": o_topstichstyle,
                            "o_breastpocket": o_breastpocket,
                            "o_coinpocket": o_coinpocket,
                            "o_facingstyle": o_facingstyle,
                            "o_pocketstyle": o_pocketstyle,
                            "o_collarstyle": o_collarstyle,
                            "o_liningstyle": o_liningstyle,
                            "o_liningoption": o_liningoption,
                            "o_threadcolor": o_threadcolor,
                            "o_overcoatfabric": o_overcoatfabric
                        };

                        var shirtCartData = {
                            "s_category": s_category,
                            "s_collar": s_collar,
                            "s_collarbutton": s_collarbutton,
                            "s_collarbuttondown": s_collarbuttondown,
                            "s_collarlayeroption": s_collarlayeroption,
                            "s_frontcollar": s_frontcollar,
                            "s_collarbelt": s_collarbelt,
                            "s_cuff": s_cuff,
                            "s_cuffwidth": s_cuffwidth,
                            "s_placket": s_placket,
                            "s_placketbutton": s_placketbutton,
                            "s_pocket": s_pocket,
                            "s_pleat": s_pleat,
                            "s_bottom": s_bottom,
                            "s_shirtcolor": s_shirtcolor,
                            "s_buttoncolor": s_buttoncolor,
                            "s_buttoningstyle": s_buttoningstyle,
                            "s_buttoncutthread": s_buttoncutthread,
                            "s_holestichthread": s_holestichthread,
                            "s_monogramtext": s_monogramtext,
                            "s_monogramfont": s_monogramfont,
                            "s_monogramcolor": s_monogramcolor,
                            "s_monogramposition": s_monogramposition,
                            "s_monogramoption": s_monogramoption,
                            "s_monogramcode": s_monogramcode,
                            "s_contrastfabric": s_contrastfabric,
                            "s_contrast": s_contrast,
                            "s_contrastposition": s_contrastposition,
                            "s_sleeve": s_sleeve,
                            "s_shoulder": s_shoulder
                        };

                        var accessoriesCartData = {
                            "a_category": a_category,
                            "a_specification": a_specification,
                            "a_rc_no": a_rc_no
                        };
                        var vestCartData = {
                            "v_category": v_category,
                            "v_collarstyle": v_collarstyle,
                            "v_lapelbuttonhole": v_lapelbuttonhole,
                            "v_buttonholestyle": v_buttonholestyle,
                            "v_buttonstyle": v_buttonstyle,
                            "v_bottomstyle": v_bottomstyle,
                            "v_lowerpocket": v_lowerpocket,
                            "v_ticketpocket": v_ticketpocket,
                            "v_backstyle": v_backstyle,
                            "v_chestdart": v_chestdart,
                            "v_topstitch": v_topstitch,
                            "v_placketstyle": v_placketstyle,
                            "v_tuxedostyle": v_tuxedostyle,
                            "v_breastpocket": v_breastpocket,
                            "v_chestpocket": v_chestpocket,
                            "v_vestfabric": v_vestfabric
                        };

                        /*freebies item box start here*/
                        var freebies_box = "";
                        var rowType = "update";
                        if (coupon_applied == "Yes/NoSelected") {
                            freebies_box = "<div class='freebies_box' id='freebies_box_" + i + "' >" +
                                "<p>Free " + freebies_item + "</p><input type='button' class='btn btn-primary' value='Select Freebies'" +
                                "onclick=selectFreeItem('" + encodeURI(freebies_item) + "','" + cart_id + "','new')></div>";
                        }
                        if (coupon_applied == "Yes/Selected") {
                            freebies_box = "<div class='freebies_box' id='freebies_box_" + i + "' >" +
                                "<p>Free " + freebies_item + "</p><input type='button' class='btn btn-primary' value='Change Freebies'" +
                                "onclick=selectFreeItem('" + encodeURI(freebies_item) + "','" + cart_id + "','" + rowType + "')></div>";
                        }
                        /*freebies item box end   here*/

                        var meas_name = "Select Measurement";
                        if (items[i].meas_name != "") {
                            meas_name = items[i].meas_name;
                        }
                        if (product_type === "Custom Suit") {
                            prod_color = items[i].j_suitcolor;
                            console.log('Custom Suit color ---' + prod_color);
                            if (prod_image)
                                prod_image = prod_image.split(",");
                            if (coupon_applied == "Yes/Selected") {
                                var freebiesData;
                                switch (freebies_item) {
                                    case "Custom Shirt":
                                        freebiesData = shirtCartData;
                                        freebiesData = JSON.stringify(freebiesData);
                                        break;
                                }
                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-8 productinfo'>" +
                                    "<div class='col-md-2'><img alt='Custom Fabric' src='" + prod_image[0] + "' class='img' style='z-index:0' />" +
                                    "<img alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' /><img  alt='' src='" + prod_image[2] +
                                    "' class='img' style='z-index:3' /><img alt=''  src='" + prod_image[3] + "' class='img' style='z-index:3' />" +
                                    "<img  alt='' src='" + prod_image[4] + "' class='img' style='z-index:4' /><img alt=''  src='" + prod_image[5] +
                                    "' class='img' style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +
                                    "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[8] +
                                    "' class='img' style='z-index:2' /></div><div class='col-md-10'><table style='font-family: " +
                                    "trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>Product Name </th><td>" +
                                    prod_name + "</td></tr> <tr><th>Selected Buttons </th><td>" + items[i].j_frontbutton + "</td></tr>" +
                                    "<tr><th>Selected Vents</th><td>" + items[i].j_backvent + "</td></tr><tr><th>Breast Pocket</th><td>" +
                                    items[i].j_breastpocket + "</td></tr><tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td>" +
                                    "</tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption + "</td></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                    "</div><div class='col-md-2'><img src='images/freebie.png' class='' style='width:100px;margin: 20px 10px 0'" +
                                    "onclick=viewProductDetail('" + escape(freebiesData) + "','" + encodeURI(freebies_item) + "')>" +
                                    freebies_box + "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +
                                    "readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                    "','Jacket','" + escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i +
                                    "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch +"') ></i><i class='fa fa-plus-circle increment_quantity' " +
                                    "onclick=incrementValue('" + items[i].user_id + "','Jacket','" + escape(items[i].j_canvasoption) +
                                    "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch +"')></i>" +
                                    "</div><input type='text'" +
                                    " id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  /><p " +
                                    "onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i></p>" +
                                    " <div class='clear'></div><input class='form-control total_ip' type='text' readonly " +
                                    "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text' " +
                                    "hidden id='stock_value' value='" + stock + "' /><input type='button' value='" + meas_name + "' " +
                                    "class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;width:100%' " +
                                    "onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";

                            }
                            else {
                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +
                                    "<div class='col-md-2'><img alt='Custom Fabric' src='" + prod_image[0] + "' class='img' style='z-index:0' />" +
                                    "<img alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' /><img  alt='' src='" + prod_image[2] +
                                    "' class='img' style='z-index:3' /><img alt=''  src='" + prod_image[3] + "' class='img' style='z-index:3' />" +
                                    "<img  alt='' src='" + prod_image[4] + "' class='img' style='z-index:4' /><img alt=''  src='" + prod_image[5] +
                                    "' class='img' style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +
                                    "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[8] +
                                    "' class='img' style='z-index:2' /></div><div class='col-md-10'><table style='font-family: " +
                                    "trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>Product Name </th><td>" +
                                    prod_name + "</td></tr> <tr><th>Selected Buttons </th><td>" + items[i].j_frontbutton + "</td></tr>" +
                                    "<tr><th>Selected Vents</th><td>" + items[i].j_backvent + "</td></tr><tr><th>Breast Pocket</th><td>" +
                                    items[i].j_breastpocket + "</td></tr><tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td>" +
                                    "</tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption + "</td></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +
                                    "readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                    "','Jacket','" + escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i +
                                    "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch+"') ></i><i class='fa fa-plus-circle increment_quantity' " +
                                    "onclick=incrementValue('" + items[i].user_id + "','Jacket','" + escape(items[i].j_canvasoption) +
                                    "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch+"')></i></div><input type='text'" +
                                    " id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  /><p " +
                                    "onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i></p>" +
                                    " <div class='clear'></div><input class='form-control total_ip' type='text' readonly " +
                                    "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text' " +
                                    "hidden id='stock_value' value='" + stock + "' />" + freebies_box + "<input type='button' value='" + meas_name + "' " +
                                    "class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;width:100%' " +
                                    "onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            }
                        }
                        else if (product_type == "Custom 2Pc Suit") {
                            prod_color = items[i].j_suitcolor;
                            console.log('Custom 2Pc suit color ---' + prod_color);
                            if (prod_image)
                            {
                                prod_image = prod_image.split(",");
                            }
                            if (coupon_applied == "Yes/Selected") {
                                if (freebies_item == "Custom Pant" || freebies_item == "Custom Suit") {
                                    rowType = "new";
                                }

                                if (coupon_applied == "Yes/NoSelected") {
                                    freebies_box = "<div class='freebies_box' id='freebies_box_" + i + "' >" +
                                        "<p>Free " + freebies_item + "</p><input type='button' class='btn btn-primary' value='Select Freebies'" +
                                        "onclick=selectFreeItem('" + encodeURI(freebies_item) + "','" + cart_id + "','new')></div>";
                                }
                                if (coupon_applied == "Yes/Selected") {
                                    freebies_box = "<div class='freebies_box' id='freebies_box_" + i + "' >" +
                                        "<p>Free " + freebies_item + "</p><input type='button' class='btn btn-primary' value='Change Freebies'" +
                                        "onclick=selectFreeItem('" + encodeURI(freebies_item) + "','" + items[i].freebies_dependency + "','update')></div>";
                                }
                                var freebiesData;
                                switch (freebies_item) {
                                    case "Custom Pant":
                                        var parentId = items[i].freebies_dependency ;
                                        for(var j =0;j<items.length; j++){
                                            if(items[j].product_type == "Custom Pant")
                                            {
                                                if(parentId == items[j].cart_id){
                                                    var p_category  =   items[j].p_category;
                                                    var p_frontpocket   =   items[j].p_frontpocket;
                                                    var p_pleatstyle    =   items[j].p_pleatstyle;
                                                    var p_watchpocket   =   items[j].p_watchpocket;
                                                    var p_backpocket    =   items[j].p_backpocket;
                                                    var p_belt  =   items[j].p_belt;
                                                    var p_pleat =   items[j].p_pleat;
                                                    var p_beltloop  =   items[j].p_beltloop;
                                                    var p_pantbottom    =   items[j].p_pantbottom;
                                                    var p_pantcolor =   items[j].p_pantcolor;

                                                    var pantCartData = {"p_category":p_category,"p_frontpocket":p_frontpocket, "p_pleatstyle":p_pleatstyle,
                                                        "p_watchpocket":p_watchpocket, "p_backpocket":p_backpocket, "p_belt":p_belt, "p_pleat":p_pleat,
                                                        "p_beltloop":p_beltloop, "p_pantbottom":p_pantbottom, "p_pantcolor":p_pantcolor};
                                                    freebiesData = JSON.stringify(pantCartData);
                                                }
                                            }
                                        }
                                        break;
                                    case "Custom Suit":
                                        var parentId = items[i].freebies_dependency ;
                                        for(var j =0;j<items.length; j++){
                                            if(items[j].product_type == "Custom Suit")
                                            {
                                                if(parentId == items[j].cart_id){
                                                    var j_category = items[j].j_category;
                                                    var j_frontbutton = items[j].j_frontbutton;
                                                    var j_lapelstyle = items[j].j_lapelstyle;
                                                    var j_lapelbuttonholes = items[j].j_lapelbuttonholes;
                                                    var j_lapelbuttonholesstyle = items[j].j_lapelbuttonholesstyle;
                                                    var j_lapelbuttonholesthread = items[j].j_lapelbuttonholesthread;
                                                    var j_lapelingredient = items[j].j_lapelingredient;
                                                    var j_lapelsatin = items[j].j_lapelsatin;
                                                    var j_chestdart = items[j].j_chestdart;
                                                    var j_feltcollar = items[j].j_feltcollar;
                                                    var j_innerflap = items[j].j_innerflap;
                                                    var j_facingstyle = items[j].j_facingstyle;
                                                    var j_sleeveslit = items[j].j_sleeveslit;
                                                    var j_sleeveslitthread = items[j].j_sleeveslitthread;
                                                    var j_breastpocket = items[j].j_breastpocket;
                                                    var j_lowerpocket = items[j].j_lowerpocket;
                                                    var j_coinpocket = items[j].j_coinpocket;
                                                    var j_backvent = items[j].j_backvent;
                                                    var j_liningstyle = items[j].j_liningstyle;
                                                    var j_liningoption = items[j].j_liningoption;
                                                    var j_lining = items[j].j_lining;
                                                    var j_shoulderstyle = items[j].j_shoulderstyle;
                                                    var j_shoulderpadding = items[j].j_shoulderpadding;
                                                    var j_buttonoption = items[j].j_buttonoption;
                                                    var j_buttonswatch = items[j].j_buttonswatch;
                                                    var j_threadoption = items[j].j_threadoption;
                                                    var j_elbowpatch = items[j].j_elbowpatch;
                                                    var j_elbowpatchcolor = items[j].j_elbowpatchcolor;
                                                    var j_canvasoption = items[j].j_canvasoption;
                                                    var j_suitcolor = items[j].j_suitcolor;
                                                    var j_shirt = items[j].j_shirt;
                                                    var j_tie = items[j].j_tie;
                                                    var j_jacket = items[j].j_jacket;
                                                    var j_armbutton = items[j].j_armbutton;
                                                    var j_sidearmbutton = items[j].j_sidearmbutton;
                                                    var j_sidearmbuttoncut = items[j].j_sidearmbuttoncut;
                                                    var j_sidearmbuttonpoint = items[j].j_sidearmbuttonpoint;
                                                    var jacketCartData = {
                                                        "j_category": j_category,
                                                        "j_frontbutton": j_frontbutton,
                                                        "j_lapelstyle": j_lapelstyle,
                                                        "j_lapelbuttonholes": j_lapelbuttonholes,
                                                        "j_lapelbuttonholesstyle": j_lapelbuttonholesstyle,
                                                        "j_lapelbuttonholesthread": j_lapelbuttonholesthread,
                                                        "j_lapelingredient": j_lapelingredient,
                                                        "j_lapelsatin": j_lapelsatin,
                                                        "j_chestdart": j_chestdart,
                                                        "j_feltcollar": j_feltcollar,
                                                        "j_innerflap": j_innerflap,
                                                        "j_facingstyle": j_facingstyle,
                                                        "j_sleeveslit": j_sleeveslit,
                                                        "j_sleeveslitthread": j_sleeveslitthread,
                                                        "j_breastpocket": j_breastpocket,
                                                        "j_lowerpocket": j_lowerpocket,
                                                        "j_coinpocket": j_coinpocket,
                                                        "j_backvent": j_backvent,
                                                        "j_liningstyle": j_liningstyle,
                                                        "j_liningoption": j_liningoption,
                                                        "j_lining": j_lining,
                                                        "j_shoulderstyle": j_shoulderstyle,
                                                        "j_shoulderpadding": j_shoulderpadding,
                                                        "j_buttonoption": j_buttonoption,
                                                        "j_buttonswatch": j_buttonswatch,
                                                        "j_threadoption": j_threadoption,
                                                        "j_elbowpatch": j_elbowpatch,
                                                        "j_elbowpatchcolor": j_elbowpatchcolor,
                                                        "j_canvasoption": j_canvasoption,
                                                        "j_suitcolor": j_suitcolor,
                                                        "j_shirt": j_shirt,
                                                        "j_tie": j_tie,
                                                        "j_jacket": j_jacket,
                                                        "j_armbutton": j_armbutton,
                                                        "j_sidearmbutton": j_sidearmbutton,
                                                        "j_sidearmbuttoncut": j_sidearmbuttoncut,
                                                        "j_sidearmbuttonpoint": j_sidearmbuttonpoint
                                                    };
                                                    freebiesData = JSON.stringify(jacketCartData);
                                                }
                                            }
                                        }
                                        break;
                                }
                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +
                                    "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' " +
                                    "style='z-index:0' /><img  alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' />" +
                                    "<img alt=''  src='" + prod_image[2] + "' class='img' style='z-index:3' /><img  alt='' src='" +
                                    prod_image[3] + "' class='img' style='z-index:3' /><img  alt='' src='" + prod_image[4] +
                                    "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[5] + "' class='img' " +
                                    "style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +
                                    "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" +
                                    prod_image[8] + "' class='img' style='z-index:2' /></div><div class='col-md-8'>" +
                                    "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr>" +
                                    "<th class=''>Suit Name </th><td>" + prod_name + "</td><th>Suit Type</th><td>Custom 2Pc Suit</td>" +
                                    "</tr><tr><th>Suit Buttons </th><td>" + items[i].j_frontbutton + "</td><th>Pant Category</th><td>" +
                                    items[i].p_category + "</td></tr><tr><th>Suit Vents</th><td>" + items[i].j_backvent + "</td><th>" +
                                    "Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Breast Pocket</th><td>" +
                                    items[i].j_breastpocket + "</td><th>Back Pocket</th><td>" + items[i].p_backpocket + "</td></tr>" +
                                    "<tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td><th>Belt Loop</th><td>" +
                                    items[i].p_beltloop + "</td></tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption +
                                    "</td><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></tr> <tr><th>Surplus Price</th>" +
                                    " <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr>" +
                                    "</table></div><div class='col-md-2'><img src='images/freebie.png' style='width:100px;margin: " +
                                    "20px 10px 0' onclick=viewProductDetail('"+escape(freebiesData)+"','"+encodeURI(freebies_item)+"') />" +
                                    "" + freebies_box + "</div>" +
                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +
                                    "readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                    "','2PcSuit','" + escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i + "','" +
                                    items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch+"')></i><i class='fa fa-plus-circle increment_quantity' " +
                                    "onclick=incrementValue('" + items[i].user_id + "','2PcSuit','" + escape(items[i].j_canvasoption) +
                                    "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch+"')></i>" +
                                    "</div><input type='text'" +
                                    "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  /><p " +
                                    "onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i>" +
                                    "</p><div class='clear'></div><input class='form-control total_ip' type='text' readonly " +
                                    "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text'" +
                                    "id='stock_value' value='" + stock + "' hidden  /><input type='button' value='" + meas_name +
                                    "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;width:100%'" +
                                    "onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            }
                            else {
                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +
                                    "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' " +
                                    "style='z-index:0' /><img  alt='' src='" + prod_image[1] + "' class='img' style='z-index:1' />" +
                                    "<img alt=''  src='" + prod_image[2] + "' class='img' style='z-index:3' /><img  alt='' src='" +
                                    prod_image[3] + "' class='img' style='z-index:3' /><img  alt='' src='" + prod_image[4] +
                                    "' class='img' style='z-index:4' /><img  alt='' src='" + prod_image[5] + "' class='img' " +
                                    "style='z-index:5' /><img  alt='' src='" + prod_image[6] + "' class='img' style='z-index:6' />" +
                                    "<img  alt='' src='" + prod_image[7] + "' class='img' style='z-index:4' /><img  alt='' src='" +
                                    prod_image[8] + "' class='img' style='z-index:2' /></div><div class='col-md-10'>" +
                                    "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr>" +
                                    "<th class=''>Suit Name </th><td>" + prod_name + "</td><th>Suit Type</th><td>Custom 2Pc Suit</td>" +
                                    "</tr><tr><th>Suit Buttons </th><td>" + items[i].j_frontbutton + "</td><th>Pant Category</th><td>" +
                                    items[i].p_category + "</td></tr><tr><th>Suit Vents</th><td>" + items[i].j_backvent + "</td><th>" +
                                    "Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Breast Pocket</th><td>" +
                                    items[i].j_breastpocket + "</td><th>Back Pocket</th><td>" + items[i].p_backpocket + "</td></tr>" +
                                    "<tr><th>Lower Pocket</th><td>" + items[i].j_lowerpocket + "</td><th>Belt Loop</th><td>" +
                                    items[i].p_beltloop + "</td></tr><tr><th>Suit Canvas</th><td>" + items[i].j_canvasoption +
                                    "</td><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></tr> <tr><th>Surplus Price</th> " +
                                    "<td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' " +
                                    "readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                    "','2PcSuit','" + escape(items[i].j_canvasoption) + "','quantity" + i + "','minus','" + i + "','" +
                                    items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch+"')></i><i class='fa fa-plus-circle increment_quantity' " +
                                    "onclick=incrementValue('" + items[i].user_id + "','2PcSuit','" + escape(items[i].j_canvasoption) +
                                    "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].j_buttonswatch+"')></i></div><input type='text'" +
                                    "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  /><p " +
                                    "onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i>" +
                                    "</p><div class='clear'></div><input class='form-control total_ip' type='text' readonly " +
                                    "value='" + items[i].cart_product_total_amount + "' id='total" + i + "' /><input type='text'" +
                                    "id='stock_value' value='" + stock + "' hidden  />" + freebies_box + "<input type='button' value='" + meas_name +
                                    "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;width:100%'" +
                                    "onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            }

                        }
                        else if (product_type == "Custom Shirt") {
                            stock = 5;
                            prod_color = items[i].s_contrastfabric;
                            console.log('custom shirt color ---' + prod_color);
                            if (prod_image)
                                prod_image = prod_image.split(",");
                            cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'" +
                                "><div class='col-md-2'><img  alt='Custom Fabric' style='margin-top:5px' src='" +
                                prod_image[0] + "' class='img' /><img  alt='' src='" + prod_image[1] + "' class='img' " +
                                "style='margin-top:5px'/><img src='" + prod_image[2] + "' class='img' " +
                                "style='z-index:2;margin-top:5px' /><img src='" + prod_image[3] + "' class='img' " +
                                "style='margin-top:5px' /><img  alt='' src='" + prod_image[4] + "' class='img' style='margin-top:5px' />" +
                                "<img  alt='' src='" + prod_image[5] + "' class='img' style='margin-top:5px' /><img alt=''  src='" +
                                prod_image[6] + "' class='img' style='margin-top:5px' /><img  alt='' src='" + prod_image[7] + "' " +
                                "class='img' style='margin-top:5px' /><img alt=''  src='" + prod_image[8] + "' class='img' " +
                                "style='margin-top:5px' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +
                                "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" + prod_name + "</td>" +
                                "</tr> <tr><th>Shirt Category</th><td>" + items[i].s_category + "</td></tr><tr><th>Shirt Color</th>" +
                                "<td>" + items[i].s_shirtcolor + "</td></tr><tr><th>Button Style</th><td>" + items[i].s_buttoningstyle + "</td>" +
                                "</tr></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div></div><div class='col-md-2 productpricing'><input type='text' " +
                                "class='form-control quantity_ip' readonly id='quantity" + i + "' value='" + items[i].quantity + "'/>" +
                                "<div class='quantity_inc'><i class='fa fa-minus-circle increment_quantity' " +
                                "onclick=incrementValue('" + items[i].user_id + "','Shirt','" + stock + "','quantity" + i + "','minus','" +
                                i + "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].s_buttoncolor +"')></i><i class='fa fa-plus-circle increment_quantity' " +
                                "onclick=incrementValue('" + items[i].user_id + "','Shirt','" + stock + "','quantity" + i +
                                "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "','"+items[i].s_buttoncolor +"') ></i></div><input type='text' " +
                                "id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden  />" +
                                "<p onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i></p>" +
                                "<div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +
                                items[i].cart_product_total_amount + "' id='total" + i + "' />" + freebies_box + " <input type='button' value='" +
                                meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;" +
                                "width:100%' onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                        }
                        else if (product_type == "Custom Pant") {
                            if (prod_image)
                                prod_image = prod_image.split(",");
                            prod_color = items[i].p_pantcolor;
                            console.log('custom pant color ---' + prod_color);
                            if(items[i].quantity === "0" || items[i].quantity === 0){

                            }
                            else {
                                cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +
                                    "<div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image[0] + "' class='img' />" +
                                    "<img alt=''  src='" + prod_image[1] + "' class='img'/><img  alt='' src='" + prod_image[2] +
                                    "' class='img' /><img alt=''  src='" + prod_image[3] + "' class='img'/><img alt=''  src='" +
                                    prod_image[4] + "' class='img' /></div><div class='col-md-10'>" +
                                    "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +
                                    "Product Name </th><td>" + prod_name + "</td></tr> <tr><th>Pant Category</th><td>" + items[i].p_category +
                                    "</td></tr><tr><th>Front Pocket</th><td>" + items[i].p_frontpocket + "</td></tr><tr><th>Back Pocket</th>" +
                                    "<td>" + items[i].p_backpocket + "</td></tr><tr><th>Belt Loop</th><td>" + items[i].p_beltloop + "</td>" +
                                    "</tr><tr><th>Pant Bottom</th><td>" + items[i].p_pantbottom + "</td></tr></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                    "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +
                                    " readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                    "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                    "','Pant','" + stock + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "','" + prod_color + "')" +
                                    "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                    "','Pant','" + stock + "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "')" +
                                    "></i></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden />" +
                                    "<p onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i>" +
                                    "</p><div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +
                                    items[i].cart_product_total_amount + "' id='total" + i + "' />" + freebies_box + "<input type='button' value='" +
                                    meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;" +
                                    "width:100%' onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                                $("#cart").html(button + cartshow);
                            }
                        }
                        else if (product_type === "Custom Vest") {
                            if (prod_image)
                                prod_image = prod_image.split(",");

                            prod_color = items[i].v_vestfabric;
                            console.log('vest color ---' + prod_color);
                            cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +
                                "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +
                                "' class='img' /><img alt='' style='z-index:6' src='" + prod_image[1] + "' class='img'/>" +
                                "<img style='z-index:6' alt='' src='" + prod_image[2] + "' class='img' /><img alt='' " +
                                "style='z-index:4' src='" + prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +
                                prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" + prod_image[5] + "' " +
                                "class='img'/><img alt='' style='z-index:1' src='" + prod_image[6] + "' class='img'/></div>" +
                                "<div class='col-md-10'>" +
                                "<table style='font-family:trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +
                                "Product Name </th><td>" + prod_name + "</td></tr> <tr><th>Vest Category</th><td>" + items[i].v_category +
                                "</td></tr><tr><th>Vest Button</th><td>" + items[i].v_buttonstyle + "</td></tr><tr><th>Vest Bottom</th>" +
                                "<td>" + items[i].v_bottomstyle + "</td></tr><tr><th>Collar Style</th><td>" + items[i].v_collarstyle + "</td>" +

                                "</tr><tr><th>Lower Pocket</th><td>" + items[i].v_lowerpocket + "</td></tr></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +
                                " readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                "','Vest','" + stock + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id + "','" + prod_color + "')" +
                                "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                "','Vest','" + stock + "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','" + prod_color + "')" +
                                "></i></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden />" +
                                "<p onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i>" +
                                "</p><div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +
                                items[i].cart_product_total_amount + "' id='total" + i + "' />" + freebies_box + "<input type='button' value='" +
                                meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px 0;" +
                                "width:100%' onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            $("#cart").html(button + cartshow);
                        }
                        else if (product_type == "Custom Overcoat") {
                            if (prod_image)
                                prod_image = prod_image.split(",");
                            prod_color = items[i].o_overcoatfabric;
                            console.log('overcoat color ---' + prod_color);
                            cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo'>" +
                                "<div class='col-md-2'><img style='z-index:0;' alt='Custom Fabric' src='" + prod_image[0] +
                                "' class='img' /><img alt='' style='z-index:1' src='" + prod_image[1] + "' class='img'/>" +
                                "<img style='z-index:2' alt='' src='" + prod_image[2] + "' class='img' /><img alt='' " +
                                "style='z-index:3' src='" + prod_image[3] + "' class='img'/><img alt='' style='z-index:3' src='" +
                                prod_image[4] + "' class='img' /><img alt='' style='z-index:5' src='" + prod_image[5] + "' " +
                                "class='img'/><img alt='' style='z-index:7' src='" + prod_image[6] + "' class='img'/></div><div class='col-md-10'>" +
                                "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:28px;'><tr><th class=''>" +
                                "Product Name </th><td>&nbsp;&nbsp;: " + prod_name +
                                "</td></tr> <tr><th>Product Type</th><td>&nbsp;&nbsp;: " + items[i].product_type +
                                "</td></tr><tr><th>Overcoat Category</th><td>&nbsp;&nbsp;: " + items[i].o_category + "</td>" +
                                "</tr><tr><th>Overcoat Lapel</th><td>&nbsp;&nbsp;: " + items[i].o_lapelstyle + "</td>" +
                                "</tr><tr><th>Overcoat Elbow Pad</th><td>&nbsp;&nbsp;: " + items[i].o_elbowpadstyle + "</td>" +
                                "</tr><tr><th>Overcoat Canvas</th><td>&nbsp;&nbsp;: " + items[i].o_canvasstyle + "</td>" +
                                "</tr></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                "</div><div class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip'" +
                                " readonly id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'>" +
                                "<i class='fa fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                "','Overcoat','" + escape(items[i].o_canvasstyle) + "','quantity" + i + "','minus','" + i +
                                "','" + items[i].cart_id + "','" + prod_color + "')" +
                                "></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                "','Overcoat','" + escape(items[i].o_canvasstyle) + "','quantity" + i + "','plus','" +
                                i + "','" + items[i].cart_id + "','" + prod_color + "')" +
                                "></i></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity + "' hidden />" +
                                "<p onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i>" +
                                "</p><div class='clear'></div><input class='form-control total_ip' type='text' readonly value='" +
                                items[i].cart_product_total_amount + "' id='total" + i + "' />" + freebies_box + "<input type='button' value='" +
                                meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "' style='margin:20px; 0" +
                                "width:100%' onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            $("#cart").html(button + cartshow);
                        }
                        else if (product_type == "Accessories") {
                            if (prod_image)
                                prod_image = items[i].product_image;
                            cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +
                                "accessinfo'><div class='col-md-2'><img  alt='Custom Fabric' src='" + prod_image + "' " +
                                " class='accessinfoimg' /></div><div class='col-md-10'><table style='font-family: trebuchet ms;" +
                                "letter-spacing:1px;line-height:38px;'><tr><th class=''>Product Name </th><td>" +
                                prod_name + "</td></tr> <tr><th>Product Category</th><td>" + items[i].product_type +
                                "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +
                                "</tr><tr><th>RC Number</th><td>" + items[i].a_rc_no + "</td></tr></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div>" +
                                "<div class='col-md-1' style='text-align:right;padding-right:26px'></div></div><div " +
                                "class='col-md-2'><input type='text' class='form-control quantity_ip' readonly id='quantity" + i
                                + "' value='" + items[i].quantity + "'/><div class='quantity_inc'><i class='fa fa-minus-circle " +
                                "increment_quantity' onclick=incrementValue('" + items[i].user_id + "','Accessories','" +
                                escape(items[i].product_name) + "','quantity" + i + "','minus','" + i + "','" + items[i].cart_id +
                                "','')></i><i class='fa fa-plus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                "','Accessories','" + escape(items[i].product_name) + "','quantity" + i + "','plus','" + i +
                                "','" + items[i].cart_id + "','')></i></div><input type='text' id='limit_reach" + i + "' value='" +
                                items[i].quantity + "' hidden  /><p onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'>" +
                                "<i class='fa fa-trash'></i></p><div class='clear'></div><input class='form-control total_ip' " +
                                "type='text' readonly value='" + items[i].cart_product_total_amount + "' id='total" + i + "'/>" +
                                "" + freebies_box + "<input type='button' value='" + meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "'" +
                                "style='margin:20px 0;width:100%' onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            $("#cart").html(cartshow);
                        }
                        else if (product_type == "Collection") {
                            prod_image = items[i].product_image;
                            cartshow = cartshow + "<div class='col-md-12 completecart'><div class='col-md-10 productinfo " +
                                "accessinfo'><div class='col-md-2' style='height:150px;overflow:hidden'><img  alt='Custom Fabric'" +
                                " src='" + prod_image + "' style='width:150px' /></div><div class='col-md-10'>" +
                                "<table style='font-family: trebuchet ms;letter-spacing:1px;line-height:38px;'><tr>" +
                                "<th>Product Name </th><td>" + prod_name + "</td></tr><tr><th>Product Category</th><td>" +
                                items[i].product_type + "</td></tr><tr><th>Specification</th><td>" + items[i].a_specification + "</td>" +
                                "</tr><tr><th>RC Number</th><td>" + items[i].a_rc_no + "</td></tr></tr> <tr><th>Surplus Price</th> <td><label>$</label><span id='extra_" + i + "'>" + items[i].cart_extra_price + "</span></td></tr></table></div></div><div " +
                                "class='col-md-2 productpricing'><input type='text' class='form-control quantity_ip' readonly" +
                                " id='quantity" + i + "' value='" + items[i].quantity + "'/><div class='quantity_inc'><i class='fa " +
                                "fa-minus-circle increment_quantity' onclick=incrementValue('" + items[i].user_id +
                                "','Collection','" + escape(items[i].a_category) + "','quantity" + i + "','minus','" + i + "','" +
                                items[i].cart_id + "','') ></i><i class='fa fa-plus-circle increment_quantity' " +
                                "onclick=incrementValue('" + items[i].user_id + "','Collection','" + escape(items[i].a_category) +
                                "','quantity" + i + "','plus','" + i + "','" + items[i].cart_id + "','')></i></div><p " +
                                "onclick=deletecart('" + items[i].cart_id + "') class='cartdelicon'><i class='fa fa-trash'></i>" +
                                "</p><div class='clear'></div><input type='text' id='limit_reach" + i + "' value='" + items[i].quantity +
                                "' hidden /><input class='form-control total_ip' type='text' readonly value='" + items[i].cart_product_total_amount + "' id='total" + i + "' />" +
                                "" + freebies_box + "<input type='button' value='" + meas_name + "' class='btn btn-danger' id='cartmeas" + items[i].cart_id + "'" +
                                "style='margin:20px 0;width:100%' onclick=selectcartMeasurement('" + items[i].cart_id + "') /></div></div>";
                            $("#cart").html(cartshow);
                        }
                    }
                    $("#cart").html(cartshow);
                    if (coupon_condition == "discount") {
                        discount_amount = (discount_amount).toFixed(1);
                        $(".coupon_amount").show();
                        $("#finalcartsubtotal").val("$" + (totalamount).toFixed(1));
                        $("#coupon_applied_amount").val("$" + discount_amount);
                        var cart_coupon_dis_amount = ((parseFloat(totalamount) * parseFloat(discount_amount)) / 100).toFixed(2);
                        $("#cart_coupon_dis_amount").val("$" + cart_coupon_dis_amount);
                        $(".finalpaymentcartlabel2").val("$" + ((parseFloat(cart_coupon_dis_amount) * 7.25) / 100).toFixed(2));
                        $("#totalcartsubtotal").val("$" + (parseFloat(((parseFloat(cart_coupon_dis_amount) * 7.25) / 100).toFixed(2)) + parseFloat(cart_coupon_dis_amount)).toFixed(2));

                    }
                    else {
                        $("#finalcartsubtotal").val("$" + (totalamount).toFixed(1));
                        $(".finalpaymentcartlabel2").val("$" + (((parseFloat(parseFloat((totalamount).toFixed(2))) * 7.25) / 100).toFixed(2)));
                        $("#totalcartsubtotal").val("$" + (parseFloat((((parseFloat(parseFloat((totalamount).toFixed(2))) * 7.25) / 100).toFixed(2))) + parseFloat((totalamount).toFixed(1))).toFixed(2));
                    }
                    $(".loader").fadeOut();

                }
                else {
                    document.getElementById("applycoupondiv").style.display = "none";
                    document.getElementById("finalpaymentcart").style.display = "none";
                    cartshow = cartshow + "<p class='nodata'>Cart Is Empty !!!!</p>";
                    document.getElementById("totalpaymentcart").style.display = "none";
                    document.getElementById("placeorder").style.display = "none";
                    $("#placeorder").attr("disabled", true);
                    $("#placeorder").css({"opacity": "0.5"});
                    $(".loader").fadeOut();
                    $("#cart").html(cartshow);
                }
            }
            else {
                document.getElementById("applycoupondiv").style.display = "none";
                document.getElementById("finalpaymentcart").style.display = "none";
                cartshow = cartshow + "<p class='nodata'>Cart Is Empty !!!!</p>";
                document.getElementById("totalpaymentcart").style.display = "none";
                document.getElementById("placeorder").style.display = "none";
                $("#placeorder").attr("disabled", true);
                $("#placeorder").css({"opacity": "0.5"});
                $(".loader").fadeOut();
                $("#cart").html(cartshow);
            }
            var reccomendPostUrl = "api/recommendProcess.php";
            var user_id = $("#user_id").val();
            $.post(reccomendPostUrl, {
                data: JSON.stringify(data.items),
                type: "addRecommends",
                userId: user_id
            }, function (data) {
                console.log('response -- ' + data);
            });
        });
        /*fetch cart data end   here*/
    });

    /*is apply coupon and fetch cart data end   here*/

    function backon_1() {
        var path = window.location.href;
        if (path.indexOf("?") >= 1) {
            path = path.split("?");
            var temp = path[1].split("=");
            if (temp[0] == "stp" && temp[1] == "2" || temp[0] == "stp" && temp[1] == "3") {
                window.location = "cart.php?stp=1&user_id=" + user_id;
            }
        }
    }

    function backon_2() {
        var path = window.location.href;
        if (path.indexOf("?") >= 2) {
            path = path.split("?");
            var temp = path[1].split("=");
            if (temp[0] == "stp" && temp[1] == "3") {
                window.location = "cart.php?stp=2&user_id=" + user_id;
            }
        }
    }

    function continue_shoping() {
        window.location = "order_generate.php";
    }

    function incrementValue(user_id, type, stock, cart_qid, opt, row_id, cart_id, prod_color,btnColor) {
        if (opt == "plus") {
            if (type == "Shirt") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                if (quantity > stock) {
                    $(".modal-title").html("<label style='color:green'>Quantity Limit Reach !!!</label>");
                    $(".modal-body").html("<h5>We have only " + stock + " Items Available in Stock.</h5>");
                    $("#myModal").modal("show");
                    return false;
                }
                var cat_name = "Shirt, " + quantity + " piece";
                var url = "../admin/webserver/get_price.php?type=shirt&cat_name=" + cat_name + "&prod_color=" + prod_color+"&quantity="+quantity+"&btnColor="+btnColor;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Jacket") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var cat_name = "";
                switch (unescape(stock)) {
                    case "C-000A:full_canvas":
                        cat_name = "Full Canvas Jacket";
                        break;
                    case "C-000B:half_canvas":
                        cat_name = "Half Canvas Jacket";
                        break;
                    case "C-00C1:fused":
                        cat_name = "Fused Canvas Jacket";
                        break;
                    case "C-00C3:deconstructed_suits_or_soft_suit":
                        cat_name = "Half Canvas Jacket";
                        break;
                }
                var url = "../admin/webserver/get_price.php?type=jacket&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color+"&btnColor="+btnColor;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;

                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "2PcSuit") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var cat_name = unescape(stock);
                var cat_name = "";
                switch (unescape(stock)) {
                    case "C-000A:full_canvas":
                        cat_name = "Full Canvas 2pc suit";
                        break;
                    case "C-000B:half_canvas":
                        cat_name = "Half Canvas 2pcs suit";
                        break;
                    case "C-00C1:fused":
                        cat_name = "Fused suit 2pcd suit";
                        break;
                    default :
                        cat_name = "Half Canvas 2pcs suit";
                        break;
                }
                var url = "../admin/webserver/get_price.php?type=2pcsuit&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color+"&btnColor="+btnColor;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Pant") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var cat_name = "Pants";
                var url = "../admin/webserver/get_price.php?type=pant&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Vest") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var url = "../admin/webserver/get_price.php?type=vest&cat_name=High-end vest&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Overcoat") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var cat_name = "";
                stock = unescape(stock);
                switch (stock) {
                    case "D-000A:full_canvas":
                        cat_name = "Full canvas Overcoat";
                        break;
                    case "D-000B:half_canvas":
                        cat_name = "Half canvas overcoat";
                        break;
                    case "D-00C1:fused":
                        cat_name = "Fused overcoat";
                        break;
                }
                var url = "../admin/webserver/get_price.php?type=overcoat&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Accessories") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var accessory_name = stock;
                var cat_name = accessory_name.split("_")[0];
                var url = "../admin/webserver/get_price.php?type=accessories&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Collection") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity + 1;
                var cat_name = stock;
                var url = "../admin/webserver/get_price.php?type=accessories&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
        }
        else {
            if (type == "Shirt") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = "Shirt, " + quantity + " piece";
                var url = "../admin/webserver/get_price.php?type=shirt&cat_name=" + cat_name + "&prod_color=" + prod_color+"&quantity="+quantity+"&btnColor="+btnColor;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).val(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Jacket") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = "";
                switch (unescape(stock)) {
                    case "C-000A:full_canvas":
                        cat_name = "Full Canvas Jacket";
                        break;
                    case "C-000B:half_canvas":
                        cat_name = "Half Canvas Jacket";
                        break;
                    case "C-00C1:fused":
                        cat_name = "Fused Canvas Jacket";
                        break;
                    case "C-00C3:deconstructed_suits_or_soft_suit":
                        cat_name = "Half Canvas Jacket";
                        break;
                }
                var url = "../admin/webserver/get_price.php?type=jacket&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color+"&btnColor="+btnColor;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "2PcSuit") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = "";
                switch (unescape(stock)) {
                    case "C-000A:full_canvas":
                        cat_name = "Full Canvas 2pc suit";
                        break;
                    case "C-000B:half_canvas":
                        cat_name = "Half Canvas 2pcs suit";
                        break;
                    case "C-00C1:fused":
                        cat_name = "Fused suit 2pcd suit";
                        break;
                    default :
                        cat_name = "Half Canvas 2pcs suit";
                        break;
                }

                var url = "../admin/webserver/get_price.php?type=2pcsuit&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color+"&btnColor="+btnColor;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Pant") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = "Pants";
                var url = "../admin/webserver/get_price.php?type=pant&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Vest") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var url = "../admin/webserver/get_price.php?type=vest&cat_name=High-end vest&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Overcoat") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = "";
                stock = unescape(stock);
                switch (stock) {
                    case "D-000A:full_canvas":
                        cat_name = "Full canvas Overcoat";
                        break;
                    case "D-000B:half_canvas":
                        cat_name = "Half canvas overcoat";
                        break;
                    case "D-00C1:fused":
                        cat_name = "Fused overcoat";
                        break;
                }
                var url = "../admin/webserver/get_price.php?type=overcoat&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            } else if (type == "Accessories") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var accessory_name = stock;
                var cat_name = accessory_name.split("_")[0];
                var url = "../admin/webserver/get_price.php?type=accessories&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Collection") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = stock;
                var url = "../admin/webserver/get_price.php?type=accessories&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
            else if (type == "Custom Overcoat") {
                var quantity = parseInt($("#" + cart_qid).val());
                quantity = quantity - 1;
                if (quantity < 1) {
                    return false;
                }
                var cat_name = stock;
                var url = "../admin/webserver/get_price.php?type=customOvercoat&cat_name=" + cat_name + "&quantity=" + quantity + "&prod_color=" + prod_color;
                $.get(url, function (datas) {
                    var json = $.parseJSON(datas);
                    var status = json.status;
                    if (status == "done") {
                        var amounts = json.amount;
                        var prod_total = amounts.total_amount;
                        var extra_price = amounts.extra_price;
                        $("#" + cart_qid).val(quantity);
                        $("#extra_" + row_id).html(extra_price);
                        $("#total" + row_id).val(prod_total);
                        //                        prod_total = parseFloat(prod_total)+parseFloat(extra_price );
                        increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price);
                    }
                });
            }
        }
    }

    function increment_cart_value(quantity, prod_total, cart_id, user_id, extra_price) {
        var url = "../admin/webserver/update_cart.php?cart_id=" + cart_id + "&product_quantity=" + quantity + "&product_total=" + prod_total + "&extra_price=" + extra_price;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                var url = "../admin/webserver/apply_coupon.php?type=getfullcartcount&user_id=" + user_id;
                $.get(url, function (data) {
                    var json = $.parseJSON(data);
                    var status = json.status;
                    if (status == "done") {
                        var subtotal = json.message;
                        var url3 = "../admin/webserver/apply_coupon.php?type=isCouponApplied&user_id=" + user_id;
                        $.get(url3, function (data3) {
                            var json3 = $.parseJSON(data3);
                            var status3 = json3.status;
                            var cart_coupon_dis_amount = 0;
                            if (status3 == "done") {
                                var resp = json3.message;
                                cart_coupon_dis_amount = subtotal - resp;
                                $(".coupon_amount").show();

                                $("#coupon_applied_amount").val("$" + (resp).toFixed(2));
                                $("#cart_coupon_dis_amount").val("$" + (cart_coupon_dis_amount).toFixed(2));
                                $("#finalcartsubtotal_actual").val("$" + (cart_coupon_dis_amount).toFixed(2));

                                $("#finalcartsubtotal").val("$" + subtotal);
                                var currentTax = (parseFloat(cart_coupon_dis_amount) * parseFloat(7.25)) / 100;
                                var finalwithtax = (parseFloat(cart_coupon_dis_amount) + parseFloat(currentTax)).toFixed(2);
                                $(".finalpaymentcartlabel2").val("$" + currentTax.toFixed(2));
                                $("#totalcartsubtotal").val("$" + finalwithtax);
                            }
                            else {
                                $("#finalcartsubtotal").val("$" + subtotal);
                                var currentTax = (parseFloat(subtotal) * parseFloat(7.25)) / 100;
                                var finalwithtax = (parseFloat(subtotal) + parseFloat(currentTax)).toFixed(2);
                                $(".finalpaymentcartlabel2").val("$" + currentTax.toFixed(2));
                                $("#totalcartsubtotal").val("$" + finalwithtax);
                            }
                        });
                    }
                    else {
                        $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                        $(".modal-body").html("Server Error !!! Please Try After Some Time");
                        $("#myModal").modal("show");
                    }
                });
            }
            else {
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(json.message);
                $("#myModal").modal("show");
            }
        }).fail(function () {
            $(".modal-title").html("<label style='color:red'>Error Occured</label>");
            $(".modal-body").html("Something Went Wrong !!! Please Try After Some Time");
            $("#myModal").modal("show");
        });
    }

    var path = window.location.href;
    if (path.indexOf("&") >= 0) {
        path = path.split("?");
        var temp = path[1].split("&");
        if (temp[0] == "stp=2") {
            /*$("#selectedMeasurement").val(meas[1]);*/
            setTimeout(function(){
                $("#clear_cart").css({"display": "none"});
            },1000);

            $("#optioncart").removeClass("optionnumboxactive");
            $("#paymentcart").addClass("optionnumboxactive");
            $("#cart").css({"display": "none"});
            $("#applycoupondiv").css({"display": "none"});
            $("#finalpaymentcart").css({"display": "none"});
            $("#totalpaymentcart").css({"display": "none"});
            $("#stp1_buttons").css({"display": "none"});
            $("#step2_div").css({"display": "block"});
            $("#make_payment").css({"display": "block"});
            userPersonal();
        }
        else {
            $("#paymentcart").removeClass("optionnumboxactive");
            $("#optioncart").addClass("optionnumboxactive");
            $("#make_payment").css({"display": "none"});

        }
    }
    else {

        $("#paymentcart").removeClass("optionnumboxactive");
        $("#optioncart").addClass("optionnumboxactive");
        $("#make_payment").css({"display": "none"});

    }

    function step_2_task() {
        window.location = "order_cart.php?stp=2&userId="+user_id;
    }

    function userPersonal() {
        var url = "../admin/webserver/register.php?type=get_billing&user_id=" + user_id;
        $("#clear_cart").css({"display": "none"});
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            var items = json.items;
            if (status == "done") {
                var item = items[0];
                $("#countryId").append("<option selected='selected'>" + item.country + "</option>");
                $("#user_id").val(item.user_id);
                $("#first_name").val(item.first_name);
                $("#last_name").val(item.last_name);
                $("#address").val(item.address);
                $("#stateId").html("<option selected='selected'>" + item.state + "</option>");
                $("#postalcode").val(item.pincode);
                $("#phonenumber").val(item.mobile);
                setTimeout(function () {
                    $("#total_am").html(parseFloat($("#finalcartsubtotal").val().split("$")[1]));
                    $("#tax").html("$" + ((parseFloat($("#finalcartsubtotal").val().split("$")[1]) * 7.25) / 100).toFixed(2));
                    $("#grand_total").html(parseFloat(parseFloat($("#finalcartsubtotal").val().split("$")[1]) + ((parseFloat($("#finalcartsubtotal").val().split("$")[1]) * 7.25) / 100)).toFixed(2));
                }, 500);
                $("#cityId").append("<option selected='selected'>" + item.city + "</option>")
            }
        });
    }

    //shipping and billing process end here
    //payment confirmation process start here
    function selectcartMeasurement(cart_id) {
        $(".loader").fadeIn("slow");
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getAllMeasurement", "user_id": user_id}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                var measurements = data.measurements;
                var datashow = "<div class='row'><div class='container-fluid'><table class='table table-bordered'><tr><th>S.No</th><th>Measurement Name</th>" +
                    "<th>Added On</th><th>Action</th>";
                for (var i = 0; i < measurements.length; i++) {
                    var date = new Date(measurements[i].added_on * 1000);
                    var year = date.getFullYear();
                    var month = date.getMonth() + 1;
                    var day = date.getDate();
                    var added_on = day + "/" + month + "/" + year;
                    datashow += "<tr><td>" + (i + 1) + "</td><td>" + measurements[i].meas_name + "</td><td>" + added_on + "</td>" +
                        "<td><input type='button' value='Select' class='btn btn-danger btn-sm' " +
                        "onclick=finalSelection('" + cart_id + "','" + measurements[i].measurement_id + "','" + escape(measurements[i].meas_name) + "') />" +
                        "<i class='fa fa-trash' onclick=deletemeas('" + measurements[i].measurement_id + "') " +
                        "style='cursor:pointer;margin-left:20px; color:#CC3834;font-size:17px;position:absolute;margin-top:6px'>" +
                        "</i></td></tr>";
                }
                datashow += "</table></div></div>";
                $(".modal-title").html("<span style='color:green'>Select Measurement File</span>");
                $(".modal-body").html(datashow);
                $(".modal-footer").css("display", "none");
                $("#myModal").modal("show");
            }
            else {
                showMessage("Not Any Measurement Available Yet !!! Please Save Atleast One Measurement First", "red");
                setTimeout(function () {
                    window.location = "customer_step_two.php?userId="+user_id;
                }, 3000);
            }
        }).fail(function () {
            alert("Server Error !!! Please Try After Some Time");
        });
        $(".loader").fadeOut("slow");
    }

    function deletemeas(measurement_id) {
        var url = "../api/registerUser.php";
        $.post(url, {"type": "deleteMeasurement", "meas_id": measurement_id}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                $("#myModal").modal("hide");
                setTimeout(function () {
                    selectMeasurement();
                }, 500);
            } else {
                alert("Server Error !!! Please Try After Some Time");
            }
        }).fail(function () {
            alert("Server Error !!! Please Try After Some Time");
        });
    }

    function finalSelection(cart_id, measurement_id, meas_name) {
        $("#selectedMeasurement").val(measurement_id);
        var url = '../api/registerUser.php';
        meas_name = unescape(meas_name);
        $.post(url, {
            "type": "addCartMeasurement",
            "meas_id": measurement_id,
            "cart_id": cart_id,
            "meas_name": meas_name
        }, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                $("#myModal").modal("hide");
                $("#cartmeas" + cart_id).val(meas_name);
            } else {
                $("#myModal").modal("hide");
                setTimeout(function () {
                    showMessage(data.Message, "red");
                }, 1000);
            }
        }).fail(function () {
            $("#myModal").modal("hide");
            setTimeout(function () {
                showMessage("Server Error !!! Please Try After Some Time", "red");
            }, 1000);
        });
        //step_2_task(measurement_id);
    }
    function update_shipping_address(paymentType) {
        $(".loader").fadeIn("slow");
        var user_id = $("#user_id").val();
        var billing_fname = $("#first_name").val();
        var billing_lname = $("#last_name").val();
        var address = $("#address").val();
        var country = $("#countryId").val();
        var state = $("#stateId").val();
        var city = $("#cityId").val();
        var pincode = $("#postalcode").val();
        var phone = $("#phonenumber").val();
        var billing_address = address;
        if (billing_fname == "" || billing_lname == "" || address == "" || country == "" || state == ""
            || city == "" || pincode == "" || phone == "") {
            $(".loader").fadeOut("slow");
            showMessage("Please Fill All the Required Feilds...", "red");
        } else {
            var url = "../api/registerUser.php";
            $.post(url, {"type": "getAllMeasurement", "user_id": user_id}, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    var url = "../admin/webserver/register.php?type=billing&user_id=" + user_id + "&billing_fname=" + billing_fname +
                        "&billing_lname=" + billing_lname + "&billing_address=" + billing_address + "&country=" + country + "&state=" +
                        state + "&city=" + city + "&pincode=" + pincode + "&phone=" + phone;
                    $.get(url, function (data) {
                        var json = $.parseJSON(data);
                        var status = json.status;
                        if (status == "done") {
                            var subtotal = parseFloat($("#finalcartsubtotal").val().split("$")[1]);
                            var coupon_code = '';
                            var url3 = "../admin/webserver/apply_coupon.php?type=isCouponApplied&user_id=" + user_id;
                            $.get(url3, function (data3) {
                                var json3 = $.parseJSON(data3);
                                var status3 = json3.status;
                                if (status3 == "done") {
                                    coupon_code = json3.coupon_code;
                                }
                                subtotal = (subtotal + (((subtotal * 7.25)) / 100)).toFixed(2);
                                order_detail(user_id, subtotal, escape(coupon_code),paymentType);
                            });
                        }
                    });
                } else {
                    showMessage("Not Any Measurement Available Yet !!! Please Save Atleast One Measurement First", "red");
                    setTimeout(function () {
                        window.location = "customer_step_two.php?userId="+user_id;
                    }, 3000);
                }
            });

        }
    }

    openNewTab = function () {
        var newTabWindow = window.open();
        return newTabWindow;
    };
    updateTabLocation = function (tabLocation, tab) {
        if (!tabLocation) {
            tab.close();
        }
        tab.location.href = tabLocation;
    };

    function order_detail(user_id, total, couponCode,paymentType) {
        couponCode = unescape(couponCode);
        var json = '{"user_id":"' + user_id + '","couponCode":"' + couponCode + '","total":"' + total + '"}';
        var url = "../admin/webserver/get_orders.php?id=3&json=" + json;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                $(".loader").fadeOut("slow");
                var order_id = json.order_id;
                if(paymentType == "square"){
                    gotoSquare(''+order_id+'',''+total+'');
                }
                else if(paymentType == "paypal"){
                    window.location.href = "order_paynow.php?order_id=" + order_id + "&order_amount=" + total;
                }
            }
            else {
                $(".loader").fadeOut("slow");
                if(json.message == "Cart Should Have Atleast 1 Item"){
                    showMessage(json.message, "red");
                    setTimeout(function () {
                        window.location = "order_generate.php";
                    }, 3000);
                }
                else{
                    showMessage(json.message, "red");
                }

            }
        });
    }
    function gotoSquare(order_id,total){
        var form = document.createElement("form");
        var element0 = document.createElement("input");
        var element1 = document.createElement("input");
        var element2 = document.createElement("input");
        form.method = "POST";
        form.action = "order_square.php?userId="+user_id;

        element0.value="square";
        element0.name="type";
        form.appendChild(element0);

        element1.value=order_id;
        element1.name="order_id";
        form.appendChild(element1);

        element2.value=total;
        element2.name="total";
        form.appendChild(element2);
        document.body.appendChild(form);
        form.submit();
    }

    function deletecart(cart_id) {
        $(".modal-title").html("<label style='color:green'>Confirm Status !!!</label>");
        $(".modal-body").html("<h5>Are you sure want to delete this item.</h5>");
        $(".modal-footer").css("display", "block");
        $(".modal-footer").html("<input type='button' class='btn btn-warning' value='Confirm' onclick=confirm_deletecart('" + cart_id + "') />");
        $("#myModal").modal("show");
    }

    function confirm_deletecart(cart_id) {
        var url = "../admin/webserver/clear_cart.php?type=remove&cart_id=" + cart_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                window.location = "order_cart.php?stp=1&userId="+user_id;
            }
            else {
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(json.message);
                $("#myModal").modal("show");
            }
        });
    }

    //clear all cart items
    function clear_cart(user_id) {
        $(".modal-title").html("<label style='color:green'>Confirm Status !!!</label>");
        $(".modal-body").html("<h5>Are you sure want to delete all items in the cart.</h5>");
        $(".modal-footer").html("<input type='button' class='btn btn-warning' value='Confirm' onclick=confirm_clear_cart('" + user_id + "') />");
        $("#myModal").modal("show");
    }

    function confirm_clear_cart(user_id) {
        var url = "../admin/webserver/clear_cart.php?type=clear&user_id=" + user_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                window.location = "order_cart.php?stp=1&userId="+user_id;
            }
            else {
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(json.message);
                $("#myModal").modal("show");
            }
        });
    }

    setTimeout(getCart(), 3000);

    function applyCoupon() {
        var couponCode = $("#validcoupon").val();
        var user_id = $("#user_id").val();
        var url = "../admin/webserver/apply_coupon.php?couponCode=" + couponCode + "&user_id=" + user_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                var price = json.message;
                var coupon_condition = json.coupon_condition;
                if (coupon_condition == "discount") {
                    var discount_amount = json.discount_amount;
                    var coupon_code = json.coupon_code;
                    var coupon_applyd_on = json.coupon_applyd_on;
                    $(".modal-title").html("<label style='color:green'>Success</label>");
                    $(".modal-body").html("Congratulations !!! you Got " + discount_amount + " Discount by Applying Code " +
                        coupon_code.toUpperCase() + "<br>Coupon Applied On : " + coupon_applyd_on);
                    $("#myModal").modal("show");
                }
                else if (coupon_condition == "freebies") {
                    $(".modal-title").html("<label style='color:green'>Success</label>");
                    $(".modal-body").html("Congratulations !!! Coupon Applied SuccessFully " +
                        "Now you can select a free '" + json.freebies_item + "' with every '" + json.coupon_applyd_on + "'");
                    $("#myModal").modal("show");
                }
                setTimeout(function () {
                    location.reload();
                }, 4000);
            }
            else {
                $(".coupon_amount").hide();
                $(".modal-title").html("<label style='color:red'>Error Occured</label>");
                $(".modal-body").html(json.message);
                $("#myModal").modal("show");
            }
        });
    }

    function isCouponApplied(user_id) {
        var url = "../admin/webserver/apply_coupon.php?type=isCouponApplied&user_id=" + user_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            var resp = "false";
            if (status == "done") {
                resp = json.message;
            }
            return resp;
        });
    }
    getCart(user_id);
</script>