<?php
include('header.php');
?>
<style>
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .main-grid-box {
        text-align: center;
        margin-left: 5%;
        margin-bottom: 2%;
        border: 1px solid #999;
        /*width:80%*/
    }
    .ext-img {
        height: 200px;
        /*height: 50px;*/
        background-position: center center;
        background-size: contain;
        background-repeat: no-repeat;
    }
    #logoBox{
        max-height: 900px;
        overflow-y: auto;
    }
</style>
<link href = "js/jquery-ui.css" rel = "stylesheet">
<script src="js/jquery-ui.js"></script>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<!--  page content  -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Awards Logo <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <form method="post" class="form-inline">
                                <div class="form-group form-inline">
                                    <label id="message" style="color: red"></label>
                                    <input type="file" class="form-control" name="fabricFile" id="logoFile" />
                                    <input type="button" Value="+ Add Logo" class="btn btn-primary btn-sm" name="filterButton"
                                           onclick="addCollar()" style="margin-bottom: -1px"/>
                                </div>
                            </form>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>

    <!--body part start here-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div id="sortable-8">
                <div id="logoBox">

                </div>
            </div>

        </div>
    </div>
    <!--body part end here-->


</div>

<!-- /page content -->
<?php
include('footer.php');
?>
<!-- script -->
<script>
    function addCollar() {
        var logoFile = $("#logoFile").val();
        if(logoFile == ""){
            $("#message").html("Please select image file");
            return false;
        }
        var data = new FormData();
        $("#message").html("");
        $(".loaderdiv").removeClass("loaderhidden");
        var _file = document.getElementById('logoFile');
        var image_ext = logoFile.split(".");
        image_ext = image_ext[image_ext.length - 1];
        image_ext = image_ext.toLowerCase();
        if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
            data.append('type', "addCollar");
            data.append('file_', _file.files[0]);
        }
        else{
            $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
            $(".loaderdiv").addClass("loaderhidden");
            return false;
        }
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#message").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else
                {
                    $("#message").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/brandLogoProcess.php');
        request.send(data);
    }
    var idOrder = [];
    /*for sorting*/
    function getBrandData(){
        var url = "api/brandLogoProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url,{"type":"getLogo"},function(data){
            var status = data.Status;
            var logoData = data.data;
            if(status == "Success"){
                /* showMessage(data.Message, "green");*/
                var logoBox = "";
                for(var a= 0; a < logoData.length; a++){
                    idOrder.push(logoData[a].id);
                    logoBox = logoBox+'<div id="'+logoData[a].sort_order+'" name="'+logoData[a].id+'" ' +
                        'class="default col-sm-3 main-grid-box sortId_'+logoData[a].sort_order+'" style="height: 250px;">' +
                        '<div class="pdf-imgr ext-img" style=background-image:url("api/Files/images/'+logoData[a].logo_file+'")>' +
                        '</div><div class="action-point">' +
                        '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+logoData[a].id+'","'+encodeURI(logoData[a].logo_file)+'");>' +
                        '<i class="fa fa-trash"></i></button></div></div>';
                }
                $("#logoBox").html(logoBox);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                showMessage(data.Message, "red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
        });
    }
    getBrandData();


    function confirmDelete(id,path) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>'+
            '<button type="button" class="btn btn-danger" onclick=deleteData("'+id+'","'+path+'")>Delete</button>');
        $("#myModal").modal("show");
    }

    function deleteData(id,path) {
        var url ='api/brandLogoProcess.php';
        path = decodeURI(path);
        $.post(url,{"type":'deleteLogo','id':id,"path":path},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                location.reload();
            }
            else{
                $("#myModal").modal("hide");
                showMessage(message,"green");
                getBrandData();
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    $('#logoBox').sortable({
        update: function(event, ui) {
            var sortOrder = $(this).sortable('toArray').toString();
            sortOrder = sortOrder.split(",");
            var updateOrderId = [];
            var updateSortOrder = [];
                for(var a=0; a < sortOrder.length; a++){
                    var sortNumber = $(".sortId_"+sortOrder[a]).attr("name");
                    sortNumber = parseInt(sortNumber);
                    updateOrderId.push(sortNumber);
                    updateSortOrder.push(a);
                }
            var url = "api/brandLogoProcess.php";
            $(".loaderdiv").removeClass("loaderhidden");
            $.post(url,{"type":'updateSortOrder',"sortOrder":updateSortOrder,"idOrder":updateOrderId},function (data) {
                var status = data.Status;
                if(status === "Success") {
                    //window.location.reload();
                        getBrandData();
                }
            });
        }
    });

</script>
<!-- / script -->
