<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 10/24/2017
 * Time: 11:21 AM
 */
namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class PRICE_CAT{
    public $link = null;
    public $userClass = null;
    public $response = array();
    public function __construct(){
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
    }

    public function getCategories() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from price_list_cat ORDER BY id";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;
    }
    public function getCategoriesPart($priceId) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from price_list_cat where id='$priceId' ORDER BY id";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;
    }
    public function editPriceCat($priceId,$new_title,$oldcategoryImagechanged) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldcategoryImagechanged == "yes") {
            if(isset($_FILES['newcatimg']['tmp_name'])) {
                $file_name = $_FILES['newcatimg']['name'];
                if (strpos($file_name, "'") >= 0) {
                    $file_name = str_replace("'", "", $file_name);
                }
                $file_tmp = $_FILES['newcatimg']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['newcatimg']['name'])));
                if (in_array($file_ext, $extensions) === true) {
                    $db_file_name = "price_".rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/price_cat/" . $db_file_name)) {
//                        move_uploaded_file($file_tmp, getcwd() . "/Files/images/partner/" . $file_name);
                        $file_path = "http://scan2fit.com/sftailor/admins/api/Files/images/price_cat/" . $db_file_name;
                        $query = "update price_list_cat set name ='$new_title',image='$db_file_name', image_path='$file_path'
                        where id = '$priceId'";
                    }
                    else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "getcwd is  ".getcwd();
                        return $this->response;
                    }
                }
                else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }
        else{
            $query = "update price_list_cat set name='$new_title' where id = '$priceId'";
        }
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            $this->response["sql"] = $query;

            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }
    
    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}