<?php
include('header.php');
include("admin/webserver/database/db.php");
if($user_id == ""){
	echo "<script>document.location.href='login.php'</script>";
}  
?>
</div>
<div class="about-bottom wthree-3">
<link rel="stylesheet" href="css/orders.css">
<style>
.top-bar {
    background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
}
.custommpbtn1
{
    background: white none repeat scroll 0 0;
    border: 1px solid #ccc;
    border-radius: 5px;
    color: #000;
    font-size: 16px;
    font-weight: 600;
    margin-left: 10px;
    margin-top: -10px!important;
    padding: 8px 60px;
}
.img {
    height: 100px;
    margin: -13px;
    position: absolute;
	width: 100px;
}
.wthree-3 {
    padding: 11em 0;
}
.col-md-12.no-gutter > h3 {
    margin-bottom: 20px;
    margin-top: 19px;
    text-align: center;
}
</style>
<div class="col-md-12 addressbar">
	<div class="col-md-1"></div>
	<div class="col-md-10">
		<label class="productviewfirstlabel"><a href="index.php"><i class="fa fa-home"></i>&nbsp;Home</a> / <span onclick="back()" style="cursor:pointer">Orders</span></label>
		<label class="productviewrightlabel" onclick="back() "><i class="fa fa-arrow-left"></i>&nbsp;Back to Product List</label>
	</div>
	<div class="col-md-1"></div>
</div>
<div class="clear"></div>
<div class="col-md-12">
	<div class="col-md-1"></div>
	<div class="col-md-10 no-gutter">
		<div class="col-md-12 no-gutter" >
			<h3>MY ORDERS</h3>
		</div>
	</div>
</div>
<div class="col-md-12">
	<div class='col-md-1'></div>
	<div class='col-md-10'id="dynamicOrders">
		<!-- data is loaded here -->
	</div>
	<div class='col-md-1' ></div>
	
</div>
	<?php
    $paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
	$paypal_id = 'kapillikes@gmail.com';  
	?>
        <form action="<?php echo $paypal_url; ?>" method="post">
            <!-- Identify your business so that you can collect the payments. -->
            <input name="business" value="<?php echo $paypal_id; ?>" type="hidden">
            <!-- Specify a Buy Now button. -->
            <input name="cmd" value="_xclick" type="hidden">
            <!--    <input type="hidden" name="cmd" value="_cart">-->
            <!-- Specify details about the item that buyers will purchase. -->
            <input name="item_name" value="Orders" type="hidden">
            <input name="item_number" id="pp_order_id" value="" type="hidden">
            <input id="pp_order_amount" name="amount" value="" type="hidden">
            <input name="currency_code" value="USD" type="hidden">
            <!-- Specify URLs -->
            <input type='hidden' name='cancel_return' value="" id="cancelReturnUrl"/>
            <input name="return" value="" id="returnUrl" type="hidden"/>
            <!-- Display the payment button. -->
            <input style="display:none" name="submit" value="PayNow" id="paybtn" type="submit">
        </form>
<script>
function order_data()
{
	var userid = $("#user_id").val();
	var url = "admin/webserver/get_orders.php?id=1&user_id="+userid;
	$.get(url,function(data){
		var json = $.parseJSON(data);
        var status = json.status;
        var cartshow = "";
        if(status == "done"){
            var items = json.data;
            if(items.length > 0){
                var datashow="";
                var formshow="";
                for(var i=0;i<items.length;i++)
                {
                    var paymentPending = items[i].order_payment_status;
                    if(paymentPending =="Pending"){
                        var paymentStatus =items[i].order_payment_status;
                        var stat="display:block";
                    }else{
                        var paymentStatus = items[i].order_payment_status;
                        var stat="display:none";
                    }
                    datashow = datashow+"<div class='order'>" +
                    "<div class='order-title'><p class='title'>"+items[i].order_state+"</p></div>" +
                    "<div class='order-info'><div class='order-info-0'>"+items[i].order_date+"</div>" +
                    "<div class='order-info-1'>"+items[i].billing_name+"</div>" +
                    "<div class='order-info-2'>Order number: "+items[i].order_number+"</div>" +"<div class='order-info-2'>Payment Status: "+paymentStatus+"<div style='"+stat+"' id='make_payment' onclick=make_complete_payment('"+items[i].order_id+"','"+items[i].order_total+"') class='custommpbtn1 pull-right order-pending' type='button'>Make payment"+"</div></div>" +
                    "<div class='order-info-left'>" +
                    "<div class = 'order-info-left-0' id='dynaForm' ></div>" +
                    "<div class='order-info-left-1'><span>Total :</span><label style='margin-right: 20px; margin-top: 8px; margin-bottom: -8px; background: forestgreen none repeat scroll 0px 0px; color: white; font-size: 19px; padding: 3px 8px; border-radius: 4px;'>$"+items[i].order_total+"</label></div>" +
                    "</div>" +
                    "</div>";
                    var orderdetail = items[i].order_detail;
                    var counter = 0;
                    var item = "";
                    for(var j=0;j<orderdetail.length;j++)
                    {
                        counter++;
                        if(orderdetail[j].product_type == "Custom Shirt")
                        {
                            var prod_image = orderdetail[j].product_image;
                            prod_image = prod_image.split(",");
                            item = item+"<div class='product-order'>" +
                            "<div class='product-order0'><img src='"+prod_image[0]+"' class='img' ><img src='"+prod_image[1]+"' class='img' >" +
                            "<img src='"+prod_image[2]+"' class='img' ><img src='"+prod_image[3]+"' class='img' ><img src='"+prod_image[4]+"' class='img' >" +
                            "<img src='"+prod_image[5]+"' class='img' ><img src='"+prod_image[6]+"' class='img' ><img src='"+prod_image[7]+"' class='img' >" +
                            "<img src='"+prod_image[8]+"' class='img' ></div>" +
                            "<div class='product-order1' style='margin-left:55px'>Product Price:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_price+"</span></div><div class='product-order1'>Product Quantity:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_quantity+"</span></div><div class='product-order1'>Product Name:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_product_name+"</span></div>"+
                            "<div class='product-order1'>Product Type :&nbsp;&nbsp;<span style='color:green;" +
                            "font-weight:bold;'>"+orderdetail[j].product_type+"</div></div>";
                        }
                        if(orderdetail[j].product_type == "Custom Pant")
                        {
                            var prod_image = orderdetail[j].product_image;
                            prod_image = prod_image.split(",");
                            item = item+"<div class='product-order'>" +
                            "<div class='product-order0'><img src='"+prod_image[0]+"' class='img' ><img src='"+prod_image[1]+"' class='img' >" +
                            "<img src='"+prod_image[2]+"' class='img' ><img src='"+prod_image[3]+"' class='img' ><img src='"+prod_image[4]+"' class='img' ></div>" +
                            "<div class='product-order1' style='margin-left:55px'>Product Price:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_price+"</span></div><div class='product-order1'>Product Quantity:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_quantity+"</span></div><div class='product-order1'>Product Name:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_product_name+"</span></div>"+
                            "<div class='product-order1'>Product Type :&nbsp;&nbsp;<span style='color:green;" +
                            "font-weight:bold;'>"+orderdetail[j].product_type+"</div></div>";
                        }
                        if(orderdetail[j].product_type == "Custom Suit")
                        {
                            var prod_image = orderdetail[j].product_image;
                            prod_image = prod_image.split(",");
                            item = item+"<div class='product-order'>" +
                            "<div class='product-order0'><img src='"+prod_image[0]+"' class='img' ><img src='"+prod_image[1]+"' class='img' >" +
                            "<img src='"+prod_image[2]+"' class='img' ><img src='"+prod_image[3]+"' class='img' ><img src='"+prod_image[4]+"' class='img' >" +
                            "<img src='"+prod_image[5]+"' class='img' ><img src='"+prod_image[6]+"' class='img' ><img src='"+prod_image[7]+"' class='img' >" +
                            "<img src='"+prod_image[8]+"' class='img' ></div>" +
                            "<div class='product-order1' style='margin-left:55px'>Product Price:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_price+"</span></div><div class='product-order1'>Product Quantity:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_quantity+"</span></div><div class='product-order1'>Product Name:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_product_name+"</span></div>"+
                            "<div class='product-order1'>Product Type :&nbsp;&nbsp;<span style='color:green;" +
                            "font-weight:bold;'>"+orderdetail[j].product_type+"</div></div>";
                        }
                        if(orderdetail[j].product_type == "Accessories")
                        {
                            var prod_image = orderdetail[j].product_image;
                            item = item+"<div class='product-order'>" +
                            "<div class='product-order0'><img src='"+prod_image+"' class='img' ></div>" +
                            "<div class='product-order1' style='margin-left:55px'>Product Price:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_price+"</span></div><div class='product-order1'>Product Quantity:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_quantity+"</span></div><div class='product-order1'>Product Name:&nbsp;&nbsp;<span style='color:green;font-weight: bold;'>"+
                            orderdetail[j].det_product_name+"</span></div>"+
                            "<div class='product-order1'>Product Type :&nbsp;&nbsp;<span style='color:green;" +
                            "font-weight:bold;'>"+orderdetail[j].product_type+"</div></div>";
                        }
                    }
                    datashow = datashow+item+"</div>";
                    $("#dynamicOrders").html(datashow);
                }
            }
            else{
                datashow = "<br><br><div class='col-md-12 no-gutter'><div class='col-md-1 no-gutter'></div><div class='col-md-10 no-gutter'><label>No Orders found</label></div></div><br><br>";
                $("#dynamicOrders").html(datashow);
            }
        }
        else{
            datashow = "<br><br><div class='col-md-12 no-gutter'><div class='col-md-1 no-gutter'></div><div class='col-md-10 no-gutter'><label>No Orders found</label></div></div><br><br>";
            $("#dynamicOrders").html(datashow);
        }
    });
}
order_data();
function make_complete_payment(order_id,order_total) {
    var order_id = order_id;
    var totalAmount = order_total;
        $("#returnUrl").val("http://scan2fit.com/sftailor/admin/webserver/payment.php?payment_status=Success&order_id=" +order_id + "&paid_amount=" + totalAmount);
        $("#cancelReturnUrl").val("http://scan2fit.com/sftailor/admin/webserver/payment.php?payment_status=Failed&order_id=" +
        order_id + "&paid_amount=" + totalAmount);
        $("#pp_order_id").val(order_id);
        $("#pp_order_amount").val(totalAmount);
        order_status();
}
function order_status() {
    var user_id = $("#user_id").val();
    var url = "admin/webserver/clear_cart.php?type=clear&user_id=" + user_id;
    $.get(url, function (data) {
        var json = $.parseJSON(data);
        var status = json.status;
        if (status == "done") {
                $("#paybtn").click();
        }
    });
}
function back()
{
    window.location="customize.php";
}
</script>
<div style="clear:both;"></div>
</br>
</div>
<?php
include('footer.php');
?>