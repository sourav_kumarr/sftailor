<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 9/25/2017
 * Time: 12:16 PM
 */

namespace Classes;

require_once "CONNECT.php";
class Recommends
{
    private $link = null;

    private $CurrentDateTime = null;
    private $data = array();
    private $user_id;

    public function __construct()
    {
        $this->link = new \Modals\CONNECT();
        $this->CurrentDateTime = date("Y-m-d h:i:s");
    }

    public function __call($function, $args)
    {
        $functionType = strtolower(substr($function, 0, 3));
        $propName = lcfirst(substr($function, 3));
        switch ($functionType) {
            case 'get':
                if (property_exists($this, $propName)) {
                    return $this->$propName;
                }
                break;
            case 'set':
                if (property_exists($this, $propName)) {
                    $this->$propName = $args[0];
                }
                break;
        }
    }

    /**
     * @param array $data
     */
    public function setData($data) {
        $this->data = $data;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }
    /**
     *
     *
     in this function all data will be saved to relevant table
     */

    public function save() {

       $link =  $this->link->connect();
       $response = array();
       if($link) {
           for($i=0;$i<count($this->data);$i++) {
               $obj_item = $this->data[$i];
               $obj_item['user_id'] = $this->user_id;
               $keys = implode(",",array_keys($obj_item));
               $values = array_map(function($value) { return "'$value'"; }, array_values($obj_item));
               $values = implode(",",$values);
               $prod_name = $obj_item['product_name'];

               if($this->isProductExists($prod_name)[Status] == Success) {
                   $query = "insert into wp_recommends($keys) values($values)";
                   $result = mysqli_query($link,$query);
                   if(!$result) {
                       $response[Status] = Error;
                       $response[Message] = "Unable to store data ".mysqli_error($link);
                       return $response;
                   }
               }
             }
           $response[Status] = Success;
           $response[Message] = "Data updated successfully";
           return $response;

       }

       $response[Status] = Error;
       $response[Message] = "Unable to connect with db ".mysqli_error($link);

       return $response;

    }


    public function addToCart() {
        $link =  $this->link->connect();
        $response = array();
        if($link) {
                $obj_item = $this->data;
                unset($obj_item['cart_id']);
                $obj_item['user_id'] = $this->user_id;
                $keys = implode(",",array_keys($obj_item));
                $values = array_map(function($value) { return "'$value'"; }, array_values($obj_item));

                 $values = implode(",",$values);
                 $prod_name = $obj_item['product_name'];

                if($this->isCartProductExists($prod_name,$this->user_id)[Status] == Success) {
                    $query = "insert into wp_cart($keys) values($values)";
//                    echo $query;
                    $result = mysqli_query($link,$query);
                    if(!$result) {
                        $response[Status] = Error;
                        $response[Message] = "Unable to store data ".mysqli_error($link);
                        return $response;
                    }
                }
                else{
                    $response[Status] = Error;
                    $response[Message] = "Product already exists with this name";
                    return $response;
                }
            $response[Status] = Success;
            $response[Message] = "Data updated successfully";
            return $response;

        }

        $response[Status] = Error;
        $response[Message] = "Unable to connect with db ".mysqli_error($link);

        return $response;

    }
    /**
     *
     *
      in this function all data will be reterived from relevant table
     */

    public function getRecommends() {
        $link =  $this->link->connect();
        $response = array();
        if($link) {
            $query ='';
            if($this->user_id!=null)
             $query = "select * from wp_recommends where user_id='$this->user_id' order by cart_id desc";
            else{
                $query = "select * from wp_recommends where 1 order by cart_id desc";
            }

            $result = mysqli_query($link,$query);
            if($result) {
                $rows = mysqli_num_rows($result);
                if($rows>0) {
                    $data = array();
                    while($rowss = mysqli_fetch_assoc($result)) {
                        $data[] = $rowss;
                    }
                    $response[Status] = Success;
                    $response[Message] = "Product found";
                    $response["data"] = $data;

                    return $response;
                }
                $response[Status] = Error;
                $response[Message] = "No data found";
                return $response;

            }
            $response[Status] = Error;
            $response[Message] = "Unable to store data ".mysqli_error($link);
            return $response;


        }
        $response[Status] = Error;
        $response[Message] = "Unable to connect with db ".mysqli_error($link);
        return $response;
    }



    public function isProductExists($prod_name) {
        $link =  $this->link->connect();
        $response = array();
        if($link) {
          $query = "select * from wp_recommends where product_name='$prod_name' and user_id='$this->user_id'";
          $result = mysqli_query($link,$query);
          if($result) {
            $rows = mysqli_num_rows($result);
            if($rows>0) {
                $response[Status] = Error;
                $response[Message] = "Product already exists";
                return $response;
             }
              $response[Status] = Success;
              $response[Message] = "New product found";
              return $response;

          }
            $response[Status] = Error;
            $response[Message] = "Unable to store data ".mysqli_error($link);
            return $response;


        }
        $response[Status] = Error;
        $response[Message] = "Unable to connect with db ".mysqli_error($link);
        return $response;
    }

    public function isCartProductExists($prod_name,$user_id) {
        $link =  $this->link->connect();
        $response = array();
        if($link) {
            $query = "select * from wp_cart where product_name='$prod_name' and user_id = '$user_id'";
            $result = mysqli_query($link,$query);
            if($result) {
                $rows = mysqli_num_rows($result);
                if($rows>0) {
                    $response[Status] = Error;
                    $response[Message] = "Product already exists";
                    return $response;
                }
                $response[Status] = Success;
                $response[Message] = "New product found";
                return $response;

            }
            $response[Status] = Error;
            $response[Message] = "Unable to store data ".mysqli_error($link);
            return $response;


        }
        $response[Status] = Error;
        $response[Message] = "Unable to connect with db ".mysqli_error($link);
        return $response;
    }


    public function searchRecommends($text,$key) {
        $link =  $this->link->connect();
        $response = array();
        if($link) {

            $query = "select * from wp_recommends where product_name like '%$text%'";

            if($key === 'Shirt') {
                $query = "select * from wp_recommends where product_type ='Custom Shirt'";
            }
            else if($key === 'Jacket'){
                $query = "select * from wp_recommends where product_type ='Custom Suit'";
            }
            else if($key === 'Suit'){
                $query = "select * from wp_recommends where product_type ='Custom Suit' or product_type='Custom 2Pc Suit'";
            }
            else if($key === 'Pants') {
                $query = "select * from wp_recommends where product_type ='Custom Pant'";
            }
            else if($key === 'Vest') {
                $query = "select * from wp_recommends where product_type ='Custom Vest'";
            }
            else if($key === 'Overcoat') {
                $query = "select * from wp_recommends where product_type ='Collection'";
            }
            else if($key === 'Accessories') {
                $query = "select * from wp_recommends where product_name like '%$text%'";
            }

            $result = mysqli_query($link,$query);
            if($result) {
                $rows = mysqli_num_rows($result);
                if($rows>0) {
                    $data = array();
                    while($rowss = mysqli_fetch_assoc($result)) {
                        $data[] = $rowss;
                    }
                    $response[Status] = Success;
                    $response[Message] = "Product found";
                    $response["data"] = $data;
                    return $response;
                }
                $response[Status] = Error;
                $response[Message] = "No data found";
                return $response;

            }
            $response[Status] = Error;
            $response[Message] = "Unable to store data ".mysqli_error($link);
            return $response;


        }
        $response[Status] = Error;
        $response[Message] = "Unable to connect with db ".mysqli_error($link);
        return $response;
    }

}