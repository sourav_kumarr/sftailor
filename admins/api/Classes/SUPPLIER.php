<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');

class SUPPLIER
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    public function addSupplier($company_name, $company_address1, $company_address2, $company_work_phone, $company_mobile_no, $company_fax, $contact_first_name, $family_name, $contact_email)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into wp_supplier (company_name,company_address1,company_address2,company_work_phone,company_mobile_no,company_fax,contact_first_name,family_name,contact_email) VALUES ('$company_name','$company_address1','$company_address2','$company_work_phone','$company_mobile_no','$company_fax','$contact_first_name','$family_name','$contact_email')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Supplier Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function updateSupplier($company_name, $company_address1, $company_address2, $company_work_phone, $company_mobile_no, $company_fax, $contact_first_name, $family_name, $contact_email, $supplierId)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update wp_supplier set company_name='$company_name',company_address1='$company_address1',company_address2='$company_address2',company_work_phone='$company_work_phone',company_mobile_no='$company_mobile_no',company_fax='$company_fax',contact_first_name='$contact_first_name',family_name='$family_name',contact_email='$contact_email' where sup_id = '$supplierId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Supplier Details Updated SuccessFully";
                $this->response['catId'] = $cat_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function getSupplierDetails()
    {
        $catArray = array();
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from wp_supplier order by sup_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while ($supData = mysqli_fetch_array($result)) {
                        $supArray[] = array(
                            "sup_id" => $supData['sup_id'],
                            "company_name" => $supData['company_name'],
                            "company_address1" => $supData['company_address1'],
                            "company_address2" => $supData['company_address2'],
                            "company_work_phone" => $supData['company_work_phone'],
                            "company_mobile_no" => $supData['company_mobile_no'],
                            "company_fax" => $supData['company_fax'],
                            "contact_first_name" => $supData['contact_first_name'],
                            "family_name" => $supData['family_name'],
                            "contact_email" => $supData['contact_email']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['supplierData'] = $supArray;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Supplier Found";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function deleteSupplier($sup_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $update = mysqli_query($link, "delete from wp_supplier WHERE sup_id='$sup_id'");
            if ($update) {
                $row = mysqli_fetch_array($result);
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Supplier Has Been Deleted Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "UnAuthorized Access";
        }
        return $this->response;
    }

    public function sendMaillSupplier($supplierEmail, $orderid ,$admin_email,$add_cc,$message_text)
    {

        $link = $this->link->connect();
        $query2 = mysqli_query($link, "select * from wp_orders where order_id = '$orderid'");
        $row = mysqli_fetch_array($query2);
        $order_number = $row['order_number'];
        $order_id = $row['order_id'];
        $order_user_id = $row['order_user_id'];
        $ids = array();

        $query2 = mysqli_query($link, "select * from wp_customers where user_id = '$order_user_id'");
        $rowd = mysqli_fetch_array($query2);
        $contact_person = $rowd['name'];

        $query12 = mysqli_query($link, "select * from style_genie where user_id = '$order_user_id'");
        $rowr = mysqli_fetch_assoc($query12);
        $back_sholuder = $rowr['back_sholuder'];
        $shoulder_forward = $rowr['shoulder_forward'];
        $body_forward = $rowr['body_forward'];
        $shoulder_slope = $rowr['shoulder_slope'];
        $belly_shape = $rowr['belly_shape'];
        $style_arms = $rowr['style_arms'];
        $style_seat = $rowr['style_seat'];
        $shoulder_slope_left = $rowr['shoulder_slope_left'];
        $shoulder_slope_right = $rowr['shoulder_slope_right'];

        if ($order_number) {
            $query22 = mysqli_query($link, "select * from wp_orders_detail where det_order_id = '$order_id'");
            $orderDIV1 = "";
            $orderDIV2 = "";
            $orderDIV3 = "";
            $orderDIV4 = "";
            $access_cat = array();
            $prod_type = array();

            while ($row = mysqli_fetch_assoc($query22)) {
                $det_product_name = $row['det_product_name'];
                $product_type = trim($row['product_type']);
                $j_category = $row['j_category'];
                $j_frontbutton = $row['j_frontbutton'];
                $j_lapelstyle = $row['j_lapelstyle'];
                $j_lapelbuttonholes = $row['j_lapelbuttonholes'];
                $j_lapelbuttonholesstyle = $row['j_lapelbuttonholesstyle'];
                $j_lapelbuttonholesthread = $row['j_lapelbuttonholesthread'];
                $j_lapelingredient = $row['j_lapelingredient'];
                $j_lapelsatin = $row['j_lapelsatin'];
                $j_chestdart = $row['j_chestdart'];
                $j_feltcollar = explode(":",$row['j_feltcollar']);
                $j_feltcollar = $j_feltcollar['0'];
                $j_innerflap = $row['j_innerflap'];
                $j_facingstyle = $row['j_facingstyle'];
                $j_sleeveslit = $row['j_sleeveslit'];
                $j_sleeveslitthread = $row['j_sleeveslitthread'];
                $j_breastpocket = $row['j_breastpocket'];
                $j_lowerpocket = $row['j_lowerpocket'];
                $j_coinpocket = $row['j_coinpocket'];
                $j_backvent = $row['j_backvent'];
                $j_liningstyle = $row['j_liningstyle'];
                $j_liningoption = explode(":",$row['j_liningoption']);
                $j_liningoption = $j_liningoption['0'];
                $j_lining = explode(":",$row['j_lining']);
                $j_lining = $j_lining['0'];
                $j_shoulderstyle = $row['j_shoulderstyle'];
                $j_shoulderpadding = $row['j_shoulderpadding'];
                $j_buttonoption = $row['j_buttonoption'];
                $j_buttonswatch = $row['j_buttonswatch'];
                $j_threadoption = $row['j_threadoption'];
                $j_elbowpatch = $row['j_elbowpatch'];
                $j_elbowpatchcolor = explode(":",$row['j_elbowpatchcolor']);
                $j_elbowpatchcolor = $j_elbowpatchcolor['0'];
                $j_canvasoption = $row['j_canvasoption'];
                $j_suitcolor = explode(":",$row['j_suitcolor']);
                $j_suitcolor = $j_suitcolor['0'];
                $j_shirt = $row['j_shirt'];
                $j_tie = $row['j_tie'];
                $j_jacket = $row['j_jacket'];
                $j_armbutton = $row['j_armbutton'];
                $j_sidearmbutton = $row['j_sidearmbutton'];
                $j_sidearmbuttoncut = $row['j_sidearmbuttoncut'];
                $j_sidearmbuttonpoint = $row['j_sidearmbuttonpoint'];
                $p_category = $row['p_category'];
                $p_frontpocket = $row['p_frontpocket'];
                $p_pleatstyle = $row['p_pleatstyle'];
                $p_watchpocket = $row['p_watchpocket'];
                $p_backpocket = $row['p_backpocket'];
                $p_backpocketsatin = $row['p_backpocketsatin'];
                $p_belt = $row['p_belt'];
                $p_pleat = $row['p_pleat'];
                $p_beltloop = $row['p_beltloop'];
                $p_pantbottom = $row['p_pantbottom'];
                $p_pantcolor = $row['p_pantcolor'];
                $o_category = $row['o_category'];
                $o_canvasstyle = $row['o_canvasstyle'];
                $o_lapelstyle = $row['o_lapelstyle'];
                $o_pocketstype = $row['o_pocketstype'];
                $o_buttonstyle = $row['o_buttonstyle'];
                $o_sweatpadstyle = $row['o_sweatpadstyle'];
                $o_elbowpadstyle = $row['o_elbowpadstyle'];
                $o_shoulderstyle = $row['o_shoulderstyle'];
                $o_shoulderpatchstyle = $row['o_shoulderpatchstyle'];
                $o_shouldertabstyle = $row['o_shouldertabstyle'];
                $o_sleeveslitstyle = $row['o_sleeveslitstyle'];
                $o_sleevebuttonstyle = $row['o_sleevebuttonstyle'];
                $o_backvent = $row['o_backvent'];
                $o_stitchstyle = $row['o_stitchstyle'];
                $o_backsideofarmhole = $row['o_backsideofarmhole'];
                $o_topstichstyle = $row['o_topstichstyle'];
                $o_breastpocket = $row['o_breastpocket'];
                $o_facingstyle = $row['o_facingstyle'];
                $o_pocketstyle = $row['o_pocketstyle'];
                $o_collarstyle = $row['o_collarstyle'];
                $o_liningstyle = $row['o_liningstyle'];
                $s_category = $row['s_category'];
                $s_collar = $row['s_collar'];
                $s_collarbutton = $row['s_collarbutton'];
                $s_collarbuttondown = $row['s_collarbuttondown'];
                $s_collarlayeroption = $row['s_collarlayeroption'];
                $s_frontcollar = $row['s_frontcollar'];
                $s_collarbelt = $row['s_collarbelt'];
                $s_cuff = $row['s_cuff'];
                $s_cuffwidth = $row['s_cuffwidth'];
                $s_placket = $row['s_placket'];
                $s_placketbutton = $row['s_placketbutton'];
                $s_pocket = $row['s_pocket'];
                $s_pleat = $row['s_pleat'];
                $s_bottom = $row['s_bottom'];
                $s_shirtcolor = $row['s_shirtcolor'];
                $s_buttoncolor = $row['s_buttoncolor'];
                $s_buttoningstyle = $row['s_buttoningstyle'];
                $s_buttoncutthread = $row['s_buttoncutthread'];
                $s_holestichthread = $row['s_holestichthread'];
                $s_monogramtext = $row['s_monogramtext'];
                $s_monogramfont = $row['s_monogramfont'];
                $s_monogramcolor = $row['s_monogramcolor'];
                $s_monogramposition = $row['s_monogramposition'];
                $s_monogramoption = $row['s_monogramoption'];
                $s_monogramcode = $row['s_monogramcode'];
                $s_contrastfabric = $row['s_contrastfabric'];
                $s_contrast = $row['s_contrast'];
                $s_contrastposition = $row['s_contrastposition'];
                $s_sleeve = $row['s_sleeve'];
                $s_shoulder = $row['s_shoulder'];
                $a_category = $row['a_category'];
                $a_specification = $row['a_specification'];
                $a_rc_no = $row['a_rc_no'];
                $v_category = $row['v_category'];
                $v_collarstyle = $row['v_collarstyle'];
                $v_lapelbuttonhole = $row['v_lapelbuttonhole'];
                $v_buttonholestyle = $row['v_buttonholestyle'];
                $v_buttonstyle = $row['v_buttonstyle'];
                $v_bottomstyle = $row['v_bottomstyle'];
                $v_lowerpocket = $row['v_lowerpocket'];
                $v_ticketpocket = $row['v_ticketpocket'];
                $v_backstyle = $row['v_backstyle'];
                $v_chestdart = $row['v_chestdart'];
                $v_topstitch = $row['v_topstitch'];
                $v_placketstyle = $row['v_placketstyle'];
                $v_tuxedostyle = $row['v_tuxedostyle'];
                $v_breastpocket = $row['v_breastpocket'];
                $v_chestpocket = $row['v_chestpocket'];
                $v_vestfabric = $row['v_vestfabric'];
                $access_category = $row['access_category'];
                if ($product_type == "Accessories" && ($access_category != "bow_tie_two" && $access_category != "neck_tie_two")) {
                    $orderDIV_bow_tie_one = '<div style="margin-bottom: 20px;margin-top: 20px;"><img src="http://scan2fit.com/sftailor/images/logo/logo.png"> <span style="font-weight: 900; margin-left: 14px;">ORDER DETAILS FOR ACCESSORIES</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">RC No.</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $a_rc_no . '</td></tr></table>';

                }
                if ($access_category == "bow_tie_two" || $access_category == "neck_tie_two") {
                    $orderDIV_bow_tie_two = '<div style="margin-bottom: 20px;margin-top: 20px;"><img src="http://scan2fit.com/sftailor/images/logo/logo.png"> <span style="font-weight: 900; margin-left: 14px;">ORDER DETAILS FOR ACCESSORIES</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">RC No.</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $a_rc_no . '</td></tr></table>';
                }
                if ($access_category != '')
                    $access_cat[] = $access_category;
                if ($product_type != '')
                    $prod_type[] = $product_type;


                $meas_id = $row['meas_id'];
                $query24 = mysqli_query($link, "select * from final_measurements where measurement_id = '$meas_id'");
                while ($roww = mysqli_fetch_array($query24)) {
                    $shirt_collar = $roww['shirt_collar'];
                    $jacket_collar = $roww['jacket_collar'];
                    $vest_collar = $roww['vest_collar'];
                    $pant_collar = $roww['pant_collar'];
                    $shirt_chest = $roww['shirt_chest'];
                    $jacket_chest = $roww['jacket_chest'];
                    $vest_chest = $roww['vest_chest'];
                    $pant_chest = $roww['pant_chest'];
                    $shirt_stomach = $roww['shirt_stomach'];
                    $jacket_stomach = $roww['jacket_stomach'];
                    $vest_stomach = $roww['vest_stomach'];
                    $pant_stomach = $roww['pant_stomach'];
                    $shirt_seat = $roww['shirt_seat'];
                    $jacket_seat = $roww['jacket_seat'];
                    $vest_seat = $roww['vest_seat'];
                    $pant_seat = $roww['pant_seat'];
                    $shirt_bicep = $roww['shirt_bicep'];
                    $jacket_bicep = $roww['jacket_bicep'];
                    $vest_bicep = $roww['vest_bicep'];
                    $pant_bicep = $roww['pant_bicep'];
                    $shirt_back_shoulder = $roww['shirt_back_shoulder'];
                    $jacket_back_shoulder = $roww['jacket_back_shoulder'];
                    $vest_back_shoulder = $roww['vest_back_shoulder'];
                    $pant_back_shoulder = $roww['pant_back_shoulder'];
                    $shirt_front_shoulder = $roww['shirt_front_shoulder'];
                    $jacket_front_shoulder = $roww['jacket_front_shoulder'];
                    $vest_front_shoulder = $roww['vest_front_shoulder'];
                    $pant_front_shoulder = $roww['pant_front_shoulder'];
                    $shirt_sleeve_left = $roww['shirt_sleeve_left'];
                    $jacket_sleeve_left = $roww['jacket_sleeve_left'];
                    $vest_sleeve_left = $roww['vest_sleeve_left'];
                    $pant_sleeve_left = $roww['pant_sleeve_left'];
                    $shirt_sleeve_right = $roww['shirt_sleeve_right'];
                    $jacket_sleeve_right = $roww['jacket_sleeve_right'];
                    $vest_sleeve_right = $roww['vest_sleeve_right'];
                    $pant_sleeve_right = $roww['pant_sleeve_right'];
                    $shirt_thigh = $roww['shirt_thigh'];
                    $jacket_thigh = $roww['jacket_thigh'];
                    $vest_thigh = $roww['pant_thigh'];
                    $pant_thigh = $roww['pant_thigh'];
                    $shirt_nape_to_waist = $roww['shirt_nape_to_waist'];
                    $jacket_nape_to_waist = $roww['jacket_nape_to_waist'];
                    $vest_nape_to_waist = $roww['vest_nape_to_waist'];
                    $pant_nape_to_waist = $roww['pant_nape_to_waist'];
                    $shirt_front_waist_length = $roww['shirt_front_waist_length'];
                    $jacket_front_waist_length = $roww['jacket_front_waist_length'];
                    $vest_front_waist_length = $roww['vest_front_waist_length'];
                    $pant_front_waist_length = $roww['pant_front_waist_length'];
                    $shirt_wrist = $roww['shirt_wrist'];
                    $jacket_wrist = $roww['jacket_wrist'];
                    $vest_wrist = $roww['vest_wrist'];
                    $pant_wrist = $roww['pant_wrist'];
                    $shirt_waist = $roww['shirt_waist'];
                    $pant_waist = $roww['pant_waist'];
                    $jacket_waist = $roww['jacket_waist'];
                    $vest_waist = $roww['vest_waist'];
                    $shirt_calf = $roww['shirt_calf'];
                    $jacket_calf = $roww['jacket_calf'];
                    $vest_calf = $roww['vest_calf'];
                    $pant_calf = $roww['pant_calf'];
                    $shirt_back_waist_height = $roww['shirt_back_waist_height'];
                    $jacket_back_waist_height = $roww['jacket_back_waist_height'];
                    $vest_back_waist_height = $roww['vest_back_waist_height'];
                    $pant_back_waist_height = $roww['pant_back_waist_height'];
                    $shirt_front_waist_height = $roww['shirt_front_waist_height'];
                    $jacket_front_waist_height = $roww['jacket_front_waist_height'];
                    $vest_front_waist_height = $roww['vest_front_waist_height'];
                    $pant_front_waist_height = $roww['pant_front_waist_height'];
                    $shirt_knee = $roww['shirt_knee'];
                    $jacket_knee = $roww['jacket_knee'];
                    $vest_knee = $roww['vest_knee'];
                    $pant_knee = $roww['pant_knee'];
                    $shirt_back_jacket_length = $roww['shirt_back_jacket_length'];
                    $jacket_back_jacket_length = $roww['jacket_back_jacket_length'];
                    $vest_back_jacket_length = $roww['vest_back_jacket_length'];
                    $pant_back_jacket_length = $roww['pant_back_jacket_length'];
                    $shirt_u_rise = $roww['shirt_u_rise'];
                    $jacket_u_rise = $roww['jacket_u_rise'];
                    $vest_u_rise = $roww['vest_u_rise'];
                    $pant_u_rise = $roww['pant_u_rise'];
                    $shirt_pant_left_outseam = $roww['shirt_pant_left_outseam'];
                    $jacket_pant_left_outseam = $roww['jacket_pant_left_outseam'];
                    $vest_pant_left_outseam = $roww['vest_pant_left_outseam'];
                    $pant_pant_left_outseam = $roww['pant_pant_left_outseam'];
                    $shirt_pant_right_outseam = $roww['shirt_pant_right_outseam'];
                    $jacket_pant_right_outseam = $roww['jacket_pant_right_outseam'];
                    $vest_pant_right_outseam = $roww['vest_pant_right_outseam'];
                    $pant_pant_right_outseam = $roww['pant_pant_right_outseam'];
                    $shirt_bottom = $roww['shirt_bottom'];
                    $jacket_bottom = $roww['jacket_bottom'];
                    $vest_bottom = $roww['vest_bottom'];
                    $pant_bottom = $roww['pant_bottom'];
                    $meas_name = $roww['meas_name'];
                    $user_id = $roww['user_id'];
                    $unit_type = $roww['unit_type'];
                    $added_on = $roww['added_on'];

                    if ($product_type == "Custom Suit") {
                        $orderDIV_suit = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR JACKET</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Made In China Label</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">088E no made in china label in jacket</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Front  Button</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_frontbutton . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel Style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button holes</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelbuttonholes . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button holes style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelbuttonholesstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button holes thread</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelbuttonholesthread . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel ingredient</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelingredient . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel satin</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelsatin . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Chest dart</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_chestdart . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Felt collar</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_feltcollar . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Inner flap</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_innerflap . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Facing style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_facingstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve slit</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_sleeveslit . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve slit thread</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_sleeveslitthread . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Breast pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_breastpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lower pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lowerpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Coin pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_coinpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back vent</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_backvent . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_liningstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_liningoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lining . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_shoulderstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder padding</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_shoulderpadding . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_buttonoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Buttons watch</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_buttonswatch . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Thread option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_threadoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Elbow patch</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_elbowpatch . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Elbow patch color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_elbowpatchcolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Canvas option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_canvasoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Suit color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_suitcolor . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Chest</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_chest . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Waist Horizontal Line (stomach)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_stomach . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_seat . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Bicep</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_bicep . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_back_shoulder . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Front Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_front_shoulder . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Sleeve length (left)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_sleeve_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Sleeve length (right)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_sleeve_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;"> 	Back Jacket Length</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_back_jacket_length . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Meas Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $meas_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Unit Type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $unit_type . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                    } elseif ($product_type == "Custom Pant") {

                        $orderDIV_pant = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR PANT</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Made In China Label</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">3662 no made in china label in pants</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Front pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_frontpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pleat style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_pleatstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Watch pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_watchpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_backpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back pocket Satin</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_backpocketsatin . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Belt loop</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_beltloop . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pant bottom</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_pantbottom . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pant color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_pantcolor . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_seat . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Thigh</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_thigh . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Waist</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_waist . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Calf</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_calf . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Waist Height</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_back_waist_height . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Front Waist Height</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_front_waist_height . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Knee</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_knee . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">U-rise</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_u_rise . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;"> 	Pant Left Outseam</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_pant_left_outseam . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Pant Right Outseam</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_pant_right_outseam . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Bottom</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_bottom . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Meas Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $meas_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Unit Type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $unit_type . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                    } elseif ($product_type == "Custom Shirt") {
                        $orderDIV_shirt = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR SHIRT</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Made In China Label</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">58S9 no made in china label in shirt</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_collar . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar button</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_collarbutton . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar button down</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_collarbuttondown . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar layer option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_collarlayeroption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Front collar</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_frontcollar . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar belt</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_collarbelt . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Cuff</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_cuff . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Cuff width</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_cuffwidth . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Placket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_placket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Placket button</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_placketbutton . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_pocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pleat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_pleat . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Bottom</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_bottom . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shirt color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_shirtcolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_buttoncolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Buttoning style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_buttoningstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button cut thread</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_buttoncutthread . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Hole stich thread</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_holestichthread . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram text</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramtext . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Code</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramcode . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram font</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramfont . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramcolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram position</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramposition . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Contrast fabric</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_contrastfabric . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Contrast</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_contrast . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Contrast position</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_contrastposition . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_sleeve . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_shoulder . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Collar</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_collar . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Chest</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_chest . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Waist Horizontal Line (stomach)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_stomach . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_seat . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Bicep</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_bicep . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_back_shoulder . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Front Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_front_shoulder . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Sleeve length (right)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_sleeve_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;"> 	Sleeve length (left)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_sleeve_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Jacket Length</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shirt_back_jacket_length . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Meas Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $meas_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Unit Type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $unit_type . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                    } elseif ($product_type == "Accessories" && ($access_category != "bow_tie_two" && $access_category != "neck_tie_two")) {

                        $orderDIV_bow_tie_one = '<div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">ORDER DETAILS FOR ACCESSORIES</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">RC No.</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $a_rc_no . '</td></tr></table>';

                    } elseif ($access_category == "bow_tie_two" || $access_category == "neck_tie_two") {
                        $orderDIV_bow_tie_two = '<div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">ORDER DETAILS FOR ACCESSORIES</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">RC No.</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $a_rc_no . '</td></tr></table>';
                    } elseif ($product_type == "Custom 2Pc Suit") {
                        $orderDIV_2pc_suit = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR JACKET 2PC SUIT</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Made In China Label</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">088E no made in china label in 2Pc Suit</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Front  Button</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_frontbutton . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel Style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button holes</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelbuttonholes . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button holes style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelbuttonholesstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button holes thread</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelbuttonholesthread . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel ingredient</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelingredient . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel satin</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lapelsatin . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Chest dart</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_chestdart . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Felt collar</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_feltcollar . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Inner flap</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_innerflap . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Facing style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_facingstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve slit</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_sleeveslit . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve slit thread</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_sleeveslitthread . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Breast pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_breastpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lower pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lowerpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Coin pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_coinpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back vent</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_backvent . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_liningstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_liningoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_lining . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_shoulderstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder padding</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_shoulderpadding . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_buttonoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Buttons watch</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_buttonswatch . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Thread option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_threadoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Elbow patch</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_elbowpatch . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Elbow patch color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_elbowpatchcolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Canvas option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_canvasoption . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Suit color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $j_suitcolor . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Chest</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_chest . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Waist Horizontal Line (stomach)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_stomach . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_seat . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Bicep</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_bicep . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_back_shoulder . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Front Shoulder</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_front_shoulder . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Sleeve length (left)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_sleeve_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Sleeve length (right)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_sleeve_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;"> 	Back Jacket Length</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $jacket_back_jacket_length . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Meas Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $meas_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Unit Type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $unit_type . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                        $orderDIV_2pc_pant = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR PANT</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Front pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_frontpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pleat style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_pleatstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Watch pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_watchpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_backpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back pocket Satin</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_backpocketsatin . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Belt loop</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_beltloop . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pant bottom</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_pantbottom . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pant color</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $p_pantcolor . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_seat . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Thigh</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_thigh . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Waist</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_waist . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Calf</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_calf . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Waist Height</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_back_waist_height . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Front Waist Height</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_front_waist_height . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Knee</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_knee . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">U-rise</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_u_rise . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;"> Pant Left Outseam</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_pant_left_outseam . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Pant Right Outseam</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_pant_right_outseam . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Bottom</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $pant_bottom . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Meas Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $meas_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Unit Type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $unit_type . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                    } elseif ($product_type == "Custom Vest") {

                        $orderDIV_vest = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR VEST</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Made In China Label</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">4219 no made in china label in vest</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_collarstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel button hole</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_lapelbuttonhole . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button hole style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_buttonholestyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_buttonstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lower pocket style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_lowerpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Ticket pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_ticketpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Chest dart</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_chestdart . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Top stitch</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_topstitch . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Placket style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_placketstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Tuxedostyle</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_tuxedostyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Breast pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_breastpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Chest pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_chestpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Vest fabric</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $v_vestfabric . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Chest</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $vest_chest . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Waist Horizontal Line (stomach)</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $vest_stomach . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Back Jacket Length</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $vest_back_jacket_length . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Meas Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $meas_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Unit Type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $unit_type . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                    } elseif ($product_type == "Custom Overcoat") {

                        $orderDIV_overcoat = '<div style="margin-bottom: 20px; margin-top: 25px;"><span style="font-weight: 900; margin-left: 14px;">ORDER ITEMS FOR OVERCOAT</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Category</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_category . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Canvas style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_canvasstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lapel style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_lapelstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pockets type</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_pocketstype . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Button style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_buttonstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sweat pad style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_sweatpadstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Elbow pad style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_elbowpadstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_shoulderstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder patch style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_shoulderpatchstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Shoulder tab style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_shouldertabstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve slit style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_sleeveslitstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Sleeve button style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_sleevebuttonstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back vent</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_backvent . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Stitch style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_stitchstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Back side of arm hole</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_backsideofarmhole . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Top stich style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_topstichstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Breast pocket</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_breastpocket . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Facing style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_facingstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Pocket style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_pocketstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Collar style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_collarstyle . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Lining style</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $o_liningstyle . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">FINISHED MEASUREMENTS</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Order No</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Product</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $det_product_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;font-weight: bold;border-right:none;">STYLE GENIE</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;"></td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Left</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_left . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Right</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope_right . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Body Forward</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $body_forward . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Shoulder Slope</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $shoulder_slope . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Belly Shape</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $belly_shape . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Style Arms</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_arms . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Seat</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $style_seat . '</td></tr></table>';

                    }

                }

            }

            require 'SMTP/PHPMailerAutoload.php';
            $supQuery = mysqli_query($link, "select * from wp_supplier");
            if ($supplierEmail == "null") {
                while ($rows = mysqli_fetch_array($supQuery)) {
                    $supId = $rows['sup_id'];
                    $email_idd = $rows['contact_email'];
                    $company_name = $rows['company_name'];
                    $company_address1 = $rows['company_address1'];
                    $company_address2 = $rows['company_address2'];
                    $company_work_phone = $rows['company_work_phone'];
                    $company_mobile_no = $rows['company_mobile_no'];
                    $contact_first_name = $rows['contact_first_name'];
                    $family_name = $rows['family_name'];

                    if ($supId == "2" && (in_array("bow_tie_two", $access_cat) || in_array("neck_tie_two", $access_cat))) {
                        mysqli_query($link, "insert into wp_crm_emails (email_person,email_time,email_subject,order_id) VALUES('$email_idd',NOW(),'$message_text','$orderid')");
                        $mail = new \PHPMailer();
                        $mail->isSMTP();
                        $mail->Host = 'scan2fit.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = 'scan2fitsftailor@scan2fit.com';
                        $mail->Password = 'sftailor';
                        $mail->SMTPSecure = 'TLS';
                        $mail->Port = 587;
                        $mail->setFrom('noreply@sftailor.com', 'SFTailor');   // sender
                        $mail->addAddress($email_idd);
                        $mail->isHTML(true);
                        $mail->Subject = 'noreply@SFTailor';
                        $mail->Body = '<div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">SUPPLIER INFORMATION</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Supplier Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $company_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Address</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_address2 . ',' . $company_address2 . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Person Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $contact_person . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Email</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $email_idd . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Phone Number</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_mobile_no . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">SHIP TO ADDRESS </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Address</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">SF Tailors 272 Hogans, Valley Way, Cary NC 27513 USA</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">CUSTOMER </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Name customer</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $contact_person . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Customer order</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr></table>'.$orderDIV_bow_tie_two;
                        $mail->AltBody = '';
                        if (!$mail->send()) {
                            return $mail->ErrorInfo;
                        } else {
                            mysqli_query($link, "update wp_orders set email_status='1' where order_id = '$orderid'");

                            // echo "email sent";
                        }

                    } elseif ($supId == "3" && (in_array("Custom Overcoat", $prod_type) || in_array("Custom Suit", $prod_type) || in_array("Custom Shirt", $prod_type) || in_array("Custom Vest", $prod_type) || in_array("Custom 2Pc Suit", $prod_type) || in_array("Custom Pant", $prod_type) || in_array("bow_tie_one", $prod_type) || in_array("neck_tie_one", $prod_type) || in_array("pocket_square", $prod_type) || in_array("tie_bar", $prod_type))) {
                        mysqli_query($link, "insert into wp_crm_emails (email_person,email_time,email_subject,order_id) VALUES('$email_idd',NOW(),'$message_text','$orderid')");
                        $mail = new \PHPMailer();
                        $mail->isSMTP();
                        $mail->Host = 'scan2fit.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = 'scan2fitsftailor@scan2fit.com';
                        $mail->Password = 'sftailor';
                        $mail->SMTPSecure = 'TLS';
                        $mail->Port = 587;
                        $mail->setFrom('noreply@sftailor.com', 'SFTailor');   // sender
                        $mail->addAddress($email_idd);
                        $mail->isHTML(true);
                        $mail->Subject = 'noreply@SFTailor';
                        $mail->Body = '<div style="margin-bottom: 20px;margin-top: 20px;"><img src="http://scan2fit.com/sftailor/images/logo/logo.png"><span style="font-weight: 900; margin-left: 14px;">SUPPLIER INFORMATION</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Name Company</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $company_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Contact Person</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_mobile_no . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Address</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_address2 . ',' . $company_address2 . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">City</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">Jimo Qingdao</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Country</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">CHINA</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Phone Number</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_mobile_no . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Email</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $email_idd . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">SHIP TO ADDRESS </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Address</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">SF Tailors 272 Hogans, Valley Way, Cary NC 27513 USA</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">CUSTOMER </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Name customer</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $contact_person . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Customer order</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr></table>'.$orderDIV_2pc_pant . $orderDIV_2pc_suit . $orderDIV_overcoat . $orderDIV_shirt . $orderDIV_vest . $orderDIV_pant . $orderDIV_suit . $orderDIV_bow_tie_one.'<div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">Monogram </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Text</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramtext . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Code </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramcode . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Font </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramfont . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Color </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramcolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Position </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramposition . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramoption . '</td></tr></table>';
                        $mail->AltBody = '';
                        if (!$mail->send()) {
                            return $mail->ErrorInfo;
                        } else {
                            mysqli_query($link, "update wp_orders set email_status='1' where order_id = '$orderid'");

                            //echo "email sent";
                        }
                    } elseif ($supId == "3" && (in_array("bow_tie_one", $access_cat) || in_array("neck_tie_one", $access_cat) || in_array("pocket_square", $access_cat) || in_array("tie_bar", $access_cat))) {
                        mysqli_query($link, "insert into wp_crm_emails (email_person,email_time,email_subject,order_id) VALUES('$email_idd',NOW(),'$message_text','$orderid')");
                        $mail = new \PHPMailer();
                        $mail->isSMTP();
                        $mail->Host = 'scan2fit.com';
                        $mail->SMTPAuth = true;
                        $mail->Username = 'scan2fitsftailor@scan2fit.com';
                        $mail->Password = 'sftailor';
                        $mail->SMTPSecure = 'TLS';
                        $mail->Port = 587;
                        $mail->setFrom('noreply@sftailor.com', 'SFTailor');   // sender
                        $mail->addAddress($email_idd);
                        $mail->isHTML(true);
                        $mail->Subject = 'noreply@SFTailor';
                        $mail->Body = '<div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">SUPPLIER INFORMATION</span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Supplier Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $company_name . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Address</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_address2 . ',' . $company_address2 . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Person Name</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $contact_person . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Email</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $email_idd . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;border-top:none;">Phone Number</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;border-top:none;">' . $company_mobile_no . '</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">SHIP TO ADDRESS </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Address</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">SF Tailors 272 Hogans, Valley Way, Cary NC 27513 USA</td></tr></table><div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">CUSTOMER </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Name customer</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $contact_person . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Customer order</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $order_number . '</td></tr></table>'.$orderDIV_bow_tie_one;
                        $mail->AltBody = '';
                        if (!$mail->send()) {
                            return $mail->ErrorInfo;
                        } else {
                            mysqli_query($link, "update wp_orders set email_status='1' where order_id = '$orderid'");

                            // echo "email sent";
                        }
                    }
                }
            } else {
                mysqli_query($link, "insert into wp_crm_emails (email_person,email_time,email_subject,order_id) VALUES ('$supplierEmail',NOW(),'$message_text','$orderid')");
                $mail = new \PHPMailer();
                $mail->isSMTP();
                $mail->Host = 'scan2fit.com';
                $mail->SMTPAuth = true;
                $mail->Username = 'scan2fitsftailor@scan2fit.com';
                $mail->Password = 'sftailor';
                $mail->SMTPSecure = 'TLS';
                $mail->Port = 587;
                $mail->setFrom('noreply@sftailor.com', 'SFTailor');
                //for to addresses
                $exp = explode(",",$supplierEmail);
                for($k =0;$k<count($exp);$k++){
                    $supplierEmail = $exp[$k];
                    $mail->addAddress($supplierEmail);
                }
                //for cc
                $exp1 = explode(",",$add_cc);
                for($k =0;$k<count($exp1);$k++){
                    $add_cc = $exp1[$k];
                    $mail->AddCC($add_cc);
                }
                //for admin mail
                $mail->addAddress($admin_email);
                $mail->isHTML(true);
                $mail->Subject = 'noreply@SFTailor';
                $mail->Body = '<div style="margin-bottom: 20px;margin-top: 20px;font-weight: bold;font-style: italic;">'.$message_text.'</div>'.$orderDIV_2pc_pant . $orderDIV_2pc_suit . $orderDIV_overcoat . $orderDIV_shirt . $orderDIV_vest . $orderDIV_pant . $orderDIV_suit . $orderDIV_bow_tie_one . $orderDIV_bow_tie_two.'<div style="margin-bottom: 20px;margin-top: 20px;"><span style="font-weight: 900; margin-left: 14px;">Monogram </span></div><table cellpadding="0" cellspacing="0" width="600" bgcolor="#eee"><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Text</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramtext . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Code </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramcode . '</td></tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Font </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramfont . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Color </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramcolor . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Position </td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramposition . '</td></tr><tr><td style="border: 1px solid rgb(153, 153, 153); padding: 3px 3px 3px 13px; width: 200px;">Monogram Option</td><td style="border:1px solid #999;border-left:none;padding-left: 25px;">' . $s_monogramoption . '</td></tr></table>';
                $mail->AltBody = '';
                if (!$mail->send()) {
                    return $mail->ErrorInfo;
                } else {
                    mysqli_query($link, "update wp_orders set email_status='1' where order_id = '$orderid'");
                    // echo "email sent 1";
                }
            }
        }

        return $this->response;
    }


    public function updateSalesTerms($editor1)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update sales_terms set message='$editor1' where terms_id = '1'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Sales terms Details Updated SuccessFully";
                $this->response['catId'] = $cat_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function updateNewEvent($event_id,$updateevent)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update wp_crm_activities set related_to='$updateevent' where c_id = '$event_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Updated SuccessFully";
                $this->response['catId'] = $event_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }


    public function getTermsData(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from sales_terms";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while($rows = mysqli_fetch_array($result)){
                        $termsData[]=array(
                            "terms_id"=>$rows["terms_id"],
                            "message"=>$rows["message"]
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "message Found";
                    $this->response['termsData'] = $termsData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function createTask($newtask_assing,$newtask_subject,$newtask_duedate,$newtask_comment,$related,$activity_type,$user_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into wp_crm_activities (assign_to,subject,due_date,comment,related_to,created_date,activity_type,user_id)            VALUES ('$newtask_assing','$newtask_subject','$newtask_duedate','$newtask_comment','$related',NOW(),'$activity_type',                 '$user_id')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Event Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function createTaskEmail($newtask_subject,$newtask_comment,$related,$activity_type,$user_id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "insert into wp_crm_activities (due_date,comment,related_to,created_date,activity_type,user_id)                             VALUES ('$newtask_subject','$newtask_comment','$related',NOW(),'$activity_type','$user_id')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Event Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function createEmailTask($email_to,$email_cc,$email_bcc,$email_subject,$email_comment,$email_related,$activity_type,$user_id,$time_zone_offset)
    {
        session_start();
        $setFrom = $_SESSION['SFTAdminEmail'];
        $set = explode("@",$setFrom);
        $set_from = $set[0];
        $link = $this->link->connect();
        require 'SMTP/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'scan2fit.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'scan2fitsftailor@scan2fit.com';
        $mail->Password = 'sftailor';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom($set_from.'@sftailor.com', 'SFTailor');
        $mail->addAddress($email_to);
        $mail->AddCC($email_cc);
        $mail->AddBCC($email_bcc);
        $mail->isHTML(true);
        $mail->Subject =$email_subject;
        $mail->Body = $email_comment;
        if (!$mail->send()) {
            return $mail->ErrorInfo;
        } else {
            $timezone_offset_minutes = $time_zone_offset;
            $timezone_name = timezone_name_from_abbr("", $timezone_offset_minutes*60, false);
            date_default_timezone_set($timezone_name);
            $date_time =  date("Y-m-d h:i:s");
            $email_comments =  str_replace("'", ' ', $email_comment);
            mysqli_query ($link, "insert into wp_crm_activities (assign_to,subject,due_date,comment,related_to,
            created_date,activity_type,user_id) VALUES ('$email_to','$email_bcc','$email_subject','$email_comments',
            '$email_related','$date_time','$activity_type','$user_id')");
            // echo "sent";
        }
        return $this->response;
    }

    public function deleteActivity($id)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "delete from wp_crm_activities where c_id='$id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "email send SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function addevent($newevent,$activity_type)
    {
        $link = $this->link->connect();
        if ($link) {

            $query = "insert into wp_crm_activities (assign_to,subject,due_date,comment,related_to,created_date,activity_type) VALUES ('','','','','$newevent',NOW(),'$activity_type')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Event Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}