<?php
include "header.php";
include "api/Constants/dbConfig.php";
include "api/Classes/CONNECT.php";
?>
    <style>
        .headingstartshirt{
            font-size: 19px;
            font-weight: bold;
            margin-bottom: 20px;
            margin-left: 15px;
            text-align: left;
            text-decoration: underline;
        }
        .col-md-6 > li {
            font-size: 18px;
            list-style: square;
        }
    </style>
    <?php
    $conn = new \Modals\CONNECT();
    $link = $conn->Connect();
    ?>
    <div class="col-md-12" style="margin-top:150px;text-align:center;">
        <p style="font-size:30px">sftailor's Custom-Made Trousers </p>
        <hr>
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <p class="headingstartshirt">CUSTOM Trousers</p>
            <div class="col-md-6">
                <label style="text-align:left">We begin with top tier fabrics as they tailor distinctly better.
                    The result is a more elegant pants with a notable difference in ease of movement, durability and a
                    cleaner silhouette.<br><br>Our pants offer a sharp silhouette and elegant style at best in class
                    pricing. We only use genuine and high-quality fabric's, linings, pleats, bottoms and
                    stitching.<br><br>Each pant comes with a complementary custom fitting.<br><br>
                    Turnaround time is 2-3 weeks.
                </label>
            </div>
            <div class="col-md-6">
                <img src="images/pant.jpg" class="img-responsive" />
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="col-md-12" style="margin-bottom:50px">
        <hr>
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <p class="headingstartshirt">RETAIL PRICE</p>
            <div class='col-md-12'>
                <?php
                $query = mysqli_query($link,"select * from categories where cat_type = 'Pants'");
                if($query){
                    $rows = mysqli_fetch_array($query);
                    echo "<label>".$rows['cat_description']."</label><br>";
                    echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                }
                ?>
                <hr>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories");
                if($query){
                    while($rows = mysqli_fetch_array($query)){
                        if($rows['cat_type'] == "Suits2pc") {
                            echo "<label>".$rows['cat_description']."</label><br>";
                            echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                        }
                    }
                }
                ?>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories");
                if($query){
                    while($rows = mysqli_fetch_array($query)){
                        if($rows['cat_type'] == "Suits3pc") {
                            echo "<label>".$rows['cat_description']."</label><br>";
                            echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                        }
                    }
                }
                ?>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories");
                if($query){
                    while($rows = mysqli_fetch_array($query)){
                        if($rows['cat_type'] == "Jacket") {
                            echo "<label>".$rows['cat_description']."</label><br>";
                            echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                        }
                    }
                }
                ?>
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
    <div class="col-md-12" style="margin-bottom:50px">
        <hr>
        <div class="col-md-1"></div>
        <div class="col-md-10">
            <p class="headingstartshirt">Specifications</p>
            <div class="col-md-8">
                <div class="col-md-6">
                    <li>Hand finished pockets</li>
                    <li>Milanese front buttonhole</li>
                    <li>Custom cut and pleat</li>
                    <li>Working buttonholes</li>
                </div>
                <div class="col-md-6">
                    <li>Signature semi-roped cut</li>
                    <li>Natural waist suppression</li>
                    <li>Hand sewn armholes</li>
                    <li>Hand pressing</li>
                </div>
            </div>
            <div class="col-md-4">
                <input type="button" value="Order Custom Pants >>" onclick="window.location='custom_pants.php'" class="btn btn-danger btn-lg" style="margin:50px" />
            </div>
        </div>
        <div class="col-md-1"></div>
    </div>
<?php
include "footer.php";
?>