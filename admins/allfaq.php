<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
    <style>
        .cop-price {
            float: left;
            height: 28px;
            width: 55px;
        }

        .couponForCheck {
            margin: 0 10px !important;
        }
        .loaderdiv {
            background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
            height: 700px;
            position: fixed;
            width: 100%;
            z-index: 99999;
            display: block;
        }
        .block {
            display: block !important;
        }
        .loaderhidden {
            display: none !important;
        }
        .loaderdiv > img {
            height: 50px;
            margin: 300px 50%;
            width: 50px;
        }
    </style>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">

        </div>
        <div class="row" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><span style="color:#1ABB9C;"></span>
                                    Maintain Frequent Asked Questions
                                    <small></small>
                                </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <div class="form-group form-inline">
                                            <input type="button" class="btn btn-danger btn-sm" onclick="addFaq()"
                                            value=" + Add FAQ" />
                                        </div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="text-muted font-13 m-b-30">
                                    From here admin can manage/modify the content of the faq
                                </p>
                                <div id="faqData"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function addFaq(){
        $(".modal-title").html("<label style='color: green'>Add New FAQ</label>");
        $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='faqError'" +
        " style='color:red'></p><div class='col-md-6 form-group'><label>Select Type</label>" +
        "<select class='form-control' id='type' ><option value='Placing Order' selected='Selected'>" +
        "Placing Order</option><option value='Placing Order2'>Placing Order2</option><option value='Placing Order3'>" +
        "Placing Order3</option><option value='Payment'>Payment</option><option value='Delivery and Shipping'>" +
        "Delivery and Shipping</option><option value='Fit'>Fit</option><option value='Product'>Product</select></div>" +
        "<div style='clear: both'></div><div class='col-md-6 form-group'><label>Question</label><textarea " +
        "id='question' class='form-control' placeholder='Enter Question'></textarea></div><div " +
        "class='col-md-6 form-group'><label>Answer</label><textarea rows='2' placeholder='Enter Answer' " +
        "class='form-control' id='answer' ></textarea></div><div class='col-md-12'><input type='button' " +
        "class='btn btn-danger pull-right' style='margin-top:30px' onclick='faqAdd()' id='subbtn' value='Submit'/>" +
        "</div></div><p class='percentprog' style='color:green;text-align:center'></div>");
        $(".modal-footer").css('display', 'none');
        $("#myModal").modal("show");
    }
    function faqAdd(){
        var type = $("#type").val();
        var question = $("#question").val();
        var answer = $("#answer").val();
        if(type === "" || question === "" || answer === ""){
            $("#faqError").html("please fill all required fields");
            $("#faqError").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "addNewFaq",'category':type,'question':question,'answer':answer}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status === "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getFaqData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
    }
    function getFaqData(){
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "getFaqData"}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var faqData = data.faqData;
            if (Status === "Success") {
                var tbodyData ="";
                var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                '<thead><tr><th>#</th><th>Category</th><th>Question</th><th>Answer</th><th>Action</th></tr></thead>';
                for(var a=0;a<faqData.length;a++){
                    tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td>"+faqData[a].type+"</td>" +
                    "<td>"+faqData[a].ques+"</td><td>"+faqData[a].ans+"</td>" +
                    "<td><a  onclick=confirmDelete_faq('"+faqData[a].u_id+"'); style='float: right;font-size:18px;" +
                    "padding:2px 4px;background: red none repeat scroll 0% 0%; color: white; margin-left: 5px;'>" +
                    "<i class='fa fa-trash-o'></i></a><a  onclick=editFaqData('"+faqData[a].u_id+"'); " +
                    "style='float: right; font-size: 18px;padding: 2px 4px; background: red none repeat scroll 0% 0%; " +
                    "color: white;'><i class='fa fa-pencil'></i></a></td></tr>";
                }
                $("#faqData").html(table+"<tbody>"+tbodyData+"</tbody></table>");
                $("#catTable").dataTable();
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                $("#faqData").html("<p class='text-center'><label>No Data Found</label></p>");
            }
        });
    }
    getFaqData();
    function editFaqData(id){
        var url = "api/userProcess.php";
        $.post(url, {"type": "editFaqData", "u_id": id}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var faqData = data.faqData;
            if (Status === "Success") {
                $(".modal-title").html("<label style='color: green'>Update FAQ</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='faqError2'" +
                " style='color:red'></p><div class='col-md-6 form-group'><label>Select Type</label>" +
                "<select class='form-control' id='e_type' ><option value='Placing Order' selected='Selected'>" +
                "Placing Order</option><option value='Placing Order2'>Placing Order2</option><option " +
                "value='Placing Order3'>Placing Order3</option><option value='Payment'>Payment</option>" +
                "<option value='Delivery and Shipping'>Delivery and Shipping</option><option value='Fit'>" +
                "Fit</option><option value='Product'>Product</select></div><div style='clear:both' ></div>" +
                "<div class='col-md-6 form-group'><label>Question</label><textarea id='e_question' " +
                "class='form-control' placeholder='Enter Question' >"+faqData.ques+"</textarea></div>" +
                "<div class='col-md-6 form-group'><label>Answer</label><textarea rows='2' placeholder='Enter Answer'" +
                "class='form-control' id='e_answer' >"+faqData.ans+"</textarea></div><div class='col-md-12'>" +
                "<input type='button' class='btn btn-danger pull-right' style='margin-top:30px' " +
                "onclick=updateFaq('"+faqData.u_id+"') id='subbtn' value='Update'/></div></div>" +
                "<p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#e_type").append("<option selected='selected' value='"+faqData.type+"'>"+faqData.type+"</option>");
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function updateFaq(id){
        var type = $("#e_type").val();
        var question = $("#e_question").val();
        var answer = $("#e_answer").val();
        if(type === "" || question === "" || answer === ""){
            $("#faqError2").html("please fill all required fields");
            $("#faqError2").css("color","red");
            return false;
        }
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updateFaq",'category':type,'question':question,'answer':answer,'u_id':id},function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if(Status === "Success"){
                $("#myModal").modal("hide");
                $(".loaderdiv").addClass("loaderhidden");
                getFaqData();
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                showMessage(Message,"red");
            }
        });
    }
    function confirmDelete_faq(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
        'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
        'onclick=delFaq("'+id+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function delFaq(id) {
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "deleteFaq", "u_id": id}, function (data) {
            var Status = data.Status;
            if (Status === "Success") {
                $("#myModal").modal("hide");
                getFaqData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
</script>
