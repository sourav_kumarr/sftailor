<?php
include('header.php');
include("admin/webserver/database/db.php");
?>
</div>
<div class="about-bottom wthree-3">
<style>
.top-bar {
    background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
}
.active {
    border: 2px solid #555;
    border-radius: 4px;
}
.primarysuggestion {
	height: auto;
	margin-left: 10px;
	margin-top: 10px;
	width: auto;
}
.dbprim{margin-left: 6px;}
.steps {
	background: #fff none repeat scroll 0 0;
	border: 2px solid #aeaeae;
	border-radius: 7%;
	cursor: pointer;
	float: left;
	font-size: 13px;
	line-height: 18px;
	margin-bottom: 20px;
	margin-left: 2.5%;
	margin-top: 10px;
	max-height: 57px;
	padding-top: 9px;
	text-align: center;
	width: 10%;
}

   .stepsactive {
    background: #c4996c none repeat scroll 0 0;
    border: 2px solid #c4996c;
    color: white;
}

.breakingline {
	width: 80%;
	margin-left: 10%;
	margin-top:40px;
}
.breakingline3 {
	margin-left: 16%;
	margin-top: 80px;
	transform: rotate(172deg);
	width: 80%;
}
.outputsugg {
	 position: absolute;
	 margin-left: -70px;
	 z-index:-1;
}
.backbtnsugg
{
	height:68%;
}
.backbtnsuggparentdiv
{
	height: 180px;
	width: 130px;
}
.backbtnsuggparentdiv p
{
	font-weight:normal;
	font-size: 12px;
	line-height: 18px!important;
}
#forcasual .primarysuggestion > img
{
	padding: 40px;
}
.okok
{
	font-size: 19px;
	height: 52px;
	margin-left: 10px;
	margin-top: 10px;
	padding-top: 6px;
	width: 162px;
}
.overflow-cmd{overflow-y: auto; height: 660px; padding-top: 22px;}
.liningoptions img {
    height: 40px;
}
</style>
<div class="loader" style="display: none">
    <img src="images/spin.gif" class="spinloader"/>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="col-md-1">
            <label onclick="rotateImage('left')" id="rotateLeft" style="margin-top: 350px"><i class="fa fa-share fa-2x"></i></label>
        </div>
        <div class="col-md-10" style="padding-top: 50px;position: relative;z-index: -100">
            <img src=""  class="outputsugg" id="frontbuttonimg" style="z-index: 4" alt=''/>
            <img src=""  class="outputsugg" id="frontbuttonpointimg" style="z-index: 0" alt=''/>
            <img src=""  class="outputsugg" id="lapelstyleimg" style="z-index: 1" alt=''/>
            <img src="" class="outputsugg" id="breastpocketimg" style="z-index: 0" alt=''/>
            <img src="" class="outputsugg" id="lowerpocketimg" style="z-index: 2" alt=''/>
            <img src="" class="outputsugg" id="frontbuttoncutimg" style="z-index: 3" alt=''/>
            <img src=""  class="outputsugg" id="backventimg" alt=''/>
            <img src="" class="outputsugg" id="shirtimg" alt=''/>
            <img src="" class="outputsugg" id="tieimg" alt=''/>
            <img src="" class="outputsugg" id="jacketimg" alt=''/>
            <img src="" class="outputsugg" id="armbuttonimg" alt=''/>
            <img src="" class="outputsugg" id="sidearmbuttonimg" alt=''/>
            <img src="" class="outputsugg" id="sidearmbuttoncutimg" alt=''/>
            <img src="" class="outputsugg" id="sidearmbuttonpointimg" alt=''/>
        </div>
        <div class="col-md-1">
            <label onclick="rotateImage('right')" id="rotateRight" style="margin-top: 350px"><i class="fa fa-reply fa-2x"></i></label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12" style="padding:50px 0">
            <div class="steps stepsactive" id="suitcategory">Suit<br>Category</div>
            <div class="steps" id="frontbutton">Front <br>Button</div>
            <div class="steps" id="lapelstyle">Lapel <br>Style</div>
            <div class="steps" id="lapelingredient">Lapel <br>Ingredient</div>
            <div class="steps" id="chestdart">Chest<br>Dart</div>
            <div class="steps" id="feltcollar">Felt<br>Collar</div>
            <div class="steps" id="innerflap">Inner<br>Flap</div>
            <div class="steps" id="facingstyle">Facing <br>Style</div>
            <div class="steps" id="sleeveslit">Sleeve <br>Slit</div>
            <div class="steps" id="breastpocket">Breast <br>Pocket</div>
            <div class="steps" id="lowerpocket">Lower <br>Pocket</div>
            <div class="steps" id="coinpocket">Coin <br>Pocket</div>
            <div class="steps" id="backvent">Back <br> Vents</div>
            <div class="steps" id="liningstyle">Lining <br> Style</div>
            <div class="steps" id="shoulderstyle">Shoulder <br>Pad</div>
            <div class="steps" id="buttonoption">Button <br>Options</div>
            <div class="steps" id="buttonswatch">Button <br>Swatch</div>
            <div class="steps" id="threadoption">Thread <br>Options</div>
            <div class="steps" id="elbowpatch">Elbow <br>Patch</div>
            <div class="steps" id="canvasoption">Canvas<br>Options</div>
            <div class="steps" id="suitcolor">Jacket <br>Color</div>
            <div class="breakingline"></div>
        </div>
        <!--
        ------
        -----category div
        ------
        -------->
        <div class="col-md-12" id="suitcategorys" style="display: block;">
            <p class="stylecaption">Suit Category</p>
            <div class="primarysuggestion okok active" id="suitcategory_formal" style="margin-left:25%">
                <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Formal / Casual</p>
            </div>
            <div class="primarysuggestion okok" id="suitcategory_tuxedo">
                <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Tuxedo</p>
            </div>
        </div>
        <!--
        ------
        -----button div
        ------
        -------->
        <div class="col-md-12 overflow-cmd" id="frontbuttons" style="display: none">
            <p class="stylecaption">Front Button Style</p>

            <div class="primarysuggestion active" id="frontbutton_onebutton">
                <img src="images/jackets/onebutton.png"/>
                <p class="suggestionname">C-0011 Single Breasted</br>One Button</p>
            </div>
            <div class="primarysuggestion" id="frontbutton_twobutton" style="margin-left: 10px">
                <img src="images/jackets/twobutton.png"/>
                <p class="suggestionname">C-0012 Single Breasted</br>Two Buttons(most popular)</p>
            </div>
            <div class="primarysuggestion" id="frontbutton_threebutton" style="margin-left: 10px">
                <img src="images/jackets/threebutton.png"/>
                <p class="suggestionname">C-0013 Single Breasted</br>Three Buttons</p>
            </div>
            <div class="primarysuggestion" id="frontbutton_fourbutton" style="margin-left: 10px">
                <img src="images/jackets/fourbutton.png"/>
                <p class="suggestionname">C-0014 Single Breasted</br>Four Buttons</p>
            </div>
            <div class="primarysuggestion" id="frontbutton_doublebreasted" style="margin-left: 10px">
                <img src="images/jackets/doublebreasted.png"/>
                <p class="suggestionname">C-0017 Double Breasted</br>Four Buttons</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_doublebreastedeightbuttons" style="margin-left: 10px">
                <img src="images/jackets/doublebreastedeightbuttons.png"/>
                <p class="suggestionname">C-0020 Double Breasted</br>Eight Buttons</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_doublebreastedfourbuttonstwobuttonsfasten" style="margin-left: 10px" title="Double Breasted four Buttons two Buttons Fasten">
                <img src="images/jackets/doublebreastedfourbuttonstwobuttonsfasten.png"/>
                <p class="suggestionname">C-0015 Double Breasted</br>Four Buttons..</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_C-0016doublebreastedsixbuttons" style="margin-left: 10px" title="Double Breasted four Buttons two Buttons Fasten">
                <img src="images/jackets/C-0016doublebreastedsixbuttons.png"/>
                <p class="suggestionname">C-0016 Double Breasted</br>Six buttons</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_doublebreastedsixbuttonsthreebuttonsfasten" style="margin-left: 10px" title="Double Breasted six Buttons Three Buttons Fasten">
                <img src="images/jackets/doublebreastedsixbuttonsthreebuttonsfasten.png"/>
                <p class="suggestionname">C-0019 Double Breasted</br>Six Buttons..</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_doublebreastedtwobuttonsonebuttonfasten" style="margin-left: 10px" title="Double Breasted two Buttons One Button Fasten">
                <img src="images/jackets/doublebreastedtwobuttonsonebuttonfasten.png"/>
                <p class="suggestionname">C-002F Double Breasted</br>Two Buttons..</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_singlebreastedfivebuttons" style="margin-left: 10px">
                <img src="images/jackets/singlebreastedfivebuttons.png"/>
                <p class="suggestionname">C-0023 Single Breasted</br>Five Buttons</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_singlebreastedsixbuttons" style="margin-left: 10px">
                <img src="images/jackets/singlebreastedsixbuttons.png"/>
                <p class="suggestionname">C-0026 Single Breasted</br>Six Buttons</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_singlebreastedthreebuttonslapelroll" style="margin-left: 10px" title="Single Breasted Three Buttons Lapel Roll">
                <img src="images/jackets/singlebreastedthreebuttonslapelroll.png"/>
                <p class="suggestionname">C-002C Single Breasted</br>Three Buttons..</p>
            </div>
			
			<div class="primarysuggestion" id="frontbutton_singlebreastedtwobuttonslapelrollbetween" style="margin-left: 10px" title="Single Breasted two Buttons Lapel Roll Between">
                <img src="images/jackets/singlebreastedtwobuttonslapelrollbetween.png"/>
                <p class="suggestionname">C-001Z Single Breasted</br>Two Buttons..</p>
            </div>
			
        </div>
        <!--
       ------
       -----front pocket div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="lapelstyles" style="display:none">
            <p class="stylecaption">Lapel Style</p>
            <div class="primarysuggestion active " id="lapelstyle_notch">
                <img src="images/jackets/notch.png"/>
                <p class="suggestionname">C-0001</br>Notch Lapel (most popular)</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_peak">
                <img src="images/jackets/peak.png"/>
                <p class="suggestionname">C-0002</br>Peak Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_seminotch">
                <img src="images/jackets/seminotch.png"/>
                <p class="suggestionname">C-0003</br>Semi Notch Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_semipeak">
                <img src="images/jackets/semipeak.png"/>
                <p class="suggestionname">C-0004</br>Semi Peak Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_shawl">
                <img src="images/jackets/shawl.png"/>
                <p class="suggestionname">D-0005</br>Shawl Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_fishmouth">
                <img src="images/jackets/fishmouth.png"/>
                <p class="suggestionname">C-0006</br>Fish Mouth lapel</p>
            </div>
            <div id="notindoublebreasted">
                <div class="primarysuggestion"  title="Sport Collar A(Notch Lapel With Left Square Tab)" id="lapelstyle_sportcollara">
                    <img src="images/jackets/sportcollara.png"/>
                    <p class="suggestionname">C-0681</br>Sport Collar A...</p>
                </div>
                <div class="primarysuggestion" title="Sport Collar B(Notch Lapel With Left Pointed Tab)" id="lapelstyle_sportcollarb">
                    <img src="images/jackets/sportcollarb.png"/>
                    <p class="suggestionname">C-0682</br>Sport Collar B...</p>
                </div>
                <div class="primarysuggestion" title="Sport Collar F(Notch Lapel With Left Round Tab)" id="lapelstyle_sportcollarf">
                    <img src="images/jackets/sportcollarf.png"/>
                    <p class="suggestionname">C-0685</br>Sport Collar F...</p>
                </div>
            </div>
			 <div class="primarysuggestion" id="lapelstyle_ducknotchlapel">
                <img src="images/jackets/ducknotchlapel.png"/>
                <p class="suggestionname">C-001U</br>Duck Notch Lapel</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_notchlapelroundcolar">
                <img src="images/jackets/notchlapelroundcolar.png"/>
                <p class="suggestionname">C-001M</br>Notch Lapel Round Colar</p>
            </div>
			
			<div class="primarysuggestion" title="Notch Lapel with Contrast Satin in Middle" id="lapelstyle_notchlapelwithcontrastsatininmiddle">
                <img src="images/jackets/notchlapelwithcontrastsatininmiddle.png"/>
                <p class="suggestionname">C-0694</br>Notch Lapel with..</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_peaklepalroundcolar">
                <img src="images/jackets/peaklepalroundcolar.png"/>
                <p class="suggestionname">C-001N</br>Peak Lepal Round Colar</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_roundpeaklapel">
                <img src="images/jackets/roundpeaklapel.png"/>
                <p class="suggestionname">C-001H</br>Round Peak Lapel</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_roundpeaklapelroundcolar" title="Round Peak Lapel Round Colar">
                <img src="images/jackets/roundpeaklapelroundcolar.png"/>
                <p class="suggestionname">C-001J</br>Round Peak Lapel..</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_shawllepalwithsatinonedge" title="Shawl Lepal with Satin on Edge">
                <img src="images/jackets/shawllepalwithsatinonedge.png"/>
                <p class="suggestionname">C-0691</br>Shawl Lepal with..</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_C-0009bandcollar" title="Shawl Lepal with Satin on Edge">
                <img src="images/jackets/C-0009bandcollar.png"/>
                <p class="suggestionname">C-0009</br>Band Collar</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_C-001Kroundnotchlapelroundcollar" title="Round Notch Lapel Round Vollar">
                <img src="images/jackets/C-001Kroundnotchlapelroundcollar.png"/>
                <p class="suggestionname">C-001K</br>Round Notch Lapel..</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_C-001Groundnotchlapel">
                <img src="images/jackets/C-001Groundnotchlapel.png"/>
                <p class="suggestionname">C-001G</br>Round Notch Lapel</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_C-0681sportcollaranotchlapelwithleftsquretab" title="Sport Collar A Notch Lapel with left squre tab">
                <img src="images/jackets/C-0681sportcollaranotchlapelwithleftsquretab.png"/>
                <p class="suggestionname">C-0681</br>Sport Collar A Notch..</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_C-0682sportcollarbnotchlapelwithleftpointedtab" title="Sport Collar B Notch Lapel with left pointed tab">
                <img src="images/jackets/C-0682sportcollarbnotchlapelwithleftpointedtab.png"/>
                <p class="suggestionname">C-0682</br>Sport Collar B Notch..</p>
            </div>
			
			<div class="primarysuggestion" id="lapelstyle_C-0683sportcollarcnotchlapelwithlefttrapzoidtab" title="Sport Collar C Notch lapel with left trapzoid tab">
                <img src="images/jackets/C-0683sportcollarcnotchlapelwithlefttrapzoidtab.png"/>
                <p class="suggestionname">C-0683</br>Sport Collar C Notch..</p>
            </div>
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Lapel Button Holes</p>
				<div class="primarysuggestion" id="lapelstyle_C-055Pbuttonholeonleftonlyforshawlanddiamondlapel" title="buttonhole on left only for shawl and diamond lapel">
					<img src="images/jackets/C-055Pbuttonholeonleftonlyforshawlanddiamondlapel.png"/>
					<p class="suggestionname">C-055P</br>buttonhole on left..</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0541left" title="">
					<img src="images/jackets/C-0541left.png"/>
					<p class="suggestionname">C-0541</br>left</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0542right" title="">
					<img src="images/jackets/C-0542right.png"/>
					<p class="suggestionname">C-0542</br>right</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0543rightandleft" title="">
					<img src="images/jackets/C-0543rightandleft.png"/>
					<p class="suggestionname">C-0543</br>right and left</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0544nolapelbuttonhole" title="">
					<img src="images/jackets/C-0544nolapelbuttonhole.png"/>
					<p class="suggestionname">C-0544</br>no lapel button hole</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0545lefttwo" title="">
					<img src="images/jackets/C-0545lefttwo.png"/>
					<p class="suggestionname">C-0545</br>left two</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0546righttwo" title="">
					<img src="images/jackets/C-0546righttwo.png"/>
					<p class="suggestionname">C-0546</br>right two</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0549leftthreerighttwo" title="">
					<img src="images/jackets/C-0549leftthreerighttwo.png"/>
					<p class="suggestionname">C-0549</br>left three right two</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0550leftthree" title="">
					<img src="images/jackets/C-0550leftthree.png"/>
					<p class="suggestionname">C-0550</br>left three</p>
				</div>
			</div>	
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Buttonhole Style Lapel</p>
				<div class="primarysuggestion" id="lapelstyle_C-055Krealstraightwith05cmbuttonhole" title="Real straight with 0.5cm button hole">
					<img src="images/jackets/C-055Krealstraightwith05cmbuttonhole.png"/>
					<p class="suggestionname">C-055K(most recommended)</br>Real straight with..</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0551straight" title="">
					<img src="images/jackets/C-0551straight.png"/>
					<p class="suggestionname">C-0551</br>straight</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0552round" title="">
					<img src="images/jackets/C-0552round.png"/>
					<p class="suggestionname">C-0552</br>round</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0553realstraight" title="">
					<img src="images/jackets/C-0553realstraight.png"/>
					<p class="suggestionname">C-0553</br>real straight</p>
				</div>
				<div class="primarysuggestion" id="lapelstyle_C-0554realround" title="">
					<img src="images/jackets/C-0554realround.png"/>
					<p class="suggestionname">C-0554</br>real round</p>
				</div>
			</div>	
        </div>
		<!--
       ------
       -----lapel ingrediant style div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="lapelingredients" style="display:none">
            <p class="stylecaption">Lapel Ingredients Style</p>
			<div class="primarysuggestion active " id="lapelingredient_C-0652satinlapel">
                <img src="images/jackets/C-0652satinlapel.png"/>
                <p class="suggestionname">C-0652 Satin Lapel</p>
            </div>
            <div class="primarysuggestion active " id="lapelingredient_C-065Qspecifyfabricforlapel" title="C-065Q Specify Fabric for Lapel">
                <img src="images/jackets/C-065Qspecifyfabricforlapel.png"/>
                <p class="suggestionname">C-065Q Specify Fabric</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelingredient_C-065R10cmsatinonlapeledge" title="C-065R 1.0cm Satin on lapel edge">
                <img src="images/jackets/C-065R10cmsatinonlapeledge.png"/>
                <p class="suggestionname">C-065R 1.0cm Satin</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelingredient_C-065T12cmsatinonlapeledge" title="C-065T 1.2cm satin on lapel edge">
                <img src="images/jackets/C-065T12cmsatinonlapeledge.png"/>
                <p class="suggestionname">C-065T 1.2cm satin</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelingredient_C-065T12cmsatinonlapeledge" title="C-065T 1.2cm satin on lapel edge">
                <img src="images/jackets/C-0544nolapelbuttonhole.png"/>
                <p class="suggestionname">standard no ingredient</p>
            </div>
			
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Satin Options</p>
				<div class="primarysuggestion" id="lapelingredient_dxn043a">
					<img src="images/jackets/dxn043a.png" class=""/>
					<p class="suggestionname">DXN043A White</p>
				</div>
				<div class="primarysuggestion" id="lapelingredient_dxn032a">
					<img src="images/jackets/dxn032a.png" class=""/>
					<p class="suggestionname">DXN032A Grey</p>
				</div>
				<div class="primarysuggestion" id="lapelingredient_dxn031a">
					<img src="images/jackets/dxn031a.png" class=""/>
					<p class="suggestionname">DXN031A Red</p>
				</div>
				<div class="primarysuggestion" id="lapelingredient_dxn030a">
					<img src="images/jackets/dxn030a.png" class=""/>
					<p class="suggestionname">DXN030A Wine</p>
				</div>
				<div class="primarysuggestion" id="lapelingredient_dxn029a">
					<img src="images/jackets/dxn029a.png" class=""/>
					<p class="suggestionname">DXN029A Navy</p>
				</div>
				<div class="primarysuggestion" id="lapelingredient_dxn008a">
					<img src="images/jackets/dxn008a.png" class=""/>
					<p class="suggestionname">DXN008A Navy</p>
				</div>
				
			</div>
			
        </div>
		
		<!--
       ------
       -----Chest dart style div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="chestdarts" style="display:none">
            <p class="stylecaption">Chest Dart Style</p>
            <div class="primarysuggestion active " id="lapelstyle_C-0521normalchestdart">
                <img src="images/jackets/C-0521normalchestdart.png"/>
                <p class="suggestionname">C-0521 Normal Chest Dart</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0522nochestdart">
                <img src="images/jackets/C-0522nochestdart.png"/>
                <p class="suggestionname">C-0522 No Chest Dart</p>
            </div>
			
        </div>
		
		<!--
       ------
       -----felt under collar style div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="feltcollars" style="display:none">
            <p class="stylecaption">Felt under collar Style</p>
            <div class="primarysuggestion active " id="feltcollar_FLD-0110">
                <img src="images/jackets/FLD-0110.png"/>
                <p class="suggestionname">FLD-0110</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-0118">
                <img src="images/jackets/FLD-0118.png"/>
                <p class="suggestionname">FLD-0118</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-0916">
                <img src="images/jackets/FLD-0916.png"/>
                <p class="suggestionname">FLD-0916</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-3803">
                <img src="images/jackets/FLD-3803.png"/>
                <p class="suggestionname">FLD-3803</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-4011">
                <img src="images/jackets/FLD-4011.png"/>
                <p class="suggestionname">FLD-4011</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-4813">
                <img src="images/jackets/FLD-4813.png"/>
                <p class="suggestionname">FLD-4813</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-5419">
                <img src="images/jackets/FLD-5419.png"/>
                <p class="suggestionname">FLD-5419</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-6544">
                <img src="images/jackets/FLD-6544.png"/>
                <p class="suggestionname">FLD-6544</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-7417">
                <img src="images/jackets/FLD-7417.png"/>
                <p class="suggestionname">FLD-7417</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-7420">
                <img src="images/jackets/FLD-7420.png"/>
                <p class="suggestionname">FLD-7420</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-7421">
                <img src="images/jackets/FLD-7421.png"/>
                <p class="suggestionname">FLD-7421</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD7436">
                <img src="images/jackets/FLD7436.png"/>
                <p class="suggestionname">FLD7436</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-9935">
                <img src="images/jackets/FLD-9935.png"/>
                <p class="suggestionname">FLD-9935</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD12126">
                <img src="images/jackets/FLD12126.png"/>
                <p class="suggestionname">FLD12126</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-12345">
                <img src="images/jackets/FLD-12345.png"/>
                <p class="suggestionname">FLD-12345</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-012347">
                <img src="images/jackets/FLD-012347.png"/>
                <p class="suggestionname">FLD-012347</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-012346">
                <img src="images/jackets/FLD-012346.png"/>
                <p class="suggestionname">FLD-012346</p>
            </div>
			<div class="primarysuggestion" id="feltcollar_FLD-012348">
                <img src="images/jackets/FLD-012348.png"/>
                <p class="suggestionname">FLD-012348</p>
            </div>
			
        </div>
		<!--
       ------
       -----Inner flap style div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="innerflaps" style="display:none">
            <p class="stylecaption">Inner Flap Style</p>
            <div class="primarysuggestion active " id="lapelstyle_C-0314exteriorpocketinnerflapliningmatchfabric" title="C-0314 Exterior Pocket Inner flap lining match fabric">
                <img src="images/jackets/C-0314exteriorpocketinnerflapliningmatchfabric.png"/>
                <p class="suggestionname">C-0314 Exterior Pocket..</br>(most recommended)</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0316exteriorpocketinnerflapwithpocket" title="C-0316 Exterior Pocket Inner flap with pocket">
                <img src="images/jackets/C-0316exteriorpocketinnerflapwithpocket.png"/>
                <p class="suggestionname">C-0316 Exterior Pocket..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0317nosealstitch">
                <img src="images/jackets/C-0317nosealstitch.png"/>
                <p class="suggestionname">C-0317 No Seal Stitch</p>
            </div>
        </div>
		<!--
       ------
       -----facing style inner div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="facingstyles" style="display:none">
            <p class="stylecaption">Facing Style Inside</p>
            <div class="primarysuggestion active " id="lapelstyle_C-0701innerfacinga">
                <img src="images/jackets/C-0701innerfacinga.png"/>
                <p class="suggestionname">C-0701 Inner Facing A</br>(most recommended)</p>
            </div>

			<div class="primarysuggestion active " id="lapelstyle_C-0702innerfacingb">
                <img src="images/jackets/C-0702innerfacingb.png"/>
                <p class="suggestionname">C-0702 Inner Facing B</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0703innerfacingc">
                <img src="images/jackets/C-0703innerfacingc.png"/>
                <p class="suggestionname">C-0703 Inner Facing C</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0706innerfacingf">
                <img src="images/jackets/C-0706innerfacingf.png"/>
                <p class="suggestionname">C-0706 Inner Facing F</p>
            </div>
			
        </div>
		<!--
       ------
       -----Sleeve Slit Style div
       ------
       -------->
        <div class="col-md-12 overflow-cmd" id="sleeveslits" style="display:none">
            <p class="stylecaption">Sleeve Slit Style</p>
            <div class="primarysuggestion active " id="lapelstyle_C-061Cnormalsleeveslitnormalslantbottonhole" title="C-061C Normal Sleeve Slit normal slant botton hole">
                <img src="images/jackets/C-061Cnormalsleeveslitnormalslantbottonhole.png"/>
                <p class="suggestionname">C-061C Normal Sleeve..</p>
            </div>

			<div class="primarysuggestion active " id="lapelstyle_C-061Drealslitslantbuttonhole" title="C-061D Real Slit slant button hole">
                <img src="images/jackets/C-061Drealslitslantbuttonhole.png"/>
                <p class="suggestionname">C-061D Real Slit slant..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-061Erealsleeveslitnormalbuttonhole" title="C-061E real sleeve slit normal button hole">
                <img src="images/jackets/C-061Erealsleeveslitnormalbuttonhole.png"/>
                <p class="suggestionname">C-061E real sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-061Fnormalsleeveslitwithoutbuttonandbuttonhole" title="C-061F Normal sleeve slit without button and button hole">
                <img src="images/jackets/C-061Fnormalsleeveslitwithoutbuttonandbuttonhole.png"/>
                <p class="suggestionname">C-061F Normal sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-061Gpointedsleeveepauletwithroundbuttonhole" title="C-061G Pointed sleeve epaulet with round button hole">
                <img src="images/jackets/C-061Gpointedsleeveepauletwithroundbuttonhole.png"/>
                <p class="suggestionname">C-061G Pointed sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-061Krealsleeveslitnobuttonholenobutton" title="C-061K Real sleeve slit no button hole no button">
                <img src="images/jackets/C-061Krealsleeveslitnobuttonholenobutton.png"/>
                <p class="suggestionname">C-061K Real sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-061Msquaresleeveepauletwithroundbuttonhole" title="C-061M Square sleeve epaulet with round button hole">
                <img src="images/jackets/C-061Msquaresleeveepauletwithroundbuttonhole.png"/>
                <p class="suggestionname">C-061M Square sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-061Proundsleeveepauletwithroundbuttonhole" title="C-061P Round sleeve epaulet with round button hole">
                <img src="images/jackets/C-061Proundsleeveepauletwithroundbuttonhole.png"/>
                <p class="suggestionname">C-061P Round sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0611normalsleeveslitnormalbuttonhole" title="C-0611 Normal sleeve slit normal button hole">
                <img src="images/jackets/C-0611normalsleeveslitnormalbuttonhole.png"/>
                <p class="suggestionname">C-0611 Normal sleeve..</br>(most recommended)</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0612realslit" title="">
                <img src="images/jackets/C-0612realslit.png"/>
                <p class="suggestionname">C-0612 Real Slit</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0613noslitandbuttonhole" title="C-0613 No slit and button hole">
                <img src="images/jackets/C-0613noslitandbuttonhole.png"/>
                <p class="suggestionname">C-0613 No slit and..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0614normalsleeveslitnobuttonhole" title="C-0614 Normal sleeve slit no button hole">
                <img src="images/jackets/C-0614normalsleeveslitnobuttonhole.png"/>
                <p class="suggestionname">C-0614 Normal sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0615realsleeveslitnobuttonhole" title="C-0615 Real sleeve slit no button hole">
                <img src="images/jackets/C-0615realsleeveslitnobuttonhole.png"/>
                <p class="suggestionname">C-0615 Real sleeve..</p>
            </div>
			
			<div class="primarysuggestion active " id="lapelstyle_C-0616realsleeveslitcuffwithoutbuttonandbuttonhole" title="C-0616 Real sleeve slit cuff without button and button hole">
                <img src="images/jackets/C-0616realsleeveslitcuffwithoutbuttonandbuttonhole.png"/>
                <p class="suggestionname">C-0616 Real sleeve..</p>
            </div>
			
        </div>
        <!------
        -----breast pocket div
        ------
        -------->
        <div class="col-md-12 overflow-cmd" id="breastpockets" style="display:none">
            <p class="stylecaption">Breast Pocket Style</p>
            <div class="primarysuggestion backbtnsuggparentdiv active" id="breastpocket_topnormalbreastpocket">
                <img src="images/jackets/topnormalbreastpocket.png" class="backbtnsugg" />
                <p class="suggestionname">C-0101 (most popular)</br>Normal Breast Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_topshipshape">
                <img src="images/jackets/topshipshape.png" class="backbtnsugg"/>
                <p class="suggestionname">Ship Shape Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_topnobreastpocket">
                <img src="images/jackets/topnobreastpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0100</br>No Breast Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_toparcbreastpocket">
                <img src="images/jackets/toparcbreastpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0102</br>Arc Breast Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_topdoublebesom">
                <img src="images/jackets/topdoublebesom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0110</br>Double Besom</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_topdoublebesomwithtabandbutton">
                <img src="images/jackets/topdoublebesomwithtabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0111 Double Besom </br>With Tab & Button</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_topdoublebesomwithflap">
                <img src="images/jackets/topdoublebesomwithflap.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom With Flap</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_topdoublebesomwithzipper">
                <img src="images/jackets/topdoublebesomwithzipper.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom With Zipper</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_toppatchbreastpocket">
                <img src="images/jackets/toppatchbreastpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0150</br>Patch Breast Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Breast Pocket With Button & Button Hole" id="breastpocket_toppatchbreastpocketwithbuttonandbuttonhole">
                <img src="images/jackets/toppatchbreastpocketwithbuttonandbuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0151</br>Patch Breast Pocket..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pocket One Pleat With Diamond Flap" id="breastpocket_toppatchpocketonepleatwithdiamondflap">
                <img src="images/jackets/toppatchpocketonepleatwithdiamondflap.png" class="backbtnsugg"/>
                <p class="suggestionname">Patch Pocket One...</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_toppatchpocketwithflap">
                <img src="images/jackets/toppatchpocketwithflap.png" class="backbtnsugg"/>
                <p class="suggestionname">Patch Pocket With Flap</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pocket One Pleat With Diamond Flap & Button" id="breastpocket_toppatchpocketonepleatwithdiamondflapandbutton">
                <img src="images/jackets/toppatchpocketonepleatwithdiamondflapandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">Patch Pocket One...</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="2.0cm width walt Breast Pocket" id="breastpocket_20cmwidthwaltbreastpocket">
                <img src="images/jackets/20cmwidthwaltbreastpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-011B</br>2.0cm width walt..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="Arc Breast Pocket with 1.0cm Satin" id="breastpocket_arcbreastpocketwith15cmsatin">
                <img src="images/jackets/arcbreastpocketwith15cmsatin.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0109</br>Arc Breast Pocket..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="Normal right breast pocket no left breast pocket" id="breastpocket_normalrightbreastpocketnoleftbreastpocket">
                <img src="images/jackets/normalrightbreastpocketnoleftbreastpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0157</br>Normal Right Breast..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_patchpocketonepleat">
                <img src="images/jackets/patchpocketonepleat.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0121</br>Patch Pocket One Pleat</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="breastpocket_trapezoidchestpocket">
                <img src="images/jackets/trapezoidchestpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-00J2</br>Trape Zoid Chest Pocket</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="welt breast pocket with 1.2 width" id="breastpocket_weltbreastpocketwith12width">
                <img src="images/jackets/weltbreastpocketwith12width.png" class="backbtnsugg"/>
                <p class="suggestionname">C-011A</br>welt breast pocket..</p>
            </div>
			
        </div>
        <!------
        -----belt loop div
        ------
        -------->
        <div class="col-md-12" id="lowerpockets" style="display:none">
            <p class="stylecaption">Lower Pocket Style</p>
            <div class="primarysuggestion backbtnsuggparentdiv active" id="lowerpocket_normalpockets">
                <img src="images/jackets/normalpockets.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0201</br>Normal Pockets </br>(most popular)</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="lowerpocket_diamondflapspockets">
                <img src="images/jackets/diamondflapspockets.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0237</br>Diamond Flaps Pockets</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Diamond Flaps Pockets With Button & Hole" id="lowerpocket_diamondflapspocketswithbuttonandbuttonhole">
                <img src="images/jackets/diamondflapspocketswithbuttonandbuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0239</br>Diamond Flaps Pockets..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv " id="lowerpocket_doublebesompockets">
                <img src="images/jackets/doublebesompockets.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0231</br>Double Besom Pockets</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Double Besom With Tab & Button" id="lowerpocket_doublebesomwithtabandbutton">
                <img src="images/jackets/doublebesomwithtabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">C-02M1</br>Double Besom With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Double Besom With Round Tab & Button" id="lowerpocket_doublebesomwithroundtabandbutton">
                <img src="images/jackets/doublebesomwithroundtabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">C-02M4</br>Double Besom With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Double Besom With Square Tab & Button" id="lowerpocket_doublebesomwithsquaretabandbutton">
                <img src="images/jackets/doublebesomwithsquaretabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">C-02M3</br>Double Besom With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="lowerpocket_nolowerpockets">
                <img src="images/jackets/nolowerpockets.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0200</br>No Lower Pockets</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="lowerpocket_patchpockets">
                <img src="images/jackets/patchpockets.png" class="backbtnsugg"/>
                <p class="suggestionname">C-02J1</br>Patch Pockets</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pockets With Double Pleats" id="lowerpocket_patchpocketswithdoublepleats">
                <img src="images/jackets/patchpocketswithdoublepleats.png" class="backbtnsugg"/>
                <p class="suggestionname">C-02K3</br>Patch Pockets With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pockets With Pointed Tab & Button" id="lowerpocket_patchpocketswithpointedtabandbutton">
                <img src="images/jackets/patchpocketswithpointedtabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">Patch Pockets With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pockets With Round Tab & Button" id="lowerpocket_patchpocketswithroundtabandbutton">
                <img src="images/jackets/patchpocketswithroundtabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">Patch Pockets With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pockets With Square Tab & Button" id="lowerpocket_patchpocketswithsquaretabandbutton">
                <img src="images/jackets/patchpocketswithsquaretabandbutton.png" class="backbtnsugg"/>
                <p class="suggestionname">Patch Pockets With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="Patch Pocket With Single Pleat" id="lowerpocket_patchpocketwithsinglepleat">
                <img src="images/jackets/patchpocketwithsinglepleat.png" class="backbtnsugg"/>
                <p class="suggestionname">C-02K4</br>Patch Pocket With..</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="lowerpocket_weltpockets">
                <img src="images/jackets/weltpockets.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0261 </br>Welt Pockets</p>
            </div>
			<div class="primarysuggestion-s  backbtnsuggparentdiv">
                <img src="images/jackets/satinbason.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0653<br>Satin Besom</p>
            </div>
        </div>
		 <!------
        -----coin pocket div
        ------
        -------->
        <div class="col-md-12" id="coinpockets" style="display:none">
            <p class="stylecaption">Coin Pocket Style</p>
            <div class="primarysuggestion backbtnsuggparentdiv active" id="coinpocket_C-320nocoinpocket">
                <img src="images/jackets/C-320nocoinpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-320</br>no coin pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="coinpocket_C-0321coinpocketinrightlowerpocket" title="coin pocket in right lower pocket">
                <img src="images/jackets/C-0321coinpocketinrightlowerpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0321</br>coin pocket in..</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="coinpocket_C-322coinpocketinleftlowerpocket" title="coin pocket in left lower pocket">
                <img src="images/jackets/C-322coinpocketinleftlowerpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-322</br>coin pocket in..</p>
            </div>
           <div class="primarysuggestion backbtnsuggparentdiv" id="coinpocket_C-323coinpocketinlowerpocket" title="coin pocket in lower pocket">
                <img src="images/jackets/C-323coinpocketinlowerpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-323</br>coin pocket in..</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="coinpocket_C-0324doublecoinpocketinrightlowerpocket" title="double coin pocket in right lower pocket">
                <img src="images/jackets/C-0324doublecoinpocketinrightlowerpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0324</br>double coin pocket..</p>
            </div>
           
        </div>
        <!------
        -----back vent div
        ------
        -------->
        <div class="col-md-12" id="backvents" style="display:none;margin-bottom:170px">
            <p class="stylecaption">Back Vent Style</p>

            <div class="primarysuggestion" id="backvent_novent">
                <img src="images/jackets/novent.png"/>
                <p class="suggestionname">No Vent</br>C-04No (standard)</p>
            </div>
            <div class="primarysuggestion active" id="backvent_centeralvent">
                <img src="images/jackets/centralvent.png"/>
                <p class="suggestionname">One Vent </br>C-04N1</p>
            </div>
            <div class="primarysuggestion" id="backvent_sidevents">
                <img src="images/jackets/sidevents.png"/>
                <p class="suggestionname">Two Vents (most popular) </br>C-04N2</p>
            </div>
			<div class="primarysuggestion" id="backvent_C-04N3sideventswithstrip" title="C-04N3 side vents with strip">
                <img src="images/jackets/C-04N3sideventswithstrip.png"/>
                <p class="suggestionname">C-04N3 side vents with..</p>
            </div>
			<div class="primarysuggestion" id="backvent_C-04V0blindpleatnovent">
                <img src="images/jackets/C-04V0blindpleatnovent.png"/>
                <p class="suggestionname">C-04V0 blind pleat no vent</p>
            </div>
			<div class="primarysuggestion" id="backvent_C-04V1blindpleatcentralvent" title="C-04V1 blind pleat central vent">
                <img src="images/jackets/C-04V1blindpleatcentralvent.png"/>
                <p class="suggestionname">C-04V1 blind pleat central..</p>
            </div>
			<div class="primarysuggestion" id="backvent_C-04V2blindpleatwithdoublevents" title="C-04V2 blind pleat with double vents">
                <img src="images/jackets/C-04V2blindpleatwithdoublevents.png"/>
                <p class="suggestionname">C-04V2 blind pleat with..</p>
            </div>
			<div class="primarysuggestion" id="backvent_C-04V3blindpleatwithdoubleventsandventstrap" title="C-04V3 blind pleat with double vents and vent strap">
                <img src="images/jackets/C-04V3blindpleatwithdoubleventsandventstrap.png"/>
                <p class="suggestionname">C-04V3 blind pleat with..</p>
            </div>
        </div>

        <!------
        -----lining Style div
        ------
        -------->
         <div class="col-md-12 overflow-cmd" id="liningstyles" style="display:none;margin-bottom:70px">
            <p class="stylecaption">Lining Style</p>
            <div class="primarysuggestion backbtnsuggparentdiv" id="liningstyle_12lininginseamwithpinding">
                <img src="images/jackets/12lininginseamwithpinding.png" class="backbtnsugg"/>
                <p class="suggestionname">1/2 lining C-05M2</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="liningstyle_fulllining">
                <img src="images/jackets/fulllining.png" class="backbtnsugg"/>
                <p class="suggestionname">Full lining C-05C1 (standard)</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" title="1/2 lining,Pick stitch on lining inseam'Normal straight bottom" id="liningstyle_12liningpickstitchonlininginseamnormalstraightbottom">
                <img src="images/jackets/12liningpickstitchonlininginseamnormalstraightbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">1/2 lining C-05M4</p>
            </div>

			<div class="primarysuggestion backbtnsuggparentdiv" id="liningstyle_C-05C1fullliningroundbottom">
                <img src="images/jackets/C-05C1fullliningroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C1 Full Linin ground bottom</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C2 Full Lining Reduce 0.6 round bottom" id="liningstyle_05C2fullliningreduce06roundbottom">
                <img src="images/jackets/C-05C2fullliningreduce06roundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C2 Full Lining Reduce..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C3 Full Lining add 0.6 round bottom" id="liningstyle_C-05C3fullliningadd06roundbottom">
                <img src="images/jackets/C-05C3fullliningadd06roundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C3 Full Lining add 0.6..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C4 full lining 1.2cm round bottom" id="liningstyle_C-05C4fulllining12cmroundbottom">
                <img src="images/jackets/C-05C4fulllining12cmroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C4 full lining 1.2cm..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C4 No lining binding inseam small round bottom" id="liningstyle_C-05C4noliningbindinginseamsmallroundbottom">
                <img src="images/jackets/C-05C4noliningbindinginseamsmallroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C4 No lining binding..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C5 full lining straight bottom" id="liningstyle_C-05C5fullliningstraightbottom">
                <img src="images/jackets/C-05C5fullliningstraightbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C5 full lining straight bottom</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C6 full lining big round bottom" id="liningstyle_C-05C6fullliningbigroundbottom">
                <img src="images/jackets/C-05C6fullliningbigroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C6 full lining big round bottom</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05C9 lining pick stich on lining inseam normal round bottom" id="liningstyle_C-05C9liningpickstichonlininginseamnormalroundbottom">
                <img src="images/jackets/C-05C9liningpickstichonlininginseamnormalroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05C9 lining pick stich on ..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05M1 lining pic stich on lining in seam reduce 0.6 round bottom" id="liningstyle_C-05M1liningpicstichonlininginseamreduce06roundbottom">
                <img src="images/jackets/C-05M1liningpicstichonlininginseamreduce06roundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05M1 lining pic stich on..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05M3 lining pic stich on lining inseam add 1.2cm round bottom" id="liningstyle_C-05M3liningpicstichonlininginseamadd12cmroundbottom">
                <img src="images/jackets/C-05M3liningpicstichonlininginseamadd12cmroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05M3 lining pic stich on..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05M5 lining pick stich on lining inseam large round bottom" id="liningstyle_C-05M5liningpickstichonlininginseamlargeroundbottom">
                <img src="images/jackets/C-05M5liningpickstichonlininginseamlargeroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05M5 lining pick stich on..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05U4 lining binding in seam normal round bottom" id="liningstyle_C-05U4liningbindinginseamnormalroundbottom">
                <img src="images/jackets/C-05U4liningbindinginseamnormalroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05U4 lining binding in seam..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05U5 lining binding in seam smaller round bottom" id="liningstyle_C-05U5liningbindinginseamsmallerroundbottom">
                <img src="images/jackets/C-05U5liningbindinginseamsmallerroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05U5 lining binding in seam..</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" title="C-05V3 no lining binding inseam normal round bottom" id="liningstyle_C-05V3noliningbindinginseamnormalroundbottom">
                <img src="images/jackets/C-05V3noliningbindinginseamnormalroundbottom.png" class="backbtnsugg"/>
                <p class="suggestionname">C-05V3 no lining binding inseam ..</p>
            </div>
			
			
			
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Lining Options</p>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining1">
					<img src="images/jackets/lining1.png" class=""/>
					<p class="suggestionname">FLL181</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining2">
					<img src="images/jackets/lining2.png" class=""/>
					<p class="suggestionname">FLL150</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining3">
					<img src="images/jackets/lining3.png" class=""/>
					<p class="suggestionname">FLL7142-5</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining4">
					<img src="images/jackets/lining4.png" class=""/>
					<p class="suggestionname">FLL3002-1</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining5">
					<img src="images/jackets/lining5.png" class=""/>
					<p class="suggestionname">FLL7142-1</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining6">
					<img src="images/jackets/lining6.png" class=""/>
					<p class="suggestionname">FLL3002-9</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining7">
					<img src="images/jackets/lining7.png" class=""/>
					<p class="suggestionname">FLL185</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining8">
					<img src="images/jackets/lining8.png" class=""/>
					<p class="suggestionname">FLL3002-10</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining9">
					<img src="images/jackets/lining9.png" class=""/>
					<p class="suggestionname">FLL186</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining10">
					<img src="images/jackets/lining10.png" class=""/>
					<p class="suggestionname">FLL178</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining11">
					<img src="images/jackets/lining11.png" class=""/>
					<p class="suggestionname">FLL148</p>
				</div>
				<div class="primarysuggestion liningoptions" id="liningstyle_lining12">
					<img src="images/jackets/lining12.png" class=""/>
					<p class="suggestionname">FLL179</p>
				</div>
				
				 <?php
                $query = "select * from wp_colors where type='allfabric' and status = '1'";
                $result = mysql_query($query);
                if($result)
                {
                    $num = mysql_num_rows($result);
                    if($num>0)
                    {
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            ?>
							<div class="primarysuggestion liningoptions" id="liningstyle_lining12" style="padding: 19px;">
                            <p class="suggestionname" id="showfab"><?php echo $colorName;?></p>
							</div>
							
                            <?php
                        }
					}
					else
					{
						echo "";
					}
				}
				else
				{
					echo mysql_error();
				}
                ?>
					
				
				<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
					<input type="text" name="fabricno" id="textfab" value="" style="margin-left: 13px; margin-top: -27px;"></br> <input type="submit" name="addfabric" onclick="addfabrics('first')" value="Add Fabric Number" style="margin-left: 13px; margin-top: 10px;">
				</div>
					
			
				<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
					<p class="stylecaption">Lining</p>
					<div class="primarysuggestion" id="liningstyle_lining13">
						<img src="images/jackets/lining13.png" class=""/>
						<p class="suggestionname">240FLLDL059</p>
					</div>
					<div class="primarysuggestion" id="liningstyle_lining14">
						<img src="images/jackets/lining14.png" class=""/>
						<p class="suggestionname">240FLLDL059</p>
					</div>	
					
					<div class="primarysuggestion" id="liningstyle_lining15">
						<img src="images/jackets/lining15.png" class=""/>
						<p class="suggestionname">263FLLDL068</p>
					</div>	
					
					<div class="primarysuggestion" id="liningstyle_lining16">
						<img src="images/jackets/lining16.png" class=""/>
						<p class="suggestionname">264FLLDL069</p>
					</div>	
					
					<div class="primarysuggestion" id="liningstyle_lining17">
						<img src="images/jackets/lining17.png" class=""/>
						<p class="suggestionname">265FLLDL070</p>
					</div>	
				</div>
					
					
			</div> 
        </div>
        <!------
        -----shoulder Style div
        ------
        -------->
         <div class="col-md-12" id="shoulderstyles" style="display:none;margin-bottom:70px">
            <p class="stylecaption">Shoulder Pad Style</p>
            <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-060Ahighsleevhead">
                <img src="images/jackets/C-060Ahighsleevhead.png" class="backbtnsugg"/>
                <p class="suggestionname">C-060A high sleev head</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-060Nniapolitanshoulder">
                <img src="images/jackets/C-060Nniapolitanshoulder.png" class="backbtnsugg"/>
                <p class="suggestionname">C-060N niapol itan shoulder</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0601normalshoulder">
                <img src="images/jackets/C-0601normalshoulder.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0601 normal shoulder</br>(most standard)</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0602snubbyshoulder">
                <img src="images/jackets/C-0602snubbyshoulder.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0602 snubby shoulder</p>
            </div>
        
			<div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0603naturalshoulder">
                <img src="images/jackets/C-0603naturalshoulder.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0603 natural shoulder</p>
            </div>
        
			<div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0604causashoulderbrunecucine">
                <img src="images/jackets/C-0604causashoulderbrunecucine.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0604 causa shoulder brune cucine</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0607shouldertempraryfixedbyhand">
                <img src="images/jackets/C-0607shouldertempraryfixedbyhand.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0607 shoulder temprary fixed by hand</p>
            </div>
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">shoulder padding</p>
				<div class="primarysuggestion" id="shoulderstyle_shoulderpadding">
					<img src="images/jackets/shoulderpadding.png" class="padding-size" />
				</div>
				<div style="float: right;">
					<p style="font-weight: bold; margin-bottom: 22px;">Four levels of shoulder padding</p>
					<select id="paddingpad">
						<option>select padding options</option>
						<option value="03cm">C-060P 0.3 cm</option>
						<option value="06cm">C-060Q 0.6 cm</option>
						<option value="12cm">C-060R 1.2 cm (recommended)</option>
						<option value="2cm">C-060S 2CM</option>
					</select>
				</div>
			</div>		
        </div>
		<!------
        -----elbow patch Style div
        ------
        -------->
         <div class="col-md-12" id="elbowpatchs" style="display:none;margin-bottom:70px">
            <p class="stylecaption">Elbow Patch Style</p>
            <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_roundelbowpatch">
                <img src="images/jackets/roundelbowpatch.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0609 Round Elbow Patch</p>
            </div>

			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_ovalelbowpatch">
                <img src="images/jackets/ovalelbowpatch.png" class="backbtnsugg"/>
                <p class="suggestionname">C-0610 Oval Elbow Patch</p>
            </div>
			
		<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
			<p class="stylecaption">Elbow Patch Color Options</p>
            <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor1">
                <img src="images/jackets/elbowcolor1.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP504</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor2">
                <img src="images/jackets/elbowcolor2.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP511</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor3">
                <img src="images/jackets/elbowcolor3.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP510</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor4">
                <img src="images/jackets/elbowcolor4.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP527</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor5">
                <img src="images/jackets/elbowcolor5.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP509</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor6">
                <img src="images/jackets/elbowcolor6.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP519</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor7">
                <img src="images/jackets/elbowcolor7.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP508</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor8">
                <img src="images/jackets/elbowcolor8.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP507</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor9">
                <img src="images/jackets/elbowcolor9.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP522</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor10">
                <img src="images/jackets/elbowcolor10.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP521</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor11">
                <img src="images/jackets/elbowcolor11.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP513</p>
            </div>
			<div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_elbowcolor12">
                <img src="images/jackets/elbowcolor12.png" class="backbtnsugg"/>
                <p class="suggestionname">QXZP506</p>
            </div>			
        </div>    
	</div>
		<!------
        -----buttoning style div
        ------
        -------->
         <div class="col-md-12" id="buttonoptions" style="display:none;margin-bottom:70px">
            <p class="stylecaption">Buttoning Style</p>
           	<div class="primarysuggestion backbtnsuggparentdiv" id="buttonoption_C-0643buttoningstyle">
                <img src="images/jackets/C-0643buttoningstyle.png" />
                <p class="suggestionname">C-0643 Buttoning Style</p>
            </div>

			<div class="primarysuggestion backbtnsuggparentdiv" id="buttonoption_C-0644buttoingstyle">
                <img src="images/jackets/C-0644buttoingstyle.png" />
                <p class="suggestionname">C-0644 Buttoing Style</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="buttonoption_C-0645clawshapeonlyforhandmade" title="C-0645clawshape (only for hand made)">
                <img src="images/jackets/C-0645clawshapeonlyforhandmade.png" />
                <p class="suggestionname">C-0645 Claw Shape</p>
            </div>
			
			<div class="primarysuggestion" id="buttonoption_C-0646squarebuttoning" title="">
                <img src="images/jackets/C-0646squarebuttoning.png" />
                <p class="suggestionname">C-0646(most popular) </br>Square Buttoning </p>
            </div>		
        </div>
		     <!------
       -----button swatch options style div
       ------
       -------->
        <div class="col-md-12" id="buttonswatchs" style="display:none">
            <p class="stylecaption">Button Swatch Style</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 50px">
                <?php
                $query = "select * from wp_colors where type='suits' and subtype='suitswatch' and status = '1'";
                $result = mysql_query($query);
                if($result)
                {
                    $num = mysql_num_rows($result);
                    if($num>0)
                    {
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            ?>
                            <div class='primarysuggestion  dbprim colorsuggestion<?php if($a == 1) echo ' active' ?>' title="<?php echo $colorName ?>" id='buttonswatch_<?php echo $colorName ?>'>
                                <img src='images/jackets/<?php echo $colorName.".png" ?>' />
                            </div>
                            <?php
                        }
                    }
                    else
                    {
                        echo "No button Found";
                    }
                }
                else
                {
                    echo mysql_error();
                }
                ?>
            </div>
        </div>
		
		<!------
       -----Thread options style div
       ------
       -------->
        <div class="col-md-12" id="threadoptions" style="display:none">
            <p class="stylecaption">Thread Options Style</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 50px">
                <?php
                $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
                $result = mysql_query($query);
                if($result)
                {
                    $num = mysql_num_rows($result);
                    if($num>0)
                    {
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            ?>
                            <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' title="<?php echo $colorName ?>" id='threadoption_<?php echo $colorName ?>'>
                                <img src='images/shirts/<?php echo $colorName.".png" ?>' />
                            </div>
                            <?php
                        }
                    }
                    else
                    {
                        echo "No Color Found";
                    }
                }
                else
                {
                    echo mysql_error();
                }
                ?>
            </div>
        </div>
		<!------
        -----canvas options div
        ------
        -------->
         <div class="col-md-12" id="canvasoptions" style="display:none;margin-bottom:70px">
            <p class="stylecaption">Canvas Options</p>
            <div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-000Afullcanvas">
                <img src="images/jackets/C-000Afullcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">C-000A Full Canvas</br>(most popular)</p>
            </div>

			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-000Bhalfcanvas">
                <img src="images/jackets/C-000Bhalfcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">C-000B Half Canvas</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-00AAsemihandmadefullcanvas">
                <img src="images/jackets/C-00AAsemihandmadefullcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">C-00AA Semi Hand Made Full Canvas</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-00C1fused">
                <img src="images/jackets/C-00C1fused.png" class="backbtnsugg"/>
                <p class="suggestionname">C-00C1 Fused</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-00C3singlelayerchestfusing">
                <img src="images/jackets/C-00C3singlelayerchestfusing.png" class="backbtnsugg"/>
                <p class="suggestionname">C-00C3 Single Layer Chest Fusing</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-00D1nochestcanvas">
                <img src="images/jackets/C-00D1nochestcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">C-00D1 No Chest Canvas</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_C-OAABfullhandmadehalfcanvas">
                <img src="images/jackets/C-OAABfullhandmadehalfcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">C-OAAB Full Hand Made Half Canvas</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_fullyhandmadefullcanvas">
                <img src="images/jackets/fullyhandmadefullcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">Fully Hand Made Full Canvas</p>
            </div>
			
			<div class="primarysuggestion backbtnsuggparentdiv" id="canvasoption_semihandmadehalfcanvas">
                <img src="images/jackets/semihandmadehalfcanvas.png" class="backbtnsugg"/>
                <p class="suggestionname">Semi Hand Made Half Canvas</p>
            </div>
			
        </div>
        <!------
        -----pant color div
        ------
        -------->
        <div class="col-md-12" id="suitcolors" style="display:none;margin-bottom:70px">
            <p class="stylecaption">Suit Color Style</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0">
                <?php
                $query = "select * from wp_colors where type='suit' and status = '1'";
                $result = mysql_query($query);
                if($result)
                {
                    $num = mysql_num_rows($result);
                    if($num>0)
                    {
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            $displayImage = $rows['displayImage'];
                            ?>
                            <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' title="<?php echo $colorName ?>" id='suitcolor_<?php echo $colorName ?>'>
                                <img src='images/jackets/<?php echo $displayImage; ?>' />
								 <p class="suggestionname"><?php echo $colorName;?></p>
                            </div>
                            <?php
                        }
                    }
                    else
                    {
                        echo "No Color Found";
                    }
                }
                else
                {
                    echo mysql_error();
                }
                ?>
            </div>
			 <?php
                $query = "select * from wp_colors where type='allfabric' and status = '1'";
                $result = mysql_query($query);
                if($result)
                {
                    $num = mysql_num_rows($result);
                    if($num>0)
                    {
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            ?>
							<div class="primarysuggestion liningoptions" id="suitcolor_<?php echo $colorName;?>" style="padding: 19px;">
                            <p class="suggestionname" id="showfab"><?php echo $colorName;?></p>
							</div>
							
                            <?php
                        }
					}
					else
					{
						echo "";
					}
				}
				else
				{
					echo mysql_error();
				}
                ?>
					
				
				<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
					<input type="text" name="fabricno" id="textfabr" value="" style="margin-left: 13px; margin-top: -27px;"></br> <input type="submit" name="addfabric" onclick="addfabrics('second')" value="Add Fabric Number" style="margin-left: 13px; margin-top: 10px;">
				</div>
        </div>
    </div>
    <div class="col-md-6" style="text-align: center; margin-top: 100px; float: right;">
        <label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
        <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
    </div>
    <script>
	$( document ).ready(function() {
		$(".nav a").removeClass("active");
	});
        var currPage = "";
        var frontbutton = "onebutton";
        var suitcategory = "formal";
        var lapelstyle = "notch";
        var breastpocket = "topnormalbreastpocket";
        var lowerpocket = "normalpockets";
        var coinpocket = "C-320nocoinpocket";
        var shoulderstyle = "shoulderstyles";
        var elbowpatch = "elbowpatchs";
        var canvasoption = "canvasoptions";
        var lapelingredient = "lapelingredients";
        var chestdart = "chestdarts";
        var innerflap = "innerflaps";
        var facingstyle = "facingstyles";
        var feltcollar = "FLD-0110";
        var sleeveslit = "sleeveslits";
        var buttonoption = "buttonoptions";
        var threadoption = "threadoptions";
        var backvent = "centeralvent";
        var suitcolor = "dbk053a";
        var shirt = "shirt";
        var tie = "tie";
        var jacket = "jacket";
        var folder = "front";
        var armbutton = "armbutton";
        var sidearmbutton = "sidearmbutton";
        var sidearmbuttoncut = "sidearmbuttoncut";
        var sidearmbuttonpoint = "sidearmbuttonpoint";
        var liningstyle = "12lininginseamwithpinding";
        var buttonswatch = "kb001";
        function loadSessionData() {
            $(".loader").fadeIn("slow");
            var prevIndex = $(".stepsactive").attr("id");
            currPage = prevIndex + "s";
            var url = "admin/webserver/selectionData.php?type=getsession&" + prevIndex + "=" + prevIndex;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    var img = "";
                    var item = json.items;
                    var name = json.items;
                    item = prevIndex + "_" + item;
                    $(".primarysuggestion").removeClass("active");
                    $("#" + item).addClass("active");
                    if (prevIndex == "suitcategory") {
                        suitcategory = name;
                    }
                    else if (prevIndex == "frontbutton") {
                        frontbutton = name;
                    }
                    else if (prevIndex == "lapelstyle") {
                        lapelstyle = name;
                    }
					else if (prevIndex == "lapelingredient") {
                        lapelingredient = name;
                    }
                    else if (prevIndex == "chestdart") {
                        chestdart = name;
                    }
					else if (prevIndex == "feltcollar") {
                        feltcollar = name;
                    }
					else if (prevIndex == "innerflap") {
                        innerflap = name;
                    }
					else if (prevIndex == "facingstyle") {
                        facingstyle = name;
                    }
					else if (prevIndex == "sleeveslit") {
                        sleeveslit = name;
                    }
					else if (prevIndex == "breastpocket") {
                        breastpocket = name;
                    }
                    else if (prevIndex == "lowerpocket") {
                        lowerpocket = name;
                    }
					else if (prevIndex == "coinpocket") {
                        coinpocket = name;
                    }
					else if (prevIndex == "shoulderstyle") {
                        shoulderstyle = name;
                    }
					else if (prevIndex == "buttonoption") {
                        buttonoption = name;
                    }
					else if (prevIndex == "buttonswatch") {
                        buttonswatch = name;
                    }
					else if (prevIndex == "threadoption") {
                        threadoption = name;
                    }
					else if (prevIndex == "elbowpatch") {
                        elbowpatch = name;
                    }
                    else if (prevIndex == "canvasoption") {
                        canvasoption = name;
                    }
					else if (prevIndex == "backvent") {
                        backvent = name;
                    }
                    else if (prevIndex == "liningstyle") {
                        liningstyle = name;
                    }
                    else if (prevIndex == "suitcolor") {
                        suitcolor = name;
                    }
                    if (currPage == "suitcategorys") {
                        folder = "front";
                        if(suitcategory == "tuxedo")
                        {
                            tie = "bow";
                        }
                        else
                        {
                            tie = "tie";
                        }
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" +frontbutton+lapelstyle+".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "frontbuttons") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" +frontbutton+lapelstyle+".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "lapelstyles") {
                        folder="front";
                        if(frontbutton == "doublebreasted")
                        {
                            $("#notindoublebreasted").hide();
                        }
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "breastpockets") {
                        folder = "front";
                       $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "lowerpockets") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "backvents") {
                        folder = "back";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "liningstyles") {
                        folder = "back";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + liningstyle + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "shoulderstyles") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "elbowpatchs") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "canvasoptions") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "lapelingredients") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "chestdarts") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "feltcollars") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "innerflaps") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "facingstyles") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "sleeveslits") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "buttonoptions") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "buttonswatchs") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "threadoptions") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "coinpockets") {
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "suitcolors") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                }
                else {
                    var url = "";
                    if (currPage == "suitcategorys") {
                        folder = "front";
                        if(suitcategory == "tuxedo")
                        {
                            tie = "bow";
                        }
                        else
                        {
                            tie = "tie";
                        }
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + suitcategory;
                    }
                    else if (currPage == "frontbuttons") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + frontbutton;
                    }
                    else if (currPage == "lapelstyles") {
                        folder = "front";
                        if(frontbutton == "doublebreasted")
                        {
                            $("#notindoublebreasted").hide();
                        }
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + lapelstyle;
                    }
                    else if (currPage == "breastpockets") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + breastpocket;
                    }
                    else if (currPage == "lowerpockets") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + lowerpocket;
                    }
                    else if (currPage == "backvents") {
                        folder = "back";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + backvent;
                    }
                    else if (currPage == "liningstyles") {
                        folder = "back";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + liningstyle + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + liningstyle;
                    }
					else if (currPage == "shoulderstyles") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + shoulderstyle;
                    }
					else if (currPage == "elbowpatchs") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + elbowpatch;
                    }
					else if (currPage == "canvasoptions") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + canvasoption;
                    }
					else if (currPage == "lapelingredients") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + lapelingredient;
                    }
					else if (currPage == "chestdarts") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + chestdart;
                    }
					else if (currPage == "feltcollars") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + feltcollar;
                    }
					else if (currPage == "innerflaps") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + innerflap;
                    }
					else if (currPage == "facingstyles") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + facingstyle;
                    }
					else if (currPage == "sleeveslits") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + sleeveslit;
                    }
					else if (currPage == "buttonoptions") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + buttonoption;
                    }
					else if (currPage == "buttonswatchs") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + buttonswatch;
                    }
					else if (currPage == "threadoptions") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + threadoption;
                    }
					else if (currPage == "coinpockets") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + coinpocket;
                    }
                    else if (currPage == "suitcolors") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + suitcolor;
                    }
                    $.get(url, function (data) {
                        var json = $.parseJSON(data);
                        var status = json.status;
                        if (status == "done") {
                            $(".loader").fadeOut("slow");
                        }
                    }).fail(function () {
                        alert("page is not available");
                    });
                }
            });
        }
        $(".primarysuggestion").click(function () {
            var Img = "";
            $(".loader").fadeIn("slow");
            $(".primarysuggestion").removeClass("active");
            var value = this.id;
            var type = value.split("_");
            var selection = type[1];
            $("#" + value).addClass("active");
            var currpage = type[0] + "s";
            var url = "admin/webserver/selectionData.php?" + type[0] + "=" + selection;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    if (type[0] == "suitcategory") {
                        suitcategory = selection;
                    }
                    else if (type[0] == "frontbutton") {
                        frontbutton = selection;
                    }
                    else if (type[0] == "lapelstyle") {
                        lapelstyle = selection;
                    }
                    else if (type[0] == "breastpocket") {
                        breastpocket = selection;
                    }
                    else if (type[0] == "lowerpocket") {
                        lowerpocket = selection;
                    }
					else if (type[0] == "coinpocket") {
                        coinpocket = selection;
                    }
                    else if (type[0] == "backvent") {
                        backvent = selection;
                    }
                    else if (type[0] == "liningstyle") {
                        liningstyle = selection;
                    }
					else if (type[0] == "shoulderstyle") {
                        shoulderstyle = selection;
                    }
					else if (type[0] == "elbowpatch") {
						
                        elbowpatch = selection;
                    }
					else if (type[0] == "canvasoption") {
                        canvasoption = selection;
                    }
					else if (type[0] == "lapelingredient") {
                        lapelingredient = selection;
                    }
					else if (type[0] == "chestdart") {
                        chestdart = selection;
                    }
					else if (type[0] == "feltcollar") {
                        feltcollar = selection;
                    }
					else if (type[0] == "innerflap") {
                        innerflap = selection;
                    }
					else if (type[0] == "facingstyle") {
                        facingstyle = selection;
                    }
					else if (type[0] == "sleeveslit") {
                        sleeveslit = selection;
                    }
					else if (type[0] == "buttonoption") {
                        buttonoption = selection;
                    }
					else if (type[0] == "buttonswatch") {
                        buttonswatch = selection;
                    }
					else if (type[0] == "threadoption") {
                        threadoption = selection;
                    }
                    else if (type[0] == "suitcolor") {
                        suitcolor = selection;
                    }
                    if (currpage == "suitcategorys") {
                        folder = "front";
                        if(suitcategory == "tuxedo")
                        {
                            tie = "bow";
                        }
                        else
                        {
                            tie = "tie";
                        }
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "frontbuttons") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "lapelstyles") {
                        folder = "front";
                        if(frontbutton == "doublebreasted")
                        {
                            $("#notindoublebreasted").hide();
                        }
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "breastpockets") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "lowerpockets") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "backvents") {
                        folder = "back";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "liningstyles") {
                        folder = "back";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + liningstyle + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "shoulderstyles") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "elbowpatchs") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "canvasoptions") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "lapelingredients") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "chestdarts") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "feltcollars") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "innerflaps") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "facingstyles") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "sleeveslits") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "buttonoptions") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "buttonswatchs") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "threadoptions") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currpage == "coinpockets") {
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "suitcolors") {
                        folder = "front";
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $(".loader").fadeOut("slow");
                    }
                }
                else {
                    alert(status);
                    $(".loader").fadeOut("slow");
                }
            }).fail(function () {
                alert("page is not available");
                $(".loader").fadeOut("slow");
            });
        });
        function navigation(switcher) {
            $("#" + currPage).fadeOut("slow");
            if (switcher == "next") {
                if (currPage == "suitcategorys") {
                    $(".steps").removeClass("stepsactive");
                    $("#frontbuttons").fadeIn("slow");
                    $("#frontbutton").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "frontbuttons") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelstyles").fadeIn("slow");
                    $("#lapelstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lapelstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelingredients").fadeIn("slow");
                    $("#lapelingredient").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "lapelingredients") {
                    $(".steps").removeClass("stepsactive");
                    $("#chestdarts").fadeIn("slow");
                    $("#chestdart").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "chestdarts") {
                    $(".steps").removeClass("stepsactive");
                    $("#feltcollars").fadeIn("slow");
                    $("#feltcollar").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "feltcollars") {
                    $(".steps").removeClass("stepsactive");
                    $("#innerflaps").fadeIn("slow");
                    $("#innerflap").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "innerflaps") {
                    $(".steps").removeClass("stepsactive");
                    $("#facingstyles").fadeIn("slow");
                    $("#facingstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "facingstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#sleeveslits").fadeIn("slow");
                    $("#sleeveslit").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "sleeveslits") {
                    $(".steps").removeClass("stepsactive");
                    $("#breastpockets").fadeIn("slow");
                    $("#breastpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "breastpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#lowerpockets").fadeIn("slow");
                    $("#lowerpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lowerpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#coinpockets").fadeIn("slow");
                    $("#coinpocket").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "coinpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#backvents").fadeIn("slow");
                    $("#backvent").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "backvents") {
                    $(".steps").removeClass("stepsactive");
                    $("#liningstyles").fadeIn("slow");
                    $("#liningstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "liningstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#shoulderstyles").fadeIn("slow");
                    $("#shoulderstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "shoulderstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonoptions").fadeIn("slow");
                    $("#buttonoption").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "buttonoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonswatchs").fadeIn("slow");
                    $("#buttonswatch").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "buttonswatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#threadoptions").fadeIn("slow");
                    $("#threadoption").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "threadoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#elbowpatchs").fadeIn("slow");
                    $("#elbowpatch").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "elbowpatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#canvasoptions").fadeIn("slow");
                    $("#canvasoption").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "canvasoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#suitcolors").fadeIn("slow");
                    $("#suitcolor").addClass("stepsactive");
                    $("#next").html("Add to Cart");
                    loadSessionData();
                }
                else if (currPage == "suitcolors") {
                    var user_id = $("#user_id").val();
                    //alert(user_id);
                    var randomnum = Math.floor((Math.random() * 600000) + 1);
                    var product_name = "CustomSuit" + randomnum;
                    var url = "admin/webserver/addto_cart.php?product_type=Custom Suit&suit_category="+suitcategory+"&product_name="+product_name+
                    "&product_button="+frontbutton+"&product_vent="+backvent+"&product_collar="+lapelstyle
                    +"&product_color="+suitcolor+"&breast_pocket="+breastpocket+"&lower_pocket="+lowerpocket
                    +"&product_price="+179.32+"&user_id="+user_id+"&quantity="+1+"&cart_product_total_amount="+179.32;
                    $.get(url, function (data) {
                        var json = $.parseJSON(data)
                        var status = json.status;
                        if (status == "done") {
                            window.location = "cart.php";
                        }
                        else {
                            alert(status);
                        }
                    });
                }
            }
            else if (switcher == "previous") {
                if (currPage == "suitcategorys") {
                    window.location = "customize.php";
                }
                else if (currPage == "frontbuttons") {
                    $(".steps").removeClass("stepsactive");
                    $("#suitcategorys").fadeIn("slow");
                    $("#suitcategory").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lapelstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#frontbuttons").fadeIn("slow");
                    $("#frontbutton").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "lapelingredients") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelstyles").fadeIn("slow");
                    $("#lapelstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "chestdarts") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelingredients").fadeIn("slow");
                    $("#lapelingredient").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "feltcollars") {
                    $(".steps").removeClass("stepsactive");
                    $("#chestdarts").fadeIn("slow");
                    $("#chestdart").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "innerflaps") {
                    $(".steps").removeClass("stepsactive");
                    $("#feltcollars").fadeIn("slow");
                    $("#feltcollar").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "facingstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#innerflaps").fadeIn("slow");
                    $("#innerflap").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "sleeveslits") {
                    $(".steps").removeClass("stepsactive");
                    $("#facingstyles").fadeIn("slow");
                    $("#facingstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "breastpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#sleeveslits").fadeIn("slow");
                    $("#sleeveslit").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lowerpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#breastpockets").fadeIn("slow");
                    $("#breastpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "coinpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#lowerpockets").fadeIn("slow");
                    $("#lowerpocket").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "backvents") {
                    $(".steps").removeClass("stepsactive");
                    $("#coinpockets").fadeIn("slow");
                    $("#coinpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "liningstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#backvents").fadeIn("slow");
                    $("#backvent").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "shoulderstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#liningstyles").fadeIn("slow");
                    $("#liningstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "buttonoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#shoulderstyles").fadeIn("slow");
                    $("#shoulderstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "buttonswatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonoptions").fadeIn("slow");
                    $("#buttonoption").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "threadoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonswatchs").fadeIn("slow");
                    $("#buttonswatch").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "elbowpatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#threadoptions").fadeIn("slow");
                    $("#threadoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "canvasoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#elbowpatchs").fadeIn("slow");
                    $("#elbowpatch").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "suitcolors") {
                    $(".steps").removeClass("stepsactive");
                    $("#canvasoptions").fadeIn("slow");
                    $("#canvasoption").addClass("stepsactive");
                    $("#next").html("Next");
                    loadSessionData();
                }
            }
        }
        function rotateImage(button) {
            $(".loader").fadeIn("slow");
            var source=[];

            source.push($("#frontbuttonimg").attr("src"));
            source.push($("#frontbuttonpointimg").attr("src"));
            source.push($("#lapelstyleimg").attr("src"));
            source.push($("#breastpocketimg").attr("src"));
            source.push($("#lowerpocketimg").attr("src"));
            source.push($("#frontbuttoncutimg").attr("src"));
            source.push($("#shirtimg").attr("src"));
            source.push($("#tieimg").attr("src"));
            source.push($("#jacketimg").attr("src"));
            source.push($("#armbuttonimg").attr("src"));
            /*alert(JSON.stringify(source));
            return false;*/
            for(var i = 0;i<source.length;i++) {
                if (source[i].indexOf("front/") >=0){
                    if (button == "left") {
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                    }
                    else if (button == "right") {
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + sidearmbutton + ".png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttoncut + ".png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttonpoint + ".png");
                    }
                }
                else if (source[i].indexOf("side/") >= 0) {
                    if (button == "left") {
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                    }
                    else if (button == "right") {
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                    }
               }
                else if (source[i].indexOf("back/") >= 0) {
                    if (button == "left") {
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + backvent + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + sidearmbutton + ".png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttoncut + ".png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttonpoint + ".png");
                    }
                    else if (button == "right") {
                        $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + shirt + ".png");
                        $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                    }
                }
            }
            $(".loader").fadeOut("slow");
        }
        $(".steps").click(function(){
            $("#"+currPage).hide(1000);
            var newcurrpage = this.id+"s";
            $(".steps").removeClass("stepsactive");
            $("#"+this.id).addClass("stepsactive");
            $("#"+newcurrpage).show(1000);
            currPage = newcurrpage;
            loadSessionData();
            if(currPage == "suitcolors")
            {
                $("#next").html("Add to Cart");
            }
            else
            {
                $("#next").html("Next");
            }
        });
        function showcolordiv()
        {
            $(".loader").fadeIn("slow");
            $("#frontbuttons").hide();
            $("#lapelstyles").hide();
            $("#breastpockets").hide();
            $("#lowerpockets").hide();
            $("#backvents").hide();
            $("#shoulderstyles").hide();
            $("#elbowpatchs").hide();
            $("#buttonoptions").hide();
            $("#buttonswatchs").hide();
            $("#canvasoptions").hide();
            $("#lapelingredients").hide();
            $("#chestdarts").hide();
            $("#innerflaps").hide();
            $("#threadoptions").hide();
            $("#coinpockets").hide();
            $("#facingstyles").hide();
            $("#feltcollars").hide();
            $("#sleeveslits").hide();
            $("#suitcolors").show();
            $(".steps").removeClass("stepsactive");
            $("#suitcolor").addClass("stepsactive");
            currPage = "suitcolors";
            $("#next").html("Add to Cart");
            var url="admin/webserver/selectionData.php?type=getParicularSession";
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                folder = "front";
                frontbutton = json.frontbutton;
                lapelstyle = json.lapelstyle;
                breastpocket = json.breastpocket;
                lowerpocket = json.lowerpocket;
                backvent = json.backvent;
                suitcolor = json.suitcolor;
                $("#jacketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + shirt + ".png");
                $("#tieimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + tie + ".png");
                $("#breastpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            });
        }
        loadSessionData();
		$('img').error(function(){
			$(this).attr('src', 'images/jackets/sugg/bawli.png');
		});
		$('#paddingpad').on('change', function() {
			  var opt = this.value;
				  if(opt =="03cm"){
					  $(".padding-size").css("height","80px");
				  } 
				  if(opt =="06cm"){
					  $(".padding-size").css("height","120px");
				  }
				  if(opt =="12cm"){
					  $(".padding-size").css("height","150px");
				  }
				  if(opt =="2cm"){
					  $(".padding-size").css("height","200px");
				  }
				  
			});
			
			
			function addfabrics(typp){
				if(typp =="first"){
					var textfab = $("#textfab").val();
					
				}
				if(typp =="second"){
					var textfab = $("#textfabr").val();
				}
					
					var url="admin/webserver/addfabric.php?type=allfabric&fabno="+textfab;
					$.get(url, function (data) {
						var json = $.parseJSON(data);
						 var status = json.status;
						 if (status == "done") {
                            //window.location = "";
                        }
                        else {
                            alert(status);
                        }
					});
				}
    </script>
    <?php
        if(isset($_REQUEST['fromtwo']))
        {
            ?>
            <script>
                 showcolordiv();
            </script>
            <?php
        }
    ?>
	</div>