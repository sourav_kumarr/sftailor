<?php
session_start();
$status = "init";
error_reporting(0);
if($_REQUEST)
{
    $response = array();
    if(isset($_REQUEST['sessionType'])) {
        if($_REQUEST['sessionType'] == "storeSession") {
            $type = $_REQUEST['type'];
            $content_id = $_REQUEST['content_id'];
            if($type === 'custom_suits') {
                $_SESSION['content_id'] = $content_id;
                $response['content_id'] = $_SESSION['content_id'];

            }
            else if($type === 'custom_pants') {
                $_SESSION['custom_pants'] = $content_id;
                $response['custom_pants'] = $_SESSION['custom_pants'];

            }
            else if($type === 'custom_vests') {
                $_SESSION['custom_vests'] = $content_id;
                $response['custom_vests'] = $_SESSION['custom_vests'];
            }
            else if($type === 'custom_shirts') {
                $_SESSION['custom_shirts'] = $content_id;
                $response['custom_shirts'] = $_SESSION['custom_shirts'];
            }
            else if($type === 'custom_overcoats') {
                $_SESSION['custom_overcoats'] = $content_id;
                $response['custom_overcoats'] = $_SESSION['custom_overcoats'];
            }
            $response['status'] = "done";

            echo json_encode($response);
        }
        else if($_REQUEST['sessionType'] == "getSession") {
            $type = $_REQUEST['type'];
            if($type == "custom_suit") {
                if(!isset($_SESSION['content_id'])) {
                    $response['status'] = "error";
                    echo json_encode($response);
                    return false;
                }
                $response['status'] = "done";
                $content_id = $_SESSION['content_id'];
                $response['content'] = $content_id;
                $response['suitcategory'] = $_SESSION['content_id'];
                $response['csuitcategory'] = $_SESSION['csuitcategory'];
                $response['frontbutton'] = $_SESSION['frontbutton'];
                $response['cfrontbutton'] = $_SESSION['cfrontbutton'];
                $response['lapelstyle'] = $_SESSION['lapelstyle'];
                $response['clapelstyle'] = $_SESSION['clapelstyle'];
                $response['lapelbuttonholes'] = $_SESSION['lapelbuttonholes'];
                $response['lapelbuttonholesstyle'] = $_SESSION['lapelbuttonholesstyle'];
                $response['lapelbuttonholesthread'] = $_SESSION['lapelbuttonholesthread'];
                $response['lapelingredient'] = $_SESSION['lapelingredient'];
                $response['lapelsatin'] = $_SESSION['lapelsatin'];
                $response['chestdart'] = $_SESSION['chestdart'];
                $response['feltcollar'] = $_SESSION['feltcollar'];
                $response['cfeltcollar'] = $_SESSION['cfeltcollar'];
                $response['innerflap'] = $_SESSION['innerflap'];
                $response['facingstyle'] = $_SESSION['facingstyle'];
                $response['sleeveslit'] = $_SESSION['sleeveslit'];
                $response['sleeveslitthreadc0619'] = $_SESSION['sleeveslitthreadc0619'];
                $response['sleeveslitthread'] = $_SESSION['sleeveslitthread'];
                $response['breastpocket'] = $_SESSION['breastpocket'];
                $response['cbreastpocket'] = $_SESSION['cbreastpocket'];
                $response['lowerpocket'] = $_SESSION['lowerpocket'];
                $response['clowerpocket'] = $_SESSION['clowerpocket'];
                $response['coinpocket'] = $_SESSION['coinpocket'];
                $response['backvent'] = $_SESSION['backvent'];
                $response['cbackvent'] = $_SESSION['cbackvent'];
                $response['liningstyle'] = $_SESSION['liningstyle'];
                $response['cliningstyle'] = $_SESSION['cliningstyle'];
                $response['liningoption'] = $_SESSION['liningoption'];
                $response['lining'] = $_SESSION['lining'];
                $response['shoulderstyle'] = $_SESSION['shoulderstyle'];
                $response['shoulderpadding'] = $_SESSION['shoulderpadding'];
                $response['threadoption'] = $_SESSION['threadoption'];
                $response['buttonswatch'] = $_SESSION['buttonswatch'];
                $response['cthreadoption'] = $_SESSION['cthreadoption'];
                $response['elbowpatch'] = $_SESSION['elbowpatch'];
                $response['elbowpatchcolor'] = $_SESSION['elbowpatchcolor'];
                $response['canvasoption'] = $_SESSION['canvasoption'];
                $response['suitcolor'] = $_SESSION['suitcolor'];
                $response['csuitcolor'] = $_SESSION['csuitcolor'];
                $response['shirt'] = $_SESSION['shirt'];
                $response['tie'] = $_SESSION['tie'];
                $response['jacket'] = $_SESSION['jacket'];
                $response['folder'] = $_SESSION['folder'];
                $response['armbutton'] = $_SESSION['armbutton'];
                $response['sidearmbutton'] = $_SESSION['sidearmbutton'];
                $response['sidearmbuttoncut'] = $_SESSION['sidearmbuttoncut'];
                $response['sidearmbuttonpoint'] = $_SESSION['sidearmbuttonpoint'];
                $response['monogramtext'] = $_SESSION['monogramtext'];
                $response['monogramfont'] = $_SESSION['monogramfont'];
                $response['monogramcode'] = $_SESSION['monogramcode'];
                $response['monogramcolor'] = $_SESSION['monogramcolor'];
                $response['monogramposition'] = $_SESSION['monogramposition'];
                $response['monogramposition'] = $_SESSION['monogramposition'];
                $response['monogramoption'] = $_SESSION['monogramoption'];
                echo json_encode($response);
            }
            else if($type === 'custom_pants') {

                if(!isset($_SESSION['custom_pants'])) {
                    $response['status'] = "error";
                    echo json_encode($response);
                    return false;
                }
                $response['status'] = "done";
                $response['content'] = isset($_SESSION['custom_pants'])?$_SESSION['custom_pants']:'';
                $response['pantcategory'] = isset($_SESSION['pantcategory'])?$_SESSION['pantcategory']:'';
                $response['frontpocket'] = isset($_SESSION['frontpocket'])?$_SESSION['frontpocket']:'';
                $response['cfrontpocket'] = isset($_SESSION['cfrontpocket'])?$_SESSION['cfrontpocket']:'';
                $response['pleatstyle'] = isset($_SESSION['pleatstyle'])?$_SESSION['pleatstyle']:'';
                $response['cpleatstyle'] = isset($_SESSION['cpleatstyle'])?$_SESSION['cpleatstyle']:'';
                $response['watchpocket'] = isset($_SESSION['watchpocket'])?$_SESSION['watchpocket']:'';
                $response['cwatchpocket'] = isset($_SESSION['cwatchpocket'])?$_SESSION['cwatchpocket']:'';
                $response['backpocket'] = isset($_SESSION['backpocket'])?$_SESSION['backpocket']:'';
                $response['backpocketsatin'] = isset($_SESSION['backpocketsatin'])?$_SESSION['backpocketsatin']:'';
                $response['cbackpocketsatin'] = isset($_SESSION['cbackpocketsatin'])?$_SESSION['cbackpocketsatin']:'';
                $response['cbackpocket'] = isset($_SESSION['cbackpocket'])?$_SESSION['cbackpocket']:'';
                $response['belt'] = isset($_SESSION['belt'])?$_SESSION['belt']:'';
                $response['pleat'] = isset($_SESSION['pleat'])?$_SESSION['pleat']:'';
                $response['beltloop'] = isset($_SESSION['beltloop'])?$_SESSION['beltloop']:'';
                $response['cbeltloop'] = isset($_SESSION['cbeltloop'])?$_SESSION['cbeltloop']:'';
                $response['pantbottom'] = isset($_SESSION['pantbottom'])?$_SESSION['pantbottom']:'';
                $response['cpantbottom'] = isset($_SESSION['cpantbottom'])?$_SESSION['cpantbottom']:'';
                $response['pantcolor'] = isset($_SESSION['pantcolor'])?$_SESSION['pantcolor']:'';
                $response['folder'] = isset($_SESSION['folder'])?$_SESSION['folder']:'';
                echo json_encode($response);

            }
            if($type == "custom_vests") {
                if (!isset($_SESSION['custom_vests'])) {
                    $response['status'] = "error";
                    echo json_encode($response);
                    return false;
                }
                $response['status'] = "done";
                $response['content'] = isset($_SESSION['custom_vests'])?$_SESSION['custom_vests']:'';
                $response['vestcategory'] = isset($_SESSION['vestcategory'])?$_SESSION['vestcategory']:'';
                $response['collarstyle'] = isset($_SESSION['collarstyle'])?$_SESSION['collarstyle']:'';
                $response['collarstyle_img'] = isset($_SESSION['collarstyle_img'])?$_SESSION['collarstyle_img']:'';
                $response['lapelbuttonhole'] = isset($_SESSION['lapelbuttonhole'])?$_SESSION['lapelbuttonhole']:'';
                $response['lapelbuttonhole_img'] = isset($_SESSION['lapelbuttonhole_img'])?$_SESSION['lapelbuttonhole_img']:'';
                $response['buttonholestyle'] = isset($_SESSION['buttonholestyle'])?$_SESSION['buttonholestyle']:'';
                $response['buttonholestyle_img'] = isset($_SESSION['buttonholestyle_img'])?$_SESSION['buttonholestyle_img']:'';
                $response['buttonstyle'] = isset($_SESSION['buttonstyle'])?$_SESSION['buttonstyle']:'';
                $response['buttonstyle_img'] = isset($_SESSION['buttonstyle_img'])?$_SESSION['buttonstyle_img']:'';
                $response['bottomstyle'] = isset($_SESSION['bottomstyle'])?$_SESSION['bottomstyle']:'';
                $response['bottomstyle_img'] = isset($_SESSION['bottomstyle_img'])?$_SESSION['bottomstyle_img']:'';
                $response['lowerpocket'] = isset($_SESSION['lowerpocket'])?$_SESSION['lowerpocket']:'';
                $response['lowerpocket_img'] = isset($_SESSION['lowerpocket_img'])?$_SESSION['lowerpocket_img']:'';
                $response['backstyle'] = isset($_SESSION['backstyle'])?$_SESSION['backstyle']:'';
                $response['backstyle_img'] = isset($_SESSION['backstyle_img'])?$_SESSION['backstyle_img']:'';
                $response['chestdart'] = isset($_SESSION['chestdart'])?$_SESSION['chestdart']:'';
                $response['chestdart_img'] = isset($_SESSION['chestdart_img'])?$_SESSION['chestdart_img']:'';
                $response['topstitch'] = isset($_SESSION['topstitch'])?$_SESSION['topstitch']:'';
                $response['topstitch_img'] = isset($_SESSION['topstitch_img'])?$_SESSION['topstitch_img']:'';
                $response['placketstyle'] = isset($_SESSION['placketstyle'])?$_SESSION['placketstyle']:'';
                $response['placketstyle_img'] = isset($_SESSION['placketstyle_img'])?$_SESSION['placketstyle_img']:'';
                $response['tuxedostyle'] = isset($_SESSION['tuxedostyle'])?$_SESSION['tuxedostyle']:'';
                $response['placketstyle_img'] = isset($_SESSION['placketstyle_img'])?$_SESSION['placketstyle_img']:'';
                $response['tuxedostyle'] = isset($_SESSION['tuxedostyle'])?$_SESSION['tuxedostyle']:'';
                $response['tuxedostyle_img'] = isset($_SESSION['tuxedostyle_img'])?$_SESSION['tuxedostyle_img']:'';
                $response['breastpocket'] = isset($_SESSION['breastpocket'])?$_SESSION['breastpocket']:'';
                $response['breastpocket_img'] = isset($_SESSION['breastpocket_img'])?$_SESSION['breastpocket_img']:'';
                $response['chestpocket'] = isset($_SESSION['chestpocket'])?$_SESSION['chestpocket']:'';
                $response['chestpocket_img'] = isset($_SESSION['chestpocket_img'])?$_SESSION['chestpocket_img']:'';
                $response['threadcolor'] = isset($_SESSION['threadcolor'])?$_SESSION['threadcolor']:'';
                $response['threadcolor_img'] = isset($_SESSION['threadcolor_img'])?$_SESSION['threadcolor_img']:'';
                $response['linningoption'] = isset($_SESSION['linningoption'])?$_SESSION['linningoption']:'';
                $response['linningoption_img'] = isset($_SESSION['linningoption_img'])?$_SESSION['linningoption_img']:'';
                $response['vestfabric'] = isset($_SESSION['vestfabric'])?$_SESSION['vestfabric']:'';
                $response['shirt'] = isset($_SESSION['shirt'])?$_SESSION['shirt']:'';
                echo json_encode($response);

            }

            if($type == "custom_shirts") {
                if (!isset($_SESSION['custom_shirts'])) {
                    $response['status'] = "error";
                    echo json_encode($response);
                    return false;
                }
                $response['status'] = "done";
                $response['content'] = isset($_SESSION['custom_shirts'])?$_SESSION['custom_shirts']:'';
                $response['shirtcategory'] = isset($_SESSION['shirtcategory'])?$_SESSION['shirtcategory']:'';
                $response['collar'] = isset($_SESSION['collar'])?$_SESSION['collar']:'';
                $response['ccollar'] = isset($_SESSION['ccollar'])?$_SESSION['ccollar']:'';
                $response['collarbuttondown'] = isset($_SESSION['collarbuttondown'])?$_SESSION['collarbuttondown']:'';
                $response['doublelayeroption'] = isset($_SESSION['doublelayeroption'])?$_SESSION['doublelayeroption']:'';
                $response['frontcollar'] = isset($_SESSION['frontcollar'])?$_SESSION['frontcollar']:'';
                $response['collarbelt'] = isset($_SESSION['collarbelt'])?$_SESSION['collarbelt']:'';
                $response['collarbutton'] = isset($_SESSION['collarbutton'])?$_SESSION['collarbutton']:'';
                $response['cuff'] = isset($_SESSION['cuff'])?$_SESSION['cuff']:'';
                $response['ccuff'] = isset($_SESSION['ccuff'])?$_SESSION['ccuff']:'';
                $response['cuffwidth'] = isset($_SESSION['cuffwidth'])?$_SESSION['cuffwidth']:'';
                $response['placket'] = isset($_SESSION['placket'])?$_SESSION['placket']:'';
                $response['cplacket'] = isset($_SESSION['cplacket'])?$_SESSION['cplacket']:'';
                $response['placketbutton'] = isset($_SESSION['placketbutton'])?$_SESSION['placketbutton']:'';
                $response['pocket'] = isset($_SESSION['pocket'])?$_SESSION['pocket']:'';
                $response['cpocket'] = isset($_SESSION['cpocket'])?$_SESSION['cpocket']:'';
                $response['bottom'] = isset($_SESSION['bottom'])?$_SESSION['bottom']:'';
                $response['cbottom'] = isset($_SESSION['cbottom'])?$_SESSION['cbottom']:'';
                $response['shirtcolor'] = isset($_SESSION['shirtcolor'])?$_SESSION['shirtcolor']:'';
                $response['buttoncolor'] = isset($_SESSION['buttoncolor'])?$_SESSION['buttoncolor']:'';
                $response['buttoningstyle'] = isset($_SESSION['buttoningstyle'])?$_SESSION['buttoningstyle']:'';
                $response['buttoncutthread'] = isset($_SESSION['buttoncutthread'])?$_SESSION['buttoncutthread']:'';
                $response['holestichthread'] = isset($_SESSION['holestichthread'])?$_SESSION['holestichthread']:'';
                $response['monogramtext'] = isset($_SESSION['monogramtext'])?$_SESSION['monogramtext']:'';
                $response['monogramfont'] = isset($_SESSION['monogramfont'])?$_SESSION['monogramfont']:'';
                $response['monogramcode'] = isset($_SESSION['monogramcode'])?$_SESSION['monogramcode']:'';
                $response['monogramcolor'] = isset($_SESSION['monogramcolor'])?$_SESSION['monogramcolor']:'';
                $response['monogramposition'] = isset($_SESSION['monogramposition'])?$_SESSION['monogramposition']:'';
                $response['monogramoption'] = isset($_SESSION['monogramoption'])?$_SESSION['monogramoption']:'';
                $response['contrastfabric'] = isset($_SESSION['contrastfabric'])?$_SESSION['contrastfabric']:'';
                $response['contrast'] = isset($_SESSION['contrast'])?$_SESSION['contrast']:'';
                $response['selectionString'] = isset($_SESSION['selectionString'])?$_SESSION['selectionString']:'';
                $response['sleeve'] = isset($_SESSION['sleeve'])?$_SESSION['sleeve']:'';
                $response['shoulder'] = isset($_SESSION['shoulder'])?$_SESSION['shoulder']:'';
                echo json_encode($response);

            }

            if($type == "custom_overcoats") {
                if (!isset($_SESSION['custom_overcoats'])) {
                    $response['status'] = "error";
                    echo json_encode($response);
                    return false;
                }
                $response['status'] = "done";
                $content_id = $_SESSION['custom_overcoats'];
                $response['content'] = isset($_SESSION['custom_overcoats'])?$_SESSION['custom_overcoats']:'';
                $response['overcoatcategory'] = isset($_SESSION['overcoatcategory'])?$_SESSION['overcoatcategory']:'';
                $response['canvasstyle'] = isset($_SESSION['canvasstyle'])?$_SESSION['canvasstyle']:'';
                $response['canvasstyle_img'] = isset($_SESSION['canvasstyle_img'])?$_SESSION['canvasstyle_img']:'';
                $response['lapelstyle'] = isset($_SESSION['lapelstyle'])?$_SESSION['lapelstyle']:'';
                $response['lapelstyle_img'] = isset($_SESSION['lapelstyle_img'])?$_SESSION['lapelstyle_img']:'';
                $response['pocketstype'] = isset($_SESSION['pocketstype'])?$_SESSION['pocketstype']:'';
                $response['pocketstype_img'] = isset($_SESSION['pocketstype_img'])?$_SESSION['pocketstype_img']:'';
                $response['buttonstyle'] = isset($_SESSION['buttonstyle'])?$_SESSION['buttonstyle']:'';
                $response['buttonstyle_img'] = isset($_SESSION['buttonstyle_img'])?$_SESSION['buttonstyle_img']:'';
                $response['sweatpadstyle'] = isset($_SESSION['sweatpadstyle'])?$_SESSION['sweatpadstyle']:'';
                $response['sweatpadstyle_img'] = isset($_SESSION['sweatpadstyle_img'])?$_SESSION['sweatpadstyle_img']:'';
                $response['elbowpadstyle'] = isset($_SESSION['elbowpadstyle'])?$_SESSION['elbowpadstyle']:'';
                $response['elbowpadstyle_img'] = isset($_SESSION['elbowpadstyle_img'])?$_SESSION['elbowpadstyle_img']:'';
                $response['shoulderstyle'] = isset($_SESSION['shoulderstyle'])?$_SESSION['shoulderstyle']:'';
                $response['shoulderstyle_img'] = isset($_SESSION['shoulderstyle_img'])?$_SESSION['shoulderstyle_img']:'';
                $response['shoulderpatchstyle'] = isset($_SESSION['shoulderpatchstyle'])?$_SESSION['shoulderpatchstyle']:'';
                $response['shoulderpatchstyle_img'] = isset($_SESSION['shoulderpatchstyle_img'])?$_SESSION['shoulderpatchstyle_img']:'';
                $response['shouldertabstyle'] = isset($_SESSION['shouldertabstyle'])?$_SESSION['shouldertabstyle']:'';
                $response['shouldertabstyle_img'] = isset($_SESSION['shouldertabstyle_img'])?$_SESSION['shouldertabstyle_img']:'';
                $response['sleeveslitstyle'] = isset($_SESSION['sleeveslitstyle'])?$_SESSION['sleeveslitstyle']:'';
                $response['sleeveslitstyle_img'] = isset($_SESSION['sleeveslitstyle_img'])?$_SESSION['sleeveslitstyle_img']:'';
                $response['sleevebuttonstyle'] = isset($_SESSION['sleevebuttonstyle'])?$_SESSION['sleevebuttonstyle']:'';
                $response['sleevebuttonstyle_img'] = isset($_SESSION['sleevebuttonstyle_img'])?$_SESSION['sleevebuttonstyle_img']:'';
                $response['backvent'] = isset($_SESSION['backvent'])?$_SESSION['backvent']:'';
                $response['stitchstyle'] = isset($_SESSION['stitchstyle'])?$_SESSION['stitchstyle']:'';
                $response['stitchstyle_img'] = isset($_SESSION['stitchstyle_img'])?$_SESSION['stitchstyle_img']:'';
                $response['backsideofarmhole'] = isset($_SESSION['backsideofarmhole'])?$_SESSION['backsideofarmhole']:'';
                $response['backsideofarmhole_img'] = isset($_SESSION['backsideofarmhole_img'])?$_SESSION['backsideofarmhole_img']:'';
                $response['topstichstyle'] = isset($_SESSION['topstichstyle'])?$_SESSION['topstichstyle']:'';
                $response['topstichstyle_img'] = isset($_SESSION['topstichstyle_img'])?$_SESSION['topstichstyle_img']:'';
                $response['breastpocket'] = isset($_SESSION['breastpocket'])?$_SESSION['breastpocket']:'';
                $response['breastpocket_img'] = isset($_SESSION['breastpocket_img'])?$_SESSION['breastpocket_img']:'';
                $response['coinpocket'] = isset($_SESSION['coinpocket'])?$_SESSION['coinpocket']:'';
                $response['coinpocket_img'] = isset($_SESSION['coinpocket_img'])?$_SESSION['coinpocket_img']:'';
                $response['facingstyle'] = isset($_SESSION['facingstyle'])?$_SESSION['facingstyle']:'';
                $response['facingstyle_img'] = isset($_SESSION['facingstyle_img'])?$_SESSION['facingstyle_img']:'';
                $response['pocketstyle'] = isset($_SESSION['pocketstyle'])?$_SESSION['pocketstyle']:'';
                $response['pocketstyle_img'] = isset($_SESSION['pocketstyle_img'])?$_SESSION['pocketstyle_img']:'';
                $response['collarstyle'] = isset($_SESSION['collarstyle'])?$_SESSION['collarstyle']:'';
                $response['collarstyle_img'] = isset($_SESSION['collarstyle_img'])?$_SESSION['collarstyle_img']:'';
                $response['liningstyle'] = isset($_SESSION['liningstyle'])?$_SESSION['liningstyle']:'';
                $response['liningstyle_img'] = isset($_SESSION['liningstyle_img'])?$_SESSION['liningstyle_img']:'';
                $response['liningoption'] = isset($_SESSION['liningoption'])?$_SESSION['liningoption']:'';
                $response['liningoption_img'] = isset($_SESSION['liningoption_img'])?$_SESSION['liningoption_img']:'';
                $response['threadcolor'] = isset($_SESSION['threadcolor'])?$_SESSION['threadcolor']:'';
                $response['overcoatfabric'] = isset($_SESSION['overcoatfabric'])?$_SESSION['overcoatfabric']:'';

                echo json_encode($response);

            }
        }
    }
    else if(isset($_REQUEST['type']))
    {
        if($_REQUEST['type'] == "getsession")
        {
            $keys = array_keys($_REQUEST);
            $key=$keys[1];
            $item = $_SESSION[$key];

            if($item != "")
            {
                $status = "done";
            }
            else
            {
                $status = "error";
            }
            $response['status'] = $status;
            $response['items'] = $item;
            echo json_encode($response);
        }
        elseif($_REQUEST['type'] == "getParicularSession")
        {
            $frontbutton = $_SESSION['frontbutton'];
            $lapelstyle = $_SESSION['lapelstyle'];
            $breastpocket = $_SESSION['breastpocket'];
            $lowerpocket = $_SESSION['lowerpocket'];
            $backvent = $_SESSION['backvent'];
            $suitcolor = $_SESSION['suitcolor'];
            $response['frontbutton'] = $frontbutton;
            $response['lapelstyle'] = $lapelstyle;
            $response['breastpocket'] = $breastpocket;
            $response['lowerpocket'] = $lowerpocket;
            $response['backvent'] = $backvent;
            $response['suitcolor'] = $suitcolor;
            echo json_encode($response);
        }
        elseif($_REQUEST['type'] == "getParicularSessionshirts")
        {
            $collar = $_SESSION['collar'];
            $cuff = $_SESSION['cuff'];
            $placket = $_SESSION['placket'];
            $pocket = $_SESSION['pocket'];
            $pleat = $_SESSION['pleat'];
            $shirtcolor = $_SESSION['shirtcolor'];
            $response['collar'] = $collar;
            $response['cuff'] = $cuff;
            $response['placket'] = $placket;
            $response['pocket'] = $pocket;
            $response['pleat'] = $pleat;
            $response['shirtcolor'] = $shirtcolor;
            echo json_encode($response);
        }
        elseif($_REQUEST['type'] == "getParicularSessionpant")
        {
            $category = $_SESSION['category'];
            $frontpocket = $_SESSION['frontpocket'];
            $backpocket = $_SESSION['backpocket'];
            $beltloop = $_SESSION['beltloop'];
            $pantbottom = $_SESSION['pantbottom'];
            $pantcolor = $_SESSION['pantcolor'];
            $response['category'] = $category;
            $response['frontpocket'] = $frontpocket;
            $response['backpocket'] = $backpocket;
            $response['beltloop'] = $beltloop;
            $response['pantbottom'] = $pantbottom;
            $response['pantcolor'] = $pantcolor;
            echo json_encode($response);
        }
    }
    else {
        $keys = array_keys($_REQUEST);
        for ($i = 0; $i < count($keys); $i++) {
            $key = $keys[$i];
            $_SESSION[$key] = $_REQUEST[$key];
        }
        $status = "done";
        $response['status']=$status;
        $response['data']=$_SESSION;
        echo json_encode($response);
    }
}
?>