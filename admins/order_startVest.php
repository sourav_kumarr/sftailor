<?php
include "header.php";
include("front_header.php");
include "../api/Constants/dbConfig.php";
include "../api/Classes/CONNECT.php";
?>
    <style>
        .headingstartshirt{
            font-size: 19px;
            font-weight: bold;
            margin-bottom: 20px;
            margin-left: 15px;
            text-align: left;
            text-decoration: underline;
        }
        .col-md-6 > li {
            font-size: 18px;
            list-style: square;
        }
        .nav_menu{
            margin-bottom: 0px;
        }
    </style>
    <?php
        $conn = new \Modals\CONNECT();
        $link = $conn->Connect();
    ?>
<div class="right_col" role="main" style="overflow-y: auto">
    <div class="col-md-12" style="text-align:center;">
        <p style="font-size:30px">sftailor's Custom-Made Vest's </p>
        <hr>
        <div class="col-md-12">
            <p class="headingstartshirt">CUSTOM VEST</p>
            <div class="col-md-6">
                <label style="text-align:left">We begin with top tier fabrics as they tailor distinctly better.
                    The result is a more elegant suit with a notable difference in ease of movement, durability and a
                    cleaner silhouette.<br><br>Our suits offer a sharp silhouette and elegant style at best in class
                    pricing. We only use genuine and high-quality interlinings, linings, canvassing, buttons and
                    stitching.<br><br>Each suit comes with a complementary custom monogram.<br><br>
                    Turnaround time is 2-3 weeks.
                </label>
            </div>
            <div class="col-md-6">
                <img src="../images/vest/vest.jpg" class="img-responsive" />
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom:50px">
        <hr>
        <div class="col-md-12">
            <p class="headingstartshirt">RETAIL PRICE</p>
            <div class='col-md-12'>
                <?php
                $query = mysqli_query($link,"select * from categories where cat_type = 'Vest'");
                if($query){
                    while($rows = mysqli_fetch_array($query)) {
                        echo "<div class='col-md-4' style='padding:0'><label>".$rows['cat_description']."</label><br>";
                        echo $rows['cat_name']." $".$rows['cat_price']."</div>";
                    }
                    echo "<div style='clear:both'></div>";
                }
                ?>
                <hr>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories");
                if($query){
                    while($rows = mysqli_fetch_array($query)){
                        if($rows['cat_type'] == "Suits2pc") {
                            echo "<label>".$rows['cat_description']."</label><br>";
                            echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                        }
                    }
                }
                ?>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories");
                if($query){
                    while($rows = mysqli_fetch_array($query)){
                        if($rows['cat_type'] == "Suits3pc") {
                            echo "<label>".$rows['cat_description']."</label><br>";
                            echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                        }
                    }
                }
                ?>
            </div>
            <div class='col-md-4'>
                <?php
                $query = mysqli_query($link,"select * from categories");
                if($query){
                    while($rows = mysqli_fetch_array($query)){
                        if($rows['cat_type'] == "Jacket") {
                            echo "<label>".$rows['cat_description']."</label><br>";
                            echo $rows['cat_name']." $" . $rows['cat_price']."<br><br>";
                        }
                    }
                }
                ?>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-bottom:50px">
        <hr>
        <div class="col-md-12">
            <div class="col-md-8">
                <p class="headingstartshirt">Specifications</p>
                <div class="col-md-6">
                    <li>Hand finished lapels and pockets</li>
                    <li>Milanese lapel buttonhole</li>
                    <li>Custom cut high armholes</li>
                    <li>Working buttonholes</li>
                    <li>Full canvassing</li>
                </div>
                <div class="col-md-6">
                    <li>Signature semi-roped shoulder</li>
                    <li>Natural waist suppression</li>
                    <li>Custom monogramming</li>
                    <li>Hand sewn armholes</li>
                    <li>Hand pressing</li>
                </div>
            </div>
            <div class="col-md-4">
                <input type="button" value="Order Custom Vests >>" onclick="window.location='order_customvest.php?userId=<?php echo $user_id; ?>'" class="btn btn-danger btn-lg" style="margin:50px" />
            </div>
        </div>
    </div>
</div>
<?php
include "footer.php";
?>
<script>
    var user_id = $("#user_id").val();
    getCart(user_id);
</script>
