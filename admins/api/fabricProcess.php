<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 9/11/2017
 * Time: 6:12 PM
 */
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
require_once('Constants/functions.php');
require_once ('Classes/excel_reader2.php');
require_once('Classes/FabricClass.php');
require_once('Classes/StockClass.php');
require_once('Classes/CONNECT.php');
require_once('Classes/USERCLASS.php');
error_reporting(0);

$userClass = new Classes\USERCLASS();
$helplerClass = new \Classes\FabricClass();
$stockHelplerClass = new \Classes\StockClass();

$requiredfields = array('dataType');
($response = RequiredFields($_REQUEST, $requiredfields));

if($response['Status'] == 'Failure'){
    $userClass->apiResponse($response);
    return false;
}
$dataType = $_REQUEST['dataType'];

if($dataType == 'storeData') {
    $requiredfields = array('fabricType');
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $link = new \Classes\CONNECT();
    $storeData = $link->storeImage('fabricFile','excel');
    if($storeData[Status] == Error) {
        $userClass->apiResponse($storeData);
        return false;
    }
    $data = new Spreadsheet_Excel_Reader($storeData['File_Path']);
    $validExcel = $helplerClass->validExcelSheet($data);
    if($validExcel[Status] == Error) {
        $userClass->apiResponse($validExcel);
        return false;
    }

    if(count($data->sheets[0]['cells'])>0) // checking sheet not empty
    {
        for($j=1;$j<=count($data->sheets[0]['cells']);$j++) // loop used to get each row of the sheet
        {
            $helplerClass->setSerialNo($data->sheets[0]['cells'][$j][1]);
            $helplerClass->setCameoCode($data->sheets[0]['cells'][$j][2]);
            $helplerClass->setRcCode($data->sheets[0]['cells'][$j][3]);
            $helplerClass->setRecmtm($data->sheets[0]['cells'][$j][4]);
            $helplerClass->setYarn($data->sheets[0]['cells'][$j][5]);
            $helplerClass->setWeight($data->sheets[0]['cells'][$j][6]);
            $helplerClass->setCatalog($data->sheets[0]['cells'][$j][7]);
            $helplerClass->setPattern($data->sheets[0]['cells'][$j][8]);
            $helplerClass->setAttribute($data->sheets[0]['cells'][$j][9]);
            $helplerClass->setColor($data->sheets[0]['cells'][$j][10]);
            $helplerClass->setUsDollar($data->sheets[0]['cells'][$j][11]);
            $helplerClass->setFabricType($data->sheets[0]['cells'][$j][12]);
            //$helplerClass->setType($_REQUEST['fabricType']);
            $saveData = $helplerClass->save();
        }
    }
    $response[Status] = Success;
    $response[Message] = "data saved successfully";
    $userClass->apiResponse($response);
    unlink($storeData['File_Path']);
}
else if($dataType == 'addStockData') {

    $link = new \Classes\CONNECT();
    $storeData = $link->storeImage('stockFile','excel');
    if($storeData[Status] == Error) {
        $userClass->apiResponse($storeData);
        return false;
    }
    $data = new Spreadsheet_Excel_Reader($storeData['File_Path']);
    $validExcel = $stockHelplerClass->validExcelSheet($data);
    if($validExcel[Status] == Error) {
        $userClass->apiResponse($validExcel);
        return false;
    }
    if(count($data->sheets[0]['cells'])>0) // checking sheet not empty
    {
        for($j=1;$j<=count($data->sheets[0]['cells']);$j++) // loop used to get each row of the sheet
        {

            $stockHelplerClass->setCameoCode($data->sheets[0]['cells'][$j][3]);
            $stockHelplerClass->setTotalStock($data->sheets[0]['cells'][$j][6]);
            $stockHelplerClass->setStockSatus($data->sheets[0]['cells'][$j][7]);
            $stockHelplerClass->setStockType($data->sheets[0]['cells'][$j][9]);
            if($data->sheets[0]['cells'][$j][7] == ""){
                $stockHelplerClass->setStockSatus("Continued");
            }
            $saveData = $stockHelplerClass->addStock();
           /* if($saveData[Status] == Error) {
                $userClass->apiResponse($saveData);
                return false;
            }*/

        }
    }
    $response[Status] = Success;
    $response[Message] = "data saved successfully";
    $userClass->apiResponse($response);
}

else if($dataType == 'getFabricData') {
    $requiredfields = array('fabricType');

    ($response = RequiredFields($_REQUEST, $requiredfields));

    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $helplerClass->setType($_REQUEST['fabricType']);
    $response = $helplerClass->getFabricData();
    $userClass->apiResponse($response);
}
else if($dataType == 'updateFabricData') {

    $requiredfields = array('id','rcmtm','yarn','weight','catalog','pattern','attribute','color','price');
    ($response = RequiredFields($_REQUEST, $requiredfields));

    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }

    $helplerClass->setRecmtm($_REQUEST['rcmtm']);
    $helplerClass->setYarn($_REQUEST['yarn']);
    $helplerClass->setWeight($_REQUEST['weight']);
    $helplerClass->setCatalog($_REQUEST['catalog']);
    $helplerClass->setPattern($_REQUEST['pattern']);
    $helplerClass->setAttribute($_REQUEST['attribute']);
    $helplerClass->setColor($_REQUEST['color']);
    $helplerClass->setUsDollar($_REQUEST['price']);
    $helplerClass->setId($_REQUEST['id']);
    $helplerClass->total_stock($_REQUEST['total_stock']);
    $helplerClass->stock_status($_REQUEST['stock_status']);

    $updateData = $helplerClass->updateFabric();
    $userClass->apiResponse($updateData);
}

else if($dataType == 'deleteFabricData') {
    $requiredfields = array('id');
    ($response = RequiredFields($_REQUEST, $requiredfields));

    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $helplerClass->setId($_REQUEST['id']);
    $updateData = $helplerClass->deleteFabric();
    $userClass->apiResponse($updateData);
}
/* edit fabricdabase*/
else if($dataType == 'editPrice') {
    $requiredfields = array('price_id','price_amount','price_amount_meter','uplifts_buttons');
    ($response = RequiredFields($_REQUEST, $requiredfields));
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $price_id = $_REQUEST['price_id'];
    $price_amount = $_REQUEST['price_amount'];
    $price_amount_meter = $_REQUEST['price_amount_meter'];
    $uplifts_buttons = $_REQUEST['uplifts_buttons'];

    $updateData = $helplerClass->editPrice($price_id,$price_amount,$price_amount_meter,$uplifts_buttons);
    $userClass->apiResponse($updateData);
}
else if($dataType == 'editBtnPrice') {
    $requiredfields = array('price_id','price_amount','uplifts_buttons_Btn','no_of_button');
    ($response = RequiredFields($_REQUEST, $requiredfields));
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $price_id = $_REQUEST['price_id'];
    $price_amount = $_REQUEST['price_amount'];
    $uplifts_buttons_Btn = $_REQUEST['uplifts_buttons_Btn'];
    $no_of_button = $_REQUEST['no_of_button'];

    $updateData = $helplerClass->editBtnPrice($price_id,$price_amount,$uplifts_buttons_Btn,$no_of_button);
    $userClass->apiResponse($updateData);
}

else if($dataType == 'searchFabricData') {
    $requiredfields = array('cameo_code');
    ($response = RequiredFields($_REQUEST, $requiredfields));

    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $helplerClass->setCameoCode($_REQUEST['cameo_code']);

    $searchData = $helplerClass->searchFabricData();
    $userClass->apiResponse($searchData);
}

else if($dataType == 'searchFabricData1') {
    $requiredfields = array('cameo_code');
    ($response = RequiredFields($_REQUEST, $requiredfields));

    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $helplerClass->setCameoCode($_REQUEST['cameo_code']);

    $searchData = $helplerClass->searchFabricData1();
    $userClass->apiResponse($searchData);
}


else if($dataType == 'updateManualRow') {

    $requiredfields = array('fabric_id');
    ($response = RequiredFields($_REQUEST, $requiredfields));

    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }

    $fabric_id = $_REQUEST['fabric_id'];
    $fabric_inc = $_REQUEST['fabric_inc'];

    $updateData = $userClass->updateFabricInc($fabric_id,$fabric_inc);
    $userClass->apiResponse($updateData);
}

