<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('CONNECT.php');
class USERCLASS
{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct()
    {
        error_reporting(0);
        $this->link = new CONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function registerUser($username,$email,$contactNumber,$address,$registersource,$file_name)
    {
        $link = $this->link->connect();
        if($link) {
            $emailResponse = $this->checkEmailExistence($email);
            if($emailResponse[STATUS] == Error){
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $emailResponse[MESSAGE];
                $this->response['UserData'] = $emailResponse['UserData'];
            }else{
                $token = $this->generateToken();
                /*$expiry_date = "";
                if ($renewal_type == "Monthly") {
                    $days = $this->link->currentMonthDays();
                    $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
                } elseif ($renewal_type == "Yearly") {
                    $days = $this->link->YearDays();
                    $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
                }*/
                $query = "insert into users (user_name,user_email,user_contact,user_status,user_token,user_address,
                register_source,email_verified,user_profile) VALUES ('$username','$email','$contactNumber','1','$token',
                '$address','$registersource','No','$file_name')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $last_id = mysqli_insert_id($link);
                    $userResponse = $this->getParticularUserData($last_id);
                    $UserData = $userResponse['UserData'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Verification Link has been Sent to your Registered E-Mail Address ";
                    $this->response['UserData'] = $UserData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateFabricInc($fabric_id,$fabric_inc) {
        $link = $this->link->connect();
        if($link) {
            $query = "update fabric set fabric_inc='$fabric_inc' where fabric_id='$fabric_id'";
            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_affected_rows($link);
                if($num>0) {
                    $response[Status] = Success;
                    $response[Message] = "Fabric Row successfully ";
                }
                else{
                    $response[Status] = Error;
                    $response[Message] = "No changes found in record";
                }
                return $response;
            }
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }

    }
    public function generateToken()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0,15)];
        }
        return $randomString;
    }
    public function checkEmailExistence($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email = '$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "E-Mail Address Already Registered";
                    $row = mysqli_fetch_assoc($result);
                    $this->response['UserData'] = $row;
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Now Register";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from wp_customers where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function updateUser($userId,$username,$email,$contactNumber,$token,$address)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update users set user_name='$username',user_email='$email',
            user_contact='$contactNumber',user_token = '$token',user_address = '$address' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updatePlan($userId,$plan_id, $renewal_type, $auto_renewal)
    {
        $link = $this->link->connect();
        if($link) {
            $expiry_date = "";
            if ($renewal_type == "Monthly") {
                $days = $this->link->currentMonthDays();
                $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
            } elseif ($renewal_type == "Yearly") {
                $days = $this->link->YearDays();
                $expiry_date = ($this->currentDateTimeStamp + (86400 * $days)) - 86400;
            }
            $query = "update users set active_plan='$plan_id',activation_date='$this->currentDateTimeStamp',
            plan_expiry_date='$expiry_date',renewal_type = '$renewal_type',
            auto_renewal = '$auto_renewal' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    ///////////////////////////////////////////////////////
    public function getUserDataFromEmail($email)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email='$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response["UserImageBaseURL"] = UserImageBaseURL;
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function statusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET user_status='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function feddStatusChange($feedId,$feedValue){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_feedback where feed_id='$feedId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE wp_feedback SET status='$feedValue' WHERE feed_id='$feedId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function renewalStatusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET auto_renewal='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteUser($user_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from users WHERE user_id='$user_id'");
                    $update2 = mysqli_query($link, "delete from wp_customers WHERE user_id='$user_id'");
                    if ($update && $update2) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $query = "select * from wp_customers where user_id='$user_id'";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $num = mysqli_num_rows($result);
                        if ($num > 0) {
                            $update2 = mysqli_query($link, "delete from wp_customers WHERE user_id='$user_id'");
                            if ($update2) {
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Users Has Been Deleted Successfully";
                            } else {
                                $this->response[STATUS] = Error;
                                $this->response[MESSAGE] = $this->link->sqlError();
                            }
                        }
                        else{
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = "UnAuthorized Access";
                        }
                    }


                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteFeedback($feedId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_feedback where feed_id='$feedId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from wp_feedback WHERE feed_id='$feedId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Feedback Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteStylebox($u_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from stylebox where u_id='$u_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from stylebox WHERE u_id='$u_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "StyleBox Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteFaq($u_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from faqs where u_id='$u_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from faqs WHERE u_id='$u_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Faq Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function addNewFaq($category,$question,$answer){
        $link = $this->link->connect();
        if($link) {
            $query = "insert into faqs (type,ques,ans) values ('$category','$question','$answer')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Faq Has Been Added Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function updateFaq($u_id,$category,$question,$answer){
        $link = $this->link->connect();
        if($link) {
            $query = "update faqs set type='$category',ques='$question',ans='$answer' where u_id = '$u_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Faq Has Been Updated Successfully";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function editStyleData($u_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from stylebox where u_id='$u_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $styleData = array();
                    $rows = mysqli_fetch_assoc($result);
                    $styleData = $rows;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "StyleBox Has Been Deleted Successfully";
                    $this->response['styleData'] = $styleData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function editFaqData($u_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from faqs where u_id='$u_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $faqData = array();
                    $rows = mysqli_fetch_assoc($result);
                    $faqData = $rows;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "StyleBox Has Been Deleted Successfully";
                    $this->response['faqData'] = $faqData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function checkMeasureExist($user_id){
        echo "userid >".$user_id;
        $link = $this->link->connect();
        if($link) {
            $query = "select * from final_measurements where user_id='$user_id' order by measurement_id DESC";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                   return $this->response[STATUS] = Success;
                }
                else{
                    return $this->response[STATUS] = Error;
                }
            }
        }
        return $this->response[STATUS] = Error;
    }
    public function getAllUsersMeasure(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $usersData=array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $userId = $rows['user_id'];
                        $query1 = "select * from final_measurements where user_id='$userId'";
                        $result1 = mysqli_query($link,$query1);
                        if($result1){
                            $num1 = mysqli_num_rows($result1);
                            if($num1 > 0){
                                $usersData[]=$rows;
                            }
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsers(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $usersData=array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $usersData[]=$rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsersData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_customers order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=$rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllQuesData($userId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from style_genie where user_id='$userId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=array(
                            "user_id"=>$rows["user_id"],
                            "shirt_que_1"=>$rows["shirt_que_1"],
                            "suit_que_1"=>$rows["suit_que_1"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['quesData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function addFeedback($name,$from,$feedback_text,$feedback_description)
    {
        $link = $this->link->connect();
        if($link) {
                $query = "insert into wp_feedback (f_name,f_from,feedback_text,feedback_description,created_date,status)
                VALUES ('$name','$from','$feedback_text','$feedback_description',now(),'0')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Your Feedback submited successfully!!";
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateFeedBack($feedId,$name,$from,$feedback_text,$feedback_description)
    {
        $link = $this->link->connect();
        if($link) {
                $query = "update wp_feedback set f_name='$name',f_from ='$from',feedback_text='$feedback_text',
                feedback_description = '$feedback_description' where feed_id = '$feedId' ";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Your Feedback update successfully!!";
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function updateStylebox($title,$description, $price, $u_id,$ImageChanged){
        $link = $this->link->connect();
        if($link) {
            if($ImageChanged == "yes"){
                $file_tmp = $_FILES['stylebox_file2']['tmp_name'];
                $file_name = $_FILES['stylebox_file2']['name'];
                $file_ext=strtolower(end(explode('.',$file_name)));
                $expensions= array("jpeg","jpg","png","gif");
                if(in_array($file_ext,$expensions)=== false){
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Selection of File";
                    return false;
                }else {
                    $file_name = rand(100000, 999999) . "." . $file_ext;
                    move_uploaded_file($file_tmp, "Files/images/stylebox/" . $file_name);
                    $query = "update stylebox set title='$title',price ='$price',description='$description', 
                    image='$file_name' where u_id = '$u_id'";
                }
            }
            else{
                $query = "update stylebox set title='$title',price ='$price',description='$description' 
                where u_id = '$u_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Data updated successfully!!";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getFeedbackData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_feedback order by feed_id desc";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $feedData=array();
                    while($rows = mysqli_fetch_array($result)){
                        $feedData[]=array(
                            "feed_id"=>$rows["feed_id"],
                            "f_name"=>$rows["f_name"],
                            "f_from"=>$rows["f_from"],
                            "feedback_text"=>$rows["feedback_text"],
                            "feedback_description"=>$rows["feedback_description"],
                            "feedback_status"=>$rows["status"],
                            "created_date"=>$rows["created_date"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['feedData'] = $feedData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getFaqData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from faqs order by u_id desc";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $faqData = array();
                    while($rows = mysqli_fetch_array($result)){
                        $faqData[]=array(
                            "u_id"=>utf8_encode($rows['u_id']),
                            "type"=>utf8_encode($rows['type']),
                            "ques"=>utf8_encode($rows['ques']),
                            "ans"=>utf8_encode($rows['ans'])
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['faqData'] = $faqData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getStyleboxData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from stylebox order by u_id desc";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $StyleboxData = array();
                    while($rows = mysqli_fetch_array($result)){
                        $StyleboxData[]=array(
                            "u_id"=>utf8_encode($rows['u_id']),
                            "image"=>utf8_encode($rows['image']),
                            "title"=>utf8_encode($rows['title']),
                            "description"=>utf8_encode($rows['description']),
                            "price"=>utf8_encode($rows['price'])
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['StyleboxData'] = $StyleboxData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function addStylebox($title,$description,$price){
        $link = $this->link->connect();
        if($link) {
            if(isset($_FILES['stylebox_file'])){
                $file_tmp = $_FILES['stylebox_file']['tmp_name'];
                $file_name = $_FILES['stylebox_file']['name'];
                $file_ext=strtolower(end(explode('.',$file_name)));
                $expensions= array("jpeg","jpg","png","gif");
                if(in_array($file_ext,$expensions)=== false){
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Selection of File";
                }else{
                    $file_name = rand(100000,999999).".".$file_ext;
                    move_uploaded_file($file_tmp,"Files/images/stylebox/".$file_name);
                    $query = "insert into stylebox (title,description,price,image) values ('$title','$description',
                    '$price','$file_name')";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Data Inserted Successfuly";
                    }
                    else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "File Not Selected";
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getPartFeedback($feedId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_feedback where feed_id = '$feedId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $feedData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['feedData'] = $feedData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getPaymentData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from payment_manage order by id desc";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $paymentData[]=array(
                            "id"=>$rows["id"],
                            "payment_id"=>$rows["payment_id"],
                            "status"=>$rows["status"],
                            "add_on"=>$rows["add_on"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['paymentData'] = $paymentData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getPartPaymentData($payId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from payment_manage where id = '$payId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $payData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['payData'] = $payData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getActivePaymentId(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from payment_manage where status = '1'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $payData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['payData'] = $payData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function updatePaymentId($paymentId,$payId)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update payment_manage set payment_id='$paymentId' where id = '$payId' ";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Your Payment Id update successfully!!";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function deletePaymentId($payId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from payment_manage where id='$payId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from payment_manage WHERE id='$payId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Feedback Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function payStatusChange($payId,$payValue){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from payment_manage where id='$payId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE payment_manage SET status='$payValue' WHERE id='$payId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function paymentIdAdd($paymentId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from payment_manage where payment_id='$paymentId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "This payment id is already exist.Please try another one.";
                }
                else {
                    $addPaymentId = mysqli_query($link, "insert into payment_manage (payment_id,status)
                    values('$paymentId','0')");
                    if ($addPaymentId) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Paymnet Id add Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getSalesData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from sales_tax";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $paymentData[]=array(
                            "id"=>$rows["id"],
                            "sales_amount"=>$rows["sales_amount"],
                            "created_date"=>$rows["created_date"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['paymentData'] = $paymentData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function getPartSalesData($payId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from sales_tax where id = '$payId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $payData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['payData'] = $payData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function updateSalesId($paymentId,$payId)
    {
        $link = $this->link->connect();
        if($link) {
            $query = "update sales_tax set sales_amount='$paymentId' where id = '$payId' ";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Sales Amount update successfully!!";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editPriceCat($priceId,$oldcategoryImagechanged) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldcategoryImagechanged == "yes") {
            if(isset($_FILES['newcatimg']['tmp_name'])) {
                $file_name = $_FILES['newcatimg']['name'];
                if (strpos($file_name, "'") >= 0) {
                    $file_name = str_replace("'", "", $file_name);
                }
                $file_tmp = $_FILES['newcatimg']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['newcatimg']['name'])));
                if (in_array($file_ext, $extensions) === true) {
                    $db_file_name = "price_".rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/price_cat/" . $db_file_name)) {
//                        move_uploaded_file($file_tmp, getcwd() . "/Files/images/partner/" . $file_name);
                        $file_path = "http://scan2fit.com/sftailor/admins/api/Files/images/price_cat/" . $db_file_name;
                        $query = "update categories set price_list_logo='$db_file_name' where cat_id = '$priceId'";
                    }
                    else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "getcwd is  ".getcwd();
                        return $this->response;
                    }
                }
                else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }
        else{
            //$query = "update price_list_cat set name='$new_title' where id = '$priceId'";
        }
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            $this->response["sql"] = $query;

            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }

    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}