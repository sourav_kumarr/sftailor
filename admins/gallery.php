<?php
include('header.php');
?>
<style>
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .main-grid-box {
        text-align: center;
        padding: 8px 8px 0px 8px;
        border: 1px solid #777;
        margin: 10px 6px;
        height: auto;
        background: #ebebeb;
    }
    .ext-img {
        image-orientation: from-image;
        max-width: 100%;
        max-height: 95%;
    }
    #galleryBox{
        max-height: 600px;
        overflow-y: auto;
    }
    .delico{
        position: absolute;
        top: 10px;
        right: 20px;
        color: #000;
        font-size: 15px;
        background: #fff;
        border-radius: 50%;
        text-align: center;
        height: 20px;
        width: 20px;
        padding-top: 2px;
        cursor: pointer;
    }
    .imagetitle {
        font-size: 12px;
        color: #777;
        font-weight: bold;
        background: rgba(0,0,0,0.8);
        padding: 4px;
        color: white;
        font-weight: 600;
    }
    .smallicon{
        font-size:18px;
        margin-right: 5px;
    }
    .evelist{
        margin-top:10px;
        font-size:18px;
        border-bottom:1px solid #eee;
        cursor:pointer;
    }
    .oldeventimg{
        height:100px;
        width:125px;
        image-orientation: from-image;
    }
    .oldgalleryimg{
        height:100px;
        width:125px;
        image-orientation :from-image;
    }
    .old_eve_img{
        position: absolute;
        top: 22px;
        height: 100px;
        width: 125px;
        opacity: 0;
        image-orientation: from-image;
    }
    .hide_now {
        display:none;
    }
    .percentprog {
        color: green;
        text-align: center;
        font-size: 17px;
        font-weight: bold;
        margin-top: 170px;
    }
    #neweventimg{
        image-orientation: from-image;
    }
    #galleryFile{
        image-orientation: from-image;
    }
    .new_gallery_img{
        image-orientation: from-image;
    }

</style>
<link href = "js/jquery-ui.css" rel = "stylesheet">
<script src="js/jquery-ui.js"></script>

<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>

<!--  page content  -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Gallery Image <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <div class="form-group form-inline">
                                <input type="button" class="btn btn-danger btn-sm" onclick="addEvent()" value=" + Add Image" />
                            </div>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--body part start here-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" id="galleryBox" style="display:none">
            <button class="btn btn-danger btn-sm backbtn" onclick="showEvent()"><i class="fa fa-caret-left"></i> Back</button>
            <div class="col-md-12 col-sm-12 col-xs-12" id="galleryBox2"></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="eventBox"></div>
    </div>
    <!--body part end here-->


</div>

<!-- /page content -->
<?php
include('footer.php');
?>
<!-- script -->
<script>
    function inputreadURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#neweventimg").change(function(){
        inputreadURL(this);
    });

    var idOrder = [];
    var idOrderAll = [];
    var oldEventImagechanged = "no";
    var oldGalleryImagechanged = "no";
    var currentEvent = "all";
    var gall_cat = "out";
    function getGalleryData(event) {
        gall_cat = "in";
        currentEvent = event;
        var url = "api/galleryProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url, {"type": "getGallery"}, function (data) {
            var status = data.Status;
            var galleryData = data.data;
            if (status == "Success") {
                /* showMessage(data.Message, "green");*/
                var galleryBox = "";
                if (event != "all") {
                    for (var a = 0; a < galleryData.length; a++) {
                        idOrderAll.push(galleryData[a].id);
                        if (unescape(event) === galleryData[a].event) {
                            galleryBox = galleryBox + '<div id="' + galleryData[a].sort_order + '" name="' + galleryData[a].id + '" class="col-sm-2 main-grid-box sortId_' + galleryData[a].sort_order + '">' +
                            '<img class="pdf-imgr ext-img" style="background-image: url(api/Files/images/gallery/' + galleryData[a].gallery_file + ');background-size: 100% auto;background-position: center center;height:200px;width: 100%;background-repeat:no-repeat;" src="images/trans.png" />' +
                            '<div class="action-point"><i class="fa fa-trash delico" onclick=confirmDelete_gallery("' + galleryData[a].id + '","' + encodeURI(galleryData[a].gallery_file) + '");>' +
                            '</i><i class="fa fa-edit delico" style="right:50px" ' +
                            'onclick=editGallery("' + galleryData[a].id + '","' + escape(galleryData[a].gallery_title) + '","' + escape(galleryData[a].gallery_file) + '")>' +
                            '</i></div><p class="imagetitle">' + (decodeURIComponent(galleryData[a].gallery_title)).substr(0,15)+"..." + '</p></div>';
                        }else{
                            galleryBox = galleryBox + '<div id="' + galleryData[a].sort_order + '" name="' + galleryData[a].id + '" class="col-sm-2 main-grid-box hide_now sortId_' + galleryData[a].sort_order + '">' +
                            '<img class="pdf-imgr ext-img" style="background-image: url(api/Files/images/gallery/' + galleryData[a].gallery_file + ');background-size: 100% auto;background-position: center center;height:200px;width: 100%;background-repeat:no-repeat;" src="images/trans.png" />' +
                            '<div class="action-point"><i class="fa fa-trash delico" onclick=confirmDelete_gallery("' + galleryData[a].id + '","' + encodeURI(galleryData[a].gallery_file) + '");>' +
                            '</i><i class="fa fa-edit delico" style="right:50px" ' +
                            'onclick=editGallery("' + galleryData[a].id + '","' + escape(galleryData[a].gallery_title) + '","' + escape(galleryData[a].gallery_file) + '")>' +
                            '</i></div><p class="imagetitle">' + (decodeURIComponent(galleryData[a].gallery_title)).substr(0,15)+"..." + '</p></div>';
                        }
                    }
                } else {
                    for (var a = 0; a < galleryData.length; a++) {
                        idOrder.push(galleryData[a].id);
                        galleryBox = galleryBox + '<div id="' + galleryData[a].sort_order + '" name="' + galleryData[a].id + '" class="col-sm-2 main-grid-box sortId_' + galleryData[a].sort_order + '" >' +
                        '<img class="pdf-imgr ext-img" style="background-image: url(api/Files/images/gallery/' + galleryData[a].gallery_file + ');background-size: 100% auto;background-position: center center;height:200px;width: 100%;background-repeat:no-repeat;" src="images/trans.png" />' +
                        '<div class="action-point"><i class="fa fa-trash delico" onclick=confirmDelete_gallery("' + galleryData[a].id + '","' + encodeURI(galleryData[a].gallery_file) + '");>' +
                        '</i><i class="fa fa-edit delico" style="right:50px" ' +
                        'onclick=editGallery("' + galleryData[a].id + '","' + escape(galleryData[a].gallery_title) + '","' + escape(galleryData[a].gallery_file) + '")>' +
                        '</i></div><p class="imagetitle">' + (decodeURIComponent(galleryData[a].gallery_title)).substr(0,15)+"..." + '</p></div>';
                    }
                }
                $("#eventBox").css("display", "none");
                $("#galleryBox").css("display", "block");
                $("#galleryBox2").html(galleryBox);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
            else {
                showMessage(data.Message, "red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request", "red");
        });
    }
    $('#galleryBox2').sortable({
        update: function(event, ui) {
            var sortOrder = $(this).sortable('toArray').toString();
            sortOrder = sortOrder.split(",");
            var updateOrderId = [];
            var updateSortOrder = [];
            for(var a=0; a < sortOrder.length; a++){
                var sortNumber = $(".sortId_"+sortOrder[a]).attr("name");
                sortNumber = parseInt(sortNumber);
                updateOrderId.push(sortNumber);
                updateSortOrder.push(a);
            }
            var url = "api/galleryProcess.php";
            $(".loaderdiv").removeClass("loaderhidden");
            $.post(url,{"type":'updateSortOrder',"sortOrder":updateSortOrder,"idOrder":updateOrderId},function (data) {
                var status = data.Status;
                if(status === "Success") {
//                    window.location.reload();
                    getGalleryData(currentEvent);
                }
            });
        }
    });

    $('#eventBox').sortable({
        update: function(event, ui) {
            var sortOrder = $(this).sortable('toArray').toString();
            sortOrder = sortOrder.split(",");
            var updateOrderId = [];
            var updateSortOrder = [];
            for(var a=0; a < sortOrder.length; a++){
                var sortNumber = $(".events_"+sortOrder[a]).attr("name");
                updateOrderId.push(sortNumber);
                updateSortOrder.push(a);
            }
            var url = "api/galleryProcess.php";
            $(".loaderdiv").removeClass("loaderhidden");
            $.post(url,{"type":'updateCatSortOrder',"sortOrder":updateSortOrder,"idOrder":updateOrderId},function (data) {
                var status = data.Status;
                if(status === "Success") {
                    $(".loaderdiv").addClass("loaderhidden");
//                    window.location.reload();
                    showEvent();
                }
            });
        }
    });


    function editGallery(gallery_id,gallery_title,gallery_image){
        gallery_title = unescape(gallery_title);
        gallery_image = unescape(gallery_image);
        $(".modal-title").html("<label style='color: green'>Edit Gallery Image!!!</label>");
        $(".modal-body").html("<p id='message'></p><div class='row'><div class='col-md-6'>" +
        "<div class='form-group'><label>Gallery Image</label><br><img src='api/Files/images/gallery/"+gallery_image+"' " +
        "class='oldgalleryimg' /><input type='file' onchange='readURL(this,"+'"oldgalleryimg"'+")' id='new_gallery_img'" +
        " class='old_eve_img'/></div></div><div class='col-md-6'><label>Image Title</label><input type='text' " +
        "class='form-control' placeholder='Enter Image Title' value='"+gallery_title+"' id='new_title' />" +
        "<div style='clear: both'></div><input type='button' value='Edit Gallery Info' class='btn btn-danger pull-right'" +
        " style='margin-top:20px' onclick=editGalleryNow('"+gallery_id+"') /></div></div>");
        $(".modal-footer").css("display","none");
        $("#myModal").modal("show");
    }
    function editGalleryNow(gallery_id){
        var new_title = $("#new_title").val();
        var new_gallery_img = $("#new_gallery_img").val();
        var data = new FormData();
        if(new_gallery_img !== ""){
            var file_new_gallery_img = document.getElementById('new_gallery_img');
            var new_gallery_image_ext = new_gallery_img.split(".");
            new_gallery_image_ext = new_gallery_image_ext[new_gallery_image_ext.length - 1];
            new_gallery_image_ext = new_gallery_image_ext.toLowerCase();
            if (new_gallery_image_ext == "jpg" || new_gallery_image_ext == "png" || new_gallery_image_ext == "gif") {
                data.append('newgalleryimg', file_new_gallery_img.files[0]);
                oldGalleryImagechanged = "yes";
            }
            else{
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color","red");
                $("#subbtn").attr("disabled",false);
            }
        }
        if(new_title !== ""){
            $("#message").html("");
            $("#subbtn").attr("disabled",true);
            $(".loaderdiv").removeClass("loaderhidden");
            data.append('type', 'editGallery');
            data.append('gallery_id', gallery_id);
            data.append('gallery_title', new_title);
            data.append('oldGalleryImagechanged',oldGalleryImagechanged);
            var request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState == 4){
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if(status == "Success"){
                        getGalleryData(currentEvent);
                        $("#myModal").modal("hide");
                    }
                    else{
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                        $("#message").css("color","red");
                        $("#subbtn").attr("disabled",false);
                        return false;
                    }
                }
            };
            request.open('POST', 'api/galleryProcess.php');
            request.send(data);
        }
        else{
            $("#message").html("please fill required fields");
            $("#message").css("color","red");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    function confirmDelete_gallery(id,path) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
        'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
        'onclick=deleteData_gallery("'+id+'","'+path+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function deleteData_gallery(id,path) {
        var url ='api/galleryProcess.php';
        path = decodeURI(path);
        $.post(url,{"type":'deleteGallery','id':id,"path":path},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                getGalleryData(currentEvent);
                //location.reload();
            }
            else{
                $("#myModal").modal("hide");
                showMessage(message,"green");
                getGalleryData(currentEvent);
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    function addEvent(){
        var url='api/galleryProcess.php';
        $.post(url,{"type":"getDistinctEvent"},function(data){
            var Status = data.Status;
            var eventData = "<option value='' selected='selected'>Select Event</option>";
            if(Status === "Success") {
                var event = data.data;
                for(var i=0;i<event.length;i++){
                    eventData += "<option value='"+event[i].event_img+"' >"+event[i].event+"</option>";
                }
            }
            eventData += "<option value='addnew'>Add New Event</option>";
            $(".modal-title").html("<label style='color: green'>Add New Gallery Image</label>");
            $(".modal-body").html("<div class='row'><div class='col-md-12'><p class='text-center' id='imgError'" +
            " style='color:red'></p><div class='col-md-6 form-group'><label>Select Event Name</label>" +
            "<select onchange='showaddeventfield(this)' id='eventname' class='form-control' >"+eventData+"</select></div>" +
            "<div class='col-md-6 form-group' style='display:none' id='addnewfield' ><label>Add New Event Name</label>" +
            "<input type='text' id='neweventname' class='form-control' placeholder='Enter New Event Name'/></div>" +
            "<div class='col-md-6 form-group' style='display:none' id='event_img_div' ><label>Select Event Image</label>" +
            "<input type='file' id='neweventimg' class='form-control' /></div><div class='col-md-6 form-group'><label>Select Image</label>" +
            "<input type='file' name='fabricFile[]' multiple id='galleryFile' class='form-control'/></div><div class='col-md-6'>" +
            "<input type='button' class='btn btn-danger pull-right' style='margin-top:30px' onclick='addGalleryImage()'" +
            " id='subbtn' value='Submit'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
            $(".modal-footer").css('display', 'none');
            $("#myModal").modal("show");
        }).fail(function(){
            alert("Server Error !!! Please Try After Some Time");
        });
    }
    function showaddeventfield(obj){
        var selectVal = $("#"+obj.id).val();
        if(selectVal === "addnew"){
            $("#addnewfield").css("display","block");
            $("#event_img_div").css("display","block");
        }else{
            $("#addnewfield").css("display","none");
            $("#event_img_div").css("display","none");
        }
    }
    function addGalleryImage() {
        var iseventimg = "no";
        var galleryFile = $("#galleryFile").val();
        var eventselection = $("#eventname :selected").text();
        var eventselectionimg = $("#eventname").val();
        if(eventselection === "Add New Event"){
            if($("#neweventname").val() === "" || $("#neweventimg").val() === ""){
                $("#imgError").html("Please Enter Event Name and Event Image");
                return false;
            }
            eventselection = $("#neweventname").val();
            eventselectionimg = $("#neweventimg").val();
        }
        if(eventselection === "Select Event"){
            $("#imgError").html("Please Select Event First");
            return false;
        }
        else if(galleryFile == ""){
            $("#imgError").html("Please Select Gallery Image");
            return false;
        }
        var data = new FormData();
        $("#imgError").html("");
        $("#subbtn").attr("disabled",true);
        if($("#neweventimg").val() !== ""){
            var event_file = $("#neweventimg").val();
            var _file_eve = document.getElementById('neweventimg');
            var eve_image_ext = event_file.split(".");
            eve_image_ext = eve_image_ext[eve_image_ext.length - 1];
            eve_image_ext = eve_image_ext.toLowerCase();
            if (eve_image_ext == "jpg" || eve_image_ext == "png" || eve_image_ext == "gif") {
                data.append('eventimg', _file_eve.files[0]);
                iseventimg = "yes";
            }
            else{
                $("#imgError").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#subbtn").attr("disabled",false);
            }
        }
        var _file = document.getElementById('galleryFile');
        for(var i = 0; i < _file.files.length; ++i){
            data.append('file_[]', _file.files[i]);
        }
        data.append('type',"addGalleryImg");
        data.append('event', eventselection);
        data.append('iseventimg',iseventimg);
        data.append('oldeventimg',eventselectionimg);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#imgError").html("Succcess");
                    $("#subbtn").attr("disabled",false);
                    $("#myModal").modal("hide");

                    if(gall_cat == "in"){
                        getGalleryData(currentEvent);
                    }
                    else if(gall_cat == "out"){
                        showEvent();
                    }
                    else{
                        location.reload();
                    }

                   /* location.reload();*/
                }
                else
                {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#imgError").html(response.Message);
                    $("#subbtn").attr("disabled",false);
                    return false;
                }
            }
        };
        request.upload.addEventListener('progress', function(e){
            $('.percentprog').html(parseInt((e.loaded/e.total)*100) + '% Complete');
        }, false);
        request.open('POST', 'api/galleryProcess.php');
        request.send(data);
    }
    function showEvent(){
        gall_cat = "out";
        var url = "api/galleryProcess.php";
        $.post(url,{"type":"getDistinctEvent"},function(data) {
            var Status = data.Status;
            if (Status === "Success") {
                var event = data.data;
                var eventData = "";
                for (var i = 0; i < event.length; i++) {
                    //alert(event[i].event+" = "+event[i].sort_order_cat);
                    eventData += '<div id="'+event[i].sort_order_cat+'" name="'+event[i].event+'"  ' +
                        'class="col-sm-2 main-grid-box  events_'+event[i].sort_order_cat+'"  style="cursor:pointer" >' +
                        '<img style="background-image: url(api/Files/images/gallery/'+event[i].event_img+');background-size: 100% auto;background-position: center center;height:200px;width: 100%;background-repeat:no-repeat;" src="images/trans.png" class="pdf-imgr ext-img" ondblclick=getGalleryData("'+escape(event[i].event)+'");  ' +
                        ' />' +
                        '<i class="fa fa-trash delico" onclick=deleteEvent("'+escape(event[i].event)+'")>' +
                        '</i><i class="fa fa-edit delico" style="right:50px" onclick=editEvent("'+escape(event[i].event)+
                        '","'+escape(event[i].event_img)+'")></i><p class="imagetitle">'+(decodeURIComponent(event[i].event)).substr(0,18)+"..."+
                        '</p></div>';
                }
               /* eventData += '<div class="col-sm-2 main-grid-box" onclick=getGalleryData("all") ' +
                'style="cursor:pointer"><img src="" class="pdf-imgr ext-img" style="text-align: ' +
                'center;padding-top: 75px;font-size: 24px;text-decoration: underline">All Images <br> Click To View</img></div>';*/

                $("#galleryBox").css("display","none");
                $("#eventBox").css("display","block");
                $("#eventBox").html(eventData);
            }else{
                $("#galleryBox").css("display","none");
                $("#eventBox").css("display","block");
                $("#eventBox").html("<p class='text-center' style='font-size:22px;margin-top:50px'>No Images Found</p>");
            }
        });
    }
    function editEvent(old_event,oldimg){
        old_event = unescape(old_event);
        oldimg = unescape(oldimg);
        $(".modal-title").html("<label style='color: green'>Edit Event!!!</label>");
        $(".modal-body").html("<p id='message'></p><div class='row'><div class='col-md-6'><div class='form-group'>" +
        "<label>Event Image</label><br><img src='api/Files/images/gallery/"+oldimg+"' class='oldeventimg' />" +
        "<input type='file' onchange='readURL(this,"+'"oldeventimg"'+")' id='new_event_img' class='old_eve_img'/>" +
        "</div></div><div class='col-md-6'><label>Event Name</label><input type='text' class='form-control' " +
        "placeholder='Enter Event Name' value='"+old_event+"' id='new_event' /><div style='clear: both'></div>" +
        "<input type='button' value='Edit Event Name' class='btn btn-danger pull-right' style='margin-top:20px'" +
        " onclick=editEventNow('"+escape(old_event)+"','"+escape(oldimg)+"') /></div></div>");
        $(".modal-footer").css("display","none");
        $("#myModal").modal("show");
    }
    function editEventNow(old_event,oldimg){
        $(".modal-backdrop").hide();
        var new_event = $("#new_event").val();
        var new_eve_img = $("#new_event_img").val();
        var data = new FormData();
        if(new_eve_img !== ""){
            var new_event_file = $("#new_event_img").val();
            var new_file_eve = document.getElementById('new_event_img');
            var new_eve_image_ext = new_event_file.split(".");
            new_eve_image_ext = new_eve_image_ext[new_eve_image_ext.length - 1];
            new_eve_image_ext = new_eve_image_ext.toLowerCase();
            if (new_eve_image_ext == "jpg" || new_eve_image_ext == "png" || new_eve_image_ext == "gif") {
                data.append('neweventimg', new_file_eve.files[0]);
                oldEventImagechanged = "yes";
            }
            else{
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color","red");
                $("#subbtn").attr("disabled",false);
            }
        }
        if(new_event !== ""){
            $("#message").html("");
            $("#subbtn").attr("disabled",true);
            $(".loaderdiv").removeClass("loaderhidden");
            data.append('type', 'editEvent');
            data.append('old_event', unescape(old_event));
            data.append('new_event', new_event);
            data.append('oldEventImagechanged',oldEventImagechanged);
            data.append('oldEventImage',unescape(oldimg));
            var request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState == 4){
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if(status == "Success"){
                        location.reload();
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").css("color","green");
                    }
                    else{
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                        $("#message").css("color","red");
                        $("#subbtn").attr("disabled",false);
                        return false;
                    }
                }
            };
            request.open('POST','api/galleryProcess.php');
            request.send(data);
        }
        else{
            $("#message").html("please fill required fields");
            $("#message").css("color","red");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    function deleteEvent(event){
        event = unescape(event);
        $(".modal-title").html("<label style='color: green'>Confirm Delete !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>This Event Contain Images!!!. Are you sure you want " +
        "to delete "+event+" Event and Images Also</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>' +
        '<button type="button" class="btn btn-danger" onclick=deleteEventNow("'+escape(event)+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function deleteEventNow(event) {
        var url ='api/galleryProcess.php';
        $.post(url,{"type":'deleteEvent','event':unescape(event)},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                location.reload();
            }
            else{
                $("#myModal").modal("hide");
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    function readURL(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                if(targetClass == "oldgalleryimg"){
                    oldGalleryImagechanged = "yes";
                }else{
                    oldEventImagechanged = "yes";
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    showEvent();
</script>
