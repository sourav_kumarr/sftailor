<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('STORECONNECT.php');
class USERCLASS{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct(){
        $this->link = new STORECONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function registerUser($username,$email,$contactNumber,$address,$registersource,$file_name){
        $link = $this->link->connect();
        if($link) {
            $emailResponse = $this->checkEmailExistence($email);
            if($emailResponse[STATUS] == Error){
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $emailResponse[MESSAGE];
                $this->response['UserData'] = $emailResponse['UserData'];
            }else{
                $token = $this->generateToken();
                $query = "insert into users (user_name,user_email,user_contact,user_status,user_token,user_address,
                register_source,email_verified,user_profile) VALUES ('$username','$email','$contactNumber','1','$token',
                '$address','$registersource','No','$file_name')";
                $result = mysqli_query($link, $query);
                if ($result) {
                    $last_id = mysqli_insert_id($link);
                    $userResponse = $this->getParticularUserData($last_id);
                    $UserData = $userResponse['UserData'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Verification Link has been Sent to your Registered E-Mail Address ";
                    $this->response['UserData'] = $UserData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function generateToken(){
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $charactersLength; $i++) {
            $randomString .= $characters[rand(0,15)];
        }
        return $randomString;
    }
    public function checkEmailExistence($email){
        $link = $this->link->connect();
        if($link) {
            $query="select * from users where user_email = '$email'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "E-Mail Address Already Registered";
                    $row = mysqli_fetch_assoc($result);
                    $this->response['UserData'] = $row;
                }
                else{
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Now Register";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularUserData($userId){
        $link = $this->link->connect();
        if($link) {
            $query="select * from wp_customers where user_id='$userId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    $userData = mysqli_fetch_assoc($result);
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $this->response['UserData'] = $userData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid User";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    ////////////////////////////////////////////////
    public function getAllUsersDataofStore($store_name){
        $link = $this->link->connect();
        if($link) {
            $userData = array();
            $query = "select * from wp_orders where store_name = '$store_name'";
            $result = mysqli_query($link,$query);
            if($result){
                $num  = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_assoc($result)) {
                        $user_id = $rows['order_user_id'];
                        $userResp = $this->getParticularUserData($user_id);
                        $userData[] = array(
                            "user_id" => $userResp['UserData']['user_id'],
                            "name" => $userResp['UserData']['name'],
                            "b_city" => $userResp['UserData']['b_city'],
                            "mobile" => $userResp['UserData']['mobile'],
                            "email" => $userResp['UserData']['email'],
                            "gender" => $userResp['UserData']['gender'],
                            "profile_pic" => utf8_encode($userResp['UserData']['profile_pic'])
                        );
                    }
                }
            }
            $query2="select * from wp_customers where stylist='$store_name'";
            $result2 = mysqli_query($link,$query2);
            if($result2){
                $num2 = mysqli_num_rows($result2);
                if($num2>0) {
                    while($rows2 = mysqli_fetch_assoc($result2)) {
                        $userData[] = array(
                            "user_id"=>$rows2['user_id'],
                            "name"=>$rows2['name'],
                            "b_city"=>$rows2['b_city'],
                            "mobile"=>$rows2['mobile'],
                            "email"=>$rows2['email'],
                            "gender"=>$rows2['gender'],
                            "profile_pic"=>utf8_encode($rows2['profile_pic'])
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                }
                else{
                    if(sizeof($userData)>0){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Found";
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "No Users Found";
                    }
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
            if($this->response[Status] == Success) {
                //$userData = array_unique($userData);
                $this->response['UserData'] = $userData;
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }///////////////////////////////////////////
    public function getAllUsersDataOfStoreUsesStyleGenie($store_name){
        $link = $this->link->connect();
        if($link) {
            $userData = array();
            $query = "select * from wp_orders where store_name = '$store_name'";
            $result = mysqli_query($link,$query);
            if($result){
                $num  = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_assoc($result)) {
                        $user_id = $rows['order_user_id'];
                        $userResp = $this->getParticularUserData($user_id);
                        $userData[] = array(
                            "user_id"=>$userResp['UserData']['user_id'],
                            "name"=>$userResp['UserData']['name'],
                            "b_city"=>$userResp['UserData']['b_city'],
                            "mobile"=>$userResp['UserData']['mobile'],
                            "email"=>$userResp['UserData']['email'],
                            "gender"=>$userResp['UserData']['gender'],
                            "profile_pic"=>utf8_encode($userResp['UserData']['profile_pic'])
                        );
                    }
                }
            }
            $query="select * from wp_customers where stylist='$store_name'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($rows = mysqli_fetch_assoc($result)) {
                        $userData[] = array(
                            "user_id"=>$rows['user_id'],
                            "name"=>$rows['name'],
                            "b_city"=>$rows['b_city'],
                            "mobile"=>$rows['mobile'],
                            "email"=>$rows['email'],
                            "gender"=>$rows['gender'],
                            "profile_pic"=>utf8_encode($rows['profile_pic'])
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid User";
                    $userData = array_unique($userData);
                    $this->response['UserData'] = $userData;
                }
                else{
                    if(sizeof($userData)>0){
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Found";
                        $userData = array_unique($userData);
                        $this->response['UserData'] = $userData;
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "No Users Found";
                    }
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }

        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }///////////////////////////////////////////
    public function updateUser($userId,$username,$email,$contactNumber,$token,$address){
        $link = $this->link->connect();
        if($link) {
            $query = "update users set user_name='$username',user_email='$email',
            user_contact='$contactNumber',user_token = '$token',user_address = '$address' where user_id='$userId' ";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Success";
                $this->response['userId'] = $userId;
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllUsersMeasure(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $usersData=array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $userId = $rows['user_id'];
                        $query1 = "select * from final_measurements where user_id='$userId'";
                        $result1 = mysqli_query($link,$query1);
                        if($result1){
                            $num1 = mysqli_num_rows($result1);
                            if($num1 > 0){
                                $usersData[]=$rows;
                            }
                        }
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsers(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $usersData=array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $usersData[]=$rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    ///////////////////////////////////////////////////////
    public function statusChange($user_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE users SET user_status='$value' WHERE user_id='$user_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteUser($user_id){/////////////////////////////////
        $link = $this->link->connect();
        if($link) {
            $query = "select * from users where user_id='$user_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from users WHERE user_id='$user_id'");
                    $update2 = mysqli_query($link, "delete from wp_customers WHERE user_id='$user_id'");
                    if ($update && $update2) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Users Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }///////////////////////////
    public function getAllQuesData($userId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from style_genie where user_id='$userId'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $usersData = array();
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=array(
                            "user_id"=>$rows["user_id"],
                            "shirt_que_1"=>$rows["shirt_que_1"],
                            "suit_que_1"=>$rows["suit_que_1"],
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['quesData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function getAllUsersData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_customers order by user_id DESC";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_array($result)){
                        $usersData[]=$rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['Count'] = $num;
                    $this->response['usersData'] = $usersData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function apiResponse($response){
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}