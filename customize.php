<?php include("header.php");?>
<script>
    if ($("#user_id").val() == "") {
        window.location = "login.php?redurl=cp";
    }
</script>
<style>
.top-bar {
    background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
}
</style>
<div class="about-bottom wthree-3">
    <div class="container-fluid">
		<h2 class="tittle" style="margin-top: 50px; font-weight: 600;">order now</h2>
        <div class="agileinfo_about_bottom_grids">
            <div class="col-md-12 agileinfo_about_bottom_grid">
                <div class="col-md-12">
                    <div class="col-md-3">
                        <img id="left" src="images/coat.jpg" style="" />
                    </div>
                    <div class="col-md-4 custom_center">
                        <h2>Bespoke Menswear</h2>
                        <h2>Leisure, Business and Formal</h2>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-4">
                        <div class="col-md-6 custom_right" onclick="window.location='startJacket.php'">
                            <h1 class="cstm_hdr_btn">Jacket</h1>
                            <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>
                        <div class="col-md-6 custom_right" onclick="window.location='startVest.php'">
                            <h1 class="cstm_hdr_btn">Outfits</h1>
                            <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 custom_right" onclick="window.location='startGift.php'" style="float: right;margin-right: 5%;">
                            <h1 class="cstm_hdr_btn" style="font-size: 26px;">Gift Certificate</h1>
                            <p class="button_caption">Our Collection<i class="fa fa-gift" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>

                        <div class="col-md-6 custom_right" onclick="window.location='startVest.php'" >
                            <h1 class="cstm_hdr_btn">Vest</h1>
                            <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-6 custom_right" onclick="window.location='startpant.php'" >
                            <h1 class="cstm_hdr_btn">Pants</h1>
                            <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>

                        <div class="clearfix"></div>
                        <div class="col-md-6 custom_right" onclick="window.location='startshirt.php'">
                            <h1 class="cstm_hdr_btn">Shirts</h1>
                            <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 custom_right" onclick="window.location='startjacketcollection.php'">
                            <h1 class="cstm_hdr_btn">Overcoat</h1>
                            <p class="button_caption">Design yourself<i class="fa fa-caret-right" aria-hidden="true" ></i></p>
                        </div>
                        <div class="col-md-6 custom_right" onclick="window.location='startconfigovercoat.php'">
                            <h1 class="cstm_hdr_btn">Overcoat </h1>
                            <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 custom_right" onclick="window.location='accessories.php'">
                            <h1 class="cstm_hdr_btn">Accessories</h1>
                            <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-6 custom_right" onclick="window.location='stylebox.php'">
                            <h1 class="cstm_hdr_btn">Style Box</h1>
                            <p class="button_caption">Our Collection<i class="fa fa-caret-right" aria-hidden="true" style="margin-left:10px"></i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>
<?php include("footer.php"); ?>
<script>
function preference(selection){
    var url = "admin/webserver/selectionData.php?preference="+selection;
    $.get(url,function(data){
        var json = $.parseJSON(data);
        var status = json.status;
        if(status == "done"){
            window.location=selection+".php";
        }
        else{
            alert(status);
        }
    }).fail(function(){
        alert("page is not available");
    });
}
</script>



