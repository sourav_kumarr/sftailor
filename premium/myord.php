<?php
include('header.php');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>ORD Files<small></small></h2>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of User's Having ORD Files.
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>User Profile</th>
                                <th>User Name</th>
                                <th>User E-Mail</th>
                                <th>User Contact</th>
                                <th>User Gender</th>
                                <th>User City</th>
                                <th>Stylist</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="userData"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    function loadUsersData(){
        var storeName = $("#store_name").val();
        if(storeName == ""){
            showError("Session Timed Out !!! Please Login Again","red");
            return false;
        }
        var url = "api/userProcess.php";
        $.post(url,{"type":"getAllUsersData","store_name":storeName},function(data){
            var Status = data.Status;
            var dataShow = "";
            var ids = [];
            if(Status == "Success"){
                var userData = data.UserData;
                for(var i=0;i<userData.length;i++){
                    if(ids.indexOf(userData[i].user_id)>-1){
                        //alert("data already exist");
                    }else {
                        ids.push(userData[i].user_id);
                        dataShow = dataShow + "<tr><td><img alt='Image' src='" + userData[i].profile_pic + "' " +
                        "style='height:40px !important;width:40px;border:2px solid #333'/></td><td>" + userData[i].name + "</td>" +
                        "<td>" + userData[i].email + "</td><td>" + userData[i].mobile + "</td><td>" + userData[i].gender + "</td>" +
                        "<td>" + userData[i].b_city + "</td><td>"+storeName+"</td><td><button class='btn btn-sm btn-info' " +
                        "onclick=myordfiles('" + userData[i].user_id + "')>View All ORD Files</button>" +
                        "<button class='btn btn-sm btn-info' onclick=uploadfiles('" + userData[i].user_id + "')>Upload ORD File</button></td></tr>";
                    }
                }
            }else{
                dataShow = "<tr><td colspan=8 style='text-align:center'>"+data.Message+"</td></tr>";
            }
            $("#userData").html(dataShow);
        }).fail(function(){
            showError("Server Error !!! Please Try After Some Time...","red");
        });
    }
    function uploadfiles(user_id) {
        $(".modal-header").css({"display":"block"});
        $(".modal-body").css({"display":"block"});
        $(".modal-footer").css({"display":"block"});
        $(".modal-title").html("Upload Ord File");
        $(".modal-body").html("<div class='row'><p class='errormsg'></p></div><div class='row'><div class='col-md-6'>" +
        "<div class='form-group'><label>Enter File Name</label><input type='text' placeholder='Enter File Name' " +
        "id='ordname' class='form-control' /></div><div class='form-group'><label>Select Ord File</label>" +
        "<input type='file' id='selectedordfile' value='' class='form-control' /></div></div><div class='col-md-6'>" +
        "<label>Instructions</label><li class='inst'>Selected File Should Be in .ord Format</li>" +
        "<li class='inst'>Selected File Size Should Not Be Exceeded From 2 MB (Mega Bytes)</li></div></div>");
        $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default' />" +
        "<input type='button' value='Upload' onclick=uploadFile('"+user_id+"') class='btn btn-danger' />");
        $("#myModal").modal("show");
    }
    function uploadFile(user_id) {
        $(".errormsg").html("");
        var file_name = $("#selectedordfile").val();
        var ordname = $("#ordname").val();
        if (file_name == "") {
            $(".errormsg").html("Please Select a ORD File First");
            return false;
        }
        var file_ext = file_name.split(".");
        file_ext = file_ext[file_ext.length - 1];
        file_ext = file_ext.toLowerCase();
        if (file_ext != "ord") {
            $(".errormsg").html("Please Select an ORD File Only");
            return false;
        }
        var data = new FormData();
        var _file = document.getElementById('selectedordfile');
        data.append('file', _file.files[0]);
        data.append('type', "uploadord");
        data.append('user_id', user_id);
        data.append('ordname', ordname);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if(request.readyState == 4) {
                var json = $.parseJSON(request.response);
                if(json.Status == "Success"){
                    $("#myModal").modal("hide");
                }else{
                    $("#myModal").modal("hide");
                    setTimeout(function(){
                        showMessage(json.Message);
                    },1000);
                }
            }
        };
        request.upload.addEventListener('progress', function (e) {
            $("#prog").html("Uploading " + parseInt((e.loaded / e.total) * 100) + '% Complete');
        }, false);
        request.open('POST', '../api/registerUser.php');
        request.send(data);
    }
    function myordfiles(user_id){
        var url="../api/registerUser.php";
        $.post(url,{"type":"getMyFiles","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                var filesData = data.filesData;
                var filesDataShow = "<table class='table table-bordered'><tr><td>S.No</td><td>File Name</td><td>Added On</td><td>Action</td></tr>"
                for(var i=0;i<filesData.length;i++){
                    filesDataShow = filesDataShow+"<tr><td>"+(i+1)+"</td><td>"+filesData[i].file_name+"</td>" +
                        "<td>"+toHumanFormat(filesData[i].added_on)+"</td><td>" +
                        "<i class='fa fa-trash' style='cursor:pointer' onclick=deleteOrd('"+filesData[i]._id+"','"+user_id+"')></i></td></tr>"
                }
                filesDataShow = filesDataShow+"</table>";
                $(".modal-header").css({"display":"block"});
                $(".modal-body").css({"display":"block"});
                $(".modal-footer").css({"display":"block"});
                $(".modal-title").html("My Ord Files");
                $(".modal-body").html("<div class='row'><div class='col-md-12'>"+filesDataShow+"</div></div>");
                $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default' />");
                $("#myModal").modal("show");
            }else{
                showMessage(data.Message);
            }
        });
    }
    function toHumanFormat(unixTime){
        date = new Date(unixTime * 1000);
        var year    = date.getFullYear();
        var month   = date.getMonth();
        var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
        var day     = date.getDate();
        var hour    = date.getHours();
        var ampm = "AM";
        var minute  = date.getMinutes();
        var seconds = date.getSeconds();
        minute = parseInt(minute);
        seconds = parseInt(seconds);
        hour = parseInt(hour);
        if(hour<12){
            ampm = "AM";
        }else if(hour == 12){
            ampm = "PM";
        }else{
            hour = hour-12;
            ampm = "PM";
        }
        if(hour<10){
            hour = "0"+hour;
        }
        if(minute<10){
            minute = "0"+minute;
        }
        if(seconds<10){
            seconds = "0"+seconds;
        }
        return(day+" "+months[month]+" "+year+" "+hour+":"+minute+":"+seconds+" "+ampm);
    }
    function deleteOrd(file_id,user_id){
        var url = "../api/registerUser.php";
        $.post(url,{"type":"deleteFile","file_id":file_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                $("#myModal").modal("hide");
                setTimeout(function() {
                    myordfiles(user_id);
                },500);
            }else{
                $("#myModal").modal("hide");
                setTimeout(function() {
                    showMessage(data.Message,'red');
                },500);
            }
        });
    }
    loadUsersData();
</script>
