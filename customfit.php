<?php include("header.php");?>
		<div class="banner top-customfitbanner">
			<div class="">
			</div>
		</div>
		<!-- //Slider -->
	</div>
	<!-- //Header -->
<div class="about-bottom wthree-3">
		<div class="container">
		<h2 class="tittle">CUSTOM <span style="color:#000;">FIT</span></h2>
			<div class="agileinfo_about_bottom_grids">
				<div class="col-md-7 agileinfo_about_bottom_grid" style="text-align:justify;">

						<p>Feeling overwhelmed because of all the fabric and design choices? No problem, that's why we work with personal stylists that guide you all the way. They can help you with all the choices and advise you on what is most popular and best suited for your taste and body type. </p></br>
						
						<p>Your journey into the bespoke garment world starts with a one-on-one appointment at your office, our showroom or another location of your choosing. During this appointment, your style advisor will take your measurements, help you select fabrics, buttons and stitching colors and walk you through all the design choices for your suit, jacket or shirt. A few weeks later, your bespoke garment will be delivered and your stylist will make sure you are pleased with the way it fits and looks on you.  </p></br>
						
						<p>Set up your appointment for your bespoke shopping experience! Contact us <a href="contact.php">here.</a></b></p></br>
						
						
					 </p>
				</div>
				<div class="col-md-5 agileinfo_about_bottom_grid">
					<img src="images/tab/stylist_appointment_web.jpg" alt=" " class="img-responsive">
				</div>
			</div>
		</div>
	</div>

<!-- //team -->
<?php include("footer.php");?>