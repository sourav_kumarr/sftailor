<?php
include("header.php");
include('admins/api/Classes/GALLERY.php');
$gallery = new \Classes\GALLERY();
?>
<!-- gallery start here-->
<!-- #region Jssor Slider Begin -->
<!-- Generator: Jssor Slider Maker -->
<!-- Source: https://www.jssor.com -->

<style>
   /* .main-grid-box {
        margin-top: 10px;
    }*/
   .block {
       display: block !important;
   }
   .loaderhidden {
       display: none !important;
   }
   .loaderdiv {
       background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
       height: 700px;
       position: fixed;
       width: 100%;
       z-index: 99999;
       display: none;
   }

   .loaderdiv > img {
       height: 50px;
       margin: 300px 50%;
       width: 50px;
   }


   .loaderr{
       display: none!important;
   }

    .ext-img {
      /*  height: 240px;*/
        background-position: center center;
        background-size: 100% auto;
        background-repeat: no-repeat;
        min-width: 100%;
        border: 3px solid #757575;
        /*padding: 5px;*/
        margin-bottom: 26px;
        background-color: #f1f1f1;
    }
    #galleryBox{
        max-height: 600px;
        overflow-y: auto;
    }
    .imagetitle {
        font-size: 15px;
        text-align: center;
        margin-top: 83%;
        background: rgba(0,0,0,0.8);
        height: 39px;
        color: white;
        font-weight: normal;
        padding: 7px;
        /*
        width: 245px;
        margin-left: -7px;*/
    }
   #galleryData div img{
       image-orientation: from-image;
   }
   .gallerystyle{
       background-position:center center !important;
       background-size: cover !important;
   }
   .gallerystyle{
       image-orientation: from-image;
   }
    /* jssor slider loading skin spin css */
   #galleryData2 div div {

   }
    #galleryData2{
    }


</style>
<style>
    .jR3DCarouselGalleryCustomeTemplate .captions{
        position: relative;
        padding: 4px 0;
        bottom: 27px;
        background: #ec1c8e;
        display:block
    }
    .jR3DCarouselGallery,.jR3DCarouselGalleryCustomeTemplate {
        margin: 0 auto; /* optional - if want to center align */
    }
    .jR3DCarouselGalleryCustomeTemplate a{
        text-decoration: none;
    }
    .jR3DCarouselGallery{
        height: 500px!important;
        width: 750px!important;
    }
    .navigation{position: absolute;
        bottom: 0;
        right: 0px;
        display: block;
        background: rgba(0,0,0,0.5);
    }
    .slide{
        background: black!important;
    }
</style>
<div class="loaderdiv ">
    <img src="images/preloader.gif"/>
</div>
<div style="margin-top: 140px">
    <div class="col-md-12" style="background: #efeded;padding-top: 32px;padding-bottom: 102px;top: -40px;">
        <div class="col-md-1"></div>
        <div class="col-md-10">

        <h2 class="tittle">Gallery <span style="color:#000;"></span></h2>
        <div class="container-fluid" id="eventsdiv">
        <?php
        $eventResponse = $gallery->getDistinctEvent();
        if($eventResponse['Status'] == "Success"){
            $data = $eventResponse['data'];
            for($i=0;$i<sizeof($data);$i++){
                ?>
                <div class="col-sm-3 main-grid-box" onclick=window.location="gallery.php?event=<?php echo rawurlencode($data[$i]['event']) ?>" >
                    <div class="pdf-imgr ext-img" style=cursor:pointer;background-image:url("admins/api/Files/images/gallery/<?php echo $data[$i]['event_img'] ?>")>
                        <div class="imagetitle"><?php echo substr($data[$i]['event'],0,29) ?></div>
                    </div>

                </div>
                <?php
            }
            ?>
            <div class="col-sm-3 main-grid-box" style="cursor:pointer" onclick=window.location="gallery.php?event=all" >
                <div class="pdf-imgr ext-img" style="text-align: center;height: 285px;font-size: 24px;text-decoration: underline; padding: 82px 11px 18px 5px;" >
                    All Images<br>Click To View
                </div>
            </div>
            <?php
        }
        ?>
        </div>
        <div class="container" id="galleryData" style="display:none">
            <input type="button" class="btn btn-danger pull-left" value="Back" onclick="backnow()">

            <div class="jR3DCarouselGallery"></div>
        </div>
        </div>
        <div class="col-md-1"></div>
    </div>
</div>
<?php
include("footer.php");
?>
<script src="images/slider/wow/js/jR3DCarousel.js"></script>
<script>
function viewGalleryData(event) {
    var url = "admins/api/galleryProcess.php";
    event = unescape(event);
    $(".loaderdiv").removeClass("loaderhidden");
    $.post(url, {"type": "getGallery"}, function (data) {
        var status = data.Status;
        var galleryData = data.data;
        if (status == "Success") {
            var galleryBox = "";
            var galleryBoxThumb = "";
            var slideImages = [];
            if (event !== "all") {
                for (var a = 0; a < galleryData.length; a++) {
                    if (event === galleryData[a].event) {

                        slideImages.push({src:'admins/api/Files/images/gallery/'+galleryData[a].gallery_file+''});
                    }
                }
            } else {
                for (var a = 0; a < galleryData.length; a++) {

                    slideImages.push({src:'admins/api/Files/images/gallery/'+galleryData[a].gallery_file+''});
                }
            }


            var jR3DCarousel;
            jR3DCarousel = $('.jR3DCarouselGallery').jR3DCarousel({
                width: 750, 		/* largest allowed width */
                height: 500, 		/* largest allowed height */
                slides: slideImages /* array of images source */
            });
            function slideShownCallback($slide){
                console.log("Slide shown: ", $slide.find('img').attr('src'))
            }
            var carouselCustomeTemplateProps =  {
                width: 750, 				/* largest allowed width */
                height: 500, 				/* largest allowed height */
                slideLayout : 'fill',     /* "contain" (fit according to aspect ratio), "fill" (stretches object to fill) and "cover" (overflows box but maintains ratio) */
                animation: 'zoomInSlide', 	/* slide | scroll | fade | zoomInSlide | zoomInScroll */
                animationCurve: 'ease',
                animationDuration: 1900,
                animationInterval: 2000,
                slideClass: 'jR3DCarouselCustomSlide',
                autoplay: true,
                controls: true,			/* control buttons */
                navigation: 'squares'			/* circles | squares | '' */,
                perspective: 2200,
                rotationDirection: 'ltr',
                onSlideShow: slideShownCallback

            }

            jR3DCarouselCustomeTemplate = $('.jR3DCarouselGalleryCustomeTemplate').jR3DCarousel(carouselCustomeTemplateProps);
            $(".navigation").attr("display","flex");
            $(".loaderdiv").addClass("loaderhidden");
            $("#eventsdiv").css("display","none");
            $("#galleryData").css("display","block");
        }
        else {
            showMessage(data.Message, "red");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }).fail(function () {
        showMessage("Unable to process the request", "red");
    });
}
function loadScript(url, callback){
    var script = document.createElement("script")
    script.type = "text/javascript";
    if (script.readyState){  //IE
        script.onreadystatechange = function(){
            if (script.readyState == "loaded" ||
                script.readyState == "complete"){
                script.onreadystatechange = null;
                callback();
            }
        };
    } else {  //Others
        script.onload = function(){
            callback();
        };
    }

    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}
function backnow(){
    window.location="gallery.php";
}
var curl = location.href;
if(curl.indexOf("?event")>-1){
    curl = curl.split('?event=')[1];
    if(curl !== ""){
        viewGalleryData(curl);
    }
}
</script>



