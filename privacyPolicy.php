<?php
include("header.php");
include_once "admins/api/Classes/CONNECT.php";
require_once('admins/api/Classes/PRIVACY_POLICY.php');
$conn = new Classes\CONNECT();
$priClass = new \Classes\PRIVACY_POLICY();
$privacyData = $priClass->getPrivacyData();

?>
    <div class="about-bottom wthree-3">
        <div class="container" style="margin-top:50px">
            <h2 class="tittle">Privacy / <span style="color:#000;">Policy</span></h2>
            <div class="agileinfo_about_bottom_grids">
                <?php echo $privacyData['privacyData'][0]['message'];?>
            </div>
        </div>
    </div>
    <!-- //team -->
<?php include("footer.php");?>