<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 11-12-2017
 * Time: 12:55
 */

namespace Classes;


class RESPONSE
{
 public static function apiResponse($result) {
     header("Content-Type: application/json");
     echo json_encode($result);
 }
}