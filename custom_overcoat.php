
<?php
include('header.php');
include("admin/webserver/database/db.php");
?>
</div>
<div class="about-bottom wthree-3">
<style>
.active {
    border: 2px solid #555;
    border-radius: 4px;
}
.primarysuggestion {
    height: 200px;
    margin-left:2px;
    margin-top:2px;
    width: auto;
}
.primarysuggestion img {
    height: 75%;
    min-width: 140px;
}
.steps {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #aeaeae;
    border-radius: 7%;
    cursor: pointer;
    float: left;
    font-size: 12px!important;
    line-height: 17px;
    margin-bottom: 0;
    margin-left: 2px;
    margin-top: 2px;
    max-height: 57px;
    padding-top: 9px;
    text-align: center;
    width: 9%;
}
.stepsactive {
    background: #c4996c none repeat scroll 0 0;
    border: 2px solid #c4996c;
    color: white;
}
.breakingline {
    margin-top: 38px;
    width: 501px;
}
.outputsugg {
    position: absolute;
}
.backbtnsuggparentdiv p
{
    font-weight:normal;
    font-size: 12px;
    line-height: 18px!important;
}
#forcasual .primarysuggestion > img
{
    padding: 40px;
}
.okok {
    font-size: 19px;
    height: 52px;
    margin-left: 10px;
    margin-top: 10px;
    padding-top: 6px;
    width: 162px;
}
.colorsuggestion {
    height: 75px!important;
    width: 75px!important;
}
.colorsuggestion img{
    height:75% !important;
    width:75px !important;
    min-width:unset;
}
.overflow-cmd{overflow-y: auto; height: 660px; padding-top: 22px;}
.data_{
    margin-top: 2px;
    padding: 0px;
    border: 1px lightgrey solid;
    border-radius: 5px;
    position: absolute;
    max-height: 100px;
    overflow: auto;
    width: 98%;
}

.data_ li{
    padding: 5px;
    border-bottom: 1px lightgrey solid;
}
.data_ li:hover{
    cursor: pointer;
    background-color: silver;
}
.data-remove{
    display:none;
}
</style>
<div class="loader" style="display: none">
    <img src="images/spin.gif" class="spinloader"/>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="col-md-1" style="">
            <i class="fa fa-mail-forward fa-2x" onclick="rotateImage('left')" style="margin-top:300px;cursor:pointer"></i>
        </div>
        <div class="col-md-10" style="padding-top: 50px;position: relative">
            <img src=""  class="outputsugg" id="shirtimg" alt='' style="z-index:0"/>
            <img src=""  class="outputsugg" id="tieimg" alt='' style="z-index:1"/>
            <img src=""  class="outputsugg" id="overcoatimg" alt='' style="z-index:2"/>
            <img src=""  class="outputsugg" id="lapelimg" alt='' style="z-index:4"/>
            <img src=""  class="outputsugg" id="frontbuttonimg" alt='' style="z-index:4"/>
            <img src=""  class="outputsugg" id="breastpocketimg" alt='' style="z-index:3"/>
            <img src=""  class="outputsugg" id="lowerpocketimg" alt='' style="z-index:3"/>
            <img src=""  class="outputsugg" id="sleevebuttonimg" alt='' style="z-index:3"/>
        </div>
        <div class="col-md-1" style="">
            <i class="fa fa-mail-reply fa-2x" onclick="rotateImage('right')" style="margin-top:300px;cursor:pointer"></i>
        </div>
    </div>
    <div class="col-md-6" style="margin-top:20px">
        <div class="col-md-12" style="padding:25px 0">
            <div class="steps stepsactive" id="category">Overcoat Category</div>
            <div class="steps" id="canvasstyle">Canvas </br>style</div>
            <div class="steps" id="lapelstyle">Lapel </br>style</div>
            <div class="steps" id="pocketstype">Pockets </br>Type</div>
            <div class="steps" id="buttonstyle">Button</br>Style</div>
            <div class="steps" id="sweatpadstyle">Sweat</br>Pad</div>
            <div class="steps" id="elbowpadstyle">Elbow</br>Pad</div>
            <div class="steps" id="shoulderstyle">Shoulder</br>Style</div>
            <div class="steps" id="sleeveslitstyle">Sleeve</br>Style</div>
            <div class="steps" id="backvent">Back</br>Vent</div>
            <div class="steps" id="stitchstyle">Stitch</br>Style</div>
			<div class="steps" id="breastpocket">Breast</br>Pocket</div>
			<div class="steps" id="coinpocket">Coin</br>Pocket</div>
			<div class="steps" id="facingstyle">Facing</br>Style</div>
			<div class="steps" id="pocketstyle">Pocket</br>Style</div>
			<div class="steps" id="collarstyle">Collar</br>Style</div>
			<div class="steps" id="liningstyle">Lining</br>Style</div>	
			<div class="steps" id="liningoption">Lining</br>Option</div>
			<div class="steps" id="threadcolor">Thread</br>Color</div>
			<div class="steps" id="overcoatfabric">OverCoat</br>Fabric</div>
            <div class="breakingline"></div>
        </div>
        <!--
        ------
        -----category div
        ------
        -------->
        <div class="col-md-12 stepdiv" id="categorys" style="display: block">
            <p class="stylecaption">Overcoat Category</p>
            <div class="primarysuggestion okok active" id="category_formal" style="margin-left: 140px">
                <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Formal</p>
            </div>
            <div class="primarysuggestion okok" id="category_casual" style="margin-left: 10px">
                <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Casual</p>
            </div>
        </div>
        <!--
        ----canvas style div
        --->
        <div class="col-md-12 stepdiv" id="canvasstyles" style="display:none">
            <p class="stylecaption">Canvas Style</p>
            <div class="primarysuggestion" id="canvasstyle_D-000A:full_canvas">
                <img src="images/overcoat/D-000Afullcanvas.png"/>
                <p class="suggestionname">D-000A </br>Full Canvas</p>
            </div>
            <div class="primarysuggestion" id="canvasstyle_D-000B:half_canvas">
                <img src="images/overcoat/D-000BHalfcanvas.png"/>
                <p class="suggestionname">D-000B</br>Half Canvas</p>
            </div>
            <div class="primarysuggestion" id="canvasstyle_D-00C1:fused">
                <img src="images/overcoat/D-00C1fused.png"/>
                <p class="suggestionname">D-00C1</br>Fused</p>
            </div>
        </div>
        <!--
        ----lapel style div
        --->
        <div class="col-md-12 stepdiv" id="lapelstyles" style="display:none">
            <p class="stylecaption">Lapel Style</p>
            <div class="primarysuggestion" id="lapelstyle_D-6001:notch" >
                <img src="images/overcoat/D-6001notchlapel.png"/>
                <p class="suggestionname">D-6001</br>Notch Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6002:peak" >
                <img src="images/overcoat/D-6002peaklapel.png"/>
                <p class="suggestionname">D-6002</br>Peak Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6003:semi_notch" >
                <img src="images/overcoat/D-6003seminotchlapel.png"/>
                <p class="suggestionname">D-6003</br>Semi Notch Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6004:semi_peak" >
                <img src="images/overcoat/D-6004semipeaklapel.png"/>
                <p class="suggestionname">D-6004</br>Semi Peak Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-601S:my_shape_collar">
                <img src="images/overcoat/D-601Smyshapecollar.png"/>
                <p class="suggestionname">D-601S</br>My Shape collar</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-601U:duck_billed_lapel">
                <img src="images/overcoat/D-601Uduckbilledlapel.png"/>
                <p class="suggestionname">D-601U</br>Duck Billed Lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6005:shawl_lapel" >
                <img src="images/overcoat/D-6005shwallapel.png"/>
                <p class="suggestionname">D-6005</br>Shawl lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6006:fish_mouth_lapel" >
                <img src="images/overcoat/D-6006fishmouthlapel.png"/>
                <p class="suggestionname">D-6006</br>Fish Mouth lapel</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6007:closed_collar" >
                <img src="images/overcoat/D-6007closedcollar.png"/>
                <p class="suggestionname">D-6007</br>Closed Collar</p>
            </div>
            <div class="primarysuggestion" id="lapelstyle_D-6681:notch_lapel_with_left_square_tab" title="Notch lapel with left square tab">
                <img src="images/overcoat/D-6681notchlapelwithleftsquaretab.png"/>
                <p class="suggestionname">D-6681</br>Notch lapel with..</p>
            </div>
            <div class="primarysuggestion"  id="lapelstyle_D-6699:peak_lapel_in_slice_in_the_middle_customer_appoint_inner_splice_fabric" title="Peak lapel in slice in the middle customer appoint inner splice fabric">
                <img src="images/overcoat/D-6699peaklapelinsliceinthemiddlecustomerappointinnersplicefabric.png"/>
                <p class="suggestionname">D-6699</br>Peak lapel in slice..</p>
            </div>
        </div>
        <!--
        ----pockets type div
        --->
        <div class="col-md-12 overflow-cmd stepdiv"  id="pocketstypes" style="display:none">
            <p class="stylecaption">Pockets Type</p>
            <div class="primarysuggestion" id="pocketstype_D-6201:normal_pockets" title="">
                <img src="images/overcoat/D-6201normalpockets.png"/>
                <p class="suggestionname">D-6201</br>normal pockets</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6200:no_lower_pockets" title="">
                <img src="images/overcoat/D-6200nolowerpockets.png"/>
                <p class="suggestionname">D-6200</br>no lower pockets</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-6231:double_besom_pocket" title="low pocket with diamond flap and button & buttonhole">
                <img src="images/overcoat/D-6231doublebesompocket.png"/>
                <p class="suggestionname">D-6231</br>Double besom pocket</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62A1:normal_slanted_pockets" >
                <img src="images/overcoat/D-62A1normalslantedpockets.png"/>
                <p class="suggestionname">D-62A1</br>Normal slanted pockets</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62A2:normal_slanted_pockets_with_normal_slanted_ticket_pocket" title="Normal slanted pockets with normal slanted ticket pocket">
                <img src="images/overcoat/D-62A2normalslantedpocketswithnormalslantedticketpocket.png"/>
                <p class="suggestionname">D-62A2</br>Normal slanted pockets..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-62A3:normal_slant_double_pocket_and_normal_slant_double_besom_ticket_pocket" title="normal slant double pocket and normal slant double besom ticket pocket">
                <img src="images/overcoat/D-62A3normalslantdoublepocketandnormalslantdoublebesomticketpocket.png"/>
                <p class="suggestionname">D-62A3</br>normal slant double..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62A6:normal_slant_pocket_with_button" title="normal slant pocket with button">
                <img src="images/overcoat/D-62A6normalslantpocketwithbutton.png"/>
                <p class="suggestionname">D-62A6</br>normal slant pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62B0:very_slant_single_besom_low_pocket" title="very slant single besom low pocket">
                <img src="images/overcoat/D-62B0veryslantsinglebesomlowpocket.png"/>
                <p class="suggestionname">D-62B0</br>very slant single..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62B1:very_slant_lower_pockets" title="very slant single besom low pocket">
                <img src="images/overcoat/D-62B1veryslantlowerpockets.png"/>
                <p class="suggestionname">D-62B1</br>very slant lower pockets</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62B2:very_slanted_pockets_with_very_slanted_ticket_pocket" title="very slanted pockets with very slanted ticket pocket">
                <img src="images/overcoat/D-62B2veryslantedpocketswithveryslantedticketpocket.png"/>
                <p class="suggestionname">D-62B2</br>very slanted pockets..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62B3:big_slanted_pockets_with_double_besom_ticket_pocket" title="Big slanted pockets with double besom ticket pocket">
                <img src="images/overcoat/D-62B3bigslantedpocketswithdoublebesomticketpocket.png"/>
                <p class="suggestionname">D-62B3</br>Big slanted pockets..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62B7:very_slant_low_pocket_with_diamond_flap" title="very slant low pocket with diamond flap">
                <img src="images/overcoat/D-62B7veryslantlowpocketwithdiamondflap.png"/>
                <p class="suggestionname">D-62B7</br>very slant low pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62B8:very_slant_low_pocket_with_diamond_flap_with_button_and_button_hole" title="very slant low pocket with diamond flap with button and button hole">
                <img src="images/overcoat/D-62B8veryslantlowpocketwithdiamondflapwithbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-62B8</br>very slant low pocket..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-62B9:very_slant_double_besom_with_button_and_tab_low_pocket" title="very slant double besom with button and tab low pocket">
                <img src="images/overcoat/D-62B9veryslantdoublebesomwithbuttonandtablowpocket.png"/>
                <p class="suggestionname">D-62B9</br>very slant double besom..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C1:very_slanted_lower_pocket" title="very slant double besom with button and tab low pocket">
                <img src="images/overcoat/D-62C1veryslantedlowerpocket.png"/>
                <p class="suggestionname">D-62C1</br>very slanted lower pocket</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C2:very_slanted_lower_pocket_with_very_slanted_ticket_pocket" title="very slanted lower pocket with very slanted ticket pocket">
                <img src="images/overcoat/D-62C2veryslantedlowerpocketwithveryslantedticketpocket.png"/>
                <p class="suggestionname">D-62C2</br>very slanted lower pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C3:very_slanted_lower_pocket_with_very_slanted_double_besom_ticket_pocket" title="very slanted lower pocket with very slanted double besom ticket pocket">
                <img src="images/overcoat/D-62C3veryslantedlowerpocketwithveryslanteddoublebesomticketpocket.png"/>
                <p class="suggestionname">D-62C3</br>very slanted lower pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C4:very_slanted_lower_pocket_with_diamond_flap" title="very slanted lower pocket with diamond flap">
                <img src="images/overcoat/D-62C4veryslantedlowerpocketwithdiamondflap.png"/>
                <p class="suggestionname">D-62C4</br>very slanted lower pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C5:very_slanted_lower_pocket_with_diamond_flap_button_and_button_hole" title="very slanted lower pocket with diamond flap button & button hole">
                <img src="images/overcoat/D-62C5veryslantedlowerpocketwithdiamondflapbutton&buttonhole.png"/>
                <p class="suggestionname">D-62C5</br>very slanted lower pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C6:very_slanted_double_besom_lower_pocket" title="very slanted double besom lower pocket">
                <img src="images/overcoat/D-62C6veryslanteddoublebesomlowerpocket.png"/>
                <p class="suggestionname">D-62C6</br>very slanted double besom..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C7:very_slanted_double_besom_lower_pocket_with_loop_and_button" title="very slanted double besom lower pocket with loop and button">
                <img src="images/overcoat/D-62C7veryslanteddoublebesomlowerpocketwithloopandbutton.png"/>
                <p class="suggestionname">D-62C7</br>very slanted double besom..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62C8:very_slanted_double_besom_lower_pocket_with_pointed_tab_button_and_button_hole" title="very slanted double besom lower pocket with pointed tab button and button hole">
                <img src="images/overcoat/D-62C8veryslanteddoublebesomlowerpocketwithpointedtabbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-62C8</br>very slanted double besom..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-62C9:very_slanted_double_besom_lower_pocket_with_square_tab_button_and_button_hole" title="very slanted double besom lower pocket with square tab button and button hole">
                <img src="images/overcoat/D-62C9veryslanteddoublebesomlowerpocketwithsquaretabbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-62C9</br>very slanted double besom..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-62D1:normal_slanted_double_besom_lower_pocket" title="normal slanted double besom lower pocket">
                <img src="images/overcoat/D-62D1normalslanteddoublebesomlowerpocket.png"/>
                <p class="suggestionname">D-62D1</br>normal slanted double besom..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62D3:normal_slanted_double_besom_lower_pocket_with_normal_slanted_double_besom_ticket_pocket" title="normal slanted double besom lower pocket with normal slanted double besom ticket pocket">
                <img src="images/overcoat/D-62D3normalslanteddoublebesomlowerpocketwithnormalslanteddoublebesomticketpocket.png"/>
                <p class="suggestionname">D-62D3</br>normal slanted double..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62D4:normal_slanted_double_besom_lower_pocket_with_normal_slanted_zipper_ticket_pocket" title="normal slanted double besom lower pocket with normal slanted zipper ticket pocket">
                <img src="images/overcoat/D-62D4normalslanteddoublebesomlowerpocketwithnormalslantedzipperticketpocket.png"/>
                <p class="suggestionname">D-62D4</br>normal slanted double..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62E6:very_slanted_double_besom_lower_pocket_with_rounded_tab_button_and_button_hole" title="very slanted double besom lower pocket with rounded tab button and button hole">
                <img src="images/overcoat/D-62E6veryslanteddoublebesomlowerpocketwithroundedtabbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-62E6</br>very slanted double..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-62G6:very_slant_double_besom_low_pocket_with_pointed_tab_and_button_and_button_hole" title="very slant double besom low pocket with pointed tab and button & buttonhole">
                <img src="images/overcoat/D-62G6veryslantdoublebesomlowpocketwithpointedtabandbutton&buttonhole.png"/>
                <p class="suggestionname">D-62G6</br>very slant double..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62G7:very_slant_double_besom_low_pocket_with_square_tab_and_button_and_button_hole" title="very slant double besom low pocket with square tab and button & button hole">
                <img src="images/overcoat/D-62G7veryslantdoublebesomlowpocketwithsquaretabandbutton&buttonhole.png"/>
                <p class="suggestionname">D-62G7</br>very slant double..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62G8:very_slant_double_besom_low_pocket_with_round_tab_and_button_and_button_hole" title="very slant double besom low pocket with round tab and button & button hole">
                <img src="images/overcoat/D-62G8veryslantdoublebesomlowpocketwithroundtabandbutton&buttonhole.png"/>
                <p class="suggestionname">D-62G8</br>very slant double..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62M1:double_besom_with_tab_and_button_below_pockets" title="Double besom with tab and button below pockets">
                <img src="images/overcoat/D-62M1doublebesomwithtabandbuttonbelowpockets.png"/>
                <p class="suggestionname">D-62M1</br>Double besom with tab..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-62M2:double_besom_with_pointed_tab_and_button_below_pockets" title="Double besom with pointed tab and button below pockets">
                <img src="images/overcoat/D-62M2doublebesomwithpointedtabandbuttonbelowpockets.png"/>
                <p class="suggestionname">D-62M2 </br>Double besom with..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62M3:double_besom_with_square_tab_with_button_below_pockets" title="Double besom with square tab with button below pockets">
                <img src="images/overcoat/D-62M3doublebesomwithsquaretabwithbuttonbelowpockets.png"/>
                <p class="suggestionname">D-62M3</br>Double besom with square..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-62M4:double_besom_with_round_tab_and_button_below_pockets" title="Double besom with round tab and button below pockets">
                <img src="images/overcoat/D-62M4doublebesomwithroundtabandbuttonbelowpockets.png"/>
                <p class="suggestionname">D-62M4</br>Double besom with round..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6202:normal_pockets_with_ticket_pocket" title="normal pockets with ticket pocket">
                <img src="images/overcoat/D-6202normalpocketswithticketpocket.png"/>
                <p class="suggestionname">D-6202</br>normal pockets with..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-6204:normal_pocket_and_double_besom_ticket_pocket" title="normal pocket and double besom ticket pocket">
                <img src="images/overcoat/D-6204normalpocketanddoublebesomticketpocket.png"/>
                <p class="suggestionname">D-6204</br>normal pocket and..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6205:normal_pocket_and_zippered_ticket_pocket" title="normal pocket and zippered ticket pocket">
                <img src="images/overcoat/D-6205normalpocketandzipperedticketpocket.png"/>
                <p class="suggestionname">D-6205</br>normal pocket and..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6211:low_pocket_with_diamond_flap_and_button_and_button_hole" title="low pocket with diamond flap and button & buttonhole">
                <img src="images/overcoat/D-6211lowpocketwithdiamondflapandbutton&buttonhole.png"/>
                <p class="suggestionname">D-6211</br>low pocket with..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-6232:double_besom_lower_pocket_with_ticket_pocket" title="double besom lower pocket with ticket pocket">
                <img src="images/overcoat/D-6232doublebesomlowerpocketwithticketpocket.png"/>
                <p class="suggestionname">D-6232</br>double besom lower..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6234:double_besom_with_double_besom_ticket_pockets" title="double besom with double besom ticket pockets">
                <img src="images/overcoat/D-6234doublebesomwithdoubebesomticketpockets.png"/>
                <p class="suggestionname">D-6234</br>double besom with..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6235:double_besom_pocket_with_zip_ticket_pocket" title="double besom pocket with zip ticket pocket">
                <img src="images/overcoat/D-6235doublebesompocketwithzipticketpocket.png"/>
                <p class="suggestionname">D-6235</br>double besom pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6237:low_pocket_with_diamond_flap" title="low pocket with diamond flap">
                <img src="images/overcoat/D-6237lowpocketwithdiamondflap.png"/>
                <p class="suggestionname">D-6237</br>low pocket with diamond..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-6238:normal_lower_pocket_with_button_and_button_hole" title="normal lower pocket with button and button hole">
                <img src="images/overcoat/D-6238normallowerpocketwithbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-6238</br>normal lower pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6251:normal_zippered_pocket" title="">
                <img src="images/overcoat/D-6251normalzipperedpocket.png"/>
                <p class="suggestionname">D-6251</br>normal zippered pocket</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6254:normal_zippered_pocket_and_double_besom_ticket_pocket" title="normal zippered pocket and double besom ticket pocket">
                <img src="images/overcoat/D-6254normalzipperedpocketanddoublebesomticketpocket.png"/>
                <p class="suggestionname">D-6254</br>normal zippered pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstype_D-6255:normal_zippered_pocket_and_zippered_ticket_pocket" title="normal zippered pocket and zippered ticket pocket">
                <img src="images/overcoat/D-6255normalzipperedpocketandzipperedticketpocket.png"/>
                <p class="suggestionname">D-6255</br>normal zippered pocket..</p>
            </div>
            <div class="primarysuggestion"  id="pocketstype_D-6261:normal_welt_pocket" title="normal zippered pocket and zippered ticket pocket">
                <img src="images/overcoat/D-6261normalweltpocket.png"/>
                <p class="suggestionname">D-6261</br>normal welt pocket</p>
            </div>
        </div>
        <!--
        ----button style div
        --->
        <div class="col-md-12 stepdiv" id="buttonstyles" style="display:none">
            <p class="stylecaption">Button Style</p>
            <div class="primarysuggestion" id="buttonstyle_D-6012:single_breasted_two_buttons" title="single breasted two buttons">
                <img src="images/overcoat/D-6012singlebreastedtwobuttons.png"/>
                <p class="suggestionname">D-6012</br>single breasted two..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6013:single_breasted_three_buttons" title="single breasted three buttons">
                <img src="images/overcoat/D-6013singlebreastedthreebuttons.png"/>
                <p class="suggestionname">D-6013</br>single breasted three..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6014:single_breasted_four_buttons" title="single breasted four buttons">
                <img src="images/overcoat/D-6014singlebreastedfourbuttons.png"/>
                <p class="suggestionname">D-6014</br>single breasted four..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6015:double_breasted_four_buttons_with_two_buttons_fasten" title="Double breasted four buttons two buttons fasten">
                <img src="images/overcoat/D-6015doublebreastedfourbuttonstwobuttonsfasten.png"/>
                <p class="suggestionname">D-6015</br>Double breasted four..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6016:double_breasted_six_buttons_with_two_buttons_fasten" title="Double breasted six buttons two buttons fasten">
                <img src="images/overcoat/D-6016doublebreastedsixbuttonstwobuttonsfasten.png"/>
                <p class="suggestionname">D-6016</br>Double breasted six..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6019:double_breasted_six_buttons_with_three_buttons_fasten" title="Double breasted six buttons three buttons fasten">
                <img src="images/overcoat/D-6019doublebreastedsixbuttonsthreebuttonsfasten.png"/>
                <p class="suggestionname">D-6019</br>Double breasted six..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-0026:single_breasted_six_buttons">
                <img src="images/overcoat/D-0026singlebreasted6buttons.png"/>
                <p class="suggestionname">D-0026</br>single breasted 6 buttons</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-602B:single_breasted_three_buttons_lapel_roll_to_the_second_button" title="single breasted 3 buttons lapel roll to the 2nd button">
                <img src="images/overcoat/D-602Bsinglebreasted3buttonslapelrolltothe2ndbutton.png"/>
                <p class="suggestionname">D-602B</br>single breasted 3..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-602C:single_breasted_three_buttons_lapel_roll_to_middle_of_the_first_and_second_buttons" title="single breasted 3 buttons lapel roll to middle of the 1st and 2nd buttons">
                <img src="images/overcoat/D-602Csinglebreasted3buttonslapelrolltomiddleofthe1stand2ndbuttons.png"/>
                <p class="suggestionname">D-602C</br>single breasted 3..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-602D:single_breasted_four_buttons_lapel_roll_to_the_second_button" title="single breasted four buttons lapel roll to the second button">
                <img src="images/overcoat/D-602Dsinglebreastedfourbuttonslapelrolltothesecondbutton.png"/>
                <p class="suggestionname">D-602D</br>single breasted four..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-602E:single_breasted_four_button_lapel_end_between_one_and_two_button" title="single breasted 4 button lapel end between 1 and 2 button">
                <img src="images/overcoat/D-602Esinglebreasted4buttonlapelendbetween1and2button.png"/>
                <p class="suggestionname">D-602E</br>single breasted 4..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6023:single_breasted_five_buttons" title="Single breasted five buttons">
                <img src="images/overcoat/D-6023singlebreastedfivebuttons.png"/>
                <p class="suggestionname">D-6023</br>Single breasted five..</p>
            </div>
            <div class="primarysuggestion" id="buttonstyle_D-6020:double_breasted_eight_buttons_with_four_buttons_fastened" title="double breasted 8 buttons 4 fastened">
                <img src="images/overcoat/D-6020doublebreasted8buttons4fastened.png"/>
                <p class="suggestionname">D-6020</br>double breasted 8..</p>
            </div>
        </div>
        <!--
        ----sweat pad style div
        --->
        <div class="col-md-12 stepdiv" id="sweatpadstyles" style="display:none">
            <p class="stylecaption">Sweat Pad Style</p>
            <div class="primarysuggestion" id="sweatpadstyle_D-6742:small_triangle_shape_lining_sweat_pad" title="small triangle shape lining sweat pad">
                <img src="images/overcoat/D-6742smalltriangleshapeliningsweatpad.png"/>
                <p class="suggestionname">D-6742</br>small triangle shape..</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6745:round_lining_sweat_pad" title="">
                <img src="images/overcoat/D-6745roundliningsweatpad.png"/>
                <p class="suggestionname">D-6745</br>round lining sweat pad</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6746:rounded_lining_sweat_pad_with_pic_stitch" title="rounded lining sweat pad with pic stitch">
                <img src="images/overcoat/D-6746roundedliningsweatpadwithpicstitch.png"/>
                <p class="suggestionname">D-6746</br>rounded lining sweat..</p>
            </div>
            <div class="primarysuggestion"  id="sweatpadstyle_D-6747:round_fabric_sweat_pad" title="">
                <img src="images/overcoat/D-6747roundfabricsweatpad.png"/>
                <p class="suggestionname">D-6747</br>round fabric sweat pad</p>
            </div>
            <div class="primarysuggestion"  id="sweatpadstyle_D-6748:rounded_fabric_sweat_pad_with_pic_stitch" title="rounded fabric sweat pad with pic stitch">
                <img src="images/overcoat/D-6748roundedfabricsweatpadwithpicstitch.png"/>
                <p class="suggestionname">D-6748</br>rounded fabric sweat pad..</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6749:round_fabric_sweat_pad_with_welt" title="round fabric sweat pad with welt">
                <img src="images/overcoat/D-6749roundfabricsweatpadwithwelt.png"/>
                <p class="suggestionname">D-6749</br>round fabric sweat pad..</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6750:u_shape_lining_sweat_pad" title="">
                <img src="images/overcoat/D-6750ushapeliningsweatpad.png"/>
                <p class="suggestionname">D-6750</br>U shape lining sweat pad</p>
            </div>
            <div class="primarysuggestion"  id="sweatpadstyle_D-6751:u_shape_lining_sweat_pad_with_pic_stitch" title="U shape lining sweat pad with pic stitch">
                <img src="images/overcoat/D-6751ushapeliningsweatpadwithpicstitch.png"/>
                <p class="suggestionname">D-6751</br>U shape lining sweat pad</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6752:u_shape_fabric_sweat_pad" title="">
                <img src="images/overcoat/D-6752ushapefabricsweatpad.png"/>
                <p class="suggestionname">D-6752</br>u shape fabric sweat pad</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6753:u_shape_fabric_sweat_pad_with_pic_stitch_on_the_edge" title="u shape fabric sweat pad with pic stitch on the edge">
                <img src="images/overcoat/D-6753ushapefabricsweatpadwithpicstitchontheedge.png"/>
                <p class="suggestionname">D-6753</br>u shape fabric sweat pad</p>
            </div>
            <div class="primarysuggestion" id="sweatpadstyle_D-6754:u_shape_lining_sweat_pad_with_welt" title="u shape lining sweat pad with welt">
                <img src="images/overcoat/D-6754ushapeliningsweatpadwithwelt.png"/>
                <p class="suggestionname">D-6754</br>u shape lining sweat pad</p>
            </div>
        </div>
        <!--
        ----elbow pad style div
        --->
        <div class="col-md-12 stepdiv" id="elbowpadstyles" style="display:none">
			<p class="stylecaption">Elbow Pad Style</p>
			<div class="primarysuggestion" id="elbowpadstyle_D-6609:round_elbow_pad">
				<img src="images/overcoat/D-6609roundelbowpad.png"/>
				<p class="suggestionname">D-6609</br>Round Elbow Pad</p>
			</div>
			<div class="primarysuggestion" id="elbowpadstyle_D-6610:oval_elbow_pad">
				<img src="images/overcoat/D-6610ovalelbowpad.png"/>
				<p class="suggestionname">D-6610</br>Oval elbow pad</p>
			</div>
		</div> 
		<!--
        ----shoulder style div
        --->
        <div class="col-md-12 stepdiv" id="shoulderstyles" style="display:none">
			<p class="stylecaption">Shoulder Style</p>
			<div class="primarysuggestion" id="shoulderstyle_D-660N:neapolitan_shoulder">
				<img src="images/overcoat/D-660Nneapolitanshoulder.png"/>
				<p class="suggestionname">D-660N</br>Neapolitan shoulder</p>
			</div>
			<div class="primarysuggestion" id="shoulderstyle_D-6601:normal_shoulder">
				<img src="images/overcoat/D-6601normalshoulder.png"/>
				<p class="suggestionname">D-6601</br>Normal shoulder</p>
			</div>
			<div class="primarysuggestion" id="shoulderstyle_D-6602:high_sleeve_head">
				<img src="images/overcoat/D-6602highsleevehead.png"/>
				<p class="suggestionname">D-6602</br>High sleeve head</p>
			</div>
			<div class="primarysuggestion" id="shoulderstyle_D-6603:natural_shoulder">
				<img src="images/overcoat/D-6603naturalshoulder.png"/>
				<p class="suggestionname">D-6603</br>Natural shoulder</p>
			</div>
			<div class="primarysuggestion" id="shoulderstyle_D-6604:cocked_shoulder">
				<img src="images/overcoat/D-6604cockedshoulder.png"/>
				<p class="suggestionname">D-6604</br>Cocked shoulder</p>
			</div>
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Shoulder Patch Style</p>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z1:left_shoulder_arch_patch" title="Left shoulder arch patch">
					<img src="images/overcoat/D-66Z1leftshoulderarchpatch.png" />
					<p class="suggestionname">D-66Z1</br>Left shoulder..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z2:right_shoulder_with_arch_gusset" title="Right shoulder with arch gusset">
					<img src="images/overcoat/D-66Z2rightshoulderwitharchgusset.png" />
					<p class="suggestionname">D-66Z2</br>Right shoulder with..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z3:left_and_right_shoulder_with_arch_patch" title="Left and right shoulder with arch patch">
					<img src="images/overcoat/D-66Z3leftandrightshoulderwitharchpatch.png" />
					<p class="suggestionname">D-66Z3</br>Left and right..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z4:left_shoulder_arch_patch_with_button_and_button_hole" title="Left shoulder arch patch with button and button hole">
					<img src="images/overcoat/D-66Z4leftshoulderarchpatchwithbuttonandbuttonhole.png" />
					<p class="suggestionname">D-66Z4</br>Left shoulder arch..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z5:right_shoulder_with_arch_gusset_button_and_button_hole" title="Right shoulder with arch gusset button and button hole">
					<img src="images/overcoat/D-66Z5rightshoulderwitharchgussetbuttonandbuttonhole.png" />
					<p class="suggestionname">D-66Z5</br>Right shoulder with..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z6:left_and_right_arc_shoulder_patch_with_button_and_button_hole" title="Left and right arc shoulder patch with button and button hole">
					<img src="images/overcoat/D-66Z6leftandrightarcshoulderpatchwithbuttonandbuttonhole.png" />
					<p class="suggestionname">D-66Z6</br>Left and right arc..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z7:left_shoulder_with_patch">
					<img src="images/overcoat/D-66Z7leftshoulderwithpatch.png" />
					<p class="suggestionname">D-66Z7</br>Left shoulder with patch</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z8:right_shoulder_with_gun_patch" title="Right shoulder with gun patch">
					<img src="images/overcoat/D-66Z8rightshoulderwithgunpatch.png" />
					<p class="suggestionname">D-66Z8</br>Right shoulder with..</p>
				</div>
				<div class="primarysuggestion" id="shoulderpatchstyle_D-66Z9:left_and_right_shoulder_patch" title="Left and right shoulder patch">
					<img src="images/overcoat/D-66Z9leftandrightshoulderpatch.png" />
					<p class="suggestionname">D-66Z9</br>Left and right shoulder..</p>
				</div>	
			</div>
			<div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Shoulder Tab Style</p>
				<div class="primarysuggestion" id="shouldertabstyle_D-660B:point_epaulet_with_button_and_round_button_hole" title="B Point epaulet with button and round button hole">
					<img src="images/overcoat/D-660Bpointepauletwithbuttonandroundbuttonhole.png" />
					<p class="suggestionname">D-660B</br>Point epaulet with..</p>
				</div>
				<div class="primarysuggestion" id="shouldertabstyle_D-660D:point_epaulet_with_button" title="Point epaulet with button">
					<img src="images/overcoat/D-660Dpointepauletwithbutton.png" />
					<p class="suggestionname">D-660D</br>Point epaulet with..</p>
				</div>
				<div class="primarysuggestion" id="shouldertabstyle_D-660E:rounded_epaulet_with_button_and_rounded_button_hole" title="Rounded epaulet with button and rounded button hole">
					<img src="images/overcoat/D-660Eroundedepauletwithbuttonandroundedbuttonhole.png" />
					<p class="suggestionname">D-660E</br>Rounded epaulet with..</p>
				</div>
				<div class="primarysuggestion" id="shouldertabstyle_D-660F:rounded_epaulet_button_hole" title="Rounded epaulet with button and rounded button hole">
					<img src="images/overcoat/D-660Froundedepauletbuttonhole.png" />
					<p class="suggestionname">D-660F</br>Rounded epaulet button hole</p>
				</div>
			</div>
		</div>  		
		<!--
        ----sleeve slit style div
        --->
        <div class="col-md-12 stepdiv overflow-cmd" id="sleeveslitstyles" style="display:none">
			<p class="stylecaption">Sleeve Slit Style</p>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66B6:cuff_with_trapezoid_squared_tab_and_buttons_button_holes" title="Cuff with trape zoid squared tab and buttons button holes">
				<img src="images/overcoat/D-66B6cuffwithtrapezoidsquaredtabandbuttonsbuttonholes.png"/>
				<p class="suggestionname">D-66B6</br>Cuff with trape..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66B7:cuff_with_trapezoid_rounded_tab_and_buttons_button_holes" title="Cuff with trape zoid rounded tab and buttons button holes">
				<img src="images/overcoat/D-66B7cuffwithtrapezoidroundedtabandbuttonsbuttonholes.png"/>
				<p class="suggestionname">D-66B7</br>Cuff with trape..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66B8:cuff_with_trapezoid_pointed_tab_and_buttons_button_holes" title="cuff with trape zoid pointed tab and buttons button holes">
				<img src="images/overcoat/D-66B8cuffwithtrapezoidpointedtabandbuttonsbuttonholes.png"/>
				<p class="suggestionname">D-66B8</br>cuff with trape..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66X6:real_sleeve_slit_and_double_faced_sleeve_vent_and_real_button_hole" title="Real sleeve slit and double faced sleeve vent and real button hole">
				<img src="images/overcoat/D-66X6realsleeveslitanddoublefacedsleeveventandrealbuttonhole.png"/>
				<p class="suggestionname">D-66X6</br>Real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66X7:real_sleeve_slit_and_button_holes_with_one_side_sleeve_vent" title="Real sleeve slit and button holes with one side sleeve vent">
				<img src="images/overcoat/D-66X7realsleeveslitandbuttonholeswithonesidesleevevent.png"/>
				<p class="suggestionname">D-66X7</br>Real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66X8:real_sleeve_slit_and_double_faced_sleeve_vent_and_real_slant_button_hole" title="Real sleeve slit and double faced sleeve vent and real slant button hole">
				<img src="images/overcoat/D-66X8realsleeveslitanddoublefacedsleeveventandrealslantbuttonhol.png"/>
				<p class="suggestionname">D-66X8</br>Real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-66X9:real_sleeve_slit_and_single_faced_sleeve_vent_and_real_slant_button_hole" title="Real sleeve slit and single faced sleeve vent and real slant button hole">
				<img src="images/overcoat/D-66X9realsleeveslitandsinglefacedsleeveventandrealslantbuttonhole.png"/>
				<p class="suggestionname">D-66X9</br>Real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661C:normal_sleeve_slit_normal_slanted_button_hole" title="Normal sleeve slit normal slanted button hole">
				<img src="images/overcoat/D-661Cnormalsleeveslitnormalslantedbuttonhole.png"/>
				<p class="suggestionname">D-661C</br>Normal sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661D:real_sleeve_slit_real_slanted_button_hole" title="Real sleeve slit real slanted button hole">
				<img src="images/overcoat/D-661Drealsleeveslitrealslantedbuttonhole.png"/>
				<p class="suggestionname">D-661D</br>Real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661F:normal_sleeve_slit_without_button_and_button_hole_decorating_hand_stitch_fixed_on_cuff" title="Normal sleeve slit without button and button hole decorating h and stitch fixed on cuff">
				<img src="images/overcoat/D-661Fnormalsleeveslitwithoutbuttonandbuttonholedecoratinghandstitchfixedoncuff.png"/>
				<p class="suggestionname">D-661F</br>Normal sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661G:no_sleeve_slit_pointed_sleeve_tab_with_button_and_button_hole" title="No sleeve slit pointed sleeve tab with button and button hole">
				<img src="images/overcoat/D-661Gnosleeveslitpointedsleevetabwithbuttonandbuttonhole.png"/>
				<p class="suggestionname">D-661G</br>No sleeve slit pointed..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661H:no_sleeve_slit_pointed_sleeve_tab_button" title="No sleeve slit pointed sleeve tab button">
				<img src="images/overcoat/D-661Hnosleeveslitpointedsleevetabbutton.png"/>
				<p class="suggestionname">D-661H</br>No sleeve slit pointed..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661J:no_sleeve_slit_squared_sleeve_tab_round_button_hole_and_button" title="No sleeve slit squared sleeve tab round button hole and button">
				<img src="images/overcoat/D-661Jnosleeveslitsquaredsleevetabroundbuttonholeandbutton.png"/>
				<p class="suggestionname">D-661J</br>No sleeve slit squared..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661K:no_sleeve_slit_square_tap_and_button" title="No sleeve slit square tap and button">
				<img src="images/overcoat/D-661Knosleeveslitsquaretapandbutton.png"/>
				<p class="suggestionname">D-661K</br>No sleeve slit square..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661L:no_sleeve_slit_rounded_sleeve_tab_round_button_hole_and_button" title="No sleeve slit rounded sleeve tab round button hole and button">
				<img src="images/overcoat/D-661Lnosleeveslitroundedsleevetabroundbuttonholeandbutton.png"/>
				<p class="suggestionname">D-661L</br>No sleeve slit rounded..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661M:no_sleeve_slit_rounded_sleeve_tab_with_button" title="No sleeve slit rounded sleeve tab with button">
				<img src="images/overcoat/D-661Mnosleeveslitroundedsleevetabwithbutton.png"/>
				<p class="suggestionname">D-661M</br>No sleeve slit rounded..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661N:real_sleeve_slit_no_button_hole_no_button_temporary_fix_band_by_hand" title="Real sleeve slit no button hole no button temporary fix band by hand">
				<img src="images/overcoat/D-661Nrealsleeveslitnobuttonholenobuttontemporaryfixbandbyhand.png"/>
				<p class="suggestionname">D-661N</br>Real sleeve slit no..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661R:real_sleeve_slit_no_button_hole_no_button_and_customer_appoint_double_faced_finished_sleeve_vent" title="Real sleeve slit no button hole no button and customer appoint double faced finished sleeve vent">
				<img src="images/overcoat/D-661Rrealsleeveslitnobuttonholenobuttonandcustomerappointdoublefacedfinishedsleevevent.png"/>
				<p class="suggestionname">D-661R</br>Real sleeve slit no..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661W:no_sleeve_slit_without_button_hole_38_turn_up_the_outside_of_cuff_with_one_large_button" title="No sleeve slit without button hole 38 turn up the outside of cuff with one large button">
				<img src="images/overcoat/D-661Wnosleeveslitwithoutbuttonhole38turnuptheoutsideofcuffwithonelargebutton.png"/>
				<p class="suggestionname">D-661W</br>No sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661X:no_sleeve_slit_without_button_hole_76_turn_up_the_outside_of_cuff_with_one_large_button" title="no sleeve slit without button hole 76 turn up the out side of cuff with one large button">
				<img src="images/overcoat/D-661Xnosleeveslitwithoutbuttonhole76turnuptheoutsideofcuffwithonelargebutton.png"/>
				<p class="suggestionname">D-661X</br>no sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-661Y:no_sleeve_slit_without_button_hole_102_turn_up_the_outside_of_cuff_with_one_large_button" title="no sleeve slit without button hole 102 turn up the outside of cuff with one large button">
				<img src="images/overcoat/D-661Ynosleeveslitwithoutbuttonhole102turnuptheoutsideofcuffwithonelargebutton.png"/>
				<p class="suggestionname">D-661Y</br>no sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-662H:real_sleeve_slit_and_single_faced_sleeve_vent_and_no_button_hole" title="real sleeve slit and single faced sleeve vent and no button hole">
				<img src="images/overcoat/D-662Hrealsleeveslitandsinglefacedsleeveventandnobuttonhole.png"/>
				<p class="suggestionname">D-662H</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-665A:no_sleeve_slit_squared_sleeve_tab_round_button_hole_button_with_adjust_button" title="no sleeve slit squared sleeve tab round button hole button with ad just button">
				<img src="images/overcoat/D-665Anosleeveslitsquaredsleevetabroundbuttonholebuttonwithadjustbutton.png"/>
				<p class="suggestionname">D-665A</br>no sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6611:normal_sleeve_slit_normal_button_holes" title="normal sleeve slit normal button holes">
				<img src="images/overcoat/D-6611normalsleeveslitnormalbuttonholes.png"/>
				<p class="suggestionname">D-6611</br>normal sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6612:real_slit_real_button_holes" title="real slit real button holes">
				<img src="images/overcoat/D-6612realslitrealbuttonholes.png"/>
				<p class="suggestionname">D-6612</br>real slit real..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6613:no_sleeve_slit_no_button_holes" title="no sleeve slit no button holes">
				<img src="images/overcoat/D-6613nosleeveslitnobuttonholes.png"/>
				<p class="suggestionname">D-6613</br>no sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6614:normalsleeveslitwithoutbuttonandbuttonhole" title="normal sleeve slit without button and button hole">
				<img src="images/overcoat/D-6614normalsleeveslitwithoutbuttonandbuttonhole.png"/>
				<p class="suggestionname">D-6614</br>normal sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6615:real_sleeve_slit_without_button_and_button_hole" title="real sleeve slit without button and button hole">
				<img src="images/overcoat/D-6615realsleeveslitwithoutbuttonandbuttonhole.png"/>
				<p class="suggestionname">D-6615</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6616:real_sleeve_slit_cuff_without_button_and_button_hole" title="real sleeve slit cuff without button and button hole">
				<img src="images/overcoat/D-6616realsleeveslitcuffwithoutbuttonandbuttonhole.png"/>
				<p class="suggestionname">D-6616</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6617:slit_sleeve_cuff_with_trapezoid_tab_button_and_button_hole" title="slit sleeve cuff with trape zoid tab button and button hole">
				<img src="images/overcoat/D-6617slitsleevecuffwithtrapezoidtabbuttonandbuttonhole.png"/>
				<p class="suggestionname">D-6617</br>slit sleeve cuff..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6618:slit_sleeve_with_trapezoid_pointed_tab_button_and_button_hole_cuff" title="slit sleeve with trape zoid pointed tab button and button hole cuff">
				<img src="images/overcoat/D-6618slitsleevewithtrapezoidpointedtabbuttonandbuttonholecuff.png"/>
				<p class="suggestionname">D-6618</br>slit sleeve with..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6620:slit_sleeve_with_trapezoid_round_tab_button_and_button_hole_cuff" title="slit sleeve with trape zoid round tab button and button hole cuff">
				<img src="images/overcoat/D-6620slitsleevewithtrapezoidroundtabbuttonandbuttonholecuff.png"/>
				<p class="suggestionname">D-6620</br>slit sleeve with..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6961:real_sleeve_slit_no_button_hole_and_customer_appoint_double_faced_finished_sleeve_vent" title="real sleeve slit no button hole and customer appoint double faced finished sleeve vent">
				<img src="images/overcoat/D-6961realsleeveslitnobuttonholeandcustomerappointdoublefacedfinishedsleevevent.png"/>
				<p class="suggestionname">D-6961</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6962:real_sleeve_slit_and_real_button_hole_and_customer_appoint_double_faced_finished_sleeve_vent" title="real sleeve slit and real button hole and customer appoint double faced finished sleeve vent">
				<img src="images/overcoat/D-6962realsleeveslitandrealbuttonholeandcustomerappointdoublefacedfinishedsleevevent.png"/>
				<p class="suggestionname">D-6962</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6963:real_sleeve_slit_no_button_hole_and_customer_appoint_single_faced_finished_sleeve_vent" title="real sleeve slit no button hole and customer appoint single faced finished sleeve vent">
				<img src="images/overcoat/D-6963realsleeveslitnobuttonholeandcustomerappointsinglefacedfinishedsleevevent.png"/>
				<p class="suggestionname">D-6963</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6964:real_sleeve_slit_and_real_button_hole_and_customer_appoint_single_faced_finished_sleeve_vent" title="real sleeve slit and real button hole and customer appoint single faced finished sleeve vent">
				<img src="images/overcoat/D-6964realsleeveslitandrealbuttonholeandcustomerappointsinglefacedfinishedsleevevent.png"/>
				<p class="suggestionname">D-6964</br>real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6965:real_sleeve_slit_and_real_slant_button_hole_and_customer_appoint_double_faced_finished" title="Real sleeve slit and real slant button hole and customer appoint double faced finished">
				<img src="images/overcoat/D-6965realsleeveslitandrealslantbuttonholeandcustomerappointdoublefacedfinished.png"/>
				<p class="suggestionname">D-6965</br>Real sleeve slit..</p>
			</div>
			<div class="primarysuggestion" id="sleeveslitstyle_D-6966:real_sleeve_slit_and_real_slant_button_hole_and_customer_appoint_single_faced_finished_sleeve_vent" title="real sleeve slit and real slant button hole and customer appoint single faced finished sleeve vent">
				<img src="images/overcoat/D-6966realsleeveslitandrealslantbuttonholeandcustomerappointsinglefacedfinishedsleevevent.png"/>
				<p class="suggestionname">D-6966</br>real sleeve slit..</p>
			</div>
            <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
				<p class="stylecaption">Sleeve Button</p>
                <div class="primarysuggestion" id="sleevebuttonstyle_D-6621:four_flat_sleeve_button" title="four flat sleeve button">
                    <img src="images/overcoat/D-6621fourflatsleevebutton.png" />
                    <p class="suggestionname">D-6621</br>four flat sleeve..</p>
                </div>
                <div class="primarysuggestion" id="sleevebuttonstyle_D-6622:four_overlap" title="">
                    <img src="images/overcoat/D-6621fourflatsleevebutton.png" />
                    <p class="suggestionname">D-6622</br>four overlap</p>
                </div>
                <div class="primarysuggestion" id="sleevebuttonstyle_D-69X9:five_sleeve_buttons_last_button_0.3cm_from_cuff_edge" title="5 sleeve buttons last button 03cm from cuff edge">
                    <img src="images/overcoat/D-69X95sleevebuttonslastbutton03cmfromcuffedge.png" />
                    <p class="suggestionname">D-69X9</br>5 sleeve buttons..</p>
                </div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-66A7:sleeve_button_one_small_button" title="sleeve button 1 small button">
					<img src="images/overcoat/D-66A7sleevebutton1smallbutton.png" />
					<p class="suggestionname">D-66A7</br>sleeve button 1 small</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-66X1:sleeve_button_six_plain_buttons" title="sleeve button 6 plain buttons">
					<img src="images/overcoat/D-66X1sleevebutton6plainbuttons.png" />
					<p class="suggestionname">D-66X1</br>sleeve button 6 plain..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X0:five_sleeve_buttons_last_button_0.6cm_from_cuff_edge" title="5 sleeve buttons last button 06 cm from cuff edge">
					<img src="images/overcoat/D-69X05sleevebuttonslastbutton06cmfromcuffedge.png" />
					<p class="suggestionname">D-69X0</br>5 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X3:four_sleeve_buttons_last_button_0.3cm_from_cuff_edge" title="4 sleeve buttons last button 03cm from cuff edge">
					<img src="images/overcoat/D-69X34sleevebuttonslastbutton03cmfromcuffedge.png" />
					<p class="suggestionname">D-69X3</br>4 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X4:four_sleeve_buttons_last_button_0.6cm_from_cuff_edge" title="4 sleeve buttons last button 06cm from cuff edge">
					<img src="images/overcoat/D-69X44sleevebuttonslastbutton06cmfromcuffedge.png" />
					<p class="suggestionname">D-69X4</br>4 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X5:five_sleeve_buttons_last_button_0.3cm_from_cuff_edge" title="5 sleeve buttons last button 03cm from cuff edge">
					<img src="images/overcoat/D-69X55sleevebuttonslastbutton03cmfromcuffedge.png" />
					<p class="suggestionname">D-69X5</br>5 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X6:two_sleeve_buttons_last_button_0.6cm_from_cuff_edge" title="2 sleeve buttons last button 06cm from cuff edge">
					<img src="images/overcoat/D-69X62sleevebuttonslastbutton06cmfromcuffedge.png" />
					<p class="suggestionname">D-69X6</br>2 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X7:three_sleeve_buttons_last_button_0.3cm_from_cuff_edge" title="3 sleeve buttons last button 03cm from cuff edge">
					<img src="images/overcoat/D-69X73sleevebuttonslastbutton03cmfromcuffedge.png" />
					<p class="suggestionname">D-69X7</br>3 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-69X8:three_sleeve_buttons_last_button_0.6cm_from_cuff_edge" title="3 sleeve buttons last button 06cm from cuff edge">
					<img src="images/overcoat/D-69X83sleevebuttonslastbutton06cmfromcuffedge.png" />
					<p class="suggestionname">D-69X8</br>3 sleeve buttons..</p>
				</div>
                <div class="primarysuggestion" id="sleevebuttonstyle_D-662Y:two_sleeve_buttons_last_button_3.0cm_from_cuff_edge" title="2 sleeve buttons last button 30cm from cuff edge">
					<img src="images/overcoat/D-662Y2sleevebuttonslastbutton30cmfromcuffedge.png" />
					<p class="suggestionname">D-662Y</br>2 sleeve buttons..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-662Z:sleeve_button_six_overlap_buttons" title="sleeve button 6 overlap buttons">
					<img src="images/overcoat/D-662Zsleevebutton6overlapbuttons.png" />
					<p class="suggestionname">D-662Z</br>sleeve button 6..</p>
				</div>
                <div class="primarysuggestion" id="sleevebuttonstyle_D-6623:one_big_button" title="sleeve button 1 big button">
					<img src="images/overcoat/D-6623sleevebutton1bigbutton.png" />
					<p class="suggestionname">D-6623</br>sleeve button 1..</p>
				</div>
				<div class="primarysuggestion" id="sleevebuttonstyle_D-6624:sleeve_button_three_plain_button" title="sleeve button 3 plain button">
					<img src="images/overcoat/D-6624sleevebutton3plainbutton.png" />
					<p class="suggestionname">D-6624</br>sleeve button 3..</p>
				</div>
			</div>
		</div>
		<!--
        ----back vent style div
        --->
        <div class="col-md-12 stepdiv" id="backvents" style="display:none">
			<p class="stylecaption">Back Vent Style</p>
            <div class="primarysuggestion" id="backvent_D-64N0:no_vent">
                <img src="images/overcoat/D-64NOnovent.png"/>
                <p class="suggestionname">D-64N0</br>No Vent</p>
            </div>
            <div class="primarysuggestion" id="backvent_D-64N1:central_vent">
                <img src="images/overcoat/D-64N1centralvent.png"/>
                <p class="suggestionname">D-64N1</br>Central Vent</p>
            </div>
            <div class="primarysuggestion" id="backvent_D-64N2:back_side_vents">
                <img src="images/overcoat/D-64N2backsidevents.png"/>
                <p class="suggestionname">D-64N2</br>Back Side Vents</p>
            </div>
			<div class="primarysuggestion" id="backvent_D-64M1:fashion_blade_pleat_with_center_vent" title="Fashion blade pleat with center vent">
				<img src="images/overcoat/D-64M1fashionbladepleatwithcentervent.png"/>
				<p class="suggestionname">D-64M1</br>Fashion Blade Pleat..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64M2:fashion_blade_pleat_with_side_vents" title="Fashion blade pleat with side vents">
				<img src="images/overcoat/D-64M2fashionbladepleatwithsidevents.png"/>
				<p class="suggestionname">D-64M2</br>Fashion Blade Pleat..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64M3:fashion_blade_pleat_with_side_vents_and_vent_strap" title="Fashion blade pleat with side vents & vent strap">
				<img src="images/overcoat/D-64M3fashionbladepleatwithsideventsandventstrap.png"/>
				<p class="suggestionname">D-64M3</br>Fashion Blade Pleat..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64M4:fashion_blade_pleat_with_side_vents_and_waist_strap" title="Fashion blade pleat with side vents & waist strap">
				<img src="images/overcoat/D-64M4fashionbladepleatwithsideventsandwaiststrap.png"/>
				<p class="suggestionname">D-64M4</br>Fashion Blade Pleat..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64M0:fashion_blade_pleat_with_no_vent" title="Fashion blade pleat with no vent">
				<img src="images/overcoat/D-64MOfashionbladepleatwithnovent.png"/>
				<p class="suggestionname">D-64M0</br>Fashion Blade Pleat..</p>
			</div>
            <div class="primarysuggestion" id="backvent_D-64N3:side_vents_with_vent_strap" title="side vents with vent strap">
				<img src="images/overcoat/D-64N3sideventswithventstrap.png"/>
				<p class="suggestionname">D-64N3</br>Side Vents With..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64N7:no_back_vent_with_belt_2m_long_and_2_belt_loops_on_waist_side_seam" title="no back vent with belt 2m long and 2 belt loops on waist side seam">
				<img src="images/overcoat/D-64N7nobackventwithbelt2mlongand2beltloopsonwaistsideseam.png"/>
				<p class="suggestionname">D-64N7</br>No Back Vent With..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64N8:central_vent_with_belt_2m_long_and_2_belt_loops_on_waist_side_seam" title="central vent with belt 2m long and 2 belt loops on waist side seam">
				<img src="images/overcoat/D-64N8centralventwithwithbelt2mlongand2beltloopsonwaistsideseam.png"/>
				<p class="suggestionname">D-64N8</br>central vent with..</p>
			</div>
            <div class="primarysuggestion" id="backvent_D-64T0:t_split_back_no_vent" title="T split back no vent">
				<img src="images/overcoat/D-64TOtsplitbacknovent.png"/>
				<p class="suggestionname">D-64TO</br>T split back..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64V1:blind_pleat_with_center_vent" title="Blind pleat with center vent">
				<img src="images/overcoat/D-64V1blindpleatwithcentervent.png"/>
				<p class="suggestionname">D-64V1</br>Blind pleat with..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64V2:blind_pleat_with_side_vents" title="Blind pleat with side vents">
				<img src="images/overcoat/D-64V2blindpleatwithsidevents.png"/>
				<p class="suggestionname">D-64V2</br>Blind pleat with..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64V3:blind_pleat_with_side_vents_and_vent_strap" title="Blind pleat with side vents & vent strap">
				<img src="images/overcoat/D-64V3blindpleatwithsideventsandventstrap.png"/>
				<p class="suggestionname">D-64V3</br>Blind pleat with..</p>
			</div>
			<div class="primarysuggestion" id="backvent_D-64V0:blind_pleat_with_no_vent" title="blind pleat with no vent">
				<img src="images/overcoat/D-64VOblindpleatwithnovent.png"/>
				<p class="suggestionname">D-64VO</br>blind pleat with..</p>
			</div>
		</div>
        <!--
        ----stitch style div
        --->
        <div class="col-md-12 overflow-cmd stepdiv"   id="stitchstyles" style="display:none">
            <p class="stylecaption">Outside pic stitch style</p>
            <div class="primarysuggestion" id="stitchstyle_D-657D:0.3*0.8cm_double_pick_stitch_on_all_edges" title="0.3*0.8cm double pick stitch on all edges">
                <img src="images/overcoat/D-657D0308cmdoublepickstitchonalledges.png"/>
                <p class="suggestionname">D-657D</br>0.3*0.8cm double..</p>
            </div>
            <div class="primarysuggestion"  id="stitchstyle_D-658C:0.8cm_top_stitch_on_front_including_lapel_collar_patch_pocket_chest_pocket" title="0.8cm top stitch on front including lapel collar patch pocket chest pocket">
                <img src="images/overcoat/D-658C08cmtopstitchonfrontincludinglapelcollarpatchpocketchestpocket.png"/>
                <p class="suggestionname">D-658C</br>0.8cm top stitch..</p>
            </div>
            <div class="primarysuggestion"  id="stitchstyle_D-658D:0.3cm_pic_stitch_on_lapel_only" title="0.3cm pic stitch on lapel only">
                <img src="images/overcoat/D-658D03cmpicstitchonlapelonly.png"/>
                <p class="suggestionname">D-658D</br>0.3cm pic stitch..</p>
            </div>
            <div class="primarysuggestion"  name="D-6570" id="stitchstyle_D-6570:no_pic_stitch" title="">
                <img src="images/overcoat/D-6570nopicstitch.png"/>
                <p class="suggestionname">D-6570</br>no pic stitch</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6571:0.3cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket" title="0.3cm narrow pick stitch on front including lapel collar pocket flap chest pocket">
                <img src="images/overcoat/D-657103cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocket.png"/>
                <p class="suggestionname">D-6571</br>0.3cm narrow pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6572:0.3cm_narrow_pic_stitch_on_all_edges" title="0.3cm narrow pic stitch on all edges">
                <img src="images/overcoat/D-657203cmnarrowpicstitchonalledges.png"/>
                <p class="suggestionname">D-6572</br>0.3cm narrow pic..</p>
            </div>
            <div class="primarysuggestion"  id="stitchstyle_D-6573:0.3cm_narrow_pic_stitch_on_pocket_flap_and_chest_pocket" title="0.3cm narrow pic stitch on pocket flap and chest pocket">
                <img src="images/overcoat/D-657303cmnarrowpicstitchonpocketflapandchestpocket.png"/>
                <p class="suggestionname">D-6573</br>0.3cm narrow pic..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6576:0.8cm_wide_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_sleeve_slit" title="0.8cm wide pick stitch on front including lapel collar pocket flap chest pocket sleeve slit">
                <img src="images/overcoat/D-657608cmwidepickstitchonfrontincludinglapelcollarpocketflapchestpocketsleeveslit.png"/>
                <p class="suggestionname">D-6576</br>0.8cm wide pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6577:0.8cm_wide_pick_stitch_on_all_edges" title="0.8cm wide pick stitch on all edges">
                <img src="images/overcoat/D-657708cmwidepickstitchonalledges.png"/>
                <p class="suggestionname">D-6577</br>0.8cm wide pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6578:0.8cm_wide_pick_stitch_on_pocket_flap_chest_pocket" title="0.8cm wide pick stitch on pocket flap chest pocket">
                <img src="images/overcoat/D-657808cmwidepickstitchonpocketflapchestpocket.png"/>
                <p class="suggestionname">D-6578</br>0.8cm wide pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6580:0.8cm_top_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket" title="0.8cm top stitch on front including lapel collar pocket flap chest pocket">
                <img src="images/overcoat/D-658008cmtopstitchonfrontincludinglapelcollarpocketflapchestpocket.png"/>
                <p class="suggestionname">D-6580</br>0.8cm top stitch..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6581:0.8cm_top_stitch_on_front_including_lapel_collar_patch_pocket_chest_pocket_sleeve_slit_certral_back_seam" title="0.8cm top stitch on front including lapel collar patch pocket chest pocket sleeve slit certral back seam">
                <img src="images/overcoat/D-658108cmtopstitchonfrontincludinglapelcollarpatchpocketchestpocketsleeveslitcertralbackseam.png"/>
                <p class="suggestionname">D-6581</br>0.8cm top stitch..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6582:0.8cm_top_stitch_on_all_edges" title="0.8cm top stitch on all edges">
                <img src="images/overcoat/D-658208cmtopstitchonalledges.png"/>
                <p class="suggestionname">D-6582</br>0.8cm top stitch..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6584:0.3cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_sleeve_slit" title="0.3cm narrow pick stitch on front including lapel collar pocket flap chest pocket sleeve slit">
                <img src="images/overcoat/D-658403cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocketsleeveslit.png"/>
                <p class="suggestionname">D-6584</br>0.3cm narrow pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6585:0.3cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_sleeve_outseam_shoulder_seam_sleeve_slit" title="0.3cm narrow pick stitch on front including lapel collar pocket flap chest pocket sleeve outseam shoulder seamleeve slit">
                <img src="images/overcoat/D-658503cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocketsleeveoutseamshoulderseamleeveslit.png"/>
                <p class="suggestionname">D-6585</br>0.3cm narrow pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6586:0.3cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_shoulder_seam_sleeve_slit" title="0.3cm narrow pick stitch on front including lapel collar pocket flap chest pocket shoulder seam sleeve slit">
                <img src="images/overcoat/D-658603cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocketshoulderseamsleeveslit.png"/>
                <p class="suggestionname">D-6586</br>0.3cm narrow pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6587:0.8cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_sleeve_outseam_sleeve_slit" title="0.8cm narrow pick stitch on front including lapel collar pocket flap chest pocket sleeve outseam sleeve slit">
                <img src="images/overcoat/D-658708cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocketsleeveoutseamsleeveslit.png"/>
                <p class="suggestionname">D-6587</br>0.8cm narrow pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6588:0.8cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_sleeve_outseam_shoulder_saem_sleeve_slit" title="0.8cm narrow pick stitch on front including lapel collar pocket flap chest pocket sleeve outseam shoulder saem sleeve slit">
                <img src="images/overcoat/D-658808cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocketsleeveoutseamshouldersaemsleeveslit.png"/>
                <p class="suggestionname">D-6588</br>0.8cm narrow pick..</p>
            </div>
            <div class="primarysuggestion" id="stitchstyle_D-6589:0.8cm_narrow_pick_stitch_on_front_including_lapel_collar_pocket_flap_chest_pocket_shoulder_seam_sleeve_slit" title="0.8cm narrow pick stitch on front including lapel collar pocket flap chest pocket shoulder seam sleeve slit">
                <img src="images/overcoat/D-658908cmnarrowpickstitchonfrontincludinglapelcollarpocketflapchestpocketshoulderseamsleeveslit.png"/>
                <p class="suggestionname">D-6589</br>0.8cm narrow pick..</p>
            </div>
            <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
                <p class="stylecaption">Back side of Armhole</p>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q2:wide_distance_0.3cm_narrow_pick_stitch_on_for_e_part_of_the_arm_hole" title="wide distance 0.3cm narrow pick stitch on for e part of the armhole">
                    <img src="images/overcoat/D-65Q2widedistance03cmnarrowpickstitchonforepartofthearmhole.png" />
                    <p class="suggestionname">D-65Q2</br>wide distance 0.3cm..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q4:wide_distance_0.8cm_pick_stitch_on_for_e_part_of_the_arm_hole" title="wide distance 0.8cm pick stitch on for epart of the arm hole">
                    <img src="images/overcoat/D-65Q4widedistance08cmpickstitchonforepartofthearmhole.png" />
                    <p class="suggestionname">D-65Q4</br>wide distance 0.8cm..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q1:0.3cm_narrow_pick_stitch_on_for_e_part_of_the_arm_hole" title="0.3cm narrow pick stitch on for epart of the armhole">
                    <img src="images/overcoat/D-65Q103cmnarrowpickstitchonforepartofthearmhole.png" />
                    <p class="suggestionname">D-65Q1</br>0.3cm narrow pick..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q3:0.8cm_wide_pick_stitch_on_for_e_part_of_the_arm_hole" title="0.8cm wide pick stitch on for epart of the armhole">
                    <img src="images/overcoat/D-65Q308cmwidepickstitchonforepartofthearmhole.png" />
                    <p class="suggestionname">D-65Q3</br>0.8cm wide pick..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q5:0.3cm_narrow_top_stitch_on_for_e_part_of_the_arm_hole" title="0.3 cm narrow top stitch on for e part of the arm hole">
                    <img src="images/overcoat/D-65Q503cmnarrowtopstitchonforepartofthearmhole.png" />
                    <p class="suggestionname">D-65Q5</br>0.3 cm narrow top..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q6:0.8cm_wide_top_stitch_on_for_e_part_of_the_arm_hole" title="0.8cm wide top stitch on for e part of the arm hole">
                    <img src="images/overcoat/D-65Q608cmwidetopstitchonforepartofthearmhole.png" />
                    <p class="suggestionname">D-65Q6</br>0.8cm wide top..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q7:0.8cm_wide_pick_stitch_on_the_front_and_back_of_arm_hole" title="0.8 cm wide pick stitch on the front and back of armhole">
                    <img src="images/overcoat/D-65Q708cmwidepickstitchonthefrontandbackofarmhol.png" />
                    <p class="suggestionname">D-65Q7</br>0.8 cm wide pick..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q8:0.3cm_top_stitch_on_both_for_e_part_and_back_side_of_the_arm_hole" title="0.3cm top stitch on both for e part and back side of the arm hole">
                    <img src="images/overcoat/D-65Q803cmtopstitchonbothforepartandbacksideofthearmhole.png" />
                    <p class="suggestionname">D-65Q8</br>0.3cm top stitch..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-65Q9:0.8cm_wide_top_stitch_on_both_for_e_part_and_back_side_of_the_arm_hole" title="0.8cm wide top stitch on both fore part and back side of the arm hole">
                    <img src="images/overcoat/D-65Q908cmwidetopstitchonbothforepartandbacksideofthearmhole.png" />
                    <p class="suggestionname">D-65Q9</br>0.8cm wide top..</p>
                </div>
                <div class="primarysuggestion" id="backsideofarmhole_D-6608:0.3cm_pick_stitch_on_the_front_and_back_of_arm_hole" title="0.3cm pick stitch on the front and back of arm hole">
                    <img src="images/overcoat/D-660803cmpickstitchonthefrontandbackofarmhole.png" />
                    <p class="suggestionname">D-6608</br>0.3cm pick stitch..</p>
                </div>
            </div>
            <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
                <p class="stylecaption">top stitch styles on shoulder seam</p>
                <div class="primarysuggestion" id="topstichstyle_C-6583:0.8cm_top_stitch_on_shoulder_seam" title="0.8cm top stitch on shoulder seam">
                    <img src="images/overcoat/C-658308cmtopstitchonshoulderseam.png" />
                    <p class="suggestionname">C-6583</br>0.8cm top stitch..</p>
                </div>
                <div class="primarysuggestion" id="topstichstyle_D-6575:0.3cm_pic_stitch_on_shoulder_seam" title="0.3cm pic stitch on shoulder seam">
                    <img src="images/overcoat/D-657503cmpicstitchonshoulderseam.png" />
                    <p class="suggestionname">D-6575</br>0.3cm pic stitch..</p>
                </div>
                <div class="primarysuggestion" id="topstichstyle_D-6591:0.8cm_wide_top_stitch_on_shoulder_seam" title="0.8cm wide top stitch on shoulder seam">
                    <img src="images/overcoat/D-659108cmwidetopstitchonshoulderseam.png" />
                    <p class="suggestionname">D-6591</br>0.8cm wide top..</p>
                </div>
                <div class="primarysuggestion" id="topstichstyle_D-6592:0.3cm_loose_narrow_pick_stitch_on_shoulder_seam" title="0.3cm loose narrow pick stitch on shoulder seam">
                    <img src="images/overcoat/D-659203cmloosenarrowpickstitchonshoulderseam.png" />
                    <p class="suggestionname">D-6592</br>0.3cm loose narrow..</p>
                </div>
                <div class="primarysuggestion" id="topstichstyle_D-6593:0.3cm_narrow_top_stitch_on_shoulder_seam" title="0.3cm narrow top stitch on shoulder seam">
                    <img src="images/overcoat/D-659303cmnarrowtopstitchonshoulderseam.png" />
                    <p class="suggestionname">D-6593</br>0.3cm narrow top..</p>
                </div>
                <div class="primarysuggestion" id="topstichstyle_D-6596:0.8cm_loose_wide_stitch_on_shoulder_seam" title="0.8cm loose wide stitch on shoulder seam">
                    <img src="images/overcoat/D-659608cmloosewidestitchonshoulderseam.png" />
                    <p class="suggestionname">D-6596</br>0.8cm loose wide..</p>
                </div>
            </div>
        </div>
        <!--
        ----breast pocket style div
        --->
        <div class="col-md-12 overflow-cmd stepdiv" id="breastpockets" style="display:none">
            <p class="stylecaption">Breast Pocket Style</p>
            <div class="primarysuggestion" id="breastpocket_D-6100:no_breast_pocket" title="no breast pocket">
                <img src="images/overcoat/D-6100nobreastpocket.png"/>
                <p class="suggestionname">D-6100</br>no breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6101:normal_breast_pocket" title="normal breast pocket">
                <img src="images/overcoat/D-6100nobreastpocket.png"/>
                <p class="suggestionname">D-6101</br>normal breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6150:patch_breast_pocket" title="patch breast pocket">
                <img src="images/overcoat/D-6150patchbreastpocket.png"/>
                <p class="suggestionname">D-6150</br>patch breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6102:arc_breast_pocket" title="arc breast pocket">
                <img src="images/overcoat/D-6102arcbreastpocket.png"/>
                <p class="suggestionname">D-6102</br>arc breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6103:ship_shape_breast_pocket" title="normal breast pocket">
                <img src="images/overcoat/D-6103shipshapebreastpocket.png"/>
                <p class="suggestionname">D-6103</br>ship shape breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-61A1:double_besom_with_pointed_tab_and_button_breast_pockets" title="double besom with pointed tab and button breast pockets">
                <img src="images/overcoat/D-61A1doublebesomwithpointedtabandbuttonbreastpockets.png"/>
                <p class="suggestionname">D-61A1</br>double besom with</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-61A2:double_besom_with_square_tab_with_button_breast_pocket" title="double besom with square tab with button breast pocket">
                <img src="images/overcoat/D-61A2doublebesomwithsquaretabwithbuttonbreastpocket.png"/>
                <p class="suggestionname">D-61A2</br>double besom with...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-61A3:double_besom_with_round_tab_and_button_breast_pockets" title="double besom with round tab and button breastpockets">
                <img src="images/overcoat/D-61A3doublebesomwithroundtabandbuttonbreastpockets.png"/>
                <p class="suggestionname">D-61A3</br>double besom with...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-61A4:patch_pocket_with_flap_button_buttonhole" title="patch pocket with flap button buttonhole">
                <img src="images/overcoat/D-61A4patchpocketwithflapbuttonbuttonhole.png"/>
                <p class="suggestionname">D-61A4</br>patch pocket with flap...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-0189:left_and_right_double_besom_chest_pocket_with_flap_and_button" title="left and right double besom chest pocket with flap and button">
                <img src="images/overcoat/D-0189leftandrightdoublebesomchestpocketwithcsfalpandbutton.png"/>
                <p class="suggestionname">D-0189</br>left and right double...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-665N:breast_pocket_fabric_customer_appointed" title="Nbreast pocket fabric customer appointed">
                <img src="images/overcoat/D-665Nbreastpocketfabriccustomerappointed.png"/>
                <p class="suggestionname">D-665</br>Nbreast pocket fabric...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-665W:1.0cm_wide_split_fabric_on_top_pocket_customer_appointed" title="cm wide split fabric onto ppocket customer appointed">
                <img src="images/overcoat/D-665W10cmwidesplitfabricontoppocketcustomerappointed.png"/>
                <p class="suggestionname">D-665W10</br>cm wide split...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6110:besom_chest_pocket" title="besom breast pocket">
                <img src="images/overcoat/D-6110besomchestpocket.png"/>
                <p class="suggestionname">D-6110</br>besom chest pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6111:double_besom_with_tab_and_button_breast_pockets" title="double besom with tab and button breast pockets">
                <img src="images/overcoat/D-6111doublebesomwithtabandbuttonbreastpockets.png"/>
                <p class="suggestionname">D-6111</br>double besom with tab...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6113:double_besom_with_flap_breast_pocket" title="double besom with tab and button breast pockets">
                <img src="images/overcoat/D-6113doublebesomwithflapbreastpocket.png"/>
                <p class="suggestionname">D-6113</br>double besom with...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6114:zip_besom_pocket_with_point_flap_and_button" title="zip besom pocket with point flap and button">
                <img src="images/overcoat/D-6114zipbesompocketwithpointflapandbutton.png"/>
                <p class="suggestionname">D-6114</br>zip besom pocket with...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6115:double_besom_zip_pocket" title="double besom zip pocket">
                <img src="images/overcoat/D-6115doublebesomzippocket.png"/>
                <p class="suggestionname">D-6115</br>double besom zip pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6116:double_besom_pocket_with_diamond_flap" title="zip besom pocket with point flap and button">
                <img src="images/overcoat/D-6116doublebesompocketwithdiamondflap.png"/>
                <p class="suggestionname">D-6116</br>double besom pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6119:double_besom_with_flap_and_button_breast_pocket" title="double besom with flap and button breast pocket">
                <img src="images/overcoat/D-6119doublebesomwithflapandbuttonbreastpocket.png"/>
                <p class="suggestionname">D-6119</br>double besom with flap...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6121:patch_pocket_one_pleat" title="double besom with flap and button breast pocket">
                <img src="images/overcoat/D-6121patchpocketonepleat.png"/>
                <p class="suggestionname">D-6121</br>patch pocket one pleat...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6151:patch_chest_pocket_with_button_buttonhole" title="patch breast pocket">
                <img src="images/overcoat/D-6151patchchestpocketwithbuttonbuttonhole.png"/>
                <p class="suggestionname">D-6151</br>patch chest pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6153:patch_pocket_with_single_pleat_and_flap" title="patch breast pocket">
                <img src="images/overcoat/D-6153patchpocketwithsignlepleatandflap.png"/>
                <p class="suggestionname">D-6153</br>patch pocket with...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6154:patch_pocket_with_flap" title="patch pocket with flap">
                <img src="images/overcoat/D-6154patchpocketwithflap.png"/>
                <p class="suggestionname">D-6154</br>patch pocket with flap...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6155:single_pleat_patch_chest_pocket_with_diamond_flap_and_button_button_hole" title="single pleat patch chest pocket with diamond flap and button button hole">
                <img src="images/overcoat/D-6155singlepleatpatchchestpocketwithdiamondflapandbuttonbuttonhole.png"/>
                <p class="suggestionname">D-6155</br>single pleat patch chest ...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6180:left_divide_pocket_with_button_and_button_hole" title="left divide pocket with button and button hole">
                <img src="images/overcoat/D-6180leftdividepocketwithbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-6180</br>left divide pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6183:left_and_right_division_and_left_breast_pocket_with_button_and_button_hole" title="left and right division and left breast pocket with button and button hole">
                <img src="images/overcoat/D-6183leftandrightdivisionandleftbreastpocketwithbuttonandbuttonhole.png"/>
                <p class="suggestionname">D-6183</br>left and right division...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6184:front_part_with_left_right_segmentation_crafts" title="front part with left right segmentation crafts">
                <img src="images/overcoat/D-6184frontpartwithleftrightsegmentationcrafts.png"/>
                <p class="suggestionname">D-6184</br>front part with left right...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6186:left_and_right_division_breast_pocket" title="left and right division breast pocket">
                <img src="images/overcoat/D-6186leftandrightdivisionbreastpocket.png"/>
                <p class="suggestionname">D-6186</br>left and right division...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-631A:pull_breast_pocket_pocket_fabric_customer_appointed" title="left and right division breast pocket">
                <img src="images/overcoat/D-631Apullbreastpocketpocketfabriccustomerappointed.png"/>
                <p class="suggestionname">D-631A</br>pull breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-631B:pull_breast_pocket_use_opposite_side_of_fabric_customer_appointed" title="pull breast pocket use opposite side of fabric customer appointed">
                <img src="images/overcoat/D-631Bpullbreastpocketuseoppositesideoffabriccustomerappointed.png"/>
                <p class="suggestionname">D-631B</br>pull breast pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6310:breast_pocket_hidden_seal_stitch" title="breast pocket hidden seal stitch">
                <img src="images/overcoat/D-6310breastpockethiddensealstitch.png"/>
                <p class="suggestionname">D-6310</br>breast pocket hidden...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6311:pull_pocket" title="breast pocket hidden seal stitch">
                <img src="images/overcoat/D-6311pullpocket.png"/>
                <p class="suggestionname">D-6311</br>pull pocket...</p>
            </div>
            <div class="primarysuggestion" id="breastpocket_D-6312:breast_pocket_opened_inner_fabric_cant_be_rolled_out" title="breast pocket opened inner fabric cant be rolled out">
                <img src="images/overcoat/D-6312breastpocketopenedinnerfabriccantberolledout.png"/>
                <p class="suggestionname">D-6312</br>breast pocket opened...</p>
            </div>
        </div>
        <!--
        ----Coin Pocket Style div
        --->
        <div class="col-md-12 overflow-cmd stepdiv" id="coinpockets" style="display:none">
            <p class="stylecaption">Coin Pocket Style</p>
            <div class="primarysuggestion" id="coinpocket_D-6320:no_exterior_lower_coin_pocket" title="no exterior lower coin pocket">
                <img src="images/overcoat/D-6320noexteriorlowercoinpocket.png"/>
                <p class="suggestionname">D-6320</br>no exterior... </p>
            </div>
            <div class="primarysuggestion" id="coinpocket_D-6321:right_exterior_lower_coin_pocket" title="right exterior lower coin pocket">
                <img src="images/overcoat/D-6321rightexteriorlowercoinpocket.png"/>
                <p class="suggestionname">D-6321</br>right exterior lower... </p>
            </div>
            <div class="primarysuggestion" id="coinpocket_D-6322:left_exterior_lower_coin_pocket" title="left exterior lower coin pocket">
                <img src="images/overcoat/D-6322leftexteriorlowercoinpocket.png"/>
                <p class="suggestionname">D-6322</br>left exterior lower... </p>
            </div>
            <div class="primarysuggestion" id="coinpocket_D-6323:both_sides_exterior_lower_coin_pocket" title="both side sexterior lower coin pocket">
                <img src="images/overcoat/D-6323bothsidesexteriorlowercoinpocket.png"/>
                <p class="suggestionname">D-6323</br>both side sexterior... </p>
            </div>
            <div class="primarysuggestion" id="coinpocket_D-6324:double_right_exterior_lower_coin_pocket" title="both side sexterior lower coin pocket">
                <img src="images/overcoat/D-6324doublerightexteriorlowercoinpocket.png"/>
                <p class="suggestionname">D-6324</br>double right exterior... </p>
            </div>
            <div class="primarysuggestion" id="coinpocket_D-6325:double_both_sides_exterior_lower_coin_pocket" title="double both sides exterior lower coin pocket">
                <img src="images/overcoat/D-6325doublebothsidesexteriorlowercoinpocket.png"/>
                <p class="suggestionname">D-6325</br>double both sides exterior... </p>
            </div>
        </div>
        <!--
        ----facing Style div
        --->
        <div class="col-md-12 overflow-cmd stepdiv" id="facingstyles" style="display:none">
            <p class="stylecaption">Facing Style</p>
            <div class="primarysuggestion" id="facingstyle_D-6130:straight_inner_facing_fly_front" title="straight inner facing fly front">
                <img src="images/overcoat/D-6130straightinnerfacingflyfront.png"/>
                <p class="suggestionname">D-6130</br>straight inner facing..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6131:french_facing_fly_front" title="">
                <img src="images/overcoat/D-6131frenchfacingflyfront.png"/>
                <p class="suggestionname">D-6131</br>french facing fly front</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6132:curved_french_facing_two_piece_fly_front" title="curved french facing two piece fly front">
                <img src="images/overcoat/D-6132curvedfrenchfacingtwopieceflyfront.png"/>
                <p class="suggestionname">D-6132</br>curved french facing..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6133:flat_french_facing_fly_front" title="flat french facing fly front">
                <img src="images/overcoat/D-6133flatfrenchfacingflyfront.png"/>
                <p class="suggestionname">D-6133</br>flat french facing..</p>
            </div>
            <div class="primarysuggestion" " id="facingstyle_D-6134:point_flat_french_facing_fly_front" title="point flat french facing fly front">
                <img src="images/overcoat/D-6134pointflatfrenchfacingflyfront.png"/>
                <p class="suggestionname">D-6134</br>point flat french..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6135:fabric_facing_for_1314_or_no_lining_options_only_fly_front" title="fabric facing for 1314 or no lining options only fly front">
                <img src="images/overcoat/D-6135fabricfacingfor1314ornoliningoptionsonlyflyfront.png"/>
                <p class="suggestionname">D-6135</br>fabric facing for..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6136:point_curved_french_facing_fly_front" title="point curved french facing fly front">
                <img src="images/overcoat/D-6136pointcurvedfrenchfacingflyfront.png"/>
                <p class="suggestionname">D-6136</br>point curved french..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6139:classic_parallel_one_clear_button_the_rest_hidden_placket" title="classic parallel one clear button the rest hidden placket">
                <img src="images/overcoat/D-6139classicparalleloneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6139</br>classic parallel one..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6140:curved_french_facing_one_piece_one_clear_button_the_rest_hidden_placket" title="curved french facing one piece one clear button the rest hidden placket">
                <img src="images/overcoat/D-6140curvedfrenchfacingonepieceoneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6140</br>curved french facing..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6141:curved_french_facing_two_piece_one_clear_button_the_rest_hidden_placket" title="curved french facing two piece one clear button the rest hidden placket">
                <img src="images/overcoat/D-6141curvedfrenchfacingtwopieceoneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6141</br>curved french facing..</p>
            </div>
            <div class="primarysuggestion"  id="facingstyle_D-6142:flat_french_facing_one_clear_button_the_rest_hidden_placket" title="flat french facing one clear button the rest hidden placket">
                <img src="images/overcoat/D-6142flatfrenchfacingoneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6142</br>flat french facing..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6143:point_flat_french_facing_upper_lower_one_clear_button_the_rest_hidden_placket" title="point flat french facing upper & lower one clear button the rest hidden placket">
                <img src="images/overcoat/D-6143pointflatfrenchfacingupper&loweroneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6143</br>point flat french..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6144:fabric_facing_13_lining_14_lining_or_no_lining_one_clear_button_the_rest_hidden_placket" title="fabric facing 1.3 lining 1.4 lining or no lining one clear button the rest hidden placket">
                <img src="images/overcoat/D-6144fabricfacing13lining14liningornoliningoneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6144</br>fabric facing 1.3..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6145:point_curved_french_facing_upper_lower_one_clear_button_the_rest_hidden_placket" title="point curved french facing upper lower one clear button the rest hidden placket">
                <img src="images/overcoat/D-6145pointcurvedfrenchfacingupperloweroneclearbuttontheresthiddenplacket.png"/>
                <p class="suggestionname">D-6145</br>point curved french..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6701:straight_inner_facing" title="">
                <img src="images/overcoat/D-6701straightinnerfacing.png"/>
                <p class="suggestionname">D-6701</br>straight inner facing</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6702:french_facing" title="">
                <img src="images/overcoat/D-6702frenchfacing.png"/>
                <p class="suggestionname">D-6702</br>french facing</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6703:curved_french_facing_two_piece" title="curved french facing two piece">
                <img src="images/overcoat/D-6703curvedfrenchfacingtwopiece.png"/>
                <p class="suggestionname">D-6703</br>curved french facing..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6704:flat_french_facing" title="">
                <img src="images/overcoat/D-6704flatfrenchfacing.png"/>
                <p class="suggestionname">D-6704</br>flat french facing</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6705:point_flat_french_facing" title="">
                <img src="images/overcoat/D-6705pointflatfrenchfacing.png"/>
                <p class="suggestionname">D-6705</br>point flat french facing</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6706:fabric_facing_for_1.3_1.4_or_no_lining_options_only" title="fabric facing for 1.3 1.4 or no lining options only">
                <img src="images/overcoat/D-6706fabricfacingfor1314ornoliningoptionsonly.png"/>
                <p class="suggestionname">D-6706</br>fabric facing for..</p>
            </div>
            <div class="primarysuggestion" id="facingstyle_D-6707:point_curved_french_facing" title="fabric facing for 1.3 1.4 or no lining options only">
                <img src="images/overcoat/D-6707pointcurvedfrenchfacing.png"/>
                <p class="suggestionname">D-6707</br>point curved french facing</p>
            </div>
        </div>
        <!--
        ----pocket style div
        --->
        <div class="col-md-12 overflow-cmd stepdiv" id="pocketstyles" style="display:none">
            <p class="stylecaption">Inner pockets style</p>
            <div class="primarysuggestion" id="pocketstyle_D-680A:inside_pockets_d_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_left_lower_card_pocket_drop_shape_pen_pocket_right_pocket_with_space" title="inside pockets d double besom w tab button left pocket double besom w tab button right pocket left lower card pocket drop shapepen pocket right pocket with space">
                <img src="images/overcoat/D-680Ainsidepocketsddoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketleftlowercardpocketdropshapepenpocketrightpocketwithspace.png"/>
                <p class="suggestionname">D-680A</br>inside pockets d..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680B:inside_pockets_e_regular_left_pocket_double_besom_w_triangle_flap_button_right_pocket_right_pocket_with_space_regular_pen_pocket_left_lower_card_pocket_right_cell_pocket" title="inside pockets e regular left pocket double besom w triangle flap button right pocket right pocket with space regular pen pocket left lower card pocket right cell pocket">
                <img src="images/overcoat/D-680Binsidepocketseregularleftpocketdoublebesomwtriangleflapbuttonrightpocketrightpocketwithspaceregularpenpocketleftlowercardpocketrightcellpocket.png"/>
                <p class="suggestionname">D-680B</br>inside pockets e..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680C:inside_pockets_f_double_besom_w_loop_button_left_inside_pocket_double_besomw_loop_button_right_inside_pocket_right_pocket_with_space_right_cell_pocket_regular_pen_pocket_left_lower_card_pocket" title="inside pockets f double besom w loop button left in side pocket double besom w loop button right inside pocket right pocket with space right cell pocket regular pen pocket left lower card pocket">
                <img src="images/overcoat/D-680Cinsidepocketsfdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketrightpocketwithspacerightcellpocketregularpenpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680C</br>inside pockets f..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680D:inside_pockets_g_double_besom_right_inside_pocket_w_triangle_loop_button_and_space_double_besom_w_triangle_loop_button_right_inside_pocket_right_pocket_with_space_right_cell_pocket_regular_pen_pocket_left" title="inside pockets g double besom right inside pocket w triangle loop button and space double besom w triangle loop button right inside pocket right pocket with space right cell pocket regular pen pocket left">
                <img src="images/overcoat/D-680Dinsidepocketsgdoublebesomrightinsidepocketwtriangleloopbuttonandspacedoublebesomwtriangleloopbuttonrightinsidepocketrightpocketwithspacerightcellpocketregularpenpocketleft.png"/>
                <p class="suggestionname">D-680D</br>inside pockets g..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680E:inner_pockets_h_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_right_cell_pocket_regular_pen_pocket_left_lower_card_pocket_right_pocket_with_space" title="inner pockets h double besom w tab button left pocket double besom w tab button right pocket right cell pocket regular pen pocket left lower card pocket right pocket with space">
                <img src="images/overcoat/D-680Einnerpocketshdoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketrightcellpocketregularpenpocketleftlowercardpocketrightpocketwithspace.png"/>
                <p class="suggestionname">D-680E</br>inner pockets h..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680F:inside_pocket_j_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_right_pocket_with_space_right_cell_pocket_drop_shape_pen_pocket_left_lower_card_pocket" title="inside pocket j double besom w tab button left pocket double besom w tab button right pocket right pocket with space right cell pocket drop shape pen pocket left lower card pocket">
                <img src="images/overcoat/D-680Finsidepocketjdoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketrightpocketwithspacerightcellpocketdropshapepenpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680F</br>inside pocket j..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680G:inside_pocket_k_double_besom_w_loop_button_left_inside_pocket_double_besom_w_loop_button_right_inside_pocket_right_pocket_with_space_regular_pen_pocket_left_lower_card_pocket_mp3_pocket" title="insidepocketkdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketrightpocketwithspaceregularpenpocketleftlowercardpocketmp3pocket">
                <img src="images/overcoat/D-680Ginsidepocketkdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketrightpocketwithspaceregularpenpocketleftlowercardpocketmp3pocket.png"/>
                <p class="suggestionname">D-680G</br>inside pocket k..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680H_inside_pocket_l_double_besom_w_triangle_loop_button_left_inside_pocket_double_besom_w_triangle_loop_button_right_inside_pocket_right_pocket_with_space_regular_pen_pocket_left_lower_card_pocket_mp3_pocket" title="D-680Hinsidepocketldoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsidepocketrightpocketwithspaceregularpenpocketleftlowercardpocketmp3pocket">
                <img src="images/overcoat/D-680Hinsidepocketldoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsidepocketrightpocketwithspaceregularpenpocketleftlowercardpocketmp3pocket.png"/>
                <p class="suggestionname">D-680H</br>inside pocket..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680J:inside_pocket_m_double_besom_w_loop_button_left_inside_pocket_double_besom_w_loop_button_right_inside_pocket_right_pocket_with_space_drop_shape_pen_pocket_left_lower_card_pocket_mp3_pocket" title="D-680Jinsidepocketmdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketrightpocketwithspacedropshapepenpocketleftlowercardpocketmp3pocket">
                <img src="images/overcoat/D-680Jinsidepocketmdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketrightpocketwithspacedropshapepenpocketleftlowercardpocketmp3pocket.png"/>
                <p class="suggestionname">D-680J</br>inside pocket m..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680K:inner_pockets_n_double_besom_w_tab_button_left_pocket_double_besomw_tab_button_right_pocket_right_pocket_with_space_left_lower_card_pocket_drop_shape_pen_pocket_mp3_pocket" title="D-680Kinnerpocketsndoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketrightpocketwithspaceleftlowercardpocketdropshapepenpocketmp3pocket">
                <img src="images/overcoat/D-680Kinnerpocketsndoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketrightpocketwithspaceleftlowercardpocketdropshapepenpocketmp3pocket.png"/>
                <p class="suggestionname">D-680K</br>inner pockets n</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680L:inside_pocket_p_regular_left_inside_pocket_double_besom_w_triangle_flap_button_right_pocket_no_space_pocket_regular_pen_pocket_left_lower_card_pocket_right_cell_pocket" title="D-680Linsidepocketpregularleftinsidepocketdoublebesomwtriangleflapbuttonrightpocketnospacepocketregularpenpocketleftlowercardpocketrightcellpocket">
                <img src="images/overcoat/D-680Linsidepocketpregularleftinsidepocketdoublebesomwtriangleflapbuttonrightpocketnospacepocketregularpenpocketleftlowercardpocketrightcellpocket.png"/>
                <p class="suggestionname">D-680L</br>inside pocket p</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680M:inside_pocket_q_double_besom_w_loop_button_left_inside_pocket_double_besom_w_loop_button_right_inside_r_pocket_right_cell_pocket_regular_pen_pocket_left_lower_card_pocket" title="D-680Minsidepocketqdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsiderpocketrightcellpocketregularpenpocketleftlowercardpocket">
                <img src="images/overcoat/D-680Minsidepocketqdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsiderpocketrightcellpocketregularpenpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680M</br>inside pocket q</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680N:inside_pocket_r_double_besom_w_triangle_loop_button_left_inside_pocket_double_besom_w_triangle_loop_button_right_inside_r_pocket_right_cell_pocket_regular_pen_pocket_left_lower_card_pocket" title="D-680Ninsidepocketrdoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsiderpocketrightcellpocketregularpenpocketleftlowercardpocket">
                <img src="images/overcoat/D-680Ninsidepocketrdoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsiderpocketrightcellpocketregularpenpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680N</br>inside pocket r</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680P:inside_pockets_regular_left_inside_pocket_double_besom_w_triangle_flap_button_right_pocket_right_cell_pocket_drop_shape_pen_pocket_left_lower_card_pocket" title="D-680Pinsidepocketsregularleftinsidepocketdoublebesomwtriangleflapbuttonrightpocketrightcellpocketdropshapepenpocketleftlowercardpocket">
                <img src="images/overcoat/D-680Pinsidepocketsregularleftinsidepocketdoublebesomwtriangleflapbuttonrightpocketrightcellpocketdropshapepenpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680P</br>inside pocket s</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680Q:inside_pocket_t_double_besom_w_triangle_loop_button_left_inside_pocket_double_besom_w_triangle_loop_button_right_inside_pocket_drop_shape_pen_pocket_right_cell_pocket_left_lower_card_pocket" title="D-680Qinsidepockettdoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsidepocketdropshapepenpocketrightcellpocketleftlowercardpocket">
                <img src="images/overcoat/D-680Qinsidepockettdoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsidepocketdropshapepenpocketrightcellpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680Q</br>inside pocket t</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680R:inside_pocket_u_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_drop_shape_pen_pocket_right_cell_phone_pocket_left_lower_card_pocket" title="D-680Rinsidepocketudoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketdropshapepenpocketrightcellphonepocketleftlowercardpocket">
                <img src="images/overcoat/D-680Qinsidepockettdoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsidepocketdropshapepenpocketrightcellpocketleftlowercardpocket.png"/>
                <p class="suggestionname">D-680R</br>inside pocket u</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680S:inside_pocket_v_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_left_right_cell_phone_pockets_right_pocket_with_space_left_right_lower_card_pockets" title="">
                <img src="images/overcoat/D-680Sinsidepocketvdoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketleftrightcellphonepocketsrightpocketwithspaceleftrightlowercardpockets.png"/>
                <p class="suggestionname">D-680S</br>inside pocket v</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680T:inside_pocket_w_double_besom_w_loop_button_left_inside_pocket_double_besom_w_loop_button_right_inside_pocket_left_right_cell_phone_pocket_s_right_pocket_with_space_left_right_lower_card_pockets" title="">
                <img src="images/overcoat/D-680Tinsidepocketwdoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketleftrightcellphonepocketsrightpocketwithspaceleftrightlowercardpockets.png"/>
                <p class="suggestionname">D-680T</br>inside pocket w</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-680X:inside_pocket_combination_normal_left_inner_pocket_right_inner_pocket_w_button_trangle_no_space_pocket_right_phone_pocket_mp3_pocket_left_bottom_name_card_pocket_drop_shape_left_pen_pocket" title="">
                <img src="images/overcoat/D-680Xinsidepocketcombinationnormalleftinnerpocketrightinnerpocketwbuttontranglenospacepocketrightphonepocketmp3pocketleftbottomnamecardpocketdropshapeleftpenpocket.png"/>
                <p class="suggestionname">D-680X</br>inside pocket c</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6801:normal_inner_pockets" title="">
                <img src="images/overcoat/D-6801normalinnerpockets.png"/>
                <p class="suggestionname">D-6801</br>normal inner pockets</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6802:mp3_pocket_standard_inner_left_pocket_right_inner_pocket_w_triangle_flap_button_right_pocket_with_space_pen_pocket_left_lower_name_card_pocket_mp3_pocket" title="">
                <img src="images/overcoat/D-6802mp3pocketstandardinnerleftpocketrightinnerpocketwtriangleflapbuttonrightpocketwithspacepenpocketleftlowernamecardpocketmp3pocket.png"/>
                <p class="suggestionname">D-6802</br>mp3 pockets t..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6803:fashion_inner_pocket_regular_left_pocket_double_besom_w_triangle_flap_button_right_pocket_right_pocket_with_space_left_phone_pocket_right_and_left_card_pocket" title="">
                <img src="images/overcoat/D-6803fashioninnerpocketregularleftpocketdoublebesomwtriangleflapbuttonrightpocketrightpocketwithspaceleftphonepocketrightandleftcardpocket.png"/>
                <p class="suggestionname">D-6803</br>fashion inner pocket</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6804:simple_pockets_regular_left_pocket_double_besom_w_triangle_flap_button_right_pocket_right_pocket_with_space" title="">
                <img src="images/overcoat/D-6804simplepocketsregularleftpocketdoublebesomwtriangleflapbuttonrightpocketrightpocketwithspace.png"/>
                <p class="suggestionname">D-6804</br>simple pockets regular</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6805:patch_inner_breast_pocket_left_and_right" title="">
                <img src="images/overcoat/D-6805patchinnerbreastpocketleftandright.png"/>
                <p class="suggestionname">D-6805</br>patch inner breast</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6806:no_inner_pocket" title="">
                <img src="images/overcoat/D-6806noinnerpocket.png"/>
                <p class="suggestionname">D-6806</br>no inner pocket</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6807:inside_pockets_a_double_besom_w_loop_button_left_inside_pocket_double_besom_w_loop_button_right_inside_pocket_regular_pen_pocket_left_lower_card_pocket_right_pocket_with_space" title="">
                <img src="images/overcoat/D-6807insidepocketsadoublebesomwloopbuttonleftinsidepocketdoublebesomwloopbuttonrightinsidepocketregularpenpocketleftlowercardpocketrightpocketwithspace.png"/>
                <p class="suggestionname">D-6807</br>inside pockets a..</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6808:inside_pockets_b_double_besom_w_triangle_loop_button_left_inside_pocket_double_besom_w_triangle_loop_button_right_inside_pocket_regular_pen_pocket_left_lower_card_pocket_right_pocket_with_space" title="">
                <img src="images/overcoat/D-6808insidepocketsbdoublebesomwtriangleloopbuttonleftinsidepocketdoublebesomwtriangleloopbuttonrightinsidepocketregularpenpocketleftlowercardpocketrightpocketwithspace.png"/>
                <p class="suggestionname">D-6808</br>inside pockets b</p>
            </div>
            <div class="primarysuggestion" id="pocketstyle_D-6909:inside_pockets_c_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_regular_pen_pocket_left_lower_card_pocket_right_pocket_with_space" title="">
                <img src="images/overcoat/D-6909insidepocketscdoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketregularpenpocketleftlowercardpocketrightpocketwithspace.png"/>
                <p class="suggestionname">D-6909</br>inside pockets c</p>
            </div>
        </div>
        <!--
        ----Collar tab
        --->
        <div class="col-md-12 overflow-cmd stepdiv" id="collarstyles" style="display:none">
            <p class="stylecaption">Collar Tab Style</p>
            <div class="primarysuggestion" id="collarstyle_D-6881:silk_hanger_loop_above_the_collar" title="silk hanger loop above the collar">
                <img src="images/overcoat/D-6881silkhangerloopabovethecollar.png"/>
                <p class="suggestionname">D-6881</br>silk hanger loop... </p>
            </div>
            <div class="primarysuggestion" id="collarstyle_D-6882:blank_slik_collar_tab_below_the_collar" title="blank slik collar tab below the collar">
                <img src="images/overcoat/D-6882blankslikcollartabbelowthecollar.png"/>
                <p class="suggestionname">D-6882</br>blank slik collar tab... </p>
            </div>
            <div class="primarysuggestion" id="collarstyle_D-6883:marked_collar_tab_on_the_collar" title="marked collar tab on the collar">
                <img src="images/overcoat/D-6883markedcollartabonthecollar.png"/>
                <p class="suggestionname">D-6883</br>marked collar tab... </p>
            </div>
            <div class="primarysuggestion" id="collarstyle_D-6884:marked_collar_tab_below_the_collar" title="marked collar tab on the collar">
                <img src="images/overcoat/D-6884markedcollartabbelowthecollar.png"/>
                <p class="suggestionname">D-6884</br>marked collar tab... </p>
            </div>
            <div class="primarysuggestion" id="collarstyle_D-6885:no_collar_tab" title="marked collar tab on the collar">
                <img src="images/overcoat/D-6885nocollartab.png"/>
                <p class="suggestionname">D-6885</br>no collar tab... </p>
            </div>
            <div class="primarysuggestion" id="collarstyle_D-688C:origin_label_under_collar_tab" title="origin label under collar tab">
                <img src="images/overcoat/D-688Coriginlabelundercollartab.png"/>
                <p class="suggestionname">D-688C</br>origin label under... </p>
            </div>
        </div>
        <!--
        ----liningstyle
        --->
        <div class="col-md-12 stepdiv" id="liningstyles" style="display:none">
            <p class="stylecaption">Lining Style</p>
            <div class="primarysuggestion" id="liningstyle_D-65C1:full_lining_round_bottom" title="">
                <img src="images/overcoat/D-65C1fullliningroundbottom.png"/>
                <p class="suggestionname">D-65C1</br>full linin ground bottom... </p>
            </div>
            <div class="primarysuggestion" id="liningstyle_D-65C5:full_lining_straight_bottom" title="full lining straight bottom">
                <img src="images/overcoat/D-65C5fullliningstraightbottom.png"/>
                <p class="suggestionname">D-65C5</br>full lining straight..</p>
            </div>
            <div class="primarysuggestion" id="liningstyle_D-65M4:1.2_lining_pic_stitch_on_inside_seams_traight_bottom" title="1.2 lining pic stitch on in side seam straight bottom">
                <img src="images/overcoat/D-65M412liningpicstitchoninsideseamstraightbottom.png"/>
                <p class="suggestionname">D-65M4</br>1.2 lining pic stitch..</p>
            </div>
            <div class="primarysuggestion" id="liningstyle_D-65P3:1.2_lining_piping_on_inside_seam_straight_bottom" title="1.2 lining piping on in side seam straight bottom">
                <img src="images/overcoat/D-65P312liningpipingoninsideseamstraightbottom.png"/>
                <p class="suggestionname">D-65P3</br>1.2 lining piping..</p>
            </div>
            <div class="primarysuggestion" id="liningstyle_D-65R9:inside_seam_straight_bottom" title="">
                <img src="images/overcoat/D-65R9insideseamstraightbottom.png"/>
                <p class="suggestionname">D-65R9</br>inside seam straight bottom</p>
            </div>
            <div class="primarysuggestion" id="liningstyle_D-65T2:1.3_lining_pic_stitch_on_inside_seam_straight_bottom" title="1.3 lining pic stitch on inside seam straight bottom">
                <img src="images/overcoat/D-65T213liningpicstitchoninsideseamstraightbottom.png"/>
                <p class="suggestionname">D-65T2</br>1.3 lining pic..</p>
            </div>
            <div class="primarysuggestion" id="liningstyle_D-65U8:1.4_lining_piping_on_inside_seams_traight_bottom" title="1.4 lining piping on in sideseam straight bottom">
                <img src="images/overcoat/D-65U814liningpipingoninsideseamstraightbottom.png"/>
                <p class="suggestionname">D-65U8</br>1.4 lining piping..</p>
            </div>
        </div>
        <!--
        ----liningoption
        --->
        <div class="col-md-12 stepdiv" id="liningoptions" style="display:none">
            <p class="stylecaption">Lining Option</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="liningoptiondiv">
                <?php
                $query = "select * from vest_option where type='linning_option' and status = '1'";
                $result = mysql_query($query);
                if($result){
                    $num = mysql_num_rows($result);
                    if($num>0){
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result)){
                            $option_name = $rows['option_name'];
                            ?>
                            <div class="primarysuggestion" style="height:100px;width:18%"
                                 id="liningoption_<?php echo $option_name ?>" onclick=setSurplusPrice('<?php echo rawurlencode($option_name); ?>','LiningOption')>
                                <img src="images/vest/<?php echo $option_name ?>.jpg" style="min-width:100%;max-width:100%"/>
                                <p class="suggestionname"><?php echo $option_name ?></p>
                            </div>
                            <?php
                        }
                    }
                    else{
                        echo "No Color Found";
                    }
                }else{
                    echo mysql_error();
                }
                ?>
            </div>
            <div class="col-md-12" style="padding: 0">
                <div class="form-group col-md-6" style="margin-top:20px;padding:0">
                    <label>Fabric Name</label>
                    <input type="text" class="form-control" value="" id="liningoptionfield" placeholder="Enter Fabric Name" onkeyup="searchFabricData(this,'LiningOption')"/>
                    <ul class="list-unstyled data_ data-remove" id="list_data" style="background: #FFFFFF">
                    </ul>
                </div>
                <div class="form-group col-md-6" style="margin-top:20px">
                    <input type="button" class="btn btn-danger pull-right FabricBtnLiningOption"  onclick="fnlinningoption()"
                           style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric" disabled />
                </div>
                <div class="row">
                    <label class="surplus_price_label" id="surplus_price_labelLiningOption" style="display: none">
                        Surplus Price :
                        <span class="surplus_price" id="surplus_priceLiningOption"></span>
                    </label>
                </div>
            </div>
        </div>
        <!--
        ----thread Color
        --->
        <div class="col-md-12 overflow-cmd" id="threadcolors" style="display:none">
            <p class="stylecaption">Thread Color</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0">
                <?php
                $query = "select * from vest_option where type='thread_option' and status = '1'";
                $result = mysql_query($query);
                if($result){
                    $num = mysql_num_rows($result);
                    if($num>0){
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result)){
                            $option_name = $rows['option_name'];
                            ?>
                            <div class="primarysuggestion colorsuggestion"
                                 id="threadcolor_<?php echo $option_name ?>">
                                <img src="images/vest/<?php echo $option_name ?>.png"/>
                                <p class="suggestionname"><?php echo $option_name ?></p>
                            </div>
                            <?php
                        }
                    }
                    else{
                        echo "No Color Found";
                    }
                }else{
                    echo mysql_error();
                }
                ?>
            </div>
        </div>
        <!--
        ----overcoat Fabric
        --->
        <div class="col-md-12 overflow-cmd" id="overcoatfabrics" style="display:none">
            <p class="stylecaption">Overcoat Fabric</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding:0" id="mainfabricdiv">
                <?php
                $query = "select * from vest_option where type='overcoat_fabric' and status = '1'";
                $result = mysql_query($query);
                if($result){
                    $num = mysql_num_rows($result);
                    if($num>0){
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result)){
                            $option_name = $rows['option_name'];
                            ?>
                            <div class="primarysuggestion colorsuggestion"
                                 id="overcoatfabric_<?php echo $option_name ?>" onclick=setSurplusPrice('<?php echo rawurlencode($option_name); ?>','OvercoatFabric')>
                                <img src="images/overcoat/<?php echo $option_name ?>.png"/>
                                <p class="suggestionname"><?php echo $option_name ?></p>
                            </div>
                            <?php
                        }
                    }
                    else{
                        echo "No Color Found";
                    }
                }else{
                    echo mysql_error();
                }
                ?>
            </div>
            <div class="col-md-12" style="padding: 0">
                <div class="form-group col-md-6" style="margin-top:20px;padding:0">
                    <label>Fabric Name</label>
                    <input type="text" class="form-control" value="" id="mainfabricfield" placeholder="Enter Fabric Name" onkeyup="searchFabricData(this,'OvercoatFabric');"/>
                    <ul class="list-unstyled data_ data-remove" id="list_data1" style="background:#fff;"></ul>
                </div>
                <div class="form-group col-md-6" style="margin-top:20px">
                    <input type="button" class="btn btn-danger pull-right FabricBtnOvercoatFabric"  onclick="fnmainfabric()"
                           style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric1" disabled/>
                </div>
                <div class="row">
                    <label class="surplus_price_label" id="surplus_price_labelOvercoatFabric" style="display: none">
                        Surplus Price :
                        <span class="surplus_price" id="surplus_priceOvercoatFabric"></span>
                    </label>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="text-align:center;margin-top: 100px">
         <div class="col-md-6"></div><label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
        <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
    </div>
    <div id ="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">.col-md-4</div>
                        <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <script>
        function setSurplusPrice(colorName,fabricId){
            colorName = decodeURI(colorName);
            $("#surplus_price_label"+fabricId).show();
            var url = "admin/webserver/getSurplusPrice.php";
            $.post(url, {"type":"Custom Suit","colorName":colorName},function(data){
                var data = JSON.parse(data);
                $("#surplus_price"+fabricId).html("$"+data.extra_price);
            });
        }

    $(".nav a").removeClass("active");
    var currPage = "category";
    var category = "formal";
    var canvasstyle = "D-000A:full_canvas";
    var canvasstyle_img = "fullcanvas";
    var lapelstyle = "D-6001:notch";
    var lapelstyle_img = "notch";
    var pocketstype = "D-6201:normal_pockets";
    var pocketstype_img = "normalpockets";
    var buttonstyle = "D-6012:single_breasted_two_buttons";
    var buttonstyle_img = "singlebreastedtwobuttons";
    var sweatpadstyle = "D-6742:small_triangle_shape_lining_sweat_pad";
    var sweatpadstyle_img = "smalltriangleshapeliningsweatpad";
    var elbowpadstyle = "D-6609:round_elbow_pad";
    var elbowpadstyle_img = "roundelbowpad";
    var shoulderstyle = "D-660N:neapolitan_shoulder";
    var shoulderstyle_img = "neapolitanshoulder";
    var shoulderpatchstyle = "D-66Z1:left_shoulder_arch_patch";
    var shoulderpatchstyle_img = "leftshoulderarchpatch";
    var shouldertabstyle = "D-660B:point_epaulet_with_button_and_round_button_hole";
    var shouldertabstyle_img = "pointepauletwithbuttonandroundbuttonhole";
    var sleeveslitstyle = "D-66B6:cuff_with_trapezoid_squared_tab_and_buttons_button_holes";
    var sleeveslitstyle_img = "cuffwithtrapezoidsquaredtabandbuttonsbuttonholes";
    var sleevebuttonstyle = "D-6621:four_flat_sleeve_button";
    var sleevebuttonstyle_img = "fourflatsleevebutton";
    var backvent = "D-64N0:no_vent";
    var backvent_img = "novent";
    var stitchstyle = "D-657D:0.3*0.8cm_double_pick_stitch_on_all_edges";
    var stitchstyle_img = "0308cmdoublepickstitchonalledges";
    var backsideofarmhole = "D-65Q2:wide_distance_0.3cm_narrow_pick_stitch_on_for_e_part_of_the_arm_hole";
    var backsideofarmhole_img = "widedistance03cmnarrowpickstitchonforepartofthearmhole";
    var topstichstyle = "C-6583:0.8cm_top_stitch_on_shoulder_seam";
    var topstichstyle_img = "08cmtopstitchonshoulderseam";
    var breastpocket = "D-6101:normal_breast_pocket";
    var breastpocket_img = "normalbreastpocket";
    var coinpocket = "D-6320:no_exterior_lower_coin_pocket";
    var coinpocket_img = "noexteriorlowercoin_pocket";
    var facingstyle = "D-6130:straight_inner_facing_fly_front";
    var facingstyle_img = "straightinnerfacingflyfront";
    var pocketstyle = "D-680A:inside_pockets_d_double_besom_w_tab_button_left_pocket_double_besom_w_tab_button_right_pocket_left_lower_card_pocket_drop_shape_pen_pocket_right_pocket_with_space";
    var pocketstyle_img = "insidepocketsddoublebesomwtabbuttonleftpocketdoublebesomwtabbuttonrightpocketleftlowercardpocketdropshapepenpocketrightpocketwithspace";
    var collarstyle = "D-6881:silk_hanger_loop_above_the_collar";
    var collarstyle_img = "silkhangerloopabovethecollar";
    var liningstyle = "D-65C1:full_lining_round_bottom";
    var liningstyle_img = "fullliningroundbottom";
    var liningoption = "FLL148";
    var liningoption_img = "FLL148";
    var threadcolor = "C3166";
    var overcoatfabric = "dbp951a";

    function getPrevValue(prevIndex){
        var result = "";
        var result_img = "";
        switch(prevIndex){
            case "category":
                result=category;
                result_img=category;
                break;
            case "canvasstyle":
                result = canvasstyle;
                result_img = canvasstyle_img;
                break;
            case "lapelstyle":
                result = lapelstyle;
                result_img = lapelstyle_img;
                break;
            case "pocketstype":
                result = pocketstype;
                result_img = pocketstype_img;
                break;
            case "buttonstyle":
                result = buttonstyle;
                result_img = buttonstyle_img;
                break;
            case "sweatpadstyle":
                result = sweatpadstyle;
                result_img = sweatpadstyle_img;
                break;
            case "elbowpadstyle":
                result = elbowpadstyle;
                result_img = elbowpadstyle_img;
                break;
            case "shoulderstyle":
                result = shoulderstyle;
                result_img = shoulderstyle_img;
                break;
            case "shoulderpatchstyle":
                result = shoulderpatchstyle;
                result_img = shoulderpatchstyle_img;
                break;
            case "shouldertabstyle":
                result = shouldertabstyle;
                result_img = shouldertabstyle_img;
                break;
            case "sleeveslitstyle":
                result = sleeveslitstyle;
                result_img = sleeveslitstyle_img;
                break;
            case "sleevebuttonstyle":
                result = sleevebuttonstyle;
                result_img = sleevebuttonstyle_img;
                break;
            case "backvent":
                result = backvent;
                result_img = backvent_img;
                break;
            case "stitchstyle":
                result = stitchstyle;
                result_img = stitchstyle_img;
                break;
            case "backsideofarmhole":
                result = backsideofarmhole;
                result_img = backsideofarmhole_img;
                break;
            case "topstichstyle":
                result = topstichstyle;
                result_img = topstichstyle_img;
                break;
            case "breastpocket":
                result = breastpocket;
                result_img = breastpocket_img;
                break;
            case "coinpocket":
                result = coinpocket;
                result_img = coinpocket_img;
                break;
            case "facingstyle":
                result = facingstyle;
                result_img = facingstyle_img;
                break;
            case "pocketstyle":
                result = pocketstyle;
                result_img = pocketstyle_img;
                break;
            case "collarstyle":
                result = collarstyle;
                result_img = collarstyle_img;
                break;
            case "liningstyle":
                result = liningstyle;
                result_img = liningstyle_img;
                break;
            case "liningoption":
                result = liningoption;
                result_img = liningoption_img;
                break;
            case "threadcolor":
                result = threadcolor;
                result_img = threadcolor;
                break;
            case "overcoatfabric":
                result = overcoatfabric;
                result_img = overcoatfabric;
                break;
        }
        return result+"@"+result_img;
    }
    function loadSessionData() {
        $(".surplus_price_label").hide();
        $(".loader").fadeIn("slow");
        var prevIndex = $(".stepsactive").attr("id");
        currPage = prevIndex + "s";
        var item = getPrevValue(prevIndex);
        var item_img = item.split("@")[1];
        item = item.split("@")[0];
        item = prevIndex + "_" + item;
//        alert(item);
        $(".primarysuggestion").removeClass("active");
        var idd = document.getElementById(item);
        $(idd).addClass("active");
        if(currPage == "sleevebuttonstyles" || currPage == "sleeveslitstyles") {
            folder = "side";
            $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/overcoat.png");
            $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#sleevebuttonimg").attr("src","images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+sleevebuttonstyle_img+".png");
            $(".loader").fadeOut("slow");
        }else if(currPage == "backvents" || currPage == "elbowpadstyles") {
            folder = "back";
            $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+backvent_img+".png");
            $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#sleevebuttonimg").attr("src","images/overcoat/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else{
            folder = "front";
            $("#shirtimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/shirt.png");
            $("#tieimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/tie.png");
            $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+buttonstyle_img+"overcoat.png");
            $("#lapelimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+"_"+lapelstyle_img+".png");
            $("#frontbuttonimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+".png");
            $("#breastpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+breastpocket_img+".png");
            $("#lowerpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+pocketstype_img+".png");
            $("#sleevebuttonimg").attr("src", "images/overcoat/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
    }
    $(".primarysuggestion").click(function () {
        $(".loader").fadeIn("slow");
        $(".primarysuggestion").removeClass("active");
        var value = this.id;
        $(this).addClass("active");
        var optionKeyArray = value.split("_");
        var optionKey = optionKeyArray[0];
        var cartselection = optionKeyArray[1];
        var cartselection_img = optionKeyArray[1];
        var currpage = optionKey + "s";
        if(value.indexOf(":")>=0) {
            var temp = value.split(":");
            optionKeyArray = temp[0].split("_");
            optionKey = optionKeyArray[0];
            cartselection = optionKeyArray[1] + ":" + temp[1];
            cartselection_img = temp[1].replace(/_/g, "");
            currpage = optionKey + "s";
        }
        switch (optionKey) {
            case "category":
                category = cartselection;
                category = cartselection_img;
                break;
            case "canvasstyle":
                canvasstyle = cartselection;
                canvasstyle_img = cartselection_img;
                break;
            case "lapelstyle":
                lapelstyle = cartselection;
                lapelstyle_img = cartselection_img;
                break;
            case "pocketstype":
                pocketstype = cartselection;
                pocketstype_img = cartselection_img;
                break;
            case "buttonstyle":
                buttonstyle = cartselection;
                buttonstyle_img = cartselection_img;
                break;
            case "sweatpadstyle":
                sweatpadstyle = cartselection;
                sweatpadstyle_img = cartselection_img;
                break;
            case "elbowpadstyle":
                elbowpadstyle = cartselection;
                elbowpadstyle_img = cartselection_img;
                break;
            case "shoulderstyle":
                shoulderstyle = cartselection;
                shoulderstyle_img = cartselection_img;
                break;
            case "shoulderpatchstyle":
                shoulderpatchstyle = cartselection;
                shoulderpatchstyle_img = cartselection_img;
                break;
            case "shouldertabstyle":
                shouldertabstyle = cartselection;
                shouldertabstyle_img = cartselection_img;
                break;
            case "sleeveslitstyle":
                sleeveslitstyle = cartselection;
                sleeveslitstyle_img = cartselection_img;
                break;
            case "sleevebuttonstyle":
                sleevebuttonstyle = cartselection;
                sleevebuttonstyle_img = cartselection_img;
                break;
            case "backvent":
                backvent = cartselection;
                backvent_img = cartselection_img;
                break;
            case "stitchstyle":
                stitchstyle = cartselection;
                stitchstyle_img = cartselection_img;
                break;
            case "backsideofarmhole":
                backsideofarmhole = cartselection;
                backsideofarmhole_img = cartselection_img;
                break;
            case "topstichstyle":
                topstichstyle = cartselection;
                topstichstyle_img = cartselection_img;
                break;
            case "breastpocket":
                breastpocket = cartselection;
                breastpocket_img = cartselection_img;
                break;
            case "coinpocket":
                coinpocket = cartselection;
                coinpocket_img = cartselection_img;
                break;
            case "facingstyle":
                facingstyle = cartselection;
                facingstyle_img = cartselection_img;
                break;
            case "pocketstyle":
                pocketstyle = cartselection;
                pocketstyle_img = cartselection_img;
                break;
            case "collarstyle":
                collarstyle = cartselection;
                collarstyle_img = cartselection_img;
                break;
            case "liningstyle":
                liningstyle = cartselection;
                liningstyle_img = cartselection_img;
                break;
            case "liningoption":

                liningoption = cartselection;
                liningoption_img = cartselection_img;
                break;
            case "threadcolor":
                threadcolor = cartselection;
                threadcolor = cartselection_img;
                break;
            case "overcoatfabric":
                overcoatfabric = cartselection;
                overcoatfabric = cartselection_img;
                break;
        }
        if(currPage == "sleevebuttonstyles" || currPage == "sleeveslitstyles") {
            folder = "side";
            $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/overcoat.png");
            $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#sleevebuttonimg").attr("src","images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+sleevebuttonstyle_img+".png");
            $(".loader").fadeOut("slow");
        }else if(currPage == "backvents" || currPage == "elbowpadstyles") {
            folder = "back";
            $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
            $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+backvent_img+".png");
            $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
            $("#sleevebuttonimg").attr("src","images/overcoat/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else{
            folder = "front";
            $("#shirtimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/shirt.png");
            $("#tieimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/tie.png");
            $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+buttonstyle_img+"overcoat.png");
            $("#lapelimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+"_"+lapelstyle_img+".png");
            $("#frontbuttonimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+".png");
            $("#breastpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+breastpocket_img+".png");
            $("#lowerpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+pocketstype_img+".png");
            $("#sleevebuttonimg").attr("src", "images/overcoat/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }


        /*
         * data store all values
         * */

        var url = 'admin/webserver/selectionData.php';
        $.post(url,{currPage:currPage,overcoatcategory:category,canvasstyle:canvasstyle,canvasstyle_img:canvasstyle_img,lapelstyle:lapelstyle,lapelstyle_img:lapelstyle_img,
            pocketstype:pocketstype,pocketstype_img:pocketstype_img,buttonstyle:buttonstyle,buttonstyle_img:buttonstyle_img,sweatpadstyle:sweatpadstyle,sweatpadstyle_img:sweatpadstyle,
            elbowpadstyle:elbowpadstyle,elbowpadstyle_img:elbowpadstyle_img,shoulderstyle:shoulderstyle,shoulderstyle_img:shoulderstyle,shoulderpatchstyle:shoulderpatchstyle,shoulderpatchstyle_img:shoulderpatchstyle_img,
            shouldertabstyle:shouldertabstyle,shouldertabstyle_img:shouldertabstyle_img,sleeveslitstyle:sleeveslitstyle,sleeveslitstyle_img:sleeveslitstyle_img,sleevebuttonstyle:sleevebuttonstyle,sleevebuttonstyle_img:sleevebuttonstyle_img,
            backvent:backvent,backvent_img:backvent_img,stitchstyle:stitchstyle,stitchstyle_img:stitchstyle_img,backsideofarmhole:backsideofarmhole,backsideofarmhole_img:backsideofarmhole_img,topstichstyle:topstichstyle,topstichstyle_img:topstichstyle_img,
            breastpocket:breastpocket,breastpocket_img:breastpocket_img,coinpocket:coinpocket,coinpocket_img:coinpocket_img,facingstyle:facingstyle,facingstyle_img:facingstyle_img,
            pocketstyle:pocketstyle,pocketstyle_img:pocketstyle_img,collarstyle:collarstyle,collarstyle_img:collarstyle_img,liningstyle:liningstyle,liningstyle_img:liningstyle_img,
            liningoption:liningoption,liningoption_img:liningoption_img,threadcolor:threadcolor,overcoatfabric:overcoatfabric


        },function(data){
            console.log('data --- '+data);
        });

    });
    function navigation(switcher) {
        $("#" + currPage).fadeOut("slow");
        if (switcher == "next") {
            if (currPage == "categorys") {
                $(".steps").removeClass("stepsactive");
                $("#canvasstyles").fadeIn("slow");
                $("#canvasstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "canvasstyles") {
                $(".steps").removeClass("stepsactive");
                $("#lapelstyles").fadeIn("slow");
                $("#lapelstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "lapelstyles") {
                $(".steps").removeClass("stepsactive");
                $("#pocketstypes").fadeIn("slow");
                $("#pocketstype").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pocketstypes") {
                $(".steps").removeClass("stepsactive");
                $("#buttonstyles").fadeIn("slow");
                $("#buttonstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttonstyles") {
                $(".steps").removeClass("stepsactive");
                $("#sweatpadstyles").fadeIn("slow");
                $("#sweatpadstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "sweatpadstyles") {
                $(".steps").removeClass("stepsactive");
                $("#elbowpadstyles").fadeIn("slow");
                $("#elbowpadstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "elbowpadstyles") {
                $(".steps").removeClass("stepsactive");
                $("#shoulderstyles").fadeIn("slow");
                $("#shoulderstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "shoulderstyles") {
                $(".steps").removeClass("stepsactive");
                $("#sleeveslitstyles").fadeIn("slow");
                $("#sleeveslitstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "sleeveslitstyles") {
                $(".steps").removeClass("stepsactive");
                $("#backvents").fadeIn("slow");
                $("#backvent").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "backvents") {
                $(".steps").removeClass("stepsactive");
                $("#stitchstyles").fadeIn("slow");
                $("#stitchstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "stitchstyles") {
                $(".steps").removeClass("stepsactive");
                $("#breastpockets").fadeIn("slow");
                $("#breastpocket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "breastpockets") {
                $(".steps").removeClass("stepsactive");
                $("#coinpockets").fadeIn("slow");
                $("#coinpocket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "coinpockets") {
                $(".steps").removeClass("stepsactive");
                $("#facingstyles").fadeIn("slow");
                $("#facingstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "facingstyles") {
                $(".steps").removeClass("stepsactive");
                $("#pocketstyles").fadeIn("slow");
                $("#pocketstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pocketstyles") {
                $(".steps").removeClass("stepsactive");
                $("#collarstyles").fadeIn("slow");
                $("#collarstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "collarstyles") {
                $(".steps").removeClass("stepsactive");
                $("#liningstyles").fadeIn("slow");
                $("#liningstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "liningstyles") {
                $(".steps").removeClass("stepsactive");
                $("#liningoptions").fadeIn("slow");
                $("#liningoption").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "liningoptions") {
                $(".surplus_price_label").hide();
                $(".steps").removeClass("stepsactive");
                $("#threadcolors").fadeIn("slow");
                $("#threadcolor").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "threadcolors") {
                $(".steps").removeClass("stepsactive");
                $("#overcoatfabrics").fadeIn("slow");
                $("#overcoatfabric").addClass("stepsactive");
                $("#next").html("Add to Cart");
                loadSessionData();
            }
            else if (currPage == "overcoatfabrics") {
                $(".surplus_price_label").hide();
                var fabricCode = overcoatfabric;
                if(fabricCode.indexOf(":") >0 ){
                    fabricCode = fabricCode.split(":")[0];
                }
                var url = "api/registerUser.php";
                $.post(url,{"type":"checkFabricStatus","fabricCode":fabricCode},function(fabricdata) {
                    var status = fabricdata.Status;
                    var message = fabricdata.Message;
                    if (status == "Success") {
                        var user_id = $("#user_id").val();
                        var randomnum = Math.floor((Math.random() * 600000) + 1);
                        var product_name = "CustomOvercoat" + randomnum;
                        var url = "admin/webserver/addto_cart.php?product_type=Custom Overcoat&product_name=" + product_name +
                            "&category="+category+"&canvasstyle="+canvasstyle+"&lapelstyle="+lapelstyle+"&pocketstype="+pocketstype+
                            "&buttonstyle="+buttonstyle+"&sweatpadstyle="+sweatpadstyle+"&elbowpadstyle="+elbowpadstyle+
                            "&shoulderstyle="+shoulderstyle+"&shoulderpatchstyle="+shoulderpatchstyle+"&shouldertabstyle="+
                            shouldertabstyle+"&sleeveslitstyle="+sleeveslitstyle+"&sleevebuttonstyle="+sleevebuttonstyle+"&backvent="+
                            backvent+"&stitchstyle="+stitchstyle+"&backsideofarmhole="+backsideofarmhole+"&topstichstyle="+
                            topstichstyle+"&breastpocket="+breastpocket+"&coinpocket="+coinpocket+"&facingstyle="+facingstyle+
                            "&pocketstyle="+pocketstyle+"&collarstyle="+collarstyle+"&liningstyle="+liningstyle+"&liningoption="+
                            liningoption+"&threadcolor="+threadcolor+"&overcoatfabric="+overcoatfabric+"&user_id=" + user_id +
                            "&quantity=1";
                        $.get(url, function (data) {
                            var json = $.parseJSON(data);
                            var status = json.status;
                            if (status == "done") {
                                window.location = "cart.php";
                            }
                        });
                    }
                    else{
                        showMessages(message,"red");
                        $("#overcoatfabrics").fadeIn("slow");
                        $(".colorsuggestion ").removeClass("active");
                        $("#overcoatfabric_dbn839a").addClass("active");
                        $("#overcoatfabric_dbn839a").trigger("click");
                        overcoatfabric ='dbn839a';
                    }
                });
            }
        }
        else if (switcher == "previous") {
            if (currPage == "categorys") {
                window.location = "customize.php";
            }
            else if (currPage == "canvasstyles") {
                $(".steps").removeClass("stepsactive");
                $("#categorys").fadeIn("slow");
                $("#category").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "lapelstyles") {
                $(".steps").removeClass("stepsactive");
                $("#canvasstyles").fadeIn("slow");
                $("#canvasstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pocketstypes") {
                $(".steps").removeClass("stepsactive");
                $("#lapelstyles").fadeIn("slow");
                $("#lapelstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttonstyles") {
                $(".steps").removeClass("stepsactive");
                $("#pocketstypes").fadeIn("slow");
                $("#pocketstype").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "sweatpadstyles") {
                $(".steps").removeClass("stepsactive");
                $("#buttonstyles").fadeIn("slow");
                $("#buttonstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "elbowpadstyles") {
                $(".steps").removeClass("stepsactive");
                $("#sweatpadstyles").fadeIn("slow");
                $("#sweatpadstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "shoulderstyles") {
                $(".steps").removeClass("stepsactive");
                $("#elbowpadstyles").fadeIn("slow");
                $("#elbowpadstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "sleeveslitstyles") {
                $(".steps").removeClass("stepsactive");
                $("#shoulderstyles").fadeIn("slow");
                $("#shoulderstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "backvents") {
                $(".steps").removeClass("stepsactive");
                $("#sleeveslitstyles").fadeIn("slow");
                $("#sleeveslitstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "stitchstyles") {
                $(".steps").removeClass("stepsactive");
                $("#backvents").fadeIn("slow");
                $("#backvent").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "breastpockets") {
                $(".steps").removeClass("stepsactive");
                $("#stitchstyles").fadeIn("slow");
                $("#stitchstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "coinpockets") {
                $(".steps").removeClass("stepsactive");
                $("#breastpockets").fadeIn("slow");
                $("#breastpocket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "facingstyles") {
                $(".steps").removeClass("stepsactive");
                $("#coinpockets").fadeIn("slow");
                $("#coinpocket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pocketstyles") {
                $(".steps").removeClass("stepsactive");
                $("#facingstyles").fadeIn("slow");
                $("#facingstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "collarstyles") {
                $(".steps").removeClass("stepsactive");
                $("#pocketstyles").fadeIn("slow");
                $("#pocketstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "liningstyles") {
                $(".steps").removeClass("stepsactive");
                $("#collarstyles").fadeIn("slow");
                $("#collarstyle").addClass("stepsactive");
                loadSessionData();
            }

            else if (currPage == "threadcolors") {
                $(".steps").removeClass("stepsactive");
                $("#liningstyles").fadeIn("slow");
                $("#liningstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "overcoatfabrics") {
                $(".steps").removeClass("stepsactive");
                $("#threadcolors").fadeIn("slow");
                $("#threadcolor").addClass("stepsactive");
                $("#next").html("Next");
                loadSessionData();
            }
        }

        storeData();
    }
        function showMessages(message, color) {
            $(".modal-header").css({"display": "none"});
            $(".modal-body").css({"display": "block"});
            $(".modal-body").css({"padding-top": "0px"});
            $(".modal-body").html("<div class='row' style='background:#000;'><h4 style='color:#fff;text-align: center" +
                ";padding:10px;font-size: 18px;' >" +
                "Fabric out of stock !!!</h4></div><div class='row'><h4 style='text-align: center;font-size: 18px;margin: 14px 0;'>"
                +message+"</h4></div>" +
                "<div class='row' style='text-align: center'><input type='button' data-dismiss='modal' " +
                "style='padding:6px;margin:6px 0;text-align: center;font-size: 16px;width: 200px;text-transform: uppercase;" +
                "background: #D9534F;border: none;color:#fff;" +
                "' value='ok'" +
                " /></div> ");
            $(".modal-footer").css({"display": "none"});
            $("#modal").modal("show");
        }
    function storeData() {

        var value = $(".stepsactive").attr("id");
        var optionKeyArray = value.split("_");
        var optionKey = optionKeyArray[0];
        var cartselection = optionKeyArray[1];
        var cartselection_img = optionKeyArray[1];
        var currpage = optionKey + "s";
        if(value.indexOf(":")>=0) {
            var temp = value.split(":");
            optionKeyArray = temp[0].split("_");
            optionKey = optionKeyArray[0];
            cartselection = optionKeyArray[1] + ":" + temp[1];
            cartselection_img = temp[1].replace(/_/g, "");
            currpage = optionKey + "s";
        }
        switch (optionKey) {
            case "category":
                category = cartselection;
                category = cartselection_img;
                break;
            case "canvasstyle":
                canvasstyle = cartselection;
                canvasstyle_img = cartselection_img;
                break;
            case "lapelstyle":
                lapelstyle = cartselection;
                lapelstyle_img = cartselection_img;
                break;
            case "pocketstype":
                pocketstype = cartselection;
                pocketstype_img = cartselection_img;
                break;
            case "buttonstyle":
                buttonstyle = cartselection;
                buttonstyle_img = cartselection_img;
                break;
            case "sweatpadstyle":
                sweatpadstyle = cartselection;
                sweatpadstyle_img = cartselection_img;
                break;
            case "elbowpadstyle":
                elbowpadstyle = cartselection;
                elbowpadstyle_img = cartselection_img;
                break;
            case "shoulderstyle":
                shoulderstyle = cartselection;
                shoulderstyle_img = cartselection_img;
                break;
            case "shoulderpatchstyle":
                shoulderpatchstyle = cartselection;
                shoulderpatchstyle_img = cartselection_img;
                break;
            case "shouldertabstyle":
                shouldertabstyle = cartselection;
                shouldertabstyle_img = cartselection_img;
                break;
            case "sleeveslitstyle":
                sleeveslitstyle = cartselection;
                sleeveslitstyle_img = cartselection_img;
                break;
            case "sleevebuttonstyle":
                sleevebuttonstyle = cartselection;
                sleevebuttonstyle_img = cartselection_img;
                break;
            case "backvent":
                backvent = cartselection;
                backvent_img = cartselection_img;
                break;
            case "stitchstyle":
                stitchstyle = cartselection;
                stitchstyle_img = cartselection_img;
                break;
            case "backsideofarmhole":
                backsideofarmhole = cartselection;
                backsideofarmhole_img = cartselection_img;
                break;
            case "topstichstyle":
                topstichstyle = cartselection;
                topstichstyle_img = cartselection_img;
                break;
            case "breastpocket":
                breastpocket = cartselection;
                breastpocket_img = cartselection_img;
                break;
            case "coinpocket":
                coinpocket = cartselection;
                coinpocket_img = cartselection_img;
                break;
            case "facingstyle":
                facingstyle = cartselection;
                facingstyle_img = cartselection_img;
                break;
            case "pocketstyle":
                pocketstyle = cartselection;
                pocketstyle_img = cartselection_img;
                break;
            case "collarstyle":
                collarstyle = cartselection;
                collarstyle_img = cartselection_img;
                break;
            case "liningstyle":
                liningstyle = cartselection;
                liningstyle_img = cartselection_img;
                break;
            case "liningoption":

                liningoption = cartselection;
                liningoption_img = cartselection_img;
                break;
            case "threadcolor":
                threadcolor = cartselection;
                threadcolor = cartselection_img;
                break;
            case "overcoatfabric":
                overcoatfabric = cartselection;
                overcoatfabric = cartselection_img;
                break;
        }
//        alert(optionKey);
        savetoSession('tab',optionKey);
    }
    function rotateImage(button) {
        var folder = "front";
        $(".loader").fadeIn("slow");
        var source=[];
        source.push($("#shirtimg").attr("src"));
        source.push($("#tieimg").attr("src"));
        source.push($("#overcoatimg").attr("src"));
        source.push($("#lapelimg").attr("src"));
        source.push($("#frontbuttonimg").attr("src"));
        source.push($("#breastpocketimg").attr("src"));
        source.push($("#lowerpocketimg").attr("src"));
        for(var i = 0;i<source.length;i++) {
            if (source[i].indexOf("front/") >=0){
                if (button == "left") {
                    folder = "back";
                    $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+backvent_img+".png");
                    $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#sleevebuttonimg").attr("src","images/overcoat/sugg/bawli.png");
                }
                else if (button == "right") {
                    folder = "side";
                    $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/overcoat.png");
                    $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#sleevebuttonimg").attr("src","images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+sleevebuttonstyle_img+".png");
                }
            }
            else if (source[i].indexOf("side/") >= 0) {
                if (button == "left") {
                    folder = "front";
                    $("#shirtimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/shirt.png");
                    $("#tieimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/tie.png");
                    $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+buttonstyle_img+"overcoat.png");
                    $("#lapelimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+"_"+lapelstyle_img+".png");
                    $("#frontbuttonimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+".png");
                    $("#breastpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+breastpocket_img+".png");
                    $("#lowerpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+pocketstype_img+".png");
                    $("#sleevebuttonimg").attr("src", "images/overcoat/sugg/bawli.png");
                }
                else if (button == "right") {
                    folder = "back";
                    $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+backvent_img+".png");
                    $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#sleevebuttonimg").attr("src","images/overcoat/sugg/bawli.png");
                }
            }
            else if (source[i].indexOf("back/") >= 0) {
                if (button == "left") {
                    folder = "side";
                    $("#shirtimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#tieimg").attr("src", "images/overcoat/sugg/bawli.png");
                    $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/overcoat.png");
                    $("#lapelimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#frontbuttonimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#breastpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#lowerpocketimg").attr("src","images/overcoat/sugg/bawli.png");
                    $("#sleevebuttonimg").attr("src","images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+sleevebuttonstyle_img+".png");
                }
                else if (button == "right") {
                    folder = "front";
                    $("#shirtimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/shirt.png");
                    $("#tieimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/tie.png");
                    $("#overcoatimg").attr("src", "images/overcoat/sugg/"+overcoatfabric+"/"+folder+"/"+buttonstyle_img+"overcoat.png");
                    $("#lapelimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+"_"+lapelstyle_img+".png");
                    $("#frontbuttonimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+buttonstyle_img+".png");
                    $("#breastpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+breastpocket_img+".png");
                    $("#lowerpocketimg").attr("src", "images/overcoat/sugg/" + overcoatfabric + "/" + folder + "/"+pocketstype_img+".png");
                    $("#sleevebuttonimg").attr("src", "images/overcoat/sugg/bawli.png");
                }
            }
        }
        $(".loader").fadeOut("slow");
    }
    $(".steps").click(function(){
        savetoSession('tab',this.id);
        $("#"+currPage).hide(1000);
        var newcurrpage = this.id+"s";
        $(".steps").removeClass("stepsactive");
        $("#"+this.id).addClass("stepsactive");
        $("#"+newcurrpage).show(1000);
        currPage = newcurrpage;
        loadSessionData();
        if(currPage == "overcoatfabrics"){
            $("#next").html("Add to Cart");
        }
        else{
            $("#next").html("Next");
        }
    });
    var shirtimg_src;
    var tieimg_src;
    var overcoatimg_src;
    var lapelimg_src;
    var frontbuttonimg_src;
    var breastpocketimg_src;
    var lowerpocketimg_src;
    var sleevebuttonimg_src;
    $(".outputsugg").load(function(){
        switch(this.id){
            case "shirtimg":
                shirtimg_src = $("#shirtimg").attr('src');
                break;
            case "tieimg":
                tieimg_src = $("#tieimg").attr('src');
                break;
            case "overcoatimg":
                overcoatimg_src = $("#overcoatimg").attr('src');
                break;
            case "lapelimg":
                lapelimg_src = $("#lapelimg").attr('src');
                break;
            case "frontbuttonimg":
                frontbuttonimg_src = $("#frontbuttonimg").attr('src');
                break;
            case "breastpocketimg":
                breastpocketimg_src = $("#breastpocketimg").attr('src');
                break;
            case "lowerpocketimg":
                lowerpocketimg_src = $("#lowerpocketimg").attr('src');
                break;
            case "sleevebuttonimg":
                sleevebuttonimg_src = $("#sleevebuttonimg").attr('src');
                break;
        }
    });
    $(".outputsugg").error(function(){
        switch(this.id){
            case "shirtimg":
                $("#shirtimg").attr('src',shirtimg_src);
                break;
            case "tieimg":
                $("#tieimg").attr('src',tieimg_src);
                break;
            case "overcoatimg":
                $("#overcoatimg").attr('src',overcoatimg_src);
                break;
            case "lapelimg":
                $("#lapelimg").attr('src',lapelimg_src);
                break;
            case "frontbuttonimg":
                $("#frontbuttonimg").attr('src',frontbuttonimg_src);
                break;
            case "breastpocketimg":
                $("#breastpocketimg").attr('src',breastpocketimg_src);
                break;
            case "lowerpocketimg":
                $("#lowerpocketimg").attr('src',lowerpocketimg_src);
                break;
            case "sleevebuttonimg":
                $("#sleevebuttonimg").attr('src',sleevebuttonimg_src);
                break;
        }
    });
    var count = 0;
    function fnmainfabric(){
        $(".FabricBtnOvercoatFabric").attr("disabled",true);
        var mainfabricfield = $("#mainfabricfield").val();
        if(mainfabricfield != ""){
            setSurplusPrice(encodeURI(mainfabricfield),'OvercoatFabric');
            var newdiv="<div class='primarysuggestion colorsuggestion' onclick=selectFabric('overcoatfabric',this);setSurplusPrice('"+encodeURI(mainfabricfield)+"','OvercoatFabric'); " +
            "id='overcoatfabric_"+mainfabricfield+"'><p class='suggestionname'style='line-height:110px'>"+
            mainfabricfield+"</p></div>";
            $("#mainError").remove();
            $("#mainfabricdiv").append(newdiv);
        }else{
            if(count == 0) {
                count++;
                $("#mainfabricdiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;" +
                "margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
            }
        }
        $('#mainfabricfield').val('');
    }
    var count2=0;
    function fnlinningoption(){
        $(".FabricBtnLiningOption").attr("disabled",true);
        var liningoptionfield = $("#liningoptionfield").val();
        if(liningoptionfield != ""){
            setSurplusPrice(encodeURI(liningoptionfield),'LiningOption');
            var newdiv="<div class='primarysuggestion' onclick=selectFabric('liningoption',this);setSurplusPrice('"+encodeURI(liningoptionfield)+"','LiningOption'); " +
            "id='liningoption_"+liningoptionfield+"' style='height:100px!important;width:18%!important'>" +
            "<p class='suggestionname'style='line-height:100px'>"+liningoptionfield+"</p></div>";
            $("#liningoptionError").remove();
            $("#liningoptiondiv").append(newdiv);
        }else{
            if(count2 == 0) {
                count2++;
                $("#liningoptiondiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;" +
                    "margin:20px 0 -20px 0' id='liningoptionError'>Please Enter Fabric Name First</p>");
            }
        }
        $('#liningoptionfield').val('');

    }
    function selectFabric(type,obj){
        $(".primarysuggestion").removeClass("active");
        $("#"+obj.id).addClass("active");
        if(type == "overcoatfabric") {
            overcoatfabric = (obj.id).split("_")[1];
        }else if(type == "liningoption"){
            liningoption = (obj.id).split("_")[1];
            liningoption_img = (obj.id).split("_")[1];
        }
    }
    function fabricSelected(id,value) {
        $('#'+id).val(value);
        if(id == 'liningoptionfield') {
            $("#findFabric").attr("disabled",false);
            $('#list_data').addClass('data-remove');
        }
        else{
            $("#findFabric1").attr("disabled",false);
            $('#list_data1').addClass('data-remove');
        }
    }
    function searchFabricData(ref,fabricBtn) {

        var value = $('#'+ref.id).val();
        if(ref.id === 'liningoptionfield'){
            $("#findFabric").attr("disabled",true);
        }
        else {
            $("#findFabric1").attr("disabled", true);
        }
        var li_='';
        var url = "admins/api/fabricProcess.php";
        $.post(url,{'dataType':'searchFabricData','cameo_code':value},function(response){
            var results = [];
            var status = response.Status;
            var message = response.Message;
            if(status === 'Success' ) {
                results = [];
                var dataa = response.data;
                li_ = '';
                for(var i=0;i<dataa.length;i++) {
                    /*if(value.charAt(0) === 'd' || value.charAt(0) === 'D' ) {
                        li_ = li_ + '<li onclick=fabricSelected("' + ref.id + '","' + dataa[i].fabric_cameo + '")>' + dataa[i].fabric_cameo + '</li>';
                    }
                    else if(value.charAt(0) === 'm' || value.charAt(0) === 'M' ) {
                        li_ = li_+'<li onclick=fabricSelected("'+ref.id+'","'+dataa[i].fabric_rc_code+'")>'+dataa[i].fabric_rc_code+'</li>';
                    }*/
                    li_ = li_ + '<li onclick=fabricSelected("' + ref.id + '","' + dataa[i].fabric_code + '")>' + dataa[i].fabric_code + '</li>';

                }
                if(ref.id === 'liningoptionfield') {
                    $('#list_data').removeClass('data-remove');
                    $('#list_data').html(li_);
                }
                else{
                    $('#list_data1').removeClass('data-remove');
                    $('#list_data1').html(li_);
                }
//                $(".FabricBtn"+fabricBtn).attr("disabled",false);
            }
            else if(status === 'Failure') {
                li_ = li_+'<li>No data found</li>';
                if(ref.id === 'liningoptionfield') {
                    $('#list_data').removeClass('data-remove');
                    $('#list_data').html(li_);
                }
                else{
                    $('#list_data1').removeClass('data-remove');
                    $('#list_data1').html(li_);
                }
            }
        });
    }
    window.onclick = function () {
        if(!$("#list_data").hasClass('data-remove')) {
            $("#list_data").addClass('data-remove');
        }
        if(!$("#list_data1").hasClass('data-remove')) {
            $("#list_data1").addClass('data-remove');
        }
    };
        function getSession() {
            var url = "admin/webserver/selectionData.php?type=custom_overcoats&sessionType=getSession";
            currPage = "category";
            $.get(url,function(data) {
                data = $.parseJSON(data);
                if(data.status ==='done') {
                    currPage = data.content;
                    if(data.overcoatfabric!=="")
                        overcoatfabric = data.overcoatfabric;
                    if(data.threadcolor!=="")
                        threadcolor = data.threadcolor;
                    if(data.liningoption_img!=="")
                        liningoption_img = data.liningoption_img;
                    if(data.liningoption!=="")
                        liningoption = data.liningoption;
                    if(data.liningstyle_img!=="")
                        liningstyle_img = data.liningstyle_img;
                    if(data.liningstyle!=="")
                        liningstyle = data.liningstyle;
                    if(data.collarstyle_img!=="")
                        collarstyle_img = data.collarstyle_img;
                    if(data.collarstyle!=="")
                        collarstyle = data.collarstyle;
                    if(data.pocketstyle_img!=="")
                        pocketstyle_img = data.pocketstyle_img;
                    if(data.facingstyle_img!=="")
                        facingstyle_img = data.facingstyle_img;
                    if(data.facingstyle!=="")
                        facingstyle = data.facingstyle;
                    if(data.coinpocket_img!=="")
                        coinpocket_img = data.coinpocket_img;
                    if(data.breastpocket_img!=="")
                        breastpocket_img = data.breastpocket_img;
                    if(data.breastpocket!=="")
                        breastpocket = data.breastpocket;
                    if(data.topstichstyle_img!=="")
                        topstichstyle_img = data.topstichstyle_img;
                    if(data.topstichstyle!=="")
                        topstichstyle = data.topstichstyle;
                    if(data.backsideofarmhole_img!=="")
                        backsideofarmhole_img = data.backsideofarmhole_img;
                    if(data.backsideofarmhole!=="")
                        backsideofarmhole = data.backsideofarmhole;
                    if(data.stitchstyle_img!=="")
                        stitchstyle_img = data.stitchstyle_img;
                    if(data.stitchstyle!=="")
                        stitchstyle = data.stitchstyle;
                    if(data.backvent_img!=="")
                        backvent_img = data.backvent_img;
                    if(data.backvent!=="")
                        backvent = data.backvent;
                    if(data.sleevebuttonstyle_img!=="")
                        sleevebuttonstyle_img = data.sleevebuttonstyle_img;
                    if(data.sleevebuttonstyle!=="")
                        sleevebuttonstyle = data.sleevebuttonstyle;
                    if(data.sleeveslitstyle_img!=="")
                        sleeveslitstyle_img = data.sleeveslitstyle_img;
                    if(data.sleeveslitstyle!=="")
                        sleeveslitstyle = data.sleeveslitstyle;
                    if(data.shouldertabstyle_img!=="")
                        shouldertabstyle_img = data.shouldertabstyle_img;
                    if(data.shouldertabstyle!=="")
                        shouldertabstyle = data.shouldertabstyle;
                    if(data.shoulderpatchstyle_img!=="")
                        shoulderpatchstyle_img = data.shoulderpatchstyle_img;
                    if(data.shoulderpatchstyle!=="")
                        shoulderpatchstyle = data.shoulderpatchstyle;
                    if(data.shoulderstyle_img!=="")
                        shoulderstyle_img = data.shoulderstyle_img;
                    if(data.shoulderstyle!=="")
                        shoulderstyle = data.shoulderstyle;
                    if(data.elbowpadstyle_img!=="")
                        elbowpadstyle_img = data.elbowpadstyle_img;
                    if(data.elbowpadstyle!=="")
                        elbowpadstyle = data.elbowpadstyle;
                    if(data.sweatpadstyle_img!=="")
                        sweatpadstyle_img = data.sweatpadstyle_img;
                    if(data.sweatpadstyle!=="")
                        sweatpadstyle = data.sweatpadstyle;
                    if(data.buttonstyle_img!=="")
                        buttonstyle_img = data.buttonstyle_img;
                    if(data.buttonstyle!=="")
                        buttonstyle = data.buttonstyle;
                    if(data.pocketstype_img!=="")
                        pocketstype_img = data.pocketstype_img;
                    if(data.pocketstype!=="")
                        pocketstype = data.pocketstype;
                    if(data.lapelstyle_img!=="")
                        lapelstyle_img = data.lapelstyle_img;
                    if(data.lapelstyle!=="")
                        lapelstyle = data.lapelstyle;
                    if(data.canvasstyle_img!=="")
                        canvasstyle_img = data.canvasstyle_img;
                    if(data.canvasstyle!=="")
                        canvasstyle = data.canvasstyle;
                    if(data.overcoatcategory!=="")
                        overcoatcategory = data.overcoatcategory;

                }


                $(".steps").removeClass("stepsactive");
                $(".stepdiv").fadeOut("slow");
                $("#"+currPage+"s").fadeIn("slow");
                $("#"+currPage).addClass("stepsactive");
                loadSessionData();


            });
        }

        function savetoSession(type,content_id) {
            var url = "admin/webserver/selectionData.php?sessionType=storeSession&type=custom_overcoats&content_id="+content_id;
            $.get(url,function(data){
                console.log('response --- '+data);
            });
        }
         loadSessionData();
//        getSession();
    </script>
</div>