<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if (isset($_POST['filterButton'])) {
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query = "select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
} else {
    $query = "select * from orders order by order_id DESC";
}
?>
<style>
    .ddkfxx ul li .p1 {
        font-weight: bold;
        width: 34% !important;
    }

    .ddkfxx td {
        border: 1px solid #999999;
        padding: 10px 100px 7px 11px;
    }
    .newddkfxx {
        padding: 28px 2px!important;
        width: 1149px !important;
    }
    .newddxq_tc {
        width: 1149px!important;
    }
    .newddkfxx th {
        font-size: 12px;
        font-weight: 500;
        text-align: center;
    }
     .saver{color: orangered;font-size: 12px;cursor: pointer;display: none;}
    .cancler{color: red;font-size: 12px;cursor: pointer;display: none;}
    .editors{color: brown;font-size: 12px;cursor: pointer;float:right;display: none;}
    .inputer{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer0{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer1{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer2{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer3{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer4{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer5{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer6{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer7{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer8{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer9{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer10{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer11{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer12{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer13{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer14{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
    .inputer15{border: 1px solid rgb(153, 153, 153);padding: 0px 7px; width: 60%;display: none}
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Orders
                            <small></small>
                        </h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px"
                                        onclick="window.location='api/excelProcess.php?dataType=allOrders'"
                                        class="btn btn-info btn-sm">Download Excel File
                                </button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control"
                                               name="startDate" id="startFilter"/>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate"
                                               id="endFilter"/>
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton"
                                           style="margin-top: 5px"/>
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="showmodel_data"></div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });
    function storeStatus(status, store_id) {
        var url = "update_store.php?store_status=" + status + "&store_id=" + store_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                if (status == "done") {
                    alert("Company status update successfully...");
                    window.location = "";
                }
                else {
                    //alert('error');
                }
            }
        });
    }

    function get_referel_payment_details(store_id, designation,reffercode) {
        var url = "api/paymentProcess.php?store_id=" + store_id + "&designation=" + designation +"&refer_code="+ reffercode;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var paymentData = json.storedata;
                var status = json.status;
                var orderModalData = '';
                if (status == "done") {
                    orderModalData = orderModalData + '<link rel="stylesheet" type="text/css" href="css/defaulten.css">' +
                        '<div class="layui-layer-shade" id="layui-layer-shade5" times="5" style="z-index: 19891018; background-color: rgb(0, 0, 0); opacity: 0.7;" onclick="close_diolog()">' +
                        '</div><div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5" type="page" times="5" showtime="0" contype="string" style="z-index: 19891019;' +
                        ' top: 65px; height: 433px; width: 1017px; left: 180px;">' +
                        '<div class="layui-layer-title" style="cursor: move;color: green;font-weight: 600;" move="ok">' + paymentData[0].store_name + '&nbsp( ' + paymentData[0].designation + ' )</div>' +
                        '<div class="layui-layer-content order-detail-modal" style="height: 556px;">' +
                        '<div class="ddxq_tc"><div class="ddkfxx"> <table class="table table-striped table-bordered"><thead>' +
                        '<tr><th>#</th><th>Order Amount</th><th>Commission %</th><th>Earning Amount</th><th>Payment Status</th></tr></thead>';
                        var j = 0;
                        var total = 0;
                        for (var a = 0; a < paymentData.length; a++) {
                            if(paymentData[a].level_commission ==""){
                                var level = '';
                            }
                            else{

                                var level = '<span>+'+paymentData[a].level_commission+'%</span>';
                            }

                            j++;
                            total = total + paymentData[a].total_commission;
                            orderModalData = orderModalData + '<tbody>' + '<tr><td>' + j + '</td><td>$' + paymentData[a].total_order + '</td><td>' + paymentData[a].single_commission +' '+level+'</td><td>' + paymentData[a].total_commission + '</td><td>' + paymentData[a].comm_status + '</td>'
                                + '</tr></tbody>';
                        }
                        orderModalData = orderModalData + '</table><table style="float:right;"><tr><td>Total Earnings </td></tr><tr><td style="font-weight: 600;color:#999;">$' + total + '</td></tr></table></div></div> <table style="float: right; text-align: center; height: 38px; margin-right: 7%; width: 21%; color: white;"><tr style="background: steelblue none repeat scroll 0% 0%; text-align: center; color: white;"><td>Pay Now</td></tr></table></div></div>';
                        $("#showmodel_data").html(orderModalData);
                }
                else {
                    alert(status);
                }
            }
        });
    }
    function close_diolog() {
        window.location = "paytostoreowner.php"
    }


    function getCommissionStatus(){
        var url = "api/levelProcess.php?type=viewcommission";
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var carrerData = json.carrerdata;
                var status = json.status;
                var commissionModalData = '';
                if (status == "done") {
                    commissionModalData = commissionModalData + '<link rel="stylesheet" type="text/css" href="css/defaulten.css">' +
                        '<div class="layui-layer-shade" id="layui-layer-shade5" times="5" style="z-index: 19891018; background-color: rgb(0, 0, 0); opacity: 0.7;" onclick="close_diolog()">' +
                        '</div><div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5" type="page" times="5" showtime="0" contype="string" style="z-index: 19891019;' +
                        ' z-index: 19891019; width: 1190px; left: 79px; top: 43px; height: 530px; overflow-x: scroll;">' +
                        '<div class="layui-layer-title" style="cursor: move;color: green;font-weight: 600;" move="ok">' +
                        'sftailor Career Plans</div>' +
                        '<div class="layui-layer-content order-detail-modal" style="height: 556px;">' +
                        '<div class="ddxq_tc newddxq_tc"><div class="ddkfxx newddkfxx"> <table class="table table-striped table-bordered"><thead>' +
                        '<tr><th>Title</th><th>Stylist</th><th>Senior Stylist</th><th>Style Associate</th><th>Style Leader</th>' +
                        '<th>Style Manager</th><th>Style Director</th><th>Style Executive</th>' +
                        '<th>Style Ambassador</th><th>Style Vice President</th><th>Style President</th>';
                        for (var a = 0; a < carrerData.length; a++) {
							commissionModalData = commissionModalData + '<tr id="showedit"><td style="width: 145px;"><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer1'+a+'" value="'+carrerData[a].title+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].title+'</span><div style="cursor: pointer; color: white; border-radius: 2px; background: steelblue none repeat scroll 0% 0%; height: 18px; padding: 1px 6px; float: right;" onclick="editData('+carrerData[a].level_id+')"><i class="fa fa-pencil"></i></div><div style="cursor: pointer; color: white; border-radius: 2px; background: seagreen none repeat scroll 0% 0%; height: 18px; padding: 1px 6px; float: right;margin-right: 2px;" onclick="updateData('+carrerData[a].level_id+')"><i class="fa fa-floppy-o"></i></div></td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer2'+a+'" value="'+carrerData[a].stylist+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].stylist+'</span>' + '</td>' +
							'<td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer3'+a+'" value="'+carrerData[a].senior_stylist+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].senior_stylist+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer4'+a+'" value="'+carrerData[a].style_associate+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_associate+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer5'+a+'" value="'+carrerData[a].style_leader+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_leader+'</span>&nbsp;&nbsp;' + '</td>' +
							'<td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer6'+a+'" value="'+carrerData[a].style_manager+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_manager+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer7'+a+'" value="'+carrerData[a].style_director+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_director+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer8'+a+'" value="'+carrerData[a].style_executive+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_executive+'</span>&nbsp;&nbsp;' + '</td>' +
							'<td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer9'+a+'" value="'+carrerData[a].style_ambassador+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_ambassador+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer10'+a+'" value="'+carrerData[a].style_vice_president+'"> <span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_vice_president+'</span>&nbsp;&nbsp;' + '</td><td><input type="text" class="inputer'+carrerData[a].level_id+'" id="inputer11'+a+'" value="'+carrerData[a].style_president+'"><span class="stylist'+carrerData[a].level_id+'">'+carrerData[a].style_president+'</span>&nbsp;&nbsp;' + '</td></tr>';
						}
                        commissionModalData = commissionModalData + '</tr></thead></table></div></div>';
                    $("#showmodel_data").html(commissionModalData);
                }
            }
        });


    }
	getCommissionStatus();

function updateData(levelId){
var inputs = $(".inputer"+levelId);
var levels = [];
for(var i = 0; i < inputs.length; i++){
		var level1 = $(inputs[i]).val();
		levels.push(level1);
	}
	var levelss = levels.join(',');
	var url = "api/levelProcess.php?type=updateCommission&level_id="+levelId+"&inputval="+levelss;
	$.get(url, function (data) {
		var json = $.parseJSON(data)
		{
			var carrerData = json.carrerdata;
			var status = json.status;
			var commissionModalData = '';
			if (status == "done") {
				alert("carrer plan updated successfully!");
				window.location="paytostoreowner.php";
			}
		}
	}); 
}
function editData(levelid){
	$(".inputer"+levelid).show();
	$(".stylist"+levelid).hide();
}


</script>
