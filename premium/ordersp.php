<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if(isset($_POST['filterButton'])){
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query="select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
}else{
    $query="select * from orders order by order_id DESC";
}

?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">

    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Orders <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Orders Generated
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Order Amount</th>
                                <th>Order Dated</th>
                                <th>Order Status</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $query = "select * from wp_orders order by order_id DESC";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($orderData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <?php
                                                    $userResp = $userClass->getParticularUserData($orderData['order_user_id']);
							
                                                    $userName = $userResp['UserData']['name'];
                                                ?>
                                                <td data-title='User Name'><?php echo $userName ?></td>
                                                <td data-title='Order Id'>
                                                        <?php echo $orderData['order_number'] ?>
                                                </td>
                                                <?php
                                                $amount = $orderData['order_total'];
                                                ?>
                                                <td data-title='Order Amount'>$<?php echo $amount ?></td>
                                                <td data-title='Order Dated'><?php echo $orderData['order_date']; ?></td>
                                                <td data-title='Order Status'><?php echo $orderData['order_state'] ?></td>
                                                <td data-title='Payment Status'><?php echo $orderData['order_payment_status'] ?></td>

                                                <?php if($orderData['order_payment_status'] != "Success"){
                                                    ?>
                                                    <td data-title='Action'>
                                                        <button class='btn btn-sm btn-danger' title="Order Cancelled Automatically" disabled ) >Cancel Order</button>
														<a href='#' onclick=delOrder('<?php echo $orderData['order_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>
                                                        <a href="invoices.php?uid=<?php echo $orderData['order_user_id'];?>&oid=<?php echo $orderData['order_number'] ?>"><button class='btn btn-sm btn-primary'>Invoice</button></a>

                                                    </td>
                                                    <?php
                                                }else{
                                                    if($orderData['order_state'] == "Success") {
                                                        ?>
                                                        <td data-title='Action'>
                                                            <button class='btn btn-sm btn-danger'
                                                                    onclick=cancelOrder('<?php echo $orderData['order_id'] ?>','<?php echo $orderData['order_dated'] ?>')>
                                                                Cancel Order
                                                            </button>
                                                        </td>
                                                        <?php
                                                    }else{
                                                        ?>
                                                        <td data-title='Action'>
                                                            <button class='btn btn-sm btn-warning' disabled>Cancelled</button>
                                                        </td>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });
	
	function delOrder(delid){
		   var url = "update_store.php?type=delitemOrder&delid="+delid;
			$.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                if (status == "done") {
                        window.location="orders.php";
                }
                else {
                    //alert('error');
                }
            }
        });
	}
	
	
</script>
