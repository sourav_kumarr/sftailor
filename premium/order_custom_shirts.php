<?php
include('header.php');
include("front_header.php");
include("../admin/webserver/database/db.php");
?>
<style>
    .nav_menu{
        margin-bottom: 0px;
    }
    .active {
        border: 2px solid #555;
        border-radius: 4px;
    }

    .primarysuggestion {
        height: auto;
        margin-left:3px;
        margin-top:3px;
        width: auto;
    }

    .steps {
        background: #fff none repeat scroll 0 0;
        border: 2px solid #aeaeae;
        border-radius: 7%;
        cursor: pointer;
        float: left;
        font-size: 13px;
        line-height: 18px;
        margin-bottom: 0;
        margin-left: 5px;
        margin-top: 5px;
        max-height: 57px;
        padding-top: 9px;
        text-align: center;
        width: 9.2%;
        font-weight: normal;
        font-family:calibri;
        text-transform: capitalize;
    }
    .stepsactive {
        background: #c4996c none repeat scroll 0 0;
        border: 2px solid #c4996c;
        color: white;
    }

    .outputsugg {
        position: absolute;
        margin-left: -70px;
        z-index: -1;
    }

    .backbtnsugg {
        height: 68%;
        width: 100%;
    }

    .backbtnsuggparentdiv {
        height: 175px;
        width: 118px;
    }

    .backbtnsuggparentdiv p {
        font-weight: normal;
        font-size: 12px;
        line-height: 18px !important;
    }

    .backbtnsuggparentdivs {
        height: 195px;
        width: 118px;
    }

    .backbtnsuggparentdivs p {
        font-weight: normal;
        font-size: 12px;
        line-height: 18px !important;
    }

    .contrastheading {
        font-size: 21px;
        margin-bottom: 25px;
        text-decoration: underline;
    }

    .contrastdiv .col-md-4 {
        cursor: pointer;
        margin-bottom: 10px;
    }

    .okok {
        font-size: 19px;
        height: 52px;
        margin-left: 10px;
        margin-top: 10px;
        padding-top: 6px;
        width: 162px;
    }

    .stylecaption {
        margin-top: 20px;
    }
   /* .shirt-imgs img {
        width: auto !important;
    }*/

    .overflow-cmd {
        overflow-y: auto;
        height: 660px;
        padding-top: 22px;
    }

    html {
        overflow-x: hidden;
    }
    .colorsuggestion {
        height: 110px!important;
        width: 100px!important;
    }
    .colorsuggestion img{
        height:80% !important;
    }
    #lettertype{
        font-weight:normal;
        line-height:34px;
        font-family:Times New Roman!important;
    }
    #lettertype{font-size: font-size: 30px!important;}

    .data_{
        margin-top: 2px;
        padding: 0px;
        border: 1px lightgrey solid;
        border-radius: 5px;
        position: absolute;
        max-height: 100px;
        overflow: auto;
        width: 98%;
        z-index: 1;
    }

    .data_ li{
        padding: 5px;
        border-bottom: 1px lightgrey solid;
    }
    .data_ li:hover{
        cursor: pointer;
        background-color: silver;
    }
    .data-remove{
        display:none;
    }
    .outputsugg {
        position: absolute;
        margin-left: -50px;
        z-index: -1;
        width: 460px;
    }
    .nav-md .container.body .right_col{
        padding: 35px  1px  0 0;
        background: #fff;
    }
    .about-bottom{
        padding: 48px;
    }
    .steps{
        font-size: 12px!important;
    }
    .suggestionname{
        font-size: 12px;
    }
    #pockets img{
        width:100%;
    }
</style>
<div class="right_col" role="main" style="overflow-y: auto">
    <div class="about-bottom wthree-3">
        <div class="loader" style="display: none">
            <img src="../images/spin.gif" class="spinloader"/>
        </div>
        <div class="col-md-12" style="padding: 0">
            <div class="col-md-6">
                <div class="col-md-1" style="z-index:100">
                    <label onclick="rotateImage('left')" id="rotateLeft" style="margin-top: 350px"><i class="fa fa-share fa-2x"></i></label>
                </div>
                <div class="col-md-10 shirt-imgs" style="padding-top: 50px;position: relative;z-index: 1">
                    <img src="" class="outputsugg" id="sleeveimg" style="z-index: 3" alt=''/>
                    <img src="" class="outputsugg" id="shirtimg" style="" alt=''/>
                    <img src="" class="outputsugg" id="shoulderimg" style="z-index:1" alt=''/>
                    <img src="" class="outputsugg" id="buttonimg" style="z-index:4" alt=''/>
                    <img src="" class="outputsugg" id="buttoncutimg" style="z-index:3" alt=''/>
                    <img src="" class="outputsugg" id="buttonpointimg" style="z-index:5" alt=''/>
                    <img src="" class="outputsugg" id="collarimg" style="z-index: 2" alt=''/>
                    <img src="" class="outputsugg" id="collarbeltimg" style="" alt=''/>
                    <img src="" class="outputsugg" id="collarbuttonimg" style="z-index:2" alt=''/>
                    <img src="" class="outputsugg" id="collarbuttoncutimg" style="z-index:1" alt=''/>
                    <img src="" class="outputsugg" id="collarbuttonpointimg" style="z-index:3" alt=''/>
                    <img src="" class="outputsugg" id="cuffimg" style="" alt=''/>
                    <img src="" class="outputsugg" id="cuffbuttonimg" style="z-index:3" alt=''/>
                    <img src="" class="outputsugg" id="cuffbuttoncutimg" style="z-index:2" alt=''/>
                    <img src="" class="outputsugg" id="cuffbuttonpointimg" style="z-index:4" alt=''/>
                    <img src="" class="outputsugg" id="placketimg" style="" alt=''/>
                    <img src="" class="outputsugg" id="pocketimg" style="" alt=''/>
                    <img src="" class="outputsugg" id="pleatimg" style="" alt=''/>
                </div>
                <div class="col-md-1" style="z-index: 100">
                    <label onclick="rotateImage('right')" id="rotateRight" style="margin-top: 350px"><i
                                class="fa fa-reply fa-2x"></i></label>
                </div>
            </div>
            <div class="col-md-6" style="padding:50px 0 25px">
                <div class="col-md-12" style="padding:0">
                    <div class="steps stepsactive" id="category">Shirt Category</div>
                    <div class="steps" id="collar">Shirt <br>Collar</div>
                    <div class="steps" id="frontcollar">Front Collar</div>
                    <div class="steps" id="collarbelt">Collar Band</div>
                    <div class="steps" id="cuff">Shirt <br>Cuff</div>
                    <div class="steps" id="cuffwidth">Cuff<br>Width</div>
                    <div class="steps" id="placket">Shirt <br> Placket</div>
                    <div class="steps" id="placketbutton">Button <br> Quantity</div>
                    <div class="steps" id="pocket">Shirt Pocket</div>
                    <div class="steps" id="pleat">Back <br>Style</div>
                    <div class="steps" id="bottom">Bottom Gusset</div>
                    <div class="steps" id="shirtcolor">Shirt <br>Color</div>
                    <div class="steps" id="buttoncolor">Button <br>Color</div>
                    <div class="steps" id="buttoningstyle">Buttoning <br>Style</div>
                    <div class="steps" id="buttoncutthread">Button Thread</div>
                    <div class="steps" id="holestichthread">Hole <br> Stich</div>
                    <div class="steps" style="padding-top: 10px" id="monogram">Mono<br>gram</div>
                    <div class="steps" id="contrastfabric">Contrast <br> Fabric</div>
                </div>
                <!------
            -----category Category div
            ------
            -------->
                <div class="col-md-12 stepdiv" id="categorys" style="display:block">
                    <p class="stylecaption">Shirt Category </p>
                    <div class="primarysuggestion okok active" style="margin-left: 122px" id="category_formal" name="formal">
                        <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Formal</p>
                    </div>
                    <div class="primarysuggestion okok" id="category_casual" name="casual">
                        <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Casual</p>
                    </div>
                    <div class="primarysuggestion okok" id="category_tuxedo" name="tuxedo">
                        <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Tuxedo</p>
                    </div>
                </div>
                <!--
            ----Collar style div
            --->
                <div class="container stepdiv">
                    <div class="col-md-12 overflow-cmd" id="collars" style="display:none">
                        <p class="stylecaption">Collar Style</p>

                        <?php
                        $query = "select * from shirt_collars where status = '1'";
                        $result = mysql_query($query);
                        if ($result) {
                            $num = mysql_num_rows($result);
                            if ($num > 0) {
                                $status = "done";
                                $a = 0;
                                while ($rows = mysql_fetch_array($result)) {
                                    $a++;
                                    $collar_name = $rows['collar_name'];
                                    $name = explode("_",$collar_name);
                                    $startName = $name[0];
                                    ?>
                                    <div class='primarysuggestion backbtnsuggparentdiv<?php if ($a == 1) echo ' active' ?>'
                                         name="<?php echo $startName ?>:<?php echo $collar_name ?>" title="<?php echo $collar_name ?>"
                                         id='collar_<?php echo $rows['collar_id']; ?>'>
                                        <img src='../admins/api/Files/images/collar/<?php echo $rows['collar_img_path'];?>' class="backbtnsugg"/>
                                        <p class="suggestionname"
                                           style="text-transform:uppercase;"><?php echo $rows['collar_description']; ?></p>
                                    </div>
                                    <?php
                                }
                            } else {
                                echo "No Data Found";
                            }
                        } else {
                            echo mysql_error();
                        }
                        ?>

                        <div class="clear"></div>
                        <div style="display:block;">
                            <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
                                <p class="stylecaption">BUTTON DOWN OR TAB WITH BUTTON</p>
                                <div class="primarysuggestion backbtnsuggparentdiv" name="S-5290:collar_point_with_button"
                                     id="collarbuttondown_S-5290collarpointwithbutton" title="">
                                    <img src="../images/shirts/S-5290collarpointwithbutton.png" class="backbtnsugg"/>
                                    <p class="suggestionname">S-5290</br>Collar point with button</p>
                                </div>
                                <div class="primarysuggestion backbtnsuggparentdiv" name="S-5058:tab_with_botton_on_collar"
                                     id="collarbuttondown_S-5058tabwithbottononcollar" title="">
                                    <img src="../images/shirts/S-5058tabwithbottononcollar.png" class="backbtnsugg"/>
                                    <p class="suggestionname">S-5058</br>Tab with botton on collar</p>
                                </div>
                            </div>
                            <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
                                <p class="stylecaption">DOUBLE LAYER OPTION (Upper contrast)</p>
                                <div class="primarysuggestion backbtnsuggparentdiv" name="S-5B41:top_collar_with_contrast_color,5G26"
                                     id="doublelayeroption_S-5B41topcollarwithcontrastcolor"
                                     title="Top collar with contrast color">
                                    <img src="../images/shirts/S-5B41topcollarwithcontrastcolor.png" class="backbtnsugg"/>
                                    <p class="suggestionname">S-5B41</br>Top collar with..</br>5G26
                                </div>
                                <div class="col-md-12"  style="display:block">
                                    <p class="stylecaption">Upper contrast Febrics</p>
                                    <div class="col-md-12" id="mainfabricdiv2" style="height: 400px;overflow-y: scroll;padding:0px">
                                        <?php
                                        $query = "select * from wp_colors where type='shirt' and onlyfor = 'shirt' and status = '1'";
                                        $result = mysql_query($query);
                                        if ($result) {
                                            $num = mysql_num_rows($result);
                                            if ($num > 0) {
                                                $status = "done";
                                                $a = 0;
                                                while ($rows = mysql_fetch_array($result)) {
                                                    $a++;
                                                    $colorName = $rows['colorName'];
                                                    ?>
                                                    <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                                                         name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                                                         id='doublelayeroption_<?php echo $colorName ?>' onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','UpperContrast')>
                                                        <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                                                        <p class="suggestionname"
                                                           style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                                                    </div>
                                                    <?php
                                                }
                                            } else {
                                                echo "No Color Found";
                                            }
                                        } else {
                                            echo mysql_error();
                                        }
                                        ?>
                                    </div>
                                    <div class="col-md-12" style="padding: 0">
                                        <div class="form-group col-md-6" style="margin-top:20px;padding:0 20px">
                                            <label>Fabric Name</label>
                                            <div contenteditable class="form-control" value="" id="mainfabricfield2" onkeyup="searchFabricData(this,'UpperContrast')" /></div>
                                        <ul class="list-unstyled data_ data-remove" id="list_data" style="background: #fff">
                                        </ul>
                                    </div>
                                    <div class="form-group col-md-6" style="margin-top:20px">
                                        <div type="button" class="btn btn-danger pull-right FabricBtnUpperContrast"  onclick="addmainfabric2()"
                                             style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric"
                                             disabled />Find Fabric</div>
                                </div>
                                <div class="row">
                                    <label class="surplus_price_label" id="surplus_price_labelUpperContrast" style="display: none">
                                        Surplus Price :
                                        <span class="surplus_price" id="surplus_priceUpperContrast"></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
                        <p class="stylecaption">DOUBLE LAYER OPTION (Lower contrast)</p>
                        <div class="primarysuggestion backbtnsuggparentdiv" name="5B12:lower_layer_of_collar_contrast_fabric,S-5B41"
                             id="doublelayeroption_5G26doubleplytoplayeris12cmsmallertheninnerlayers"
                             title="Double Ply top layer is 1.2cm smaller then inner layer">
                            <img src="../images/shirts/S-G26doubleplytoplayeris12cmsmallertheninnerlayer.png"
                                 class="backbtnsugg"/>
                            <br class="suggestionname">5G26</br>Double Ply top..</br>S-5B41</p>
                        </div>
                        <div class="col-md-12" id="" style="display:block">
                            <p class="stylecaption">Lower contrast Febrics</p>
                            <div class="col-md-12" id="mainfabricdiv3" style="height: 400px;overflow-y: scroll;padding:0px">
                                <?php
                                $query = "select * from wp_colors where type='shirt' and onlyfor = 'shirt' and status = '1'";
                                $result = mysql_query($query);
                                if ($result) {
                                    $num = mysql_num_rows($result);
                                    if ($num > 0) {
                                        $status = "done";
                                        $a = 0;
                                        while ($rows = mysql_fetch_array($result)) {
                                            $a++;
                                            $colorName = $rows['colorName'];
                                            ?>
                                            <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                                                 name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                                                 id='doublelayeroption1_<?php echo $colorName ?>' onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','LowerContrast')>
                                                <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                                                <p class="suggestionname"
                                                   style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                                            </div>
                                            <?php
                                        }
                                    } else {
                                        echo "No Color Found";
                                    }
                                } else {
                                    echo mysql_error();
                                }
                                ?>
                            </div>
                            <div class="col-md-12" style="padding: 0">
                                <div class="form-group col-md-6" style="margin-top:20px;padding:0 20px">
                                    <label>Fabric Name</label>
                                    <div contenteditable class="form-control" value="" id="mainfabricfield3" onkeyup="searchFabricData(this,'LowerContrast')" /></div>
                                <ul class="list-unstyled data_ data-remove" id="list_data1" style="background: #fff;z-index:1">
                                </ul>
                            </div>
                            <div class="form-group col-md-6" style="margin-top:20px">
                                <div type="button" class="btn btn-danger pull-right FabricBtnLowerContrast"  onclick="addmainfabric3()"
                                     style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric1" disabled />Find Fabric</div>

                        </div>
                        <div class="row">
                            <label class="surplus_price_label" id="surplus_price_labelLowerContrast" style="display: none">
                                Surplus Price :
                                <span class="surplus_price" id="surplus_priceLowerContrast"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="frontcollars" style="display:none">
        <p class="stylecaption">Front Collar Style</p>
        <div class="primarysuggestion backbtnsuggparentdiv active" name="frontcollarstandheight25cm"
             id="frontcollar_frontcollarstandheight25cm">
            <img src="../images/shirts/frontcollarstandheight25cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Front Collar Stand Height 2.5cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="frontcollarstandheight3cm"
             id="frontcollar_frontcollarstandheight3cm">
            <img src="../images/shirts/frontcollarstandheight3cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Front Collar Stand Height 3cm(standard)</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="frontcollarstandheight35cm"
             id="frontcollar_frontcollarstandheight35cm">
            <img src="../images/shirts/frontcollarstandheight35cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Front Collar Stand Height 3.5cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="frontcollarstandheight38cm"
             id="frontcollar_frontcollarstandheight38cm">
            <img src="../images/shirts/frontcollarstandheight38cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Front Collar Stand Height 3.8cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="frontcollarstandheight5cm"
             id="frontcollar_frontcollarstandheight5cm">
            <img src="../images/shirts/frontcollarstandheight5cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Front Collar Stand Height 5cm</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="collarbelts" style="display:none">
        <p class="stylecaption">Collar Band Style</p>
        <div class="primarysuggestion backbtnsuggparentdiv active" name="squared1collaronebutton"
             style="" id="collarbelt_squared1collaronebutton">
            <img src="../images/shirts/squaredcollaronebutton.png" class="backbtnsugg"/>
            <p class="suggestionname">Squared Collar One Button</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="squared1collartwobutton"
             id="collarbelt_squared1collartwobutton">
            <img src="../images/shirts/squaredcollartwobutton.png" class="backbtnsugg"/>
            <p class="suggestionname">Squared Collar Two Button</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="rounded1collaronebutton"
             id="collarbelt_rounded1collaronebutton">
            <img src="../images/shirts/roundedcollaronebutton.png" class="backbtnsugg"/>
            <p class="suggestionname">Rounded Collar One Button(most Popular)</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="rounded1collartwobutton"
             id="collarbelt_rounded1collartwobutton">
            <img src="../images/shirts/roundedcollartwobutton.png" class="backbtnsugg"/>
            <p class="suggestionname">Rounded Collar Two Button</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv  overflow-cmd" id="cuffs" style="display:none">
        <p class="stylecaption">Cuff Style</p>
        <div class="primarysuggestion backbtnsuggparentdiv active" name="S-5410:rounded_cuff_one_button_and_one_button_hole"
             title="Rounded Cuff One Button & One Button Hole" id="cuff_roundedcuffonebuttonandonebuttonhole">
            <img src="../images/shirts/roundedcuffonebuttonandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5410 Rounded Cuff One Button (Most Popular)..</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5411:rounded_cuff_two_buttons_and_one_button_hole"
             title="Rounded Cuff Two Buttons & One Button Hole" id="cuff_roundedcufftwobuttonsandonebuttonhole">
            <img src="../images/shirts/roundedcufftwobuttonsandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5411 Rounded Cuff Two Buttons...</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="square_french" title="Square French"
             id="cuff_SquareFrench">
            <img src="../images/shirts/SquareFrench.png" class="backbtnsugg"/>
            <p class="suggestionname">Square French</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="french_three" title="French Three"
             id="cuff_FrenchThree">
            <img src="../images/shirts/FrenchThree.png" class="backbtnsugg"/>
            <p class="suggestionname">French Three</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="545T:rounded_french" title="Rounded French"
             id="cuff_RoundedFrench">
            <img src="../images/shirts/RoundedFrench.png" class="backbtnsugg"/>
            <p class="suggestionname">545T Rounded French</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5412:rounded_cuff_two_buttons_and_two_button_holes"
             title="Rounded Cuff Two Buttons & Two Button Holes" id="cuff_roundedcufftwobuttonsandtwobuttonholes">
            <img src="../images/shirts/roundedcufftwobuttonsandtwobuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5412 Rounded Cuff Two Buttons...</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5413:rounded_cuff_three_buttons_and_three_button_holes"
             title="Rounded Cuff Three Buttons & Three Button Holes"
             id="cuff_roundedcuffthreebuttonsandthreebuttonholes">
            <img src="../images/shirts/roundedcuffthreebuttonsandthreebuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5413 Rounded Cuff Three Buttons...</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5414:small_rounded_cuff_one_button_and_one_button_hole"
             title="Small Rounded Cuff One Button & One Button Hole"
             id="cuff_smallroundedcuffonebuttonandonebuttonhole">
            <img src="../images/shirts/smallroundedcuffonebuttonandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5414 Small Rounded Cuff One Button..</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5415:small_rounded_cuff_two_buttons_and_two_button_holes"
             title="Small Rounded Cuff Two Buttons & Two Button Holes"
             id="cuff_smallroundedcufftwobuttonsandtwobuttonholes">
            <img src="../images/shirts/smallroundedcufftwobuttonsandtwobuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5415 Small Rounded Cuff ..</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5416:small_rounded_cuff_three_buttons_and_three_button_holes"
             title="Small Rounded Cuff Three Buttons & Three Button Holes"
             id="cuff_smallroundedcuffthreebuttonsandthreebuttonholes">
            <img src="../images/shirts/smallroundedcuffthreebuttonsandthreebuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5416 Small Rounded Cuff..</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5420:large_rounded_cuff_onebutton_and_one_button_hole"
             title="Large Rounded Cuff One Button & One Button Hole"
             id="cuff_largeroundedcuffonebuttonandonebuttonhole">
            <img src="../images/shirts/largeroundedcuffonebuttonandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5420 Large Rounded Cuff..</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5421:large_rounded_cuff_two_buttons_and_one_button_hole"
             title="Large Rounded Cuff Two Buttons & One Button Hole"
             id="cuff_largeroundedcufftwobuttonsandonebuttonhole">
            <img src="../images/shirts/largeroundedcufftwobuttonsandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5421 Large Rounded Cuff Two Buttons...</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5422:large_rounded_cuff_two_buttons_and_two_button_holes"
             title="Large Rounded Cuff Two Buttons & Two Button Holes"
             id="cuff_largeroundedcufftwobuttonsandtwobuttonholes">
            <img src="../images/shirts/largeroundedcufftwobuttonsandtwobuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5422 Large Rounded Cuff Two Buttons...</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5423:large_rounded_cuff_three_buttons_and_three_button_holes"
             title="Large Rounded Cuff Three Buttons & Three Button Holes"
             id="cuff_largeroundedcuffthreebuttonsandthreebuttonholes">
            <img src="../images/shirts/largeroundedcuffthreebuttonsandthreebuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5423 Large Rounded Cuff Three Buttons...</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5400:square_cuff_one_button_and_one_button_hole"
             title="Square Cuff One Button & One Button Hole" id="cuff_squarecuffonebuttonandonebuttonhole">
            <img src="../images/shirts/squarecuffonebuttonandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5400 Squared Cuff One Button..</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="S-5401:square_cuff_two_buttons_and_one_button_hole"
             title="S-5401 Square cuff two buttons and one button hole"
             id="cuff_S-5401squarecufftwobuttonsandonebuttonhole">
            <img src="../images/shirts/squarecuffonebuttonandonebuttonhole.png" class="backbtnsugg"/>
            <p class="suggestionname">S-5401 Square cuff two buttons..</p>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">CUFF OPTIONS I</p>
            <div class="primarysuggestion backbtnsuggparentdiv" name="53A3:french_inverted_arc_cuff_two_buttons"
                 id="cuff_53A3frenchinvertedarccufftwobuttons" title="French inverted arc cuff two buttons">
                <img src="../images/shirts/53A3frenchinvertedarccufftwobuttons.png" class="backbtnsugg"/>
                <p class="suggestionname">53A3</br>French inverted..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="59B0:french_cuff_with_tab_five_button_holes_cuff_top_stitch_0.3cm"
                 id="cuff_59B0frenchcuffwithtabfivebuttonholescufftopstitch03cm"
                 title="French cuff with tab five button holes cuff top stitch 0.3cm">
                <img src="../images/shirts/59B0frenchcuffwithtabfivebuttonholescufftopstitch03cm.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">59B0</br>French cuff with..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="545B:semi_circle_french_cuff_two_buttons_and_two_button_holes"
                 id="cuff_545bsemicirclefrenchcufftwobuttonsandtwobuttonholes"
                 title="Semi Circle french cuff two buttons and two button holes">
                <img src="../images/shirts/545bsemicirclefrenchcufftwobuttonsandtwobuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">545B</br>Semi Circle french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="545C:single_semi_circle_french_cuff_two_buttons_and_two_button_holes"
                 id="cuff_545Csinglesemicirclefrenchcufftwobuttonsandtwobuttonholes"
                 title="Single Semi circle french cuff two buttons and two button holes">
                <img src="../images/shirts/545Csinglesemicirclefrenchcufftwobuttonsandtwobuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">545C</br>Single Semi circle.</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="545P:trapezoid_french_cuff_with_one_button_and_button_hole"
                 id="cuff_545Ptrapezoidfrenchcuffwith1buttonandbuttonhole"
                 title="Trape zoid french cuff with 1 button and button hole">
                <img src="../images/shirts/545Ptrapezoidfrenchcuffwith1buttonandbuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">545P</br>Trape zoid french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="546Z:semi_circle_french_cuff_three_buttons_and_three_button_holes"
                 id="cuff_546Zsemicirclefrenchcuffthreebuttonsandthreebuttonholes"
                 title="Semi circle french cuff three buttons and three button holes">
                <img src="../images/shirts/546Zsemicirclefrenchcuffthreebuttonsandthreebuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">546Z</br>Semi circle french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5453:traperzoid_french_cuff_two_buttons_and_two_button_holes"
                 id="cuff_5453traperzoidfrenchcufftwobuttonsandtwobuttonholes"
                 title="Traper zoid french cuff two buttons and two button holes">
                <img src="../images/shirts/5453traperzoidfrenchcufftwobuttonsandtwobuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">5453</br>Traper zoid french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5454:triangle_french_cuff_four_button_holes"
                 id="cuff_5454trianglefrenchcufffourbuttonholes" title="Triangle french cuff four button holes">
                <img src="../images/shirts/5454trianglefrenchcufffourbuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">5454</br>Triangle french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5455:square_french_cuff_four_button_holes"
                 id="cuff_5455squarefrenchcufffourbuttonholes" title="Square french cuff four button holes">
                <img src="../images/shirts/5455squarefrenchcufffourbuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">5455</br>Square french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5456:large_arc_french_cuff_four_button_holes"
                 id="cuff_5456largearcfrenchcufffourbuttonholes" title="Large arc french cuff four button holes">
                <img src="../images/shirts/5456largearcfrenchcufffourbuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">5456</br>Large arc french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5457:french_cuff_with_tab_five_button_holes_cuff_top_stitch_0.3cm"
                 id="cuff_5457frenchcuffwithtabfivebuttonholescufftopstitch03cm"
                 title="French cuff with tab five button holes cuff top stitch 0.3cm">
                <img src="../images/shirts/5457frenchcuffwithtabfivebuttonholescufftopstitch03cm.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">5457</br>French cuff with..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5467:french_square_cuff_with_six_botton_holes"
                 id="cuff_5467frenchsquarecuffwith6bottonholes" title="French square cuff with 6 botton holes">
                <img src="../images/shirts/5467frenchsquarecuffwith6bottonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">5467</br>French square..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5783:rounded_cuff_with_two_buttons_and_button_hole_two_buttons_on_inner_sleeve_placket"
                 id="cuff_5783roundedcuffwith2buttonsandbuttonhole2buttonsoninnersleeveplacket"
                 title="Rounded cuff with 2 buttons and button hole 2 buttons on inner sleeve placket">
                <img src="../images/shirts/5783roundedcuffwith2buttonsandbuttonhole2buttonsoninnersleeveplacket.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">5783</br>Rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-545Y:french_cuff_with_tab_one_buttons_and_three_button_holes"
                 id="cuff_S-545Yfrenchcuffwithtabonebuttonsandthreebuttonholes"
                 title="French cuff with tab one buttons and three button holes">
                <img src="../images/shirts/S-545Yfrenchcuffwithtabonebuttonsandthreebuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-545Y</br>French cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-540B:hexagon_cuff_one_button_and_two_button_holes"
                 id="cuff_S-540Bhexagoncuffonebuttonandtwobuttonholes"
                 title="Hexagon cuff one button and two button holes">
                <img src="../images/shirts/S-540Bhexagoncuffonebuttonandtwobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-540B</br>Hexagon cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5451:square_cuff_with_tap_on_sleeve_placket_outside_one_button_and_button_hole"
                 id="cuff_S-545Isquarecuffwithtaponsleeveplacketoutside1buttonandbuttonhole"
                 title="square cuff with tap on sleeve placket out side 1 button and button hole">
                <img src="../images/shirts/S-545Isquarecuffwithtaponsleeveplacketoutside1buttonandbuttonhole.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-5451</br>square cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-545I:square_cuff_w_tab_one_button_and_one_button_holes"
                 id="cuff_S-545Isquarecuffwtabonebuttonandonebuttonholes"
                 title="Square cuff w tab one button and one button holes">
                <img src="../images/shirts/S-545Isquarecuffwtabonebuttonandonebuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-545I</br>Square cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-545N:square_cuff_with_four_button_holes_and_two_buttons"
                 id="cuff_S-545Nsquarecuffwith4buttonholesand2buttons"
                 title="Square cuff with 4 button holes and 2 buttons">
                <img src="../images/shirts/S-545Nsquarecuffwith4buttonholesand2buttons.png" class="backbtnsugg"/>
                <p class="suggestionname">S-545N</br>Square cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5403:square_cuff_three_buttons_and_three_button_holes"
                 id="cuff_S-5403squarecuffthreebuttonsandthreebuttonholes"
                 title="Square cuff three buttons and three button holes">
                <img src="../images/shirts/S-5403squarecuffthreebuttonsandthreebuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5403</br>Square cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5406:Single_square_french_cuff_two_button_holes"
                 id="cuff_S-5406Singlesquarefrenchcufftwobuttonholes"
                 title="Single square french cuff two button holes">
                <img src="../images/shirts/S-5406Singlesquarefrenchcufftwobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5406</br>Single square..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5411:rounded_cuff_two_buttons_and_one_button_hole"
                 id="cuff_S-5411roundedcufftwobuttonsandonebuttonhole"
                 title="Rounded cuff two buttons and one button hole">
                <img src="../images/shirts/S-5411roundedcufftwobuttonsandonebuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5411</br>Rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5412:rounded_cuff_with_two_buttons_and_button_hole"
                 id="cuff_S-5412roundedcuffwith2buttonsandbuttonhole"
                 title="Rounded cuff with 2 buttons and button hole">
                <img src="../images/shirts/S-5412roundedcuffwith2buttonsandbuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5412</br>Rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5441:pointed_cuff_one_button_and_one_button_hole"
                 id="cuff_S-5441pointedcuffonebuttonandonebuttonhole"
                 title="Pointed cuff one button and one button hole">
                <img src="../images/shirts/S-5441pointedcuffonebuttonandonebuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5441</br>Pointed cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5786:hexagon_cuff_width_two_buttons_and_button_hole_two_buttons_on_inner_sleeve_placket"
                 id="cuff_5786hexagoncuffwidth2buttonsandbuttonhole2buttonsoninnersleeveplacket"
                 title="Hexagon cuff width 2 buttons and button hole 2 buttons on inner sleeve placket">
                <img src="../images/shirts/S-5441pointedcuffonebuttonandonebuttonhole.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">5786</br>Hexagon cuff..</p>
            </div>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">CUFF OPTIONS II</p>
            <div class="primarysuggestion backbtnsuggparentdiv" name="53A1:arc_shaped_french_cuff_with_one_button_and_button_hole"
                 id="cuff_53A1arcshapedfrenchcuffwith1buttonandbuttonhole"
                 title="Arc shaped french cuff with 1 button & buttonhole">
                <img src="../images/shirts/53A1arcshapedfrenchcuffwith1button&buttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">53A1</br>Arc shaped french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="58A0:double_hexagon_french_cuff_with_four_button_holes_on_1/3_position_after_folded"
                 id="cuff_58A0doublehexagonfrenchcuffwith4buttonholeson13positionafterfolded"
                 title="Double hexagon french cuff with 4 button holes on 1/3 position after folded">
                <img src="../images/shirts/58A0doublehexagonfrenchcuffwith4buttonholeson13positionafterfolded.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">58A0</br>Double hexagon..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="59A0:french_cuff_with_tab_five_button_holes_cuff_top_stitch_0.6cm"
                 id="cuff_59A0frenchcuffwithtabfivebuttonholescufftopstitch06cm"
                 title="French cuff with tab five button holes cuff top stitch 0.6cm">
                <img src="../images/shirts/59A0frenchcuffwithtabfivebuttonholescufftopstitch06cm.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">59A0</br>French cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="545D:square_french_cuff_four_button_holes_button_hole_is_on_1/3_part_of_the_cuffs"
                 id="cuff_545Dsquarefrenchcufffourbuttonholesbuttonholeison13partofthecuffs"
                 title="Square french cuff four button holes button hole is on 1/3 part of the cuffs">
                <img src="../images/shirts/545Dsquarefrenchcufffourbuttonholesbuttonholeison13partofthecuffs.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">545D</br>Square french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="545K:trapezoid_french_cuff_with_three_buttons_and_button_hole"
                 id="cuff_545Ktrapezoidfrenchcuffwith3buttonsandbuttonhole"
                 title="Trape zoid french cuff with 3 buttons & button hole">
                <img src="../images/shirts/545Ktrapezoidfrenchcuffwith3buttons&buttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">545K</br>Trape zoid french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="545T:double_hexagon_french_cuff_with_four_button_holes" id="cuff_545Troundedfrenchcuff"
                 title="">
                <img src="../images/shirts/545Troundedfrenchcuff.png" class="backbtnsugg"/>
                <p class="suggestionname">545T</br>Rounded french cuff</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5459:french_cuff_with_two_layers_four_button_holes_on_the_1/3_position_after_folded"
                 id="cuff_5459doublehexagonfrenchcuffwith4buttonholes"
                 title="Double hexagon french cuff with 4 button holes">
                <img src="../images/shirts/5459doublehexagonfrenchcuffwith4buttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">5459</br>French cuff Double..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5578:rounded_cuff_one_button_and_two_button_holes_with_button_inside_cuff"
                 id="cuff_5578frenchcuffwith2layers4buttonholesonthe13positionafterfolded"
                 title="French cuff with 2 layers 4 button holes on the 1/3 position after folded">
                <img src="../images/shirts/5578frenchcuffwith2layers4buttonholesonthe13positionafterfolded.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">5578</br>French cuff with..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-545M:square_cuff_one_button_and_one_button_hole"
                 id="cuff_S-545Mroundedcuffonebuttonandtwobuttonholeswithbuttoninsidecuff"
                 title="Rounded cuff one button & two button holes with button inside cuff">
                <img src="../images/shirts/S-545Mroundedcuffonebutton&twobuttonholeswithbuttoninsidecuff.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-545M</br>Rounded cuff one..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5400:square_cuff_two_buttons_and_two_button_holes"
                 id="cuff_S-5400squarecuffonebuttonandonebuttonhole"
                 title="Square cuff one button & one button hole">
                <img src="../images/shirts/S-5400squarecuffonebutton&onebuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5400</br>Square cuff one..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5402:single_square_french_cuff_two_button_holes"
                 id="cuff_S-5402squarecufftwobuttonsandtwobuttonholes"
                 title="Square cuff two buttons & two button holes">
                <img src="../images/shirts/S-5402squarecufftwobuttons&twobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5402</br>Square cuff two..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5408:single_rounded_french_cuff_two_button_holes"
                 id="cuff_S-5408singlesquarefrenchcufftwobuttonholes"
                 title="Single square french cuff two button holes">
                <img src="../images/shirts/S-5408singlesquarefrenchcufftwobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5408</br>Single square french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5409:small_rounded_cuff_one_button_and_one_button_hole"
                 id="cuff_S-5409singleroundedfrenchcufftwobuttonholes"
                 title="Single rounded french cuff two button holes">
                <img src="../images/shirts/S-5409singleroundedfrenchcufftwobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5409</br>Single rounded french..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5414:small_rounded_cuff_two_buttons_and_two_button_holes"
                 id="cuff_S-5414smallroundedcuffonebuttonandonebuttonhole"
                 title="Small rounded cuff one button & one button hole">
                <img src="../images/shirts/S-5414smallroundedcuffonebutton&onebuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5414</br>Small rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5415:small_rounded_cuff_three_buttons_and_three_button_holes"
                 id="cuff_S-5415smallroundedcufftwobuttonsandtwobuttonholes"
                 title="small rounded cuff two buttons & two buttonholes">
                <img src="../images/shirts/S-5415smallroundedcufftwobuttons&twobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5415</br>small rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5416:large_rounded_cuff_three_buttons_and_three_button_holes"
                 id="cuff_S-5416smallroundedcuffthreebuttonsandthreebuttonholes"
                 title="small rounded cuff three buttons & three button holes">
                <img src="../images/shirts/S-5416smallroundedcuffthreebuttons&threebuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-5416</br>small rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5423:hexagon_cuff_one_button_and_one_button_hole"
                 id="cuff_S-5423largeroundedcuffthreebuttonsandthreebuttonholes"
                 title="Large rounded cuff three buttons & three button holes">
                <img src="../images/shirts/S-5423largeroundedcuffthreebuttons&threebuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-5423</br>Large rounded cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5430:hexagon_cuff_two_buttons_and_one_button_holes"
                 id="cuff_S-5430hexagoncuffonebuttonandonebuttonhole"
                 title="Hexagon cuff one button & one button hole">
                <img src="../images/shirts/S-5423largeroundedcuffthreebuttons&threebuttonholes.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-5430</br>Hexagon cuff one..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5431:hexagon_cuff_two_buttons_and_two_button_holes"
                 id="cuff_S-5431hexagoncufftwobuttonsandonebuttonholes"
                 title="Hexagon cuff two buttons & one button holes">
                <img src="../images/shirts/S-5431hexagoncufftwobuttons&onebuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5431</br>Hexagon cuff two..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5432:hexagon_cuff_three_buttons_and_three_button_holes"
                 id="cuff_S-5432hexagoncufftwobuttonsandtwobuttonholes"
                 title="hexagon cuff two buttons & two button holes">
                <img src="../images/shirts/S-5432hexagoncufftwobuttons&twobuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5432</br>Hexagon cuff two..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5433:large_hexagon_cuff_one_button_and_button_hole"
                 id="cuff_S-5433hexagoncuffthreebuttonsandthreebuttonholes"
                 title="Hexagon cuff three buttons & three button holes">
                <img src="../images/shirts/S-5433hexagoncuffthreebuttons&threebuttonholes.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5433</br>Hexagon cuff three..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5434:square_french_cuff_with_three_buttons_holes_tap_on_the_outside_sleeve_placket"
                 id="cuff_S-5434largehexagoncuffonebuttonandbuttonhole"
                 title="Large hexagon cuff one button & button hole">
                <img src="../images/shirts/S-5434largehexagoncuffonebutton&buttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">S-5434</br>Large hexagon cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="S-5450:small_arc_french_cuff_four_button_holes"
                 id="cuff_S-5450squarefrenchcuffwith3buttonholestapontheoutsidesleeveplacket"
                 title="Square french cuff with 3 button holes tap on the outside sleeve placket">
                <img src="../images/shirts/S-5450squarefrenchcuffwith3buttonholestapontheoutsidesleeveplacket.png"
                     class="backbtnsugg"/>
                <p class="suggestionname">S-5450</br>Square french cuff..</p>
            </div>

            <div class="primarysuggestion backbtnsuggparentdiv" name="5458:small_arc_french_cuff_four_button_holes"
                 id="cuff_5458smallarcfrenchcufffourbuttonholes" title="small arc french cuff four button holes">
                <img src="../images/shirts/S-5450squarefrenchcuffwith3buttonholestapontheoutsidesleeveplacket.png" class="backbtnsugg"/>
                <p class="suggestionname">5458</br>small arc french cuff..</p>
            </div>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="cuffwidths" style="display:none">
        <p class="stylecaption">Cuff Width Style</p>
        <div class="primarysuggestion backbtnsuggparentdiv active" name="cuff_width_6.5cm_-_7.5cm"
             id="cuffwidth_cuffwidth6575">
            <img src="../images/shirts/cuffwidth6575.png" class="backbtnsugg"/>
            <p class="suggestionname">Cuff Width 6.5 - 7.5cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="cuff_width_6.6cm_-_7.6cm" id="cuffwidth_cuffwidth6676">
            <img src="../images/shirts/cuffwidth6676.png" class="backbtnsugg"/>
            <p class="suggestionname">Cuff Width 6.6 - 7.6cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="cuff_width_6.7cm" id="cuffwidth_cuffwidth67">
            <img src="../images/shirts/cuffwidth67.png" class="backbtnsugg"/>
            <p class="suggestionname">Cuff Width 6.7cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="cuff_width_7cm" id="cuffwidth_cuffwidth7cm">
            <img src="../images/shirts/cuffwidth7cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Cuff Width 7.0cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="cuff_width_7.5cm" id="cuffwidth_cuffwidth75cm">
            <img src="../images/shirts/cuffwidth75cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Cuff Width 7.5cm</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv " name="cuff_width_8cm" id="cuffwidth_cuffwidth8cm">
            <img src="../images/shirts/cuffwidth8cm.png" class="backbtnsugg"/>
            <p class="suggestionname">Cuff Width 8.0cm</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv overflow-cmd" id="plackets" style="display:none">
        <p class="stylecaption">Front Placket Style</p>
        <div class="primarysuggestion active" style="" name="regular" id="placket_regular">
            <img src="../images/shirts/regular.png"/>
            <p class="suggestionname">Regular Placket</p>
        </div>
        <div class="primarysuggestion" name="french" id="placket_french">
            <img src="../images/shirts/french.png"/>
            <p class="suggestionname">French Placket</p>
        </div>
        <div class="primarysuggestion" name="concealed" id="placket_concealed">
            <img src="../images/shirts/concealed.png"/>
            <p class="suggestionname">Concealed Placket</p>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">PLACKET STYLE I</p>
            <div class="primarysuggestion" name="520Q:left_and_right_placket" id="placket_leftrightplacketfront">
                <img src="../images/shirts/leftrightplacketfront.png"/>
                <p class="suggestionname">520Q Left and </br>Right Placket Front</p>
            </div>
            <div class="primarysuggestion" name="5102:un-functional" id="placket_unfunctional">
                <img src="../images/shirts/unfunctional.png"/>
                <p class="suggestionname">5102 Un-Functional </br>Placket Front</p>
            </div>
            <div class="primarysuggestion" name="520D:gripping" id="placket_gripping">
                <img src="../images/shirts/gripping.png"/>
                <p class="suggestionname">520D Gripping </br>hidden button front</p>
            </div>
            <div class="primarysuggestion" name="5103:hidden_button_front" id="placket_hiddenbuttonfront">
                <img src="../images/shirts/hiddenbuttonfront.png"/>
                <p class="suggestionname">5103</br>hidden button front</p>
            </div>
            <div class="primarysuggestion" name="518B:left_and_right_plain_front" id="placket_leftandrighplainfront">
                <img src="../images/shirts/leftandrighplainfront.png"/>
                <p class="suggestionname">518B Left and Right</br>Plain Front</p>
            </div>
            <div class="primarysuggestion" name="5101:standard" id="placket_5101placket">
                <img src="../images/shirts/5101placket.png"/>
                <p class="suggestionname">5101 </br>Placket(Standard)</p>
            </div>
            <div class="primarysuggestion" name="531D:right_cover_left" id="placket_rihgtcoverleft">
                <img src="../images/shirts/rihgtcoverleft.png"/>
                <p class="suggestionname">531D Right Cover Left</p>
            </div>
        </div>

        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">PLACKET STYLE II</p>
            <div class="primarysuggestion" name="5109:top_five_buttons_part_are_plain_front_there_stis_hidden_button_front"
                 id="placket_5109topfivebuttonspartareplainfronttherestishiddenbuttonfront"
                 title="5109 top five buttons part are plain front there stis hidden button front">
                <img src="../images/shirts/5109topfivebuttonspartareplainfronttherestishiddenbuttonfront.png"/>
                <p class="suggestionname">5109 </br>Top Five Buttons</p>
            </div>

            <div class="primarysuggestion" name="5118" id="placket_5118:tuxedo_front_42.5cm_pleat_for_each_side"
                 title="5118 tuxedo front 42.5cm pleat foreach side ">
                <img src="../images/shirts/5118tuxedofront425cmpleatforeachside.png"/>
                <p class="suggestionname">5118</br>Tuxedo front 42.5cm</p>
            </div>

            <div class="primarysuggestion" name="5119:two_placket_front_w_pleated_patch" id="placket_5119twoplacketfrontwpleatedpatch"
                 title="5119 two placket front w pleated patch">
                <img src="../images/shirts/5119twoplacketfrontwpleatedpatch.png"/>
                <p class="suggestionname">5119</br>Two Placket Front</p>
            </div>

            <div class="primarysuggestion" name="S-513A:front_with_triangle_patched_fabrics_0.1cm_top_stitch"
                 id="placket_S-513Afrontwithtrianglepatchedfabrics01cmtopstitch"
                 title="front with triangle patched fabrics 0.1cm top stitch">
                <img src="../images/shirts/S-513Afrontwithtrianglepatchedfabrics01cmtopstitch.png"/>
                <p class="suggestionname">S-513A</br>Front with triangle</p>
            </div>

            <div class="primarysuggestion" name="S-520S:tuxedo_front_ten_0.7cm_pleat_on_each_side_to_bottom"
                 id="placket_S-520Stuxedofrontten07cmpleatoneachsidetobottom"
                 title="Tuxedo Front Ten 0.7 cm pleat on each side to bottom">
                <img src="../images/shirts/S-520Stuxedofrontten07cmpleatoneachsidetobottom.png"/>
                <p class="suggestionname">S-520S</br>Tuxedo Front Ten..</p>
            </div>

            <div class="primarysuggestion" name="S-530A:accordion_pleats_front_with_pleated_patch" id="placket_S-530Aaccordionpleatsfrontwithpleatedpatch"
                 title="Accordion Pleats front with pleated patch">
                <img src="../images/shirts/S-530aaccordionpleatsfrontwithpleatedpatch.png"/>
                <p class="suggestionname">S-530A</br>Accordion Pleats..</p>
            </div>

            <div class="primarysuggestion" name="S-530W:single_pleat_on_each_side" id="placket_S-530Wsinglepleatoneachside"
                 title="Single Pleat on each side">
                <img src="../images/shirts/S-530Wsinglepleatoneachside.png"/>
                <p class="suggestionname">S-530W</br>Single Pleat..</p>
            </div>

            <div class="primarysuggestion" name="S-530X:normal" id="placket_S-530Xnormal" title="">
                <img src="../images/shirts/S-530Xnormal.png"/>
                <p class="suggestionname">S-530X</br>Normal(standard)</p>
            </div>

            <div class="primarysuggestion" name="S-5110:tuxedo_front_10.07cm_pleat_for_each_side" id="placket_S-5110tuxedofront1007cmpleatforeachside"
                 title="Tuxedo Front 10.07cm pleat foreach side">
                <img src="../images/shirts/S-5110tuxedofront1007cmpleatforeachside.png"/>
                <p class="suggestionname">S-5110</br>Tuxedo Front..</p>
            </div>
        </div>

        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">PLACKET STYLE III</p>
            <div class="primarysuggestion" name="S-5108:tuxedo_patch_left_and_right_patch" id="placket_S-5108tuxedopatchleftandrightpatch"
                 title="Tuxedo Patch left and right patch">
                <img src="../images/shirts/S-5108tuxedopatchleftandrightpatch.png"/>
                <p class="suggestionname">S-5108</br>Tuxedo Patch..</p>
            </div>
        </div>

        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">PLACKET STYLE IV</p>
            <div class="primarysuggestion" name="510G:8_buttons_on_front_including_button_on_collar_band" id="placket_510G8buttonsonfrontincludingbuttononcollarband"
                 title="8 Buttons on front including button on collar band">
                <img src="../images/shirts/510G8buttonsonfrontincludingbuttononcollarband.png"/>
                <p class="suggestionname">510G</br>8 Buttons on front..</p>
            </div>

            <div class="primarysuggestion" name="510H:9_buttons_on_front_including_button_on_collar_band" id="placket_510H9buttonsonfrontincludingbuttononcollarband"
                 title="9 Buttons on front including button on collar band">
                <img src="../images/shirts/510H9buttonsonfrontincludingbuttononcollarband.png"/>
                <p class="suggestionname">510H</br>9 Buttons on front..</p>
            </div>

            <div class="primarysuggestion" name="510P:7_buttons_on_front_including_button_on_collar_band" id="placket_510P7buttonsonfrontincludingbuttononcollarband"
                 title="7 Buttons on front including button on collar band">
                <img src="../images/shirts/510P7buttonsonfrontincludingbuttononcollarband.png"/>
                <p class="suggestionname">510P(standard)</br>7 Buttons on front..</p>
            </div>

            <div class="primarysuggestion" name="510Q:10_buttons_on_front_including_button_on_collar_band" id="placket_510Q10buttonsonfrontincludingbuttononcollarband"
                 title="10 Buttons on front including button on collar band">
                <img src="../images/shirts/510Q10buttonsonfrontincludingbuttononcollarband.png"/>
                <p class="suggestionname">510Q</br>10 Buttons on front..</p>
            </div>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">Piping</p>
            <div class="primarysuggestion" name="530R:0.3_cm_piping_on_placket" id="placket_530R03cmpipingonplacket">
                <img src="../images/shirts/530R03cmpipingonplacket.png"/>
                <p class="suggestionname">530R</br>0.3cm Piping..</p>
            </div>
        </div>
        <div class="col-md-12" id="plackets" style="display:block">
            <p class="stylecaption">Piping Febrics</p>
            <div class="col-md-12" id="mainfabricdiv1" style="height: 400px;overflow-y: scroll;padding:0px">
                <?php
                $query = "select * from wp_colors where type='shirt' and onlyfor = 'shirt' and status = '1'";
                $result = mysql_query($query);
                if ($result) {
                    $num = mysql_num_rows($result);
                    if ($num > 0) {
                        $status = "done";
                        $a = 0;
                        while ($rows = mysql_fetch_array($result)) {
                            $a++;
                            $colorName = $rows['colorName'];
                            ?>
                            <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                                 name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                                 id='placket_<?php echo $colorName ?>'>
                                <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                                <p class="suggestionname"
                                   style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                            </div>
                            <?php
                        }
                    } else {
                        echo "No Color Found";
                    }
                } else {
                    echo mysql_error();
                }
                ?>
            </div>
            <div class="col-md-12" style="padding: 0">
                <div class="form-group col-md-6" style="margin-top:20px;padding:0 20px">
                    <label>Fabric Name</label>
                    <input type="text" class="form-control" value="" id="mainfabricfield1" onkeyup="searchFabricData(this)" placeholder="Enter Fabric Name" />
                </div>
                <div class="form-group col-md-6" style="margin-top:20px">
                    <input type="button" class="btn btn-danger pull-right"  onclick="addmainfabric1()" style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" />
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="placketbuttons" style="display:none">
        <p class="stylecaption">Front Placket Button Quantity</p>
        <div class="primarysuggestion backbtnsuggparentdiv active " name="placket7buttons"
             id="placketbutton_placket7buttons">
            <img src="../images/shirts/placket7buttons.png" class="backbtnsugg"/>
            <p class="suggestionname">Placket 7 Buttons</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="placket7buttonsincludingcollarbandbuttons"
             id="placketbutton_placket7buttonsincludingcollarbandbuttons">
            <img src="../images/shirts/placket7buttonsincludingcollarbandbuttons.png" class="backbtnsugg"/>
            <p class="suggestionname">Placket 7 Buttons Including Collar Band Buttons</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="_placket8buttonsincludingcollartable"
             id="placketbutton_placket8buttonsincludingcollartable">
            <img src="../images/shirts/placket8buttonsincludingcollartable.png" class="backbtnsugg"/>
            <p class="suggestionname">Placket 8 Buttons Including Collar Table</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="placket9buttonsincludingcollarbandbuttons"
             id="placketbutton_placket9buttonsincludingcollarbandbuttons">
            <img src="../images/shirts/placket9buttonsincludingcollarbandbuttons.png" class="backbtnsugg"/>
            <p class="suggestionname">Placket 9 Buttons Including Collar Band Buttons</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="placket10buttonsincludingcollarbandbuttons"
             id="placketbutton_placket10buttonsincludingcollarbandbuttons">
            <img src="../images/shirts/placket10buttonsincludingcollarbandbuttons.png" class="backbtnsugg"/>
            <p class="suggestionname">Placket 10 Buttons Including Collar Band Buttons</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdiv" name="tuxedobuttonswithfourbuttonholes"
             id="placketbutton_tuxedobuttonswithfourbuttonholes">
            <img src="../images/shirts/tuxedobuttonswithfourbuttonholes.png" class="backbtnsugg"/>
            <p class="suggestionname">Tuxedo Buttons With Four Button Holes</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="pockets" style="display:none">
        <p class="stylecaption">Pocket Style</p>
        <div class="primarysuggestion backbtnsuggparentdivs active " name="roundedpocketdecoratepleat"
             id="pocket_roundedpocketdecoratepleat">
            <img src="../images/shirts/roundedpocketdecoratepleat.png"/>
            <p class="suggestionname">Rounded Pocket Decorate Pleat</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdivs" name="roundedpocketstraightpleat"
             id="pocket_roundedpocketstraightpleat">
            <img src="../images/shirts/roundedpocketstraightpleat.png"/>
            <p class="suggestionname">Rounded Pocket Straight Pleat</p>
        </div>
        <div class="primarysuggestion backbtnsuggparentdivs" name="roundedpockettrianglepleat"
             id="pocket_roundedpockettrianglepleat">
            <img src="../images/shirts/roundedpockettrianglepleat.png"/>
            <p class="suggestionname">Rounded Pocket Triangle Pleat</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="grippingroundpocketwithinvertedcenterboxpleat"
             title="Gripping Round Pocket with Inverted Center Box Pleat"
             id="pocket_grippingroundpocketwithinvertedcenterboxpleat">
            <img src="../images/shirts/grippingroundpocketwithinvertedcenterboxpleat.png"/>
            <p class="suggestionname">Gripping Round Pocket..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="hexagonepocketwithtringleedgefold"
             title="Hexagone Pocket with tringle edge fold" id="pocket_hexagonepocketwithtringleedgefold">
            <img src="../images/shirts/hexagonepocketwithtringleedgefold.png"/>
            <p class="suggestionname">Hexagone Pocket with..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="roundedpocketinvertedboxpleat"
             title="Rounded Pocket Inverted Box Pleat" id="pocket_roundedpocketinvertedboxpleat">
            <img src="../images/shirts/roundedpocketinvertedboxpleat.png"/>
            <p class="suggestionname">Rounded Pocket Inverted..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="roundedpocketwithboxpleat"
             title="Rounded Pocket with box Pleat" id="pocket_roundedpocketwithboxpleat">
            <img src="../images/shirts/roundedpocketwithboxpleat.png"/>
            <p class="suggestionname">Rounded Pocket with..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="squarepocketinvertedboxpleat"
             title="Square Pocket Inverted Box Pleat" id="pocket_squarepocketinvertedboxpleat">
            <img src="../images/shirts/squarepocketinvertedboxpleat.png"/>
            <p class="suggestionname">Square Pocket Inverted..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="squarepocketwithpenpocket"
             title="Square Pocket with pen pocket" id="pocket_squarepocketwithpenpocket">
            <img src="../images/shirts/squarepocketwithpenpocket.png"/>
            <p class="suggestionname">Square Pocket with..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="squrehiddenpocketwithdoublebesom"
             title="Square Hidden Pocket with double besom" id="pocket_squrehiddenpocketwithdoublebesom">
            <img src="../images/shirts/squrehiddenpocketwithdoublebesom.png"/>
            <p class="suggestionname">Square Hidden Pocket..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="tringlepocketwighttringleedgefold"
             title="Tringle Pocket Wight tringle edge fold" id="pocket_tringlepocketwighttringleedgefold">
            <img src="../images/shirts/tringlepocketwighttringleedgefold.png"/>
            <p class="suggestionname">Tringle Pocket Wight..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="tringlepocketwithornamentaledgefold"
             title="Tringle Pocket with ornamental edge fold" id="pocket_tringlepocketwithornamentaledgefold">
            <img src="../images/shirts/tringlepocketwithornamentaledgefold.png"/>
            <p class="suggestionname">Tringle Pocket with..</p>
        </div>

        <div class="primarysuggestion backbtnsuggparentdivs" name="tringlepocketwithstraightedgefold"
             title="Tringle Pocket with straight edge fold" id="pocket_tringlepocketwithstraightedgefold">
            <img src="../images/shirts/tringlepocketwithstraightedgefold.png"/>
            <p class="suggestionname">Tringle Pocket with..</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="pleats" style="display:none">
        <p class="stylecaption">Pleat Style</p>

        <div class="primarysuggestion active" name="S-5140:no_pleat_and_no_dart" id="pleat_nopleatandnodart">
            <img src="../images/shirts/nopleatandnodart.png"/>
            <p class="suggestionname">S-5140</br>No Pleat No Dart</p>
        </div>
        <div class="primarysuggestion" name="S-5143:box_pleats" id="pleat_boxpleats">
            <img src="../images/shirts/boxpleats.png"/>
            <p class="suggestionname">S-5143</br>Box Pleats</p>
        </div>
        <div class="primarysuggestion" name="S-5141:side_pleats" id="pleat_sidepleats">
            <img src="../images/shirts/sidepleats.png"/>
            <p class="suggestionname">S-5141</br>Side Pleat(standard)</p>
        </div>

        <div class="primarysuggestion" name="5782:western_yoke" id="pleat_5782westernyoke">
            <img src="../images/shirts/5782westernyoke.png"/>
            <p class="suggestionname">5782</br>Western Yoke</p>
        </div>

        <div class="primarysuggestion" name="S-5143:box_pleats" id="pleat_S-5143boxpleats">
            <img src="../images/shirts/S-5143boxpleats.png"/>
            <p class="suggestionname">S-5143</br>Box Pleats</p>
        </div>

        <div class="primarysuggestion" name="S-5144:no_pleat_with_normal_dart" id="pleat_S-5144nopleatwithnormaldart">
            <img src="../images/shirts/S-5144nopleatwithnormaldart.png"/>
            <p class="suggestionname">S-5144</br>No Pleat with Normal..</p>
        </div>

        <div class="primarysuggestion" name="S-5142:inverted_box_pleat" id="pleat_s-5142invertedboxpleat">
            <img src="../images/shirts/s-5142invertedboxpleat.png"/>
            <p class="suggestionname">S-5142</br>Inverted Box Pleat</p>
        </div>

        <div class="primarysuggestion" name="S-5145:box_pleats_to_bottom" id="pleat_S-5145boxpleatstobottom">
            <img src="../images/shirts/S-5145boxpleatstobottom.png"/>
            <p class="suggestionname">S-5145</br>Box Pleats to Bottom</p>
        </div>

        <div class="primarysuggestion" name="S-5148:shoulder_pleat_double" id="pleat_S-5148shoulderpleatdouble">
            <img src="../images/shirts/S-5148shoulderpleatdouble.png"/>
            <p class="suggestionname">S-5148</br>Shoulder Pleat Double</p>
        </div>

        <div class="primarysuggestion" name="S-5149:triangle_yoke" id="pleat_S-5149triangleyoke">
            <img src="../images/shirts/S-5149triangleyoke.png"/>
            <p class="suggestionname">S-5149</br>Triangle Yoke</p>
        </div>

        <div class="primarysuggestion" name="S-5170:normal_yoke" id="pleat_S-5170normalyoke">
            <img src="../images/shirts/S-5170normalyoke.png"/>
            <p class="suggestionname">S-5170</br>Normal Yoke</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="bottoms" style="display:none">
        <p class="stylecaption">Bottom Style</p>
        <div class="primarysuggestion active" name="S-5120:normal_rounded_bottom" id="bottom_normalroundedbottom">
            <img src="../images/shirts/normalroundedbottom.png"/>
            <p class="suggestionname">S-5120</br>Normal Rounded Bottom</p>
        </div>
        <div class="primarysuggestion" name="S-5130:normal_square_bottom" id="bottom_normalsquarebottom">
            <img src="../images/shirts/normalsquarebottom.png"/>
            <p class="suggestionname">S-5130</br>Normal Square Bottom</p>
        </div>
        <div class="primarysuggestion" name="S-5131:straight_side_vents_bottom" id="bottom_straightsideventsbottom">
            <img src="../images/shirts/straightsideventsbottom.png"/>
            <p class="suggestionname">S-5131</br>Straight Side Vents..</p>
        </div>




        <div class="col-md-12">
            <p class="stylecaption">Gusset</p>
            <div class="primarysuggestion" name="5133:hexagon_gusset" id="bottom_5133hexagongusset">
                <img src="../images/shirts/5133hexagongusset.png"/>
                <p class="suggestionname">5133</br>Hexagon Gusset</p>
            </div>

            <div class="primarysuggestion" name="5134:triangle_gusset" id="bottom_5134trianglegusset">
                <img src="../images/shirts/5134trianglegusset.png"/>
                <p class="suggestionname">5134</br>Triangle Gusset</p>
            </div>

            <div class="primarysuggestion" name="5135:pentagon_gusset" id="bottom_5135pentagongusset">
                <img src="../images/shirts/5135pentagongusset.png"/>
                <p class="suggestionname">5135</br>Pentagon Gusset</p>
            </div>

            <div class="primarysuggestion" name="5553:bottom_gusset_with_contrast_fabric" id="bottom_5553bottomgussetwithcontrastfebric"
                 title="Bottom Gusset with Contrast Febric">
                <img src="../images/shirts/5553bottomgussetwithcontrastfebric.png"/>
                <p class="suggestionname">5553</br>Bottom Gusset with..</p>
            </div>

            <div class="primarysuggestion" name="S-5132:large_rounded_bottom" id="bottom_S-5132largeroundedbottom">
                <img src="../images/shirts/S-5132largeroundedbottom.png"/>
                <p class="suggestionname">S-5132</br>Large Rounded Bottom</p>
            </div>

            <div class="primarysuggestion" name="no_gusset" id="bottom_no_gusset">
                <img src="../images/shirts/noguesset.png"/>
                <p class="suggestionname"></br>No Gusset</p>
            </div>
            <div class="primarysuggestion" name="5134:traingle_gusset_bottom" id="bottom_5134trainglegussetbottom">
                <img src="../images/shirts/S-5134.png"/>
                <p class="suggestionname">5134</br>Traingle Gusset bottom</p>
            </div>
        </div>





        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <div class="col-md-12"  style="display:block">
                <p class="stylecaption">Bottom Fabric's</p>
                <div class="col-md-12" id="mainfabricdiv4" style="height: 400px;overflow-y: scroll;padding:0px">
                    <?php
                    $query = "select * from wp_colors where type='shirt' and onlyfor = 'shirt' and status = '1'";
                    $result = mysql_query($query);
                    if ($result) {
                        $num = mysql_num_rows($result);
                        if ($num > 0) {
                            $status = "done";
                            $a = 0;
                            while ($rows = mysql_fetch_array($result)) {
                                $a++;
                                $colorName = $rows['colorName'];
                                ?>
                                <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                                     name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                                     id='bottomFabric_<?php echo $colorName ?>' onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','bottomFabric')>
                                    <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                                    <p class="suggestionname"
                                       style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                                </div>
                                <?php
                            }
                        } else {
                            echo "No Color Found";
                        }
                    } else {
                        echo mysql_error();
                    }
                    ?>
                </div>

                <div class="col-md-12" style="padding: 0">
                    <div class="form-group col-md-6" style="margin-top:20px;padding:0 20px">
                        <label>Fabric Name</label>
                        <div contenteditable class="form-control" value="" id="bottomFabric" onkeyup="searchFabricData(this,'bottomFabric')" ></div>
                        <ul class="list-unstyled data_ data-remove" id="list_data3" style="background: #fff">
                        </ul>
                    </div>
                </div>
                <div class="form-group col-md-6" style="margin-top:20px">
                    <div type="button" class="btn btn-danger pull-right FabricBtnbottomFabric"  onclick="addmainfabric4()" style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric3"
                         disabled > Find Fabric </div>
                </div>


                <div class="row">
                    <label class="surplus_price_label" id="surplus_price_labelbottomFabric" style="display: none">
                        Surplus Price :
                        <span class="surplus_price" id="surplus_pricebottomFabric"></span>
                    </label>
                </div>
            </div>
        </div>

    </div>
    <div class="col-md-12 stepdiv" id="shirtcolors" style="display:none">
        <p class="stylecaption">Shirt Color Style</p>
        <div class="col-md-12" id="mainfabricdiv" style="height: 400px;overflow-y: scroll;padding:0px">
            <?php
            $query = "select * from wp_colors where type='shirt' and onlyfor = 'shirt' and status = '1'";
            $result = mysql_query($query);
            if ($result) {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $status = "done";
                    $a = 0;
                    while ($rows = mysql_fetch_array($result)) {
                        $a++;
                        $colorName = $rows['colorName'];
                        ?>
                        <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                             name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                             id='shirtcolor_<?php echo $colorName ?>' onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','ShirtColor')>
                            <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                            <p class="suggestionname"
                               style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                        </div>
                        <?php
                    }
                } else {
                    echo "No Color Found";
                }
            } else {
                echo mysql_error();
            }
            ?>
        </div>
        <div class="col-md-12" style="padding: 0">
            <div class="form-group col-md-6" style="margin-top:20px;padding:0 20px">
                <label>Fabric Name</label>
                <input type="text" class="form-control" value="" id="mainfabricfield" onkeyup="searchFabricData(this,'ShirtColor')" placeholder="Enter Fabric Name" />
            </div>
            <div class="form-group col-md-6" style="margin-top:20px">
                <input type="button" class="btn btn-danger pull-right FabricBtnShirtColor"  onclick="addmainfabric()"
                       style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" disabled />
            </div>
            <div class="row">
                <label class="surplus_price_label" id="surplus_price_labelShirtColor" style="display: none">
                    Surplus Price :
                    <span class="surplus_price" id="surplus_priceShirtColor"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="col-md-12 overflow-cmd stepdiv" id="buttoncolors" style="display:none ">
    <p class="stylecaption">Button Colors</p>
        <?php
        $query = "select * from button where btn_type='shirt' and status = '1'";
        $result = mysql_query($query);
        if ($result) {
            $num = mysql_num_rows($result);
            if ($num > 0) {
                $status = "done";
                $a = 0;
                while ($rows = mysql_fetch_array($result)) {
                    $a++;
                    $btn_name = $rows['btn_name'];
                    $btn_image = $rows['btn_image'];
                    if($a == 1){

                        echo "<input type='hidden' id='buttoncolorVar' value='".$btn_name."'>";
                    }
                    ?>
                    <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                         name='<?php echo $btn_name ?>' id='buttoncolor_<?php echo $btn_name ?>'
                         style="margin-left:5px;">
                        <img style="width: 100%" src="../admins/api/Files/images/buttons/<?php echo $btn_image; ?>"/>
                        <p class="suggestionname" style="text-transform:uppercase;"><?php echo $btn_name; ?></p>
                    </div>

                    <?php
                }
            } else {
                echo "No Color Found";
            }
        } else {
            echo mysql_error();
        }

        ?>
    </div>
    <div class="col-md-12 stepdiv" id="buttoningstyles" style="display:none">
        <p class="stylecaption">Buttoning Style</p>
        <div class="primarysuggestion active" name="531I:135_degree_front_button_hole" id="buttoningstyle_531I135degreefrontbuttonhole"
             title="135 Degree front button hole">
            <img src="../images/shirts/531I135degreefrontbuttonhole.png"/>
            <p class="suggestionname">531I 135 Degree front..</p>
        </div>
        <div class="primarysuggestion" name="531J:placket_with_45_degree_slanted_button_hole" id="buttoningstyle_531Jplacketwith45degreeslantedbuttonhole"
             title="Placket with 45 degree slanted button hole">
            <img src="../images/shirts/531Jplacketwith45degreeslantedbuttonhole.png"/>
            <p class="suggestionname">531J Placket with 45..</p>
        </div>
        <div class="primarysuggestion" name="collar_band_with_horizontal_button_hole_and_the_front_with_horizontal_button_hole"
             id="buttoningstyle_collarbandwithhorizontalbuttonholeandthefrontwithhorizontalbuttonhole"
             title="collar band with horizontal button hole and the front with horizontal button hole">
            <img src="../images/shirts/collarbandwithhorizontalbuttonholeandthefrontwithhorizontalbuttonhole.png"/>
            <p class="suggestionname">collar band with..</p>
        </div>

        <div class="primarysuggestion"
             name="regular_making_way_button_hole_making_way_collar_band_with_horizontal_button_hole_and_front_with_vertical_button_hole"
             id="buttoningstyle_regularmakingwaybuttonholemakingwaycollarbandwithhorizontalbuttonholeandfrontwithverticalbuttonhole"
             title="Regular making way button hole making way collar band with horizontal button hole and front with vertical button hole">
            <img src="../images/shirts/regularmakingwaybuttonholemakingwaycollarbandwithhorizontalbuttonholeandfrontwithverticalbuttonhole.png"/>
            <p class="suggestionname">Regular making way..</p>
        </div>

        <div class="primarysuggestion"
             name="regular_making_way_button_hole_making_way_collar_band_with_horizontal_button_hole_front_with_vertical_button_hole"
             id="buttoningstyle_regularmakingwaybuttonholemakingwaycollarbandwithhorizontalbuttonholefrontwithverticalbuttonhole"
             title="Regular making way button hole making way collar band with horizontal button hole front with vertical button hole">
            <img src="../images/shirts/regularmakingwaybuttonholemakingwaycollarbandwithhorizontalbuttonholefrontwithverticalbuttonhole.png"/>
            <p class="suggestionname">Regular making way..</p>
        </div>

        <div class="primarysuggestion"
             name="sleeve_placket_with_45_degree_slanted_button_hole_cuff_with_45_degree_slanted_button_hole"
             id="buttoningstyle_sleeveplacketwith45degreeslantedbuttonholecuffwith45degreeslantedbuttonhole"
             title="sleeve placket with 45 degree slanted button hole cuff with 45 degree slanted button hole">
            <img src="../images/shirts/sleeveplacketwith45degreeslantedbuttonholecuffwith45degreeslantedbuttonhole.png"/>
            <p class="suggestionname">sleeve placket with 45..</p>
        </div>

        <div class="primarysuggestion"
             name="sleeve_placket_with_135_degree_slanted_button_hole_cuff_with_135_degree_slanted_button_hole"
             id="buttoningstyle_sleeveplacketwith135degreeslantedbuttonholecuffwith135degreeslantedbuttonhole"
             title="sleeve placket with 135 degree slanted button hole cuff with 135 degree slanted button hole">
            <img src="../images/shirts/sleeveplacketwith135degreeslantedbuttonholecuffwith135degreeslantedbuttonhole.png"/>
            <p class="suggestionname">sleeve placket with 135..</p>
        </div>

        <div class="primarysuggestion" name="sleeve_placket_with_horizontal_button_hole_and_front_with_horizontal_button_hole"
             id="buttoningstyle_sleeveplacketwithhorizontalbuttonholeandfrontwithhorizontalbuttonhole"
             title="sleeve placket with horizontal buttonhole and front with horizontal button hole">
            <img src="../images/shirts/sleeveplacketwithhorizontalbuttonholeandfrontwithhorizontalbuttonhole.png"/>
            <p class="suggestionname">sleeve placket with..</p>
        </div>
    </div>
    <div class="col-md-12 stepdiv overflow-cmd" id="monograms" style="display:none">
        <p class="stylecaption">Shirt Monogram</p>
        <div class="col-md-12 contrastdiv">
            <div class="col-md-4 form-group" style="padding:0">
                <label>Monogram Letters</label>
                <input type="text" style="font-weight:normal" class="form-control" onKeyUp="edValueKeyPress(this.value)" id="monolatters" value="" />
            </div>
            <div class="col-md-1"></div>
            <div class="col-md-4 form-group" style="padding:0">
                <label>Monogram Output</label>
                <br>
                <span id="lettertype"></span>
            </div>
        </div>
        <div class="col-md-12 contrastdiv">
            <label>Monogram Font</label><br><br>
            <label style="font-family:ZapfChancery;font-size:18px;width: 70px;"><input checked type="radio" name="font" value="RC01" class="rccode" id="ZapfChancery"/>&nbsp;RC01</label>
            <label style="font-family:times-roman;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC02" class="rccode" id="times-roman"/> RC02</label>
            <label style="font-family:MicrosoftYaHeiBold;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC03" class="rccode" id="MicrosoftYaHeiBold"/> RC03</label>
            <label style="font-family:commercial-script-bt;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC04" class="rccode" id="commercial-script-bt"/>&nbsp;RC04</label>
            <label style="font-family:bradley-hand-itc;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC05" class="rccode" id="bradley-hand-itc"/>&nbsp;RC05</label>
            <label style="font-family:iowan-old-style;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC06" class="rccode" id="iowan-old-style"/>&nbsp;RC06</label>
            <label style="font-family:FangSong;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC07" class="rccode" id="FangSong"/>&nbsp;RC07</label>
            <label style="font-family:old-english-text-mt;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC08" class="rccode" id="old-english-text-mt"/>&nbsp;RC08</label>
            <label style="font-family:Gradl-Regular;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC09" class="rccode" id="Gradl-Regular"/>&nbsp;RC09</label>
            <label style="font-family:Raavi;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC10" class="rccode" id="Raavi"/> RC10</label>
            <label style="font-family:pierre;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC11" class="rccode" id="pierre"/> RC11</label>
            <label style="font-family:bradley-hand-itc;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC12" class="rccode" id="bradley-hand-itc"/> RC12</label>
        </div>
        <div class="col-md-12">
            <hr>
            <p class="contrastheading">Monogram Thread</p>
            <?php
            $query = "select * from wp_colors where type='shirt' and subtype = 'monothread' and status = '1'";
            $result = mysql_query($query);
            if ($result) {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $status = "done";
                    $a = 0;
                    while ($rows = mysql_fetch_array($result)) {
                        $a++;
                        $colorName = $rows['colorName'];
                        $colorCode = $rows['onlyfor'];
                        ?>
                        <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                             name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                             onclick="getColorCode('<?php echo $colorCode; ?>')"
                             id='monogram_<?php echo $colorName ?>'>
                            <input type="hidden" id="colorcode_<?php echo $colorName ?>"
                                   value="<?php echo $colorCode; ?>">
                            <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                        </div>
                        <?php
                    }
                } else {
                    echo "No Color Found";
                }
            } else {
                echo mysql_error();
            }
            ?>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top:30px;">
            <p class="contrastheading">Monogram Positions</p>
            <div class="primarysuggestion active" name="left_bottom_of_shirt_2cm_from_front_and_2cm_from_edge"
                 id="monogramposition_leftbottomofshirt2cmfromfrontand2cmfromedge"
                 title="left bottom of shirt 2cm from front and 2cm from edge">
                <img src="../images/shirts/leftbottomofshirt2cmfromfrontand2cmfromedge.png"/>
                <p class="suggestionname">Left Bottom of Shirt..</p>
            </div>
            <div class="primarysuggestion active" name="left_chest_second_button_level"
                 id="monogramposition_leftchestsecondbuttonlevel" title="Left chest second button level">
                <img src="../images/shirts/leftchestsecondbuttonlevel.png"/>
                <p class="suggestionname">Left chest second..</p>
            </div>
            <div class="primarysuggestion active" name="left_west_monogram" id="monogramposition_leftwestmonogram">
                <img src="../images/shirts/leftwestmonogram.png"/>
                <p class="suggestionname">Left West Monogram</p>
            </div>

            <div class="primarysuggestion active" name="right_chest_second_button_level"
                 id="monogramposition_rightchestsecondbuttonlevel" title="right chest second button level">
                <img src="../images/shirts/rightchestsecondbuttonlevel.png"/>
                <p class="suggestionname">right chest second..</p>
            </div>
            <div class="primarysuggestion active" name="right_waist_monogram"
                 id="monogramposition_rightwaistmonogram">
                <img src="../images/shirts/rightwaistmonogram.png"/>
                <p class="suggestionname">Right waist monogram</p>
            </div>
            <div class="primarysuggestion active" name="left_chest_third_button_level"
                 id="monogramposition_leftchestthirdbuttonlevel" title="Left Chest Third button level">
                <img src="../images/shirts/leftchestthirdbuttonlevel.png"/>
                <p class="suggestionname">Left Chest Third..</p>
            </div>
            <div class="primarysuggestion active" name="left_front_fifth_botton_level_include_collar_band_button"
                 id="monogramposition_leftfrontfifthbottonlevelinludecollarbandbotton"
                 title="Left Front Fifth botton level inlude collar band botton">
                <img src="../images/shirts/leftfrontfifthbottonlevelinludecollarbandbotton.png"/>
                <p class="suggestionname">Left Front Fifth..</p>
            </div>
            <div class="primarysuggestion active" name="left_waist_46cm_vertical_from_shoulder_neck_point"
                 id="monogramposition_leftwaist46cmverticalfromshoulderneckpoint"
                 title="Left Waist 46cm vertical from shoulder neck point">
                <img src="../images/shirts/leftwaist46cmverticalfromshoulderneckpoint.png"/>
                <p class="suggestionname">Left Waist 46cm..</p>
            </div>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top:30px;">
            <p class="contrastheading">Monogram Options</p>
            <div class="primarysuggestion active" name="attach_fabric_inside_yoke"
                 id="monogramoption_attachfabricinsideyoke" title="">
                <img src="../images/shirts/attachfabricinsideyoke.png"/>
                <p class="suggestionname">Attach fabric inside yoke</p>
            </div>
            <div class="primarysuggestion active" name="band_of_left_bicep" id="monogramoption_bandofleftbicep"
                 title="">
                <img src="../images/shirts/bandofleftbicep.png"/>
                <p class="suggestionname">Band of left bicep</p>
            </div>
            <div class="primarysuggestion active" name="band_of_left_short_sleeve_cuff"
                 id="monogramoption_bandofleftshortsleevecuff" title="Band of left short sleeve cuff">
                <img src="../images/shirts/bandofleftshortsleevecuff.png"/>
                <p class="suggestionname">Band of left short..</p>
            </div>
            <div class="primarysuggestion active" name="band_of_short_sleeve_cuff_band_of_short_sleeve_cuff"
                 id="monogramoption_bandofshortsleevecuffbandofshortsleevecuff"
                 title="Band of short sleeve cuff band of short sleeve cuff">
                <img src="../images/shirts/bandofshortsleevecuffbandofshortsleevecuff.png"/>
                <p class="suggestionname">Band of short sleeve..</p>
            </div>
            <div class="primarysuggestion active" name="band_of_under_collar" id="monogramoption_bandofundercollar"
                 title="">
                <img src="../images/shirts/bandofundercollar.png"/>
                <p class="suggestionname">Band of under collar</p>
            </div>
            <div class="primarysuggestion active" name="bottom_of_left_sleeve_gusset_bottom_of_right_sleeve_gusset"
                 id="monogramoption_bottomofleftsleevegussetbottomofrightsleevegusset"
                 title="Bottom of left sleeve gusset bottom of right sleeve gusset">
                <img src="../images/shirts/bottomofleftsleevegussetbottomofrightsleevegusset.png"/>
                <p class="suggestionname">Bottom of left sleeve..</p>
            </div>
            <div class="primarysuggestion active" name="center_of_collar_band" id="monogramoption_centerofcollarband"
                 title="">
                <img src="../images/shirts/centerofcollarband.png"/>
                <p class="suggestionname">Center of collar band</p>
            </div>
            <div class="primarysuggestion active" name="customer_logo_inside_yoke"
                 id="monogramoption_customerlogoinsideyoke" title="">
                <img src="../images/shirts/customerlogoinsideyoke.png"/>
                <p class="suggestionname">Customer logo inside yoke</p>
            </div>
            <div class="primarysuggestion active" name="face_side_of_left_placket_bottom"
                 id="monogramoption_facesideofleftplacketbottom" title="Face side of left placket bottom">
                <img src="../images/shirts/facesideofleftplacketbottom.png"/>
                <p class="suggestionname">Face side of left..</p>
            </div>
            <div class="primarysuggestion" name="face_side_of_right_placket_bottom"
                 id="monogramoption_facesideofrightplacketbottom" title="Face side of right placket bottom">
                <img src="../images/shirts/facesideofrightplacketbottom.png"/>
                <p class="suggestionname">Face side of right..</p>
            </div>
            <div class="primarysuggestion" name="inside_left_cuff" id="monogramoption_insideleftcuff" title="">
                <img src="../images/shirts/insideleftcuff.png"/>
                <p class="suggestionname">Inside left cuff</p>
            </div>
            <div class="primarysuggestion" name="inside_left_cuff_right_vertical"
                 id="monogramoption_insideleftcuffrightvertical" title="Inside left cuff right vertical">
                <img src="../images/shirts/insideleftcuffrightvertical.png"/>
                <p class="suggestionname">Inside left cuff..</p>
            </div>
            <div class="primarysuggestion" name="inside_right_cuff" id="monogramoption_insiderightcuff" title="">
                <img src="../images/shirts/insiderightcuff.png"/>
                <p class="suggestionname">Inside right cuff</p>
            </div>
            <div class="primarysuggestion" name="inside_right_cuff_left_vertical"
                 id="monogramoption_insiderightcuffleftvertical" title="Inside right cuff left vertical">
                <img src="../images/shirts/insiderightcuffleftvertical.png"/>
                <p class="suggestionname">Inside right cuff..</p>
            </div>
            <div class="primarysuggestion" name="insideyoke" id="monogramoption_insideyoke" title="">
                <img src="../images/shirts/insideyoke.png"/>
                <p class="suggestionname">Inside yoke</p>
            </div>
            <div class="primarysuggestion" name="inside_yoke_1cm_under_label"
                 id="monogramoption_insideyoke1cmunderlabel" title="Inside yoke 1cm under label">
                <img src="../images/shirts/insideyoke1cmunderlabel.png"/>
                <p class="suggestionname">Inside yoke 1cm..</p>
            </div>
            <div class="primarysuggestion" name="inside_yoke_laundry_marker"
                 id="monogramoption_insideyokelaundrymarker" title="">
                <img src="../images/shirts/insideyokelaundrymarker.png"/>
                <p class="suggestionname">Inside yoke laundry marker</p>
            </div>
            <div class="primarysuggestion" name="left_cuff" id="monogramoption_leftcuff" title="">
                <img src="../images/shirts/leftcuff.png"/>
                <p class="suggestionname">Left cuff</p>
            </div>
            <div class="primarysuggestion" name="left_gusset_right_gusset" id="monogramoption_leftgussetrightgusset"
                 title="">
                <img src="../images/shirts/leftgussetrightgusset.png"/>
                <p class="suggestionname">Left gusset right gusset</p>
            </div>
            <div class="primarysuggestion" name="left_sleeve_gusset_right_sleeve_gusset"
                 id="monogramoption_leftsleevegussetrightsleevegusset"
                 title="Left sleeve gusset right sleeve gusset">
                <img src="../images/shirts/leftsleevegussetrightsleevegusset.png"/>
                <p class="suggestionname">Left sleeve gusset right..</p>
            </div>
            <div class="primarysuggestion" name="middle_of_collar_leaf" id="monogramoption_middleofcollarleaf"
                 title="Middle of collar leaf">
                <img src="../images/shirts/middleofcollarleaf.png"/>
                <p class="suggestionname">Middle of collar leaf</p>
            </div>
            <div class="primarysuggestion" name="monogram_outside_yoke" id="monogramoption_monogramoutsideyoke"
                 title="">
                <img src="../images/shirts/monogramoutsideyoke.png"/>
                <p class="suggestionname">Monogram out side yoke</p>
            </div>
            <div class="primarysuggestion" name="mono_on_middle_of_left_cuff" id="monogramoption_monoonmiddleofleftcuff"
                 title="">
                <img src="../images/shirts/monoonmiddleofleftcuff.png"/>
                <p class="suggestionname">Mono on middle of left cuff</p>
            </div>
            <div class="primarysuggestion" name="mono_on_middle_of_right_cuff"
                 id="monogramoption_monoonmiddleofrightcuff" title="">
                <img src="../images/shirts/monoonmiddleofrightcuff.png"/>
                <p class="suggestionname">Mono on middle of right cuff</p>
            </div>
            <div class="primarysuggestion" name="name_label_monogram" id="monogramoption_namelabelmonogram" title="">
                <img src="../images/shirts/namelabelmonogram.png"/>
                <p class="suggestionname">Name label monogram</p>
            </div>
            <div class="primarysuggestion" name="pocket_bottom_band" id="monogramoption_pocketbottomband" title="">
                <img src="../images/shirts/pocketbottomband.png"/>
                <p class="suggestionname">Pocket bottom band</p>
            </div>
            <div class="primarysuggestion" name="pocket_center" id="monogramoption_pocketcenter" title="">
                <img src="../images/shirts/pocketcenter.png"/>
                <p class="suggestionname">Pocket center</p>
            </div>
            <div class="primarysuggestion" name="pocket_top_band" id="monogramoption_pockettopband" title="">
                <img src="../images/shirts/pockettopband.png"/>
                <p class="suggestionname">Pocket top band</p>
            </div>
            <div class="primarysuggestion" name="pocket_top_center" id="monogramoption_pockettopcenter" title="">
                <img src="../images/shirts/pockettopcenter.png"/>
                <p class="suggestionname">Pocket top center</p>
            </div>
            <div class="primarysuggestion" name="pocket_top_right" id="monogramoption_pockettopright" title="">
                <img src="../images/shirts/pockettopright.png"/>
                <p class="suggestionname">Pocket top right</p>
            </div>
            <div class="primarysuggestion" name="reversesideofleftplacketbottom"
                 id="monogramoption_reversesideofleftplacketbottom" title="Reverse side of left placket bottom">
                <img src="../images/shirts/reversesideofleftplacketbottom.png"/>
                <p class="suggestionname">Reverse side of left..</p>
            </div>
            <div class="primarysuggestion" name="right_bottom_of_outside_yoke"
                 id="monogramoption_rightbottomofoutsideyoke" title="Right bottom of out side yoke">
                <img src="../images/shirts/rightbottomofoutsideyoke.png"/>
                <p class="suggestionname">Right bottom of out..</p>
            </div>
            <div class="primarysuggestion" name="rightcuff" id="monogramoption_rightcuff" title="">
                <img src="../images/shirts/rightcuff.png"/>
                <p class="suggestionname">Right Cuff</p>
            </div>
            <div class="primarysuggestion" name="right_placket_bottom_5cm_from_edge"
                 id="monogramoption_rightplacketbottom5cmfromedge" title="Right placket bottom 5cm from edge">
                <img src="../images/shirts/rightplacketbottom5cmfromedge.png"/>
                <p class="suggestionname">Right placket bottom..</p>
            </div>
            <div class="primarysuggestion" name="right_placket_laundry_marker_interior"
                 id="monogramoption_rightplacketlaundrymarkerinterior"
                 title="Right placket laundry marker interior">
                <img src="../images/shirts/rightplacketlaundrymarkerinterior.png"/>
                <p class="suggestionname">Right placket laundry..</p>
            </div>
            <div class="primarysuggestion" name="right_sleeve_placket_left_sleeve_placket"
                 id="monogramoption_rightsleeveplacketleftsleeveplacket"
                 title="Right sleeve placket left sleeve placket">
                <img src="../images/shirts/rightsleeveplacketleftsleeveplacket.png"/>
                <p class="suggestionname">Right sleeve placket..</p>
            </div>
            <div class="primarysuggestion" name="top_of_inside_yoke" id="monogramoption_topofinsideyoke" title="">
                <img src="../images/shirts/topofinsideyoke.png"/>
                <p class="suggestionname">Top of inside yoke</p>
            </div>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="contrastfabrics" style="display:none">
        <p class="stylecaption">Shirt Color Style</p>
        <div class="col-md-12 contrastdiv">
            <p class="contrastheading">Contrast Position</p>
            <div class="col-md-4" id="topcollar" onclick="contrastselection('topcollar')">
                <i class="fa fa-square-o" id="topcollaricon"></i>&nbsp;Top Collar
            </div>
            <div class="col-md-4" id="topinnercollar" onclick="contrastselection('topinnercollar')"><i
                        class="fa fa-square-o" id="topinnercollaricon"></i>&nbsp;Top Inner Collar
            </div>
            <div class="col-md-4" id="undercollarleaf" onclick="contrastselection('undercollarleaf')"><i
                        class="fa fa-square-o" id="undercollarleaficon"></i>&nbsp;Under Collar Leaf
            </div>
            <div class="col-md-4" id="collarband" onclick="contrastselection('collarband')"><i
                        class="fa fa-square-o" id="collarbandicon"></i>&nbsp;Collar Band
            </div>
            <div class="col-md-4" id="outercuff" onclick="contrastselection('outercuff')"><i class="fa fa-square-o"
                                                                                             id="outercufficon"></i>&nbsp;Outer
                Cuff
            </div>
            <div class="col-md-4" id="innercuff" onclick="contrastselection('innercuff')"><i class="fa fa-square-o"
                                                                                             id="innercufficon"></i>&nbsp;Inner
                Cuff
            </div>
            <div class="col-md-4" id="rightplackettop" onclick="contrastselection('rightplackettop')"><i
                        class="fa fa-square-o" id="rightplackettopicon"></i>&nbsp;Right Placket Top
            </div>
            <div class="col-md-4" id="rightplacketinner" onclick="contrastselection('rightplacketinner')"><i
                        class="fa fa-square-o" id="rightplacketinnericon"></i>&nbsp;Right Placket Inner
            </div>
            <div class="col-md-4" id="leftplackettop" onclick="contrastselection('leftplackettop')"><i
                        class="fa fa-square-o" id="leftplackettopicon"></i>&nbsp;Left Placket Top
            </div>
        </div>
        <div class="col-md-12" style="height: 400px;overflow-y: scroll">
            <hr>
            <p class="contrastheading">Contrast Fabric</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="contrastfabricdiv">
                <?php
                $query = "select * from wp_colors where type='shirt' and subtype = 'contrast' and status ='1'";
                $result = mysql_query($query);
                if ($result) {
                    $num = mysql_num_rows($result);
                    if ($num > 0) {
                        $status = "done";
                        $a = 0;
                        while ($rows = mysql_fetch_array($result)) {
                            $a++;
                            $colorName = $rows['colorName'];
                            ?>
                            <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                                 name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                                 id='contrastfabric_<?php echo $colorName ?>' onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','ShirtContrast')>
                                <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                                <p class="suggestionname"
                                   style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                            </div>
                            <?php
                        }
                    } else {
                        echo "No Color Found";
                    }
                } else {
                    echo mysql_error();
                }
                ?>
            </div>
            <div class="col-md-12" style="padding: 0">
                <div class="form-group col-md-6" style="margin-top:20px;padding:0 20px">
                    <label>Fabric Name</label>
                    <input type="text" class="form-control" value="" id="contrastfabricfield" onkeyup="searchFabricData(this,'ShirtContrast')" placeholder="Enter Fabric Name" />
                    <ul class="list-unstyled data_ data-remove" id="list_data2" style="background: #fff">
                    </ul>
                </div>
                <div class="form-group col-md-6" style="margin-top:20px">
                    <input type="button" class="btn btn-danger pull-right FabricBtnShirtContrast"
                           onclick="addcontrastfabric()" style="margin-right: 44px;margin-top: 20px;width: 175px;"
                           value="Find Fabric" id="findFabric2" disabled />
                </div>
                <div class="row">
                    <label class="surplus_price_label" id="surplus_price_labelShirtContrast" style="display: none">
                        Surplus Price :
                        <span class="surplus_price" id="surplus_priceShirtContrast"></span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-md-12 contrastdiv" style="margin-top: 5px;">
            <p class="stylecaption">Constrast</p>
            <div class="primarysuggestion" id="contrast_554Ftrimfabriconoutsideyoke" name="554F:trim_fabric_on_outside_yoke"
                 title="Trim fabric on outside yoke">
                <img src="../images/shirts/554Ftrimfabriconoutsideyoke.png"/>
                <p class="suggestionname">554F</br>Trim fabric on..</p>
            </div>

            <div class="primarysuggestion" id="contrast_554Nsleevecontrastfabriccustomerappointedfabric"
                 name="554N:sleeve_contrast_fabric_customer_appointed_fabric" title="Sleeve contrast fabric customer appointed fabric">
                <img src="../images/shirts/554Nsleevecontrastfabriccustomerappointedfabric.png"/>
                <p class="suggestionname">554N</br>Sleeve contrast..</p>
            </div>

            <div class="primarysuggestion" id="contrast_554Qcustomerappointsleevecontrastshortsleeve"
                 name="554Q:customer_appoint_sleeve_contrast_short_sleeve" title="Customer appoint sleeve contrast short sleeve">
                <img src="../images/shirts/554Qcustomerappointsleevecontrastshortsleeve.png"/>
                <p class="suggestionname">554Q</br>Customer appoint..</p>
            </div>

            <div class="primarysuggestion" name="5544:inside_of_yoke" id="contrast_5544insideofyoke" title="">
                <img src="../images/shirts/5544insideofyoke.png"/>
                <p class="suggestionname">5544</br>Inside of yoke</p>
            </div>

            <div class="primarysuggestion" name="5546:small_sleeve_placket" id="contrast_5546smallsleeveplacket" title="">
                <img src="../images/shirts/5546smallsleeveplacket.png"/>
                <p class="suggestionname">5546</br>Small sleeve placket</p>
            </div>

            <div class="primarysuggestion" name="5550:Yoke_inside_patch_L8_W_6cm_fabric_color_appointed"
                 id="contrast_5550YokeinsidepatchL8W6cmfabriccolorappointed"
                 title="Yoke in side patch L8 W 6.cm fabric color appointed">
                <img src="../images/shirts/5550YokeinsidepatchL8W6cmfabriccolorappointed.png"/>
                <p class="suggestionname">5550</br>Yoke in side patch..</p>
            </div>

            <div class="primarysuggestion" name="bigsleeveplacketcontrast"
                 id="contrast_bigsleeveplacketcontrast" title="">
                <img src="../images/shirts/bigsleeveplacketcontrast.png"/>
                <p class="suggestionname">Big sleeve placket contrast</p>
            </div>

        </div>
    </div>
    <div class="col-md-12 stepdiv" id="buttoncutthreads" style="display:none">
        <p class="stylecaption">Button Cut Color Style</p>
        <div class="col-md-12" style="height: 400px;overflow-y: scroll">
            <?php
            $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
            $result = mysql_query($query);
            if ($result) {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $status = "done";
                    $a = 0;
                    while ($rows = mysql_fetch_array($result)) {
                        $a++;
                        $colorName = $rows['colorName'];
                        ?>
                        <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                             name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                             id='buttoncutthread_<?php echo $colorName ?>'>
                            <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                        </div>
                        <?php
                    }
                } else {
                    echo "No Color Found";
                }
            } else {
                echo mysql_error();
            }
            ?>
        </div>
    </div>
    <div class="col-md-12 stepdiv" id="holestichthreads" style="display:none">
        <p class="stylecaption">Button Hole Stich Thread Color</p>
        <div class="col-md-12" style="height: 400px;overflow-y: scroll">
            <?php
            $query = "select * from wp_colors where type='shirt' and subtype='buttonthread' and status = '1'";
            $result = mysql_query($query);
            if ($result) {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $status = "done";
                    $a = 0;
                    while ($rows = mysql_fetch_array($result)) {
                        $a++;
                        $colorName = $rows['colorName'];
                        ?>
                        <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                             name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                             id='holestichthread_<?php echo $colorName ?>'>
                            <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                        </div>
                        <?php
                    }
                } else {
                    echo "No Color Found";
                }
            } else {
                echo mysql_error();
            }
            ?>
            <?php
            $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
            $result = mysql_query($query);
            if ($result) {
                $num = mysql_num_rows($result);
                if ($num > 0) {
                    $status = "done";
                    $a = 0;
                    while ($rows = mysql_fetch_array($result)) {
                        $a++;
                        $colorName = $rows['colorName'];
                        ?>
                        <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                             name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                             id='holestichthread_<?php echo $colorName ?>'>
                            <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                        </div>
                        <?php
                    }
                } else {
                    echo "No Color Found";
                }
            } else {
                echo mysql_error();
            }
            ?>
        </div>
    </div>
    </div>
    <?php
        $query = "select * from categories where cat_type='Shirts' and cat_name='Shirt, 1 piece' order by cat_id ASC";
        $result = mysql_query($query);
        if ($result) {
            $num = mysql_num_rows($result);
            if ($num > 0) {
                while ($rows = mysql_fetch_array($result)) {
                    $product_amount = $rows['cat_price'];
                    ?>
                    <input type="hidden" id="total_amount" value="<?php echo $product_amount; ?>">
                    <?php
                }
            } else {
                echo "";
            }
        } else {
            echo mysql_error();
        }
    ?>
        <div class="col-md-12" style="text-align:center;margin-top: 100px">

        <div id ="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">.col-md-4</div>
                            <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
        <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
    </div>
    </div>
</div>

<?php include "footer.php";?>
<script>
    function setSurplusPrice(colorName,fabricId){
        colorName = decodeURI(colorName);
        var url = "../admin/webserver/getSurplusPrice.php";
        $.post(url, {"type":"Custom Suit","colorName":colorName},function(data){
            var data = JSON.parse(data);
//                $("#surplus_price"+fabricId).html("$"+data.extra_price);
        });
    }
    $(".nav a").removeClass("active");
    var currPage = "";
    var category = "formal";
    var collar = "middlecutawaycollarh";
    var ccollar = "5AL8:Middle_Cut_away_Collar_H";// for cart
    var collarbuttondown = "S-5290:collar_point_with_button";
    var doublelayeroption = "S-5B41:top_collar_with_contrast_color";
    var frontcollar = "frontcollarstandheight25cm";
    var collarbelt = "squared";
    var collarbutton = "collaronebutton";
    var cuff = "roundedcuffonebuttonandonebuttonhole";
    var ccuff = "S-5410:rounded_cuff_one_button_and_one_button_hole"; //for cart
    var cuffwidth = "cuff_width_6.5cm_-_7.5cm";
    var placket = "regular";
    var cplacket = "regular"; ///for cart
    var placketbutton = "placket7buttons";
    var pocket = "roundedpocketdecoratepleat";
    var cpocket = "roundedpocketdecoratepleat"; //for cart
    var pleat = "nopleatandnodart";
    var cpleat = "S-5140:no_pleat_and_no_dart";/// for cart
    var bottom = "normalroundedbottom";
    var cbottom = "S-5120:normal_rounded_bottom"; //for cart
    var shirtcolor = "sda400a";
    var buttoncolor = $("#buttoncolorVar").val();
    var buttoningstyle = "531I:135_degree_front_button_hole";
    var buttoncutthread = "buttoncut1202yellow";
    var holestichthread = "buttoncut1202yellow";
    var monogramtext = "";
    var monogramfont = "Times New Roman";
    var monogramcode = "RC01";
    var monogramcolor = "buttoncut1202yellow";
    var monogramposition = "left_bottom_of_shirt_2cm_from_front_and_2cm_from_edge";
    var monogramoption = "attach_fabric_inside_yoke";
    var contrastfabric = "sak066a";
    var contrast = "554F:trim_fabric_on_outside_yoke";
    var selectionString = ""; //contrastposition
    var sleeve = "longsleeveshirt";
    var shoulder = "shoulder";

    function getPrevValue(prevIndex){
        var result = "";
        switch(prevIndex){
            case "category":
                result=category;
                break;
            case "collar":
                result=collar;
                break;
            case "collarbuttondown":
                result=collarbuttondown;
                break;
            case "doublelayeroption":
                result=doublelayeroption;
                break;
            case "frontcollar":
                result=frontcollar;
                break;
            case "collarbelt":
                result=collarbelt;
                break;
            case "cuff":
                result=cuff;
                break;
            case "cuffwidth":
                result=cuffwidth;
                break;
            case "placket":
                result=placket;
                break;
            case "placketbutton":
                result=placketbutton;
                break;
            case "pocket":
                result=pocket;
                break;
            case "pleat":
                result=pleat;
                break;
            case "bottom":
                result=bottom;
                break;
            case "shirtcolor":
                result=shirtcolor;
                break;
            case "buttoncolor":
                result=buttoncolor;
                break;
            case "buttoningstyle":
                result=buttoningstyle;
                break;
            case "buttoncutthread":
                result=buttoncutthread;
                break;
            case "holestichthread":
                result=holestichthread;
                break;
            case "monogramcolor":
                result=monogramcolor;
                break;
            case "monogramposition":
                result=monogramposition;
                break;
            case "monogramoption":
                result=monogramoption;
                break;
            case "contrastfabric":
                result=contrastfabric;
                break;
            case "contrast":
                result=contrast;
                break;
        }
        return result;
    }
    function showMessages(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").css({"padding-top": "0px"});
        $(".modal-body").html("<div class='row' style='background:#000;'><h4 style='color:#fff;text-align: center" +
            ";padding:10px;font-size: 18px;' >" +
            "Fabric out of stock !!!</h4></div><div class='row'><h4 style='text-align: center;font-size: 18px;margin: 14px 0;'>"
            +message+"</h4></div>" +
            "<div class='row' style='text-align: center'><input type='button' data-dismiss='modal' " +
            "style='padding:6px;margin:6px 0;text-align: center;font-size: 16px;width: 200px;text-transform: uppercase;" +
            "background: #D9534F;border: none;color:#fff;" +
            "' value='ok'" +
            " /></div> ");
        $(".modal-footer").css({"display": "none"});
        $("#modal").modal("show");
    }
    function loadSessionData() {
        $(".loader").fadeIn("slow");
        var prevIndex = $(".stepsactive").attr("id");
        currPage = prevIndex + "s";
        var img = "";

        if (currPage == "categorys") {
            folder = "front";
            if (category == "casual") {
                shirtcolor = "sak069a";
                contrastfabric = "sak069a";
            }
            if (category == "tuxedo") {
                shirtcolor = "sak342a";
                contrastfabric = "sak342a";
            }
            if (category == "formal") {
                shirtcolor = "sda400a";
                contrastfabric = "sda400a";
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "collars") {
            folder = "front";
            $(".surplus_price_label").hide();
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "frontcollars") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "collarbelts") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "cuffs") {
            folder = "side";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + cuff + ".png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + bottom + ".png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "cuffwidths") {
            folder = "side";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + cuff + ".png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + bottom + ".png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "plackets") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "placketbuttons") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "pockets") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "pleats") {
            folder = "back";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pleat + ".png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/bawli.png");
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "bottoms") {
            folder = "side";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + cuff + ".png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + bottom + ".png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "shirtcolors") {
            $(".surplus_price_label").hide();
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "buttoncolors") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "buttoningstyles") {
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "buttonswatchs") {
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "contrastfabrics") {
            $(".surplus_price_label").hide();
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "buttoncutthreads") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "holestichthreads") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currPage == "monograms") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }else{
            $(".loader").fadeOut("slow");
        }

        var item = getPrevValue(prevIndex);
        item = prevIndex + "_" + item;

        $(".primarysuggestion").removeClass("active");
        $("#"+item).addClass("active");
//            alert(item);
    }
    $(".primarysuggestion").click(function () {
        var Img = "";
        $(".loader").fadeIn("slow");
        $(".primarysuggestion").removeClass("active");
        var value = this.id;
        var name = $("#" + value).attr("name");
        var type = value.split("_");
        var selection = type[1];
        $("#" + value).addClass("active");

        var currpage = type[0] + "s";
        var finalparam = type[0];
        switch (type[0]) {
            case "category":
                category = name;
                break;
            case "collar":
                collar = selection;
                ccollar = name;//for cart
                break;
            case "collarbuttondown":
                collarbuttondown = name;
                break;
            case "doublelayeroption":
                doublelayeroption = name;
                switch(doublelayeroption){
                    case "5B12:lower_layer_of_collar_contrast_fabric":
                        $( "div[name^='S-5B41:top_collar_with_contrast_color']" ).addClass("active");
                        doublelayeroption = "S-5B41:top_collar_with_contrast_color,5B12:lower_layer_of_collar_contrast_fabric";
                        break;
                    case "S-5B41:top_collar_with_contrast_color":
                        $( "div[name^='5B12:lower_layer_of_collar_contrast_fabric']" ).addClass("active");
                        doublelayeroption = "S-5B41:top_collar_with_contrast_color,5B12:lower_layer_of_collar_contrast_fabric";
                        break;
                }
                break;
            case "frontcollar":
                frontcollar = name;
                break;
            case "collarbelt":
                var temp = selection.split("1");
                collarbelt = temp[0];
                collarbutton = temp[1];
                break;
            case "cuff":
                cuff = selection;
                ccuff = name; //for cart
                break;
            case "cuffwidth":
                cuffwidth = name;
                break;
            case "placket":
                placket = selection;
                cplacket = name;
                break;
            case "placketbutton":
                placketbutton = name;
                break;
            case "pocket":
                pocket = selection;
                cpocket = name;
                break;
            case "pleat":
                pleat = selection;
                cpleat = name;
                break;
            case "bottom":
                bottom = selection;
                cbottom = name;
                break;
            case "shirtcolor":
                shirtcolor = name;
                break;
            case "buttoncolor":
                buttoncolor = name;
                break;
            case "buttoningstyle":
                buttoningstyle = name;
                break;
            case "buttoncutthread":
                buttoncutthread = name;
                break;
            case "holestichthread":
                holestichthread = name;
                break;
            case "monogramcolor":
                monogramcolor = name;
                break;
            case "monogramposition":
                monogramposition = name;
                break;
            case "monogramoption":
                monogramoption = name;
                break;
            case "contrastfabric":
                contrastfabric = name;
                break;
            case "contrast":
                contrast = name;
                break;
        }
        if (currpage == "categorys") {
            folder = "front";
            if (category == "casual") {
                shirtcolor = "sak069a";
                contrastfabric = "sak069a";
            }
            if (category == "tuxedo") {
                shirtcolor = "sak342a";
                contrastfabric = "sak342a";
            }
            if (category == "formal") {
                shirtcolor = "sda400a";
                contrastfabric = "sda400a";
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {

                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {

                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "collars") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "frontcollars") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "collarbelts") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "cuffs") {
            folder = "side";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + cuff + ".png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + bottom + ".png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "cuffwidths") {
            folder = "side";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + cuff + ".png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + bottom + ".png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "plackets") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "placketbuttons") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "pockets") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "pleats") {
            folder = "back";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pleat + ".png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/bawli.png");
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "bottoms") {
            folder = "side";
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + cuff + ".png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + bottom + ".png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/collarbelt.png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/collarbelt.png");
            }
            $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "shirtcolors") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "buttoncolors") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "buttoningstyles") {
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "buttonswatchs") {
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "contrastfabrics") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "buttoncutthreads") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "holestichthreads") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else if (currpage == "monograms") {
            folder = "front";
            $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + cuff + ".png");
            $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + sleeve + ".png");
            $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
            $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + shoulder + ".png");
            $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/" + pocket + ".png");
            if (placket == "concealed") {
                $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/french.png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/french.png");
                }
            }
            else {
                $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/button.png");
                $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/buttoncut.png");
                $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/buttonpoint.png");
                if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/" + folder + "/" + placket + ".png");
                }
                else {
                    $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/" + folder + "/" + placket + ".png");
                }
            }
            $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/" + folder + "/shirt.png");
            if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/" + folder + "/" + collarbelt + ".png");
            }
            else {
                $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/" + folder + "/" + collarbelt + ".png");
            }
            if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/" + folder + "/" + cuff + ".png");
            }
            else {
                $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/" + folder + "/" + cuff + ".png");
            }
            if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/" + folder + "/" + collar + ".png");
            }
            else {
                $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/" + folder + "/" + collar + ".png");
            }
            $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/" + folder + "/" + collarbutton + ".png");
            $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/" + folder + "/" + collarbutton + "cut.png");
            $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/" + folder + "/" + collarbutton + "point.png");
            $(".loader").fadeOut("slow");
        }
        else {
            $(".loader").fadeOut("slow");
        }

        /*
             * data store all values
             * */

        var url = '../admin/webserver/selectionData.php';

        $.post(url,{currPage:currPage,shirtcategory:category,collar:collar,ccollar:ccollar,collarbuttondown:collarbuttondown,doublelayeroption:doublelayeroption,
            frontcollar:frontcollar,collarbelt:collarbelt,collarbutton:collarbutton,cuff:cuff,ccuff:ccuff,cuffwidth:cuffwidth,placket:placket,cplacket:cplacket,
            placketbutton:placketbutton,pocket:pocket,cpocket:cpocket,pleat:pleat,cpleat:cpleat,bottom:bottom,cbottom:cbottom,shirtcolor:shirtcolor,buttoncolor:buttoncolor,
            buttoningstyle:buttoningstyle,buttoncutthread:buttoncutthread,holestichthread:holestichthread,monogramtext:monogramtext,monogramfont:monogramfont,monogramcode:monogramcode,
            monogramcolor:monogramcolor,monogramposition:monogramposition,monogramoption:monogramoption,contrastfabric:contrastfabric,contrast:contrast,selectionString:selectionString,
            sleeve:sleeve,shoulder:shoulder

        },function(data){
            console.log('data --- '+data);
        });
    });
    function navigation(switcher) {
        $("#" + currPage).fadeOut("slow");
        if (switcher == "next") {
            if (currPage == "categorys") {
                $(".steps").removeClass("stepsactive");
                $("#collars").fadeIn("slow");
                $("#collar").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "collars") {
                $(".steps").removeClass("stepsactive");
                $("#frontcollars").fadeIn("slow");
                $("#frontcollar").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "frontcollars") {
                $(".steps").removeClass("stepsactive");
                $("#collarbelts").fadeIn("slow");
                $("#collarbelt").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "collarbelts") {
                $(".steps").removeClass("stepsactive");
                $("#cuffs").fadeIn("slow");
                $("#cuff").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "cuffs") {
                $(".steps").removeClass("stepsactive");
                $("#cuffwidths").fadeIn("slow");
                $("#cuffwidth").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "cuffwidths") {
                $(".steps").removeClass("stepsactive");
                $("#plackets").fadeIn("slow");
                $("#placket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "plackets") {
                $(".steps").removeClass("stepsactive");
                $("#placketbuttons").fadeIn("slow");
                $("#placketbutton").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "placketbuttons") {
                $(".steps").removeClass("stepsactive");
                $("#pockets").fadeIn("slow");
                $("#pocket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pockets") {
                $(".steps").removeClass("stepsactive");
                $("#pleats").fadeIn("slow");
                $("#pleat").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pleats") {
                $(".steps").removeClass("stepsactive");
                $("#bottoms").fadeIn("slow");
                $("#bottom").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "bottoms") {
                $(".steps").removeClass("stepsactive");
                $("#shirtcolors").fadeIn("slow");
                $("#shirtcolor").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "shirtcolors") {
                $(".steps").removeClass("stepsactive");
                $("#buttoncolors").fadeIn("slow");
                $("#buttoncolor").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttoncolors") {
                $(".steps").removeClass("stepsactive");
                $("#buttonswatchs").fadeIn("slow");
                $("#buttonswatch").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttonswatchs") {
                $(".steps").removeClass("stepsactive");
                $("#buttoningstyles").fadeIn("slow");
                $("#buttoningstyle").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttoningstyles") {
                $(".steps").removeClass("stepsactive");
                $("#buttoncutthreads").fadeIn("slow");
                $("#buttoncutthread").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttoncutthreads") {
                $(".steps").removeClass("stepsactive");
                $("#holestichthreads").fadeIn("slow");
                $("#holestichthread").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "holestichthreads") {
                $(".steps").removeClass("stepsactive");
                $("#monograms").fadeIn("slow");
                $("#monogram").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "monograms") {
                $(".steps").removeClass("stepsactive");
                $("#contrastfabrics").fadeIn("slow");
                $("#contrastfabric").addClass("stepsactive");
                $("#next").html("Add to Cart");
                loadSessionData();
            }
            else if (currPage == "contrastfabrics") {
                var fabricCode = contrastfabric;
                if(fabricCode.indexOf(":") >0 ){
                    fabricCode = fabricCode.split(":")[0];
                }
                var url = "../api/registerUser.php";
                $.post(url,{"type":"checkFabricStatus","fabricCode":fabricCode},function(fabricdata) {
                    var status = fabricdata.Status;
                    var message = fabricdata.Message;
                    if (status == "Success") {
                        var user_id = $("#user_id").val();
                        var totalamount = $("#total_amount").val();
                        var randomnum = Math.floor((Math.random() * 600000) + 1);
                        var product_name = "CustomShirt" + randomnum;
                        var urlData = window.location.href;
                        var url = "";
                        if(urlData.indexOf("freebies=Custom_Shirt")>0){
                            var cart_id = getQueryStringValue("pid");
                            url = "../admin/webserver/addto_cart.php?product_type=freebies_custom_shirt&cart_id="+cart_id+"&product_name="+product_name+
                                "&category=" + category+"&collar=" + ccollar + "&collarbutton="+collarbutton+"&collarbuttondown="+
                                collarbuttondown + "&collarlayeroption=" + doublelayeroption +"&frontcollar=" + frontcollar +
                                "&collarbelt=" + collarbelt +"&cuff=" + ccuff + "&cuffwidth=" + cuffwidth +"&placket=" +
                                cplacket + "&placketbutton=" + placketbutton +"&pocket=" + cpocket + "&pleat=" + cpleat +
                                "&bottom=" + cbottom + "&shirtcolor=" + shirtcolor +"&buttoncolor=" + buttoncolor +
                                "&buttoningstyle=" + buttoningstyle + "&buttoncutthread=" + buttoncutthread +
                                "&holestichthread=" + holestichthread + "&monogramtext=" + monogramtext +"&monogramfont=" +
                                monogramfont +"&monogramcolor=" + monogramcolor +"&monogramposition=" + monogramposition +
                                "&monogramoption=" +monogramoption + "&monogramcode="+ monogramcode +"&contrastfabric=" + contrastfabric + "&contrast=" +
                                contrast + "&sleeve=" + sleeve +"&shoulder=" + shoulder +"&contrastposition=" +
                                selectionString + "&product_price=" + totalamount +"&user_id=" + user_id + "&quantity=" + 1 +
                                "&cart_product_total_amount="+totalamount+"&imgcuff="+cuff+"&imgcollar="+collar+
                                "&imgplacket="+placket;

                        }
                        else{
                            url = "../admin/webserver/addto_cart.php?product_type=Custom Shirt&product_name="+product_name+
                                "&category=" + category+"&collar=" + ccollar + "&collarbutton="+collarbutton+"&collarbuttondown="+
                                collarbuttondown + "&collarlayeroption=" + doublelayeroption +"&frontcollar=" + frontcollar +
                                "&collarbelt=" + collarbelt +"&cuff=" + ccuff + "&cuffwidth=" + cuffwidth +"&placket=" +
                                cplacket + "&placketbutton=" + placketbutton +"&pocket=" + cpocket + "&pleat=" + cpleat +
                                "&bottom=" + cbottom + "&shirtcolor=" + shirtcolor +"&buttoncolor=" + buttoncolor +
                                "&buttoningstyle=" + buttoningstyle + "&buttoncutthread=" + buttoncutthread +
                                "&holestichthread=" + holestichthread + "&monogramtext=" + monogramtext +"&monogramfont=" +
                                monogramfont +"&monogramcolor=" + monogramcolor +"&monogramposition=" + monogramposition +
                                "&monogramoption=" +monogramoption + "&monogramcode="+ monogramcode +"&contrastfabric=" + contrastfabric + "&contrast=" +
                                contrast + "&sleeve=" + sleeve +"&shoulder=" + shoulder +"&contrastposition=" +
                                selectionString + "&product_price=" + totalamount +"&user_id=" + user_id + "&quantity=" + 1 +
                                "&cart_product_total_amount="+totalamount+"&imgcuff="+cuff+"&imgcollar="+collar+
                                "&imgplacket="+placket;
                        }
                        $.get(url, function (data) {
                            var json = $.parseJSON(data);
                            var status = json.status;
                            if (status == "done") {
                                window.location = "order_cart.php?userId="+user_id;
                            }
                            else {
                                alert(status);
                            }
                        });
                    }
                    else{
                        showMessages(message,"red");
                        $("#contrastfabrics").fadeIn("slow");
                        $(".colorsuggestion ").removeClass("active");
                        $("#contrastfabric_sak066a").addClass("active");
                        $("#contrastfabric_sak066a").trigger("click");
                        contrastfabric ='sak066a';
                    }
                });
            }
        }
        else if (switcher == "previous") {
            if (currPage == "categorys") {
                window.location = "customize.php";
            }
            else if (currPage == "collars") {
                $(".steps").removeClass("stepsactive");
                $("#categorys").fadeIn("slow");
                $("#category").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "frontcollars") {
                $(".steps").removeClass("stepsactive");
                $("#collars").fadeIn("slow");
                $("#collar").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "collarbelts") {
                $(".steps").removeClass("stepsactive");
                $("#frontcollars").fadeIn("slow");
                $("#frontcollar").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "cuffs") {
                $(".steps").removeClass("stepsactive");
                $("#collarbelts").fadeIn("slow");
                $("#collarbelt").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "cuffwidths") {
                $(".steps").removeClass("stepsactive");
                $("#cuffs").fadeIn("slow");
                $("#cuff").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "plackets") {
                $(".steps").removeClass("stepsactive");
                $("#cuffwidths").fadeIn("slow");
                $("#cuffwidth").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "placketbutton") {
                $(".steps").removeClass("stepsactive");
                $("#plackets").fadeIn("slow");
                $("#placket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pockets") {
                $(".steps").removeClass("stepsactive");
                $("#placketbuttons").fadeIn("slow");
                $("#placketbutton").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "pleats") {
                $(".steps").removeClass("stepsactive");
                $("#pockets").fadeIn("slow");
                $("#pocket").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "bottoms") {
                $(".steps").removeClass("stepsactive");
                $("#pleats").fadeIn("slow");
                $("#pleat").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "shirtcolors") {
                $(".steps").removeClass("stepsactive");
                $("#bottoms").fadeIn("slow");
                $("#bottom").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttoncolors") {
                $(".steps").removeClass("stepsactive");
                $("#shirtcolors").fadeIn("slow");
                $("#shirtcolor").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttonswatchs") {
                $(".steps").removeClass("stepsactive");
                $("#buttoncolors").fadeIn("slow");
                $("#buttoncolor").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttoningstyles") {
                $(".steps").removeClass("stepsactive");
                $("#buttonswatchs").fadeIn("slow");
                $("#buttonswatch").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "buttoncutthreads") {
                $(".steps").removeClass("stepsactive");
                $("#buttoningstyles").fadeIn("slow");
                $("#buttoningstyles").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "holestichthreads") {
                $(".steps").removeClass("stepsactive");
                $("#buttoncutthreads").fadeIn("slow");
                $("#buttoncutthread").addClass("stepsactive");
                loadSessionData();
            }
            else if (currPage == "contrastfabrics") {
                $(".steps").removeClass("stepsactive");
                $("#holestichthreads").fadeIn("slow");
                $("#holestichthread").addClass("stepsactive");
                $("#next").html("Next");
                loadSessionData();
            }
        }
        storeData();
    }

    function storeData() {
        var value = $(".stepsactive").attr("id");
        var name = $("#" + value).attr("name");
        var type = value.split("_");
        var selection = type[1];

        switch (type[0]) {
            case "category":
                category = name;
                break;
            case "collar":
                collar = selection;
                ccollar = name;//for cart
                break;
            case "collarbuttondown":
                collarbuttondown = name;
                break;
            case "doublelayeroption":
                doublelayeroption = name;
                switch(doublelayeroption){
                    case "5B12:lower_layer_of_collar_contrast_fabric":
                        $( "div[name^='S-5B41:top_collar_with_contrast_color']" ).addClass("active");
                        doublelayeroption = "S-5B41:top_collar_with_contrast_color,5B12:lower_layer_of_collar_contrast_fabric";
                        break;
                    case "S-5B41:top_collar_with_contrast_color":
                        $( "div[name^='5B12:lower_layer_of_collar_contrast_fabric']" ).addClass("active");
                        doublelayeroption = "S-5B41:top_collar_with_contrast_color,5B12:lower_layer_of_collar_contrast_fabric";
                        break;
                }
                break;
            case "frontcollar":
                frontcollar = name;
                break;
            case "collarbelt":
                var temp = selection.split("1");
                collarbelt = temp[0];
                collarbutton = temp[1];
                break;
            case "cuff":
                cuff = selection;
                ccuff = name; //for cart
                break;
            case "cuffwidth":
                cuffwidth = name;
                break;
            case "placket":
                placket = selection;
                cplacket = name;
                break;
            case "placketbutton":
                placketbutton = name;
                break;
            case "pocket":
                pocket = selection;
                cpocket = name;
                break;
            case "pleat":
                pleat = selection;
                cpleat = name;
                break;
            case "bottom":
                bottom = selection;
                cbottom = name;
                break;
            case "shirtcolor":
                shirtcolor = name;
                break;
            case "buttoncolor":
                buttoncolor = name;
                break;
            case "buttoningstyle":
                buttoningstyle = name;
                break;
            case "buttoncutthread":
                buttoncutthread = name;
                break;
            case "holestichthread":
                holestichthread = name;
                break;
            case "monogramcolor":
                monogramcolor = name;
                break;
            case "monogramposition":
                monogramposition = name;
                break;
            case "monogramoption":
                monogramoption = name;
                break;
            case "contrastfabric":
                contrastfabric = name;
                break;
            case "contrast":
                contrast = name;
                break;
        }
        savetoSession('tab',type[0]);
    }
    function rotateImage(button) {
        $(".loader").fadeIn("slow");
        var source = [];
        source.push($("#cuffimg").attr("src"));
        source.push($("#cuffbuttonimg").attr("src"));
        source.push($("#cuffbuttoncutimg").attr("src"));
        source.push($("#cuffbuttonpointimg").attr("src"));
        source.push($("#sleeveimg").attr("src"));
        source.push($("#shoulderimg").attr("src"));
        source.push($("#placketimg").attr("src"));
        source.push($("#pocketimg").attr("src"));
        source.push($("#pleatimg").attr("src"));
        source.push($("#buttonimg").attr("src"));
        source.push($("#buttoncutimg").attr("src"));
        source.push($("#buttonpointimg").attr("src"));
        source.push($("#shirtimg").attr("src"));
        source.push($("#collarimg").attr("src"));
        source.push($("#collarbeltimg").attr("src"));
        source.push($("#collarbuttonimg").attr("src"));
        source.push($("#collarbuttoncutimg").attr("src"));
        source.push($("#collarbuttonpointimg").attr("src"));
        /*alert(JSON.stringify(source));
                 return false;*/
        for (var i = 0; i < source.length; i++) {
            if (source[i].indexOf("front/") >= 0) {
                if (button == "left") {
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/back/" + cuff + ".png");
                    }
                    else {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/back/" + cuff + ".png");
                    }
                    $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/back/" + sleeve + ".png");
                    $("#pleatimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/back/" + pleat + ".png");
                    $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/back/" + shoulder + ".png");
                    $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shirtimg").attr("src", "../images/shirts/sugg/bawli.png");
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/back/collarbelt.png");
                    }
                    else {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/back/collarbelt.png");
                    }
                    $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                }
                else if (button == "right") {
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/side/" + cuff + ".png");
                    }
                    else {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/side/" + cuff + ".png");
                    }
                    $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/side/" + cuff + ".png");
                    $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/side/" + cuff + ".png");
                    $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/side/" + cuff + ".png");
                    $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/side/" + sleeve + ".png");
                    $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/side/" + shoulder + ".png");
                    $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/side/" + bottom + ".png");
                    if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/side/collarbelt.png");
                    }
                    else {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/side/collarbelt.png");
                    }
                    $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                }
            }
            else if (source[i].indexOf("side/") >= 0) {
                if (button == "left") {
                    $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/front/" + cuff + ".png");
                    $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/" + sleeve + ".png");
                    $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/" + shoulder + ".png");
                    $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/" + pocket + ".png");
                    if (placket == "concealed") {
                        $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                        $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                        $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                        if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/front/french.png");
                        }
                        else {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/front/french.png");
                        }
                    }
                    else {
                        $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/front/button.png");
                        $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/front/buttoncut.png");
                        $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/front/buttonpoint.png");
                        if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/front/" + placket + ".png");
                        }
                        else {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/front/" + placket + ".png");
                        }
                    }
                    $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/shirt.png");
                    if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/front/" + collarbelt + ".png");
                    }
                    else {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/front/" + collarbelt + ".png");
                    }
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/front/" + cuff + ".png");
                    }
                    else {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/front/" + cuff + ".png");
                    }
                    if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                        $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/front/" + collar + ".png");
                    }
                    else {
                        $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/front/" + collar + ".png");
                    }
                    $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/front/" + collarbutton + ".png");
                    $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/front/" + collarbutton + "cut.png");
                    $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/front/" + collarbutton + "point.png");
                }
                else if (button == "right") {
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/back/" + cuff + ".png");
                    }
                    else {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/back/" + cuff + ".png");
                    }
                    $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/back/" + sleeve + ".png");
                    $("#pleatimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/back/" + pleat + ".png");
                    $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/back/" + shoulder + ".png");
                    $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shirtimg").attr("src", "../images/shirts/sugg/bawli.png");
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/back/collarbelt.png");
                    }
                    else {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/back/collarbelt.png");
                    }
                    $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                }
            }
            else if (source[i].indexOf("back/") >= 0) {
                if (button == "left") {
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/side/" + cuff + ".png");
                    }
                    else {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/side/" + cuff + ".png");
                    }
                    $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/side/" + cuff + ".png");
                    $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/side/" + cuff + ".png");
                    $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/side/" + cuff + ".png");
                    $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/side/" + sleeve + ".png");
                    $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/side/" + shoulder + ".png");
                    $("#pocketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#placketimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/side/" + bottom + ".png");
                    if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/side/collarbelt.png");
                    }
                    else {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/side/collarbelt.png");
                    }
                    $("#collarimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                }
                else if (button == "right") {
                    $("#cuffbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/front/" + cuff + ".png");
                    $("#cuffbuttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#cuffbuttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#sleeveimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/" + sleeve + ".png");
                    $("#pleatimg").attr("src", "../images/shirts/sugg/bawli.png");
                    $("#shoulderimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/" + shoulder + ".png");
                    $("#pocketimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/" + pocket + ".png");
                    if (placket == "concealed") {
                        $("#buttonimg").attr("src", "../images/shirts/sugg/bawli.png");
                        $("#buttoncutimg").attr("src", "../images/shirts/sugg/bawli.png");
                        $("#buttonpointimg").attr("src", "../images/shirts/sugg/bawli.png");
                        if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/front/french.png");
                        }
                        else {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/front/french.png");
                        }
                    }
                    else {
                        $("#buttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/front/button.png");
                        $("#buttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/front/buttoncut.png");
                        $("#buttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/front/buttonpoint.png");
                        if ($("#leftplackettop").attr("class").indexOf("selected") >= 0) {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + contrastfabric + "/front/" + placket + ".png");
                        }
                        else {
                            $("#placketimg").attr("src", "../images/shirts/sugg/shirt/placket/" + shirtcolor + "/front/" + placket + ".png");
                        }
                    }
                    $("#shirtimg").attr("src", "../images/shirts/sugg/shirt/shirt/" + shirtcolor + "/front/shirt.png");
                    if ($("#collarband").attr("class").indexOf("selected") >= 0) {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + contrastfabric + "/front/" + collarbelt + ".png");
                    }
                    else {
                        $("#collarbeltimg").attr("src", "../images/shirts/sugg/shirt/collarbelt/" + shirtcolor + "/front/" + collarbelt + ".png");
                    }
                    if ($("#outercuff").attr("class").indexOf("selected") >= 0) {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + contrastfabric + "/front/" + cuff + ".png");
                    }
                    else {
                        $("#cuffimg").attr("src", "../images/shirts/sugg/shirt/cuff/" + shirtcolor + "/front/" + cuff + ".png");
                    }
                    if ($("#topcollar").attr("class").indexOf("selected") >= 0) {
                        $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + contrastfabric + "/front/" + collar + ".png");
                    }
                    else {
                        $("#collarimg").attr("src", "../images/shirts/sugg/shirt/collar/" + shirtcolor + "/front/" + collar + ".png");
                    }
                    $("#collarbuttonimg").attr("src", "../images/shirts/sugg/button/" + buttoncolor + "/front/" + collarbutton + ".png");
                    $("#collarbuttoncutimg").attr("src", "../images/shirts/sugg/thread/buttoncut/" + buttoncutthread + "/front/" + collarbutton + "cut.png");
                    $("#collarbuttonpointimg").attr("src", "../images/shirts/sugg/thread/buttonpoint/" + holestichthread + "/front/" + collarbutton + "point.png");
                }
            }
        }
        $(".loader").fadeOut("slow");
    }

    $(".steps").click(function () {
        savetoSession('tab',this.id);
        $("#" + currPage).hide(1000);
        var newcurrpage = this.id + "s";
        $(".steps").removeClass("stepsactive");
        $("#" + this.id).addClass("stepsactive");
        $("#" + newcurrpage).show(1000);
        currPage = newcurrpage;
        loadSessionData();
        if (currPage == "contrastfabrics") {
            $("#next").html("Add to Cart");
        }
        else {
            $("#next").html("Next");
        }
    });

    function contrastselection(selectedoption) {
        if ($("#" + selectedoption).attr("Class").indexOf("selected") == -1) {
            $("#" + selectedoption + "icon").removeClass("fa-square-o");
            $("#" + selectedoption + "icon").addClass("fa-check-square-o");
            $("#" + selectedoption).addClass("selected");
            selectionString = selectionString + selectedoption + ",";
            if (selectedoption == "topcollar") {
                var source = ($("#collarimg").attr("src"));
                source = source.replace(shirtcolor, contrastfabric);
                $("#collarimg").attr("src", source);
            }
            if (selectedoption == "collarband") {
                var source = ($("#collarbeltimg").attr("src"));
                source = source.replace(shirtcolor, contrastfabric);
                $("#collarbeltimg").attr("src", source);
            }
            if (selectedoption == "leftplackettop") {
                var source = ($("#placketimg").attr("src"));
                source = source.replace(shirtcolor, contrastfabric);
                $("#placketimg").attr("src", source);
            }
            if (selectedoption == "outercuff") {
                var source = ($("#cuffimg").attr("src"));
                source = source.replace(shirtcolor, contrastfabric);
                $("#cuffimg").attr("src", source);
            }
        }
        else {
            $("#" + selectedoption + "icon").removeClass("fa-check-square-o");
            $("#" + selectedoption + "icon").addClass("fa-square-o");
            $("#" + selectedoption).removeClass("selected");
            selectionString = selectionString.replace(selectedoption + ",", "");
            if (selectedoption == "topcollar") {
                var source = ($("#collarimg").attr("src"));
                source = source.replace(contrastfabric, shirtcolor);
                $("#collarimg").attr("src", source);
            }
            if (selectedoption == "collarband") {
                var source = ($("#collarbeltimg").attr("src"));
                source = source.replace(contrastfabric, shirtcolor);
                $("#collarbeltimg").attr("src", source);
            }
            if (selectedoption == "leftplackettop") {
                var source = ($("#placketimg").attr("src"));
                source = source.replace(contrastfabric, shirtcolor);
                $("#placketimg").attr("src", source);
            }
            if (selectedoption == "outercuff") {
                var source = ($("#cuffimg").attr("src"));
                source = source.replace(contrastfabric, shirtcolor);
                $("#cuffimg").attr("src", source);
            }
        }
    }
    var count = 0;
    function addcontrastfabric(){
        $(".FabricBtnShirtContrast").attr("disabled",true);
        var contrastfabricfield = $("#contrastfabricfield").val();
        if(contrastfabricfield != ""){
            setSurplusPrice(encodeURI(contrastfabricfield),'ShirtContrast');
            var newdiv = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric('contrastfabric',this);setSurplusPrice('"+encodeURI(contrastfabricfield)+"','ShirtContrast'); " +
                "name='"+contrastfabricfield+"' id='contrastfabric_"+contrastfabricfield+"'><p class='suggestionname' " +
                "style='line-height:80px'>"+contrastfabricfield+"</p></div>";
            $("#contrastError").remove();
            $("#contrastfabricdiv").append(newdiv);
        }else{
            if(count == 0) {
                count++;
                $("#contrastfabricdiv").append("<div style='clear:both'></div><p style='text-align:center;color" +
                    ":red;margin:20px 0 -20px 0' id='contrastError'>Please Enter Fabric Name First</p>");
            }
        }
    }
    var count2 = 0;
    function addmainfabric(){
        $(".FabricBtnShirtColor").attr("disabled",true);
        var mainfabricfield = $("#mainfabricfield").val();
        if(mainfabricfield != ""){
            setSurplusPrice(encodeURI(mainfabricfield),'ShirtColor');
            var newdiv = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric('mainfabric',this);setSurplusPrice('"+encodeURI(mainfabricfield)+"','ShirtColor'); " +
                "name='"+mainfabricfield+"' id='shirtcolor_"+mainfabricfield+"'><p class='suggestionname' " +
                "style='line-height:80px'>"+mainfabricfield+"</p></div>";
            $("#mainError").remove();
            $("#mainfabricdiv").append(newdiv);
        }else{
            if(count2 == 0) {
                count2++;
                $("#mainfabricdiv").append("<div style='clear:both'></div><p style='text-align:center;color" +
                    ":red;margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
            }
        }
    }
    function selectFabric(type,obj){
        $(".primarysuggestion").removeClass("active");
        $("#"+obj.id).addClass("active");
        if(type == "contrastfabric"){
            contrastfabric = $("#"+obj.id).attr('name');
        }
        else if(type == "mainfabric"){
            shirtcolor = $("#"+obj.id).attr('name');
        }
    }

    var count3 = 0;
    function addmainfabric1(){
        var mainfabricfield1 = $("#mainfabricfield1").val();
        if(mainfabricfield1 != ""){
            var newdiv1 = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric1('mainfabric1',this)" +
                "name='"+mainfabricfield1+"' id='placket_"+mainfabricfield1+"'><p class='suggestionname' " +
                "style='line-height:80px'>"+mainfabricfield1+"</p></div>";
            $("#mainError").remove();
            $("#mainfabricdiv1").append(newdiv1);
        }else{
            if(count3 == 0) {
                count3++;
                $("#mainfabricdiv1").append("<div style='clear:both'></div><p style='text-align:center;color" +
                    ":red;margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
            }
        }
    }
    var count4 = 0;
    function addmainfabric2() {
        $(".FabricBtnUpperContrast").attr("disabled",true);
        var mainfabricfield2 = $("#mainfabricfield2").text();
        if( mainfabricfield2 !== "") {
            setSurplusPrice(encodeURI(mainfabricfield2),'UpperContrast');
            var newdiv2 = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric1('mainfabric2',this);setSurplusPrice('"+encodeURI(mainfabricfield2)+"','UpperContrast'); "+
                "name='"+mainfabricfield2+"' id='doublelayeroption_"+mainfabricfield2+"'><p class='suggestionname' " +
                "style='line-height:80px'>"+mainfabricfield2+"</p></div>";
            $("#mainError").remove();
            $("#mainfabricdiv2").append(newdiv2);
        }
        else{
            if(count4 == 0) {
                count4++;
                $("#mainfabricdiv2").append("<div style='clear:both'></div><p style='text-align:center;color" +
                    ":red;margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
            }
        }
    }

    var count5 = 0;
    function addmainfabric3() {
        $(".FabricBtnLowerContrast").attr("disabled",true);
        var mainfabricfield3 = $("#mainfabricfield3").text();
        if(mainfabricfield3 != ""){
            setSurplusPrice(encodeURI(mainfabricfield3),'LowerContrast');
            var newdiv3 = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric1('mainfabric3',this);setSurplusPrice('"+encodeURI(mainfabricfield3)+"','LowerContrast'); " +
                "name='"+mainfabricfield3+"' id='doublelayeroption1_"+mainfabricfield3+"'><p class='suggestionname' " +
                "style='line-height:80px'>"+mainfabricfield3+"</p></div>";
            $("#mainError").remove();
            $("#mainfabricdiv3").append(newdiv3);
        }else{
            if(count5 == 0) {
                count5++;
                $("#mainfabricdiv3").append("<div style='clear:both'></div><p style='text-align:center;color" +
                    ":red;margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
            }
        }
    }

    var count6=0;
    function addmainfabric4() {
        $(".FabricBtnLowerContrast").attr("disabled",true);
        var mainfabricfield4 = $("#bottomFabric").text();
        if(mainfabricfield4 !== "") {
            setSurplusPrice(encodeURI(mainfabricfield4),'bottomFabric');
            var newdiv3 = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric1('bottomFabric',this);setSurplusPrice('"+encodeURI(mainfabricfield4)+"','bottomFabric'); " +
                "name='"+mainfabricfield4+"' id='doublelayeroption1_"+mainfabricfield4+"'><p class='suggestionname' " +
                "style='line-height:80px'>"+mainfabricfield4+"</p></div>";
            $("#mainError").remove();
            $("#mainfabricdiv4").append(newdiv3);
        }else{
            if(count6 === 0) {
                count6++;
                $("#mainfabricdiv4").append("<div style='clear:both'></div><p style='text-align:center;color" +
                    ":red;margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
            }
        }
    }
    function selectFabric1(type,obj){
        $(".primarysuggestion").removeClass("active");
        $("#"+obj.id).addClass("active");
        if(type == "contrastfabric"){
            contrastfabric = $("#"+obj.id).attr('name');
        }
        else if(type == "mainfabric1"){
            shirtcolor = $("#"+obj.id).attr('name');
        }
        else if(type == "mainfabric2"){
            shirtcolor = $("#"+obj.id).attr('name');
        }
    }

    function edValueKeyPress(test) {
        $("#lettertype").html(test);
        monogramtext = test;
    }
    $('.rccode').click(function(){
        var font = this.id;
        var code = this.value;
        $("#lettertype").attr("style","font-family:"+font+"!important;");
        monogramfont = font;
        monogramcode = code;
    });
    function getColorCode(colorCode) {
        $("#lettertype").css("color", "#" + colorCode);
        monogramcolor = colorCode;
    }
    function searchFabricData(ref,fabricBtn) {

        var value = $('#'+ref.id).val();
        if(ref.id === 'mainfabricfield2' || ref.id === 'mainfabricfield3' || ref.id ==='bottomFabric') {
            value = $('#'+ref.id).text();
        }
//                    $("#findFabric").attr("disabled",true);
        var url = "admins/api/fabricProcess.php";
        $.post(url,{'dataType':'searchFabricData','cameo_code':value},function(response){
            var results = [];
//                   var response = $.parseJSON(data);
            var status = response.Status;
            var message = response.Message;
            var li_ = '';
            if(status === 'Success' ) {
                results = [];
                var dataa = response.data;

                for(var i=0;i<dataa.length;i++) {
                    li_ = li_ + '<li onclick=fabricSelected("' + ref.id + '","' + dataa[i].fabric_code + '")>' + dataa[i].fabric_code + '</li>';
                }
                if(ref.id === 'mainfabricfield2') {
                    $('#list_data').removeClass('data-remove');
                    $('#list_data').html(li_);
                }
                else if(ref.id === 'contrastfabricfield') {
                    $('#list_data2').removeClass('data-remove');
                    $('#list_data2').html(li_);
                }
                else if(ref.id === 'bottomFabric') {
                    $('#list_data3').removeClass('data-remove');
                    $('#list_data3').html(li_);
                }
                else{
                    $('#list_data1').removeClass('data-remove');
                    $('#list_data1').html(li_);
                }
            }
            else if(status === 'Failure') {
                li_ = li_+'<li>No data found</li>';
                if(ref.id === 'mainfabricfield2') {
                    $('#list_data').removeClass('data-remove');
                    $('#list_data').html(li_);
                }
                else if(ref.id === 'contrastfabricfield') {
                    $('#list_data2').removeClass('data-remove');
                    $('#list_data2').html(li_);
                }
                else if(ref.id === 'bottomFabric') {
                    $('#list_data3').removeClass('data-remove');
                    $('#list_data3').html(li_);
                }
                else{
                    $('#list_data1').removeClass('data-remove');
                    $('#list_data1').html(li_);
                }
            }

        });
    }

    function fabricSelected(id,value) {

        $('#'+id).val(value);
        if(id === 'mainfabricfield2' || id === 'mainfabricfield3' || id === 'bottomFabric') {
            $('#'+id).html(value);
        }
        if(id === 'mainfabricfield2') {
            $("#findFabric").attr("disabled",false);
            $('#list_data').addClass('data-remove');
        }
        else if(id === 'contrastfabricfield') {
            $("#findFabric2").attr("disabled",false);
            $('#list_data2').addClass('data-remove');
        }
        else if(id === 'bottomFabric') {
            $("#findFabric3").attr("disabled",false);
            $('#list_data3').addClass('data-remove');
        }
        else{
            $("#findFabric1").attr("disabled",false);
            $('#list_data1').addClass('data-remove');
        }
    }

    window.onclick = function () {
        if(!$("#list_data").hasClass('data-remove')) {
            $("#list_data").addClass('data-remove');
        }
        if(!$("#list_data1").hasClass('data-remove')) {
            $("#list_data1").addClass('data-remove');
        }
        if(!$("#list_data2").hasClass('data-remove')) {
            $("#list_data2").addClass('data-remove');
        }
        if(!$("#list_data3").hasClass('data-remove')) {
            $("#list_data3").addClass('data-remove');
        }
    };
    function getQueryStringValue (key) {
        return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
    }

    function savetoSession(type,content_id) {
        var url = "../admin/webserver/selectionData.php?sessionType=storeSession&type=custom_shirts&content_id="+content_id;
        $.get(url,function(data){
            console.log('response --- '+data);
        });
    }
    function getSession() {
        var url = "../admin/webserver/selectionData.php?type=custom_shirts&sessionType=getSession";
        currPage = "category";
        $.get(url,function(data) {
            data = $.parseJSON(data);
            if(data.status ==='done') {
                currPage = data.content;
                if(data.shirtcategory!=="")
                    category = data.shirtcategory;
                if(data.collar!=="")
                    collar = data.collar;
                if(data.ccollar!=="")
                    ccollar = data.ccollar;
                if(data.collarbuttondown!=="")
                    collarbuttondown = data.collarbuttondown;
                if(data.doublelayeroption!=="")
                    doublelayeroption = data.doublelayeroption;
                if(data.frontcollar!=="")
                    frontcollar = data.frontcollar;
                if(data.collarbelt!=="")
                    collarbelt = data.collarbelt;
                if(data.collarbutton!=="")
                    collarbutton = data.collarbutton;
                if(data.cuff!=="")
                    cuff = data.cuff;
                if(data.ccuff!=="")
                    ccuff = data.ccuff;
                if(data.cuffwidth!=="")
                    cuffwidth = data.cuffwidth;
                if(data.placket!=="")
                    placket = data.placket;
                if(data.cplacket!=="")
                    cplacket = data.cplacket;
                if(data.placketbutton!=="")
                    placketbutton = data.placketbutton;
                if(data.pocket!=="")
                    pocket = data.pocket;
                if(data.cpocket!=="")
                    cpocket = data.cpocket;
                if(data.pleat!=="")
                    pleat = data.pleat;
                if(data.cpleat!=="")
                    cpleat = data.cpleat;
                if(data.bottom!=="")
                    bottom = data.bottom;
                if(data.cbottom!=="")
                    cbottom = data.cbottom;
                if(data.shirtcolor!=="")
                    shirtcolor = data.shirtcolor;
                if(data.buttoncolor!=="")
                    buttoncolor = data.buttoncolor;
                if(data.buttoningstyle!=="")
                    buttoningstyle = data.buttoningstyle;
                if(data.buttoncutthread!=="")
                    buttoncutthread = data.buttoncutthread;
                if(data.holestichthread!=="")
                    holestichthread = data.holestichthread;
                if(data.monogramtext!=="")
                    monogramtext = data.monogramtext;
                if(data.monogramfont!=="")
                    monogramfont = data.monogramfont;
                if(data.monogramcode!=="")
                    monogramcode = data.monogramcode;
                if(data.monogramcolor!=="")
                    monogramcolor = data.monogramcolor;
                if(data.monogramposition!=="")
                    monogramposition = data.monogramposition;
                if(data.contrastfabric!=="")
                    contrastfabric = data.contrastfabric;
                if(data.contrast!=="")
                    contrast = data.contrast;
                if(data.selectionString!=="")
                    selectionString = data.selectionString;
                if(data.sleeve!=="")
                    sleeve = data.sleeve;
                if(data.shoulder!=="")
                    shoulder = data.shoulder;
            }


            $(".steps").removeClass("stepsactive");
            $(".stepdiv").fadeOut("slow");
            $("#"+currPage+"s").fadeIn("slow");
            $("#"+currPage).addClass("stepsactive");
            loadSessionData();


        });
    }
    loadSessionData();
    //        getSession();
    var user_id = $("#user_id").val();
    getCart(user_id);
</script>