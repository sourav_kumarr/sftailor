<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js">
<head>
    <link rel="icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon"/>
    <link rel='stylesheet' id='main-css' href='css/main.css' type='text/css' media='all'/>
    <link rel='stylesheet' id='main-css' href='css/font-awesome.css' type='text/css' media='all'/>
    <script type="text/javascript" src="js/raven.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/jquery-2.2.2.js"></script>
    <title>sftailor</title>
</head>
<?php
error_reporting(0);
session_start();
$user_id = "";
$user_id = $_SESSION['sftuser_id'];
echo "<input type = 'hidden' id='user_id' value=" . $user_id . " >";
?>
<input type="hidden" value="" id="sustain">

<style>
    .loaderr {
        background: #fff;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }

    .loaderimg {
        background-attachment: fixed;
        margin-top: 20%;
        margin-left: 49%;
        height: 50px;
        width: 50px;
    }

    .hideloader {
        display: none !important;
    }
</style>
<style>
    .header-ancor {
        cursor: pointer;
    }

    .submenu-items-ul {
        list-style-type: none;
        display: inline-block !important;
        line-height: 1 !important;
        position: absolute;

        width: 200px;
        background: #fff;
        padding-left: 3px;
        letter-spacing: .8;

    }

    .submenu-items-a {
        text-decoration: none !important;
    }

    .submenu-items-li {
        line-height: 1 !important;
    }

    .search-box {
        float: right;
        margin-top: 13px;
    }

    .sb-search {
        backface-visibility: hidden;
        float: right;
        height: 40px;
        margin-top: 14px;
        min-width: 42px;
        position: absolute;
        right: 0;
        transition: width 0.3s ease 0s;
        width: 0;
        z-index: 999;
    }

    .sb-icon-search {
        background: #811215 url("images/search.png") no-repeat scroll 7px 9px;
        border-radius: 50px;
        height: 40px;
        width: 40px;
        z-index: 90;
    }

    .sb-icon-search, .sb-search-submit {
        cursor: pointer;
        display: block;
        height: 41px;
        line-height: 71px;
        margin: 0;
        padding: 0;
        position: absolute;
        right: 79px;
        text-align: center;
        top: 0;
        width: 42px;
    }

    .bagde {
        background: red none repeat scroll 0 0;
        border-radius: 50%;
        color: white;
        font-size: 12px;
        font-weight: bold;
        height: 24px;
        line-height: 24px;
        margin: -4px;
        position: absolute;
        text-align: center;
        width: 24px;
        font-family: fantasy;
    }

    .header-ancor {
        cursor: pointer;
    }

    .submenu-items2-ul {
        list-style-type: none;
        display: inline-block !important;
        line-height: 5rem !important;
        position: absolute;

        width: 238px;
        background: #fff;
        padding-left: 3px;
        letter-spacing: .8;
        height: auto !important;
        display: none !important;

    }

    .advantage-li:hover .submenu-items2-ul {
        display: inline-block !important;
    }

    .submenu-items2-a {
        text-decoration: none !important;
        cursor: pointer;
        line-height: 5rem !important;
    }

    .submenu-items2-li {
        width: 100%;
        line-height: 2rem !important;
    }

    @media screen and (-webkit-min-device-pixel-ratio: 0) {
        .sb-search {
            margin-top: -80px !important;
        }
    }

    .profile_img {
        border: 1px solid #ccc;
        border-radius: 50%;
        height: 40px;
        margin-top: 14px;
        margin-bottom: -13px;
        width: 40px;
    }

    .header__menu {
        margin-left: 0rem;
    }

    @media screen and (-webkit-min-device-pixel-ratio: 0)
    and (min-resolution: .001dpcm) {
        .sb-icon-search, .sb-search-submit {
            top: 102px !important;
        }
    }


    /*awards start here*/
    .nbs-flexisel-ul {
        position: relative;
        width: 9999px;
        margin: 0px;
        padding: 0px;
        list-style-type: none;
        text-align: center;
    }
    .nbs-flexisel-item {
        float: left;
        margin: 0px;
        padding: 0px;
        cursor: pointer;
        position: relative;
        line-height: 0px;
    }
    .nbs-flexisel-item > img {
        width: 80%;
        cursor: pointer;
        positon: relative;
        margin-top: 0.5em;
        max-width: 200px;
        max-height: 90px;
        height: 66px;
    }
    .nbs-flexisel-container {
        position: relative;
        max-width: 100%;
    }
    .nbs-flexisel-inner {
        overflow: hidden;
        width: 90%;
        margin: 0 auto;
        margin-top: 1%;
    }
    .nbs-flexisel-nav-left, .nbs-flexisel-nav-right {
        width: 26px;
        height: 36px;
        position: absolute;
        cursor: pointer;
        z-index: 100;
    }
    .footer__quicklinks nav{
        width: 36rem;
    }

    .nbs-flexisel-nav-left {
        left: 0px;
        background: url(images/c_arrows.png) no-repeat 0px 0px;
    }
    .nbs-flexisel-nav-right {
        right: 1px;
        background: url(images/c_arrows.png) no-repeat -18px 0px;
    }
    /*awards end   here*/
    .submenu-items2-li-ul{
        display: none;
        line-height: 5rem !important;
        position: absolute;
        width: 238px;
        background: #fff;
        padding-left: 3px;
        list-style-type: none;
        height: auto !important;
        top: 50%;
        margin-left: 90%;
    }
    .submenu-items2-li:hover .submenu-items2-li-ul{
        display: inline-block !important;
    }
    .loaderr{
        display: none;
    }
</style>
<script>
    function viewRecommends(){
        window.location = "recommends.php";
    }
</script>
<body>
<div class="loaderr" id="loadd">
    <img src="images/762.gif" class="loaderimg"/>
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="site">
    <header class="site-header header" role="banner">
        <div class="header__container">
            <a href="index.php" class="header__logo">
                <img src="images/logo/logo.png" style="height: 60px; margin-top: 17px;"/>
            </a>

            <div class="header__toggler">
                <a href="#">
                    <svg viewBox="0 0 16 13" xmlns="http://www.w3.org/2000/svg">
                        <g>
                            <rect id="top-line" x="0" y="0" width="16" height="1"></rect>
                            <rect id="middle-line" x="0" y="6" width="16" height="1"></rect>
                            <rect id="bottom-line" x="0" y="12" width="16" height="1"></rect>
                        </g>
                    </svg>
                </a>
            </div>
            <div class="header__list">
                <nav class="header__menu header__menu--main">
                    <ul id="menu-main-en" class="menu">

                        <li id="menu-item-29"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
                            <a onclick="pageRedirect('index')" class="header-ancor">HOME</a>
                        </li>
                        <li id="menu-item-1042"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042 advantage-li">
                            <a onclick="pageRedirect('about')" class="header-ancor">ABOUT US</a>
                            <ul class="submenu-items2-ul">

                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('about')" class="submenu-items2-a">BESPOKE</a>
                                </li>

                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('suits')" class="submenu-items2-a">SUITS</a>
                                </li>

                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('shirts')" class="submenu-items2-a">SHIRTS</a>
                                </li>
                                <li  class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('fabric')" class="submenu-items2-a">Fabric</a>
                                    <ul class="submenu-items2-li-ul" style="display: none">
                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('fabric')" class="submenu-items2-a">Fabric Descriptions</a>
                                        </li>

                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('topbrands')" class="submenu-items2-a">TOP Fabrics brands</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('fullbespoke')" class="submenu-items2-a">ADVANTAGE</a>
                                    <ul class="submenu-items2-li-ul" style="display: none">
                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('fullbespoke')" class="submenu-items2-a">FULL BESPOKE
                                                ADVANTAGES</a>
                                        </li>

                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('customfit')" class="submenu-items2-a">CUSTOM FIT</a>
                                        </li>

                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('canvas')" class="submenu-items2-a">CANVAS</a>
                                        </li>

                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('pricelist')" class="submenu-items2-a">PRICE LIST</a>
                                        </li>

                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('workprocess')" class="submenu-items2-a">Work Process</a>
                                        </li>
                                        <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                            <a onclick="pageRedirect('highenddetails')" class="submenu-items2-a">High end
                                                Details</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('faq')" class="submenu-items2-a">FAQ</a>
                                </li>
                            </ul>
                        </li>
                        <li id="menu-item-1042"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042">
                            <a onclick="pageRedirect('stylish')" class="header-ancor">Join Our Team</a>
                        </li>
                        <li id="menu-item-1043"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1043">
                            <a href="http://www.sftailorsblog.com" target="_blank" class="header-ancor">Our Blog</a>
                        </li>
                        <li id="menu-item-29"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
                            <a onclick="pageRedirect('gallery')" class="header-ancor">GALLERY</a>
                        </li>
                        <li id="menu-item-29"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29">
                            <a onclick="pageRedirect('partners')" class="header-ancor">PARTNERS</a>
                        </li>
                        <li id="menu-item-129" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-129">
                            <a onclick="pageRedirect('book_appointment')" class="header-ancor">Book Appointment</a>
                        </li>
                        <li id="menu-item-291" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-291">
                            <a onclick="pageRedirect('feedback')" class="header-ancor">FEEDBACK</a>
                        </li>
                        <li id="menu-item-26"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-26 advantage-li">
                            <a onclick="pageRedirect('customize')" class="header-ancor" style="padding: 0px 10px 0px 8px;color: #c4996c !important;font-size: 16px;font-weight: bold !important;letter-spacing: -1px;margin-top: -2px;">ORDER NOW</a>
                           <!-- <ul class="submenu-items2-ul">
                                <li class="submenu-items2-li " style="line-height: 5rem!important;">
                                    <a onclick="pageRedirect('accessories')" class="submenu-items2-a">ACCESSORIES</a>
                                </li>
                            </ul>-->
                        </li>
                        <li id="menu-item-25"
                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25"><a
                                    onclick="pageRedirect('contact')" class="header-ancor">CONTACT US</a></li>
                        <?php
                        if ($user_id != "") {
                            ?>
                            <div class="search-box" onclick="pageRedirect('cart')">
                                <div id="sb-search" class="sb-search">
                                    <span class="sb-icon-search"><span class="bagde">0</span> </span>
                                </div>
                            </div>
                        <?php }
                        ?>
                        <?php
                        if ($user_id != "") {
                            ?>
                            <li><img src="" class="profile_img" id="profile"
                                     alt="profile photo"/></li>
                            <li style="margin-left:-5px" id="menu-item-27"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27 advantage-li">
                                <a class="header-ancor" id="username">User Name</a>
                                <ul class="submenu-items2-ul">
                                    <li id="menu-item-1042"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1042"
                                        style="height: 78px;">
                                        <a onclick="uploadOrdFromIndex()" class="header-ancor">Upload ORD</a>
                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">

                                        <a onclick="pageRedirect('sizematching')" class="submenu-items2-a">Twin Genie</a>
                                    </li>

                                    <!--<li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('orders')" class="submenu-items2-a">MY ORDERS</a>
                                    </li>-->
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="bodyStyle()" class="submenu-items2-a">Body Style</a>
                                    </li>

                                    <!--<li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('sdet')" class="submenu-items2-a">Style Genie</a>
                                    </li>-->
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="checkMeasurement()" class="submenu-items2-a">Check Measurement</a>
                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="myfilesFromIndex()" class="submenu-items2-a">My ORD Files</a>
                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="viewRecommends()" class="submenu-items2-a">Reorder Product</a>
                                    </li>
                                    <li id="menu-item-26"
                                        class="submenu-items2-li">
                                        <a onclick="pageRedirect('orderStatus')" class="submenu-items2-a" >ORDER STATUS</a>

                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="viewSavedfromIndex()" class="submenu-items2-a">Saved Measurements</a>
                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('myaccount')" class="submenu-items2-a">MY ACCOUNT</a>
                                    </li>
                                    <li id="menu-item-26"
                                        class="submenu-items2-li">
                                        <a onclick="pageRedirect('customize')" class="submenu-items2-a" >ORDER NOW</a>

                                    </li>
                                    <li class="submenu-items2-li" style="line-height: 5rem!important;">
                                        <a onclick="pageRedirect('logout')" class="submenu-items2-a">LOGOUT</a>
                                    </li>
                                </ul>
                            </li>


                            <?php
                        } else {
                            ?>
                            <li id="menu-item-25"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-25">
                                <a class="header-ancor" onclick="pageRedirect('login')">LOGIN/REGISTER</a></li>
                            <?php
                        }
                        ?>

                    </ul>
                </nav>
            </div>
        </div>
    </header>


    <!-- header end here -->

    <main id="wrapper">

        <div data-namespace="front-page"
             class="home page page-id-9 page-template page-template-front-page page-template-front-page-php container"
             data-translations='{"en":{"id":6,"order":0,"slug":"en","locale":"en-US","name":"English","url":"http:\/\/www.scabal.com\/en\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/us.png","current_lang":true,"no_translation":false,"classes":["lang-item","lang-item-6","lang-item-en","lang-item-first","current-lang"]},"zh":{"id":9,"order":0,"slug":"zh","locale":"zh-CN","name":"\u4e2d\u6587 (\u4e2d\u56fd)","url":"http:\/\/www.scabal.com\/zh\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/cn.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-9","lang-item-zh"]},"fr":{"id":439,"order":0,"slug":"fr","locale":"fr-FR","name":"Fran\u00e7ais","url":"http:\/\/www.scabal.com\/fr\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/fr.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-439","lang-item-fr"]},"es":{"id":443,"order":0,"slug":"es","locale":"es-ES","name":"Espa\u00f1ol","url":"http:\/\/www.scabal.com\/es\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/es.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-443","lang-item-es"]},"it":{"id":447,"order":0,"slug":"it","locale":"it-IT","name":"Italiano","url":"http:\/\/www.scabal.com\/it\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/it.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-447","lang-item-it"]},"nl":{"id":451,"order":0,"slug":"nl","locale":"nl-NL","name":"Nederlands","url":"http:\/\/www.scabal.com\/nl\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/nl.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-451","lang-item-nl"]},"de":{"id":492,"order":0,"slug":"de","locale":"de-DE","name":"Deutsch","url":"http:\/\/www.scabal.com\/de\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/de.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-492","lang-item-de"]},"ru":{"id":496,"order":0,"slug":"ru","locale":"ru-RU","name":"\u0420\u0443\u0441\u0441\u043a\u0438\u0439","url":"http:\/\/www.scabal.com\/ru\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/ru.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-496","lang-item-ru"]},"ja":{"id":500,"order":0,"slug":"ja","locale":"ja","name":"\u65e5\u672c\u8a9e","url":"http:\/\/www.scabal.com\/ja\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/jp.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-500","lang-item-ja"]},"ko":{"id":504,"order":0,"slug":"ko","locale":"ko-KR","name":"\ud55c\uad6d\uc5b4","url":"http:\/\/www.scabal.com\/ko\/","flag":"http:\/\/www.scabal.com\/wp-content\/plugins\/polylang-pro\/flags\/kr.png","current_lang":false,"no_translation":false,"classes":["lang-item","lang-item-504","lang-item-ko"]}}'
        >
            <div class="lateral-nav-wrapper">
                <nav class="lateral-nav">
                    <ul>
                        <!-- <li class="active"><span>Tailor on Demand</span></li>
                        <li ><span>Tailoring</span></li>
                        <li ><span>Fabrics</span></li>
                        <li ><span>Club</span></li>
                        <li ><span>Heritage</span></li> -->
                    </ul>
                </nav>
            </div>
            <section class="movie">
                <div class="block block--video">
                    <div class="block__overlay block__video">
                        <video autoplay loop muted data-width="1080" data-height="600">
                            <source src="video/Homepage-7.mp4" type="video/mp4">
                        </video>

                        <img class="block__video__fallback"
                             src="images/indexImage/scan2tailor-luxury-tailored-suit-1024x435.jpg"
                             srcset="images/indexImage/scan2tailor-luxury-tailored-suit-300x127.jpg 300w, images/indexImage/scan2tailor-luxury-tailored-suit-768x326.jpg 768w, images/indexImage/scan2tailor-luxury-tailored-suit-1024x435.jpg 1024w, images/indexImage/scan2tailor-luxury-tailored-suit-450x191.jpg 450w, images/indexImage/scan2tailor-luxury-tailored-suit-600x255.jpg 600w, images/indexImage/scan2tailor-luxury-tailored-suit-800x340.jpg 800w, images/indexImage/scan2tailor-luxury-tailored-suit-900x382.jpg 900w, images/indexImage/scan2tailor-luxury-tailored-suit-1200x510.jpg 1200w, images/indexImage/scan2tailor-luxury-tailored-suit-1600x680.jpg 1600w"
                             alt="scan2tailor-luxury-tailored-suit"/>
                    </div>

                    <div class="block__overlay movie__text">
                        <div class="block__center">
                            <h1>HAVE THE PERFECT FIT</h1>
                            <a href="#"
                               data-play-video data-video-id="186789276"
                               data-video-width="1900"
                               data-video-height="700"
                               data-play-icon>
                                <!--   <svg width="40px" height="40px" viewBox="0 0 30 30" version="1.1" xmlns="http://www.w3.org/2000/svg" id="play-triangle-svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                   <rect width="40" height="40" style="fill:rgba(0,0,0,0);" />
                                   <defs>
                                   </defs>
                                   <g id="play-triangle-shape" stroke-width="1" fill="none" fill-rule="evenodd">
                                       <circle class="play-circle" id="play-circle" cx="15" cy="15" r="12" stroke="#C0AB89"/>

                                       <g class="triangle-container">
                                         <polygon class="play-triangle" id="Play-Triangle" points="8 26 8 4 27 15" stroke="#C0AB89" stroke-width="2"></polygon>
                                       </g>
                                   </g>
                               </svg> -->
                            </a>
                        </div>
                    </div>
                </div>
            </section>
            <section class="tailoring">
                <div class="block block--video">
                    <div class="block__overlay block__video">
                        <video autoplay loop muted data-width="1080" data-height="600">
                            <source src="video/Tailoring.mp4" type="video/mp4">
                        </video>

                        <img class="block__video__fallback"
                             src="images/indexImage/scabal-tailor-made-to-measure-suit-1024x684.jpg"
                             srcset="images/indexImage/scabal-tailor-made-to-measure-suit-300x200.jpg 300w, images/indexImage/scabal-tailor-made-to-measure-suit-768x513.jpg 768w, images/indexImage/scabal-tailor-made-to-measure-suit-1024x684.jpg 1024w, images/indexImage/scabal-tailor-made-to-measure-suit-450x300.jpg 450w, images/indexImage/scabal-tailor-made-to-measure-suit-600x401.jpg 600w, images/indexImage/scabal-tailor-made-to-measure-suit-800x534.jpg 800w, images/indexImage/scabal-tailor-made-to-measure-suit-900x601.jpg 900w, images/indexImage/scabal-tailor-made-to-measure-suit-1200x801.jpg 1200w, images/indexImage/scabal-tailor-made-to-measure-suit-1600x1068.jpg 1600w"
                             alt="scabal-tailor-made-to-measure-suit"
                             sizes="(min-width: 48.75) 50vw, 100vw"/>
                    </div>

                    <div class="block__title">
                        <h2>Welcome to</h2>
                        <h3>sftailor</h3>
                    </div>
                </div>

                <div class="block block--text" data-fit-text>
                    <div class="block--text__wrapper">
                        <span class="divider"></span>
                        <p>Bespoke/Personalized Custom is in first place of the ten technologies which will change our
                            future.</p>
                        <span class="divider"></span>
                        <a href="javascript:void()" onclick="orderRedirect('customize')">Order now</a>

                    </div>
                </div>

                <div class="block block--image tailoring__curzon">
                    <img class="block--image__background"
                         src="images/indexImage/scabal-tailored-suits-1024x684.jpg"
                         srcset="images/indexImage/scabal-tailored-suits-300x200.jpg 300w, images/indexImage/scabal-tailored-suits-768x513.jpg 768w, images/indexImage/scabal-tailored-suits-1024x684.jpg 1024w, images/indexImage/scabal-tailored-suits-600x401.jpg 600w, images/indexImage/scabal-tailored-suits-800x534.jpg 800w, images/indexImage/scabal-tailored-suits-900x601.jpg 900w, images/scabal-tailored-suits-1200x801.jpg 1200w, images/indexImage/scabal-tailored-suits-1600x1068.jpg 1600w"
                         sizes="(min-width: 48.75em) 50vw,
            100vw"
                         alt="scabal-tailored-suits"/>
                </div>

                <div class="block block--image tailoring__soho">
                    <img class="block--image__background"
                         src="images/indexImage/scabal-tailored-suit-curzon-676x1024.jpg"
                         srcset="images/indexImage/scabal-tailored-suit-curzon-198x300.jpg 198w, images/indexImage/scabal-tailored-suit-curzon-768x1163.jpg 768w, images/indexImage/scabal-tailored-suit-curzon-676x1024.jpg 676w, images/indexImage/scabal-tailored-suit-curzon-450x682.jpg 450w, images/indexImage/scabal-tailored-suit-curzon-600x909.jpg 600w, images/indexImage/scabal-tailored-suit-curzon-800x1212.jpg 800w, images/indexImage/scabal-tailored-suit-curzon-900x1363.jpg 900w, images/indexImage/scabal-tailored-suit-curzon-1200x1818.jpg 1200w, images/indexImage/scabal-tailored-suit-curzon-1600x2424.jpg 1600w"
                         sizes="(min-width: 48.75em) 50vw,
            100vw"
                         alt="scabal-tailored-suit-curzon"/>
                </div>

                <div class="block block--image tailoring__mayfair">
                    <img class="block--image__background"
                         src="images/indexImage/1scabal-tailored-suit-clevedon-676x1024.jpg"
                         srcset="images/indexImage/1scabal-tailored-suit-clevedon-198x300.jpg 198w, images/indexImage/1scabal-tailored-suit-clevedon-768x1163.jpg 768w, images/indexImage/1scabal-tailored-suit-clevedon-676x1024.jpg 676w, images/indexImage/1scabal-tailored-suit-clevedon-450x682.jpg 450w, images/indexImage/1scabal-tailored-suit-clevedon-600x909.jpg 600w, images/indexImage/1scabal-tailored-suit-clevedon-800x1212.jpg 800w, images/indexImage/1scabal-tailored-suit-clevedon-900x1363.jpg 900w, images/indexImage/1scabal-tailored-suit-clevedon-1200x1818.jpg 1200w, images/indexImage/1scabal-tailored-suit-clevedon-1600x2424.jpg 1600w"
                         sizes="(min-width: 48.75em) 50vw,
            100vw"
                         alt="scabal-tailored-suit-clevedon"/>
                </div>
            </section>
            <section class="fabrics">
                <div class="block block--video">
                    <div class="block__overlay block__video">
                        <video autoplay loop muted data-width="710" data-height="680">
                            <source src="video/Homepage_Fabrics.mp4" type="video/mp4">
                        </video>

                        <img class="block__video__fallback"
                             src="images/indexImage/image2.png"
                             srcset="images/indexImage/mond of copenhagen store2.png 225w, images/indexImage/scabal-luxury-finest-cloth-768x1024.jpg 768w, images/indexImage/scabal-luxury-finest-cloth-450x600.jpg 450w, images/indexImage/scabal-luxury-finest-cloth-600x800.jpg 600w, images/indexImage/scabal-luxury-finest-cloth-800x1067.jpg 800w, images/scabal-luxury-finest-cloth-900x1201.jpg 900w, images/indexImage/scabal-luxury-finest-cloth-1200x1601.jpg 1200w, images/indexImage/scabal-luxury-finest-cloth-1600x2134.jpg 1600w"
                             sizes="(min-width: 48.75em) 50vw,
            100vw"
                             alt="scabal-luxury-finest-cloth"/>
                    </div>

                    <div class="block__title">
                        <h2>FABRICS</h2>
                        <h3> A PASSION OF CLOTHES </h3>
                    </div>
                </div>

                <div class="block block--text" data-fit-text>
                    <div class="block--text__wrapper">
                        <span class="divider"></span>
                        <p>Each season our team of designers and artisan weavers create collections of the finest luxury
                            fabrics at our mill. Each is the perfect fit for tailored suits that prioritise
                            aesthetic, comfort and function.</p>
                        <span class="divider"></span>
                        <a href="fabric.php">Fabrics Brands</a>
                    </div>
                </div>

                <div class="block block--image fabrics__collection">
                    <img class="block--image__background"
                         src="images/indexImage/scabal-luxury-fabrics-huddersfield-1024x768.jpg"
                         srcset="images/indexImage/scabal-luxury-fabrics-huddersfield-300x225.jpg 300w, images/indexImage/scabal-luxury-fabrics-huddersfield-768x576.jpg 768w, images/indexImage/scabal-luxury-fabrics-huddersfield-1024x768.jpg 1024w, images/indexImage/scabal-luxury-fabrics-huddersfield-450x337.jpg 450w, images/indexImage/scabal-luxury-fabrics-huddersfield-600x450.jpg 600w, images/indexImage/scabal-luxury-fabrics-huddersfield-800x600.jpg 800w, images/indexImage/scabal-luxury-fabrics-huddersfield-900x675.jpg 900w, images/indexImage/scabal-luxury-fabrics-huddersfield-1200x900.jpg 1200w, images/indexImage/scabal-luxury-fabrics-huddersfield-1600x1199.jpg 1600w"
                         sizes="(min-width: 48.75em) 50vw,
            100vw"
                         alt="scabal-luxury-fabrics-huddersfield"/>
                </div>
            </section>
            <section class="club">
                <div class="block block--video">
                    <div class="block__overlay block__video">
                        <video autoplay
                               loop muted
                               data-width="1080"
                               data-height="600"
                               data-heightm="2">

                            <source src="video/Gentlemen-2.mp4" type="video/mp4">
                        </video>

                        <img class="block__video__fallback"
                             src="images/indexImage/scabal-savile-row-london-store.jpg"
                             srcset="images/indexImage/scabal-savile-row-london-store-300x259.jpg 300w, images/indexImage/scabal-savile-row-london-store-768x663.jpg 768w, images/indexImage/scabal-savile-row-london-store-450x389.jpg 450w, images/indexImage/scabal-savile-row-london-store-600x518.jpg 600w, images/indexImage/scabal-savile-row-london-store-800x691.jpg 800w, images/indexImage/scabal-savile-row-london-store-900x777.jpg 900w, images/indexImage/scabal-savile-row-london-store.jpg 944w"
                             sizes="(min-width: 48.75em) 50vw,
                100vw"
                             alt="scabal-savile-row-london-store"/>
                    </div>

                    <div class="block__title">
                        <h2>Gentlemen Club</h2>
                        <h3>Begin your sartorial experience</h3>
                    </div>
                </div>

                <div class="block block--text block--small" data-fit-text>
                    <div class="block--text__wrapper">
                        <span class="divider"></span>
                        <p>The First Industrial Revolution: The invention of the steam engine ushered in the industrial
                            age. The Second Industrial Revolution: The invention of industrial pipeline operations by
                            Ford.Our platform of C2Mbusiness model makes everyone a designer, everyone is a consumer,
                            everyone is operator, this new commercial civilization is upcoming. </p>
                        <span class="divider"></span>
                        <a href="contact.php">Register Now</a>
                    </div>
                </div>

                <div class="block block--image block--small">
                    <img class="block--image__background"
                         src="images/indexImage/scabal-savile-row-tailor.jpg"
                         srcset="images/indexImage/scabal-savile-row-tailor-300x282.jpg 300w, images/indexImage/scabal-savile-row-tailor-768x721.jpg 768w, images/indexImage/scabal-savile-row-tailor-450x423.jpg 450w, images/indexImage/scabal-savile-row-tailor-600x563.jpg 600w, images/indexImage/scabal-savile-row-tailor-800x751.jpg 800w, images/indexImage/scabal-savile-row-tailor.jpg 868w"
                         sizes="(min-width: 48.75em) 25vw,
             100vw"
                         alt="scabal-savile-row-tailor"/>
                </div>

                <div class="block block--image block--small">
                    <img class="block--image__background"
                         src="images/indexImage/scabal-tailor-london.jpg"
                         srcset="images/indexImage/scabal-tailor-london-300x241.jpg 300w, images/indexImage/scabal-tailor-london-768x618.jpg 768w, images/indexImage/scabal-tailor-london-450x362.jpg 450w, images/indexImage/scabal-tailor-london-600x483.jpg 600w, images/indexImage/scabal-tailor-london-800x644.jpg 800w, images/indexImage/scabal-tailor-london-900x724.jpg 900w, images/indexImage/scabal-tailor-london.jpg 1014w"
                         sizes="(min-width: 48.75em) 25vw,
             100vw"
                         alt="scabal-tailor-london"/>
                </div>
            </section>
            <section class="heritage">
                <div class="block block--video">
                    <div class="block__overlay block__video">
                        <video autoplay
                               loop muted
                               data-width="840"
                               data-height="690">

                            <source src="video/heritage-loop-1.mp4" type="video/mp4">
                        </video>

                        <img class="block__video__fallback"
                             src="images/indexImage/proposition-header-e1471533342692.jpg"
                             srcset="images/indexImage/proposition-header-e1471533342692-300x167.jpg 300w, images/indexImage/proposition-header-e1471533342692-768x429.jpg 768w, images/indexImage/proposition-header-e1471533342692-450x251.jpg 450w, images/indexImage/proposition-header-e1471533342692-600x335.jpg 600w, images/indexImage/proposition-header-e1471533342692-800x446.jpg 800w, images/indexImage/proposition-header-e1471533342692-900x502.jpg 900w, images/indexImage/proposition-header-e1471533342692.jpg 1000w"
                             sizes="(min-width: 48.75em) 50vw,
                100vw"
                             alt="proposition-header"/>
                    </div>

                    <div class="block__title">
                        <h2>REVOLUTION</h2>
                        <h3>Industrial Revolution</h3>
                    </div>
                </div>

                <div class="block block--text" data-fit-text>
                    <div class="block--text__wrapper">
                        <span class="divider"></span>
                        <p>The Industrial Revolution: The deep combination of the digitalization and industrialization,
                            making personalized products with the industrial means, deep to meet the social progress and
                            human civilization</p>
                        <span class="divider"></span>
                        <a href="about.php">Learn more about sftailor</a>
                    </div>
                </div>

                <div class="block block--text heritage__fact">
                    <div class="block--text__wrapper">
                        <h3>Producing</h3>
                        <p>the best suits in most efficient way</p>
                    </div>
                </div>

                <div class="block block--image heritage__mobile-picture">
                    <img class="block--image__background"
                         src="images/indexImage/proposition-header-e1471533342692.jpg"
                         srcset="images/indexImage/proposition-header-e1471533342692-300x167.jpg 300w, images/indexImage/proposition-header-e1471533342692-768x429.jpg 768w, images/indexImage/proposition-header-e1471533342692-450x251.jpg 450w,images/proposition-header-e1471533342692-600x335.jpg 600w, images/indexImage/proposition-header-e1471533342692-800x446.jpg 800w, images/indexImage/proposition-header-e1471533342692-900x502.jpg 900w, images/proposition-header-e1471533342692.jpg 1000w"
                         sizes="100vw"
                         alt="proposition-header"/>
                </div>
            </section>
            <div class="scroll-down">
                <div class="scroll-down__content">
                    <span>Scroll</span>
                    <svg width="16px" height="24px" viewBox="0 0 16 24" version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="scroll-down" transform="translate(0, -65.000000)" fill="#C0AB89">
                            <g transform="translate(-89.000000, -16.000000)" id="scroll-icon">
                                <g transform="translate(90.000000, 81.000000)">
                                    <path d="M13.0909091,7.54925455 L13.0909091,16.2765273 C13.0909091,19.2841636 10.644,21.7310727 7.63636364,21.7310727 C4.62872727,21.7310727 2.18181818,19.2841636 2.18181818,16.2765273 L2.18181818,7.54925455 C2.18181818,4.54161818 4.62872727,2.09470909 7.63636364,2.09470909 C10.644,2.09470909 13.0909091,4.54161818 13.0909091,7.54925455 L13.0909091,7.54925455 Z M6.54545455,0.000163636364 C2.85109091,0.531981818 0,3.7098 0,7.54925455 L0,16.2765273 C0,20.4874364 3.42490909,23.9128909 7.63636364,23.9128909 C11.8467273,23.9128909 15.2727273,20.4874364 15.2727273,16.2765273 L15.2727273,7.54925455 C15.2727273,3.7098 12.4216364,0.531981818 8.72727273,0.000163636364 L6.54545455,0.000163636364 Z"
                                          id="mouse-outline"></path>
                                    <path d="M6.54545455,6.45834545 L6.54545455,8.64016364 C6.54545455,9.24234545 7.03363636,9.73107273 7.63636364,9.73107273 C8.23854545,9.73107273 8.72727273,9.24234545 8.72727273,8.64016364 L8.72727273,6.45834545 C8.72727273,5.85561818 8.23854545,5.36743636 7.63636364,5.36743636 C7.03363636,5.36743636 6.54545455,5.85561818 6.54545455,6.45834545"
                                          id="mouse-button"></path>
                                </g>
                            </g>
                        </g>
                    </svg>
                    <span>Down</span>
                    <svg width="10px" height="42px" viewBox="0 0 10 21" version="1.1" xmlns="http://www.w3.org/2000/svg"
                         xmlns:xlink="http://www.w3.org/1999/xlink">
                        <g id="scroll-down-arrow" fill="#C0AB89" transform="translate(-2,-5) rotate(90, 11, 11)">
                            <g id="scroll-down-arrow-shape">
                                <path d="M24.857891,14.8491619 C25.0507278,14.6515042 25.0468196,14.3349458 24.8491619,14.142109 C24.6515042,13.9492722 24.3349458,13.9531804 24.142109,14.1508381 L20.142109,18.2508385 C19.9492722,18.4484962 19.9531804,18.7650546 20.1508381,18.9578914 C20.3484958,19.1507282 20.6650542,19.14682 20.857891,18.9491623 L24.857891,14.8491619 Z"
                                      id="oblicBottom"></path>
                                <path d="M5.5,14 C5.22385763,14 5,14.2238576 5,14.5 C5,14.7761424 5.22385763,15 5.5,15 L24.5,15 C24.7761424,15 25,14.7761424 25,14.5 C25,14.2238576 24.7761424,14 24.5,14 L5.5,14 Z"
                                      id="horizontalLine"></path>
                                <path d="M24.1464466,14.8535534 C24.3417088,15.0488155 24.6582912,15.0488155 24.8535534,14.8535534 C25.0488155,14.6582912 25.0488155,14.3417088 24.8535534,14.1464466 L20.8535534,10.1464466 C20.6582912,9.95118446 20.3417088,9.95118446 20.1464466,10.1464466 C19.9511845,10.3417088 19.9511845,10.6582912 20.1464466,10.8535534 L24.1464466,14.8535534 Z"
                                      id="oblicTop"></path>
                            </g>
                        </g>
                    </svg>
                </div>
            </div>
        </div><!-- end .container -->
    </main><!-- end #wrapper -->


    <!-- footer start here -->
    <footer class="site-footer footer" role="contentinfo">
        <div class="footer__hq">
            <img src="images/logo/logo.png" style="height: 80px;"/>

            <!--  <address>
             Rue du chantier | Werfstraat 5<br>
             1000 Bruxelles | Brussel<br>
             Belgium        </address> -->
        </div>

        <div class="footer__quicklinks">
            <p class="subtitle h3">Quick links</p>
            <nav>
                <ul id="menu-footer-en" class="menu">
                    <li id="menu-item-5078"
                        class="menu-item menu-item-type-post_type 	menu-item-object-page menu-item-5078">
                        <a href="index.php">Home</a>
                    </li>
                    <li id="menu-item-38" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-38">
                        <a href="about.php">About Us</a></li>
                    <li id="menu-item-5079"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-5079"><a
                                href="fullbespoke.php">Fabric</a></li>
                    <li id="menu-item-1251"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1251"><a
                                href="accessories.php">Accessories</a></li>
                    <li id="menu-item-1251"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1251"><a
                                onclick="pageRedirect('faq')">FAQ</a></li>
                    <li id="menu-item-36" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-36">
                        <a href="customize.php">customize</a></li>
                    <li id="menu-item-1280"
                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1280"><a
                                href="customfit">Advantage</a></li>
                    <li id="menu-item-970"
                        class="menu-item menu-item-type-post_type menu-item-object-story menu-item-970"><a
                                href="customize.php">Order Now</a></li>

                    <li id="menu-item-969"
                        class="menu-item menu-item-type-post_type menu-item-object-story menu-item-969"><a
                                href="contact.php">Contact Us</a></li>
                    <li id="menu-item-969"
                        class="menu-item menu-item-type-post_type menu-item-object-story menu-item-969"><a
                                onclick="pageRedirect('terms')">Sales Terms</a></li>
                    <li id="menu-item-969"
                        class="menu-item menu-item-type-post_type menu-item-object-story menu-item-969"><a
                                onclick="pageRedirect('privacyPolicy')">Privacy Policy</a></li>
                </ul>
            </nav>
        </div>
        <div class="gallery agileinfo" id="partner">
            <div style="width: 400px;">
                <p class="subtitle h3">Awards/Members</p>
                <div class="awardSlider">

                </div>
            </div>
        </div>
        <div class="footer__connect">
            <div class="footer__newsletter">
                <p class="subtitle h3">Newsletter</p>

                <p class="footer__newsletter__feedback"></p>

                <form action="#" type="POST" method="POST">
                    <input type="hidden" name="action" value="newsletter_form"/>
                    <input type="email" name="email" placeholder="Your email address" autocomplete="off"/>
                    <button type="submit">Join</button>
                </form>
            </div>

            <div class="footer__social">
                <p class="subtitle h3">Follow us</p>
                <ul>
                <?php
                include('admins/api/Classes/CONNECT.php');
                include('admins/api/Constants/DbConfig.php');
                include('admins/api/Constants/configuration.php');
                include('admins/api/Classes/ADMIN.php');
                $conn = new \Classes\CONNECT();
                $userClass = new \Classes\ADMIN();
                $getSocialLinks = $userClass->getAllSocialLinks();
                $allLink = $getSocialLinks['socialData'];
                for($k=0;$k<count($allLink);$k++){
                    $name = $allLink[$k]['social_name'];
                    if($name =="Facebook"){ ?>
                        <li><a href="<?php echo $allLink[$k]['social_link'];?>" target="_blank"><i class="fa fa-facebook-square" aria-hidden="true"></i></a></li>
                     <?php } elseif($name =="Twitter"){ ?>
                        <li><a href="<?php echo $allLink[$k]['social_link'];?>" target="_blank"><i class="fa fa-twitter-square" aria-hidden="true"></i></a></li>
                        <?php } elseif ($name =="Pinterest") { ?>
                        <li><a href="<?php echo $allLink[$k]['social_link'];?>" target="_blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                        <?php } } ?>



                   <!-- <li><a href="#" target="_blank"><i class="fa fa-google-plus-square" aria-hidden="true"></i>
                        </a></li>
                    <li><a href="#" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i>
                        </a></li>-->
                </ul>
            </div>

        </div>
    </footer>

    <script type="x-template" id="fm-controls-html">
        <div class="fm__bar fm__bar--top">
            <a href="#" class="fm__close" data-close-fm><i class="icon-close"></i>Close</a>
        </div>

        <div class="fm__bar fm__bar--bottom">
            <div class='plyr__controls'>
                <button type='button' data-plyr='play'>
                    <i class="icon-play-fill"></i>
                    <span class='plyr__sr-only'>Play</span>
                </button>
                <button type='button' data-plyr='pause'>
                    <i class="icon-pause"></i>
                    <span class='plyr__sr-only'>Pause</span>
                </button>
                <span class='plyr__progress'>
          <label for='seek{id}' class='plyr__sr-only'>Seek</label>
          <input id='seek{id}' class='plyr__progress--seek' type='range' min='0' max='100' step='0.1' value='0'
                 data-plyr='seek'>
          <progress class='plyr__progress--played' max='100' value='0' role='presentation'></progress>
          <progress class='plyr__progress--buffer' max='100' value='0'>
              <span>0</span>% buffered          </progress>
          <span class='plyr__tooltip'>00:00</span>
      </span>
                <span class='plyr__time'>
          <span class='plyr__sr-only'>Current time</span>
          <span class='plyr__time--current'>00:00</span>
      </span>
                <button type='button' data-plyr='mute'>
                    <svg class='icon--muted'>
                        <use xlink:href='#plyr-muted'></use>
                    </svg>
                    <svg>
                        <use xlink:href='#plyr-volume'></use>
                    </svg>
                    <span class='plyr__sr-only'>Toggle Mute</span>
                </button>
            </div>
        </div>
    </script>
</div> <!-- end .site -->

<section class="wechat" data-wechat-popup data-prevent-scroll="true">
    <div class="wechat__layer"></div>
    <div class="wechat__box">
        <div class="wechat__close">
            <span class="icon-close"></span>
            <span>Close</span>
            <a href="#"></a>
        </div>

        <img src=""
             alt="WeChat QR code"/>
    </div>
</section>

<div id="transition-block1" class="transition-panel transition-panel--white"></div>
<!-- <div id="transition-block2" class="transition-panel transition-panel--gold"></div> -->

<div class="needle-loading">
    <svg id="loadingScabal" x="0px" y="0px" viewBox="0 0 130 30" width="120px" height="30px"
         style="background-color:transparent;" xml:space="preserve">
      <style type="text/css">
          .lst5 {
              fill: #766345;
          }

          .lst6 {
              fill: none;
              stroke: #766345;
              stroke-width: 1;
              stroke-miterlimit: 10;
          }

          .lst7 {
              fill: none;
              stroke: #C0AB89;
              stroke-width: 2;
              stroke-miterlimit: 10;
          }
      </style>
        <path id="needle" class="lst5"
              d="M5.08,0C2.28,0,0,.17,0,1.5S2.28,3,5.08,3C7,3,90,1.47,90,1.47S7,0,5.08,0ZM4.55,2.12C3.18,2.12,1.62,2,1.62,1.46S3.22,0.8,4.55.8C7,0.8,10.6.92,10.6,1.46S7,2.12,4.55,2.12Z"
              transform="translate(43,15)"/>
        <polyline id="sinusoide" class="lst6" points="0,0 0,0 0,0 0,0"/>
    </svg>
</div>

<div class="loading-text" id="loadingText">Loading</div>
<div id="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog"
     aria-labelledby="gridSystemModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-4">.col-md-4</div>
                    <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script src="js/raven.min.js"></script>
<script src="js/jquery-2.1.4.min.js"></script>
<script>
    Raven.config('https://c8ccdb6a5e134f468b11b2d5e0972536@sentry.svc.epic-sys.io/4').install()
</script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-25559079-1', 'auto');
    ga('send', 'pageview');
</script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var SCABALINFO = {
        "BASEURL": "http:\/\/www.scabal.com\/wp-content\/themes\/scabal",
        "ADMIN_BAR_VISIBLE": "",
        "FACEBOOK_KEY": "1670372613289998",
        "GOOGLE_MAPS_KEY": "AIzaSyDPH0ECl3UQNoffqB-r1KklDllaTVRTFWI",
        "CURRENT_LANG": "en",
        "CURRENT_LOCATION": "BE"
    };
    /* ]]> */


    function pageRedirect(pageName) {
        if (pageName == "sdet") {
            var user_id = $("#user_id").val();
            var url = "api/registerUser.php";
            $.post(url, {"type": "isStyleGenieUsed", "user_id": user_id}, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    window.location = 'sdet.php';
                } else {
                    window.location = 'sizematching.php';
                }
            });
        } else {
            window.location = pageName + ".php";
        }
    }
    function orderRedirect(pageName) {
        window.location = pageName + ".php?order";
    }
    function userPersonal() {

        var user_id = $("#user_id").val();
        if (user_id != "") {
            var url = "admin/webserver/register.php?type=get_billing&user_id=" + user_id;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    var items = json.items;
                    var item = items[0];
                    var email = item.email;

                    var name = item.first_name +" "+ item.last_name;
                    if(item.first_name == "" && item.last_name == ""){
                        name = email.substr(0,5);
                    }
                    $("#username").html("&nbsp;" + name);
                    $("#profile").attr("src", item.profile_pic);
                    getCart(user_id);
                }
            }).fail(function () {
                alert("failed");
            });
        }
    }
    function getCart(user_id) {
        var url = "admin/webserver/getcart.php?user_id=" + user_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                var cartshow = "";
                if (status == "done") {
                    var items = json.items;
                    var count = items.length;
                    $(".bagde").html(count);
                }
                else {
                    //alert('error');
                }
            }
        });
    }
    function uploadOrdFromIndex() {
        window.location = "sizematching.php?up";
    }
    function myfilesFromIndex() {
        window.location = "sizematching.php?mf";
    }
    function viewSavedfromIndex() {
        window.location = "sizematching.php?sm";
    }
    function showMessage(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
        $(".modal-footer").css({"display": "none"});
        $("#modal").modal("show");
        setTimeout(function () {
            $("#modal").modal("hide");
        }, 3000);
    }
    userPersonal();
    /*check twin ginny complete or not*/
    function bodyStyle(){
        window.location= "sizematching.php?bodyStyle"
    }
    function checkMeasurement(){
        window.location="check_measurement.php"
    }



    /*awards data start  here*/
    function getBrandData(){
        var url = "admins/api/brandLogoProcess.php";
        $.post(url,{"type":"getLogo"},function(data){
            var status = data.Status;
            var logoData = data.data;
            if(status == "Success"){
                var logoBox = "";
                for(var a= 0; a < logoData.length; a++){
                    logoBox = logoBox+'<li><img src="admins/api/Files/images/'+logoData[a].logo_file+'" /></li>';
                }
                logoBox = '<ul id="brandLogo">'+logoBox+'</ul>';
                $(".awardSlider").html(logoBox);
                    $("#brandLogo").flexisel({
                        visibleItems: 4,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint:480,
                                visibleItems: 1
                            },
                            landscape: {
                                changePoint:640,
                                visibleItems: 2
                            },
                            tablet: {
                                changePoint:768,
                                visibleItems: 3
                            }
                        }
                    });
            }
            else{
                showMessage(data.Message, "red");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
        });
    }
    getBrandData();

    /*awards data end   here*/
</script>
<script type="text/javascript" src="js/jquery.flexisel.js"></script>
<script type='text/javascript' src='js/main.js'></script>
</body>
</html>



<!-- footer end   here --> 