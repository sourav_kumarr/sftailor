<?php
require_once '../admin/webserver/database/db.php';
include('api/Classes/CONNECT.php');
include('api/Constants/DbConfig.php');
$conn = new \Classes\CONNECT();
$link = $conn->connect();//for scan2tailor
$userId = $_REQUEST['uid'];
$order_id = $_REQUEST['oid'];
$qq = "select * from wp_customers where user_id = '$userId'";
$rr = mysql_query($qq);
$num = mysql_num_rows($rr);
if($num>0)
{
    $invoiceData = mysql_fetch_assoc($rr);
    $customerName = $invoiceData['name'];
    $addressMain = $invoiceData['address'];
    $bcountry = $invoiceData['b_country'];
    $bstate = $invoiceData['b_state'];
    $bcity = $invoiceData['b_city'];
    $pincode = $invoiceData['pincode'];
    $email = $invoiceData['email'];
    $user_id = $invoiceData['user_id'];
}
else{
    echo "error";
}

if ($link) {
    $query = "select * from wp_orders where order_number='$order_id'";
    $result = mysqli_query($link, $query);
    if ($result) {
        $num = mysqli_num_rows($result);
        if ($num > 0) {
            $orderData = mysqli_fetch_assoc($result);
            $orderid = $orderData['order_id'];
            $order_date = $orderData['order_date'];
            $order_state = $orderData['order_state'];
            $order_payment_status = $orderData['order_payment_status'];
            $order_number = $orderData['order_number'];
            $order_total = $orderData['order_total'];
            $paid_amount = $orderData['paid_amount'];
            $coupon = $orderData['coupon'];
            $store_name = $orderData['store_name'];
            $query = "select * from wp_orders_detail where det_order_id='$orderid'";
            $result1 = mysqli_query($link, $query);
                if ($result1) {
                    $num = mysqli_num_rows($result1);
                    if ($num > 0) {
                        $orderDataf = mysqli_fetch_assoc($result1);
                        $det_product_name = $orderDataf['det_product_name'];
                        $det_quantity = $orderDataf['det_quantity'];
                        $det_price = $orderDataf['det_price'];
                }
            }
        }
    }
}

?>
<!DOCTYPE html>
<html class="nextcontemporary read-only" id="ReadOnly">
<head>
    <link rel="stylesheet" href="css/invoices.css" type="text/css">
    <style type="text/css">
        .accent-text-color {
        color: #020003 !important;
        }
        .accent-bg-color {
        background: #020003 !important;
        color: #fff !important;
        }
        .accent-bg-color * {
        color: inherit !important;
        }
        .separate-info tr td:nth-child(2) {
        text-align: right !important;
        }
        .border-top {
        border-top: 1px solid #ccc !important;
        }
        .border-bottom {
        border-bottom: 1px solid #ccc !important;
        }
        .pdf-export #NextContemporary, .read-only-view #NextContemporary {
        min-height: 732px;
        }
        .pdf-export #NextContemporary .contemporary-template__header__logo img, .read-only-view #NextContemporary .contemporary-template__header__logo img {
        max-height: 180px;
        }
        .pdf-export, .read-only-view {
        margin-bottom: 0px;
        }
        #ReadOnlyMain #ReadOnlyView {
        margin-bottom: 0px;
        }
        #ReadOnlyMain.horizontal-redesign #ReadOnlyView {
        background: #202020 none repeat scroll 0 0;
        }
    </style>


<body class="nextcontemporary">
<div id="Container" class=" has-payments-callout">
    <div id="ReadOnlyMain" class=" horizontal-redesign">
        <div class="iframe-parent">
            <div class="container-fluid non-mobile">
                <div class="row-fluid">
                    <div id="ReadOnlyView" class="span12 read-only-view">
                        <a href="ordersp.php" style="float:left;"><button class='btn btn-sm btn-primary' style="border-radius: 2px;background:#ddd;margin-left: 11px;margin-top: 14px;">Back</button></a>
                        <!-- invoice start -->
                        <div id="NextContemporary" class="export-template">
                            <section class="contemporary-template__header">
                                <div class="contemporary-template__header__logo">
                                    <img class="contemporary-template__business-logo" src="images/logoimg.jpg">
                                </div>
                                <div class="contemporary-template__header__info">
                                    <div class="wv-heading--title">INVOICE</div>
                                    <span class="wv-text--strong">SF Tailors, LLC</span>
                                    <div class="contemporary-template__header__info__address">
                                        2500 Reliance Avenue<br>
                                        Apex, NC 27539<br>
                                        United States<br>
                                        <br>
                                        Mobile: 919-656-7327<br>
                                        <span class="wrappable">www.sftailors.com<br></span>
                                   </div>
                                </div>
                            </section>
                            <div class="contemporary-template__divider contemporary-template__divider--full-width"></div>
                            <section class="contemporary-template__metadata">
                                <div class="contemporary-template__metadata__customer">
                                    <div class="contemporary-template__metadata__customer--billing">
                                        <div class="contemporary-template__metadata__customer__address-header">BILL TO
                                        </div>
                                        <span class="wv-text--strong"><?php echo $customerName;?></span>
                                        <div class="contemporary-template__metadata__customer__address">
                                            <?php echo $addressMain;?><br>
                                            <?php echo $bstate;?>,
                                            <?php echo $bcity;?>, <?php echo $pincode;?><br>
                                            <?php echo $bcountry;?><br>
                                            <br>
                                        </div>
                                    </div>
                                </div>

                                <div class="invoice-template-details">
                                    <table class="wv-table">
                                        <tbody>
                                        <tr class="wv-table__row">
                                            <td class="wv-table__cell">
                                                <strong class="wv-text--strong">Invoice Number:</strong>
                                            </td>
                                            <td class="wv-table__cell">
                                                <span><?php echo $order_number;?></span>
                                            </td>
                                        </tr>
                                        <tr class="wv-table__row">
                                            <td class="wv-table__cell">
                                                <strong class="wv-text--strong">Invoice Date:</strong>
                                            </td>
                                            <td class="wv-table__cell">
                                                <span><?php echo $order_date;?></span>
                                            </td>
                                        </tr>
                                        <tr class="wv-table__row">
                                            <td class="wv-table__cell">
                                                <strong class="wv-text--strong">Payment Due:</strong>
                                            </td>
                                            <td class="wv-table__cell">
                                                <span><?php echo $order_date;?></span>
                                            </td>
                                        </tr>
                                        <tr class="wv-table__row">
                                            <td class="wv-table__cell">
                                              <span class="wv-text--strong">
                                                Amount Due (USD):
                                              </span>
                                            </td>
                                            <td class="wv-table__cell">
                                            <span class="wv-text--strong">
                                               <?php echo $paid_amount;?>
                                            </span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </section>

                            <div class="contemporary-template__items">
                                <table class="wv-table">
                                    <thead class="wv-table__header" style="background-color: #020003;">
                                    <tr class="wv-table__row">
                                        <th class="wv-table__cell" colspan="4" style="color: #FFFFFF;">Items</th>
                                        <th class="wv-table__cell--smaller contemporary-template__items__column--center"
                                            colspan="1" style="color: #FFFFFF;">Quantity
                                        </th>
                                        <th class="wv-table__cell--amount" colspan="1" style="color: #FFFFFF;">Price
                                        </th>
                                        <th class="wv-table__cell--amount" colspan="1" style="color: #FFFFFF;">Amount
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody class="wv-table__body">

                                    <tr class="wv-table__row">
                                        <td class="wv-table__cell" colspan="4">
                                            <p class="wv-table__cell--nested">
                                                <?php echo $det_product_name;?>
                                            </p>
                                        </td>
                                        <td class="wv-table__cell contemporary-template__items__column--center"
                                            colspan="1">
                                            <span><?php echo $det_quantity;?></span>
                                        </td>
                                        <td class="wv-table__cell--amount" colspan="1">
                                            <span><?php echo $det_price;?></span>
                                        </td>
                                        <td class="wv-table__cell--amount" colspan="1">
                                            <span><?php echo $det_price;?></span>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="contemporary-template__divider contemporary-template__divider--full-width contemporary-template__divider--bold"></div>
                            <div>
                                <section class="contemporary-template__totals">
                                    <div class="contemporary-template__totals__blank"></div>
                                    <div class="contemporary-template__totals__amounts">
                                        <div class="contemporary-template__totals__amounts__line">
                                            <div class="contemporary-template__totals__amounts__line__label">
                                                <strong>Subtotal:</strong>
                                            </div>
                                            <div class="contemporary-template__totals__amounts__line__amount">
                                                <span><?php echo $det_price;?></span>
                                            </div>
                                        </div>
                                        <div class="contemporary-template__totals__amounts__line">
                                            <div class="contemporary-template__totals__amounts__line__label">
                                                <span>NC Sales 7.25%</span>:
                                            </div>
                                            <div class="contemporary-template__totals__amounts__line__amount">
                                              <span>
                                                $7.25
                                              </span>
                                            </div>
                                        </div>
                                        <div class="contemporary-template__divider contemporary-template__divider--small-margin"></div>
                                        <div class="contemporary-template__totals__amounts__line">
                                            <div class="contemporary-template__totals__amounts__line__label">
                                                <strong>Total:</strong>
                                            </div>
                                            <div class="contemporary-template__totals__amounts__line__amount">
                                                <wavetext>
                                                    <?php echo $paid_amount;?>
                                                </wavetext>
                                            </div>
                                        </div>
                                        <div>
                                            <div class="contemporary-template__totals__amounts__line">
                                                <div class="contemporary-template__totals__amounts__line__label">
                                                    <strong>
                                                        Amount Due
                                                        (USD)
                                                        :
                                                    </strong>
                                                </div>
                                                <div class="contemporary-template__totals__amounts__line__amount">
                                                    <strong>
                                                        <?php echo $paid_amount;?>
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                        </div>
                        <!-- invoice end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>