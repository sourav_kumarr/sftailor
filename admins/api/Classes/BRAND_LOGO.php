<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 10/24/2017
 * Time: 11:21 AM
 */


namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class BRAND_LOGO
{
    private $pdf_id;
    public $link = null;
    public $userClass = null;
    public $response = array();

    public function __construct()
    {
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
    }
    public function addLogo() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $fileResponse = $this->link->storeImage('file_','images');
        if($fileResponse[Status] === Error) {
            return $fileResponse;
        }
        $file_name = $fileResponse["File_Name"];
        $file_name = rawurlencode($file_name);
        $file_path = $fileResponse["File_Path"];
        $file_path = rawurlencode($file_path);
        $pdf_title = explode('.',$file_name)[0];
        $fileExists = $this->isFileExists($file_name);

        if($fileExists[Status] === Error) {
            return $fileExists;
        }

        $getMaxSort = "select MAX(sort_order)  AS sort_max from brand_logo";
        $getMaxSort_result = mysqli_query($link,$getMaxSort);

        if($getMaxSort_result) {
            $sort_max = 0;
            $sortNum = mysqli_num_rows($getMaxSort_result);
            if($sortNum > 0){
                $sort_row = mysqli_fetch_assoc($getMaxSort_result);
                $sort_max = $sort_row["sort_max"];
                $sort_max++;
            }
            $query = "insert into brand_logo(logo_title, logo_file,logo_file_path,sort_order,status)
            values('$pdf_title','$file_name','$file_path','$sort_max','1')";
            $result = mysqli_query($link,$query);
            if(!$result) {
                $this->response[STATUS] = $sortNum;
                $this->response[MESSAGE] = $this->link->sqlError();
                return $this->response;
            }
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "File uploaded successfully";
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function isFileExists($file_name) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from brand_logo where logo_file='$file_name'";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $this->response[STATUS] = Error;
        $this->response[MESSAGE] = "File already exists with this name ".$file_name;
        $this->response["data"] = $data;
        return $this->response;
    }
    public function getLogo() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from brand_logo ORDER BY sort_order";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;

    }
    public function updateSortOrder($sortOrder,$idOrder) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        for($j = 0;$j < count($idOrder); $j++){
            $update_sort = "update brand_logo set sort_order= '$sortOrder[$j]' where id = '$idOrder[$j]' ";
            $result = mysqli_query($link,$update_sort);
            $count = mysqli_affected_rows($link);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update_sort;

                return $this->response;
            }
        }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }
    public function deleteLogo($id,$path) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from brand_logo where id='$id'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        $this->response["data"] = $data;
        $path = rawurldecode($path);
        unlink("Files/images/".$path);
        return $this->response;

    }
    public function addCollar() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $fileResponse = $this->link->storeImage('file_','images/collar');
        if($fileResponse[Status] === Error) {
            return $fileResponse;
        }
        $file_name = $fileResponse["File_Name"];
        $file_name = rawurlencode($file_name);
        $collar = explode(".",$file_name);
        $collar_name = $collar[0];
        $collar_id  = str_replace('_', '', $collar_name);
        $description  = str_replace('_', ' ', $collar_name);
        $fileExists = $this->isFileExists($file_name);


        if($fileExists) {
            $query = "insert into shirt_collars(collar_name,collar_description,collar_id,collar_img_path,status)
            values('$collar_name','$description','$collar_id','$file_name','1')";
            $result = mysqli_query($link,$query);
            if(!$result) {
                $this->response[STATUS] = $sortNum;
                $this->response[MESSAGE] = $this->link->sqlError();
                return $this->response;
            }
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "File uploaded successfully";
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getCollars() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from shirt_collars ORDER BY id desc";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;

    }


    public function getPartClrs($btnId){
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $query = "select * from shirt_collars where id ='$btnId'";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $rows = mysqli_fetch_assoc($result);

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["btnData"] = $rows;
        return $this->response;
    }

    public function editCollars($clrId,$collar_name,$collar_description,$oldClrImagechanged,$oldClrImage) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldClrImagechanged == "yes") {
            if(isset($_FILES['new_clr_img'])) {
                $file_tmp = $_FILES['new_clr_img']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['new_clr_img']['name'])));
                if (in_array($file_ext, $extensions) === true) {

                    $db_file_name = $collar_name."_".rand(0000, 9999) . strtotime(date("d M Y h:i:s A")) . "." . $file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/collar/".$db_file_name)) {
                        $clrImg = $db_file_name;
                        $pathImg = "https://scan2fit.com/sftailor/admins/api/Files/collar/".$db_file_name;
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Server Error to Upload File";
                        return $this->response;
                    }
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }
        else{
            $clrImg = $oldClrImage;
        }
        $query = "update shirt_collars set collar_name='$collar_name', collar_description = '$collar_description',collar_img_path='$clrImg' where id = '$clrId'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "Unable to update data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }

    public function deleteCollar($id) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from shirt_collars where id='$id'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        $this->response["data"] = $data;
        return $this->response;

    }

    public function updateCollarsStatus($collar_id,$clrStatus) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
            $update = "update shirt_collars set status= '$clrStatus' where id = '$collar_id' ";
            $result = mysqli_query($link,$update);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update;

                return $this->response;
            }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }



    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }

}