<?php
include('header.php');
include("admin/webserver/database/db.php");
?>
</div>
<div class="about-bottom wthree-3">
    <style>

        .active {
            border: 2px solid #555;
            border-radius: 4px;
        }
        .primarysuggestion {
            height: 150px;
            margin: 0 10px 10px 0;
            width: 150px;
        }
        .primarysuggestionl {
            height: auto !important;
            margin: 0 10px 10px 0;
            width: 150px;
        }
        .primarysuggestion img{
            width: 65% !important;
        }
        .primarysuggestionl img{
            height: auto !important;
            margin: 0 10px 10px 0;
            width: 150px;
        }
        .steps {
            background: #fff none repeat scroll 0 0;
            border: 2px solid #aeaeae;
            border-radius: 7%;
            cursor: pointer;
            float: left;
            font-size: 13px;
            line-height: 18px;
            margin-bottom: 20px;
            margin-left: 2%;
            margin-top: 10px;
            max-height: 57px;
            padding-top: 9px;
            text-align: center;
            width: 10%;
        }
        .stepsactive {
            background: #c4996c none repeat scroll 0 0;
            border: 2px solid #c4996c;
            color: white;
        }
        .breakingline {
            width: 525px;
            margin-top: 38px;
        }
        .outputsugg {
            position: absolute;
            margin-left: -70px;
            z-index:-1;
        }
        .backbtnsugg
        {
            height:68%;
        }
        .backbtnsuggparentdiv
        {
            height: 130px;
            width: 130px;
        }
        .backbtnsuggparentdiv p
        {
            font-weight:normal;
            font-size: 12px;
            line-height: 18px!important;
        }
        #forcasual .primarysuggestion > img
        {
            padding: 40px;
        }
        #frontpockets{overflow-y: scroll; height: 660px; padding-top: 22px;}
        .colorsuggestion {
            height: 100px!important;
            width: 100px!important;
        }

        .data_{
            margin-top: 2px;
            padding: 0px;
            border: 1px lightgrey solid;
            border-radius: 5px;
            position: absolute;
            max-height: 100px;
            overflow: auto;
            width: 98%;
            z-index: 9999;
        }

        .data_ li{
            padding: 5px;
            border-bottom: 1px lightgrey solid;
        }
        .data_ li:hover{
            cursor: pointer;
            background-color: silver;
        }
        .data-remove{
            display:none;
        }
    </style>
    <div class="loader" style="display: none">
        <img src="images/spin.gif" class="spinloader"/>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="col-md-1">
                <label onclick="rotateImage('left')" id="rotateLeft" style="margin-top: 350px"><i class="fa fa-share fa-2x"></i></label>
            </div>
            <div class="col-md-10" style="padding-top: 150px;position: relative">
                <img src=""  class="outputsugg" id="pantimg" alt=''/>
                <img src=""  class="outputsugg" id="beltimg" alt=''/>
                <img src=""  class="outputsugg" id="beltloopimg" alt=''/>
                <img src=""  class="outputsugg" id="pleatimg" alt=''/>
                <img src=""  class="outputsugg" id="frontpocketimg" alt=''/>
                <img src=""  class="outputsugg" id="backpocketimg" alt=''/>
                <img src=""  class="outputsugg" id="pantbottomimg" alt=''/>
            </div>
            <div class="col-md-1">
                <label onclick="rotateImage('right')" id="rotateRight" style="margin-top: 350px"><i class="fa fa-reply fa-2x"></i></label>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="padding:50px 0">
                <div class="steps stepsactive" id="category">Pant Category</div>
                <div class="steps" id="frontpocket">Front Pocket</div>
                <div class="steps" id="pleatstyle">Pleats Style</div>
                <div class="steps" id="watchpocket">Watch Pocket</div>
                <div class="steps" id="backpocket" style="width: 14%;">Back Pocket</br>Satin Options</div>
                <div class="steps" id="beltloop">Belt <br>Loop</div>
                <div class="steps" id="pantbottom">Pant Bottom</div>
                <div class="steps" id="pantcolor">Pant <br>Color</div>
                <div class="breakingline"></div>
            </div>
            <!--
            ----category div
            --->
            <div class="col-md-12 stepdiv" id="categorys" style="display: block">
                <p class="stylecaption">Pant Category</p>
                <div class="primarysuggestion primarysuggestionl active" id="category_formal" name="formal" style="margin-left: 140px">
                    <img src="images/pants/formal.jpg"/>
                    <p class="suggestionname">Formal</p>
                </div>
                <div class="primarysuggestion primarysuggestionl" id="category_casual"  name="casual"  style="margin-left: 10px">
                    <img src="images/pants/casual.jpg"/>
                    <p class="suggestionname">Casual</p>
                </div>
            </div>
            <!--
            ----front pocket div
            --->
            <div class="col-md-12 stepdiv" id="frontpockets" style="display:none">
                <p class="stylecaption">Front Pocket Style</p>
                <div id="forcasual" style="display: none">
                    <div class="primarysuggestion" id="frontpocket_arcjeanspocket" name="T-3135:arc_jeans_pocket">
                        <img src="images/pants/arcjeanspocket.png"/>
                        <p class="suggestionname">Arc Jeans Pocket</p>
                    </div>
                    <div class="primarysuggestion" id="frontpocket_diamondjeanspocket" name="T-3134:diamond_jeans_pocket">
                        <img src="images/pants/diamondjeanspocket.png"/>
                        <p class="suggestionname">Diamond Jeans Pocket</p>
                    </div>
                    <div class="primarysuggestion" id="frontpocket_squarejeanspocket" name="T-3133:square_jeans_pocket">
                        <img src="images/pants/squarejeanspocket.png"/>
                        <p class="suggestionname">Square Jeans Pocket</p>
                    </div>
                    <div class="primarysuggestion" id="frontpocket_roundjeanspocket" name="T-3136:round_jeans_pocket">
                        <img src="images/pants/roundjeanspocket.png"/>
                        <p class="suggestionname">Round Jeans Pocket</p>
                    </div>
                </div>
                <div class="primarysuggestion active " id="frontpocket_nopocket" name="T-313C:no_pocket">
                    <img src="images/pants/nopocket.png"/>
                    <p class="suggestionname">T-313C</br>No Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_20cmslantpocket" name="T-3100:2.0_cm_slant_pocket">
                    <img src="images/pants/20cmslantpocket.png"/>
                    <p class="suggestionname">T-3100</br>2.0 cm Slant Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_25slantpocket" name="T-3104:2.5_cm_slant_pocket">
                    <img src="images/pants/25slantpocket.png"/>
                    <p class="suggestionname">T-3104</br>2.5 cm Slant Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_32cmslantpocket" name="T-3101:3.2_cm_slant_pocket">
                    <img src="images/pants/32cmslantpocket.png"/>
                    <p class="suggestionname">T-3101</br>3.2 cm Slant Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_51cmslantpocket" name="T-3102:5.1_cm_slant_pocket">
                    <img src="images/pants/51cmslantpocket.png"/>
                    <p class="suggestionname">T-3102</br>5.1 cm Slant Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_doublebesompocket" name="T-3131:double_besom_pocket">
                    <img src="images/pants/doublebesompocket.png"/>
                    <p class="suggestionname">T-3131</br>Double Besom Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_sideseampocket" name="T-3130:side_seam_pocket">
                    <img src="images/pants/sideseampocket.png"/>
                    <p class="suggestionname">T-3130</br>Side Seam Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_weltpocket" name="T-3132:welt_pocket">
                    <img src="images/pants/weltpocket.png"/>
                    <p class="suggestionname">T-3132</br>Welt Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_veryslantpocketwithsrip_1" name="T-3124:very_slant_pocket_with_strip">
                    <img src="images/pants/veryslantpocketwithsrip.png"/>
                    <p class="suggestionname">T-3124</br>Very Slant Pocket with Strip</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_veryslantpocket" name="T-3103:very_slant_pocket">
                    <img src="images/pants/veryslantpocket.png"/>
                    <p class="suggestionname">T-3103</br>Very Slant Pocket</p>
                </div>
                <div class="primarysuggestion" title="2.0 cm Slant Pocket with Strip" id="frontpocket_20cmslantpocketwithstrip" name="T-3120:2.0_cm_slant_pocket_with_strip">
                    <img src="images/pants/20cmslantpocketwithstrip.png"/>
                    <p class="suggestionname">T-3120</br>2.0 cm Slant Pocket with..</p>
                </div>
                <div class="primarysuggestion" title="3.2 cm Slant Pocket with Strip" id="frontpocket_32cmslantpocketwithstrip" name="T-3121:3.2_cm_slant_pocket_with_strip">
                    <img src="images/pants/32cmslantpocketwithstrip.png"/>
                    <p class="suggestionname">T-3121</br>3.2 cm Slant Pocket with..</p>
                </div>
            </div>
            <!--
            ----Pleats style div
            --->
            <div class="col-md-12 stepdiv" id="pleatstyles" style="display:none">
                <p class="stylecaption">Pleats Style</p>
                <div class="primarysuggestion" id="pleatstyle_plainfront" name="T-3020:plain_front">
                    <img src="images/pants/plainfront.png"/>
                    <p class="suggestionname">T-3020</br>Plain Front</p>
                </div>
                <div class="primarysuggestion" title="Single Pleat Forward Sideseam" id="pleatstyle_singlepleatforwardsideseam_1" name="T-3021:single_pleat_forward_side_seam">
                    <img src="images/pants/singlepleatforwardsideseam.png"/>
                    <p class="suggestionname">T-3021</br>Single Pleat Forward..</p>
                </div>
                <div class="primarysuggestion" id="pleatstyle_singlepleatforwardfly_1" name="T-3022:single_pleat_forward_fly">
                    <img src="images/pants/singlepleatforwardfly.png"/>
                    <p class="suggestionname">T-3022</br>Single Pleat Forward Fly</p>
                </div>
                <div class="primarysuggestion" id="pleatstyle_doublepleatforwardsideseam_1" title="Double Pleat Forward Sideseam" name="T-3023:double_pleat_forward_side_seam">
                    <img src="images/pants/doublepleatforwardsideseam.png"/>
                    <p class="suggestionname">T-3023</br>Double Pleat Forward..</p>
                </div>
                <div class="primarysuggestion" title="One Opposite Pleat one Pleat forward Sideseam" id="pleatstyle_oneoppositepleatonepleatforwardsideseam_1" name="T-3028:one_opposite_pleat_one_pleat_forward_side_seam">
                    <img src="images/pants/oneoppositepleatonepleatforwardsideseam.png"/>
                    <p class="suggestionname">T-3028</br>One Opposite Pleat one..</p>
                </div>
                <div class="primarysuggestion" title="One Opposite Pleat one Pleat Forward Fly" id="pleatstyle_oneoppositepleatonepleatforwardfly_1" name="T-3029:one_opposite_pleat_one_pleat_forward_fly">
                    <img src="images/pants/oneoppositepleatonepleatforwardfly.png"/>
                    <p class="suggestionname">T-3029</br>One Opposite Pleat one..</p>
                </div>
                <div class="primarysuggestion" title="One Pleat Forward Sideseam one Opposite Pleat" id="pleatstyle_onepleateforwardsideseamoneoppositepleat_1" name="T-302A:one_pleat_forward_side_seam_one_opposite_pleat">
                    <img src="images/pants/onepleateforwardsideseamoneoppositepleat.png"/>
                    <p class="suggestionname">T-302A</br>One Pleat Forward Side..</p>
                </div>
                <div class="primarysuggestion" title="One Pleat Forward Fly One Opposite Pleat" id="pleatstyle_onepleateforwardflyoneoppositepleat_1"  name="T-302B:one_pleat_forward_fly_one_opposite_pleat">
                    <img src="images/pants/onepleateforwardflyoneoppositepleat.png"/>
                    <p class="suggestionname">T-302B</br>One Pleat Forward Fly..</p>
                </div>
                <div class="primarysuggestion"  id="pleatstyle_doublepleatsforwardfly_1" title="Double Pleats Forward Fly"  name="T-3024:double_pleat_forward_fly">
                    <img src="images/pants/doublepleatsforwardfly.png"/>
                    <p class="suggestionname">T-3024</br>Double Pleats Forward..</p>
                </div>
                <div class="primarysuggestion"  id="pleatstyle_oneoppositepleat_1"  name="T-3027:one_opposite_pleat">
                    <img src="images/pants/oneoppositepleat.png"/>
                    <p class="suggestionname">T-3027</br>One Opposite Pleat</p>
                </div>
                <div class="primarysuggestion" id="pleatstyle_oneboxpleat_1"  name="T-302P:one_box_pleat">
                    <img src="images/pants/oneboxpleat.png"/>
                    <p class="suggestionname">T-302P</br>One Box Pleat</p>
                </div>
            </div>
            <!--
            ----watch pocket style div
            --->
            <div class="col-md-12 stepdiv" id="watchpockets" style="display:none">
                <p class="stylecaption">Watch Pocket Style</p>
                <div class="primarysuggestion" id="watchpocket_normalleftwatchpocket_1" name="T-3170:normal_left_watch_pocket">
                    <img src="images/pants/normalleftwatchpocket.png"/>
                    <p class="suggestionname">T-3170</br>Normal Left Watch Pocket</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_leftbesomwatchpocket_1" name="T-3171:left_besom_watch_pocket">
                    <img src="images/pants/leftbesomwatchpocket.png"/>
                    <p class="suggestionname">T-3171</br>Left Besom Watch Pocket</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_watchleftpocketwithdiamondflap_1" title="Left Watch Pocket with Diamond Flap" name="T-3174:watch_left_pocket_with_diamond_flap">
                    <img src="images/pants/watchleftpocketwithdiamondflap.png"/>
                    <p class="suggestionname">T-3174</br>Left Watch Pocket with..</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_rightwatchpocketwithsquareflap_1" title="Right watch Pocket with Square Flap" name="T-3175:right_watch_pocket_with_square_flap">
                    <img src="images/pants/rightwatchpocketwithsquareflap.png"/>
                    <p class="suggestionname">T-3175</br>Right watch Pocket with..</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_leftdobublebesomwatchpocket_1" title="Left Double Besom watch Pocket" name="T-3178:left_dobuble_besom_watch_pocket">
                    <img src="images/pants/leftdobublebesomwatchpocket.png"/>
                    <p class="suggestionname">T-3178</br>Left Double Besom..</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_rightweltwatchpocket_1" title="Left Double Besom watch Pocket" name="T-3181:right_welt_watch_pocket">
                    <img src="images/pants/rightweltwatchpocket.png"/>
                    <p class="suggestionname">T-3181</br>Right Welt watch Pocket</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_normalrightwatchpocket_1" title="Normal Right watch Pocket" name="T-3180:normal_right_watch_pocket">
                    <img src="images/pants/normalrightwatchpocket.png"/>
                    <p class="suggestionname">T-3180</br>Normal Right watch Pocket</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_rightwatchpocketwithtrangleflap_1" title="Right watch Pocket with Trangle Flap" name="T-3182:right_watch_pocket_with_trangle_flap">
                    <img src="images/pants/rightwatchpocketwithtrangleflap.png"/>
                    <p class="suggestionname">T-3182</br>Right watch Pocket..</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_rightwatchpocketwithdiomondflap_1" title="Right watch Pocket with Diomond Flap" name="T-3184:right_watch_pocket_with_diomond_flap">
                    <img src="images/pants/rightwatchpocketwithdiomondflap.png"/>
                    <p class="suggestionname">T-3184</br>Right watch Pocket..</p>
                </div>
                <div class="primarysuggestion" id="watchpocket_doublebesomrightwatchpocket_1" title="Double Besom Right Watch Pocket" name="T-3188:double_besom_right_watch_pocket">
                    <img src="images/pants/doublebesomrightwatchpocket.png"/>
                    <p class="suggestionname">T-3188</br>Double Besom Right..</p>
                </div>
            </div>
            <!--
            ----back pocket div
            --->
            <div class="col-md-12 stepdiv" id="backpockets" style="display:none">
                <p class="stylecaption">Back Pockets And Satin options</p>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_nobackpocket" name="T-31A0:no_back_pocket">
                    <img src="images/pants/nobackpocket.png" class="backbtnsugg" />
                    <p class="suggestionname">T-31A0</br>No Back Pocket</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv active" id="backpocket_diamondflapbackpocket" name="T-3212:diamond_flap_back_pocket">
                    <img src="images/pants/diamond_flap_back_pocket.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3212</br>Dianond Flap Back..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_diamondflapwithbutton" name="T-3213:diamond_flap_with_button">
                    <img src="images/pants/diamond_flap_with_button.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3213</br>Diamond Flap With..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesombackpocket" name="T-3200:double_besom_back_pocket">
                    <img src="images/pants/double_besom_back_pocket.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3200</br>Double Besom Back..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandbuttonhole" name="doublebesomwithbuttonandbuttonhole:double_besom_with_button_and_button_hole">
                    <img src="images/pants/doublebesomwithbuttonhole.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-320G</br>Double Besom With..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandloop" name="doublebesomwithbuttonandloop:double_besom_with_button_and_loop">
                    <img src="images/pants/double_besom_with_button_loop.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-32C7</br>Double Besom With..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandpointtab" name="doublebesomwithbuttonandpointtab:double_besom_with_button_and_point_tab">
                    <img src="images/pants/double_besom_with_button_point_tab%20.png" class="backbtnsugg"/>
                    <br class="suggestionname">T-32C9</br>Double Besom With..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandsquaretab" name="T-32C1:double_besom_with_button_and_square_tab">
                    <img src="images/pants/double_besom_with_button_square_tab.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-32C1</br>Double Besom With..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_flapwithbutton" name="T-3211:flap_with_button">
                    <img src="images/pants/flap_with_button.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3211</br>Flap With Button</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_pointedflapbackpocket"  name="T-3214:pointed_flap_back_pocket">
                    <img src="images/pants/pointed_flap_back_pocket.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3214</br>Pointed Flap Back..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_pointedflapwithbutton"  name="T-3215:pointed_flap_with_button">
                    <img src="images/pants/pointed_flap_with_button.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3215</br>Pointed Flap With..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_standardflapbackpocket"  name="T-3210:standard_flap_back_pocket">
                    <img src="images/pants/standard_flap_back_pocket.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3210</br>Standard Flap Back..</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_weltbackpocket"  name="T-3220:welt_back_pocket">
                    <img src="images/pants/weltbackpocket.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3220</br>Welt Back Pocket</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_weltpocketwithbuttonandbuttonhole"  name="T-3221:welt_pocket_with_button_and_button_hole">
                    <img src="images/pants/welt_pocket_with_button_buttonhole.png" class="backbtnsugg"/>
                    <p class="suggestionname" title="Welt Pocket With Button and Button Hole">T-3221</br>Welt Pocket With...</p>
                </div>

                <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="backpocketsatin">
                    <p class="stylecaption">Satin Options</p>
                    <div class="primarysuggestion" name="DXN043A:white" id="backpocketsatin_dxn043a">
                        <img src="images/jackets/dxn043a.png" class=""/>
                        <p class="suggestionname">DXN043A White</p>
                    </div>
                    <div class="primarysuggestion" name="DXN032A:grey" id="backpocketsatin_dxn032a">
                        <img src="images/jackets/dxn032a.png" class=""/>
                        <p class="suggestionname">DXN032A Grey</p>
                    </div>
                    <div class="primarysuggestion" name="DXN031A:red" id="backpocketsatin_dxn031a">
                        <img src="images/jackets/dxn031a.png" class=""/>
                        <p class="suggestionname">DXN031A Red</p>
                    </div>
                    <div class="primarysuggestion" name="DXN030A:wine" id="backpocketsatin_dxn030a">
                        <img src="images/jackets/dxn030a.png" class=""/>
                        <p class="suggestionname">DXN030A Wine</p>
                    </div>
                    <div class="primarysuggestion" name="DXN029A:navy" id="backpocketsatin_dxn029a">
                        <img src="images/jackets/dxn029a.png" class=""/>
                        <p class="suggestionname">DXN029A Navy</p>
                    </div>
                    <div class="primarysuggestion" name="DXN008A:navy" id="backpocketsatin_dxn008a">
                        <img src="images/jackets/dxn008a.png" class=""/>
                        <p class="suggestionname">DXN008A Black</p>
                    </div>
                </div>


                <div class="col-md-12" id="backpockets" style="margin-top: 20px;">
                    <p class="stylecaption">Satin Positions</p>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_sideseamwithsatinstrip20cmwidth" name="T-3313:side_seam_with_satin_strip_2.0cm_width">
                        <img src="images/pants/T-3313sideseamwithsatinstrip20cmwidth.png" class="backbtnsugg" />
                        <p class="suggestionname">T-3313</br>side seam with..</p>
                    </div>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_sideseamwithsatin12cmwidth" name="T-3314:side_seam_with_satin_1.2_cm_width">
                        <img src="images/pants/T-3314sideseamwithsatin12cmwidth.png" class="backbtnsugg" />
                        <p class="suggestionname">T-3314</br>side seam with..</p>
                    </div>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_sideseamwithsatinstripe12cmwidth" name="T-3315:side_seam_with_satin_stripe_1.2_cm_width">
                        <img src="images/pants/T-3315sideseamwithsatinstripe12cmwidth.png" class="backbtnsugg" />
                        <p class="suggestionname">T-3315</br>side seam with ..</p>
                    </div>
                </div>


                <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="backpocketsatin">
                    <p class="stylecaption">Satin Options</p>
                    <div class="primarysuggestion" name="DXN043A:white" id="backpocketsatin_dxn043a_1">
                        <img src="images/jackets/dxn043a.png" class=""/>
                        <p class="suggestionname">DXN043A White</p>
                    </div>
                    <div class="primarysuggestion" name="DXN032A:grey" id="backpocketsatin_dxn032a_1">
                        <img src="images/jackets/dxn032a.png" class=""/>
                        <p class="suggestionname">DXN032A Grey</p>
                    </div>
                    <div class="primarysuggestion" name="DXN031A:red" id="backpocketsatin_dxn031a_1">
                        <img src="images/jackets/dxn031a.png" class=""/>
                        <p class="suggestionname">DXN031A Red</p>
                    </div>
                    <div class="primarysuggestion" name="DXN030A:wine" id="backpocketsatin_dxn030a_1">
                        <img src="images/jackets/dxn030a.png" class=""/>
                        <p class="suggestionname">DXN030A Wine</p>
                    </div>
                    <div class="primarysuggestion" name="DXN029A:navy" id="backpocketsatin_dxn029a_1">
                        <img src="images/jackets/dxn029a.png" class=""/>
                        <p class="suggestionname">DXN029A Navy</p>
                    </div>
                    <div class="primarysuggestion" name="DXN008A:navy" id="backpocketsatin_dxn008a_1">
                        <img src="images/jackets/dxn008a.png" class=""/>
                        <p class="suggestionname">DXN008A Black</p>
                    </div>
                    <!--
                                ----pant color div
                                --->
                    <div class="col-md-12 stepdiv" id="pantcolors_fabric1" >
                        <p class="stylecaption">Pant Fabric</p>
                        <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="mainfabricdiv1">
                            <?php
                            $query = "select * from wp_colors where type='suit' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $a++;
                                        $colorName = $rows['colorName'];
                                        $displayImage = $rows['displayImage'];
                                        ?>
                                        <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>'
                                             name='<?php echo $colorName ?>' id='pantcolor1_<?php echo $colorName ?>'
                                             onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>')>
                                            <img title="<?php echo $displayImage ?> " src='images/pants/<?php echo $displayImage ?>' />
                                            <p><?php echo $colorName ?></p>
                                        </div>
                                        <?php
                                    }

                                }else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6" style="margin-top:20px;padding:0;">
                                    <label>Fabric Name</label>
                                    <input type="text" class="form-control" value="" id="mainfabricfield1" onkeyup="searchFabricData(this)"   placeholder="Enter Fabric Name" />
                                    <ul class="list-unstyled data_ data-remove" id="list_data1" style="background: #FFFFFF">
                                    </ul>
                                </div>
                                <div class="form-group col-md-6" style="margin-top:20px">
                                    <input type="button" class="btn btn-danger pull-right colorFabricBtn"  onclick="mainfabric('mainfabricfield1','mainfabricdiv1')"
                                           style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric1" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <label class="surplus_price_label" style="display: none">
                                    Surplus Price :
                                    <span class="surplus_price"></span>
                                </label>
                            </div>
                        </div>

                    </div>

                </div>

                <div class="col-md-12" id="backpockets" style="margin-top: 20px;">
                    <p class="stylecaption">Hip Pocket Besom</p>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_hippocketbesomwithsatin" name="T-325A:hip_pocket_besom_with_satin">
                        <p class="suggestionname">T-325A</br>Hip pocket besom with satin</p>
                    </div>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_hippocketbesomappointedfabric" name="T-325B:hip_pocket_besom_appointed_fabric">
                        <p class="suggestionname">T-325B</br>Hip pocket besom appointed fabric</p>
                    </div>
                </div>

                <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="backpocketsatin">
                    <p class="stylecaption">Satin Options</p>
                    <div class="primarysuggestion" name="DXN043A:white" id="backpocketsatin_dxn043a_2">
                        <img src="images/jackets/dxn043a.png" class=""/>
                        <p class="suggestionname">DXN043A White</p>
                    </div>
                    <div class="primarysuggestion" name="DXN032A:grey" id="backpocketsatin_dxn032a_2">
                        <img src="images/jackets/dxn032a.png" class=""/>
                        <p class="suggestionname">DXN032A Grey</p>
                    </div>
                    <div class="primarysuggestion" name="DXN031A:red" id="backpocketsatin_dxn031a_2">
                        <img src="images/jackets/dxn031a.png" class=""/>
                        <p class="suggestionname">DXN031A Red</p>
                    </div>
                    <div class="primarysuggestion" name="DXN030A:wine" id="backpocketsatin_dxn030a_2">
                        <img src="images/jackets/dxn030a.png" class=""/>
                        <p class="suggestionname">DXN030A Wine</p>
                    </div>
                    <div class="primarysuggestion" name="DXN029A:navy" id="backpocketsatin_dxn029a_2">
                        <img src="images/jackets/dxn029a.png" class=""/>
                        <p class="suggestionname">DXN029A Navy</p>
                    </div>
                    <div class="primarysuggestion" name="DXN008A:navy" id="backpocketsatin_dxn008a_2">
                        <img src="images/jackets/dxn008a.png" class=""/>
                        <p class="suggestionname">DXN008A Black</p>
                    </div>

                    <!--
                                ----pant color div
                                --->
                    <div class="col-md-12 stepdiv" id="pantcolors_fabric2" >
                        <p class="stylecaption">Pant Fabric</p>
                        <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="mainfabricdiv2">
                            <?php
                            $query = "select * from wp_colors where type='suit' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $a++;
                                        $colorName = $rows['colorName'];
                                        $displayImage = $rows['displayImage'];
                                        ?>
                                        <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>'
                                             name='<?php echo $colorName ?>' id='pantcolor2_<?php echo $colorName ?>'
                                             onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>')>
                                            <img title="<?php echo $displayImage ?> " src='images/pants/<?php echo $displayImage ?>' />
                                            <p><?php echo $colorName ?></p>
                                        </div>
                                        <?php
                                    }

                                }else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6" style="margin-top:20px;padding:0;">
                                    <label>Fabric Name</label>
                                    <input type="text" class="form-control" value="" id="mainfabricfield2" onkeyup="searchFabricData(this)"   placeholder="Enter Fabric Name" />
                                    <ul class="list-unstyled data_ data-remove" id="list_data2" style="background: #FFFFFF">
                                    </ul>
                                </div>
                                <div class="form-group col-md-6" style="margin-top:20px">
                                    <input type="button" class="btn btn-danger pull-right colorFabricBtn"  onclick="mainfabric('mainfabricfield2','mainfabricdiv2')"
                                           style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric2" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <label class="surplus_price_label" style="display: none">
                                    Surplus Price :
                                    <span class="surplus_price"></span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="col-md-12" id="backpockets" style="margin-top: 20px;">
                    <p class="stylecaption">Waistband Fabric</p>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_satinwaistband" name="T-33A1:satin_waistband">
                        <p class="suggestionname">T-33A1</br>Satin Waistband</p>
                    </div>
                    <div style="height: 173;width: 267;padding-top: 55px;" class="primarysuggestion backbtnsuggparentdiv" id="backpocket_waistband" name="T-33A2:waistband">
                        <p class="suggestionname">T-33A2</br>Waistband</p>
                    </div>
                </div>

                <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="backpocketsatin">
                    <p class="stylecaption">Satin Options</p>
                    <div class="primarysuggestion" name="DXN043A:white" id="backpocketsatin_dxn043a_3">
                        <img src="images/jackets/dxn043a.png" class=""/>
                        <p class="suggestionname">DXN043A White</p>
                    </div>
                    <div class="primarysuggestion" name="DXN032A:grey" id="backpocketsatin_dxn032a_3">
                        <img src="images/jackets/dxn032a.png" class=""/>
                        <p class="suggestionname">DXN032A Grey</p>
                    </div>
                    <div class="primarysuggestion" name="DXN031A:red" id="backpocketsatin_dxn031a_3">
                        <img src="images/jackets/dxn031a.png" class=""/>
                        <p class="suggestionname">DXN031A Red</p>
                    </div>
                    <div class="primarysuggestion" name="DXN030A:wine" id="backpocketsatin_dxn030a_3">
                        <img src="images/jackets/dxn030a.png" class=""/>
                        <p class="suggestionname">DXN030A Wine</p>
                    </div>
                    <div class="primarysuggestion" name="DXN029A:navy" id="backpocketsatin_dxn029a_3">
                        <img src="images/jackets/dxn029a.png" class=""/>
                        <p class="suggestionname">DXN029A Navy</p>
                    </div>
                    <div class="primarysuggestion" name="DXN008A:navy" id="backpocketsatin_dxn008a_3">
                        <img src="images/jackets/dxn008a.png" class=""/>
                        <p class="suggestionname">DXN008A Black</p>
                    </div>

                    <!--
                                ----pant color div
                                --->
                    <div class="col-md-12 stepdiv" id="pantcolors_fabric3" >
                        <p class="stylecaption">Pant Fabric</p>
                        <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="mainfabricdiv3">
                            <?php
                            $query = "select * from wp_colors where type='suit' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $a++;
                                        $colorName = $rows['colorName'];
                                        $displayImage = $rows['displayImage'];
                                        ?>
                                        <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>'
                                             name='<?php echo $colorName ?>' id='pantcolor3_<?php echo $colorName ?>'
                                             onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>')>
                                            <img title="<?php echo $displayImage ?> " src='images/pants/<?php echo $displayImage ?>' />
                                            <p><?php echo $colorName ?></p>
                                        </div>
                                        <?php
                                    }

                                }else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group col-md-6" style="margin-top:20px;padding:0;">
                                    <label>Fabric Name</label>
                                    <input type="text" class="form-control" value="" id="mainfabricfield3" onkeyup="searchFabricData(this)"   placeholder="Enter Fabric Name" />
                                    <ul class="list-unstyled data_ data-remove" id="list_data3" style="background: #FFFFFF">
                                    </ul>
                                </div>
                                <div class="form-group col-md-6" style="margin-top:20px">
                                    <input type="button" class="btn btn-danger pull-right colorFabricBtn"  onclick="mainfabric('mainfabricfield3','mainfabricdiv3')"
                                           style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric3" disabled/>
                                </div>
                            </div>
                            <div class="row">
                                <label class="surplus_price_label" style="display: none">
                                    Surplus Price :
                                    <span class="surplus_price"></span>
                                </label>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
            <!--
            ----belt loop div
            --->
            <div class="col-md-12 stepdiv" id="beltloops" style="display:none">
                <p class="stylecaption">Belt Loop Style</p>
                <div class="primarysuggestion backbtnsuggparentdiv active" id="beltloop_nobeltloop" name="T-3329:no_belt_loop">
                    <img src="images/pants/nobeltloop.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3329</br>No Belt Loop</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_standardbeltloop" name="T-3330:standard_belt_loop">
                    <img src="images/pants/standardbeltloop.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3330</br>Standard Belt Loop</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv " id="beltloop_doublebeltloop" name="T-3331:double_belt_loop">
                    <img src="images/pants/doublebeltloop.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3331</br>double Belt Loop</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_doubleslantbeltloop" name="T-3339:double_slant_belt_loop">
                    <img src="images/pants/doubleslantbeltloop.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3339</br>Double Slant Belt Loop</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_normal" name="normal:normal">
                    <img src="images/pants/normal.png" class="backbtnsugg"/>
                    <p class="suggestionname">Normal</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_20" name="T-3332:2.0">
                    <img src="images/pants/20.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-3332</br>2.0</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_38" name="T-33D1:3.8">
                    <img src="images/pants/38.png" class="backbtnsugg"/>
                    <p class="suggestionname">T-33D1</br>3.8</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_50" name="T-3338:5.0">
                    <img src="images/pants/50.png" class="backbtnsugg"/>
                    <br class="suggestionname">T-3338</br>5.0</p>
                </div>
                <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_beltloopwidth32cm" name="T-333F:belt_loop_width_3.2cm">
                    <img src="images/pants/beltloopwidth32cm.png" class="backbtnsugg"/>
                    <br class="suggestionname">T-333F</br>Belt Loop 3.2cm</p>
                </div>
            </div>
            <!--
            ----pant bottom div
            --->
            <div class="col-md-12 stepdiv" id="pantbottoms" style="display:none">
                <p class="stylecaption">Pant Bottom Style</p>
                <div class="primarysuggestion active" id="pantbottom_32cmcuff" name="T-3602:3.2_cm_cuff">
                    <img src="images/pants/32cmcuff.png"/>
                    <br class="suggestionname">T-3602 &nbsp;3.2cm Cuff</p>
                </div>
                <div class="primarysuggestion" id="pantbottom_38cmcuff" name="T-3603:3.8_cm_cuff">
                    <img src="images/pants/38cmcuff.png"/>
                    <br class="suggestionname">T-3603 &nbsp;3.8cm Cuff</p>
                </div>
                <div class="primarysuggestion" id="pantbottom_44cmcuff" name="T-3604:4.4_cm_cuff">
                    <img src="images/pants/44cmcuff.png"/>
                    <br class="suggestionname">T-3604 &nbsp;4.4cm Cuff</p>
                </div>
                <div class="primarysuggestion" id="pantbottom_51cmcuff" name="T-3605:5.1_cm_cuff">
                    <img src="images/pants/51cmcuff.png"/>
                    <br class="suggestionname">T-3605 &nbsp;5.1cm Cuff</p>
                </div>
                <div class="primarysuggestion" id="pantbottom_nocuff" name="T-3600:no_cuff">
                    <img src="images/pants/no_cuff_pant_bottom.png"/>
                    <br class="suggestionname">T-3600 No Cuff</p>
                </div>
            </div>

            <!--
            ----pant color div
            --->
            <div class="col-md-12 stepdiv" id="pantcolors" style="display:none">
                <p class="stylecaption">Pant Color Style</p>
                <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="mainfabricdiv">
                    <?php
                    $query = "select * from wp_colors where type='suit' and status = '1'";
                    $result = mysql_query($query);
                    if($result){
                        $num = mysql_num_rows($result);
                        if($num>0){
                            $status = "done";
                            $a=0;
                            while($rows = mysql_fetch_array($result)){
                                $a++;
                                $colorName = $rows['colorName'];
                                $displayImage = $rows['displayImage'];
                                ?>
                                <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>'
                                     name='<?php echo $colorName ?>' id='pantcolor4_<?php echo $colorName ?>'
                                     onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>')>
                                    <img title="<?php echo $displayImage ?> " src='images/pants/<?php echo $displayImage ?>' />
                                    <p><?php echo $colorName ?></p>
                                </div>
                                <?php
                            }

                        }else{
                            echo "No Color Found";
                        }
                    }else{
                        echo mysql_error();
                    }
                    ?>
                </div>

                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group col-md-6" style="margin-top:20px;padding:0;">
                            <label>Fabric Name</label>
                            <input type="text" class="form-control" value="" id="mainfabricfield" onkeyup="searchFabricData(this)"   placeholder="Enter Fabric Name" />
                            <ul class="list-unstyled data_ data-remove" id="list_data" style="background: #FFFFFF">
                            </ul>
                        </div>
                        <div class="form-group col-md-6" style="margin-top:20px">
                            <input type="button" class="btn btn-danger pull-right colorFabricBtn"  onclick="mainfabric('mainfabricfield','mainfabricdiv')"
                                   style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric" disabled/>
                        </div>
                    </div>
                    <div class="row">
                        <label class="surplus_price_label" style="display: none">
                            Surplus Price :
                            <span class="surplus_price"></span>
                        </label>
                    </div>
                </div>

            </div>
        </div>
        <?php
        $query = "select * from categories where cat_type='Pants' order by cat_id ASC";
        $result = mysql_query($query);
        if($result){
            $num = mysql_num_rows($result);
            if($num>0){
                while($rows = mysql_fetch_array($result)){
                    $product_amount = $rows['cat_price'];
                    ?>
                    <input type="hidden" id="product_amount" value="<?php echo $product_amount;?>">
                    <?php
                }
            }else{
                echo "";
            }
        }else{
            echo mysql_error();
        }
        ?>
        <div class="col-md-12" style="text-align:center;margin-top: 100px">
            <label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
            <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
        </div>
        <div id ="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">.col-md-4</div>
                            <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>
            function setSurplusPrice(colorName){
                var colorName = decodeURI(colorName);
                $(".surplus_price_label").show();
                var url = "admin/webserver/getSurplusPrice.php";
                $.post(url, {"type":"Custom Pant","colorName":colorName},function(data){
                    var data = JSON.parse(data);
                    $(".surplus_price").html("$"+data.extra_price);
                });
            }

            $(".nav a").removeClass("active");
            var currPage = "";
            var category = "formal";
            var frontpocket = "20cmslantpocket";
            var cfrontpocket = "T-313C:no_pocket";
            var pleatstyle = "plainfront";
            var cpleatstyle = "T-3020:plain_front";
            var watchpocket = "normalleftwatchpocket_1";
            var cwatchpocket = "T-3170:normal_left_watch_pocket";
            var backpocket = "diamondflapbackpocket";
            var backpocketsatin = "DXN043A:White";
            var cbackpocketsatin = "DXN043A:White";
            var cbackpocket = "T-3212:diamond_flap_back_pocket";
            var belt = "belt";
            var pleat = "pleat";
            var beltloop = "standardbeltloop";
            var cbeltloop = "T-3330:standard_belt_loop";
            var pantbottom =    "32cmcuff";
            var cpantbottom = "T-3602:3.2_cm_cuff";
            var pantcolor = "dbk053a";
            var folder = "front";

            function getPrevVal(prevIndex) {
                var result = "";
                switch(prevIndex) {
                    case "category":
                        result = category;
                        break;
                    case "frontpocket":
                        result = frontpocket;
                        break;
                    case "pleatstyle":
                        result = pleatstyle;
                        break;
                    case "watchpocket":
                        result = watchpocket;
                        break;
                    case "backpocket":
                        result = backpocket;
                        break;
                    case "backpocketsatin":
                        result = backpocketsatin;
                        break;
                    case "beltloop":
                        result = beltloop;
                        break;
                    case "pantbottom":
                        result = pantbottom;
                        break;
                    case "pantcolor":
                        result = pantcolor;
                        break;
                }
                return result;
            }
            function loadSessionData() {
                $(".loader").fadeIn("slow");
                var prevIndex = $(".stepsactive").attr("id");
                currPage = prevIndex + "s";
                var name = getPrevVal(prevIndex);
                var item = name;
                var img = "";
                item = prevIndex + "_" + item;
                $(".primarysuggestion").removeClass("active");
                $("#" + item).addClass("active");
                if (currPage == "categorys"){
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual"){
                        if (frontpocket == "20cmslantpocket" || frontpocket == "25slantpocket" ||
                            frontpocket == "32cmslantpocket" || frontpocket == "51cmslantpocket" ||
                            frontpocket == "doublebesompocket" || frontpocket == "sideseampocket" ||
                            frontpocket == "weltpocket" || frontpocket == "nopocket") {
                            frontpocket = "arcjeanspocket";
                        }
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else{
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "frontpockets") {
                    folder = "side";
                    if(category == "casual"){
                        $("#forcasual").show();
                    }
                    else if(category == "formal"){
                        $("#forcasual").hide();
                        if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                        frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket"){
                            frontpocket = "20cmslantpocket";
                            $("#frontpocket_20cmslantpocket").addClass("active");
                        }
                    }
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "pleatstyles") {
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "watchpockets") {
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "backpockets") {
                    folder = "back";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#frontpocketimg").attr("src","images/pants/sugg/bawli.png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "beltloops") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual")
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else
                    {
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "pantbottoms") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual")
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else
                    {
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "pantcolors") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual")
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else
                    {
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");

                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                not_found = 0;
            }
            $(".primarysuggestion").click(function () {
                var Img = "";
                $(".loader").fadeIn("slow");
                $(".primarysuggestion").removeClass("active");
                var value = this.id;
                var name = $("#"+value).attr("name");
                var type = value.split("_");
                var selection = type[1];
                $("#" + value).addClass("active");
                var currpage = type[0] + "s";
                
                if(type[0].substring(0, type[0].length-1) == "pantcolor"){
                    type[0] = "pantcolor";
                    currpage = "pantcolors";
                }
                switch(type[0]) {
                    case "category":
                        category = name;
                        break;
                    case "frontpocket":
                        frontpocket = selection;
                        cfrontpocket = name;
                        break;
                    case "pleatstyle":
                        pleatstyle = selection;
                        cpleatstyle = name;
                        break;
                    case "watchpocket":
                        watchpocket = selection;
                        cwatchpocket = name;
                        break;
                    case "backpocket":
                        backpocket = selection;
                        cbackpocket = name;
                        break;
                    case "backpocketsatin":
                        backpocketsatin = selection;
                        cbackpocketsatin = name;
                        break;
                    case "beltloop":
                        beltloop = selection;
                        cbeltloop = name;
                        break;
                    case "pantbottom":
                        pantbottom = selection;
                        cpantbottom = name;
                        break;
                    case "pantcolor":
                        pantcolor = name;
                        break;
                }
                if (currpage == "categorys") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual") {
                        if (frontpocket == "20cmslantpocket" || frontpocket == "25slantpocket" || frontpocket == "32cmslantpocket"
                            || frontpocket == "51cmslantpocket" || frontpocket == "doublebesompocket" || frontpocket == "sideseampocket"
                            || frontpocket == "weltpocket" || frontpocket == "nopocket") {
                            frontpocket = "arcjeanspocket";
                        }
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                    }else{
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "frontpockets") {
                    folder = "side";
                    if(category == "casual")
                    {
                        $("#forcasual").show();
                    }
                    else if(category == "formal")
                    {
                        $("#forcasual").hide();
                        if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                            frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                        {
                            frontpocket = "20cmslantpocket";
                            $("#frontpocket_20cmslantpocket").addClass("active");
                        }
                    }
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "pleatstyles") {
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "watchpockets") {
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "backpockets") {
                    folder = "back";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#frontpocketimg").attr("src","images/pants/sugg/bawli.png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "beltloops") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual")
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else
                    {
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "pantbottoms") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual")
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else
                    {
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else if (currpage == "pantcolors") {
                    folder = "front";
                    $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                    $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                    if(category == "casual")
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else
                    {
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                    }
                    $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                    $(".loader").fadeOut("slow");
                }
                else{
                    $(".loader").fadeOut("slow");
                }

                /*
                 * data store all values
                 * */

                var url = 'admin/webserver/selectionData.php';

                $.post(url,{currPage:currPage,pantcategory:category,frontpocket:frontpocket,cfrontpocket:cfrontpocket,pleatstyle:pleatstyle,
                    cpleatstyle:cpleatstyle,watchpocket:watchpocket,cwatchpocket:cwatchpocket,backpocket:backpocket,backpocketsatin:backpocketsatin,
                    cbackpocketsatin:cbackpocketsatin,cbackpocket:cbackpocket,belt:belt,pleat:pleat,beltloop:beltloop,cbeltloop:cbeltloop,
                    pantbottom:pantbottom,cpantbottom:cpantbottom,pantcolor:pantcolor,folder:folder

                },function(data){
                    console.log('data --- '+data);
                });
            });
            function navigation(switcher){
                $("#" + currPage).fadeOut("slow");
                if (switcher == "next") {
                    if (currPage == "categorys") {
                        $(".steps").removeClass("stepsactive");
                        $("#frontpockets").fadeIn("slow");
                        $("#frontpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "frontpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#pleatstyles").fadeIn("slow");
                        $("#pleatstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "pleatstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#watchpockets").fadeIn("slow");
                        $("#watchpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "watchpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#backpockets").fadeIn("slow");
                        $("#backpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "backpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#beltloops").fadeIn("slow");
                        $("#beltloop").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "beltloops") {
                        $(".steps").removeClass("stepsactive");
                        $("#pantbottoms").fadeIn("slow");
                        $("#pantbottom").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "pantbottoms") {
                        $(".steps").removeClass("stepsactive");
                        $("#pantcolors").fadeIn("slow");
                        $("#pantcolor").addClass("stepsactive");
                        $("#next").html("Add to Cart");
                        loadSessionData();
                    }
                    else if (currPage == "pantcolors") {

                        var fabricCode = pantcolor;
                        if(fabricCode.indexOf(":") >0 ){
                            fabricCode = fabricCode.split(":")[0];
                        }
                        var url = "api/registerUser.php";
                        $.post(url,{"type":"checkFabricStatus","fabricCode":fabricCode},function(fabricdata) {
                            var status = fabricdata.Status;
                            var message = fabricdata.Message;
                            if (status == "Success") {
                                var user_id = $("#user_id").val();
                                var product_amount = $("#product_amount").val();
                                var randomnum = Math.floor((Math.random() * 600000) + 1);
                                var product_name = "CustomPant" + randomnum;
                                var urlData = window.location.href;
                                var url = "";
                                if(urlData.indexOf("freebies=Custom_Pant")>0) {
                                    var cart_id = getQueryStringValue("pid");
                                    var rowType = getQueryStringValue("rowType");
                                    url = "admin/webserver/addto_cart.php?product_type=freebies_custom_pant&rowType="+rowType+
                                        "&cart_id="+cart_id+"&product_name="+product_name+
                                        "&category="+category+"&frontpocket="+cfrontpocket+"&pleatstyle="+cpleatstyle+
                                        "&watchpocket="+cwatchpocket+"&backpocket="+cbackpocket+"&belt="+belt+"&pleat="+pleat+
                                        "&beltloop="+cbeltloop+"&pantbottom="+cpantbottom+"&pantcolor="+pantcolor+"&price="+product_amount+
                                        "&user_id="+user_id+"&quantity="+1+"&cart_product_total_amount="+product_amount+
                                        "&imgfrontpocket="+frontpocket+"&imgpleatstyle="+pleatstyle+"&imgwatchpocket="+watchpocket
                                        +"&imgbeltloop="+beltloop+"&imgpantbottom="+pantbottom+"&imgbackpocket="+backpocket+
                                        "&backpocketsatin="+cbackpocketsatin;
                                }
                                else{
                                    url = "admin/webserver/addto_cart.php?product_type=Custom Pant&product_name="+product_name+
                                        "&category="+category+"&frontpocket="+cfrontpocket+"&pleatstyle="+cpleatstyle+
                                        "&watchpocket="+cwatchpocket+"&backpocket="+cbackpocket+"&belt="+belt+"&pleat="+pleat+
                                        "&beltloop="+cbeltloop+"&pantbottom="+cpantbottom+"&pantcolor="+pantcolor+"&price="+product_amount+
                                        "&user_id="+user_id+"&quantity="+1+"&cart_product_total_amount="+product_amount+
                                        "&imgfrontpocket="+frontpocket+"&imgpleatstyle="+pleatstyle+"&imgwatchpocket="+watchpocket
                                        +"&imgbeltloop="+beltloop+"&imgpantbottom="+pantbottom+"&imgbackpocket="+backpocket+"&backpocketsatin="+cbackpocketsatin;
                                }
                                $.get(url, function (data) {
                                    var json = $.parseJSON(data);
                                    var status = json.status;
                                    if (status == "done") {
                                        window.location = "cart.php";
                                    }
                                });
                            }
                            else{
                                showMessages(message,"red");
                                $("#pantcolors").fadeIn("slow");
                                $(".colorsuggestion ").removeClass("active");
                                $("#pantcolor4_dbk053a").addClass("active");
                                $("#pantcolor4_dbk053a").trigger("click");
                                pantcolor ='dbk053a';
                            }
                        });
                    }
                }
                else if (switcher == "previous") {
                    if (currPage == "categorys") {
                        window.location = "customize.php";
                    }
                    else if (currPage == "frontpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#categorys").fadeIn("slow");
                        $("#category").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "pleatstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#frontpockets").fadeIn("slow");
                        $("#frontpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "watchpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#pleatstyles").fadeIn("slow");
                        $("#pleatstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "backpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#watchpockets").fadeIn("slow");
                        $("#watchpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "beltloops") {
                        $(".steps").removeClass("stepsactive");
                        $("#backpockets").fadeIn("slow");
                        $("#backpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "pantbottoms") {
                        $(".steps").removeClass("stepsactive");
                        $("#beltloops").fadeIn("slow");
                        $("#beltloop").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "pantcolors") {
                        $(".steps").removeClass("stepsactive");
                        $("#pantbottoms").fadeIn("slow");
                        $("#pantbottom").addClass("stepsactive");
                        $("#next").html("Next");
                        loadSessionData();
                    }
                }
                storeData();
            }
            function showMessages(message, color) {
                $(".modal-header").css({"display": "none"});
                $(".modal-body").css({"display": "block"});
                $(".modal-body").css({"padding-top": "0px"});
                $(".modal-body").html("<div class='row' style='background:#000;'><h4 style='color:#fff;text-align: center" +
                    ";padding:10px;font-size: 18px;' >" +
                    "Fabric out of stock !!!</h4></div><div class='row'><h4 style='text-align: center;font-size: 18px;margin: 14px 0;'>"
                    +message+"</h4></div>" +
                    "<div class='row' style='text-align: center'><input type='button' data-dismiss='modal' " +
                    "style='padding:6px;margin:6px 0;text-align: center;font-size: 16px;width: 200px;text-transform: uppercase;" +
                    "background: #D9534F;border: none;color:#fff;" +
                    "' value='ok'" +
                    " /></div> ");
                $(".modal-footer").css({"display": "none"});
                $("#modal").modal("show");
            }

            function storeData() {

                var value = $(".stepsactive").attr("id");
                var name = $("#"+value).attr("name");
                var type = value.split("_");
                var selection = type[1];
                switch(type[0]) {
                    case "category":
                        category = name;
                        break;
                    case "frontpocket":
                        frontpocket = selection;
                        cfrontpocket = name;
                        break;
                    case "pleatstyle":
                        pleatstyle = selection;
                        cpleatstyle = name;
                        break;
                    case "watchpocket":
                        watchpocket = selection;
                        cwatchpocket = name;
                        break;
                    case "backpocket":
                        backpocket = selection;
                        cbackpocket = name;
                        break;
                    case "backpocketsatin":
                        backpocketsatin = selection;
                        cbackpocketsatin = name;
                        break;
                    case "beltloop":
                        beltloop = selection;
                        cbeltloop = name;
                        break;
                    case "pantbottom":
                        pantbottom = selection;
                        cpantbottom = name;
                        break;
                    case "pantcolor":
                        pantcolor = name;
                        break;
                }
                savetoSession('tab',type[0]);
            }
            function getQueryStringValue (key) {
                return decodeURIComponent(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));
            }
            function rotateImage(button) {
                $(".loader").fadeIn("slow");
                var source=[];
                source.push($("#pantimg").attr("src"));
                source.push($("#beltimg").attr("src"));
                source.push($("#beltloopimg").attr("src"));
                source.push($("#pleatimg").attr("src"));
                source.push($("#frontpocketimg").attr("src"));
                source.push($("#backpocketimg").attr("src"));
                source.push($("#pantbottomimg").attr("src"));
                for(var i = 0;i<source.length;i++) {
                    if (source[i].indexOf("front/") >=0){
                        if (button == "left") {
                            $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+category+".png");
                            $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+belt+".png");
                            $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+beltloop+".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                            $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+backpocket+".png");
                            $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+pantbottom+".png");
                        }
                        else if (button == "right") {
                            $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+category+".png");
                            $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+belt+".png");
                            $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+beltloop+".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                            if(category == "formal")
                            {
                                if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                                    frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                                {
                                    frontpocket = "20cmslantpocket";
                                }
                            }
                            $("#frontpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+frontpocket+".png");
                            $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+backpocket+".png");
                            $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                    }
                    else if (source[i].indexOf("side/") >= 0) {
                        if (button == "left") {
                            $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+category+".png");
                            $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+belt+".png");
                            $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+beltloop+".png");
                            if(category == "casual")
                            {
                                $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + frontpocket + ".png");
                                $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                            }
                            else
                            {
                                $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + pleat + ".png");
                                $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                            }
                            $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                            $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+pantbottom+".png");
                        }
                        else if (button == "right") {
                            $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+category+".png");
                            $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+belt+".png");
                            $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+beltloop+".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                            $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+backpocket+".png");
                            $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+pantbottom+".png");
                        }
                    }
                    else if (source[i].indexOf("back/") >= 0) {
                        if (button == "left") {
                            $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+category+".png");
                            $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+belt+".png");
                            $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+beltloop+".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                            if(category == "formal")
                            {
                                if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                                    frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                                {
                                    frontpocket = "20cmslantpocket";
                                }
                            }
                            $("#frontpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+frontpocket+".png");
                            $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+backpocket+".png");
                            $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else if (button == "right") {
                            $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+category+".png");
                            $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+belt+".png");
                            $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+beltloop+".png");
                            if(category == "casual")
                            {
                                $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + frontpocket + ".png");
                                $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                            }
                            else
                            {
                                $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + pleat + ".png");
                                $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                            }
                            $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                            $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+pantbottom+".png");
                        }
                    }
                }
                $(".loader").fadeOut("slow");
            }
            $(".steps").click(function(){
                savetoSession('tab',this.id);
                $("#"+currPage).hide(1000);
                var newcurrpage = this.id+"s";
                $(".steps").removeClass("stepsactive");
                $("#"+this.id).addClass("stepsactive");
                $("#"+newcurrpage).show(1000);
                currPage = newcurrpage;
                loadSessionData();
                if(currPage == "pantcolors"){
                    $("#next").html("Add to Cart");
                }else{
                    $("#next").html("Next");
                }
            });
            var count = 0;
            function mainfabric(id,boxId){
                $(".colorFabricBtn").attr("disabled",true);
                var mainfabricfield = $("#"+id).val();
                if(mainfabricfield != ""){
                    setSurplusPrice(encodeURI(mainfabricfield));
                    var newdiv = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric('main',this);setSurplusPrice('"+encodeURI(mainfabricfield)+"') " +
                        "name='"+mainfabricfield+"' id='pantcolor_"+mainfabricfield+"'><p class='suggestionname' " +
                        "style='line-height:80px;width:70px'>"+mainfabricfield+"</p></div>";
                    $("#mainError").remove();
                    $("#"+boxId).append(newdiv);
                    $("#"+id).val("");
                }else{
                    if(count == 0) {
                        count++;
                        $("#"+boxId).append("<div style='clear:both'></div><p style='text-align:center;color:red;" +
                            "margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
                    }
                }
            }
            function selectFabric(type,obj){
                $(".primarysuggestion").removeClass("active");
                $("#"+obj.id).addClass("active");
                if(type == "main") {
                    pantcolor = (obj.id).split("_")[1];
                }
            }
            function fabricSelected(id,value) {
                $('#'+id).val(value);
                if(id === 'mainfabricfield') {
                    $("#findFabric").attr("disabled",false);
                    $('#list_data').addClass('data-remove');
                }
                else if(id === 'mainfabricfield1') {
                    $("#findFabric1").attr("disabled",false);
                    $('#list_data1').addClass('data-remove');
                }
                else if(id === 'mainfabricfield2') {
                    $("#findFabric2").attr("disabled",false);
                    $('#list_data2').addClass('data-remove');
                }
                else if(id === 'mainfabricfield3') {
                    $("#findFabric3").attr("disabled",false);
                    $('#list_data3').addClass('data-remove');
                }


            }

            function searchFabricData(ref) {
                var value = $('#'+ref.id).val();
                $("#findFabric").attr("disabled",true);
                var li_='';
                var url = "admins/api/fabricProcess.php";

                $.post(url,{'dataType':'searchFabricData','cameo_code':value},function(response){
                    var results = [];
//                   var response = $.parseJSON(data);
                    var status = response.Status;
                    var message = response.Message;
                    if(status === 'Success' ) {
                        results = [];
                        var dataa = response.data;
                        li_ = '';
                        for(var i=0;i<dataa.length;i++) {
//                         results.push(dataa[i].fabric_cameo);
                            li_ = li_+'<li onclick=fabricSelected("'+ref.id+'","'+dataa[i].fabric_code+'")>'+dataa[i].fabric_code+'</li>';
                        }

                        if(ref.id === 'mainfabricfield') {
                            $('#list_data').removeClass('data-remove');
                            $('#list_data').html(li_);
                        }
                        else if(ref.id === 'mainfabricfield1') {
                            $('#list_data1').removeClass('data-remove');
                            $('#list_data1').html(li_);
                        }
                        else if(ref.id === 'mainfabricfield2') {
                            $('#list_data2').removeClass('data-remove');
                            $('#list_data2').html(li_);
                        }
                        else if(ref.id === 'mainfabricfield3') {
                            $('#list_data3').removeClass('data-remove');
                            $('#list_data3').html(li_);
                        }
                        $(".colorFabricBtn").attr("disabled",false);

                    }
                    else if(status === 'Failure') {
//                       $( "#linningoptionfield").val(message);
                        li_ = li_+'<li>No data found</li>';

                        if(ref.id === 'mainfabricfield') {
                            $('#list_data').removeClass('data-remove');
                            $('#list_data').html(li_);
                        }
                        else if(ref.id === 'mainfabricfield1') {
                            $('#list_data1').removeClass('data-remove');
                            $('#list_data1').html(li_);
                        }
                        else if(ref.id === 'mainfabricfield2') {
                            $('#list_data2').removeClass('data-remove');
                            $('#list_data2').html(li_);
                        }
                        else if(ref.id === 'mainfabricfield3') {
                            $('#list_data3').removeClass('data-remove');
                            $('#list_data3').html(li_);
                        }
                    }

                });
            }

            window.onclick = function () {
                if(!$("#list_data").hasClass('data-remove')) {
                    $("#list_data").addClass('data-remove');
                }

                if(!$("#list_data1").hasClass('data-remove')) {
                    $("#list_data1").addClass('data-remove');
                }
                if(!$("#list_data2").hasClass('data-remove')) {
                    $("#list_data2").addClass('data-remove');
                }
                if(!$("#list_data3").hasClass('data-remove')) {
                    $("#list_data3").addClass('data-remove');
                }
            };

            function savetoSession(type,content_id) {
                var url = "admin/webserver/selectionData.php?sessionType=storeSession&type=custom_pants&content_id="+content_id;
                $.get(url,function(data){
                    console.log('response --- '+data);
                });
            }

            function getSession() {
                var url = "admin/webserver/selectionData.php?type=custom_pants&sessionType=getSession";
                currPage = "category";
                $.get(url,function(data) {
                    data = $.parseJSON(data);
                    if(data.status ==='done') {
                        currPage = data.content;
                        if(data.category!="")
                         category = data.category;
                        if(data.frontpocket!="")
                        frontpocket = data.frontpocket;
                        if(data.cfrontpocket!="")
                         cfrontpocket = data.cfrontpocket;
                        if(data.pleatstyle!="")
                         pleatstyle = data.pleatstyle;
                        if(data.cpleatstyle!="")
                         cpleatstyle = data.cpleatstyle;
                        if(data.watchpocket!="")
                         watchpocket = data.watchpocket;
                        if(data.cwatchpocket!="")
                         cwatchpocket = data.cwatchpocket;
                        if(data.backpocket!="")
                         backpocket = data.backpocket;
                        if(data.backpocketsatin!="")
                         backpocketsatin = data.backpocketsatin;
                        if(data.cbackpocketsatin!="")
                         cbackpocketsatin = data.cbackpocketsatin;
                        if(data.cbackpocket!="")
                         cbackpocket = data.cbackpocket;
                        if(data.belt!="")
                         belt = data.belt;
                        if(data.pleat!="")
                          pleat = data.pleat;
                        if(data.beltloop!="")
                         beltloop = data.beltloop;
                        if(data.cbeltloop!="")
                         cbeltloop = data.cbeltloop;
                        if(data.pantbottom!="")
                         pantbottom = data.pantbottom;
                        if(data.cpantbottom!="")
                         cpantbottom = data.cpantbottom;
                        if(data.pantcolor!="")
                          pantcolor = data.pantcolor;
                        if(data.folder!="")
                         folder = data.folder;

                    }


                    $(".steps").removeClass("stepsactive");
                    $(".stepdiv").fadeOut("slow");
                    $("#"+currPage+"s").fadeIn("slow");
                    $("#"+currPage).addClass("stepsactive");
                    loadSessionData();


                });
            }

             var not_found = 0;
            $(".outputsugg").error(function(){

                if(not_found>6) {
                    return;
                }
                switch(this.id) {
                    case "pantimg":
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        break;
                    case "beltimg":
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        break;
                    case "beltloopimg":
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        break;
                    case "pleatimg":
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        break;
                    case "frontpocketimg":
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        break;
                    case "pantbottomimg":
                        $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                        break;
                }
                not_found++;

            });

//            getSession();
            loadSessionData();

        </script>
    </div>