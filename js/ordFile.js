function uploadOrd() {
    $(".modal-header").css({"display":"block"});
    $(".modal-body").css({"display":"block"});
    $(".modal-footer").css({"display":"block"});
    $(".modal-title").html("Upload Ord File");
    $(".modal-body").html("<div class='row'><p class='errormsg'></p></div><div class='row'><div class='col-md-6'>" +
        "<div class='form-group'><label>Enter File Name</label><input type='text' placeholder='Enter File Name' " +
        "id='ordname' class='form-control' /></div><div class='form-group'><label>Select Ord File</label>" +
        "<input type='file' id='selectedordfile' value='' class='form-control' /></div></div><div class='col-md-6'>" +
        "<label>Instructions</label><li class='inst'>Selected File Should Be in .ord Format</li>" +
        "<li class='inst'>Selected File Size Should Not Be Exceeded From 2 MB (Mega Bytes)</li></div></div>");
    $(".modal-footer").html("<label id='prog' style='margin-right:80px'></label><input type='button' value='Cancel' " +
        "data-dismiss='modal' class='btn btn-default' /><input type='button' value='Upload' onclick='uploadFile()' " +
        "class='btn btn-danger' />");
    $("#modal").modal("show");
}
function uploadFile() {
    $(".errormsg").html("");
    var file_name = $("#selectedordfile").val();
    var ordname = $("#ordname").val();
    var user_id = $("#user_id").val();
    if (file_name == "") {
        $(".errormsg").html("Please Select a ORD File First");
        return false;
    }
    var file_ext = file_name.split(".");
    file_ext = file_ext[file_ext.length - 1];
    file_ext = file_ext.toLowerCase();
    if (file_ext != "ord") {
        $(".errormsg").html("Please Select an ORD File Only");
        return false;
    }
    var data = new FormData();
    var _file = document.getElementById('selectedordfile');
    data.append('file', _file.files[0]);
    data.append('type', "uploadord");
    data.append('user_id', user_id);
    data.append('ordname', ordname);
    var request = new XMLHttpRequest();
    request.onreadystatechange = function () {
        if (request.readyState == 4) {
            var response = $.parseJSON(request.response);
            var status = response.Status;
            if (status == "Success") {
                $("#modal").modal("hide");
                location.reload();
            }
            else {
                $("#modal").modal("hide");
                alert("Server Error Occured While Uploading Ord File");
            }
        }
    };
    request.upload.addEventListener('progress', function (e) {
        $("#prog").html("Uploading " + parseInt((e.loaded / e.total) * 100) + '% Complete');
    }, false);
    request.open('POST', 'api/registerUser.php');
    request.send(data);
}
function myfiles(){
    var user_id = $("#user_id").val();
    var url="api/registerUser.php";
    $.post(url,{"type":"getMyFiles","user_id":user_id},function(data){
        var Status = data.Status;
        if(Status == "Success"){
            var filesData = data.filesData;
            var filesDataShow = "<table class='table table-bordered'><tr><td>S.No</td><td>File Name</td><td>Added On</td><td>Action</td></tr>"
            for(var i=0;i<filesData.length;i++){
                filesDataShow = filesDataShow+"<tr><td>"+(i+1)+"</td><td>"+filesData[i].file_name+"</td>" +
                "<td>"+toHumanFormat(filesData[i].added_on)+"</td><td>" +
                "<i class='fa fa-trash' style='cursor:pointer' onclick=deleteOrd('"+filesData[i]._id+"')></i></td></tr>"
            }
            filesDataShow = filesDataShow+"</table>";
            $(".modal-header").css({"display":"block"});
            $(".modal-body").css({"display":"block"});
            $(".modal-footer").css({"display":"block"});
            $(".modal-title").html("My Ord Files");
            $(".modal-body").html("<div class='row'><div class='col-md-12'>"+filesDataShow+"</div></div>");
            $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-default' />");
            $("#modal").modal("show");
        }else{
            showMessage(data.Message);
        }
    });
}
function toHumanFormat(unixTime){
    date = new Date(unixTime * 1000);
    var year    = date.getFullYear();
    var month   = date.getMonth();
    var months = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];
    var day     = date.getDate();
    var hour    = date.getHours();
    var ampm = "AM";
    var minute  = date.getMinutes();
    var seconds = date.getSeconds();
    minute = parseInt(minute);
    seconds = parseInt(seconds);
    hour = parseInt(hour);
    if(hour<12){
        ampm = "AM";
    }else if(hour == 12){
        ampm = "PM";
    }else{
        hour = hour-12;
        ampm = "PM";
    }
    if(hour<10){
        hour = "0"+hour;
    }
    if(minute<10){
        minute = "0"+minute;
    }
    if(seconds<10){
        seconds = "0"+seconds;
    }
    return(day+" "+months[month]+" "+year+" "+hour+":"+minute+":"+seconds+" "+ampm);
}
function deleteOrd(file_id){
    var url = "api/registerUser.php";
    $.post(url,{"type":"deleteFile","file_id":file_id},function(data){
        var Status = data.Status;
        if(Status == "Success"){
            $("#modal").modal("hide");
            setTimeout(function() {
                myfiles();
            },500);
        }else{
            $("#modal").modal("hide");
            setTimeout(function() {
                showMessage(data.Message,'red');
            },500);
        }
    });
}