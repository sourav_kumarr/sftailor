<?Php
require_once('Classes/STYLIST.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$storeClass = new \Classes\STYLIST();
if(isset($_REQUEST["_"])){
    $_SESSION['capd'] = $_REQUEST['_'];
    header("Location:../login?_=cpd");
}
else {
    $requiredfields = array('type');
    ($response = RequiredFields($_POST, $requiredfields));
    if ($response['Status'] == 'Failure') {
        $storeClass->apiResponse($response);
        return false;
    }
    error_reporting(0);
    $type = $_POST['type'];
    if ($type == "store_login") {
        $requiredfields = array('email', 'password');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $storeClass->apiResponse($response);
            return false;
        }
        $username = trim($_POST['email']);
        $password = trim($_POST['password']);
        $response = $storeClass->storeLogin($username, $password);
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $storeClass->apiResponse($response);
    }
    else if ($type == "changeAdminPassword") {
        $requiredfields = array('old_password', 'new_password', 'admin_id');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $storeClass->apiResponse($response);
            return false;
        }
        $adminId = trim($_POST['admin_id']);
        $old_password = MD5(trim($_POST['old_password']));
        $new_password = MD5(trim($_POST['new_password']));
        $response = $storeClass->changeAdminPassword($old_password, $new_password, $adminId);
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $storeClass->apiResponse($response);
    }
    else if ($type == "changeForgotPassword") {
        $requiredfields = array('new_password');
        $response = RequiredFields($_POST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $storeClass->apiResponse($response);
            return false;
        }
        $admin_email = trim($_SESSION['capd']);
        $new_password = MD5(trim($_POST['new_password']));
        $response = $storeClass->changeForgotPassword($new_password, $admin_email);
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $storeClass->apiResponse($response);
    }
    else if ($type == "forgotPassword") {
        $response = $storeClass->getAdminEmail();
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $adminEmail = $response['admin_email'];
        $response = $storeClass->sendMaill($adminEmail);
        $storeClass->apiResponse($response);
    }
    else if ($type == "addAdmins") {

		$adminName 	 = $_POST['admin_name'];
		$email 		 = $_POST['email'];
		$password 	 = MD5(trim($_POST['password']));
		$password2 	 = $_POST['password'];
		$access_menu = $_POST['access_name'];
		$menus 		 = implode(',', $access_menu);
        $requiredfields = array($email,$password);
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $response = $storeClass->addAdministratorData($adminName,$email,$password,$menus,$password2);
		
        $storeClass->apiResponse($response); 
		
    }
	else if ($type == "updateAdministrator") {
		$admin_id 	 = $_POST['adminId'];
		$adminName 	 = $_POST['admin_name'];
		$email 		 = $_POST['email'];
		$password 	 = MD5(trim($_POST['password']));
		$password2 	 = $_POST['password'];
		$access_menu = $_POST['access_name'];
		$menus 		 = implode(',', $access_menu);
        $requiredfields = array($admin_id);
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $response = $storeClass->updateAdministratorData($adminName,$email,$password,$menus,$admin_id,$password2);
        $storeClass->apiResponse($response); 
    }
	 else if ($type == "deleteAdmin") {
		$admin_id 	 = $_POST['admin_id'];
        $requiredfields = array($admin_id);
        if ($response[STATUS] == Error) {
            $storeClass->apiResponse($response);
            return false;
        }
        $response = $storeClass->delAdministratorData($admin_id);
        $storeClass->apiResponse($response); 
    }
    else {
        $response[STATUS] = Error;
        $response[MESSAGE] = "502 UnAuthorised Request";
        $storeClass->apiResponse($response);
    }
}
?>