<?php
include('header.php');
?>
<style>
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .main-grid-box {
        margin-top: 10px;
    }
    .ext-img {
        height: 240px;
        background-position: center center;
        background-size: 100% auto;
        background-repeat: no-repeat;
    }
    #partnerBox{
        max-height: 900px;
        overflow-y: auto;
    }
    .delico {
        position: absolute;
        top: 10px;
        right: 20px;
        color: #fff;
        font-size: 15px;
        background: #000;
        border-radius: 50%;
        text-align: center;
        height: 24px;
        width: 24px;
        padding-top: 4px;
        cursor: pointer;
    }
    .categoryDataRow{
        display: none;
    }
    .evelist {
        margin-top: 10px;
        font-size: 18px;
        border-bottom: 1px solid #eee;
        cursor: pointer;
    }
    .old_eve_img{
        position: absolute;
        top: 22px;
        height: 100px;
        width: 125px;
        opacity: 0;
    }
    .oldpartnerimg{
        height:100px;
        width:125px;
    }
    .hidenow {
        display:none;
    }
</style>
<link href = "js/jquery-ui.css" rel = "stylesheet">
<script src="js/jquery-ui.js"></script>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>

<!--  page content  -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Partner <small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <form method="post" class="form-inline">
                                <div class="form-group form-inline">
                                    <input type="button" Value="+ Add Partner" class="btn btn-primary btn-sm" name="filterButton"
                                           onclick="addPartner()" style="margin-bottom: -1px" id="addPartnerBtn" />
                                </div>
                            </form>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--body part start here-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12" id="categoryBoxRow">
                <label>List of Category</label><hr>
                <div class="categoryBox"></div>
            </div>
            <div class="col-md-12" id="partnerBoxRow" style="display: none">
                <button class="btn btn-primary" style="height: 35px;width: 55px" onclick="switchList('partnerBoxRow')">
                    <i class="fa fa-caret-left" aria-hidden="true" style="font-size: 20px"></i>
                </button>
                <div id="partnerBox" ></div>
            </div>
        </div>
    </div>
    <!--body part end here-->
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<!-- script -->
<script>
    var oldEventImagechanged = "no";
    var idOrderAll = [];
    var currentEvent = "";
    function switchList(hideThis){
        if(hideThis == "partnerBoxRow"){
            $("#categoryBoxRow").show();
            $("#partnerBoxRow").hide();
        }
        else{
            $("#categoryBoxRow").hide();
            $("#partnerBoxRow").show();
        }

    }
    var  partner_cat_arr =[];
    function getCategory(){
        var url = "api/partnerProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $("#partnerBoxRow").hide();
        $.post(url,{"type":"getCategory"},function(data) {
            var status = data.Status;
            var message = data.Message;
            if(status == "Success"){
                var catData = data.catData;
                var catBox ="";
                for(var a=0; a<catData.length; a++){
                    var category = encodeURI(catData[a].category);
                    partner_cat_arr.push(catData[a].category);
                    catBox= catBox +'<div class="col-sm-3 main-grid-box" style="width:22%;margin-right:20px;' +
                    'cursor:pointer;border:1px solid #ccc;height:150px" ><div onclick=getPartCatData("'+category+'") ' +
                    'class="pdf-imgr" style="text-align:center;padding-top: 50px;font-size: 24px;height:150px;' +
                    'text-decoration: underline">'+catData[a].category+'</div><i class="fa fa-trash delico" ' +
                    'onclick=confirmDelCategory(encodeURI("'+catData[a].category+'")) ></i><i class="fa fa-edit delico" ' +
                    'style="right:50px" onclick=editCategory("'+category+'") ></i></div>';
                }
                catBox +='<div class="col-sm-3 main-grid-box" onclick=getPartnerData() ' +
                'style="border:1px solid #ccc;cursor:pointer;width:22%;margin-right:20px;height:150px">' +
                '<div class="pdf-imgr" style="text-align:center;padding-top: 30px;font-size: 24px;height:150px;' +
                'text-decoration: underline">All Logo Images<br> Click To View</div></div>';
                $(".categoryBox").html(catBox);
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
            $(".loaderdiv").addClass("loaderhidden");
        });
    }
    getCategory();
    function addPartner(){
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html('Add Partner');
        $(".modal-body").html("<p id='message'></p><div class='row'><p id='catType' style='display:none '>notNew</p>" +
            "<div class='col-md-6'><label> Add Category</label><select id='catPartner' onchange='checkCatType()' class='form-control'>" +
            "<option value =''>Select Category</option></select><input type='text' style='display: none' class='form-control catPartnerInput' placeholder='" +
            "add new category'></div>" +
            "<div class='col-md-6'><div class='form-group'>" +
            "<label>Partner Title</label><input type='text' class='form-control' id='fairTitle' placeholder='partner title' /></div></div>" +
            "</div><div class='row'><div class='col-md-6'> <div class='form-group'><label>" +
            "Partner Link</label><input type='text' class='form-control' id='fairLink' placeholder='partner link'/></div></div>" +
            "<div class='col-md-6'><div class='form-group'><label>" +
            "Partner Logo</label><input type='file' class='form-control' id='fairLogo' /></div></div></div>" +
            "<div class='row'> <div class='col-md-6 pull-right'><input type='submit' class='btn btn-info pull-right'  value='Add Partner'" +
            " onclick=partnerAdd(); style='margin-top: 24px' /></div></div>" +
            "");
        $(".modal-footer").css({"display":"none"});
        $("#myModal").modal("show");

        var option ="";
        $("#catPartner").empty();
        $("#catPartner").append(new Option("Select Category",""));
        for (var i=0; i<partner_cat_arr.length; i++){
            $("#catPartner").append(new Option(partner_cat_arr[i], partner_cat_arr[i]));
        }
        $("#catPartner").append(new Option("Add New Option","new option"));
    }
    function checkCatType(){
        var catPartner = $("#catPartner").val();
        if(catPartner == "new option"){
            $(".catPartnerInput").show();
            $("#catPartner").hide();
            $("#catType").html("new");
        }
        else
        {
            $(".catPartnerInput").hide();
            $("#catPartner").show();
            $("#catType").html("notNew");
        }
    }
    function partnerAdd() {
        var fairTitle = $("#fairTitle").val();
        var fairLink = $("#fairLink").val();
        var fairLogo = $("#fairLogo").val();
        var catType = $("#catType").html();
        if(catType == "new"){
            var catPartner = $(".catPartnerInput").val();
        }
        else if(catType == "notNew"){
            var catPartner = $("#catPartner").val();
        }

        if( fairTitle == "" || fairLink == "" || fairLogo == ""|| catPartner == ""){
            $("#message").html("Please fill required fields");
            $("#message").css("color","red");
            return false;
        }
        var data = new FormData();
        $("#message").html("");
        $(".loaderdiv").removeClass("loaderhidden");
        var _file = document.getElementById('fairLogo');
        var image_ext = fairLogo.split(".");
        image_ext = image_ext[image_ext.length - 1];
        if (image_ext == "jpg" || image_ext == "png" || image_ext == "gif") {
            data.append('type', "addPartner");
            data.append('fairTitle', fairTitle);
            data.append('fairLink', fairLink);
            data.append('catPartner', catPartner);
            data.append('file_', _file.files[0]);
        }
        else{
            $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
            $(".loaderdiv").addClass("loaderhidden");
            return false;
        }
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#message").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else
                {
                    $("#message").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/partnerProcess.php');
        request.send(data);
    }
    function partnerUpdate() {
        var fairTitle = $("#fairTitle").val();
        var fairLink = $("#fairLink").val();
        var fairLogo = $("#newpartnerimg").val();
        var catPartner = $("#catPartner").val();
        var pid = $("#pid").val();
        var catType = $("#catType").html();
        if(catType == "new"){
            catPartner = $(".catPartnerInput").val();
        }
        var data = new FormData();
        if(fairLogo != ""){
            var new_partner_file = $("#newpartnerimg").val();
            var new_file_partner = document.getElementById('newpartnerimg');
            var new_partner_image_ext = new_partner_file.split(".");
            new_partner_image_ext = new_partner_image_ext[new_partner_image_ext.length - 1];
            if (new_partner_image_ext == "jpg" || new_partner_image_ext == "png" || new_partner_image_ext == "gif") {
                data.append('file_', new_file_partner.files[0]);
                oldEventImagechanged = "yes";
            }
            else{
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color","red");
            }
        }
        if(fairTitle == "" || fairLink == ""){
            $("#message").html("Please fill required fields");
            $("#message").css("color","red");
            return false;
        }
        $("#message").html("");
        $(".loaderdiv").removeClass("loaderhidden");
        data.append('type', "editPartner");
        data.append('fairTitle', fairTitle);
        data.append('pid', pid);
        data.append('fairLink', fairLink);
        data.append('catPartner', catPartner);
        data.append('oldEventImagechanged', oldEventImagechanged);

        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $("#message").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else
                {
                    $("#message").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/partnerProcess.php');
        request.send(data);
    }
    function getPartnerData(){
        idOrderAll = [];
        var url = "api/partnerProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url,{"type":"getPartner"},function(data){
            var status = data.Status;
            var galleryData = data.data;
            if(status == "Success"){
                /* showMessage(data.Message, "green");*/
                var partnerBox = "";
                for(var a= 0; a < galleryData.length; a++){
                    idOrderAll.push(galleryData[a].id);
                    partnerBox = partnerBox+'<div id="' + galleryData[a].sort_order + '" name="' + galleryData[a].id + '" class="col-sm-3 main-grid-box sortId_' + galleryData[a].sort_order + '" style="height: 310px;">' +
                    '<div class="pdf-imgr ext-img" style=background-image:url("api/Files/images/'+galleryData[a].fairLogo+'")>' +
                    '</div><div class="action-point">' +
                    '<i class="fa fa-edit smallicon delico" ' + ' style="color:skyblue;margin-right: 25px;" onclick=editPartner("'+escape(galleryData[a].id)+'","'+escape(galleryData[a].category)+'","'+escape(galleryData[a].fairTitle)+'","'+escape(galleryData[a].fairLink)+'","'+escape(galleryData[a].fairLogo)+'"); ></i>' +
                    '<i class="fa fa-trash delico" onclick=confirmPartnerDelete("'+galleryData[a].id+'","'+encodeURI(galleryData[a].fairLogo)+'");>' +
                    '</i></div><p class="imagetitle">'+galleryData[a].fairTitle+'</p></div>';
                }
                $("#partnerBox").html(partnerBox);
                $("#partnerBoxRow").show();
                $("#categoryBoxRow").hide();
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                showMessage(data.Message, "red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
        });
    }
    //getPartnerData();
    function readURL(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                oldEventImagechanged = "yes";
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function editPartner(partnerId,category,fairTitle,fairLink,fairLogo) {
       var partnerId= decodeURI(partnerId);
       var category = decodeURI(category);
       var fairTitle = decodeURI(fairTitle);
       var fairLink =decodeURI(fairLink);
       var fairLogo =decodeURI(fairLogo);
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html('Edit Partner');
        $(".modal-body").html("<p id='message'></p><div class='row'><p id='catType' style='display:none '>notNew</p>" +
            "<input type='hidden' name='pid' id='pid' value='"+partnerId+"'><div class='col-md-6'><label> Edit Category</label>" +
            "<select id='catPartner' onchange='checkCatType()' class='form-control'>" +
            "<option value =''>Select Category</option></select><input type='text' style='display: none' class='form-control catPartnerInput' placeholder='" +
            "add new category'></div>" +
            "<div class='col-md-6'><div class='form-group'>" +
            "<label>Partner Title</label><input type='text' class='form-control' value='"+fairTitle+"' id='fairTitle' placeholder='partner title' /></div></div>" +
            "</div><div class='row'><div class='col-md-6'> <div class='form-group'><label>" +
            "Partner Link</label><input type='text' class='form-control' id='fairLink' value='"+fairLink+"' placeholder='partner link'/></div></div>" +
            "<div class='col-md-6'><div class='form-group'><label>" +
            "Partner Logo</label><br><img src='api/Files/images/"+fairLogo+"' class='oldpartnerimg' />" +
            "<input type='file' class='form-control old_eve_img' onchange='readURL(this,"+'"oldpartnerimg"'+")' id='newpartnerimg' /></div></div></div>" +
            "<div class='row'> <div class='col-md-6 pull-right'><input type='submit' class='btn btn-info pull-right'  value='Update Partner'" +
            " onclick=partnerUpdate(); style='margin-top: 24px' /></div></div>" +
            "");
        $(".modal-footer").css({"display":"none"});
        $("#myModal").modal("show");

        var option ="";
        $("#catPartner").empty();
        $("#catPartner").append(new Option("Select Category",""));
        for (var i=0; i<partner_cat_arr.length; i++){
                $("#catPartner").append(new Option(partner_cat_arr[i], partner_cat_arr[i]));
        }
        $("#catPartner").append(new Option("Add New Option","new option"));
        $("#catPartner option").each(function(){
            if ($(this).text() == category)
                $(this).attr("selected","selected");
        });
    }
    function confirmPartnerDelete(id,path) {
        $(".modal-footer").show();
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>'+
            '<button type="button" class="btn btn-danger" onclick=deletePartnerData("'+id+'","'+path+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function deletePartnerData(id,path) {
        var url ='api/partnerProcess.php';
        path = decodeURI(path);
        $.post(url,{"type":'deletePartner','id':id,"path":path},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                location.reload();
            }
            else{
                $("#myModal").modal("hide");
                showMessage(message,"green");
                getBrandData();
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    function editCategory(catName){
        catName = decodeURI(catName);
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html('Edit Category');
        $(".modal-body").html("<p id='message'></p><div class='row'>" +
            "<div class='col-md-6'><div class='form-group'>" +
            "<label>Category Title</label><input type='text' class='form-control' id='catTitle' value='"+catName+"' /></div></div>" +
            "<div class='col-md-6 pull-right'><input type='submit' class='btn btn-info pull-right'  value='Update Category'" +
            " onclick=categoryEdit(encodeURI('"+catName+"')); style='margin-top: 24px' /></div></div>" +
            "");
        $(".modal-footer").css({"display":"none"});
        $("#myModal").modal("show");
    }
    function categoryEdit(catName){
        catName = decodeURI(catName);
        var catTitle = $("#catTitle").val();
        if(catTitle == ""){
            $("#message").html("Please fill category title");
            $("#message").css("color","red");
            return false;
        }
        var url = "api/partnerProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url,{"type":"updateCategory","newCatName":catTitle,"oldCatName":catName},function(data) {
            var status = data.Status;
            var message = data.Message;
            if(status == "Success"){
                $("#message").html(message);
                $("#message").css("color","green");
                $(".loaderdiv").addClass("loaderhidden");
                setTimeout(function(){
                    location.reload();
                },1000);
            }
            else{
                $("#message").html(message);
                $("#message").css("color","red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            $("#message").html("Unable to process the request");
            $("#message").css("color","red");
            $(".loaderdiv").addClass("loaderhidden");
        });
    }
    function confirmDelCategory(catName){
        catName = decodeURI(catName);
        $(".modal-footer").show();
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>This Category Contain  dependent data!!!. " +
            "Are you sure you want to delete "+catName+" category and dependent data</label>");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>'+
            '<button type="button" class="btn btn-danger" onclick=deleteCategory(encodeURI("'+catName+'"))>Delete</button>');
        $("#myModal").modal("show");
    }
    function deleteCategory(catName){
        catName = decodeURI(catName);
        var url ='api/partnerProcess.php';
        $.post(url,{"type":'deleteCategory','catName':catName},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                location.reload();
            }
            else{
                $("#myModal").modal("hide");
                showMessage(message,"green");
                getBrandData();
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
    }
    function getPartCatData(catName){
        currentEvent = catName;
        catName = decodeURI(catName);
        var url = "api/partnerProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url,{"type":"getPartner"},function(data){
            var status = data.Status;
            var galleryData = data.data;
            if(status == "Success"){
                /* showMessage(data.Message, "green");*/
                var partnerBox = "";
                for(var a= 0; a < galleryData.length; a++){
                    idOrderAll.push(galleryData[a].id);
                    if(galleryData[a].category === catName) {
                        partnerBox = partnerBox + '<div id="' + galleryData[a].sort_order + '" name="' + galleryData[a].id + '" class="col-sm-3 main-grid-box sortId_' + galleryData[a].sort_order + '" style="height: 310px;">' +
                        '<div class="pdf-imgr ext-img" style=background-image:url("api/Files/images/' + galleryData[a].fairLogo + '")>' +
                        '</div><div class="action-point">' +
                        '<i class="fa fa-edit smallicon delico" ' + ' style="color:skyblue;margin-right: 25px;" onclick=editPartner("' + escape(galleryData[a].id) + '","' + escape(galleryData[a].category) + '","' + escape(galleryData[a].fairTitle) + '","' + escape(galleryData[a].fairLink) + '","' + escape(galleryData[a].fairLogo) + '"); ></i>' +
                        '<i class="fa fa-trash delico" onclick=confirmPartnerDelete("' + galleryData[a].id + '","' + encodeURI(galleryData[a].fairLogo) + '");>' +
                        '</i></div><p class="imagetitle">' + galleryData[a].fairTitle + '</p></div>';
                    }else{
                        partnerBox = partnerBox + '<div id="' + galleryData[a].sort_order + '" name="' + galleryData[a].id + '" class="col-sm-3 main-grid-box hidenow sortId_' + galleryData[a].sort_order + '" style="height: 310px;">' +
                        '<div class="pdf-imgr ext-img" style=background-image:url("api/Files/images/' + galleryData[a].fairLogo + '")>' +
                        '</div><div class="action-point">' +
                        '<i class="fa fa-edit smallicon delico" ' + ' style="color:skyblue;margin-right: 25px;" onclick=editPartner("' + escape(galleryData[a].id) + '","' + escape(galleryData[a].category) + '","' + escape(galleryData[a].fairTitle) + '","' + escape(galleryData[a].fairLink) + '","' + escape(galleryData[a].fairLogo) + '"); ></i>' +
                        '<i class="fa fa-trash delico" onclick=confirmPartnerDelete("' + galleryData[a].id + '","' + encodeURI(galleryData[a].fairLogo) + '");>' +
                        '</i></div><p class="imagetitle">' + galleryData[a].fairTitle + '</p></div>';
                    }
                }
                $("#partnerBox").html(partnerBox);
                $("#partnerBoxRow").show();
                $("#categoryBoxRow").hide();
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                showMessage(data.Message, "red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
        });
    }
    $('#partnerBox').sortable({
        update: function(event, ui) {
            var sortOrder = $(this).sortable('toArray').toString();
            sortOrder = sortOrder.split(",");
            var updateOrderId = [];
            var updateSortOrder = [];
            for(var a=0; a < sortOrder.length; a++){
                var sortNumber = $(".sortId_"+sortOrder[a]).attr("name");
                sortNumber = parseInt(sortNumber);
                updateOrderId.push(sortNumber);
                updateSortOrder.push(a);
            }
            var url = "api/partnerProcess.php";
            $(".loaderdiv").removeClass("loaderhidden");
            $.post(url,{"type":'updateSortOrder',"sortOrder":updateSortOrder,"idOrder":updateOrderId},function (data) {
                var status = data.Status;
                if(status === "Success") {
//                    window.location.reload();
                    if(currentEvent == ""){
                        getPartnerData();
                    }else{
                        getPartCatData(currentEvent);
                    }
                }
            });
        }
    });
</script>
<!-- / script -->
