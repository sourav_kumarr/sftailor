<script src="js/jquery.min.js"></script>
<?php
$paypal_url = 'https://www.paypal.com/cgi-bin/webscr'; //Test PayPal API URL
//$paypal_id = 'daisyvc@sftailors.com'; //Business Email
$order_id = $_REQUEST['order_id'];
$order_amount = $_REQUEST['order_amount'];

include('../admins/api/Classes/CONNECT.php');
include('../admins/api/Constants/DbConfig.php');
include('../admins/api/Constants/configuration.php');
include('../admins/api/Classes/ADMIN.php');
$conn = new \Classes\CONNECT();
require_once('../admins/api/Classes/USERCLASS.php');
$userClass = new \Classes\USERCLASS();
$getPayData = $userClass->getActivePaymentId();
$paypal_id = $getPayData['payData']['payment_id'];
if($paypal_id == ""){
    $paypal_id = 'daisyvc@sftailors.com'; //Business Email
}


?>
<form id="myForm" name="myForm" action="<?php echo $paypal_url; ?>" method="post">
    <!-- Identify your business so that you can collect the payments. -->
    <input name="business" value="<?php echo $paypal_id; ?>" type="hidden">
    <!-- Specify a Buy Now button. -->
    <input name="cmd" value="_xclick" type="hidden">
    <!--    <input type="hidden" name="cmd" value="_cart">-->
    <!-- Specify details about the item that buyers will purchase. -->
    <input name="item_name" value="Orders" type="hidden">
    <input name="item_number" value="<?php echo $order_id ?>" type="hidden">
    <input name="amount" value="<?php echo $order_amount ?> " type="hidden">
    <input name="currency_code" value="USD" type="hidden">
    <!-- Specify URLs -->
    <input type='hidden' name='cancel_return' value="http://scan2fit.com/sftailor/admin/webserver/payment.php?payment_status=Failed&order_id=<?php echo $order_id ?>&paid_amount=<?php echo $order_amount ?>" />
    <input name="return" value="http://scan2fit.com/sftailor/admin/webserver/payment.php?payment_status=Success&order_id=<?php echo $order_id ?>&paid_amount=<?php echo $order_amount ?>" type="hidden"/>
    <!-- Display the payment button. -->
    <input style="display:none" name="submit" value="PayNow" id="paybtn" type="submit">
</form>
<script>
    $(document).ready(function(){
        $("#paybtn").click();
    });
</script>