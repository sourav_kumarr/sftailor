<?php
$user_id = "";
if(!(isset($_REQUEST['user_id']))){
    header("Location:bodystyle.php");
}
$user_id = $_REQUEST['user_id'];
echo "<input type='hidden' value='".$_REQUEST['user_id']."' id='user_id' />";
include('header.php');
?>
<style>
    .seatimg {
        height: 150px;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count"></div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Body Style<small></small></h2>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            Body Style Answers / Values
                        </p>
                        <div class="col-md-12 main-ques currPage1 step" id="step1" style="display:block;text-align: center;">
                            <h3 style="margin-bottom: 50px;">Shoulder Forward / Body Backwards (Men)</h3>
                            <div class="col-md-2">
                                <img src="../images/shape/verybackward.png" class="quess-images">
                                <input type="radio" name="first_ques" id="very_backward" value="Very Backward">
                                <p>Very Backward</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/slightbackward.png" class="quess-images">
                                <input type="radio" name="first_ques" id="slightly_backward" value="Slight Backward">
                                <p>Slight Backward</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/regular.png" class="quess-images">
                                <input type="radio" name="first_ques" id="regular" value="Regular">
                                <p>Regular</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/slightforward.png" class="quess-images">
                                <input type="radio" name="first_ques" id="slightly_forward" value="Slight Forward">
                                <p>Slight Forward</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/veryforward.png" class="quess-images">
                                <input type="radio" name="first_ques" id="very_forward" value="Very Forward">
                                <p>Very Forward</p>
                            </div>
                        </div>
                        <!-----Second Question Div ---------->
                        <div class="col-md-12 main-ques2 currPage2 step" id="step2" style="display:block;text-align: center;">
                            <hr>
                            <h3 style="margin-bottom: 50px;">Body Forward / Body Backwards (Men)</h3>
                            <div class="col-md-2">
                                <img src="../images/shape/bodyforward10.png" class="quess-images">
                                <input type="radio" id="body_forward_10" name="second_ques" value="Body Forward 1.0">
                                <p>Body Forward 1.0</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bodyforward08.png" class="quess-images">
                                <input type="radio" id="body_forward_08" name="second_ques" value="Body Forward 0.8">
                                <p>Body Forward 0.8</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bodyforward05.png" class="quess-images">
                                <input type="radio" name="second_ques" id="body_forward_05" value="Body Forward 0.5">
                                <p>Body Forward 0.5</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bodyforward05.png" class="quess-images">
                                <input type="radio" name="second_ques" id="normal" value="normal">
                                <p>Normal</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bodybackward05.png" class="quess-images">
                                <input type="radio" name="second_ques" id="body_backward_05" value="Body Backward 0.5">
                                <p>Body Backward 0.5</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bodybackward08.png" class="quess-images">
                                <input type="radio" name="second_ques" id="body_backward_08" value="Body Backward 0.8">
                                <p>Body Backward 0.8</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bodybackward10.png" class="quess-images">
                                <input type="radio" name="second_ques" id="body_backward_10" value="Body Backward 1.0">
                                <p>Body Backward 1.0</p>
                            </div>
                        </div>
                        <!-----Third Question Div ---------->
                        <div class="col-md-12 currPage step" id="step3" style="display:block;text-align: center;">
                            <hr>
                            <div class="col-md-6">
                                <h3 style="margin-bottom: 50px;">Shoulder Slope Left </h3>
                                <div class="col-md-6">
                                    <img src="../images/shape/sholderslop_left.png" style="width:250px">
                                </div>
                                <div class="col-md-6">
                                    <p style="text-align: left;">Shoulder can be identified by shoulder device, please refer to our body
                                        measuring method to learn the way how to use shoulder device.</p>
                                    <h4 style="text-align: left; text-transform: uppercase; margin-top: 24px;">Identity method Left</h4>
                                    <select style="float: left; margin-top: 17px; height: 30px; width: 260px; border: 2px solid rgb(68, 68, 68);" name="third_ques" id="thirdQuestion_left">
                                        <option value="Very high shoulder-G">Very high shoulder-G</option>
                                        <option value="High shoulder-F">High shoulder-F</option>
                                        <option value="Slight High E">Slight High E</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Slight Slope-B">Slight Slope-B</option>
                                        <option value="Slope-C">Slope-C</option>
                                        <option value="Very Slope-D">Very Slope-D</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 style="margin-bottom: 50px;">Shoulder Slope Right </h3>
                                <div class="col-md-6">
                                    <img src="../images/shape/sholderslop_right.png" style="width:250px">
                                </div>
                                <div class="col-md-6">
                                    <p style="text-align: left;">Shoulder can be identified by shoulder device, please refer to our body
                                        measuring method to learn the way how to use shoulder device.</p>
                                    <h4 style="text-align: left; text-transform: uppercase; margin-top: 24px;">Identity method Right</h4>
                                    <select style="float: left; margin-top: 17px; height: 30px; width: 260px; border: 2px solid rgb(68, 68, 68);" name="third_ques" id="thirdQuestion_right">
                                        <option value="Very high shoulder-G">Very high shoulder-G</option>
                                        <option value="High shoulder-F">High shoulder-F</option>
                                        <option value="Slight High E">Slight High E</option>
                                        <option value="Regular">Regular</option>
                                        <option value="Slight Slope-B">Slight Slope-B</option>
                                        <option value="Slope-C">Slope-C</option>
                                        <option value="Very Slope-D">Very Slope-D</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!-----Fourth Question Div ---------->
                        <div class="col-md-12 main-ques2 step" id="step4" style="display:block;text-align: center;">
                            <hr>
                            <h3 style="margin-bottom: 50px;">Belly Shape (Visual)</h3>
                            <div class="col-md-4"></div>
                            <div class="col-md-2">
                                <img src="../images/shape/bellyregular.png" class="quess-images">
                                <input type="radio" name="forth_ques" value="Regular">
                                <p>Regular</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/bellyportly.png" class="quess-images">
                                <input type="radio" name="forth_ques" value="Portly">
                                <p>Portly</p>
                            </div>
                        </div>
                        <!-----Fifth Question Div ---------->
                        <div class="col-md-12 main-ques2 step" id="step5" style="display:block;text-align: center;">
                            <hr>
                            <h3 style="margin-bottom: 50px;">Arms (Visual)</h3>
                            <div class="col-md-3"></div>
                            <div class="col-md-2">
                                <img src="../images/shape/armsbackward.png" class="quess-images">
                                <input type="radio" name="fifth_ques" value="Backward">
                                <p>Backward</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/armsregular.png" class="quess-images">
                                <input type="radio" name="fifth_ques" value="Regular">
                                <p>Regular</p>
                            </div>
                            <div class="col-md-2">
                                <img src="../images/shape/armsforward.png" class="quess-images">
                                <input type="radio" name="fifth_ques" value="Forward">
                                <p>Forward</p>
                            </div>
                        </div>
                        <!-----sixth Question Div ---------->
                        <div class="col-md-12 main-ques2 step" id="step6" style="display:block;text-align: center;">
                            <hr>
                            <h3 style="margin-bottom: 50px;">Seat (Visual)</h3>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-2 op1">
                                    <img src="../images/shape/seatnormal.png" class="quess-images seatimg">
                                    <input type="radio" name="sixth_ques" value="Normal">
                                    <p>Normal</p>
                                </div>
                                <div class="col-md-2 op1">
                                    <img src="../images/shape/seatfelt.png" class="quess-images seatimg">
                                    <input type="radio" name="sixth_ques" value="Felt">
                                    <p>Felt</p>
                                </div>
                                <div class="col-md-2 op1">
                                    <img src="../images/shape/seatprominent.png" class="quess-images seatimg">
                                    <input type="radio" name="sixth_ques" value="Prominent">
                                    <p>Prominent</p>
                                </div>
                                <div class="col-md-2 op1">
                                    <img src="../images/shape/seatdrop.png" class="quess-images seatimg">
                                    <input type="radio" name="sixth_ques" value="Drop">
                                    <p>Drop</p>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <!-----shirt seventh Question Div ---------->
                        <div class="col-md-12 main-ques2 step" id="step6a" style="display:block;text-align: center;">
                            <hr>
                            <h3 style="margin-bottom: 50px;">Stoop (Visual)</h3>
                            <div class="col-md-2"></div>
                            <div class="col-md-8">
                                <div class="col-md-2 op1">
                                    <img src="../images/stoopnormal.jpg" class="quess-images">
                                    <input type="radio" name="sixth_ques_stoop" value="Regular">
                                    <p>Regular</p>
                                </div>
                                <div class="col-md-2 op1">
                                    <img src="../images/stoopnormal.jpg" class="quess-images">
                                    <input type="radio" name="sixth_ques_stoop" value="Slight Stoop">
                                    <p>Slight Stoop</p>
                                </div>
                                <div class="col-md-2 op1">
                                    <img src="../images/stoopl.jpg" class="quess-images">
                                    <input type="radio" name="sixth_ques_stoop" value="Moderate Stoop">
                                    <p>Moderate Stoop</p>
                                </div>
                                <div class="col-md-2 op1">
                                    <img src="../images/stoopl.jpg" class="quess-images">
                                    <input type="radio" name="sixth_ques_stoop" value="Very Stoop">
                                    <p>Very Stoop</p>
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                        </div>
                        <!-----shirt eighth Question Div ---------->
                        <div class="col-md-12 step" id="step7" style="display:block;text-align: left;">
                            <hr>
                            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">
                                Shirt</h3>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">1) How do you like your shirt to fit through the body?</h4>
                                    <li><input type="radio" name="shirt_ques1" value="European">Very Fitted (european)</li>
                                    <li><input type="radio" name="shirt_ques1" value="Tailored">Tailored to the body not too tight (tailored)</li>
                                    <li><input type="radio" name="shirt_ques1" value="Classic">Roomy (Classic)</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">2) How do you like your shirt sleeve to fit?</h4>
                                    <li><input type="radio" name="shirt_ques2" value="European"> Very Fitted (european)</li>
                                    <li><input type="radio" name="shirt_ques2" value="Tailored">Tailored to the body not too tight (tailored)</li>
                                    <li><input type="radio" name="shirt_ques2" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                                </ul>
                            </div>
                            <div class="col-md-4" style="margin-bottom: 70px;">
                                <ul>
                                    <h4 style="margin-bottom:14px;">3) Do you tuck your shirt in?</h4>
                                    <li><input type="radio" name="shirt_ques3" value="Always tucked">&nbsp;&nbsp; Always tucked</li>
                                    <li><input type="radio" name="shirt_ques3" value="Always untucked">&nbsp;&nbsp; Always untucked</li>
                                    <li><input type="radio" name="shirt_ques3" value="Both">&nbsp;&nbsp; Both</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">4) For your short -sleeve shirts, what length do you like</h4>
                                    <li><input type="radio" name="shirt_ques4" value="Stylish short">&nbsp;&nbsp; Stylish
                                        short, mid biceps
                                    </li>
                                    <li><input type="radio" name="shirt_ques4" value="Standard">&nbsp;&nbsp; Standard,
                                        below biceps
                                    </li>
                                    <li><input type="radio" name="shirt_ques4" value="Classic">&nbsp;&nbsp;
                                        Classic, just above the elbow
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">5) Do you wear a watch</h4>
                                    <li><input type="radio" name="shirt_ques5" value="Left wrist">&nbsp;&nbsp; Left Wrist</li>
                                    <li><input type="radio" name="shirt_ques5" value="Right wrist">&nbsp;&nbsp; Right Wrist</li>
                                    <li><input type="radio" name="shirt_ques5" value="Both">&nbsp;&nbsp; Both</li>
                                    <li><input type="radio" name="shirt_ques5" value="None">&nbsp;&nbsp; None</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">6) Size of the watch</h4>
                                    <li><input type="radio" name="shirt_ques6" value="Normal">&nbsp;&nbsp; Normal</li>
                                    <li><input type="radio" name="shirt_ques6" value="Big">&nbsp;&nbsp; Big</li>
                                </ul>
                            </div>
                        </div>
                        <!-----eidght Question Div ---------->
                        <div class="col-md-12 step" id="step8" style="display:block;text-align: left;">
                            <hr>
                            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">
                                Suit</h3>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">1) How do you like your jackets to fit through the body?</h4>
                                    <li><input type="radio" name="suit_ques1" value="European">&nbsp;&nbsp; Very Fitted
                                        (european)
                                    </li>
                                    <li><input type="radio" name="suit_ques1" value="Tailored">&nbsp;&nbsp;
                                        Tailored to the body not too tight (tailored)
                                    </li>
                                    <li><input type="radio" name="suit_ques1" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">2) How do you like your jacket sleeve to fit?</h4>
                                    <li><input type="radio" name="suit_ques2" value="European">&nbsp;&nbsp; Very Fitted
                                        (european)
                                    </li>
                                    <li><input type="radio" name="suit_ques2" value="Tailored">&nbsp;&nbsp;
                                        Tailored to the body not too tight (tailored)
                                    </li>
                                    <li><input type="radio" name="suit_ques2" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                                </ul>
                            </div>
                            <div class="col-md-4" style="margin-bottom:60px;">
                                <ul>
                                    <h4 style="margin-bottom:14px;">3) How long do you prefer your jacket to be?</h4>
                                    <li><input type="radio" name="suit_ques3" value="European">&nbsp;&nbsp; Stylish Short</li>
                                    <li><input type="radio" name="suit_ques3" value="Tailored">&nbsp;&nbsp;
                                        Standard, covers 2/3 of seat
                                    </li>
                                    <li><input type="radio" name="suit_ques3" value="Classic">&nbsp;&nbsp; Classic
                                        covers entire seat
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">4) How long do you like your jacket sleeves to be jacket to be?</h4>
                                    <li><input type="radio" name="suit_ques4" value="Stylish">&nbsp;&nbsp;
                                        Stylish shorter, show a bit of cuf (about A 1/2)
                                    </li>
                                    <li><input type="radio" name="suit_ques4" value="Standard">&nbsp;&nbsp;
                                        Standard, show a bit a cuf (about A 1/4)
                                    </li>
                                    <li><input type="radio" name="suit_ques4" value="Classic">&nbsp;&nbsp; Classic
                                        covers cuff entiry
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <ul>
                                    <h4 style="margin-bottom:14px;">5) Do you have typically have any of these items in your
                                        jacket?</h4>
                                    <li><input type="radio" name="suit_ques5" value="Wallet">&nbsp;&nbsp; Wallet</li>
                                    <li><input type="radio" name="suit_ques5" value="Phone">&nbsp;&nbsp; Phone</li>
                                    <li><input type="radio" name="suit_ques5" value="Wallet and phone">&nbsp;&nbsp; Wallet and phone
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 step" id="step9" style="display:block;text-align: left;">
                            <hr>
                            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">
                                Vest</h3>
                            <div class="col-md-12">
                                <ul>
                                    <h4 style="margin-bottom:14px;">1) How do you like your vest to fit through the body?</h4>
                                    <li><input type="radio" name="vest_ques1" value="European">&nbsp;&nbsp; Very Fitted
                                        (european)
                                    </li>
                                    <li><input type="radio" name="vest_ques1" value="Tailored">&nbsp;&nbsp;
                                        Tailored to the body not too tight (tailored)
                                    </li>
                                    <li><input type="radio" name="vest_ques1" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 step" id="step10" style="display:block;text-align: left;">
                            <hr>
                            <h3 style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center; color: white; padding: 4px;">Pant</h3>
                            <div class="col-md-12">
                                <ul>
                                    <h4 style="margin-bottom:14px;">1) How do you like your pant to fit through the body?</h4>
                                    <li><span><input type="radio" name="pant_ques1" value="European"> Very Fitted(european)</span></li>
                                    <li><input type="radio" name="pant_ques1" value="Tailored">&nbsp;&nbsp;
                                        Tailored to the body not too tight (tailored)
                                    </li>
                                    <li><input type="radio" name="pant_ques1" value="Classic">&nbsp;&nbsp; Roomy (Classic)</li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-12 text-center">
                            <button class="btn btn-danger" onclick="updateBodyStyleUserData()">Update Values</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    function viewBodyStyle(){
        var user_id = $("#user_id").val();
        if(user_id != "") {
            var url = "../api/registerUser.php";
            $.post(url, {"type": "getStyleGenieData", "user_id": user_id}, function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    var userData = data.userData;
                    var shoulder_slope = userData.shoulder_slope;
                    var shoulder_slope = shoulder_slope.split(",");
                    var shoulder_slope_left = shoulder_slope[0];
                    var shoulder_slope_right = shoulder_slope[1];
                    $("input[name='first_ques'][value='" + userData.shoulder_forward + "' ]").prop('checked', true);
                    $("input[name='second_ques'][value='" + userData.body_forward + "' ]").prop('checked', true);
                    $('#thirdQuestion_left > option[value="' + shoulder_slope_left + '"]').attr("selected", "selected");
                    $('#thirdQuestion_right > option[value="' + shoulder_slope_right + '"]').attr("selected", "selected");
                    $("input[name='forth_ques'][value='" + userData.belly_shape + "']").prop('checked', true);
                    $("input[name='fifth_ques'][value='" + userData.style_arms + "']").prop('checked', true);
                    $("input[name='sixth_ques'][value='" + userData.style_seat + "']").prop('checked', true);
                    $("input[name='sixth_ques_stoop'][value='" + userData.sixth_ques_stoop + "']").prop('checked', true);
                    $("input[name='shirt_ques1'][value='" + userData.shirt_que_1 + "']").prop('checked', true);
                    $("input[name='shirt_ques2'][value='" + userData.shirt_que_2 + "']").prop('checked', true);
                    $("input[name='shirt_ques3'][value='" + userData.shirt_que_3 + "']").prop('checked', true);
                    $("input[name='shirt_ques4'][value='" + userData.shirt_que_4 + "']").prop('checked', true);
                    $("input[name='shirt_ques5'][value='" + userData.shirt_que_5 + "']").prop('checked', true);
                    $("input[name='shirt_ques6'][value='" + userData.shirt_que_6 + "']").prop('checked', true);
                    $("input[name='suit_ques1'][value='" + userData.suit_que_1 + "']").prop('checked', true);
                    $("input[name='suit_ques2'][value='" + userData.suit_que_2 + "']").prop('checked', true);
                    $("input[name='suit_ques3'][value='" + userData.suit_que_3 + "']").prop('checked', true);
                    $("input[name='suit_ques4'][value='" + userData.suit_que_4 + "']").prop('checked', true);
                    $("input[name='suit_ques5'][value='" + userData.suit_que_5 + "']").prop('checked', true);
                    $("input[name='vest_ques1'][value='" + userData.vest_que_1 + "']").prop('checked', true);
                    $("input[name='pant_ques1'][value='" + userData.pant_que_1 + "']").prop('checked', true);
                }
                else {
                    showError("No Body Style Data Available", "red");
                }
            }).fail(function () {
                alert("Server Error !!! Please Try After Some Time");
            });
        }
    }
    function updateBodyStyleUserData(){
        var first_ques = $('input[name=first_ques]:checked').val();
        var second_ques = $('input[name=second_ques]:checked').val();
        var third_ques_left = $("#thirdQuestion_left option:selected").text();
        var third_ques_right = $("#thirdQuestion_right option:selected").text();
        var forth_ques = $('input[name=forth_ques]:checked').val();
        var fifth_ques = $('input[name=fifth_ques]:checked').val();
        var sixth_ques = $('input[name=sixth_ques]:checked').val();
        var sixth_ques_stoop = $('input[name=sixth_ques_stoop]:checked').val();
        var shirt_que_1 = $('input[name=shirt_ques1]:checked').val();
        var shirt_que_2 = $('input[name=shirt_ques2]:checked').val();
        var shirt_que_3 = $('input[name=shirt_ques3]:checked').val();
        var shirt_que_4 = $('input[name=shirt_ques4]:checked').val();
        var shirt_que_5 = $('input[name=shirt_ques5]:checked').val();
        var shirt_que_6 = $('input[name=shirt_ques6]:checked').val();
        var suit_que_1 = $('input[name=suit_ques1]:checked').val();
        var suit_que_2 = $('input[name=suit_ques2]:checked').val();
        var suit_que_3 = $('input[name=suit_ques3]:checked').val();
        var suit_que_4 = $('input[name=suit_ques4]:checked').val();
        var suit_que_5 = $('input[name=suit_ques5]:checked').val();
        var vest_que_1 = $('input[name=vest_ques1]:checked').val();
        var pant_que_1 = $('input[name=pant_ques1]:checked').val();
        var url = "../api/registerUser.php";
        $.post(url, {
            "type": "styleGenieUpdate",
            "user_id": $("#user_id").val(),
            "first_ques": first_ques,
            "second_ques": second_ques,
            "third_ques": third_ques_left + "," + third_ques_right,
            "forth_ques": forth_ques,
            "fifth_ques": fifth_ques,
            "sixth_ques": sixth_ques,
            "sixth_ques_stoop": sixth_ques_stoop,
            "shirt_que_1": shirt_que_1,
            "shirt_que_2": shirt_que_2,
            "shirt_que_3": shirt_que_3,
            "shirt_que_4": shirt_que_4,
            "shirt_que_5": shirt_que_5,
            "shirt_que_6": shirt_que_6,
            "suit_que_1": suit_que_1,
            "suit_que_2": suit_que_2,
            "suit_que_3": suit_que_3,
            "suit_que_4": suit_que_4,
            "suit_que_5": suit_que_5,
            "vest_que_1": vest_que_1,
            "pant_que_1": pant_que_1
        },
        function (data) {
            var status = data.Status;
            if (status == "Success") {
                showError("Size Genie Measurements Created Successfully....", 'green');
            } else {
                showError("Server Error !!! Please Try After Some Time....", 'red');
            }
        });
    }
    viewBodyStyle();
</script>
