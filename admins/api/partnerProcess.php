<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:42 PM
 */

require_once('Classes/PARTNER.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$partnerClass = new \Classes\PARTNER();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $partnerClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
if($type === 'addGalleryImg') {
    $requiredfields = array('event','iseventimg');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $event = $_REQUEST['event'];
    $iseventimg = $_REQUEST['iseventimg'];
    $oldeventimg = $_REQUEST['oldeventimg'];
    $partnerlink = $_REQUEST['partnerlink'];
    $partnername = $_REQUEST['partnername'];
    $logoResponse = $partnerClass->addGalleryImg($event,$iseventimg,$oldeventimg,$partnerlink,$partnername);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'getCategories') {
    $response = $partnerClass->getCategories();
    $partnerClass->apiResponse($response);
}
else if($type === 'getPartner') {
    $logoResponse = $partnerClass->getPartner();
    $partnerClass->apiResponse($logoResponse);
}

else if($type === 'deletePartner') {
    $requiredfields = array('id','path');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $id = $_REQUEST['id'];
    $path = $_REQUEST['path'];
    $logoResponse = $partnerClass->deletePartner($id,$path);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'deleteCategory') {
    $requiredfields = array('category');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $category = $_REQUEST['category'];
    $logoResponse = $partnerClass->deleteCategory($category);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'editCategory') {
    $requiredfields = array('new_category','old_category','oldcategoryImagechanged','oldcategoryImage',);
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $new_category = $_REQUEST['new_category'];
    $old_category = $_REQUEST['old_category'];
    $oldcategoryImagechanged = $_REQUEST['oldcategoryImagechanged'];
    $oldcategoryImage = $_REQUEST['oldcategoryImage'];
    $logoResponse = $partnerClass->editCategory($new_category,$old_category,$oldcategoryImagechanged,$oldcategoryImage);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'editGallery') {
    $requiredfields = array('gallery_id','gallery_title','oldGalleryImagechanged','partner_link');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $gallery_id = $_REQUEST['gallery_id'];
    $gallery_title = $_REQUEST['gallery_title'];
    $oldGalleryImagechanged = $_REQUEST['oldGalleryImagechanged'];
    $partner_link = $_REQUEST['partner_link'];


    $logoResponse = $partnerClass->editGallery($gallery_id,$gallery_title,$oldGalleryImagechanged,$partner_link);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'updateSortOrder') {
    $requiredfields = array('sortOrder','idOrder');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $sortOrder = $_REQUEST['sortOrder'];
    $idOrder = $_REQUEST['idOrder'];
    $logoResponse = $partnerClass->updateSortOrder($sortOrder,$idOrder);
    $partnerClass->apiResponse($logoResponse);
}
else if($type === 'updateCatSortOrder') {
    $requiredfields = array('sortOrder','idOrder');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $partnerClass->apiResponse($response);
        return false;
    }
    $sortOrder = $_REQUEST['sortOrder'];
    $idOrder = $_REQUEST['idOrder'];
    $logoResponse = $partnerClass->updateCatSortOrder($sortOrder,$idOrder);
    $partnerClass->apiResponse($logoResponse);
}
?>