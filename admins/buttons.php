<?php
include('header.php');
?>
<style>
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }

    .block {
        display: block !important;
    }

    .loaderhidden {
        display: none !important;
    }

    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .buttonImg{
        width: 50px;
        height: 50px;
    }
    .old_btn_image{
        position: absolute;
        top: 22px;
        height: 100px;
        width: 125px;
        opacity: 0;
        image-orientation: from-image;
    }
    .oldbtnimg{
        height:100px;
        width:125px;
        image-orientation :from-image;
    }
    #buttonBox{
        max-height: 520px;
        overflow-y: auto;
    }
</style>
<link href="js/jquery-ui.css" rel="stylesheet">
<script src="js/jquery-ui.js"></script>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>

<!--  page content  -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Buttons
                        <small></small>
                    </h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <div class="form-group form-inline">
                                <label id="message" style="color: red"></label>
                                <input type="file" class="form-control" name="btnFile" id="btnFile" />
                                <input type="button" Value="+ Add Button File" class="btn btn-primary btn-sm" name="filterButton"  onclick="addBtnCsv()" style="margin-bottom: -1px"/>
                                <input type="button" class="btn btn-danger btn-sm" onclick="addButton()"
                                       value=" + Add Button"/>
                            </div>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--body part start here-->
    <div class="row">
        <div class="col-md-12">
            <ul class="nav nav-tabs">
                <li class="alltab active" id="jacket" onclick="onTabClicked('jacket')">
                    <a href="#tab1" data-toggle="tab">Jacket</a>
                </li>
                <li class="alltab " id="shirt" onclick="onTabClicked('shirt')">
                    <a href="#tab1" data-toggle="tab">Shirt</a>
                </li>
            </ul>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="buttonBox">
            <div id="jacketData" class="boxdata"> </div>
            <div id="shirtData" class="boxdata" style="display: none"> </div>
        </div>
    </div>
    <!--body part end here-->
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<!-- script -->
<script>
    function addBtnCsv() {
        var btnFile = document.getElementById('btnFile');
        var data = new FormData();
        data.append('type','storeData');
        data.append('btnFile',btnFile.files[0]);
        if($("#btnFile").val() == '') {
            $("#message").html("Please select .xls file");
            return false;
        }
        $(".loaderdiv").removeClass("loaderhidden");
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            // alert(request.readyState);
            if(request.readyState === 4) {
                var response = $.parseJSON(request.response);
                var status = response.Status;
                console.log(status);
                if(status == "Success")
                {
//                    $("#myModal").modal("hide");
//                    location.reload(true);

                    $("#message").html(response.Message);
                    $("#message").css("color","green");
                    getButtonData();
                    setTimeout(function () {
                        $("#message").html('');
                    },1000);
                }
                else
                {
                    $("#message").html(response.Message);
                }
            }
        };
        request.open('POST', 'api/buttonProcess.php');
        request.send(data);
    }









    function onTabClicked(boxValue){
        $(".boxdata").hide();
        $("#"+boxValue+"Data").show();
    }
    var idOrder = [];
    var idOrderAll = [];
    var oldBtnImagechanged = "no";
    var oldBtnImagechanged = "no";
    var currentCategory = "all";

    function addButton() {
        $(".modal-title").html("<label style='color: green'>Add New Button</label>");
        $(".modal-body").html("<div class='row'><div class='col-md-12'><p class='text-center' id='imgError'" +
            " style='color:red'></p>" +
            "<div class='col-md-6 form-group'><label>Select Button For</label>" +
            "<select class='form-control' id='button_for'><option value=''>Select Button For</option>" +
            "<option value='jacket' >Jacket</option><option value='shirt' selected>Shirt</option></select></div>" +
            "<div class='col-md-6 form-group'><label>Button Name</label><input type='text' name='button_name' " +
            "id='button_name' class='form-control' placeholder='Button Name' /></div>" +
            "<div class='col-md-6 form-group'><label>Button Size</label><input type='text' name='button_size' " +
            "id='button_size' class='form-control' placeholder='Button Size' value='16L' /></div>" +
            "<div class='col-md-6 form-group' ><label>Button Price</label><input type='text' name='button_price' " +
            "id='button_price' class='form-control' placeholder='Button Price' value='1.50' /></div>" +
            "<div class='col-md-6 form-group'><label>Button Image</label><input type='file' name='buttonFile'" +
            "id='buttonFile' class='form-control'/></div>" +
            "<div class='col-md-6'><input type='button'" +
            " class='btn btn-danger pull-right' style='margin-top:30px' onclick='addButtonImage()'" +
            " id='subbtn' value='Submit'/></div></div><p class='percentprog' style='color:green;text-align:center'>" +
            "</div>");
        $(".modal-footer").css('display', 'none');
        $("#myModal").modal("show");
    }
    function addButtonImage() {
        var button_for = $("#button_for").val();
        var button_name = $("#button_name").val();
        var button_size = $("#button_size").val();
        var button_price = $("#button_price").val();
        var buttonFile = $("#buttonFile").val();
        if (button_for == "" || button_name == "" || button_size == "" ||  button_price == "" ||  buttonFile == "") {
            $("#imgError").html("Please fill all fields");
            return false;
        }
        var data = new FormData();
        if(button_name.indexOf(" ") > 0 || button_name.indexOf("_") > 0)
        {
            $("#imgError").html("Invalid Button name,  space or underscore( _ ) not allowed");
            return false;
        }

        $("#imgError").html("");
        $("#subbtn").attr("disabled", true);
        var buttonFile = $("#buttonFile").val();
        var _file_btn = document.getElementById('buttonFile');
        var btn_image_ext = buttonFile.split(".");
        btn_image_ext = btn_image_ext[btn_image_ext.length - 1];
        btn_image_ext = btn_image_ext.toLowerCase();
        if (btn_image_ext === "jpg" || btn_image_ext === "png" || btn_image_ext === "gif" || btn_image_ext === "jpeg") {
            data.append('type', "addButtonImg");
            data.append('button_for', button_for);
            data.append('button_name', button_name);
            data.append('button_size', button_size);
            data.append('button_price', button_price);
            data.append('btn_image', _file_btn.files[0]);
        }
        else {
            $("#imgError").html("Image File Should Be JPG, PNG, GIF Formats Only");
            $("#subbtn").attr("disabled", false);
            return false;
        }
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4) {
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if (status == "Success") {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#imgError").html("Succcess");
                    $("#subbtn").attr("disabled", false);
                    $("#myModal").modal("hide");
                    getButtonData();
                }
                else {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#imgError").html(response.Message);
                    $("#subbtn").attr("disabled", false);
                    return false;
                }
            }
        };
        request.open('POST', 'api/buttonProcess.php');
        request.send(data);
    }
    function getButtonData() {
        var url = "api/buttonProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url, {"type": "getButtons"}, function (data) {
            var status = data.Status;
            var buttonData = data.data;
            if (status == "Success") {
                var tbodyShirtData ="";
                var tbodyJacketData ="";
                var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                    '<thead><tr><th>#</th><th>Button Image</th><th>Button For</th><th>Button Name</th>' +
                    '<th>Button Size</th><th>Button Price</th><th>Visibility</th><th>Action</th></tr></thead>';
                var buttonBox = "";
                var shirtNum = 1;
                var jacketNum = 1;
                    for (var a = 0; a < buttonData.length; a++) {
                        var button_status = buttonData[a].status;
                        if(button_status == "1"){
                            var selectStatus = "<select id='updateBtnStatus_"+a+"' style='width: 108px; height: 28px;" +
                                " margin-top: 3px;' onchange=updateBtnStatus('"+buttonData[a].id+"','"+a+"');>" +
                                "<option selected value='"+button_status+"'>Visible</option>" +
                                "<option value='0'>Not Visible</option></select>";
                        }
                        else{
                            var selectStatus = "<select id='updateBtnStatus_"+a+"' style='width: 108px; height: 28px;" +
                                " margin-top: 3px;' onchange=updateBtnStatus('"+buttonData[a].id+"','"+a+"');>" +
                                "<option  value='1'>Visible</option>" +
                                "<option selected  value='0'>Not Visible</option></select>";
                        }
                        if(buttonData[a].btn_type == "shirt"){
                            tbodyShirtData = tbodyShirtData + "<tr><td>"+ shirtNum++ +"</td><td><img class='buttonImg' " +
                                "src='api/Files/images/buttons/"+buttonData[a].btn_image+"' /></td> <td>"+buttonData[a].btn_type+"</td>" +
                                "<td>"+buttonData[a].btn_name+"</td><td>"+buttonData[a].btn_size+"</td><td>$"+buttonData[a].btn_price
                                +"</td> <td>"+selectStatus+"</td><td><a  onclick=confirmDelete_Btn('"+buttonData[a].id+"');" +
                                " style='float: right; font-size: 18px;padding: 2px 4px; background: red none repeat scroll 0% 0%; " +
                                "color: white; margin-left: 5px;'><i class='fa fa-trash-o'></i>" +
                                "</a>" + "<a  onclick=getBtnPart('"+buttonData[a].id+"','"+encodeURI(buttonData[a].btn_image)+"'); style='float: right; font-size: 18px;" +
                                "padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white;'><i class='fa fa-pencil'></i>" +
                                "</a></td></tr>";
                        }
                        else if(buttonData[a].btn_type == "jacket"){
                            tbodyJacketData = tbodyJacketData + "<tr><td>"+jacketNum++ +"</td><td><img class='buttonImg' " +
                                "src='api/Files/images/buttons/"+buttonData[a].btn_image+"' /></td> <td>"+buttonData[a].btn_type+"</td>" +
                                "<td>"+buttonData[a].btn_name+"</td><td>"+buttonData[a].btn_size+"</td><td>$"+buttonData[a].btn_price
                                +"</td> <td>"+selectStatus+"</td><td><a  onclick=confirmDelete_Btn('"+buttonData[a].id+"');" +
                                " style='float: right; font-size: 18px;padding: 2px 4px; background: red none repeat scroll 0% 0%; " +
                                "color: white; margin-left: 5px;'><i class='fa fa-trash-o'></i>" +
                                "</a>" + "<a  onclick=getBtnPart('"+buttonData[a].id+"','"+encodeURI(buttonData[a].btn_image)+"'); style='float: right; font-size: 18px;" +
                                "padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white;'><i class='fa fa-pencil'></i>" +
                                "</a></td></tr>";
                        }

                    }
                $("#jacketData").html(table+"<tbody>"+tbodyJacketData+"</tbody></table>");
                $("#shirtData").html(table+"<tbody>"+tbodyShirtData+"</tbody></table>");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
            else {
                showMessage(data.Message, "red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request", "red");
        });
    }
    function updateBtnStatus(btnId,a){
        var btnValue = $("#updateBtnStatus_"+a).val();
        var url = "api/buttonProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "updateBtnStatus", "btnId": btnId, "btnValue": btnValue}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if (Status == "Success") {
                getButtonData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function confirmDelete_Btn(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete this Button ?</label>");
        $(".modal-footer").css("display", "block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
            'class="btn btn-primary" data-dismiss="modal"> Close</button>' + '<button type="button" class="btn btn-danger" ' +
            'onclick=delete_btn("' + id + '")>Delete</button>');
        $("#myModal").modal("show");
    }
    function delete_btn(id) {
        var url = 'api/buttonProcess.php';
        $.post(url, {"type": 'deletebtn', 'btnId': id}, function (data) {
            var status = data.Status;
            var message = data.Message;
            if (status === 'Success') {
                $("#myModal").modal("hide");
                location.reload();
            }
            else {
                $("#myModal").modal("hide");
                getButtonData();
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request", "red");
        });
        $(".modal-backdrop").hide();
    }
    function getBtnPart(btnId,oldImage){
        var url = "api/buttonProcess.php";
        $.post(url, {"type": "getPartBtn", "btnId": btnId}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var btnData = data.btnData;
            if (Status == "Success") {
                $(".modal-title").html("<label style='color: green'>Update Button</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='message'" +
                    " style='color:red'></p>" +
                    "<div class='col-md-6 form-group'><label>Select Button For</label>" +
                    "<select class='form-control' id='button_for'><option value='jacket'>Jacket</option>" +
                    "<option value='shirt'>Shirt</option></select></div>" +
                    "<div class='col-md-6 form-group'><label>Button Name</label><input type='text' name='button_name' " +
                    "id='button_name' class='form-control' placeholder='Button Name' value='"+btnData.btn_name+"' /></div>" +
                    "<div class='col-md-6 form-group'><label>Button Size</label><input type='text' name='button_size' " +
                    "id='button_size' class='form-control' placeholder='Button Size' value='"+btnData.btn_size+"' /></div>" +
                    "<div class='col-md-6 form-group'><label>Button Price</label><input type='text' name='button_price' " +
                    "id='button_price' class='form-control' placeholder='Button Price' value='"+btnData.btn_price+"' /></div>" +
                    "<div class='col-md-6 form-group'><label>Button Image</label><br><img src='api/Files/images/buttons/"+btnData.btn_image+"' class='oldbtnimg' />" +
                    "<input type='file' name='buttonFile' id='new_btn_img' class='form-control old_btn_image'  " +
                    "onchange=readURL(this,'oldbtnimg');  /></div><div class='col-md-12'>" +
                    "<input type='button' class='btn btn-danger pull-right' style='margin-top:30px' onclick=updateBtn('"+btnData.id+"','"+oldImage+"','"+btnData.btn_type+"');" +
                    " id='subbtn' value='Update'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#myModal").modal("show");
                $('#button_for option[value='+btnData.btn_type+']').attr('selected','selected');
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function readURL(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                if(targetClass == "oldbtnimg"){
                    oldBtnImagechanged = "yes";
                }else{
                    oldBtnImagechanged = "yes";
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function updateBtn(btnId,oldImage,btnType) {
        oldImage = decodeURI(oldImage);
        var button_for = $("#button_for").val();
        var button_name = $("#button_name").val();
        var button_size = $("#button_size").val();
        var button_price = $("#button_price").val();
        var new_btn_img = $("#new_btn_img").val();
        var data = new FormData();
        if (new_btn_img !== "") {
            var file_new_btn_img = document.getElementById('new_btn_img');
            var new_btn_image_ext = new_btn_img.split(".");
            new_btn_image_ext = new_btn_image_ext[new_btn_image_ext.length - 1];
            new_btn_image_ext = new_btn_image_ext.toLowerCase();
            if (new_btn_image_ext == "jpg" || new_btn_image_ext == "png" || new_btn_image_ext == "gif" || new_btn_image_ext == "jpeg") {
                data.append('new_btn_img', file_new_btn_img.files[0]);
                oldBtnImagechanged = "yes";
            }
            else {
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color", "red");
                $("#subbtn").attr("disabled", false);
                return false;
            }
        }
        if(button_name.indexOf(" ") > 0 || button_name.indexOf("_") > 0)
        {
            $("#message").html("Invalid Button name,  space or underscore( _ ) not allowed");
            return false;
        }

        if (button_for !== "" || button_name !== "" || button_size != "" || button_price != "") {
            $("#message").html("");
            $("#subbtn").attr("disabled", true);
            $(".loaderdiv").removeClass("loaderhidden");
            data.append('type', 'editButton');
            data.append('btnId', btnId);
            data.append('button_for', button_for);
            data.append('button_name', button_name);
            data.append('button_size', button_size);
            data.append('button_price', button_price);
            data.append('oldBtnImage', oldImage);
            data.append('oldBtnImagechanged', oldBtnImagechanged);
            var request = new XMLHttpRequest();
            request.onreadystatechange = function () {
                if (request.readyState == 4) {
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if (status == "Success") {
                        location.reload();
                        $("#myModal").modal("hide");
                    }
                    else {
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                        $("#message").css("color", "red");
                        $("#subbtn").attr("disabled", false);
                        return false;
                    }
                }
            };
            request.open('POST', 'api/buttonProcess.php');
            request.send(data);
        }
        else {
            $("#message").html("please fill required fields");
            $("#message").css("color", "red");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    getButtonData();
</script>
