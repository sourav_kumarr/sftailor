<?php
include('header.php');
?>
<style>
    .tick_image{
        width: 80px;
    }
</style>
<div class="jumbotron text-xs-center" style="text-align: center;margin-top: 74px;">
    <?php
        $status = $_REQUEST['status'];
        if($status == "Failed") {
            ?>
            <img src="images/crosss.png" class="tick_image">
            <h1 class="display-3">Sorry!</h1>
            <p class="lead"><strong>
                    Your Transaction is Failed. </strong> Please try again later.</p>
            <hr>
            <?php
        }
        else
            {
                ?>

                <img src="images/tick.png" class="tick_image">
                <h1 class="display-3">Thanking You !</h1>
                <p class="lead"><strong>
                        Your Transaction is Successfull. </strong> </p>
                <hr>
                <?php
        }?>

    <p class="lead">
        <a class="btn btn-primary btn-sm" href="index.php" role="button">Continue to homepage</a>
    </p>
</div>
<?php
include('footer.php');
?>