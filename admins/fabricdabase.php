<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if(isset($_POST['filterButton'])){
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query="select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
}else{
    $query="select * from orders order by order_id DESC";
}
?>
<style>
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
        background: #eee;
    }
    #orderTable_filter{
        display:none;
    }
    .search-container{
        display: flex;
        flex-direction: row;
    }
    .search-remove{
        display: none;
    }
    .no-gutter{
        margin: 0px;
        padding: 0px;
    }
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">

    </div>
        <div class="">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content" id="view-price" style="display: block;>
                            <div class="tab-content" style="background-color: rgb(238, 238, 238); padding: 28px 38px; border-radius: 10px; margin-top: -70px;">
                                <div class="tab-pane active" id="tab1">
                                    <label style="color: red" id="fabric_error">Fabric Price Included</label>
                                    <table id="priceTable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Price Amount in $ (max value is included)</th>
                                            <th>Needed fabric per item in meters</th>
                                            <th> Uplift coefficient</th>
                                            <th>Product Type</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $link = $conn->connect();//for scan2tailor
                                        //$link2 = $conn->connect2();//for tailormade

                                        if ($link) {
                                            $query = "select * from wp_prices order by price_id DESC";
                                            $result = mysqli_query($link, $query);
                                            if ($result) {

                                                $num = mysqli_num_rows($result);
                                                if ($num > 0) {
                                                    $j = 0;
                                                    while ($priceData = mysqli_fetch_array($result)) {
                                                        $j++;
                                                        ?>
                                                        <tr>
                                                            <td data-title='#'><?php echo $j ?></td>
                                                            <td><input type="text" id="price_amount_<?php echo $priceData['price_id'];?>" value="<?php echo $priceData['price_amount']; ?>" style="border:none;"></td>
                                                            <td><input type="text" id="price_amount_meter_<?php echo $priceData['price_id'];?>" value="<?php echo $priceData['price_per_meter']; ?>" style="border:none;"></td>
                                                            <td><input type="text" id="uplifts_buttons_<?php echo $priceData['price_id'];?>" value="<?php echo $priceData['uplifts_button']; ?>" style="border:none;"></td>
                                                            <td><?php echo $priceData['price_type']; ?></td>
                                                            <td><?php echo $priceData['price_created_at']; ?></td>
                                                            <td><a href='#' onclick=editPrice('<?php echo $priceData['price_id'];?>') style="font-size: 18px; padding: 2px 4px; background: green none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-floppy-o"></i> </a> </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="tab-pane active" id="tab2">
                                     <label style="color: red" id="fabric_error">Button Price Included</label>
                                     <table id="buttonTable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Max Price in $ of button included</th>
                                            <th>Need number of buttons</th>
                                            <th>uplift</th>
                                            <th>Product Type</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $link = $conn->connect();//for scan2tailor
                                        //$link2 = $conn->connect2();//for tailormade

                                        if ($link) {
                                            $query = "select * from button_price_process order by id DESC";
                                            $result = mysqli_query($link, $query);
                                            if ($result) {

                                                $num = mysqli_num_rows($result);
                                                if ($num > 0) {
                                                    $j = 0;
                                                    while ($buttonData = mysqli_fetch_array($result)) {
                                                        $j++;
                                                        ?>
                                                        <tr>
                                                            <td data-title='#'><?php echo $j ?></td>
                                                            <td>
                                                                <input type="text" id="price_amount_Btn<?php echo $buttonData['id'];?>"
                                                                       value="<?php echo $buttonData['price_amount']; ?>" style="border:none;"></td>
                                                            <td>
                                                                <input type="text" id="no_of_button<?php echo $buttonData['id'];?>"
                                                                       value="<?php echo $buttonData['no_of_buttons']; ?>" style="border:none;"></td>
                                                            <td><input type="text" id="uplifts_buttons_Btn<?php echo $buttonData['id'];?>"
                                                                       value="<?php echo $buttonData['uplifts']; ?>" style="border:none;"></td>
                                                            <td>
                                                                <?php echo $buttonData['price_type']; ?>
                                                            </td>

                                                            <td><?php echo $buttonData['add_on']; ?></td>
                                                            <td>
                                                                <a href='#' onclick=editBtnPrice('<?php echo $buttonData['id'];?>')
                                                                   style="font-size: 18px; padding: 2px 4px; background: green none repeat scroll 0% 0%;
                                                                   color: white; margin-right: 10px;"><i class="fa fa-floppy-o"></i> </a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>

    $(document).ready(function () {
        $('#priceTable').DataTable({});
        $('#buttonTable').DataTable({});
    });

    function editPrice(price_id){
        var url = "api/fabricProcess.php";
        var amount = $("#price_amount_"+price_id).val();
        var per_meter  = $("#price_amount_meter_"+price_id).val();
        var uplifts_buttons  = $("#uplifts_buttons_"+price_id).val();

        $.post(url,{"dataType":"editPrice","price_id":price_id,"price_amount":amount,"price_amount_meter":per_meter,
        "uplifts_buttons":uplifts_buttons}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if (Status == "Success"){
                showMessage(Message,"green");
                setTimeout(
                    function(){
                        /*window.location="fabric.php";*/
                        location.reload();
                    },3000);
            }
            else{
                showMessage(Message,"red");
            }
        });
    }

    function editBtnPrice(price_id){
        var url = "api/fabricProcess.php";
        var amount = $("#price_amount_Btn"+price_id).val();
        var no_of_button  = $("#no_of_button"+price_id).val();
        var uplifts_buttons_Btn  = $("#uplifts_buttons_Btn"+price_id).val();
        $.post(url,{"dataType":"editBtnPrice","price_id":price_id,"price_amount":amount,"uplifts_buttons_Btn":uplifts_buttons_Btn,
            "no_of_button":no_of_button}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            if (Status == "Success"){
                showMessage(Message,"green");
                setTimeout(
                    function(){
                        /*window.location="fabric.php";*/
                        location.reload();
                    },3000);
            }
            else{
                showMessage(Message,"red");
            }
        });
    }
</script>
