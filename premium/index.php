<?php
error_reporting(0);
include('header.php');
/*require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
$allUsersData = $userClass->getAllUsersData();*/
?>
<style>.shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .hideloader {
        display: none;
    }
    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
    #create_stores{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
    @media print {
        body * {
            visibility: hidden;
        }
        #section-to-print, #section-to-print * {
            visibility: visible;
        }
        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
    .userMgmtBoxes{
        display: none;
    }
    #signup_form {
        top: -65px;
    }
</style>

<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count"></div>
    <div class="row" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title" style="border-bottom: 1px solid #e6e9ed;">
                            <h2><span style="color:#1ABB9C;"></span>Customer
                                <small></small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <form method="post" class="form-inline">
                                        <div class="form-group form-inline">
                                            <label id="message" style="color: red"></label>
                                            <input type="button" onclick="switchBox('customer_create')"
                                                   Value="Customer Create" class="btn btn-danger btn-sm userDataBox"  style="margin-bottom: -1px"/>
                                            <input type="button" onclick="switchBox('viewUserData')"
                                                   Value="View User Data" class="btn btn-danger btn-sm createUserBox "  style="margin-bottom: -1px;
                                                display: none;"/>
                                        </div>


                                    </form>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 catagrories-suit userDataBox">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th>consumer Image</th>
                                        <th>consumer Name</th>
                                        <th>consumer Email</th>
                                        <th>consumer Contact</th>
                                        <th>consumer Gender</th>
                                        <th>consumer City</th>
                                        <th>Stylist</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody id="userData"></tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="createUserBox" style="display: none">
        <div class="x_content">
            <p class="text-muted font-13 m-b-30">
                Create User
            </p>
            <div class="col-md-12" style="box-shadow:0 0 10px #333;border-radius:5px;background:rgba(255,255,255,0.7)" id="signup_form">
                <form method="Post" enctype="multipart/form-data" id="registerform">
                    <div class="row cardview" style="">
                        <p id="errorhere" style="color:red;font-size:16px;padding:10px"></p>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>First Name</label>
                                <input placeholder="First Name" name="fname" class="form-control" type="text" />
                            </div>
                            <div class="form-group">
                                <label>Last Name</label>
                                <input placeholder="Last Name" name="lname" class="form-control" type="text">
                            </div>
                            <div class="form-group">
                                <label>Select Country</label>
                                <select name="b_country" class="countries form-control" id="countryId" onchange='datedate()'>
                                    <option value="">Select Country</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select State</label>
                                <select name="b_state" class="states form-control" id="stateId">
                                    <option value="">Select State</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Select City</label>
                                <div class="col-md-12" id="cityfeild" style="padding:0;">
                                    <select name="b_city" class="cities form-control" id="cityId">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label style="margin-top:15px">D.O.B</label>
                                <input id="inputdob" name="dob" placeholder="Enter D.O.B" class="form-control" type="text">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group" >
                                <label>Profile Picture</label>
                                <input name="file0" class="form-control" type="file">
                            </div>
                            <div class="form-group">
                                <label>Enter Height</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="heightparam" class="form-control" id="height_parameter" onchange='height_param()'>
                                            <option value="Inches">Inches</option>
                                            <option value="Centimeters">Centimeters</option>
                                            <option value="Feets">Feets</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6" id='heightdiv'>
                                        <input name='height' class='form-control' placeholder="Enter Height" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>E-Mail</label>
                                <input required placeholder="Email" name="email" class="form-control" type="email">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input required placeholder="Password" id="password" name="password" class="form-control" type="password">
                            </div>
                            <div class="form-group">
                                <label>Confirm Password</label>
                                <input required placeholder="Repeat Password" id="cpassword" class="form-control" type="password">
                            </div>
                            <div class="form-group">
                                <label>Stylist</label>
                                <input type="hidden" name="stylist" id="stylist" value="<?php echo $_SESSION['SFT_Store_Name'];?>">
                               <input type="text" class='form-control' value="<?php echo $_SESSION['SFT_Store_Name'];?>" disabled>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Select Gender</label>
                                <select class="form-control" name="gender">
                                    <option selected="" value="">Select Gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Language</label>
                                <select class="form-control" name="lang">
                                    <option selected="" value="">Select Language</option>
                                    <option value="English(US)">English(US)</option>
                                    <option value="English(UK)">English(UK)</option>
                                    <option value="French">French</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Weight</label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <select name="weightparam" class="form-control">
                                            <option selected="" value="">Parameter</option>
                                            <option value="Kilograms">Kilograms</option>
                                            <option value="Pounds">Pounds</option>
                                            <option value="Stones">Stones</option>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <input type='text' name='weight' class='form-control' placeholder='Enter Weight' />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>ZipCode</label>
                                <input type="text" placeholder="Enter ZipCode" name="zipcode" class="form-control" />
                            </div>
                            <div class="form-group">
                                <label>Address / Street No.</label>
                                <textarea placeholder="Enter Address" name="address" class="form-control"></textarea>
                            </div>
                            <div class="form-group" style="margin:0px">
                                <input value="Register" class="btn btn-danger pull-right" style="width:200px" type="submit">
                            </div>
                            <div class="row" style="margin-top:10px">
                                <input value="sftailor" name="site_name" type="hidden">
                                <input value="Website" name="app_type" type="hidden">
                            </div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
<?php
    include('footer.php');
?>
<script>
    function switchBox(boxValue){
        if(boxValue === "customer_create"){
            $(".userDataBox").hide();
            $(".createUserBox").show();
        }
        else{
            $(".userDataBox").show();
            $(".createUserBox").hide();
        }
    }
    function loadUsersData(){
        var storeName = $("#store_name").val();
        if(storeName === ""){
            showError("Session Timed Out !!! Please Login Again","red");
            return false;
        }
        var url = "api/userProcess.php";
        $.post(url,{"type":"getAllUsersData","store_name":storeName},function(data){
            var Status = data.Status;
            var dataShow = "";
            var ids = [];
            if(Status === "Success"){
                var userData = data.UserData;
                for(var i=0;i<userData.length;i++){
                    if(ids.indexOf(userData[i].user_id)>-1){
                        //alert("data already exist");
                    }else{
                        ids.push(userData[i].user_id);
                        dataShow=dataShow+"<tr><td><img alt='Image' src='"+userData[i].profile_pic+"' " +
                        "style='height:40px !important;width:40px;border:2px solid #333'/></td><td>"+userData[i].name+"</td>" +
                        "<td>"+userData[i].email+"</td><td>"+userData[i].mobile+"</td><td>"+userData[i].gender+"</td>" +
                        "<td>"+userData[i].b_city+"</td><td>"+storeName+"</td><td><i class='fa fa-edit' style='display:none;color:#006400;cursor:pointer' " +
                        "onclick=editUser('"+userData[i].user_id+"') ></i>&nbsp;&nbsp;<i class='fa fa-trash' " +
                        "style='color:red;cursor:pointer' onclick=deleteStylistUsers('"+userData[i].user_id+"') ></i></td></tr>";
                    }
                }
            }else{
                dataShow = "<tr><td colspan=8 style='text-align:center'>"+data.Message+"</td></tr>";
            }
            $("#userData").html(dataShow);
        }).fail(function(){
           showError("Server Error !!! Please Try After Some Time...","red");
        });
    }
    function deleteStylistUsers(user_id){
        $(".modal-content").css({"display":"block"});
        $(".modal-dialog").css({"display":"block"});
        $(".modal-header").css({"display":"block"});
        $(".modal-title").html("Delete Permission");
        $(".modal-body").css({"display":"block"});
        $(".modal-body").html("<span style='color:red'>Are You Sure you want to Delete this User</span>");
        $(".modal-footer").css({"display":"block"});
        $(".modal-footer").html("<input type='button' value='Cancel' data-dismiss='modal' class='btn btn-sm btn-default' />" +
        "<input type='button' value='Delete' data-dismiss='modal' onclick=confirmUserDelete('"+user_id+"') class='btn btn-sm btn-danger' />");
        $("#myModal").modal("show");
    }
    function confirmUserDelete(user_id){
        var url = "api/userProcess.php";
        $.post(url,{"type":"deleteUser","user_id":user_id} ,function (data) {
            var status = data.Status;
            setTimeout(function(){
                if (status === "Success"){
                    showError(data.Message,"green");
                    location.reload(true);
                }
                else{
                    showError(data.Message,"red");
                }
            },500);
        }).fail(function(){
            showError("Server Error!!! Please Try After Some Time","red")
        });
    }
    function delCoupon(cat_id) {
        var url = "api/categoryProcess.php";
        $.post(url, {"type": "deleteCategory", "cat_id": cat_id}, function (data) {
            var Status = data.Status;
            if (Status === "Success") {
                window.location = "index.php?type=Coupons";
            }
        });
    }
    loadUsersData();


    function delUser(user_id){
        var url = "api/userProcess.php";
        $.post(url,{"type":"deleteUser","user_id":user_id}, function (data) {
            var Status = data.Status;
            if (Status === "Success"){
                window.location="users.php";
            }
        });
    }
    $("#update_user").submit(function(e){
        e.preventDefault();
        $('.loader').fadeIn("slow");
        $.ajax({
            url: "update_users.php?type=update",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                $('.loader').fadeOut("slow");
                var json = $.parseJSON(data)
                {
                    var status = json.status;
                    var message = json.message;
                    if(status === "done"){
                        alert(message);
                        window.location="users.php";
                    }
                    else{
                        alert(message);
                    }
                }
            }
        });
    });

    function close_diolog() {
        window.location = "users.php"
    }
    /*user create starrt here*/
    $("#registerform").submit(function(e) {
        e.preventDefault();
        $('.loader').fadeIn("slow");
        $.ajax({
            url: "../admin/webserver/premiumSignup.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data){
                $('.loader').fadeOut("slow");
                var json = $.parseJSON(data);
                var status = json.status;
                var message = json.message;
                if(status == "Registered Successfully"){
                    var user_id = json.user_id;
                    showMessage(status,"green");
                    setTimeout(function(){
                        window.location='customer_step_two.php?userId='+user_id;
                    },3000);
                }else{
                    $('.loaderr').addClass("hideloader");
                    $(".modal-title").html("<span>Error Occured</span>");
                    $(".modal-body").html("<span style='color:red'>"+status+"</span>");
                    $("#myModal").modal("show");
                }
            }
        });
    });

    function height_param(){
        var height_param_val = $("#height_parameter").val();
        if(height_param_val === "Feets"){
            $("#heightdiv").html("<div class='col-md-6' style='padding:0'><input name='height0' class='form-control' placeholder='Enter Feets' /></div><div class='col-md-6' style='padding:0'><input name='height1' class='form-control' placeholder='Enter Inches' /></div>");
        }
        else if(height_param_val === "Centimeters"){
            $("#heightdiv").html("<input name='height' class='form-control' placeholder='Enter Height' />");
        }
        else{
            $("#heightdiv").html("<input name='height' class='form-control' placeholder='Enter Height' />");
        }
    }
    /* $("#inputdob").datetimepicker({
         format: "MM/DD/YYYY",
         minDate: today
     });*/
    function datedate(){
        var country = $("#countryId").val();
        if(country === "United States"){
            $('#inputdob').datetimepicker({
                format:"MM/DD/YYYY"
            });
        }else{
            $('#inputdob').datetimepicker({
                format:"DD/MM/YYYY"
            });
        }
    }
    /*user create end  here*/

</script>