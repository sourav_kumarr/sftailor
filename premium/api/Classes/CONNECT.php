<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 12:45 PM
 */

namespace Classes;
class CONNECT
{
    public $link = null;
    public $link2 = null;
    public $response = null;
    public function connect()
    {
        $this->link = mysqli_connect(DBHOST,DBUSER,DBPASSWORD,DBNAME);
        return $this->link;
    }
    public function closeConnection()
    {
        mysqli_close($this->link);
    }
    public function getLastId()
    {
        return mysqli_insert_id($this->link);
    }
    public function sqlError()
    {
        return mysqli_error($this->link);
    }


    public function storeImageR($FileParam,$FolderName)
    {

            $file_tmp =$_FILES[$FileParam]['tmp_name'];
//
            if($FolderName === "pdf") {
                $file_path = PdfPath;
            }
            else if($FolderName === "videos") {
                $file_path = VideoPath;
            }
            else if($FolderName === "images"){
                $file_path = ImagePath;
            }

            $target = getcwd() . "/Files/" . $FolderName . "/";
            $file_name = $_FILES[$FileParam]['name'];

            if (move_uploaded_file($file_tmp, $target . $file_name)) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "File Uploaded";
                $this->response['File_Name'] = $file_name;
                $this->response['File_Path'] = $file_path.$file_name;

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Might be Path Error of $FileParam";
                $this->response['File_Name'] = "";
            }


        return $this->response;
    }




    public function storeImage($FileParam,$FolderName)
    {
        if(isset($_FILES[$FileParam]))
        {
            $file_tmp =$_FILES[$FileParam]['tmp_name'];
            $file_path = '';
            $file_ext=strtolower(end(explode('.',$_FILES[$FileParam]['name'])));
//            echo $file_ext;
            if($FolderName == "audio"){
                $expensions= array("mp3","wav","amr");
            }
            else if($FolderName == "excel") {
                $expensions= array("xls");
                $file_path = ExcelPath;
            }
            else if($FolderName === "pdf") {
                $expensions= array("pdf");
                $file_path = PdfPath;
            }
            else if($FolderName === "videos") {
                $expensions= array("mp4","3gp","avi","mov");
                $file_path = VideoPath;
            }
            else{
                $expensions= array("jpeg","jpg","png");
                $file_path = ImagePath;
            }
            if(in_array($file_ext,$expensions)=== false){
                $this->response[STATUS] = Error;
                if($FolderName == "audio"){
                    $this->response[MESSAGE] = "Invalid File Extension Please Choose a WAV, AMR or MP3 file.";
                }
                else if($FolderName == "excel") {
                    $this->response[MESSAGE] = "Invalid File Extension Please Choose a .XLS";

                }
                else if($FolderName == "pdf") {
                    $this->response[MESSAGE] = "Invalid File Extension Please Choose a .PDF";
                }
                else if($FolderName === "videos") {
                    $this->response[MESSAGE] = "Invalid File Extension Please Choose a ".implode(',',$expensions);
                }
                else{
                    $this->response[MESSAGE] = "Invalid File Extension Please Choose a JPEG,PNG or GIF file.";
                }
                $this->response['File_Name'] = "";
            }
            else {
                $target = getcwd() . "/Files/" . $FolderName . "/";
                $file_name = $_FILES[$FileParam]['name'];
                /*if(!$FolderName === 'pdf') {
                    $file_name = "File" . rand(000000, 999999) . "." . $file_ext;
                }*/

                if (move_uploaded_file($file_tmp, $target . $file_name)) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "File Uploaded";
                    $this->response['File_Name'] = $file_name;
                    $this->response['File_Path'] = $file_path.$file_name;

                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Might be Path Error of $FileParam";
                    $this->response['File_Name'] = "";
                }
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $FileParam." File Is Missing";
            $this->response['File_Name'] = "";
        }
        return $this->response;
    }
    public function isValidToken($user_id,$token)
    {
        $this->link = $this->connect();
        $link = $this->link;
        if($link){
            $query = "select * from users where user_id = '$user_id' and user_token = '$token'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $this->response[STATUS] = STATUS;
                    $this->response[MESSAGE] = "Valid Token";
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Session Timed Out !!! Please Login Again";
                }
            }
            else{
                $this->response[STATUS]  = Error;
                $this->response[MESSAGE]  = $this->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->sqlError();
        }
        return $this->response;
    }
    function currentMonthDays(){
        $d=cal_days_in_month(CAL_GREGORIAN,date("m"),date('Y'));
        return $d;
    }
    function YearDays(){
        return  date("z", mktime(0,0,0,12,31,date("Y"))) + 1;
    }
    function __destruct()
    {
        $this->link = null;
        $this->response = null;
    }
}