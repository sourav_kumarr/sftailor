<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 05-12-2017
 * Time: 20:27
 */

include("header.php");
require_once ('api/Classes/USERCLASS.php');
require_once('api/Classes/MESSAGE.php');
require_once ('api/Constants/functions.php');
require_once('api/Constants/configuration.php');
require_once('api/Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();
$userResponse = $userClass->getAllUsersData();
$status = $userResponse[Status];
$users = $userResponse["usersData"];

$msgClass = new \Classes\MESSAGE();
$id = base64_decode($_REQUEST['id']);
$msgClass->setMsgId($id);
$response = $msgClass->getParticularMessage();

$msg_text = $response['data']["msg_text"];
$msg_desc = $response['data']["msg_desc"];
$msg_id = $response['data']["msg_id"];
$msg_signature = $response['data']["msg_signature"];
echo "<input type='hidden' id='msg_txt' value='".$msg_text."'/>";
echo "<input type='hidden' id='msg_desc' value='".$msg_desc."'/>";
echo "<input type='hidden' id='msg_id' value='".$msg_id."'/>";

?>
<link rel="stylesheet" href="css/create_message.css"/>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="msg-body msg-body-1 active">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title" style="padding-bottom:2px; ">
                        <label style="font-size: 22px;letter-spacing: 1px;cursor: pointer" onclick="window.location='draft_message.php'"><i class="fa fa-arrow-circle-left"></i> Broadcast Message</label>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <label>When this message should be sent ?</label>
                            <button class="btn btn-primary btn-sm pull-right" onclick="send_broadcast();" style="margin-top: -3px;"> <i class="fa fa-clock-o"></i>&nbsp;&nbsp;Schedule</button>

                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter date-container">
                                <label>Select Date</label>
                                <input type="text" id="start_date" placeholder= "Select schedule date" class="form-control" style="width:33%;"/>
                            </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 no-gutter ">
                        <div class="col-md-8 col-sm-8 col-xs-8 no-gutter">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter msgs-header">
                                <!--                                <label style="margin-top: 7px;">All Subscribers</label>-->
                                <div class="checkbox-inline" style="margin-top: 7px;">
                                    <input type="checkbox" value="select_all"
                                           id="CheckboxSelectAll" onclick="selected_checkbox(this);"/>
                                    <label for="CheckboxSelectAll">Select All</label>
                                </div>

                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter user-container">
                                <!--<div class="col-md-12 col-sm-12 col-xs-12" style="padding: 1%">
                                    <div class="checkbox-inline">
                                        <input type="checkbox" value="select_all"
                                               id="CheckboxSelectAll" onclick="selected_checkbox(this);"/>
                                        <label for="CheckboxSelectAll" style="font-weight: normal">Select All</label>
                                    </div>

                                </div>-->
                                <?php
                                foreach($users as $user) {
                                    ?>
                                    <div class="col-md-6 col-xs-6" style="padding: 1%;border: 1px solid #fff">
                                        <div class="checkbox-inline">
                                            <input type="checkbox" value="<?php echo $user['email']; ?>"
                                                   id="Checkbox_<?php echo $user['user_id']; ?>" class="styled" name="checked_item"/>
                                            <label for="Checkbox_<?php echo $user['user_id']; ?>" style="font-weight: normal"><?php echo $user['email']; ?></label>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>

                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 no-gutter">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter msgs-header">
                                <label style="margin-top: 7px;">Preview : HTML</label>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter html-data" style="height: 355px;">
                                <?php echo $msg_desc.$msg_signature;?>
                            </div>

                        </div>

                    </div>
                        </div>
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<script>
    var today = new Date();
    var offset = today.getTimezoneOffset();
    var kuwait_time = -330;
    var diff = parseInt(offset)+parseInt(kuwait_time);
    today.setTime( today.getTime() + diff*60*1000 );  // set timezone in javascript

    function selected_checkbox(ref) {
        var isChecked = $("#"+ref.id).is(":checked");
        $(".styled").prop("checked",isChecked);
    }

    function send_broadcast() {
        var selected_items = [];
        var updated_at =  $("#start_date").val();
        var msg_id = $("#msg_id").val();

        $("input:checkbox[name=checked_item]:checked").each(function(){
            selected_items.push($(this).val());
        });

        if(selected_items.length === 0 || updated_at === '' ) {
            $(".modal-title").html("<label style='color: #202020;' >Error</label>");
            $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>Please select all fields</label>");
            $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>');
            $("#myModal").modal("show");
            return false;
        }
        var curr_date = moment().format('YYYY-MM-DD hh:mm:ss');
        console.log(curr_date);
        var url = "api/broadcastProcess.php";
        $.post(url,{type:'createBroadCast',broad_users:selected_items.join(','),broad_created_at:curr_date,
            broad_updated:updated_at,broad_msg_id:msg_id},function(data){
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $(".modal-title").html("<label style='color: #202020;' >Success</label>");
                $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>"+message+"</label>");
                $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>');
                $("#myModal").modal("show");
                setTimeout(function () {
                    window.location = "broadcasts.php";
                },500);

            }
            else{
                $(".modal-title").html("<label style='color: #202020;' >Error</label>");
                $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>"+message+"</label>");
                $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>');
                $("#myModal").modal("show");

            }
        });
        console.log('ids --- '+selected_items);
    }

    $('#start_date').datetimepicker({
        dayOfWeekStart : 1,
        lang:'en',
        value: "",
        step:30,
        format:"Y-m-d h:i:s A",
        startDate:	new Date()
    });
</script>

