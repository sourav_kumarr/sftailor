<?php
require_once 'errorCodes.php';
require_once 'functions.php';
require_once 'database/db.php';
$status="error";
$items=array();
$message="unable to get data";
$mail_status= "";
$type = $_REQUEST['type'];
if($type=="storeinsert"){
    $requiredfields = array('user_name', 'user_email','store_phone','store_password');
    if(!RequiredFields($_REQUEST, $requiredfields)){
        return false;
    }
    $user_name = $_REQUEST['user_name'];
    $last_name = $_REQUEST['last_name'];
	$store_name = $user_name.$last_name;
    $user_email = $_REQUEST['user_email'];
    $store_phone = $_REQUEST['store_phone'];
    $store_status = $_REQUEST['store_status'];
    $store_password = $_REQUEST['store_password'];
    $company_name = $_REQUEST['company_name'];
    $store_contact_address = $_REQUEST['store_contact_address'];
    $store_about = $_REQUEST['store_about'];
    $zip_code = $_REQUEST['zip_code'];
    $b_country = $_REQUEST['b_country'];
    $b_state = $_REQUEST['b_state'];
    $b_city = $_REQUEST['b_city'];
    $dob = $_REQUEST['dob'];
    $referralCode = $_REQUEST['referal'];
    $mother = $_REQUEST['mother'];
    $founding = $_REQUEST['founding'];
	if($mother ==""){
		$mother ="";
	}
	else{
		$mother = $_REQUEST['mother'];
	}
	if($founding ==""){
		$mother ="";
	}
	else{
        $founding = $_REQUEST['founding'];
	}

    function get_store_date()
    {
        $levelsData = "select * from wp_commission_personal where level_type='month_comm'";
        $rep = mysql_query($levelsData);
        $num = mysql_num_rows($rep);
        if ($num > 0) {
            $rows = mysql_fetch_assoc($rep);
            $bonus_expire_date = $rows['price_maximum'];
            $next_date = strtotime(date('Y-m-d', strtotime('+'.$bonus_expire_date."month")));
        }
        return $next_date;
    }

    $alr = "select * from wp_store where store_name='$store_name'";
    $rcm = mysql_query($alr);
    $num = mysql_num_rows($rcm);
    if($num>0){
        $status=  "done";
        $message = "Store name already Exist Please Choose Another";
    }
    else {
        if($referralCode != "") {
            $alr2 = "select * from wp_store where store_status='1' and store_refferal_code='$referralCode'";
            $rcm2 = mysql_query($alr2);
            $num = mysql_num_rows($rcm2);
            if ($num > 0) {

                $filename = '../../' . $store_name;
                if (!file_exists($filename)) {
                     $makeDir = mkdir('../../' . $store_name);
                } else {
                    $message = $store_name." already exists please select another name";
                }
                if ($makeDir) {
                    $indexcopy = copy('../../demo/index.php', '../../' . $store_name . '/index.php');
                    if ($indexcopy) {
                        file_put_contents('../../' . $store_name . '/index.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                    }
                    $headcopy = copy('../../demo/header.php', '../../' . $store_name . '/header.php');
                    if ($headcopy) {
                        file_put_contents('../../' . $store_name . '/header.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                    }
                    $copycontact = copy('../../demo/contact.php', '../../' . $store_name . '/contact.php');
                    if ($copycontact) {
                        file_put_contents('../../' . $store_name . '/contact.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                    }
                    $copycart = copy('../../demo/cart.php', '../../' . $store_name . '/cart.php');
                    if ($copycart) {
                        file_put_contents('../../' . $store_name . '/cart.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                    }
                    $copycart = copy('../../demo/orders.php', '../../' . $store_name . '/orders.php');
                    if ($copycart) {
                        file_put_contents('../../' . $store_name . '/orders.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                    }
                    copy('../../demo/about.php', '../../' . $store_name . '/about.php');
                    copy('../../demo/canvas.php', '../../' . $store_name . '/canvas.php');
                    copy('../../demo/accessories.php', '../../' . $store_name . '/accessories.php');
                    copy('../../demo/androidObject.php', '../../' . $store_name . '/androidObject.php');
                    copy('../../demo/colorSwitcher.php', '../../' . $store_name . '/colorSwitcher.php');
                    copy('../../demo/custom_configure.php', '../../' . $store_name . '/custom_configure.php');
                    copy('../../demo/custom_configure_overcoat.php', '../../' . $store_name . '/custom_configure_overcoat.php');
                    copy('../../demo/custom_overcoat.php', '../../' . $store_name . '/custom_overcoat.php');
                    copy('../../demo/custom_pants.php', '../../' . $store_name . '/custom_pants.php');
                    copy('../../demo/custom_shirts.php', '../../' . $store_name . '/custom_shirts.php');
                    copy('../../demo/custom_suit.php', '../../' . $store_name . '/custom_suit.php');
                    copy('../../demo/customfit.php', '../../' . $store_name . '/customfit.php');
                    copy('../../demo/customize.php', '../../' . $store_name . '/customize.php');
                    copy('../../demo/fabric.php', '../../' . $store_name . '/fabric.php');
                    copy('../../demo/fullbespoke.php', '../../' . $store_name . '/fullbespoke.php');
                    copy('../../demo/highend.php', '../../' . $store_name . '/highend.php');
                    copy('../../demo/highenddetails.php', '../../' . $store_name . '/highenddetails.php');
                    copy('../../demo/login.php', '../../' . $store_name . '/login.php');
                    copy('../../demo/logout.php', '../../' . $store_name . '/logout.php');
                    copy('../../demo/myaccount.php', '../../' . $store_name . '/myaccount.php');
                    copy('../../demo/objectLoader.php', '../../' . $store_name . '/objectLoader.php');
                    copy('../../demo/pricelist.php', '../../' . $store_name . '/pricelist.php');
                    copy('../../demo/suit_old.php', '../../' . $store_name . '/suit_old.php');
                    copy('../../demo/topbrands.php', '../../' . $store_name . '/topbrands.php');
                    copy('../../demo/workprocess.php', '../../' . $store_name . '/workprocess.php');
                    copy('../../demo/sizematching.php', '../../' . $store_name . '/sizematching.php');
                    copy('../../demo/copycustomsuit.php', '../../' . $store_name . '/copycustomsuit.php');
                    copy('../../demo/footer.php', '../../' . $store_name . '/footer.php');
                    copy('../../demo/sdet.php', '../../' . $store_name . '/sdet.php');
                    copy('../../demo/startconfigovercoat.php', '../../' . $store_name . '/startconfigovercoat.php');
                    copy('../../demo/startJacket.php', '../../' . $store_name . '/startJacket.php');
                    copy('../../demo/startjacketcollection.php', '../../' . $store_name . '/startjacketcollection.php');
                    copy('../../demo/startovercoat.php', '../../' . $store_name . '/startovercoat.php');
                    copy('../../demo/startpant.php', '../../' . $store_name . '/startpant.php');
                    copy('../../demo/startshirt.php', '../../' . $store_name . '/startshirt.php');
                    copy('../../demo/stylegenie.php', '../../' . $store_name . '/stylegenie.php');
                    copy('../../demo/stylish.php', '../../' . $store_name . '/stylish.php');
                    copy('../../demo/paynow.php', '../../' . $store_name . '/paynow.php');
                    copy('../../demo/jquery-2.2.2.js', '../../' . $store_name . '/jquery-2.2.2.js');

                    $target_path = "upload/storelogo/";
                    $file_ext = strtolower(end(explode('.', $_FILES['store_logo']['name'])));
                    $tmp_name = $_FILES['store_logo']['tmp_name'];
                    $file_name = "logo" . $store_name . "." . $file_ext;
                    $store_refferal_code = "STR" . rand(1000000, 9999999);
                    $target_path = $target_path . $file_name;
                    if (move_uploaded_file($tmp_name, $target_path)) {
                        $store_logo = "admin/webserver/" . $target_path;
                        $bonus_ex_date = get_store_date();
                        $insert_data = "insert into wp_store (user_name,last_name,company_name,user_email,store_phone,store_password,store_name,
                        store_logo,store_about,store_contact_address,zip_code,b_country,b_state,b_city,dob,store_status,
                        created_date,store_refferal_code,reffered_by,designation,mother,founding,start_month_bonus,bonus_expire_date) values('$user_name','$last_name','$company_name','$user_email','$store_phone',
                        '$store_password','$store_name','$store_logo','$store_about','$store_contact_address','$zip_code',
                        '$b_country','$b_state','$b_city','$dob','$store_status',NOW(),'$store_refferal_code','$referralCode','Stylist','$mother','$founding','inactive','$bonus_ex_date')";
                        $insert_res = mysql_query($insert_data);
                        if ($insert_res) {
                            $rowrcm2 = mysql_fetch_array($rcm2);
                            if ($rowrcm2['referal_count'] == 1) {
                                $qq = "update wp_store set referal_count = referal_count+1,designation = 'Senior Stylist'
                                where store_refferal_code = '$referralCode'";
                            } else if ($rowrcm2['referal_count'] == 3) {
                                $qq = "update wp_store set referal_count = referal_count+1,designation = 'Style Associate'
                                where store_refferal_code = '$referralCode'";
                            } else if ($rowrcm2['referal_count'] == 5) {
                                $qq = "update wp_store set referal_count = referal_count+1,designation = 'Style Leader'
                                where store_refferal_code = '$referralCode'";
                            } else {
                                $qq = "update wp_store set referal_count = referal_count+1 where store_refferal_code = '$referralCode'";
                            }
                            $rr = mysql_query($qq);
                            if ($rr) {
                                $status = "done";
                                $message = "Your Stylist has been created successfully and it would be live after approval from admin.";
                            } else {
                                $status = "error";
                                $message = mysql_error();
                            }
                        } else {
                            $status = "error";
                            $message = mysql_error();
                        }
                    }
                }
            } else {
                $status = "error";
                $message = "Invalid Refferal Code";
            }
        }else{
            $filename = '../../' . $store_name;
            if (!file_exists($filename)){
                 $makeDir = mkdir('../../' . $store_name);
            }
            else {
                $message = $store_name." already exists please select another name";
            }
            if ($makeDir) {
                $indexcopy = copy('../../demo/index.php', '../../' . $store_name . '/index.php');
                if ($indexcopy) {
                    file_put_contents('../../' . $store_name . '/index.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                }
                $headcopy = copy('../../demo/header.php', '../../' . $store_name . '/header.php');
                if ($headcopy) {
                    file_put_contents('../../' . $store_name . '/header.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                }
                $copycontact = copy('../../demo/contact.php', '../../' . $store_name . '/contact.php');
                if ($copycontact) {
                    file_put_contents('../../' . $store_name . '/contact.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                }
				$copycart = copy('../../demo/cart.php', '../../' . $store_name . '/cart.php');
				 if ($copycart) {
					file_put_contents('../../' . $store_name . '/cart.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
				}
                $copycart = copy('../../demo/orders.php', '../../' . $store_name . '/orders.php');
                if ($copycart) {
                    file_put_contents('../../' . $store_name . '/orders.php', "<input type='hidden' value='$store_name' id='get_store_data'>", FILE_APPEND);
                }
                copy('../../demo/about.php', '../../' . $store_name . '/about.php');
                copy('../../demo/canvas.php', '../../' . $store_name . '/canvas.php');
                copy('../../demo/accessories.php', '../../' . $store_name . '/accessories.php');
                copy('../../demo/androidObject.php', '../../' . $store_name . '/androidObject.php');
                copy('../../demo/colorSwitcher.php', '../../' . $store_name . '/colorSwitcher.php');
                copy('../../demo/custom_configure.php', '../../' . $store_name . '/custom_configure.php');
                copy('../../demo/custom_configure_overcoat.php', '../../' . $store_name . '/custom_configure_overcoat.php');
                copy('../../demo/custom_overcoat.php', '../../' . $store_name . '/custom_overcoat.php');
                copy('../../demo/custom_pants.php', '../../' . $store_name . '/custom_pants.php');
                copy('../../demo/custom_shirts.php', '../../' . $store_name . '/custom_shirts.php');
                copy('../../demo/custom_suit.php', '../../' . $store_name . '/custom_suit.php');
                copy('../../demo/customfit.php', '../../' . $store_name . '/customfit.php');
                copy('../../demo/customize.php', '../../' . $store_name . '/customize.php');
                copy('../../demo/fabric.php', '../../' . $store_name . '/fabric.php');
                copy('../../demo/fullbespoke.php', '../../' . $store_name . '/fullbespoke.php');
                copy('../../demo/highend.php', '../../' . $store_name . '/highend.php');
                copy('../../demo/highenddetails.php', '../../' . $store_name . '/highenddetails.php');
                copy('../../demo/login.php', '../../' . $store_name . '/login.php');
                copy('../../demo/logout.php', '../../' . $store_name . '/logout.php');
                copy('../../demo/myaccount.php', '../../' . $store_name . '/myaccount.php');
                copy('../../demo/objectLoader.php', '../../' . $store_name . '/objectLoader.php');
                copy('../../demo/pricelist.php', '../../' . $store_name . '/pricelist.php');
                copy('../../demo/suit_old.php', '../../' . $store_name . '/suit_old.php');
                copy('../../demo/topbrands.php', '../../' . $store_name . '/topbrands.php');
                copy('../../demo/workprocess.php', '../../' . $store_name . '/workprocess.php');
                copy('../../demo/sizematching.php', '../../' . $store_name . '/sizematching.php');
				copy('../../demo/copycustomsuit.php', '../../' . $store_name . '/copycustomsuit.php');
				copy('../../demo/footer.php', '../../' . $store_name . '/footer.php');
				copy('../../demo/sdet.php', '../../' . $store_name . '/sdet.php');
				copy('../../demo/startconfigovercoat.php', '../../' . $store_name . '/startconfigovercoat.php');
				copy('../../demo/startJacket.php', '../../' . $store_name . '/startJacket.php');
				copy('../../demo/startjacketcollection.php', '../../' . $store_name . '/startjacketcollection.php');
				copy('../../demo/startovercoat.php', '../../' . $store_name . '/startovercoat.php');
				copy('../../demo/startpant.php', '../../' . $store_name . '/startpant.php');
				copy('../../demo/startshirt.php', '../../' . $store_name . '/startshirt.php');
				copy('../../demo/stylegenie.php', '../../' . $store_name . '/stylegenie.php');
				copy('../../demo/stylish.php', '../../' . $store_name . '/stylish.php');
                copy('../../demo/paynow.php', '../../' . $store_name . '/paynow.php');
				copy('../../demo/jquery-2.2.2.js', '../../' . $store_name . '/jquery-2.2.2.js');
                $target_path = "upload/storelogo/";
                $file_ext = strtolower(end(explode('.', $_FILES['store_logo']['name'])));
                $tmp_name = $_FILES['store_logo']['tmp_name'];
                $file_name = "logo" . $store_name . "." . $file_ext;
                $store_refferal_code = "STR".rand(1000000,9999999);
                $target_path = $target_path . $file_name;
                if (move_uploaded_file($tmp_name, $target_path)) {
                    $store_logo = "admin/webserver/" . $target_path;
                    $bonus_ex_date = get_store_date();
                    $insert_data = "insert into wp_store (user_name,last_name,company_name,user_email,store_phone,store_password,store_name,
                    store_logo,store_about,store_contact_address,zip_code,b_country,b_state,b_city,dob,store_status,
                    created_date,store_refferal_code,designation,mother,founding,start_month_bonus,bonus_expire_date)values('$user_name','$last_name','$company_name','$user_email','$store_phone','$store_password',
                    '$store_name','$store_logo','$store_about','$store_contact_address','$zip_code','$b_country',
                    '$b_state','$b_city','$dob','$store_status',NOW(),'$store_refferal_code','Stylist','$mother','$founding','inactive','$bonus_ex_date')";
                    $insert_res = mysql_query($insert_data);
                    if ($insert_res) {
                        $status = "done";
                        $message = "Your Stylist has been created successfully and it would be live after approval from admin.";
                    } else {
                        $status = "error";
                        $message = mysql_error();
                    }
                }
            }
        }
    }
    echo json_encode(array("status"=>$status,"message"=>$message));
}
if($type=="storeupdate"){
    $store_id = $_REQUEST['store_id'];
    $qq = "select * from wp_store where store_id = '$store_id'";
    $rr = mysql_query($qq);
    $num = mysql_num_rows($rr);
    if($num>0)
    {

        $update_data = "update wp_store set user_name='$user_name',store_phone='$store_phone',store_contact_address='$store_contact_address' ,store_password='$store_password' where store_id='$store_id'";
        $update_res = mysql_query($update_data);
        if($update_res){
            $status=  "done";
            $message = "Store information Updated Successfully";
        }
        else{
            $status=  "error";
            $message=mysql_error();
        }
    }
    echo json_encode(array("status"=>$status,"message"=>$message));
}
?>