<?php
include('header.php');
include("admin/webserver/database/db.php");
?>
</div>
<div class="about-bottom wthree-3">
    <style>
        .active {
            border: 2px solid #555;
            border-radius: 4px;
        }
        .primarysuggestion {
            margin-left: 3px;
            margin-top: 3px;
            width: 23%;
            height:250px;
        }
        .primarysuggestion img {
            height: 60%;
            width: 175px;
        }
        .colorsuggestion img {
            height: 75%;
        }
        .steps {
            background: #fff none repeat scroll 0 0;
            border: 2px solid #aeaeae;
            border-radius: 7%;
            cursor: pointer;
            float: left;
            font-size: 13px;
            line-height: 17px;
            margin-bottom: 0;
            margin-left: 5px;
            margin-top: 5px;
            max-height: 57px;
            padding-top: 9px;
            text-align: center;
            width: 11%;
        }
        .stepsactive {
            background: #c4996c none repeat scroll 0 0;
            border: 2px solid #c4996c;
            color: white;
        }
        .outputsugg {
            position: absolute;
        }
        .backbtnsuggparentdiv p
        {
            font-weight:normal;
            font-size: 12px;
            line-height: 18px!important;
        }
        .okok {
            font-size: 19px;
            height: 52px;
            margin-left: 10px;
            margin-top: 10px;
            padding-top: 6px;
            width: 162px;
        }
        .colorsuggestion{
            height:110px;
            width:16%;
        }
        .suggestionname{
            line-height:15px;
        }
        .overflow-cmd{overflow-y: auto; height: 670px; padding-top: 22px;}

        .data_{
            margin-top: 2px;
            padding: 0px;
            border: 1px lightgrey solid;
            border-radius: 5px;
            position: absolute;
            max-height: 100px;
            overflow: auto;
            width: 98%;
        }

        .data_ li{
            padding: 5px;
            border-bottom: 1px lightgrey solid;
        }
        .data_ li:hover{
            cursor: pointer;
            background-color: silver;
        }
        .data-remove{
            display:none;
        }
    </style>

    <div class="loader" style="display: none">
        <img src="images/spin.gif" class="spinloader"/>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
            <div class="col-md-1" style="">
                <i class="fa fa-mail-forward fa-2x" onclick="rotateImage('left')" style="margin-top:300px;cursor:pointer"></i>
            </div>
            <div class="col-md-10" style="padding-top: 50px;position: relative">
                <img src=""  class="outputsugg" id="shirtimg" alt='' style="z-index:0;"/>
                <img src=""  class="outputsugg" id="breastpocketimg" alt='' style="z-index:5;"/>
                <img src=""  class="outputsugg" id="lowerpocketimg" alt='' style="z-index:5;"/>
                <img src=""  class="outputsugg" id="vestimg" alt='' style="z-index:1;"/>
                <img src=""  class="outputsugg" id="frontbuttonimg" alt='' style="z-index:6;"/>
                <img src=""  class="outputsugg" id="frontbuttoncutimg" alt='' style="z-index:5;"/>
                <img src=""  class="outputsugg" id="frontbuttonpointimg" alt='' style="z-index:7;"/>
            </div>
            <div class="col-md-1" style="">
                <i class="fa fa-mail-reply fa-2x" onclick="rotateImage('right')" style="margin-top:300px;cursor:pointer"></i>
            </div>
        </div>
        <div class="col-md-6">
            <div class="col-md-12" style="padding:25px 0">
                <div class="steps stepsactive" id="category">Vest Category</div>
                <div class="steps" id="collarstyle">Collar </br>Style</div>
                <div class="steps" id="lapelbuttonhole">Lapel </br>Buttonhole</div>
                <div class="steps" id="buttonholestyle">Buttonhole </br>Style</div>
                <div class="steps" id="buttonstyle">Button </br>Style</div>
                <div class="steps" id="bottomstyle">Bottom </br>Style</div>
                <div class="steps" id="lowerpocket">Lower </br>Pocket</div>
                <div class="steps" id="ticketpocket">Ticket </br>Pocket</div>
                <div class="steps" id="backstyle">Back </br>Style</div>
                <div class="steps" id="chestdart">Chest Dart</br>Style</div>
                <div class="steps" id="topstitch">Outside</br>Top Stitch</div>
                <div class="steps" id="placketstyle">Placket</br>Style</div>
                <div class="steps" id="tuxedostyle">Tuxedo</br>Style</div>
                <div class="steps" id="breastpocket">Breast</br>Pocket</div>
                <div class="steps" id="chestpocket">Chest</br>Pocket</div>
                <div class="steps" id="threadcolor">Thread</br>Color</div>
                <div class="steps" id="linningoption">Linning</br>Option</div>
                <div class="steps" id="vestfabric">Vest</br>Color</div>
            </div>
            <!--
            ----category div
            --->
            <div class="col-md-12 stepdiv" id="categorys" style="display: block">
                <p class="stylecaption">Vest Category</p>
                <div class="primarysuggestion okok active" id="category_formal" style="margin-left: 140px">
                    <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Formal</p>
                </div>
                <div class="primarysuggestion okok" id="category_casual" style="margin-left: 10px">
                    <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Casual</p>
                </div>
            </div>
            <!--
            ----collar style div
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="collarstyles" style="display:none">
                <p class="stylecaption">Collar Style</p>
                <div class="primarysuggestion" id="collarstyle_V-4000:v_shape">
                    <img src="images/vest/v-4000standardvshapenocollar.png"/>
                    <p class="suggestionname">V-4000</br>standard v shape collar(standard)</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4001:u_shape" >
                    <img src="images/vest/V-4001Ushapenocollar.png"/>
                    <p class="suggestionname">V-4001</br>U shape no collar</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4002:shawl_with_collar_band" >
                    <img src="images/vest/V-4002shawllapelwithcollarband.png"/>
                    <p class="suggestionname">V-4002</br>shawl lapel with collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4003:shawl_without_collar_band" >
                    <img src="images/vest/V-4003shawllapelnocollarband.png"/>
                    <p class="suggestionname">V-4003</br>shawl lapel no collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4004:notch_without_collar_band" >
                    <img src="images/vest/V-4004notchlapelnocollarband.png"/>
                    <p class="suggestionname">V-4004</br>notch lapel no collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4005:notch_with_collar_band" >
                    <img src="images/vest/V-4005notchlapelwithcollarband.png"/>
                    <p class="suggestionname">V-4005</br>notch lapel with collar band</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4006:peak_without_collar_band" >
                    <img src="images/vest/V-4006peaklapelnocollarband.png"/>
                    <p class="suggestionname">V-4006</br>peak lapel no collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4007:peak_with_collar_band" >
                    <img src="images/vest/V-4007peaklapelwithcollarband.png"/>
                    <p class="suggestionname">V-4007</br>peak lapel with collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4011:semi_notch_without_collar_band" >
                    <img src="images/vest/V-4011seminotchlapelnocollarband.png"/>
                    <p class="suggestionname">V-4011</br>semi notch lapel no collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4012:semi_notch_with_collar_band" >
                    <img src="images/vest/V-4012seminotchlapelwithcollarband.png"/>
                    <p class="suggestionname">V-4012</br>semi notch lapel with collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4013:semi_peak_without_collar_band" >
                    <img src="images/vest/V-4013semipeaklapelnocollarband.png"/>
                    <p class="suggestionname">V-4013</br>semi peak lapel no collar band</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4014:semi_peak_with_collar_band" >
                    <img src="images/vest/V-4014semipeaklapelwithcollarband.png"/>
                    <p class="suggestionname">V-4014</br>semi peak lapel with collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4015:rounded_notch_without_collar_band" >
                    <img src="images/vest/V-4015roundednotchlapelnocollarband.png"/>
                    <p class="suggestionname">V-4015</br>rounded notch lapel</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4016:rounded_notch_adding_stand_collar" >
                    <img src="images/vest/V-4016roundednocthlapleaddingstandcollar.png"/>
                    <p class="suggestionname">V-4016</br>rounded nocth laple adding standcollar</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4017:notch_rounded_collar_point_without_collar_band" >
                    <img src="images/vest/V-4017Notchlapelroundedcollarpointnocollarband.png"/>
                    <p class="suggestionname">V-4017</br>Notch lapel rounded collar point no collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4018:rounded_notch_rounded_collar" >
                    <img src="images/vest/V-4018roundednotchlapelroundedcollar.png"/>
                    <p class="suggestionname">V-4018</br>Rounded notch lapel rounded collar</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4019:notch_round_collar_point_without_collar_band" >
                    <img src="images/vest/V-4019notchlapelroundcollarpointnocollarband.png"/>
                    <p class="suggestionname">V-4019</br>notch lapel round collar point no collarband</p>
                </div>
                <div class="primarysuggestion" id="collarstyle_V-4020:rounded_notch_rounded_collar" >
                    <img src="images/vest/V-4020roundednotchlapelroundedcollar.png"/>
                    <p class="suggestionname">V-4020</br>Rounded notch lapel rounded collar</p>
                </div>
            </div>
            <!--
            ----Lapel buttonhole style div
            --->
            <div class="col-md-12 stepdiv" id="lapelbuttonholes" style="display:none">
                <p class="stylecaption">Lapel Buttonhole Position</p>
                <div class="primarysuggestion" id="lapelbuttonhole_V-455G:left_four_lapel_sham_button_holes" >
                    <img src="images/vest/V-455Gleftfourlapelshambuttonholes.png"/>
                    <p class="suggestionname">V-455G</br>left four lapel sham buttonholes</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-455L:right_left_two_lapel_sham_button_hole" >
                    <img src="images/vest/V-455Lrightlefttwolapelshambuttonhole.png"/>
                    <p class="suggestionname">V-455L</br>right & left two lapel sham buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4541:left_lapel_sham_button_hole" name="lapelbuttonhole_V-4541:left_lapel_sham_button_hole">
                    <img src="images/vest/V-4541leftlapelshambuttonhole.png"/>
                    <p class="suggestionname">V-4541</br>left lapel sham buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4542:right_lapel_sham_button_hole_no_left_lapel_sham_button_hole" >
                    <img src="images/vest/V-4542rightlapelshambuttonholenoleftlapelshambuttonhole.png"/>
                    <p class="suggestionname">V-4542</br>Right lapel sham button hole no left lapel sham buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4543:right_left_lapel_sham_button_hole" >
                    <img src="images/vest/V-4543rightleftlapelshambuttonhole.png"/>
                    <p class="suggestionname">V-4543</br>right & left lapel sham buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4544:no_lapel_sham_button_hole" >
                    <img src="images/vest/V-4544nolapelshambuttonhole.png"/>
                    <p class="suggestionname">V-4544</br>no lapel sham buttonhole(most popular)</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4545:left_two_lapel_sham_button_holes" >
                    <img src="images/vest/V-4545lefttwolapelshambuttonholes.png"/>
                    <p class="suggestionname">V-4545</br>left two lapel sham buttonholes</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4546:right_two_lapel_sham_button_holes_no_left_lapel_sham_button_hole" >
                    <img src="images/vest/V-4546righttwolapelshambuttonholesnoleftlapelshambuttonhole.png"/>
                    <p class="suggestionname">V-4546</br>right two lapel sham button holes no left lapel sham buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4549:left_three_right_two_lapel_sham_button_holes" >
                    <img src="images/vest/V-4549leftthreerighttwolapelshambuttonholes.png"/>
                    <p class="suggestionname">V-4549</br>left three right two lapel sham buttonholes</p>
                </div>
                <div class="primarysuggestion" id="lapelbuttonhole_V-4550:left_three_lapel_sham_button_holes">
                    <img src="images/vest/V-4550leftthreelapelshambuttonholes.png"/>
                    <p class="suggestionname">V-4550</br>left three lapel sham buttonholes</p>
                </div>
             </div>
            <!--
            ----buttonhole style div
            --->
            <div class="col-md-12 stepdiv" id="buttonholestyles" style="display:none">
                <p class="stylecaption">Button Hole Style</p>
                <div class="primarysuggestion" id="buttonholestyle_V-455B:customer_appointed_fabric_for_double_besom_lapel_button_hole" >
                    <img src="images/vest/V-455Bcustomerappointedfabricfordoublebesomlapelbuttonhole.png"/>
                    <p class="suggestionname">V-455B</br>customer appointed fabric for double besom lapel buttonhole</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-455K:real_straight_with_0.5cm_lapel_button_hole" >
                    <img src="images/vest/V-455Krealstraightwith05cmlapelbuttonhole.png"/>
                    <p class="suggestionname">V-455K</br>real straight with 05cm lapel buttonhole</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-4551:normal_straight_lapel_button_hole" >
                    <img src="images/vest/V-4551normalstraightlapelbuttonhole.png"/>
                    <p class="suggestionname">V-4551</br>normal straight lapel buttonhole</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-4552:normal_round_point_lapel_button_hole" >
                    <img src="images/vest/V-4552normalroundpointlapelbuttonhole.png"/>
                    <p class="suggestionname">V-4552</br>normal round point lapel buttonhole</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-4553:functional_straight_lapel_button_hole" >
                    <img src="images/vest/V-4553functionalstraightlapelbuttonhole.png"/>
                    <p class="suggestionname">V-4553</br>functional straight lapel buttonhole</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-4554:real_round_point_lapel_sham_button_hole" >
                    <img src="images/vest/V-4554realroundpointlapelshambuttonhole.png"/>
                    <p class="suggestionname">V-4554</br>real round point lapel sham buttonhole</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-4556:real_straight_lapel_sham_button_hole_with_hand_stitching" >
                    <img src="images/vest/V-4556realstraightlapelshambuttonholewithhandstitching.png"/>
                    <p class="suggestionname">V-4556</br>real straight lapel sham button hole with hand stitching</p>
                </div>
                <div class="primarysuggestion" id="buttonholestyle_V-4557:real_round_lapel_sham_button_hole_with_hand_stitching" >
                    <img src="images/vest/V-4557realroundlapelshambuttonholewithhandstitching.png"/>
                    <p class="suggestionname">V-4557</br>real round lapel sham button hole with hand stitching</p>
                </div>
            </div>
            <!--
            ----button style div
            --->
            <div class="col-md-12 stepdiv" id="buttonstyles" style="display:none">
                <p class="stylecaption">Button Style</p>
                <div class="primarysuggestion" id="buttonstyle_V-401C:single_breasted_three_button" >
                    <img src="images/vest/V-401Csinglebreasted3buttons.png"/>
                    <p class="suggestionname">V-401C</br>single breasted 3 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401D:single_breasted_four_button" >
                    <img src="images/vest/V-401Dsinglebreasted4buttons.png"/>
                    <p class="suggestionname">V-401D</br>single breasted 4 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401F:single_breasted_five_button_use_the_pattern_of_single_breasted_seven_button" title="single breasted 5 buttons use the pattern of single breasted 7 buttons">
                    <img src="images/vest/V-401Fsinglebreasted5buttonsusethepatternofsinglebreasted7buttons.png"/>
                    <p class="suggestionname">V-401F</br>single breasted 5 buttons use the pattern of single breasted 7 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401G:single_breasted_five_button" title="">
                    <img src="images/vest/V-401Gsinglebreasted5buttons.png"/>
                    <p class="suggestionname">V-401G</br>single breasted 5 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401K:double_breasted_six_button" title="">
                    <img src="images/vest/V-401Kdoublebreasted6buttonswith3fasten.png"/>
                    <p class="suggestionname">V-401K</br>double breasted 6 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401L:double_breasted_eight_button_with_four_fasten" title="">
                    <img src="images/vest/V-401Ldoublebreasted8buttonswith4fasten.png"/>
                    <p class="suggestionname">V-401L</br>double breasted 8 buttons with 4 fasten </p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401M:double_breasted_four_button_with_two_fasten" title="">
                    <img src="images/vest/V-401Mdoublebreasted4buttonswith2fasten.png"/>
                    <p class="suggestionname">V-401M</br>double breasted 4 buttons with 2 fasten</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401O:single_breasted_eight_button" title="">
                    <img src="images/vest/V-401Osinglebreasted8buttons.png"/>
                    <p class="suggestionname">V-401O </br>single breasted 8 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401P:single_breasted_six_button" title="">
                    <img src="images/vest/V-401Osinglebreasted8buttons.png"/>
                    <p class="suggestionname">V-401P</br>single breasted 6 buttons</p>
                </div>
                <div class="primarysuggestion" id="buttonstyle_V-401S:single_breasted_seven_button" title="">
                    <img src="images/vest/V-401Ssinglebreasted7buttons.png"/>
                    <p class="suggestionname">V-401S</br>single breasted 7 buttons</p>
                </div>
            </div>
            <!--
            ----bottom style div
            --->
            <div class="col-md-12 stepdiv" id="bottomstyles" style="display:none">
                <p class="stylecaption">Bottom Style</p>
                <div class="primarysuggestion" id="bottomstyle_V-401J:pointed" >
                    <img src="images/vest/V-401Jpointedbottom.png"/>
                    <p class="suggestionname">V-401J</br>pointed bottom</p>
                </div>
                <div class="primarysuggestion" id="bottomstyle_V-401Y:round" >
                    <img src="images/vest/V-401Yroundbottom.png"/>
                    <p class="suggestionname">V-401Y</br>round bottom</p>
                </div>
                <div class="primarysuggestion" id="bottomstyle_V-401Z:squared" >
                    <img src="images/vest/V-401Zsquarebottom.png"/>
                    <p class="suggestionname">V-401Z</br>square bottom</p>
                </div>
                <div class="primarysuggestion" id="bottomstyle_V-401A:no_chest_dart_with_pointed" >
                    <img src="images/vest/V-401Anochestdartwithpointedbottom.png"/>
                    <p class="suggestionname">V-401A</br>no chest dart with pointed bottom</p>
                </div>
                <div class="primarysuggestion" id="bottomstyle_V-401B:no_chest_dart_with_straight" >
                    <img src="images/vest/V-401Bnochestdartstraightbottom.png"/>
                    <p class="suggestionname">V-401B</br>no chest dart straight bottom</p>
                </div>
                <div class="primarysuggestion" id="bottomstyle_V-401E:no_chest_dart_with_rounded" >
                    <img src="images/vest/V-401Enochestdartroundedbottom.png"/>
                    <p class="suggestionname">V-401E</br>no chest dart rounded bottom</p>
                </div>
                <div class="primarysuggestion" id="bottomstyle_V-401H:square_bottom_with_slant_front" >
                    <img src="images/vest/V-401Hsuqrebottomwithslantfront.png"/>
                    <p class="suggestionname">V-401H</br>Square Bottom with slant front</p>
                </div>
            </div>
            <!--
            -----lower pocket style div
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="lowerpockets" style="display:none">
                <p class="stylecaption">Lower Pocket Style</p>
                <div class="primarysuggestion" id="lowerpocket_V-4300:normal_welt" >
                    <img src="images/vest/V-4300weltpocket.png"/>
                    <p class="suggestionname">V-4300</br>Normal welt</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4301:double_besom" >
                    <img src="images/vest/V-4301doublebesompockets.png"/>
                    <p class="suggestionname">V-4301</br>Double Besom</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4302:double_besom_with_flap" >
                    <img src="images/vest/V-4302doublebesomwithflappockets.png"/>
                    <p class="suggestionname">V-4302</br>Double Besom with Flap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4151:no_lower_pocket" >
                    <img src="images/vest/V-4151nolowerpocket.png"/>
                    <p class="suggestionname">V-4151</br>no lower pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4152:double_besom_with_simi_pointed_flap" >
                    <img src="images/vest/V-4152doublebesomlowerpocketwithsimipointedflap.png"/>
                    <p class="suggestionname">V-4152</br>double besom lower pocket with simi pointed flap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4153:double_besom_with_pointed_flap" >
                    <img src="images/vest/V-4153doublebesomlowerpocketwithpointedflap.png"/>
                    <p class="suggestionname">V-4153</br>double besom lower pocket with pointed flap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4154:double_besom_with_button_and_button_hole_on_flap" >
                    <img src="images/vest/V-4154doublebesomlowerpocketwithbuttonbuttonholeonflap.png"/>
                    <p class="suggestionname">V-4154</br>double besom lower pocket with button button hole on flap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4155:double_besom_with_diamond_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4155doublebesomlowerpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4155</br>double besom lower pocket with diamond flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4156:double_besom_with_semi_pointed_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4156doublebesomlowerpocketwithsemipointedflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4156</br>double besom lower pocket with semi pointed flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4157:double_besom_with_pointed_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4157doublebesomlowerpocketwithpointedflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4157</br>double besom lower pocket with pointed flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4158:double_besom_with_loop_button" >
                    <img src="images/vest/V-4158doublebesomlowerpocketwithoopbutton.png"/>
                    <p class="suggestionname">V-4158</br>double besom lower pocket with oop button</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4159:double_besom_with_pointed_tab_button_and_button_hole" >
                    <img src="images/vest/V-4159doublebesomlowerpocketwithpointedtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4159</br>double besom lower pocket with pointed tab button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4160:double_besom_with_square_tab_button_and_button_hole" >
                    <img src="images/vest/V-4160doublebesomlowerpocketwithsquaretabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4160</br>double besom lower pocket with square tab button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4161:double_besom_with_round_tab_button_and_button_hole" >
                    <img src="images/vest/V-4161doublebesomlowerpocketwithroundtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4161</br>double besom lower pocket with round tab button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4162:patch" >
                    <img src="images/vest/V-4162patchlowerpocket.png"/>
                    <p class="suggestionname">V-4162</br>patch lower pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4163:patch_with_button_and_button_hole" >
                    <img src="images/vest/V-4163patchlowerpocketwithbuttonandbuttonhole.png"/>
                    <p class="suggestionname">V-4163</br>patch lower pocket with button and buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4164:patch_with_flap" >
                    <img src="images/vest/V-4164patchlowerpocketwithflap.png"/>
                    <p class="suggestionname">V-4164</br>patch lower pocket with flap </p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4165:patch_with_diamond_flap_button_and_button_hole" >
                    <img src="images/vest/V-4165patchlowerpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4165</br>patch lower pocket with diamond flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4166:patch_with_flap_button_and_button_hole" >
                    <img src="images/vest/V-4166patchlowerpocketwithflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4166</br>patch lower pocket with flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4167:patch_with_diamond_flap_button_and_button_hole">
                    <img src="images/vest/V-4167patchlowerpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4167</br>patch lower pocket with diamond flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4168:patch_with_single_pleat" >
                    <img src="images/vest/V-4168patchlowerpocketwithsinglepleat.png"/>
                    <p class="suggestionname">V-4168</br>patch lower pocket with single pleat</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4169:slant_welt" >
                    <img src="images/vest/V-4169slantweltlowerpocket.png"/>
                    <p class="suggestionname">V-4169</br>slant welt lower pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4170:slant_double_besom" >
                    <img src="images/vest/V-4170slantdoublebesomlowerpocket.png"/>
                    <p class="suggestionname">V-4170</br>slant double besom lower pocket </p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4171:slant_double_besom_with_flap" >
                    <img src="images/vest/V-4171slantdoublebesomlowerpocketwithflap.png"/>
                    <p class="suggestionname">V-4171</br>slant double besom lower pocket with flap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4172:slant_double_besom_with_diamond_flap" >
                    <img src="images/vest/V-4172slantdoublebesomlowerpocketwithdiamonflap.png"/>
                    <p class="suggestionname">V-4172</br>slant double besom lower pocket with diamonflap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4173:slant_double_besom_with_flap_button_and_button_hole" >
                    <img src="images/vest/V-4173slantdoublebesomlowerpocketwithfalpbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4173</br>slant double besom lower pocket with falp button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4174:slant_double_besom_with_diamond_flap_button_and_button_holes" >
                    <img src="images/vest/V-4174slantdoublebesomlowerpocketwithdiamonflapbuttonbuttonholes.png"/>
                    <p class="suggestionname">V-4174</br>slant double besom lower pocket with diamond of lap button buttonholes</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4175:slant_double_besom_with_loop_button" >
                    <img src="images/vest/V-4175slantdoublebesomlowerpocketwithloopbutton.png"/>
                    <p class="suggestionname">V-4175</br>slant double besom lower pocket with loop button</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4176:slant_double_besom_with_pointed_tab_button_and_button_hole" >
                    <img src="images/vest/V-4176slantdoublebesomlowerpocketwithpointedtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4176</br>slant double besom lower pocket with pointed tab button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4177:slant_double_besom_with_square_tab_button_and_button_hole" >
                    <img src="images/vest/V-4177slantdoublebesomlowerpocketwithsquaretabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4177</br>slant double besom lower pocket with square tab button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-4178:slant_double_besom_with_round_tab_button_and_button_hole" >
                    <img src="images/vest/V-4178slantdoublebesomlowerpocketwithroundtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4178</br>slant double besom lower pocket with round tab button buttonhole</p>
                </div>
                <!--unwanted pockets starts from here-->
                <div class="primarysuggestion" id="lowerpocket_V-41A0:welt_lower_pocket_left_welt_breast_pocket" >
                    <img src="images/vest/V-41A0weltlowerpocketleftweltbreastpocket.png"/>
                    <p class="suggestionname">V-41A0</br>welt lower pocket left welt breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A1:double_besom_lower_pocket_left_double_besom_breast_pocket">
                    <img src="images/vest/V-41A1doublebesomlowerpocketleftdoublebesombreastpocket.png"/>
                    <p class="suggestionname">V-41A1</br>double besom lower pocket left double besom breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A2:double_besom_lower_pocket_with_flap_left_welt_chest_pocket" >
                    <img src="images/vest/V-41A2doublebesomlowerpocketwithflapleftweltchestpocket.png"/>
                    <p class="suggestionname">V-41A2</br>double besom lower pocket with flap left welt chest pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A2:double_besom_lower_pocket_left_welt_breast_pocket" >
                    <img src="images/vest/V-41A3doublebesomlowerpocketleftweltbreastpocket.png"/>
                    <p class="suggestionname">V-41A3</br>double besom lower pocket left welt breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A4:double_besom_lower_pocket_with_flap_left_double_besom_breast_pocket" >
                    <img src="images/vest/V-41A4doublebesomlowerpocketwithflapleftdoublebesombreastpocket.png"/>
                    <p class="suggestionname">V-41A4</br>double besom lower pocket with flap left double besom breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A5:welt_lower_pocket_welt_breast_pocket" >
                    <img src="images/vest/V-41A5weltlowerpocketweltbreastpockets.png"/>
                    <p class="suggestionname">V-41A5</br>welt lower pocket welt breast pockets</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A6:double_besom_lower_pocket_left_right_double_besom_breast_pocket" >
                    <img src="images/vest/V-41A6doublebesomlowerpocketleftrightdoublebesombreastpocket.png"/>
                    <p class="suggestionname">V-41A6</br>double besom lower pocket left right double besom breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A7:double_besom_lower_pocket_with_flap_welt_breast_pockets" >
                    <img src="images/vest/V-41A7doublebesomlowerpocketwithflapweltbreastpockets.png"/>
                    <p class="suggestionname">V-41A7</br>double besom lower pocket with flap welt breast pockets</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A8:double_besom_lower_pocket_welt_breast_pockets" >
                    <img src="images/vest/V-41A8doublebesomlowerpocketweltbreastpockets.png"/>
                    <p class="suggestionname">V-41A8</br>double besom lower pocket welt breast pockets</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41A9:double_besom_lower_pocket_with_flap_left_right_double_besom_breast_pocket" >
                    <img src="images/vest/V-41A9doublebesomlowerpocketwithflapleftrightdoublebesombreastpocket.png"/>
                    <p class="suggestionname">V-41A9</br>double besom lower pocket with flap left right double besom breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-41B1:welt_lower_pocket_breast_pocket">
                    <img src="images/vest/V-41B1weltlowerpocketbreastpocket.png"/>
                    <p class="suggestionname">V-41B1</br>welt lower pocket breast pocket</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-415A:double_besom_lower_pocket_with_diamond_flap" >
                    <img src="images/vest/V-415Adoublebesomlowerpocketwithdiamondflap.png"/>
                    <p class="suggestionname">V-415A</br>double besom lower pocket with diamond flap</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-415B:left_double_besom_lower_pocket_right_lower_pocket_with_half_pointed_flap_button_and_button_hole" >
                    <img src="images/vest/V-415Bleftdoublebesomlowerpocketrightlowerpocketwithhalfpointedflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-415B</br>left double besom lower pocket right lower pocket with half pointed flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="lowerpocket_V-430B:welt_1.0cm_width_lower_pocket" >
                    <img src="images/vest/V-430Bwelt10cmwidthlowerpocket.png"/>
                    <p class="suggestionname">V-430B</br>welt 1.0cm width lowerpocket</p>
                </div>
            </div>
            <!--
            -----ticket pocket style div
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="ticketpockets" style="display:none">
                <p class="stylecaption">Ticket Pocket Style</p>
                <div class="primarysuggestion" id="ticketpocket_V-42R4:slanted_double_besom" >
                    <img src="images/vest/V-42R4slanteddoublebesomticketpocket.png"/>
                    <p class="suggestionname">V-42R4</br>slant double besom ticket pocket</p>
                </div>
                <div class="primarysuggestion" id="ticketpocket_V-42R5:slant_double_besom_ticket_pocket_with_flap" >
                    <img src="images/vest/V-42R5slantdoublebesomticketpocketwithflap.png"/>
                    <p class="suggestionname">V-42R5</br>slant double besom ticket pocket with flap</p>
                </div>
                <div class="primarysuggestion" id="ticketpocket_V-48B1:slant_double_besom_ticket_pocket_with_flap_button_and_button_hole" >
                    <img src="images/vest/V-48B1slantdoublebesomticketpocketwithflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-48B1</br>slant double besom ticket pocket with flap button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-48B2:slant_double_besom_with_diamond_flap_button_and_button_hole" >
                    <img src="images/vest/V-48B2slantdoublebesomticketpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-48B2</br>slant double besom ticket pocket with diamond flap button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-48B3:slant_double_besom_with_loop_and_button" >
                    <img src="images/vest/V-48B3slantdoublebesomticketpocketwithloopandbutton.png"/>
                    <p class="suggestionname">V-48B3</br>slant double besom ticket pocket with loop and button</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-48B4:slant_double_besom_with_pointed_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-48B4slantdoublebesomticketpocketwithpointedtabwithbuttonandbuttonhole.png"/>
                    <p class="suggestionname">V-48B4</br>slant double besom ticket pocket with pointed tab with button and buttonhole</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-48B5:slant_double_besom_with_square_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-48B5slantdoublebesomticketpocketwithsquaretabwithbuttonandbuttonhole.png"/>
                    <p class="suggestionname">V-48B5</br>slant double besom ticket pocket with square tab with button and buttonhole</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-48B6:slant_double_besom_with_round_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-48B6slantdoublebesomticketpocketwithroundtabwithbuttonandbuttonhole.png"/>
                    <p class="suggestionname">V-48B6</br>slant double besom ticket pocket with round tab with button and buttonhole</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-48B7:slant_double_besom_with_diamond_flap" >
                    <img src="images/vest/V-48B7slantdoublebesomticketpocketwithdiamondflap.png"/>
                    <p class="suggestionname">V-48B7</br>slant double besom ticket pocket with diamond flap</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-427C:left_and_right_slant_welt" >
                    <img src="images/vest/V-427Cleftandrightslantweltticketpocket.png"/>
                    <p class="suggestionname">V-427C</br>left and right slant welt ticket pocket</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4201:right_welt" >
                    <img src="images/vest/V-4201rightweltticketpocket.png"/>
                    <p class="suggestionname">V-4201</br>right welt ticket pocket</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4203:double_besom" >
                    <img src="images/vest/V-4203doublebesomticketpocket.png"/>
                    <p class="suggestionname">V-4203</br>double besom ticket pocket</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4204:left_welt" >
                    <img src="images/vest/V-4204leftweltticketpocket.png"/>
                    <p class="suggestionname">V-4204</br>left welt ticket pocket</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4204:left_and_right_welt" >
                    <img src="images/vest/V-4205leftandrightweltticketpocket.png"/>
                    <p class="suggestionname">V-4205</br>left and right welt ticket pocket</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4204:slant_welt" >
                    <img src="images/vest/V-4207slantweltticketpocket.png"/>
                    <p class="suggestionname">V-4207</br>slant welt ticket pocket</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4208:double_besom_with_diamond_flap" >
                    <img src="images/vest/V-4208doublebesomticketpocketwithdiamondflap.png"/>
                    <p class="suggestionname">V-4208</br>double besom ticket pocket with diamond flap</p>
                </div>
				<div class="primarysuggestion" id="ticketpocket_V-4209:double_besom_with_flap" >
                    <img src="images/vest/V-4209doublebesomticketpocketwithflap.png"/>
                    <p class="suggestionname">V-4209</br>double besom ticket pocket with flap</p>
                </div>
            </div>
    		<!--
            ----back style div
            --->
            <div class="col-md-12 stepdiv" id="backstyles" style="display:none">
                <p class="stylecaption">Back Style</p>
                <div class="primarysuggestion" id="backstyle_V-4200:back_with_strap">
                    <img src="images/vest/V-420Astandardvestwithstrapvestbackwithfabric.png"/>
                    <p class="suggestionname">V-4200</br>Standard vest with strap</p>
                </div>
                <div class="primarysuggestion" id="backstyle_V-4211:vest_back_with_fabric">
                    <img src="images/vest/V-420Astandardvestwithstrapvestbackwithfabric.png"/>
                    <p class="suggestionname">V-4211</br>Vest back with fabric</p>
                </div>
                <div class="primarysuggestion" id="backstyle_V-420B:back_without_strap">
                    <img src="images/vest/V-420Bnostrapvestbackwithlining.png"/>
                    <p class="suggestionname">V-420B</br>No strap,Vest back with lining</p>
                </div>
                <div class="primarysuggestion" id="backstyle_V-420C:no_strap_vest_back_with_body_fabric">
                    <img src="images/vest/V-420Cnostrapvestbackwithbodyfabric.png"/>
                    <p class="suggestionname">V-420C</br>No strap,Vest back with body fabric</p>
                </div>
				<div class="primarysuggestion" id="backstyle_V-420L:standard_vest_with_strap_vest_back_with_lining">
                    <img src="images/vest/V-420Lstandardvestwithstrapvestbackwithlining.png"/>
                    <p class="suggestionname">V-420L</br>Standard vest with strap,vest back with lining</p>
                </div>
				<div class="primarysuggestion" id="backstyle_V-4202:standard_without_belt">
                    <img src="images/vest/V-4202standardwithoutbelt.png"/>
                    <p class="suggestionname">V-4202</br>Standard without belt</p>
                </div>
			</div>	
			<!--
            ----chest dart style div
            ---->
            <div class="col-md-12 stepdiv" id="chestdarts" style="display:none">
                <p class="stylecaption">Chest Dart Style</p>
                <div class="primarysuggestion" id="chestdart_V-45C1:use_0.15_width_narrow_pick_stitch">
                    <img src="images/vest/V-45C1chestdartuse015widthnarrowpickstitch.png"/>
                    <p class="suggestionname">V-45C1</br>chest dart use 0.15 width narrow pick stitch</p>
                </div>
                <div class="primarysuggestion" id="chestdart_V-45C2:use_0.15_width_skip_wide_pick_stitch">
                    <img src="images/vest/V-45C2chestdartuse015widthskipwidepickstitch.png"/>
                    <p class="suggestionname">V-45C2</br>chest dart use 0.15 width skip wide pick stitch</p>
                </div>
                <div class="primarysuggestion" id="chestdart_V-45C3:use_0.6_width_pick_stitch">
                    <img src="images/vest/V-45C3chestdartuse06widthpickstitch.png"/>
                    <p class="suggestionname">V-45C3</br>chest dart use 0.6 width pick stitch</p>
                </div>
                <div class="primarysuggestion" id="chestdart_V-45C4:with_0.8_width_outside_pick_stitch">
                    <img src="images/vest/V-45C4chestdartwith08widthoutsidepickstitch.png"/>
                    <p class="suggestionname">V-45C4</br>chest dart with 0.8 width out side pick stitch</p>
                </div>
			</div>	
            <!--
            ----outside top stich style div
            --->
            <div class="col-md-12 stepdiv" id="topstitchs" style="display:none">
                <p class="stylecaption">Outside Top Stitch</p>
                <div class="primarysuggestion" id="topstitch_V-4572:collar_lapel_0.15cm_top_stitch" >
                    <img src="images/vest/V-4572collarlapel015cmtopstitch.png"/>
                    <p class="suggestionname">V-4572</br>collar lapel 0.15cm top stitch </p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4572:collar_lapel_0.15cm_pick_stitch" >
                    <img src="images/vest/V-4573collarlapel015cmpickstitch.png"/>
                    <p class="suggestionname">V-4573</br>collar lapel 0.15cm pick stitch</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4575:0.6cm_wide_top_stitch_on_collar_and_lapel" >
                    <img src="images/vest/V-457506cmwidetopstitchoncollarandlapel.png"/>
                    <p class="suggestionname">V-4575</br>0.6 cm wide top stitch on collar and lapel</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4576:0.6cm_pick_stitch_on_front_collar_and_pocket" >
                    <img src="images/vest/V-457606cmpickstitchonfrontcollarandpocket.png"/>
                    <p class="suggestionname">V-4576</br>0.6 cm pick stitch on front collar and pocket</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4577:0.6cm_wide_pick_stitch_on_collar_and_lapel" >
                    <img src="images/vest/V-457706cmwidepickstitchoncollarandlapel.png"/>
                    <p class="suggestionname">V-4577</br>0.6 cm wide pick stitch on collar and lapel</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4578:0.6cm_wide_pick_stich_on_collar_lapel_and_big_stitch" >
                    <img src="images/vest/V-457806cmwidepicstichoncollarlapelandbigstitch.png"/>
                    <p class="suggestionname">V-4578</br>0.6 cm wide pick stich on collar lapel and big stitch</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4580:0.6cm_top_stitch_on_front_collar_and_pocket" >
                    <img src="images/vest/V-458006cmtopstitchonfrontcollarandpocket.png"/>
                    <p class="suggestionname">V-4580</br>0.6 cm top stitch on front collar and pocket</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4571:0.15cm_narrow_pick_stitch_on_front_collar_and_pocket" >
                    <img src="images/vest/V-4571015cmnarrowpickstitchonfrontcollarandpocket.png"/>
                    <p class="suggestionname">V-4571</br>0.15 cm narrow pick stitch on front collar and pocket</p>
                </div>
                <div class="primarysuggestion" id="topstitch_V-4574:0.15cm_narrow_skip_pick_stitch_on_collar_and_lapel" >
                    <img src="images/vest/V-4574015cmnarrowskippickstitchoncollarandlapel.png"/>
                    <p class="suggestionname">V-4574</br>0.15 cm narrow skip pick stitch on collar and lapel</p>
                </div>
			</div>
            <!--
            ----placket style div
            --->
            <div class="col-md-12 stepdiv" id="placketstyles" style="display:none">
                <p class="stylecaption">Placket Front shape sewing</p>
                <div class="primarysuggestion" id="placketstyle_V-423A:not_sew_button_and_button_hole_deliver_the_button_thread_with_garment" >
                    <img src="images/vest/V-423Anotsewbuttonandbuttonholedeliverthebuttonthreadwithgarment.png"/>
                    <p class="suggestionname">V-423A</br>not sew button and button hole deliver the button thread with garment</p>
                </div>
                <div class="primarysuggestion" id="placketstyle_V-423B:not_sew_button_and_button_hole_deliver_the_button_thread_with_garment" >
                    <img src="images/vest/V-423Bnotsewbuttonandbuttonholedeliverthebuttonthreadwithgarment.png"/>
                    <p class="suggestionname">V-423B</br>not sew button and button hole deliver the button thread with garment</p>
                </div>
			</div>	
            <!--
            -----tuxedo style div
            ---->
            <div class="col-md-12 stepdiv" id="tuxedostyles" style="display:none">
                <p class="stylecaption">Tuxedo Style</p>
                <div class="primarysuggestion" id="tuxedostyle_V-4221:formal_tuxedo" >
                    <img src="images/vest/V-4221formaltuxedo.png"/>
                    <p class="suggestionname">V-4221</br>formal tuxedo</p>
                </div>
                <div class="primarysuggestion" id="tuxedostyle_V-4225:top_collar_use_satin" >
                    <img src="images/vest/V-4225topcollarusesatin.png"/>
                    <p class="suggestionname">V-4225</br>top collar use satin</p>
                </div>
                <div class="primarysuggestion" id="tuxedostyle_V-4225:satin_lapel" >
                    <img src="images/vest/V-4236satinlapel.png"/>
                    <p class="suggestionname">V-4236</br>satin lapel</p>
                </div>
                <div class="primarysuggestion" id="tuxedostyle_V-4237:pocket_besom_with_satin" >
                    <img src="images/vest/V-4237pocketbesomwithsatin.png"/>
                    <p class="suggestionname">V-4237</br>pocket besom with satin</p>
                </div>
                <div class="primarysuggestion" id="tuxedostyle_465U:collar_front_edge_use_05cm_satin" >
                    <img src="images/vest/V-4237pocketbesomwithsatin.png"/>
                    <p class="suggestionname">465U-collar, piping front edge use 0.5cm satin</p>
                </div>
                <div class="primarysuggestion" id="tuxedostyle_465U:collar_front_edge_use_05cm_satin_1" >
                    <img src="images/vest/V-4237pocketbesomwithsatin.png"/>
                    <p class="suggestionname">465U-collar, piping front edge use 0.5cm satin</p>
                </div>
			</div>
            <!--
            ----breast pocket style div
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="breastpockets" style="display:none">
                <p class="stylecaption">Breast Pocket Style</p>
                <div class="primarysuggestion" id="breastpocket_V-4185:top_patch" >
                    <img src="images/vest/V-4185Leftandrightpatchbreastpocket.png"/>
                    <p class="suggestionname">V-4185</br>Left and right patch </p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4191:top_patch_with_single_pleat" >
                    <img src="images/vest/V-4191Leftandrightpatchbreastpocketwithonepleat.png"/>
                    <p class="suggestionname">V-4191</br>Left and right patch breast pocket with one pleat</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4192:top_slant_single_besome" >
                    <img src="images/vest/V-4192Leftrightslantsinglebesombreastpocket.png"/>
                    <p class="suggestionname">V-4192</br>Left right slant single besom breast pocket</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4193:top_slant_double_besome" >
                    <img src="images/vest/V-4193Leftrightslantdoublebesombreastpocket.png"/>
                    <p class="suggestionname">V-4193</br>Left right slant double besom breastpocket</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4194:top_slant_double_besome_with_flap" >
                    <img src="images/vest/V-4194Leftrightslantdoublebesombreastpocketwithflap.png"/>
                    <p class="suggestionname">V-4194</br>Left right slant double besom breast pocket with flap</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4179:top_welt_left_welt_width_10Cm" >
                    <img src="images/vest/V-4179Weltleftbreastpocketweltwidth10CM.png"/>
                    <p class="suggestionname">V-4179</br>Welt left breast pocket breast pocket welt width 10CM</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4146:top_double_besom_with_semi_pointed_flap" >
                    <img src="images/vest/V-4146Leftandrightdoublebesombreastpocketwithsemipointedflap.png"/>
                    <p class="suggestionname">V-4146</br>Left and right double besom breast pocket with semi pointed flap</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4147:top_double_besom_with_pointed_flap" >
                    <img src="images/vest/V-4147Leftandrightdoublebesombreastpocketwithpointedflap.png"/>
                    <p class="suggestionname">V-4147</br>Left and right double besom breast pocket with pointed flap</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4148:top_double_besom_with_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4148Leftrightdoublebesombreastpocketwithflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4148</br>Left right double besom breast pocket with flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4149:top_double_besom_with_diamond_flap_with_button_and_buttonhole" >
                    <img src="images/vest/V-4149Leftrightdoublebesombreastpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4149</br>Left right double besom breast pocket with diamond flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4149:top_double_besom_with_semi_pointed_flap_pocket_with_button_and_button_hole" >
                    <img src="images/vest/V-4150Leftandrightdoublebesombreastpocketswithsemipointedflappocketbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4150</br>Left and right double besom breast pockets with semi pointed flap pocket button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4180:top_double_besom_with_pointed_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4180Leftrightdoublebesombreastpocketwithpointedflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4180</br>Left right double besom breast pocket with pointed flap button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4181:top_double_besom_with_loop_and_button" >
                    <img src="images/vest/V-4181Leftandrightdoublebesombreastpocketswithloopandbutton.png"/>
                    <p class="suggestionname">V-4181</br>Left and right double besom breast pockets with loop and button</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4182:top_double_besom_with_pointed_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-4182Leftrightdoublebesombreastpocketwithpointedtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4182</br>Left right double besom breast pocket with pointed tab button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4183:top_double_besom_with_square_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-4183Leftandrightdoublebesombreastpocketswithsquaretabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4183</br>Left and right double besom breast pockets with square tab button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4184:top_double_besom_with_round_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-4184Leftandrightdoublebesombreastpocketswithroundtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4184</br>Left and right double besom breast pockets with round tab button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4186:top_patch_with_button_and_button_hole" >
                    <img src="images/vest/V-4186Leftandrightpatchbreastpocketwithbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4186</br>Left and right patch breast pocket with button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4187:top_patch_with_flap" >
                    <img src="images/vest/V-4187Leftandrightpatchbreastpocketwithflap.png"/>
                    <p class="suggestionname">V-4187</br>Left and right patch breast pocket with flap</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4188:top_patch_with_diamond_flap" >
                    <img src="images/vest/V-4188Leftandrightpatchbreastpocketwithdiamondflap.png"/>
                    <p class="suggestionname">V-4188</br>Left and right patch breast pocket with diamond flap</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4189:top_patch_with_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4189Leftandrightpatchbreastpocketwithflapbuttonandbuttonhole.png"/>
                    <p class="suggestionname">V-4189</br>Left and right patch breast pocket with flap button and buttonhole</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4190:top_patch_with_diamond_flap_with_button_and_button_hole" >
                    <img src="images/vest/V-4190Leftrightpatchbreastpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4190</br>Left right patch breast pocket with diamond flap button buttonhole</p>
                </div>
                <div class="primarysuggestion" id="breastpocket_V-4195:top_slant_double_besom_with_diamond_shape_flap" >
                    <img src="images/vest/V-4195Leftandrightslantdoublebesombreastpocketwithdiamondshapeflap.png"/>
                    <p class="suggestionname">V-4195</br>Left and right slant double besom breast pocket with diamond shape flap</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4196:top_slant_double_besom_with_flap_and_button" >
                    <img src="images/vest/V-4196Leftandrightslantdoublebesombreastpocketwithdiamondflapandbutton.png"/>
                    <p class="suggestionname">V-4196</br>Left and right slant double besom breast pocket with diamond flap and button</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4197:top_slant_double_besom_with_diamond_flap_and_button" >
                    <img src="images/vest/V-4197Leftandrightslantdoublebesombreastpocketwithdiamondflapandbutton.png"/>
                    <p class="suggestionname">V-4197</br>Left and right slant double besom breast pocket with diamond flap and button</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4198:top_slant_double_besom_with_tap_and_button" >
                    <img src="images/vest/V-4198Leftandrightslantdoublebesombreastpocketwithtapandbutton.png"/>
                    <p class="suggestionname">V-4198</br>Left and right slant double besom breast pocket with tap and button</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4199:top_slant_double_besom_with_pointed_flap_and_button" >
                    <img src="images/vest/V-4199Leftandrightslantdoublebesombreastpocketwithpointedflapbutton.png"/>
                    <p class="suggestionname">V-4199</br>Left and right slant double besom breast pocket with pointed flap button</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4281:top_slant_double_besom_with_square_tab_and_button" >
                    <img src="images/vest/V-4281Leftandrightslantdoublebesombreastpocketwithsquaretabandbutton.png"/>
                    <p class="suggestionname">V-4281</br>Left and right slant double besom breast pocket with square tab and button</p>
                </div>
				<div class="primarysuggestion" id="breastpocket_V-4282:top_slant_double_besom_with_round_tab_with_button_and_button_hole" >
                    <img src="images/vest/V-4282Leftrightslantdoublebesombreastpocketwithroundtabbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4282</br>Left right slant double besom breast pocket with round tab button buttonhole</p>
                </div>
			</div>	
			<!--
            ----chest pocket style div
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="chestpockets" style="display:none">
                <p class="stylecaption">Chest Pocket Style</p>
                <div class="primarysuggestion" id="chestpocket_V-41C1:left_double_besom_pen_pocket" >
                    <img src="images/vest/V-41C1Leftdoublebesompenpocket.png"/>
                    <p class="suggestionname">V-41C1</br>Left double besom pen pocket</p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-41C2:left_welt_chest_pen_pocket" >
                    <img src="images/vest/V-41C2Leftweltchestpenpocket.png"/>
                    <p class="suggestionname">V-41C2</br>Left welt chest pen pocket </p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4110:left_ship_breast_pocket" >
                    <img src="images/vest/V-4110Leftshipbreastpocket%20.png"/>
                    <p class="suggestionname">V-4110</br>Left ship breast pocket </p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4111:left_single_besom_breast_pocket" >
                    <img src="images/vest/V-4111Leftsinglebesombreastpocket.png"/>
                    <p class="suggestionname">V-4111</br>Left single besom breast pocket</p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4112:left_double_besom_breast_pocket" >
                    <img src="images/vest/V-4112Leftdoublebesombreastpocket.png"/>
                    <p class="suggestionname">V-4112</br>Left double besom breastpocket</p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4113:left_double_besom_breast_pocket_with_flap" >
                    <img src="images/vest/V-4113Leftdoublebesombreastpocketwithflap.png"/>
                    <p class="suggestionname">V-4113</br>Left double besom breast pocket with flap</p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4114:left_double_besom_breast_pocket_with_diamond_flap" >
                    <img src="images/vest/V-4114Leftdoublebesombreastpocketwithdiamondflap.png"/>
                    <p class="suggestionname">V-4114</br>Left double besom breast pocket with diamond flap</p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4115:left_double_besom_breast_pocket_with_semi_pointed_flap" >
                    <img src="images/vest/V-4115Leftdoublebesombreastpocketwithsemipointedflap.png"/>
                    <p class="suggestionname">V-4115</br>Left double besom breast pocket with semi pointed flap</p>
                </div>
                <div class="primarysuggestion" id="chestpocket_V-4116:left_double_besom_breast_pocket_with_pointed_flap" >
                    <img src="images/vest/V-4116Leftdoublebesombreastpocketwithpointedflap.png"/>
                    <p class="suggestionname">V-4116</br>Left double besom breast pocket with pointed flap</p>
                </div>
				<div class="primarysuggestion" id="chestpocket_V-4117:left_double_besom_breast_pocket_with_normal_flap_button_button_hole" >
                    <img src="images/vest/V-4117Leftdoublebesombreastpocketwithnormalflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4117</br>Left double besom breast pocket with normal flap button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="chestpocket_V-4118:left_double_besom_breast_pocket_with_diamond_flap_button_and_button_hole" >
                    <img src="images/vest/V-4118Leftdoublebesombreastpocketwithdiamondflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4118</br>Left double besom breast pocket with diamond flap button buttonhole</p>
                </div>
				<div class="primarysuggestion" id="chestpocket_V-4119:left_double_besom_breast_pocket_with_semi_pointed_flap_button_and_button_hole" >
                    <img src="images/vest/V-4119Leftdoublebesombreastpocketwithsemipointedflapbuttonbuttonhole.png"/>
                    <p class="suggestionname">V-4119</br>Left double besom breast pocket with semi pointed flap button buttonhole</p>
                </div>
			</div>
            <!--
            ----thread Color
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="threadcolors" style="display:none">
                <p class="stylecaption">Thread Color</p>
                <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0">
                    <?php
                    $query = "select * from vest_option where type='thread_option' and status = '1'";
                    $result = mysql_query($query);
                    if($result){
                        $num = mysql_num_rows($result);
                        if($num>0){
                            $status = "done";
                            $a=0;
                            while($rows = mysql_fetch_array($result)){
                                $option_name = $rows['option_name'];
                                ?>
                                <div class="primarysuggestion colorsuggestion" style="height:75px;width:10%"
                                     id="threadcolor_<?php echo $option_name ?>">
                                    <img src="images/vest/<?php echo $option_name ?>.png"/>
                                    <p class="suggestionname"><?php echo $option_name ?></p>
                                </div>
                                <?php
                            }
                        }
                        else{
                            echo "No Color Found";
                        }
                    }else{
                        echo mysql_error();
                    }
                    ?>
                </div>
            </div>
            <!--
            ----Linning Option
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="linningoptions" style="display:none">
                <p class="stylecaption">Linning Option</p>
                <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="linningoptiondiv">
                    <?php
                    $query = "select * from vest_option where type='linning_option' and status = '1'";
                    $result = mysql_query($query);
                    if($result){
                        $num = mysql_num_rows($result);
                        if($num>0){
                            $status = "done";
                            $a=0;
                            while($rows = mysql_fetch_array($result)){
                                $option_name = $rows['option_name'];
                                ?>
                                <div class="primarysuggestion colorsuggestion" style="height:100px;width:19%"
                                id="linningoption_<?php echo $option_name ?>" onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','LiningOption')>
                                    <img src="images/vest/<?php echo $option_name ?>.jpg"/>
                                    <p class="suggestionname"><?php echo $option_name ?></p>
                                </div>
                                <?php
                            }
                        }
                        else{
                            echo "No Color Found";
                        }
                    }else{
                        echo mysql_error();
                    }
                    ?>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <div class="form-group col-md-6" style="margin-top:20px;padding:0">
                        <label>Fabric Name</label>
                        <input type="text" class="form-control" value="" id="linningoptionfield" placeholder="Enter Fabric Name" onkeyup="searchFabricData(this,'LiningOption')"/>
                        <ul class="list-unstyled data_ data-remove" id="list_data" style="background: #fff">
                        </ul>
                    </div>
                    <div class="form-group col-md-6" style="margin-top:20px">
                        <input type="button" class="btn btn-danger pull-right"  onclick="fnlinningoption()" style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric"/>
                    </div>
                    <div class="row">
                        <label class="surplus_price_label" id="surplus_price_labelLiningOption" style="display: none">
                            Surplus Price :
                            <span class="surplus_price" id="surplus_priceLiningOption"></span>
                        </label>
                    </div>
                </div>
            </div>
            <!--
            ----vest Fabric
            --->
            <div class="col-md-12 overflow-cmd stepdiv" id="vestfabrics" style="display:none">
                <p class="stylecaption">Vest Fabrics</p>
                <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="mainfabricdiv">
                    <?php
                    $query = "select * from wp_colors where type='suit' and status = '1'";
                    $result = mysql_query($query);
                    if($result){
                        $num = mysql_num_rows($result);
                        if($num>0){
                            $status = "done";
                            $a=0;
                            while($rows = mysql_fetch_array($result)){
                                $colorName = $rows['colorName'];
                                $displayImage = $rows['displayImage'];
                                ?>
                                <div class="primarysuggestion colorsuggestion"
                                     id="vestfabric_<?php echo $colorName ?>" onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','VestFabric')>
                                    <img src="images/jackets/<?php echo $displayImage ?>"/>
                                    <p class="suggestionname"><?php echo $colorName ?></p>
                                </div>
                                <?php
                            }
                        }else{
                            echo "No Color Found";
                        }
                    }else{
                        echo mysql_error();
                    }
                    ?>
                </div>
                <div class="col-md-12" style="padding: 0">
                    <div class="form-group col-md-6" style="margin-top:20px;padding:0">
                        <label>Fabric Name</label>
                        <input type="text" class="form-control" value="" id="mainfabricfield" placeholder="Enter Fabric Name" onkeyup="searchFabricData(this,'VestFabric');"/>
                        <ul class="list-unstyled data_ data-remove" id="list_data1" style="background: #fff">
                        </ul>
                    </div>
                    <div class="form-group col-md-6" style="margin-top:20px">
                        <input type="button" class="btn btn-danger pull-right "  onclick="fnmainfabric()" style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric1"/>
                    </div>
                    <div class="row">
                        <label class="surplus_price_label" id="surplus_price_labelVestFabric" style="display: none">
                            Surplus Price :
                            <span class="surplus_price" id="surplus_priceVestFabric"></span>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="text-align:center;margin-top: 100px">
            <div class="col-md-6"></div><label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
            <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
        </div>
        <div id ="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-4">.col-md-4</div>
                            <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <script>
            function setSurplusPrice(colorName,fabricId){
                colorName = decodeURI(colorName);
                $("#surplus_price_label"+fabricId).show();
                var url = "admin/webserver/getSurplusPrice.php";
                $.post(url, {"type":"Custom Suit","colorName":colorName},function(data){
                    var data = JSON.parse(data);
                    $("#surplus_price"+fabricId).html("$"+data.extra_price);
                });
            }

            $(".nav a").removeClass("active");
            var currPage = "";
            var category = "formal";
            var collarstyle = "V-4000:v_shape";
            var collarstyle_img = "vshape";
            var lapelbuttonhole = "V-455G:left_four_lapel_sham_button_holes";
            var lapelbuttonhole_img = "leftfourlapelshambuttonholes";
            var buttonholestyle = "V-455B:customer_appointed_fabric_for_double_besom_lapel_button_hole";
            var buttonholestyle_img = "customerappointedfabricfordoublebesomlapelbuttonhole";
            var buttonstyle = "V-401C:single_breasted_three_button";
            var buttonstyle_img = "singlebreastedthreebutton";
            var bottomstyle = "V-401J:pointed";
            var bottomstyle_img = "pointed";
            var lowerpocket = "V-4300:normal_welt";
            var lowerpocket_img = "normalwelt";
            var ticketpocket = "V-42R4:slanted_double_besom";
            var ticketpocket_img = "slanteddoublebesom";
            var backstyle = "V-4200:back_with_strap";
            var backstyle_img = "backwithstrap";
            var chestdart = "V-45C1:use_0.15_width_narrow_pick_stitch";
            var chestdart_img = "use015widthnarrowpickstitch";
            var topstitch = "V-4572:collar_lapel_0.15cm_top_stitch";
            var topstitch_img = "collarlapel015cmtopstitch";
            var placketstyle = "V-423A:not_sew_button_and_button_hole_deliver_the_button_thread_with_garment";
            var placketstyle_img = "notsewbuttonandbuttonholedeliverthebuttonthreadwithgarment";
            var tuxedostyle = "V-4221:formal_tuxedo";
            var tuxedostyle_img = "formaltuxedo";
            var breastpocket = "V-4185:top_patch";
            var breastpocket_img = "toppatch";
            var chestpocket = "V-41C1:left_double_besom_pen_pocket";
            var chestpocket_img = "leftdoublebesompenpocket";
            var threadcolor = "C3166";
            var threadcolor_img = "C3166";
            var linningoption = "FLL148";
            var linningoption_img = "FLL148";
            var vestfabric = "dbf344a";
            var shirt = "shirt";

            function getPrevValue(prevIndex){
                var result = "";
                var result_img = "";
                switch(prevIndex){
                    case "category":
                        result=category;
                        result_img=category;
                        break;
                    case "collarstyle":
                        result = collarstyle;
                        result_img=collarstyle_img;
                        break;
                    case "lapelbuttonhole":
                        result = lapelbuttonhole;
                        result_img=lapelbuttonhole_img;
                        break;
                    case "buttonholestyle":
                        result = buttonholestyle;
                        result_img=buttonholestyle_img;
                        break;
                    case "buttonstyle":
                        result = buttonstyle;
                        result_img=buttonstyle_img;
                        break;
                    case "bottomstyle":
                        result = bottomstyle;
                        result_img=bottomstyle_img;
                        break;
                    case "lowerpocket":
                        result = lowerpocket;
                        result_img=lowerpocket_img;
                        break;
                    case "ticketpocket":
                        result = ticketpocket;
                        result_img=ticketpocket_img;
                        break;
                    case "backstyle":
                        result = backstyle;
                        result_img=backstyle_img;
                        break;
                    case "chestdart":
                        result = chestdart;
                        result_img=chestdart_img;
                        break;
                    case "topstitch":
                        result = topstitch;
                        result_img=topstitch_img;
                        break;
                    case "placketstyle":
                        result = placketstyle;
                        result_img=placketstyle_img;
                        break;
                    case "tuxedostyle":
                        result = tuxedostyle;
                        result_img=tuxedostyle_img;
                        break;
                    case "breastpocket":
                        result = breastpocket;
                        result_img=breastpocket_img;
                        break;
                    case "chestpocket":
                        result = chestpocket;
                        result_img=chestpocket_img;
                        break;
                    case "linningoption":
                        result = linningoption;
                        result_img=linningoption_img;
                        break;
                    case "threadcolor":
                        result = threadcolor;
                        result_img=threadcolor_img;
                        break;
                    case "vestfabric":
                        result = vestfabric;
                        result_img=vestfabric;
                        break;
                }
                return result+"@"+result_img;
            }
            function loadSessionData() {
                $(".surplus_price_label").hide();
                $(".loader").fadeIn("slow");
                var prevIndex = $(".stepsactive").attr("id");
                currPage = prevIndex + "s";
                var item = getPrevValue(prevIndex);
                var item_img = item.split("@")[1];
                item = item.split("@")[0];
                item = prevIndex + "_" + item;
                var dataa = document.getElementById(item);
                $(dataa).addClass("active");
//                alert(item);
                if(currPage === "backstyles") {
                    folder = "back";
                    $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                    $("#breastpocketimg").attr("src","images/vest/sugg/bawli.png");
                    $("#lowerpocketimg").attr("src","images/vest/sugg/bawli.png");
                    $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+backstyle_img+".png");
                    $("#frontbuttonimg").attr("src","images/vest/sugg/bawli.png");
                    $("#frontbuttoncutimg").attr("src","images/vest/sugg/bawli.png");
                    $("#frontbuttonpointimg").attr("src","images/vest/sugg/bawli.png");
                    $(".loader").fadeOut("slow");
                }else{
                    folder = "front";
                    $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                    $("#breastpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+breastpocket_img+".png");
                    $("#lowerpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+lowerpocket_img+".png");
                    $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+collarstyle_img+"_"+buttonstyle_img+"_"+bottomstyle_img+".png");
                    $("#frontbuttonimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+".png");
                    $("#frontbuttoncutimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"cut.png");
                    $("#frontbuttonpointimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"point.png");
                    $(".loader").fadeOut("slow");
                }

            }

            $(".primarysuggestion").click(function () {
                $(".loader").fadeIn("slow");
                $(".primarysuggestion").removeClass("active");
                var value = this.id;
                $(this).addClass("active");
                var optionKeyArray = value.split("_");
                var optionKey = optionKeyArray[0];
                var cartselection = optionKeyArray[1];
                var cartselection_img = optionKeyArray[1];
                var currpage = optionKey + "s";
                if(value.indexOf(":")>=0) {
                    var temp = value.split(":");
                    optionKeyArray = temp[0].split("_");
                    optionKey = optionKeyArray[0];
                    cartselection = optionKeyArray[1] + ":" + temp[1];
                    cartselection_img = temp[1].replace(/_/g, "");
                    currpage = optionKey + "s";
                }
                switch (optionKey) {
                    case "category":
                        category = cartselection;
                        category = cartselection_img;
                        break;
                    case "collarstyle":
                        collarstyle = cartselection;
                        collarstyle_img = cartselection_img;
                        switch(collarstyle_img){
                            case "shawlwithoutcollarband":
                                collarstyle_img = "shawlwithcollarband";
                                break;
                            case "notchwithoutcollarband":
                                collarstyle_img = "notchwithcollarband";
                                break;
                            case "peakwithoutcollarband":
                                collarstyle_img = "peakwithcollarband";
                                break;
                            case "seminotchwithoutcollarband":
                                collarstyle_img = "semipeakwithcollarband";
                                break;
                            case "seminotchwithcollarband":
                                collarstyle_img = "semipeakwithcollarband";
                                break;
                            case "semipeakwithoutcollarband":
                                collarstyle_img = "semipeakwithcollarband";
                                break;
                        }
                        break;
                    case "lapelbuttonhole":
                        lapelbuttonhole = cartselection;
                        lapelbuttonhole_img = cartselection_img;
                        break;
                    case "buttonholestyle":
                        buttonholestyle = cartselection;
                        buttonholestyle_img = cartselection_img;
                        break;
                    case "buttonstyle":
                        buttonstyle = cartselection;
                        buttonstyle_img = cartselection_img;
                        break;
                    case "bottomstyle":
                        bottomstyle = cartselection;
                        bottomstyle_img = cartselection_img;
                        break;
                    case "lowerpocket":
                        lowerpocket = cartselection;
                        lowerpocket_img = cartselection_img;
                        break;
                    case "ticketpocket":
                        ticketpocket = cartselection;
                        ticketpocket_img = cartselection_img;
                        break;
                    case "backstyle":
                        backstyle = cartselection;
                        backstyle_img = cartselection_img;
                        break;
                    case "chestdart":
                        chestdart = cartselection;
                        chestdart_img = cartselection_img;
                        break;
                    case "topstitch":
                         topstitch = cartselection;
                        topstitch_img = cartselection_img;
                        break;
                    case "placketstyle":
                        placketstyle = cartselection;
                        placketstyle_img = cartselection_img;
                        break;
                    case "tuxedostyle":
                        tuxedostyle = cartselection;
                        tuxedostyle_img = cartselection_img;
                        break;
                    case "breastpocket":
                        breastpocket = cartselection;
                        breastpocket_img = cartselection_img;
                        break;
                    case "chestpocket":
                        chestpocket = cartselection;
                        chestpocket_img = cartselection_img;
                        break;
                    case "linningoption":
                        linningoption = cartselection;
                        linningoption_img = cartselection_img;
                        break;
                    case "threadcolor":
                        threadcolor = cartselection;
                        threadcolor_img = cartselection_img;
                        break;
                    case "vestfabric":
                        vestfabric = cartselection;
                        vestfabric = cartselection_img;
                        break;
                }
                if(currpage == "backstyles") {
                    folder = "back";
                    $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                    $("#breastpocketimg").attr("src","images/vest/sugg/bawli.png");
                    $("#lowerpocketimg").attr("src","images/vest/sugg/bawli.png");
                    $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+backstyle_img+".png");
                    $("#frontbuttonimg").attr("src","images/vest/sugg/bawli.png");
                    $("#frontbuttoncutimg").attr("src","images/vest/sugg/bawli.png");
                    $("#frontbuttonpointimg").attr("src","images/vest/sugg/bawli.png");
                    $(".loader").fadeOut("slow");
                }
                else{
                    folder = "front";
                    $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                    $("#breastpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+breastpocket_img+".png");
                    $("#lowerpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+lowerpocket_img+".png");
                    $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+collarstyle_img+"_"+buttonstyle_img+"_"+bottomstyle_img+".png");
                    $("#frontbuttonimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+".png");
                    $("#frontbuttoncutimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"cut.png");
                    $("#frontbuttonpointimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"point.png");
                    $(".loader").fadeOut("slow");
                }


                /*
                 * data store all values
                 * */

                var url = 'admin/webserver/selectionData.php';

                $.post(url,{currPage:currPage,vestcategory:category,collarstyle:collarstyle,collarstyle_img:collarstyle_img,lapelbuttonhole:lapelbuttonhole,
                    lapelbuttonhole_img:lapelbuttonhole_img,buttonholestyle:buttonholestyle,buttonholestyle_img:buttonholestyle_img,buttonstyle:buttonstyle,
                    buttonstyle_img:buttonstyle_img,bottomstyle:bottomstyle,bottomstyle_img:bottomstyle_img,lowerpocket:lowerpocket,lowerpocket_img:lowerpocket_img,
                    ticketpocket:ticketpocket,ticketpocket_img:ticketpocket_img,backstyle:backstyle,backstyle_img:backstyle_img,chestdart:chestdart,chestdart_img:chestdart_img,
                    topstitch:topstitch,topstitch_img:topstitch_img,placketstyle:placketstyle,placketstyle_img:placketstyle_img,tuxedostyle:tuxedostyle,tuxedostyle_img:tuxedostyle_img,
                    breastpocket:breastpocket,breastpocket_img:breastpocket_img,threadcolor:threadcolor,threadcolor_img:threadcolor_img,linningoption:linningoption,
                    linningoption_img:linningoption_img,vestfabric:vestfabric,shirt:shirt

                },function(data){
                    console.log('data --- '+data);
                });

            });
            var count = 0;
            function fnmainfabric(){
                var mainfabricfield = $("#mainfabricfield").val();
                if(mainfabricfield != ""){
                    setSurplusPrice(encodeURI(mainfabricfield),'VestFabric');
                    var newdiv="<div class='primarysuggestion colorsuggestion' onclick=selectFabric('vestfabric',this);setSurplusPrice('"+encodeURI(mainfabricfield)+"','VestFabric'); " +
                    "id='vestfabric_"+mainfabricfield+"'><p class='suggestionname'style='line-height:110px'>"+
                    mainfabricfield+"</p></div>";
                    $("#mainError").remove();
                    $("#mainfabricdiv").append(newdiv);
                }else{
                    if(count == 0) {
                        count++;
                        $("#mainfabricdiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;" +
                        "margin:20px 0 -20px 0' id='mainError'>Please Enter Fabric Name First</p>");
                    }
                }
                $('#mainfabricfield').val('');
            }

            var count2=0;
            function fnlinningoption(){
                var linningoptionfield = $("#linningoptionfield").val();
                if(linningoptionfield != ""){
                    setSurplusPrice(encodeURI(linningoptionfield),'LiningOption');
                    var newdiv="<div class='primarysuggestion colorsuggestion' onclick=selectFabric('linningoption',this);setSurplusPrice('"+encodeURI(linningoptionfield)+"','LiningOption'); " +
                    "id='linningoption_"+linningoptionfield+"' style='height:100px!important;width:19%!important'>" +
                    "<p class='suggestionname'style='line-height:100px'>"+linningoptionfield+"</p></div>";
                    $("#linningoptionError").remove();
                    $("#linningoptiondiv").append(newdiv);

                }else{
                    if(count2 == 0) {
                        count2++;
                        $("#linningoptiondiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;" +
                        "margin:20px 0 -20px 0' id='linningoptionError'>Please Enter Fabric Name First</p>");
                    }
                }
                $('#linningoptionfield').val('');

            }
            function selectFabric(type,obj){
                $(".primarysuggestion").removeClass("active");
                $("#"+obj.id).addClass("active");
                if(type == "vestfabric") {
                    vestfabric = (obj.id).split("_")[1];
                }else if(type == "linningoption"){
                    linningoption = (obj.id).split("_")[1];
                }
            }
            function navigation(switcher) {
                $("#"+currPage).fadeOut("slow");
                if (switcher == "next") {
                    if (currPage == "categorys") {
                        $(".steps").removeClass("stepsactive");
                        $("#collarstyles").fadeIn("slow");
                        $("#collarstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "collarstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#lapelbuttonholes").fadeIn("slow");
                        $("#lapelbuttonhole").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "lapelbuttonholes") {
                        $(".steps").removeClass("stepsactive");
                        $("#buttonholestyles").fadeIn("slow");
                        $("#buttonholestyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "buttonholestyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#buttonstyles").fadeIn("slow");
                        $("#buttonstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "buttonstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#bottomstyles").fadeIn("slow");
                        $("#bottomstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "bottomstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#lowerpockets").fadeIn("slow");
                        $("#lowerpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "lowerpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#ticketpockets").fadeIn("slow");
                        $("#ticketpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "ticketpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#backstyles").fadeIn("slow");
                        $("#backstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "backstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#chestdarts").fadeIn("slow");
                        $("#chestdart").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "chestdarts") {
                        $(".steps").removeClass("stepsactive");
                        $("#topstitchs").fadeIn("slow");
                        $("#topstitch").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "topstitchs") {
                        $(".steps").removeClass("stepsactive");
                        $("#placketstyles").fadeIn("slow");
                        $("#placketstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "placketstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#tuxedostyles").fadeIn("slow");
                        $("#tuxedostyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "tuxedostyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#breastpockets").fadeIn("slow");
                        $("#breastpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "breastpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#chestpockets").fadeIn("slow");
                        $("#chestpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "chestpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#threadcolors").fadeIn("slow");
                        $("#threadcolor").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "threadcolors") {
                        $(".steps").removeClass("stepsactive");
                        $("#linningoptions").fadeIn("slow");
                        $("#linningoption").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "linningoptions") {
                        $(".steps").removeClass("stepsactive");
                        $("#vestfabrics").fadeIn("slow");
                        $("#vestfabric").addClass("stepsactive");
                        $("#next").html("Add to Cart");
                        loadSessionData();
                    }
                    else if (currPage == "vestfabrics") {
                        var fabricCode = vestfabric;
                        if(fabricCode.indexOf(":") >0 ){
                            fabricCode = fabricCode.split(":")[0];
                        }
                        var url = "api/registerUser.php";
                        $.post(url,{"type":"checkFabricStatus","fabricCode":fabricCode},function(fabricdata) {
                            var status = fabricdata.Status;
                            var message = fabricdata.Message;
                            if (status == "Success") {
                                var user_id = $("#user_id").val();
                                var randomnum = Math.floor((Math.random() * 600000) + 1);
                                var product_name = "CustomVest" + randomnum;
                                var url = "admin/webserver/addto_cart.php?product_type=Custom Vest&product_name=" + product_name +
                                    "&category="+category+"&collarstyle="+collarstyle+"&lapelbuttonhole="+lapelbuttonhole+
                                    "&buttonholestyle="+buttonholestyle+"&buttonstyle="+buttonstyle+"&bottomstyle="+bottomstyle+
                                    "&lowerpocket="+lowerpocket+"&ticketpocket="+ticketpocket+"&backstyle="+backstyle+
                                    "&chestdart="+chestdart+"&topstitch="+topstitch+"&placketstyle="+placketstyle+
                                    "&tuxedostyle="+tuxedostyle+"&breastpocket="+breastpocket+"&chestpocket="+chestpocket+
                                    "&threadcolor="+threadcolor+"&linningoption="+linningoption+"&vestfabric="+vestfabric+
                                    "&user_id=" + user_id + "&quantity=1";
                                $.get(url, function (data) {
                                    var json = $.parseJSON(data);
                                    var status = json.status;
                                    if (status == "done") {
                                        window.location = "cart.php";
                                    }
                                });
                            }
                            else {
                                showMessages(message,"red");
                                $("#vestfabrics").fadeIn("slow");
                                $(".colorsuggestion ").removeClass("active");
                                $("#vestfabric_dbk053a").addClass("active");
                                $("#vestfabric_dbk053a").trigger("click");
                                vestfabric ='dbk053a';
                            }
                        });
                    }
                }
                else if (switcher == "previous") {
                    if (currPage == "categorys") {
                        window.location = "customize.php";
                    }
                    else if (currPage == "collarstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#categorys").fadeIn("slow");
                        $("#category").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "lapelbuttonholes") {
                        $(".steps").removeClass("stepsactive");
                        $("#collarstyles").fadeIn("slow");
                        $("#collarstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "buttonholestyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#lapelbuttonholes").fadeIn("slow");
                        $("#lapelbuttonhole").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "buttonstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#buttonholestyles").fadeIn("slow");
                        $("#buttonholestyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "bottomstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#buttonstyles").fadeIn("slow");
                        $("#buttonstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "lowerpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#bottomstyles").fadeIn("slow");
                        $("#bottomstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "ticketpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#lowerpockets").fadeIn("slow");
                        $("#lowerpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "backstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#ticketpockets").fadeIn("slow");
                        $("#ticketpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "chestdarts") {
                        $(".steps").removeClass("stepsactive");
                        $("#backstyles").fadeIn("slow");
                        $("#backstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "topstitchs") {
                        $(".steps").removeClass("stepsactive");
                        $("#chestdarts").fadeIn("slow");
                        $("#chestdart").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "placketstyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#topstitchs").fadeIn("slow");
                        $("#topstitch").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "tuxedostyles") {
                        $(".steps").removeClass("stepsactive");
                        $("#placketstyles").fadeIn("slow");
                        $("#placketstyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "breastpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#tuxedostyles").fadeIn("slow");
                        $("#tuxedostyle").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "chestpockets") {
                        $(".steps").removeClass("stepsactive");
                        $("#breastpockets").fadeIn("slow");
                        $("#breastpocket").addClass("stepsactive");
                        loadSessionData();
                    }
                    else if (currPage == "vestfabrics") {
                        $(".steps").removeClass("stepsactive");
                        $("#chestpockets").fadeIn("slow");
                        $("#chestpocket").addClass("stepsactive");
                        $("#next").html("Next");
                        loadSessionData();
                    }
                }
                storeData();
            }
            function showMessages(message, color) {
                $(".modal-header").css({"display": "none"});
                $(".modal-body").css({"display": "block"});
                $(".modal-body").css({"padding-top": "0px"});
                $(".modal-body").html("<div class='row' style='background:#000;'><h4 style='color:#fff;text-align: center" +
                    ";padding:10px;font-size: 18px;' >" +
                    "Fabric out of stock !!!</h4></div><div class='row'><h4 style='text-align: center;font-size: 18px;margin: 14px 0;'>"
                    +message+"</h4></div>" +
                    "<div class='row' style='text-align: center'><input type='button' data-dismiss='modal' " +
                    "style='padding:6px;margin:6px 0;text-align: center;font-size: 16px;width: 200px;text-transform: uppercase;" +
                    "background: #D9534F;border: none;color:#fff;" +
                    "' value='ok'" +
                    " /></div> ");
                $(".modal-footer").css({"display": "none"});
                $("#modal").modal("show");
            }
            function storeData() {
                var value = $(".stepsactive").attr("id");

                var optionKeyArray = value.split("_");
                var optionKey = optionKeyArray[0];
                var cartselection = optionKeyArray[1];
                var cartselection_img = optionKeyArray[1];
                var currpage = optionKey + "s";
                if(value.indexOf(":")>=0) {
                    var temp = value.split(":");
                    optionKeyArray = temp[0].split("_");
                    optionKey = optionKeyArray[0];
                    cartselection = optionKeyArray[1] + ":" + temp[1];
                    cartselection_img = temp[1].replace(/_/g, "");
                    currpage = optionKey + "s";
                }
                switch (optionKey) {
                    case "category":
                        category = cartselection;
                        category = cartselection_img;
                        break;
                    case "collarstyle":
                        collarstyle = cartselection;
                        collarstyle_img = cartselection_img;
                        switch(collarstyle_img){
                            case "shawlwithoutcollarband":
                                collarstyle_img = "shawlwithcollarband";
                                break;
                            case "notchwithoutcollarband":
                                collarstyle_img = "notchwithcollarband";
                                break;
                            case "peakwithoutcollarband":
                                collarstyle_img = "peakwithcollarband";
                                break;
                            case "seminotchwithoutcollarband":
                                collarstyle_img = "semipeakwithcollarband";
                                break;
                            case "seminotchwithcollarband":
                                collarstyle_img = "semipeakwithcollarband";
                                break;
                            case "semipeakwithoutcollarband":
                                collarstyle_img = "semipeakwithcollarband";
                                break;
                        }
                        break;
                    case "lapelbuttonhole":
                        lapelbuttonhole = cartselection;
                        lapelbuttonhole_img = cartselection_img;
                        break;
                    case "buttonholestyle":
                        buttonholestyle = cartselection;
                        buttonholestyle_img = cartselection_img;
                        break;
                    case "buttonstyle":
                        buttonstyle = cartselection;
                        buttonstyle_img = cartselection_img;
                        break;
                    case "bottomstyle":
                        bottomstyle = cartselection;
                        bottomstyle_img = cartselection_img;
                        break;
                    case "lowerpocket":
                        lowerpocket = cartselection;
                        lowerpocket_img = cartselection_img;
                        break;
                    case "ticketpocket":
                        ticketpocket = cartselection;
                        ticketpocket_img = cartselection_img;
                        break;
                    case "backstyle":
                        backstyle = cartselection;
                        backstyle_img = cartselection_img;
                        break;
                    case "chestdart":
                        chestdart = cartselection;
                        chestdart_img = cartselection_img;
                        break;
                    case "topstitch":
                        topstitch = cartselection;
                        topstitch_img = cartselection_img;
                        break;
                    case "placketstyle":
                        placketstyle = cartselection;
                        placketstyle_img = cartselection_img;
                        break;
                    case "tuxedostyle":
                        tuxedostyle = cartselection;
                        tuxedostyle_img = cartselection_img;
                        break;
                    case "breastpocket":
                        breastpocket = cartselection;
                        breastpocket_img = cartselection_img;
                        break;
                    case "chestpocket":
                        chestpocket = cartselection;
                        chestpocket_img = cartselection_img;
                        break;
                    case "linningoption":
                        linningoption = cartselection;
                        linningoption_img = cartselection_img;
                        break;
                    case "threadcolor":
                        threadcolor = cartselection;
                        threadcolor_img = cartselection_img;
                        break;
                    case "vestfabric":
                        vestfabric = cartselection;
                        vestfabric = cartselection_img;
                        break;
                }
                savetoSession('tab',optionKey);
            }

            function rotateImage(button) {
                var folder = "front";
                $(".loader").fadeIn("slow");
                var source=[];
                source.push($("#shirtimg").attr("src"));
                source.push($("#breastpocketimg").attr("src"));
                source.push($("#lowerpocketimg").attr("src"));
                source.push($("#vestimg").attr("src"));
                source.push($("#frontbuttonimg").attr("src"));
                source.push($("#frontbuttoncutimg").attr("src"));
                source.push($("#frontbuttonpointimg").attr("src"));
                for(var i = 0;i<source.length;i++) {
                    if (source[i].indexOf("front/") >=0){
                        if (button == "left") {
                            folder = "back";
                            $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                            $("#breastpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#lowerpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+backstyle_img+".png");
                            $("#frontbuttonimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttoncutimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttonpointimg").attr("src","images/vest/sugg/bawli.png");
                        }
                        else if (button == "right") {
                            folder = "side";
                            $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                            $("#breastpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#lowerpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/vest.png");
                            $("#frontbuttonimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttoncutimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttonpointimg").attr("src","images/vest/sugg/bawli.png");
                        }
                    }
                    else if (source[i].indexOf("side/") >= 0) {
                        if (button == "left") {
                            folder = "front";
                            $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                            $("#breastpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+breastpocket_img+".png");
                            $("#lowerpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+lowerpocket_img+".png");
                            $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+collarstyle_img+"_"+buttonstyle_img+"_"+bottomstyle_img+".png");
                            $("#frontbuttonimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+".png");
                            $("#frontbuttoncutimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"cut.png");
                            $("#frontbuttonpointimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"point.png");
                        }
                        else if (button == "right") {
                            folder = "back";
                            $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                            $("#breastpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#lowerpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+backstyle_img+".png");
                            $("#frontbuttonimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttoncutimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttonpointimg").attr("src","images/vest/sugg/bawli.png");
                        }
                    }
                    else if (source[i].indexOf("back/") >= 0) {
                        if (button == "left") {
                            folder = "side";
                            $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                            $("#breastpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#lowerpocketimg").attr("src","images/vest/sugg/bawli.png");
                            $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/vest.png");
                            $("#frontbuttonimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttoncutimg").attr("src","images/vest/sugg/bawli.png");
                            $("#frontbuttonpointimg").attr("src","images/vest/sugg/bawli.png");
                        }
                        else if (button == "right") {
                            folder = "front";
                            $("#shirtimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+shirt+".png");
                            $("#breastpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+breastpocket_img+".png");
                            $("#lowerpocketimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+lowerpocket_img+".png");
                            $("#vestimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+collarstyle_img+"_"+buttonstyle_img+"_"+bottomstyle_img+".png");
                            $("#frontbuttonimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+".png");
                            $("#frontbuttoncutimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"cut.png");
                            $("#frontbuttonpointimg").attr("src", "images/vest/sugg/" + vestfabric + "/" + folder + "/"+buttonstyle_img+"point.png");
                        }
                    }
                }
                $(".loader").fadeOut("slow");
            }
            $(".steps").click(function(){
                savetoSession('tab',this.id);
                $("#"+currPage).hide(1000);
                var newcurrpage = this.id+"s";
                $(".steps").removeClass("stepsactive");
                $("#"+this.id).addClass("stepsactive");
                $("#"+newcurrpage).show(1000);
                currPage = newcurrpage;
                loadSessionData();
                if(currPage == "vestfabrics"){
                    $("#next").html("Add to Cart");
                }
                else{
                    $("#next").html("Next");
                }
            });
            var shirtimg_src;
            var breastpocketimg_src;
            var lowerpocketimg_src;
            var vestimg_src;
            var frontbuttonimg_src;
            var frontbuttoncutimg_src;
            var frontbuttonpointimg_src;
            $(".outputsugg").load(function(){
                switch(this.id){
                    case "shirtimg":
                        shirtimg_src = $("#shirtimg").attr('src');
                        break;
                    case "breastpocketimg":
                        breastpocketimg_src = $("#breastpocketimg").attr('src');
                        break;
                    case "lowerpocketimg":
                        lowerpocketimg_src = $("#lowerpocketimg").attr('src');
                        break;
                    case "vestimg":
                        vestimg_src = $("#vestimg").attr('src');
                        break;
                    case "frontbuttonimg":
                        frontbuttonimg_src = $("#frontbuttonimg").attr('src');
                        break;
                    case "frontbuttoncutimg":
                        frontbuttoncutimg_src = $("#frontbuttoncutimg").attr('src');
                        break;
                    case "frontbuttonpointimg":
                        frontbuttonpointimg_src = $("#frontbuttonpointimg").attr('src');
                        break;
                }
            });
            $(".outputsugg").error(function(){
                switch(this.id){
                    case "shirtimg":
                        $("#shirtimg").attr('src',shirtimg_src);
                        break;
                    case "breastpocketimg":
                        $("#breastpocketimg").attr('src',breastpocketimg_src);
                        break;
                    case "lowerpocketimg":
                        $("#lowerpocketimg").attr('src',lowerpocketimg_src);
                        break;
                    case "vestimg":
                        $("#vestimg").attr('src',vestimg_src);
                        break;
                    case "frontbuttonimg":
                        $("#frontbuttonimg").attr('src',frontbuttonimg_src);
                        break;
                    case "frontbuttoncutimg":
                        $("#frontbuttoncutimg").attr('src',frontbuttoncutimg_src);
                        break;
                    case "frontbuttonpointimg":
                        $("#frontbuttonpointimg").attr('src',frontbuttonpointimg_src);
                        break;
                }
            });

            function fabricSelected(id,value) {
                $('#'+id).val(value);

                if(id == 'linningoptionfield') {
                    $("#findFabric").attr("disabled",false);
                    $('#list_data').addClass('data-remove');
                }
                else{
                    $("#findFabric1").attr("disabled",false);
                    $('#list_data1').addClass('data-remove');
                }
            }
            function searchFabricData(ref) {
                var value = $('#'+ref.id).val();
                if(ref.id === 'linningoptionfield'){
                    $("#findFabric").attr("disabled",true);
                }
                else {
                    $("#findFabric1").attr("disabled", true);
                }
                var li_='';
                var url = "admins/api/fabricProcess.php";
                $.post(url,{'dataType':'searchFabricData','cameo_code':value},function(response){
                    var results = [];
                    var status = response.Status;
                    var message = response.Message;
                    if(status === 'Success' ) {
                        results = [];
                        var dataa = response.data;
                        li_ = '';
                        for(var i=0;i<dataa.length;i++) {
                            li_ = li_ + '<li onclick=fabricSelected("' + ref.id + '","' + dataa[i].fabric_code + '")>' + dataa[i].fabric_code + '</li>';
                        }
                        if(ref.id === 'linningoptionfield') {
                            $('#list_data').removeClass('data-remove');
                            $('#list_data').html(li_);
                        }
                        else{
                            $('#list_data1').removeClass('data-remove');
                            $('#list_data1').html(li_);
                        }
                    }
                    else if(status === 'Failure') {
                        li_ = li_+'<li>No data found</li>';
                        if(ref.id === 'linningoptionfield') {
                            $('#list_data').removeClass('data-remove');
                            $('#list_data').html(li_);
                        }
                        else{
                            $('#list_data1').removeClass('data-remove');
                            $('#list_data1').html(li_);
                        }
                    }
                });
            }
            window.onclick = function () {
                if(!$("#list_data").hasClass('data-remove')) {
                    $("#list_data").addClass('data-remove');
                }
                if(!$("#list_data1").hasClass('data-remove')) {
                    $("#list_data1").addClass('data-remove');
                }
            };
//            getSession();
              loadSessionData();

            function savetoSession(type,content_id) {
                var url = "admin/webserver/selectionData.php?sessionType=storeSession&type=custom_vests&content_id="+content_id;
                $.get(url,function(data){
                    console.log('response --- '+data);
                });
            }

            function getSession() {
                var url = "admin/webserver/selectionData.php?type=custom_vests&sessionType=getSession";
                currPage = "category";
                $.get(url,function(data) {
                    data = $.parseJSON(data);
                    if(data.status ==='done') {
                        currPage = data.content;
                        if(data.vestcategory!=="")
                            category = data.vestcategory;
                        if(data.collarstyle!=="")
                            collarstyle = data.collarstyle;
                        if(data.collarstyle_img!=="")
                            collarstyle_img = data.collarstyle_img;
                        if(data.lapelbuttonhole!=="")
                            lapelbuttonhole = data.lapelbuttonhole;
                        if(data.lapelbuttonhole_img!=="")
                            lapelbuttonhole_img = data.lapelbuttonhole_img;
                        if(data.buttonholestyle!=="")
                            buttonholestyle = data.buttonholestyle;
                        if(data.buttonholestyle_img!=="")
                            buttonholestyle_img = data.buttonholestyle_img;
                        if(data.bottomstyle!=="")
                            bottomstyle = data.bottomstyle;
                        if(data.bottomstyle_img!=="")
                            bottomstyle_img = data.bottomstyle_img;
                        if(data.lowerpocket!=="")
                            lowerpocket = data.lowerpocket;
                        if(data.lowerpocket_img!=="")
                            lowerpocket_img = data.lowerpocket_img;
                        if(data.ticketpocket!=="")
                            ticketpocket = data.ticketpocket;
                        if(data.ticketpocket_img!=="")
                            ticketpocket_img = data.ticketpocket_img;
                        if(data.backstyle!=="")
                            backstyle = data.backstyle;
                        if(data.backstyle_img!=="")
                            backstyle_img = data.backstyle_img;
                        if(data.chestdart!=="")
                            chestdart = data.chestdart;
                        if(data.chestdart_img!=="")
                            chestdart_img = data.chestdart_img;
                        if(data.topstitch!=="")
                            topstitch = data.topstitch;
                        if(data.topstitch_img!=="")
                            topstitch_img = data.topstitch_img;
                        if(data.placketstyle!=="")
                            placketstyle = data.placketstyle;
                        if(data.placketstyle_img!=="")
                            placketstyle_img = data.placketstyle_img;
                        if(data.tuxedostyle!=="")
                            tuxedostyle = data.tuxedostyle;
                        if(data.tuxedostyle_img!=="")
                            tuxedostyle_img = data.tuxedostyle_img;
                        if(data.breastpocket!=="")
                            breastpocket = data.breastpocket;
                        if(data.breastpocket_img!=="")
                            breastpocket_img = data.breastpocket_img;
                        if(data.chestpocket!=="")
                            chestpocket = data.chestpocket;
                        if(data.chestpocket_img!=="")
                            chestpocket_img = data.chestpocket_img;
                        if(data.threadcolor!=="")
                            threadcolor = data.threadcolor;
                        if(data.threadcolor_img!=="")
                            threadcolor_img = data.threadcolor_img;
                        if(data.linningoption!=="")
                            linningoption = data.linningoption;
                        if(data.linningoption_img!=="")
                            linningoption_img = data.linningoption_img;
                        if(data.vestfabric!=="")
                            vestfabric = data.vestfabric;
                        if(data.shirt!=="")
                            shirt = data.shirt;
                    }


                    $(".steps").removeClass("stepsactive");
                    $(".stepdiv").fadeOut("slow");
                    $("#"+currPage+"s").fadeIn("slow");
                    $("#"+currPage).addClass("stepsactive");
                    loadSessionData();


                });
            }
        </script>
    </div>