<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 05-12-2017
 * Time: 11:33
 */
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
require_once('Classes/MESSAGE.php');
require_once('Classes/CONNECT.php');

$msgClass = new \Classes\MESSAGE();
$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure') {
    $msgClass->apiResponse($response);
    return false;
}

$type = $_REQUEST['type'];

if($type === 'saveMessage') {

    $requiredfields = array('msg_text','msg_desc','msg_signature','msg_broadcast_date');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $msgClass->apiResponse($response);
        return false;
    }
    $msgClass->setMsgText($_REQUEST['msg_text']);
    $msgClass->setMsgDesc($_REQUEST['msg_desc']);
    $msgClass->setMsgSignature($_REQUEST['msg_signature']);
    $msgClass->setMsgAttachment('');
    $msgClass->setMsgBroadcastDate($_REQUEST['msg_broadcast_date']);

    if(!isset($_REQUEST['msg_id'])) {
        $response = $msgClass->save();
    }
    else{
        $msgClass->setMsgId($_REQUEST['msg_id']);
        $response = $msgClass->update();
    }
    $msgClass->apiResponse($response);

}
else if($type === "allMessages") {
    $response = $msgClass->getAllMessages();
    $msgClass->apiResponse($response);
}
else if($type === 'removeMessage') {
    $requiredfields = array('msg_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $msgClass->apiResponse($response);
        return false;
    }
    $msgClass->setMsgId($_REQUEST['msg_id']);
    $response = $msgClass->removeMessage();
    $msgClass->apiResponse($response);
}
else if($type === 'sendMessage') {
    $requiredfields = array('ids','msg','subject');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $msgClass->apiResponse($response);
        return false;
    }
    $ids = $_REQUEST['ids'];
    $msg = $_REQUEST['msg'];
    $subject = $_REQUEST['subject'];
    $response = $msgClass->sendMessages($ids,$msg,$subject);
     $msgClass->apiResponse($response);
}


?>