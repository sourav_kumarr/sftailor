<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
<style>
    .cop-price {
        float: left;
        height: 28px;
        width: 55px;
    }

    .couponForCheck {
        margin: 0 10px !important;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .x_title h2 {
        white-space: unset;
    }
</style>
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<!-- page content -->
<div class="right_col" role="main">
    <!-- top tiles -->
    <div class="row tile_count">

    </div>
    <div class="row" role="main">
        <div class="">
            <div class="clearfix"></div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>
                                <span style="color:#1ABB9C;"></span>
                                Gift Certificate
                                <small></small>
                            </h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <div class="form-group form-inline">
                                        <input type="button" class="btn btn-danger btn-sm" onclick="addGiftbox()"
                                               value=" + Add Gift Certificate" />
                                    </div>
                                </li>
                            </ul>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content">
                            <p class="text-muted font-13 m-b-30">
                                From here admin can manage/modify the content of the Gift Certificate
                            </p>
                            <div id="styleboxData"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- /page content -->
<?php
include('footer.php');
?>
<style>
    .stylebox_img{
        height: 100px;
        width: 100px;
        border: 1px solid #ccc;
        margin-top: 1px;
    }
    .old_stylebox_img{
        height: 100px;
        width: 100px;
        border: 1px solid #ccc;
    }
    .filebtn{
        height: 100px;
        width: 100px;
        position: absolute;
        top: 24px;
        opacity: 0;
    }
</style>
<script>
    function addGiftbox(){
        $(".modal-title").html("<label style='color: #c9302c;font-size: 16px;'>Add New Gift Certificate</label>");
        $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='styleboxError'" +
            " style='color:red'></p><div class='col-md-6 form-group'><label>Gift certificate for:</label>" +
            "<input type='text' class='form-control' id='title' placeholder='Enter Title Gift for' /></div>" +
            "<div class='col-md-6 form-group'><label>Gift Code</label>" +
            "<input type='text' id='code' class='form-control' placeholder='Enter Applied Gift Code' /></div>" +
            "<div class='col-md-12 form-group'><label>Gift Certificate Description</label>" +
            "<textarea rows='2' placeholder='Enter Description' class='form-control' id='description' style='height: 180px;overflow: hidden'>" +
            "</textarea></div><div class='col-md-6 form-group'><label>Please Select Gift Certificate Image</label><br>" +
            "<img src='images/gift_marvel.gif' alt='Select Image' class='stylebox_img' /><input type='file' " +
            "name='styleboxfile' id='stylebox_file' onchange=readURL2(this,'stylebox_img') class='filebtn' /></div><div class='col-md-6 form-group'><label>Gift Price:</label>" +
            "<input type='text' class='form-control' id='price' placeholder='Enter Price' /></div>" +
            "<div class='col-md-12'><input type='button' class='btn btn-danger pull-right' style='margin-top:30px' " +
            "onclick=loadTextArea('') id='subbtn' value='Submit'/></div></div><p class='percentprog' " +
            "style='color:green;text-align:center'></div>");
        $(".modal-footer").css('display', 'none');
        $("#myModal").modal("show");
    }
    function readURL2(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                //oldEventImagechanged = "yes";
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function loadTextArea(g_id){
        $(".loaderdiv").addClass("block");
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].updateElement();
        }
        setTimeout(function(){
            if(g_id === ""){
                addStyleboxFinal()
            }else {
                updateStylebox(g_id)
            }
        },1000);
    }
    function addStyleboxFinal(){
        var title = $("#title").val();
        var code = $("#code").val();
        var price = $("#price").val();
        var description = $("#description").val();
        var stylebox_file = $("#stylebox_file").val();
        if(title === "" || code === "" || price ==="" || description === "" || stylebox_file === ""){
            $("#styleboxError").html("Please fill all required fields");
            $("#styleboxError").css("color","red");
            return false;
        }
        var data = new FormData();
        var stylebox_file_obj = document.getElementById('stylebox_file');
        var stylebox_file_ext = stylebox_file.split(".");
        stylebox_file_ext = stylebox_file_ext[stylebox_file_ext.length - 1];
        if (stylebox_file_ext === "jpg" || stylebox_file_ext === "png" || stylebox_file_ext === "gif" || stylebox_file_ext === "jpeg") {
            data.append('stylebox_file', stylebox_file_obj.files[0]);
        }
        else{
            $("#styleboxError").html("Image File Should Be JPG, PNG, GIF Formats Only");
            $("#styleboxError").css("color","red");
        }
        data.append('type', "addGiftbox");
        data.append('title', title);
        data.append('description', description);
        data.append('code', code);
        data.append('price', price);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState === 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status === "Success"){
                    $("#styleboxError").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else{
                    $("#styleboxError").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/giftProcess.php');
        request.send(data);
    }
    function getGiftboxData(){
        var url = "api/giftProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "getGiftboxData"}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var StyleboxData = data.GiftboxData;
            if (Status === "Success") {
                var tbodyData ="";
                var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                    '<thead><tr><th>#</th><th>Image</th><th>Gift For</th><th>Gift Code</th><th>Description</th><th>Price</th><th>Action</th></tr></thead>';
                for(var a=0;a<StyleboxData.length;a++){
                    var styleboxImg = StyleboxData[a].gift_image;
                    if(styleboxImg === ""){
                        styleboxImg = "images/suit-accessories.jpg";
                    }else{
                        styleboxImg = "api/Files/images/giftbox/"+styleboxImg;
                    }
                    tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td><img src='"+styleboxImg+"' style='height:40px' /></td>" +
                        "<td>"+StyleboxData[a].gift_for+"</td><td>"+StyleboxData[a].gift_code+"</td><td>"+StyleboxData[a].gift_description+"</td><td>"+StyleboxData[a].gift_price+"</td>" +
                        "<td><a  onclick=confirmDelete_stylebox('"+StyleboxData[a].g_id+"'); style='float:right;font-size:18px;" +
                        "padding:2px 4px;background: red;color:white;margin-left: 5px'><i class='fa fa-trash-o'></i></a>" +
                        "<a  onclick=editGiftbox('"+StyleboxData[a].g_id+"'); style='float: right; font-size:18px;" +
                        "padding:2px 4px; background:red;color: white'><i class='fa fa-pencil'></i></a></td></tr>";
                }
                $("#styleboxData").html(table+"<tbody>"+tbodyData+"</tbody></table>");
                $("#catTable").dataTable();
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                $("#styleboxData").html("<p class='text-center'><label>No Data Found</label></p>");
            }
        });
    }
    getGiftboxData();
    function editGiftbox(g_id){
        var url = "api/giftProcess.php";
        $.post(url, {"type": "editGiftbox", "g_id": g_id}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var styleboxData = data.giftData;
            if (Status === "Success") {
                $(".modal-title").html("<label style='color: green'>Update Gift Certificate Box</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='styleboxError'" +
                    " style='color:red'></p><div class='col-md-6 form-group'><label>Gift certificate for:</label>" +
                    "<input type='text' class='form-control' id='e_title' value='"+styleboxData.gift_for+"' " +
                    "placeholder='Enter Gift For' /></div><div class='col-md-6 form-group'><label>Gift Code</label>" +
                    "<input type='text' id='e_code' class='form-control' placeholder='Enter Gift Code' " +
                    "value='"+styleboxData.gift_code+"' /></div><div class='col-md-12 form-group'><label>Gift Certificate Description</label>" +
                    "<textarea name='editor1' rows='2' placeholder='Enter Description' class='form-control' " +
                    "id='description1'>"+styleboxData.gift_description+"</textarea></div><div class='col-md-6 form-group'>" +
                    "<label>Gift Certificate Image</label><br><img src='api/Files/images/giftbox/"+styleboxData.gift_image+"' " +
                    "alt='StyleBox Image' class='old_stylebox_img' /><input type='file' name='styleboxfile' " +
                    "id='stylebox_file2' onchange=readURL2(this,'old_stylebox_img') class='filebtn' /></div><div class='col-md-6 form-group'><label>Gift Price:</label>" +
                    "<input type='text' class='form-control' id='e_price' placeholder='Enter Price' value='"+styleboxData.gift_price+"' /></div>" +
                    "<div class='col-md-12'><input type='button' class='btn btn-danger pull-right' " +
                    "style='margin-top:30px' onclick=updateStylebox('"+styleboxData.g_id+"') id='subbtn' value='Update'/>" +
                    "</div></div><p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }

    function updateStylebox(g_id){
        var e_title = $("#e_title").val();
        var e_code = $("#e_code").val();
        var e_price = $("#e_price").val();
        var e_description = $("#description1").val();
        if(e_title === "" || e_price ==="" || e_code === "" || e_description === "" ){
            $("#styleboxError").html("please fill all required fields");
            $("#styleboxError").css("color","red");
            return false;
        }
        var data = new FormData();
        var ImageChanged = "no";
        var stylebox_file2 = $("#stylebox_file2").val();
        if(stylebox_file2 !== ""){
            ImageChanged = "yes";
            var stylebox_file_obj = document.getElementById('stylebox_file2');
            var stylebox_file_ext = stylebox_file2.split(".");
            stylebox_file_ext = stylebox_file_ext[stylebox_file_ext.length - 1];
            if (stylebox_file_ext === "jpg" || stylebox_file_ext === "png" || stylebox_file_ext === "gif" || stylebox_file_ext === "jpeg") {
                data.append('stylebox_file2', stylebox_file_obj.files[0]);
            }
            else{
                $("#styleboxError").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#styleboxError").css("color","red");
            }
        }
        data.append('type', "updateGiftbox");
        data.append('title', e_title);
        data.append('description', e_description);
        data.append('code', e_code);
        data.append('price', e_price);
        data.append('g_id', g_id);
        data.append('ImageChanged', ImageChanged);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState === 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status === "Success"){
                    $("#styleboxError").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else{
                    $("#styleboxError").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/giftProcess.php');
        request.send(data);
    }
    function confirmDelete_stylebox(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
            'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
            'onclick=delStylebox("'+id+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function delStylebox(id) {
        var url = "api/giftProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "deleteGiftbox", "g_id": id}, function (data) {
            var Status = data.Status;
            if (Status === "Success") {
                $("#myModal").modal("hide");
                getGiftboxData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
</script>
