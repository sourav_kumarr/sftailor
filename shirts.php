<?php
/**
 * Created by PhpStorm.
 * User: MrShyAm
 * Date: 8/19/2017
 * Time: 4:24 PM
 */
?>
<?php include("header.php");?>
<div class="banner top-aboutbannershirt">
    <div class="">
    </div>
</div>
<!-- //Slider -->
</div>
<!-- //Header -->
<div class="about-bottom wthree-3">
    <div class="container">
        <h2 class="tittle">A SHIRT AS UNIQUE AS YOU!</h2>
        <div class="agileinfo_about_bottom_grids">
		   <div class="col-md-12 agileinfo_about_bottom_grid">
               <p>The simple shirt is too often overlooked, but it makes your outfit complete. Your perfect wardrobe starts with a bespoke shirt. A custom-made shirt that fits you so perfectly, it makes you feel like you're on top of the world. Every man should have at least a few shirts like that and SF Tailors makes it possible without breaking the bank.</p><br>
               <p>Customize every single detail of your bespoke shirt. White, colored or patterned. Tucked or untucked. Button-down or spread collar. With or without cufflinks. Monogrammed or not. The combinations are endless. 
               </p><br>
                <p>
                    <b>Feel on top of the world with your bespoke shirt!<b>
                </p>
				<br><br>
				 <img src="images/aboutus_shirt_collage.jpg" alt=" " class="img-responsive"></br>
				 <h2 class="tittle">Customize YOUR SHIRT</h2>
                <img src="images/slider/customize_your_shirtcopy.jpg" alt=" " class="img-responsive">
            </div>
			
        </div>
        <h3 class="tittle" style="font-size:2em;">FASHION CAN BE BOUGHT. STYLE ONE MUST POSSESS. </br>EDNA WOOLMAN CHASE</h3>
    </div>
</div>

<!-- //team -->
<?php include("footer.php");?>
