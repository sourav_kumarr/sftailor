<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if(isset($_POST['filterButton'])){
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query="select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
}else{
    $query="select * from orders order by order_id DESC";
}

?>
<style>
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
        background: #eee;
    }
    #orderTable_filter{
        display:none;
    }
    .search-container{
        display: flex;
        flex-direction: row;
    }
    .search-remove{
        display: none;
    }
    .no-gutter{
        margin: 0px;
        padding: 0px;
    }
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
</style>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">

    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">

                    <div class="x_content" id="view-price" style="display: block;>
                        <div class="tab-content" style="background-color: rgb(238, 238, 238); padding: 28px 38px; border-radius: 10px; margin-top: -70px;">
                    <div class="tab-pane active" id="tab1">
                        <label style="" id="fabric_error">Commission Level Price</label>

                        <table id="priceTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Minimum Amount</th>
                                <th>Maximum Amount</th>
                                <th>Commission %</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();//for scan2tailor
                            if ($link) {
                                $query = "select * from wp_commission_personal where level_type='comm_level' order by comm_id asc";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($priceData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td>$<input type="text" id="price_minimum_<?php echo $priceData['comm_id'];?>" value="<?php echo $priceData['price_minimum']; ?>" style="border:none;"></td>
                                                <td>$<input type="text" id="price_maximum_<?php echo $priceData['comm_id'];?>" value="<?php echo $priceData['price_maximum']; ?>" style="border:none;"></td>
                                                <td><input type="text" id="price_commission_<?php echo $priceData['comm_id'];?>" value="<?php echo $priceData['price_commission']; ?>" style="border:none;"> </td>
                                                <td><a href='#' onclick=editPrice('<?php echo $priceData['comm_id'];?>') style="font-size: 18px; padding: 2px 4px; background: green none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-floppy-o"></i> </a> </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </br></br></br>
                        <label style="" id="fabric_error"> Starting Stylists:</label></br>

                        <table id="priceTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Stylist Type</th>
                                <th>Total Months</th>
                                <th>Commission %</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();//for scan2tailor
                            if ($link) {
                                $query = "select * from wp_commission_personal where level_type='month_comm' order by comm_id asc";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($priceData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td>Starting Stylists</td>
                                                <td><input type="text" id="price_maximum_<?php echo $priceData['comm_id'];?>" value="<?php echo $priceData['price_maximum']; ?>" style="border:none;width: 20px;">&nbsp;&nbsp;Months</td>
                                                <td><input type="text" id="price_commission_<?php echo $priceData['comm_id'];?>" value="<?php echo $priceData['price_commission']; ?>" style="border:none;"> </td>
                                                <td><a href='#' onclick=editPrice('<?php echo $priceData['comm_id'];?>') style="font-size: 18px; padding: 2px 4px; background: green none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-floppy-o"></i> </a> </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>




<!-- /page content -->
<?php
include('footer.php');
?>
<script>

    $(document).ready(function () {
        $('#priceTable').DataTable({});
    });

    function editPrice(comm_id){


        var price_minimum = $("#price_minimum_"+comm_id).val();
        var price_maximum  = $("#price_maximum_"+comm_id).val();
        var price_commission  = $("#price_commission_"+comm_id).val();

   /*     var url = "api/levelProcess.php";
        $.post(url,{"type":"editPrice","comm_id":comm_id,"price_minimum":price_minimum,"price_maximum":price_maximum,"price_commission":price_commission},
            function (data) {

            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done"){
                window.location="commissionpersonal.php";
            }
        });
*/


        var url = "api/levelProcess.php?type=editPrice&price_minimum=" + price_minimum + "&price_maximum=" + price_maximum +"&price_commission="+price_commission+"&comm_id="+comm_id;
        $.get(url, function (data) {
            var json = $.parseJSON(data)
            {
                var status = json.status;
                if (status == "done") {
                    window.location = "commissionpersonal.php";
                }
                else {
                    //alert('error');
                }
            }
        });





    }


</script>
