<?php
include('header.php');
?>
<style>
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
    .matchingTabs{cursor: pointer;}
    .allowtable{overflow: hidden}
   .pdf-imgr img{height: 261px;width: 255px;"}
</style>
<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<div class="right_col" role="main">
    <div class="row tile_count"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>All DOCUMENTS </h2>
                  <!--  <ul class="nav navbar-right panel_toolbox">
                        <!--<li>
                            <form method="post" class="form-inline">
                                <div class="form-group form-inline">
                                    <input type="file" class="form-control" name="file" id="file"/>
                                </div>
                                <input type="button" Value="Go" class="btn btn-primary btn-sm" name="filterButton"
                                       style="margin-top: 5px" onclick="uploadData();"/>
                            </form>
                        </li>-->
                    </ul>-->
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        List of Orders Generated
                    </p>

                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li role="presentation" id="trainingallow" class="matchingTabs tabs active" onclick="switchMatching('training')"><a >Training</a></li>
                            <li role="presentation" class="tabs matchingTabs" id="craftallow" onclick="switchMatching('craft')"><a>Craft Book</a></li>
                            <li role="presentation" class="tabs matchingTabs" id="marketingallow" onclick="switchMatching('marketing')"><a>Marketing</a></li>

                        </ul>
                    </div>
                        <div class="row">
                            <div class="col-md-12 allowtable" id="training_table" style="display:block;margin-top: 20px;">
                                Not Available
                            </div>
                            <div class="col-md-12 allowtable" id="craft_table" style="display:none;">
                                <div id="data_pdf" style="margin-top: 30px;">
                                    <div class="container" id="ViewGrid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 allowtable" id="marketing_table" style="display:none">
                                <div id="data_img" style="margin-top: 30px;">
                                    <div class="row"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<style>
    .main-grid-box{height: 288px;
        padding-top: 10px;
        text-align: center;
        margin-left: 5%;
        margin-bottom: 24px;
        border: 1px solid #999;
        width: 28%;
    }
    .pdf-img img {
        margin-bottom: 12px;
    }
    .pdf-title{margin-bottom: 12px;margin-top: 9px;}
    .ext-img{height: 240px;
        width: 272px;
        margin-bottom: 15px;
        background-position: center center;
        background-size: cover;}
</style>
<?php
include('footer.php');
?>
<script>
    function switchMatching(tab) {
        $(".allowtable").css("display","none");
        $("#"+tab+"_table").css("display","block");
        $(".tabs").removeClass("active");
        $("#"+tab+"allow").addClass("active");
    }
    function getAllPDF() {
        var url = 'api/pdfProcess.php';
        $(".loader").show();
        $.post(url,{'type':'getAllPDF'},function(data){
            var status = data.Status;
            var message = data.Message;
            $(".loader").hide();
            var th_ = '<div class="row">';
            var td_ = '';
            var th1_ = '<div class="row">';
            var td1_ = '';
            var th2_ = '<div class="row">';
            var td2_ = '';

            if(status === 'Success') {
                var items = data.data;

                for(var i=0;i<items.length;i++) {
                    var obj = items[i];
                    var title = obj.pdf_title;
                    var pdftitle = title.replace(/_/g, " ");
                    var file_type = obj.file_type;
                    if(file_type =="pdf"){
                        td_ = td_+'<div class="col-sm-3 main-grid-box"><div class="pdf-img"><iframe src="http://scan2fit.com/sftailor/admins/api/Files/pdf/'+title+'.pdf#scrollbar=0&scrolling=0" width="270" height="205"  frameborder="0" scrolling="no"></iframe>' +
                            '</div><p class="pdf-title"> '+pdftitle+' </p><div class="action-point">' +
                            '<a href="'+obj.pdf_file_path+'" target="_blank">' +
                            '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></div></div>';
                        $("#data_pdf").html(th_+td_+'</div>');
                    }
                    else if(file_type =="images"){
                        td1_ = td1_+'<div class="col-sm-3 main-grid-box" style="height: 310px;"><div class="pdf-imgr ext-img" style=background-image:url("http://scan2fit.com/sftailor/admins/api/Files/images/'+obj.pdf_file+'")>' +
                            '</div><div class="action-point">' +
                            '<a href="'+obj.pdf_file_path+'" target="_blank">' +
                            '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></div></div>';
                        $("#data_img").html(th1_+td1_+'</div>');
                    }
                    else if(file_type =="videos"){
                        td2_ = td2_+'<div class="col-sm-3 main-grid-box" style="height: 267px;"><div class="pdf-imgr"><video src="http://scan2fit.com/sftailor/admins/api/Files/videos/'+obj.pdf_file+'" style="margin-bottom: 15px;width: 267px;">' +
                            '</div><div class="action-point">' +
                            '<a href="'+obj.pdf_file_path+'" target="_blank">' +
                            '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></div></div>';
                        $("#training_table").html(th2_+td2_+'</div>');
                    }

                }


            }

            $('#pdfTable').DataTable({destroy:true});

        }).fail(function () {
            $(".modal-title").html("<label style='color: green'>Error !!!</label>");
            $(".modal-body").html("<label style='font-family: monospace'>Unable to process the request</label>");
            $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $("#myModal").modal("show");
        });

    }

    function uploadData() {
        var file = $("#file").val();
        if(file === ''){
            $(".modal-title").html("<label style='color: green'>Error !!!</label>");
            $(".modal-body").html("<label style='font-family: monospace'>Please select file before upload</label>");
            $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $("#myModal").modal("show");
            return false;
        }
        var file_data = document.getElementById("file");

        var request = new XMLHttpRequest();
        var data = new FormData();
        var url = 'api/pdfProcess.php';
        request.open("POST", url, true);
        $(".loader").show();
        request.onreadystatechange = function () {
            $(".loader").hide();
            if(request.readyState === 4 && request.status === 200) {
                var response = $.parseJSON(request.responseText);
                var status = response.Status;
                var message = response.Message;
                console.log(status);
                if(status === 'Failure') {
                    $(".modal-title").html("<label style='color: green'>Error !!!</label>");
                    $(".modal-body").html("<label style='font-family: monospace'>"+message+"</label>");
                    $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');

                    $("#myModal").modal("show");
                    return false;
                }
                getAllPDF();
            }
        };
        data.append("type","createPdf");
        data.append("file",file_data.files[0]);
        request.send(data);
    }

    function confirmDelete(id,path) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>'+
            '<button type="button" class="btn btn-danger" onclick=deleteData("'+id+'","'+path+'")>Delete</button>');

        $("#myModal").modal("show");
    }

    function deleteData(id,path) {
      var url ='api/pdfProcess.php';
      path = decodeURI(path);
      $.post(url,{type:'deleteData','id':id,path:path},function (data) {
          var status = data.Status;
          var message = data.Message;
          if(status === 'Success') {
            $("#myModal").modal("hide");
            getAllPDF();

          }
          $("#message").html(message);
      }).fail(function () {
          $("#message").html("Unable to process the request");
      });
    }


    getAllPDF();
</script>