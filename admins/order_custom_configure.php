<?php
include('header.php');
include("front_header.php");
include("../admin/webserver/database/db.php");
?>
<style>
    .nav_menu{
        margin-bottom: 0px;
    }
    .top-bar {
        background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
    }
    .stylecaption {
        background: #eee none repeat scroll 0 0;
        border-radius: 4px;
        font-size: 21px;
        font-weight: bold;
        letter-spacing: 1px;
        margin-bottom: 32px;
        margin-top: 0;
        padding: 4px;
        text-align: center;
    }
    .active {
        border: 2px solid #555;
        border-radius: 4px;
    }
    .primarysuggestion {
        height: auto;
        margin-left: 10px;
        margin-top: 10px;
        width: auto;
    }
    .primarysuggestions {
        height: auto;
        margin-left: 10px;
        margin-top: 10px;
        width: auto;
    }

    .steps {

    }

    .stepsactive {
        background: #c4996c none repeat scroll 0 0;
        border: 2px solid #c4996c;
        color: white;
    }

    .breakingline {
        margin-top: 38px;
        width: 501px;
    }
    .outputsugg {
        position: absolute;
        margin-left: 70px;
        z-index:-1;
    }
    .backbtnsugg
    {
        height:68%;
    }
    .backbtnsuggparentdiv
    {
        height: 130px;
        width: 130px;
    }
    .backbtnsuggparentdiv p
    {
        font-weight:normal;
        font-size: 12px;
        line-height: 18px!important;
    }
    #forcasual .primarysuggestion > img
    {
        padding: 40px;
    }
    #frontpockets{overflow-y: scroll; height: 660px; padding-top: 22px;}
    .okok {
        font-size: 19px;
        height: 52px;
        margin-left: 10px;
        margin-top: 10px;
        padding-top: 6px;
        width: 162px;
    }
    .stylecaption {
        margin-top: -54px;
    }
    .overflow-cmd{overflow-y: auto; height: 660px; padding-top: 22px;}
    .addtocarts {
        margin-top: 479px;
        position: absolute;
        width: 137px;
        margin-left: -250px;
        z-index: 9999;
    }
    .primarysuggestions {
        height: auto!important;
        margin-left: 10px;
        margin-top: 10px;
        width: auto;
    }
    .primarysuggestions {
        border: 2px solid #ccc;
        border-radius: 4px;
        float: left;
        height: 171px;
        margin-left: 42px;
        margin-top: 35px;
        text-align: center;
        width: 165px;
    }
    .outputsugg {
        position: absolute;
        margin-left: 70px;
        width: 300px;
    }
    .nav-md .container.body .right_col{
        padding: 35px  1px  0 0;
        background: #fff;
    }
    .about-bottom{
        padding: 48px;
    }
    .steps{
        font-size: 12px!important;
    }
    .suggestionname{
        font-size: 12px;
    }
    .primarysuggestions img{
        width: 100%;
    }
</style>
<div class="right_col" role="main" style="overflow-y: auto">
    <div class="about-bottom wthree-3" style="padding: 0">
        <div class=" loader loader" style="display: none">
            <img src="../images/spin.gif" class="spinloader"/>
        </div>
        <!------gallary part---------->
        <div id="ourconfig">
        <p class="stylecaption" style="margin-left: 88px;margin-top: 68px;width: 86%;">Our Collection</p>
            <div class="" id="firstconfigovers" style="display: block; margin-left: 27px;">
                <div class="primarysuggestions steps"  id="firstconfig"  name="firstconfig" onclick="customsuits('firstconfigs','default0001')">
                    <img src="../images/preconfig/suit/default0001.png"/>
                </div>
                <div class="primarysuggestions steps" id="secondconfig" name="secondconfig" onclick="customsuits('secondconfigs','default0002')">
                    <img src="../images/preconfig/suit/default0002.png"/>
                </div>
                <div class="primarysuggestions steps" id="thirdconfig" name="thirdconfig" onclick="customsuits('thirdconfigs','default0003')">
                    <img src="../images/preconfig/suit/default0003.png"/>
                </div>
                <div class="primarysuggestions steps" id="forthconfig" name="forthconfig" onclick="customsuits('forthconfigs','default0004')">
                    <img src="../images/preconfig/suit/default0004.png"/>
                </div>
                <div class="primarysuggestions steps" id="fifthconfig" name="fifthconfig" onclick="customsuits('fifthconfigs','default0005')">
                    <img src="../images/preconfig/suit/default0005.png"/>
                </div>
                <div class="primarysuggestions steps" id="sixthconfig" name="sixthconfig" onclick="customsuits('sixthconfigs','default0006')">
                    <img src="../images/preconfig/suit/default0006.png"/>
                </div>
                <div class="primarysuggestions steps" id="sevenththconfig" name="sevenththconfig" onclick="customsuits('sevenththconfigs','default0007')">
                    <img src="../images/preconfig/suit/default0007.png"/>
                </div>
                <div class="primarysuggestions steps" id="eighthconfig" name="eighthconfig" onclick="customsuits('eighthconfigs','default0008')">
                    <img src="../images/preconfig/suit/default0008.png"/>
                </div>
                <div class="primarysuggestions steps" id="ninethconfig" name="ninethconfig" onclick="customsuits('ninethconfigs','default0009')">
                    <img src="../images/preconfig/suit/default0009.png"/>
                </div>
                <div class="primarysuggestions steps" id="tenththconfig" name="tenththconfig"  onclick="customsuits('tenththconfigs','default00010')">
                    <img src="../images/preconfig/suit/default00010.png"/>
                </div>
                <div class="primarysuggestions steps" id="eleventhconfig" name="eleventhconfig"  onclick="customsuits('eleventhconfigs','default00011')">
                    <img src="../images/preconfig/suit/default00011.png"/>
                </div>
                <div class="primarysuggestions steps" id="twelveconfig" name="twelveconfig"  onclick="customsuits('twelveconfigs','default00012')">
                    <img src="../images/preconfig/suit/default00012.png"/>
                </div>
            </div>
        </div>


        <div class="col-md-12" id="showconfig" style="display:none;">
    <div class="col-md-6">
       <div class="col-md-6" style="padding-top: 50px;position: relative;z-index: 1">
            <img src="../images/preconfig/default0001.png"  class="outputsugg" id="preconfigimg" alt=''/>
        </div>
    </div>
    <div class="col-md-6" style="margin-top: 90px;">
        <div class="col-md-12" style="padding:50px 0;display:none;">
            <div class="steps stepsactive" id="firstconfig" name="firstconfig"></div>
            <div class="steps" id="secondconfig" name="secondconfig" ></div>
            <div class="steps" id="thirdconfig"  name="thirdconfig" ></div>
            <div class="steps" id="forthconfig"  name="forthconfig" ></div>
            <div class="steps" id="fifthconfig" name="fifthconfig" ></div>
            <div class="steps" id="sixthconfig" name="sixthconfig" ></div>
            <div class="steps" id="sevenththconfig" name="sevenththconfig" ></div>
            <div class="steps" id="eighthconfig" name="eighthconfig" ></div>
            <div class="steps" id="ninethconfig" name="ninethconfig" ></div>
            <div class="steps" id="tenththconfig" name="tenththconfig" ></div>
            <div class="steps" id="eleventhconfig" name="eleventhconfig" ></div>
            <div class="steps" id="twelveconfig" name="twelveconfig" ></div>
          
        </div>
        <!--
        ------
        -----first div
        ------
        -------->
     
	    <p class="stylecaption">Our Collection</p>
		 
        <div class="col-md-12" id="firstconfigs" style="display:none">
                
				<div class="primarysuggestion" name="rcfabricsuitDBP740A"  id="firstconfig_rcfabricsuitDBP740A">
                    <img src="../images/preconfig/rcfabricsuitDBP740A.png"/>
                    <p class="suggestionname">RC Fabric suit</br>DBP740A</p>
                </div>
				
				<div class="primarysuggestion" name="shirtSAM049A"  id="firstconfig_shirtSAM049A">
                    <img src="../images/preconfig/shirtSAM049A.png"/>
                    <p class="suggestionname">Shirt</br>SAM049A</p>
                </div>
				<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6038"  id="firstconfig_handkerchiefKFJ1612WBR6038">
                    <img src="../images/preconfig/handkerchiefKFJ1612WBR6038.png"/>
                    <p class="suggestionname">Pocket Square</br>KFJ1612WBR6038</p>
                </div>
				<div class="primarysuggestion"  name="bowtieKLD1612WBR6106" id="firstconfig_bowtieKLD1612WBR6106">
                    <img src="../images/preconfig/bowtieKLD1612WBR6106.png"/>
                    <p class="suggestionname">Neck Tie</br>KLD1612WBR6106</p>
                </div>
				<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAT2716','default0001','Pocket Square','KFJ1612WBR6038','handkerchiefKFJ1612WBR6038','Neck Tie','KLD1612WBR6106','bowtieKLD1612WBR6106')" class="prevnextbtn addtocarts">Add to Cart</label>
        </div>  
		<!--
       ------
       -----second style div
       ------
       -------->
        <div class="col-md-12" id="secondconfigs" style="display:none">
		  
			<div class="primarysuggestion"  name="rcfabricsuitDBP748A" id="secondconfig_rcfabricsuitDBP748A">
				<img src="../images/preconfig/rcfabricsuitDBP748A.png"/>
				<p class="suggestionname">RC Fabric suit</br>DBP748A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAP015A"  id="secondconfig_shirtSAP015A">
				<img src="../images/preconfig/shirtSAP015A.png"/>
				<p class="suggestionname">Shirt</br>SAP015A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6061"  id="secondconfig_handkerchiefKFJ1612WBR6061">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLD1612WBR6101"  id="secondconfig_bowtieKLD1612WBR6101">
				<img src="../images/preconfig/bowtieKLD1612WBR6101.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6101</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 3pc suit','High-end Fabric Suit','DAP276A','default0002','Pocket Square','KFJ1612WBR6061','handkerchiefKFJ1612WBR6061','Neck Tie','KLD1612WBR6101','bowtieKLD1612WBR6101')" class="prevnextbtn addtocarts">Add to Cart</label>
			
			
		</div>
	<!--
       ------
       -----third style div
       ------
       -------->		
		<div class="col-md-12" id="thirdconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBN388A"  id="thirdconfig_rcfabricsuitDBN388A">
				<img src="../images/preconfig/rcfabricsuitDBN388A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBN388A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAM219A"  id="thirdconfig_shirtSAM219A">
				<img src="../images/preconfig/shirtSAM219A.png"/>
				<p class="suggestionname">shirt</br>SAM219A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6061-1"  id="thirdconfig_handkerchiefKFJ1612WBR6061-1">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061-1.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLD1612WBR6101-1"  id="thirdconfig_bowtieKLD1612WBR6101-1">
				<img src="../images/preconfig/bowtieKLD1612WBR6101-1.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6101</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAT2653','default0003','Pocket Square','KFJ1612WBR6061','handkerchiefKFJ1612WBR6061','Neck Tie','KLD1612WBR6101','bowtieKLD1612WBR6101')" class="prevnextbtn addtocarts">Add to Cart</label>
			
			
		</div>
		<!--
       ------
       -----forth style div
       ------
       -------->		
		<div class="col-md-12" id="forthconfigs" style="display:none">
		
			<div class="primarysuggestion" name="rcfabricsuitDBP740A"  id="forthconfig_rcfabricsuitDBP740A">
				<img src="../images/preconfig/rcfabricsuitDBP740A-2.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP740A</p>
			</div>
			
			<div class="primarysuggestion"  name="shirtSAM219A" id="forthconfig_shirtSAM219A">
				<img src="../images/preconfig/shirtSAM219A-2.png"/>
				<p class="suggestionname">shirt</br>SAM219A</p>
			</div>
			<div class="primarysuggestion" name="bowtieKFJ1612WBR6061"  id="forthconfig_bowtieKFJ1612WBR6061">
				<img src="../images/preconfig/bowtieKFJ1612WBR6061.png"/>
				<p class="suggestionname">Neck Tie</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKLD1612WBR6093"  id="forthconfig_handkerchiefKLD1612WBR6093">
				<img src="../images/preconfig/handkerchiefKLD1612WBR6093.png"/>
				<p class="suggestionname">Pocket Square</br>KLD1612WBR6093</p>
			</div>
			<label id="" onclick="buyAccessories('Full Canvas 3pc suit','High-end Fabric Suit','DAQ024A','default0004','Pocket Square','KLD1612WBR6093','handkerchiefKLD1612WBR6093','Neck Tie','KFJ1612WBR6061','bowtieKFJ1612WBR6061')" class="prevnextbtn addtocarts">Add to Cart</label>
			
		</div>
		<!--
       ------
       -----fifth style div
       ------
       -------->		
		<div class="col-md-12" id="fifthconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBP739A"  id="fifthconfig_rcfabricsuitDBP739A">
				<img src="../images/preconfig/rcfabricsuitDBP739A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP739A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAP016A"  id="fifthconfig_shirtSAP016A">
				<img src="../images/preconfig/shirtSAP016A.png"/>
				<p class="suggestionname">shirt</br>SAP016A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6055"  id="fifthconfig_handkerchiefKFJ1612WBR6055">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6055.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6055</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLD1612WBR6097"  id="fifthconfig_bowtieKLD1612WBR6097">
				<img src="../images/preconfig/bowtieKLD1612WBR6097.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6097</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAT2645','default0005','Pocket Square','KFJ1612WBR6055','handkerchiefKFJ1612WBR6055','Neck Tie','KLD1612WBR6097','bowtieKLD1612WBR6097')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----sixth style div
       ------
       -------->		
		<div class="col-md-12" id="sixthconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBP680A"  id="sixthconfig_rcfabricsuitDBP680A">
				<img src="../images/preconfig/rcfabricsuitDBP680A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP680A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAP016A"  id="sixthconfig_shirtSAP016A">
				<img src="../images/preconfig/shirtSAP016A-6.png"/>
				<p class="suggestionname">shirt</br>SAP016A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKLD1612WBR6101"  id="sixthconfig_handkerchiefKLD1612WBR6101">
				<img src="../images/preconfig/handkerchiefKLD1612WBR6101.png"/>
				<p class="suggestionname">Pocket Square</br>KLD1612WBR6101</p>
			</div>
			<div class="primarysuggestion"  name="bowtieKFJ1612WBR6061" id="sixthconfig_bowtieKFJ1612WBR6061">
				<img src="../images/preconfig/bowtieKFJ1612WBR6061-6.png"/>
				<p class="suggestionname">Neck Tie</br>KFJ1612WBR6061</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAT2654','default0006','Pocket Square','KLD1612WBR6101','handkerchiefKLD1612WBR6101','Neck Tie','KFJ1612WBR6061','bowtieKFJ1612WBR6061-6')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----seventhth style div
       ------
       -------->		
		<div class="col-md-12" id="sevenththconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBP740A"  id="sevenththconfig_rcfabricsuitDBP740A">
				<img src="../images/preconfig/rcfabricsuitDBP740A-7.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP740A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAN463A"  id="sevenththconfig_shirtSAN463A">
				<img src="../images/preconfig/shirtSAN463A.png"/>
				<p class="suggestionname">shirt</br>SAN463A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6061" id="sevenththconfig_handkerchiefKFJ1612WBR6061">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061-7.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLJ1612WBR6033"  id="sevenththconfig_bowtieKLJ1612WBR6033">
				<img src="../images/preconfig/bowtieKLJ1612WBR6033.png"/>
				<p class="suggestionname">Neck Tie</br>KLJ1612WBR6033</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAP378A','default0007','Pocket Square','KFJ1612WBR6061','handkerchiefKFJ1612WBR6061-7','Neck Tie','KLJ1612WBR6033','bowtieKLJ1612WBR6033')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----eighthconfig style div
       ------
       -------->		
		<div class="col-md-12" id="eighthconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBP737A"  id="eighthconfig_rcfabricsuitDBP737A">
				<img src="../images/preconfig/rcfabricsuitDBP737A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP737A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAN463A"  id="eighthconfig_shirtSAN463A">
				<img src="../images/preconfig/shirtSAN463A-8.png"/>
				<p class="suggestionname">shirt</br>SAN463A</p>
			</div>
			<div class="primarysuggestion"  name="handkerchiefKFJ1612WBR6061" id="eighthconfig_handkerchiefKFJ1612WBR6061">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061-8.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLD1612WBR6107"  id="eighthconfig_bowtieKLD1612WBR6107">
				<img src="../images/preconfig/bowtieKLD1612WBR6107.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6107</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAR2130','default0008','Pocket Square','KFJ1612WBR6061','handkerchiefKFJ1612WBR6061-8','Neck Tie','KLD1612WBR6107','bowtieKLD1612WBR6107')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----ninethconfig style div
       ------
       -------->		
		<div class="col-md-12" id="ninethconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBP737A"  id="ninethconfig_rcfabricsuitDBP737A">
				<img src="../images/preconfig/rcfabricsuitDBP737A-11.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP737A</p>
			</div>
			
			<div class="primarysuggestion" name="shirtSAM219A"  id="ninethconfig_shirtSAM219A">
				<img src="../images/preconfig/shirtSAM219A-11.png"/>
				<p class="suggestionname">shirt</br>SAM219A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6030"  id="ninethconfig_handkerchiefKFJ1612WBR6030">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6030.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6030</p>
			</div>
			<div class="primarysuggestion"  name="bowtieKLJ1612WBR6030" id="ninethconfig_bowtieKLJ1612WBR6030">
				<img src="../images/preconfig/bowtieKLJ1612WBR6030.png"/>
				<p class="suggestionname">Neck Tie</br>KLJ1612WBR6030</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAR2130','default0009','Pocket Square','KFJ1612WBR6030','handkerchiefKFJ1612WBR6030','Neck Tie','KLJ1612WBR6030','bowtieKLJ1612WBR6030')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----tenththconfigs style div
       ------
       -------->		
		<div class="col-md-12" id="tenththconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBR229A"  id="tenththconfig_rcfabricsuitDBR229A">
				<img src="../images/preconfig/rcfabricsuitDBR229A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBR229A</p>
			</div>
			
			<div class="primarysuggestion"  name="shirtSAP013A" id="tenththconfig_shirtSAP013A">
				<img src="../images/preconfig/shirtSAP013A.png"/>
				<p class="suggestionname">shirt</br>SAP013A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6061"  id="tenththconfig_handkerchiefKFJ1612WBR6061">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061-12.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLD1612WBR6096"  id="tenththconfig_bowtieKLD1612WBR6096">
				<img src="../images/preconfig/bowtieKLD1612WBR6096.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6096</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAR2135','default00010','Pocket Square','KFJ1612WBR6061','handkerchiefKFJ1612WBR6061-12','Neck Tie','KLD1612WBR6096','bowtieKLD1612WBR6096')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----eleventhconfigs style div
       ------
       -------->		
		<div class="col-md-12" id="eleventhconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBL647A"  id="eleventhconfig_rcfabricsuitDBL647A">
				<img src="../images/preconfig/rcfabricsuitDBL647A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBL647A</p>
			</div>
			
			<div class="primarysuggestion"  name="shirtSAK342A" id="eleventhconfig_shirtSAK342A">
				<img src="../images/preconfig/shirtSAK342A.png"/>
				<p class="suggestionname">shirt</br>SAK342A</p>
			</div>
			<div class="primarysuggestion"  name="handkerchiefKFJ1612WBR6061" id="eleventhconfig_handkerchiefKFJ1612WBR6061">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061-22.png"/>
				<p class="suggestionname">Pocket Square</br>KFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion"  name="bowtieKLD1612WBR6091" id="eleventhconfig_bowtieKLD1612WBR6091">
				<img src="../images/preconfig/bowtieKLD1612WBR6091.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6091</p>
			</div>
			
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAP885A','default00011','Pocket Square','KFJ1612WBR6061','handkerchiefKFJ1612WBR6061-22','Neck Tie','KLD1612WBR6091','bowtieKLD1612WBR6091')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		<!--
       ------
       -----twelveconfigs style div
       ------
       -------->		
		<div class="col-md-12" id="twelveconfigs" style="display:none">
			
			<div class="primarysuggestion" name="rcfabricsuitDBP645A"  id="twelveconfig_rcfabricsuitDBP645A">
				<img src="../images/preconfig/rcfabricsuitDBP645A.png"/>
				<p class="suggestionname">RC fabric suit</br>DBP645A</p>
			</div>
		
			<div class="primarysuggestion" name="shirtSAM049A"  id="twelveconfig_shirtSAM049A">
				<img src="../images/preconfig/shirtSAM049A-33.png"/>
				<p class="suggestionname">shirt</br>SAM049A</p>
			</div>
			<div class="primarysuggestion" name="handkerchiefKFJ1612WBR6061"  id="twelveconfig_handkerchiefKFJ1612WBR6061">
				<img src="../images/preconfig/handkerchiefKFJ1612WBR6061-33.png"/>
				<p class="suggestionname">Pocket Square</br>fKFJ1612WBR6061</p>
			</div>
			<div class="primarysuggestion" name="bowtieKLD1612WBR6091"  id="twelveconfig_bowtieKLD1612WBR6091">
				<img src="../images/preconfig/bowtieKLD1612WBR6091-33.png"/>
				<p class="suggestionname">Neck Tie</br>KLD1612WBR6091</p>
			</div>
			
			<label id="" onclick="buyAccessories('Full Canvas 2pc suit','High-end Fabric Suit','DAQ024A','default00012','Pocket Square','fKFJ1612WBR6061','handkerchiefKFJ1612WBR6061-33','Neck Tie','KLD1612WBR6091','bowtieKLD1612WBR6091-33')" class="prevnextbtn addtocarts">Add to Cart</label>
		</div>
		
		
		
    </div>
         <div class="col-md-6"></div>
        <div class="col-md-6">
            <label id="previous" onclick="navigation('previous')" style="margin-top: 52px;" class="prevnextbtn">Previous</label>
            <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
        </div>

    <?php
        if(isset($_REQUEST['fromtwo']))
        {
            ?>
            <script>
                 showcolordiv();
            </script>
            <?php
        }
    ?>
</div>
    </div>
</div>
<?php include "footer.php"; ?>
<script>
        $( document ).ready(function() {
            $(".nav a").removeClass("active");
        });
        var currPage = "";
        var category = "formal";
        var firstconfig = "highendfabricsuitDAT2716";
        var secondconfig = "highendfabricsuitDAP276A";
        var thirdconfig = "highendfabricsuitDAT2653";
        var forthconfig = "highendfabricsuitDAQ024A";
        var fifthconfig = "highendfabricsuitDAT2645";
        var sixthconfig = "highendfabricsuitDAT2654";
        var sevenththconfig = "highendfabricsuitDAP378A";
        var eighthconfig = "highendfabricsuitDAR2130";
        var ninethconfig = "highendfabricsuitDAR2130-11";
        var tenththconfig = "highendfabricsuitDAR2135";
        var eleventhconfig = "highendfabricsuitDAP885A";
        var twelveconfig = "highendfabricsuitDAQ024A";

        function loadSessionData() {
            $(".loader").fadeIn("slow");
            var prevIndex = $(".stepsactive").attr("id");
            currPage = prevIndex + "s";
            var url = "../admin/webserver/selectionData.php?type=getsession&" + prevIndex + "=" + prevIndex;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    var img = "";
                    var item = json.items;
                    var name = json.items;
                    item = prevIndex + "_" + item;
                    $(".primarysuggestion").removeClass("active");
                    $("#" + item).addClass("active");
                    if (prevIndex == "category") {
                        category = name;
                    }
                    else if (prevIndex == "firstconfig") {
                        firstconfig = name;
                    }
                    else if (prevIndex == "secondconfig") {
                        secondconfig = name;
                    }
                    else if (prevIndex == "thirdconfig") {
                        thirdconfig = name;
                    }
                    else if (prevIndex == "forthconfig") {
                        forthconfig = name;
                    }
                    else if (prevIndex == "fifthconfig") {
                        fifthconfig = name;
                    }
                    else if (prevIndex == "sixthconfig") {
                        sixthconfig = name;
                    }
                    else if (prevIndex == "sevenththconfig") {
                        sevenththconfig = name;
                    }
                    else if (prevIndex == "eighthconfig") {
                        eighthconfig = name;
                    }
                    else if (prevIndex == "ninethconfig") {
                        ninethconfig = name;
                    }
                    else if (prevIndex == "tenththconfig") {
                        tenththconfig = name;
                    }
                    else if (prevIndex == "eleventhconfig") {
                        eleventhconfig = name;
                    }
                    else if (prevIndex == "twelveconfig") {
                        twelveconfig = name;
                    }


                    if (currPage == "categorys") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0001.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "firstconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0001.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "secondconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0002.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "thirdconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0003.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "forthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0004.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "fifthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0005.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "sixthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0006.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "sevenththconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0007.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "eighthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0008.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "ninethconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0009.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "tenththconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00010.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "eleventhconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00011.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "twelveconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00012.png");
                        $(".loader").fadeOut("slow");
                    }

                }
                else {
                    var url = "";
                    if (currPage == "categorys") {
                        folder = "front";
                        $("#preconfigimg").attr("src", "../images/preconfig/default0001.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + category;
                    }
                    else if (currPage == "firstconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0001.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + firstconfig;
                    }
                    else if (currPage == "secondconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0002.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + secondconfig;
                    }
                    else if (currPage == "thirdconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0003.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + thirdconfig;
                    }
                    else if (currPage == "forthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0004.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + forthconfig;
                    }
                    else if (currPage == "fifthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0005.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + fifthconfig;
                    }
                    else if (currPage == "sixthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0006.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + sixthconfig;
                    }
                    else if (currPage == "sevenththconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0007.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + sevenththconfig;
                    }
                    else if (currPage == "eighthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0008.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + eighthconfig;
                    }
                    else if (currPage == "ninethconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0009.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + ninethconfig;
                    }
                    else if (currPage == "tenththconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00010.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + tenththconfig;
                    }
                    else if (currPage == "eleventhconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00011.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + eleventhconfig;
                    }
                    else if (currPage == "twelveconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00012.png");
                        url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + twelveconfig;
                    }


                    $.get(url, function (data) {
                        var json = $.parseJSON(data);
                        var status = json.status;
                        if (status == "done") {
                            $(".loader").fadeOut("slow");
                        }
                    }).fail(function () {
                        alert("page is not available");
                    });
                }
            });
        }

        $(".primarysuggestion").click(function () {
            var Img = "";
            $(".loader").fadeIn("slow");
            $(".primarysuggestion").removeClass("active");
            var value = this.id;
            var name = $("#"+value).attr("name");
            var type = value.split("_");
            var selection = type[1];
            $("#" + value).addClass("active");
            var currpage = type[0] + "s";
            var url = "../../admin/webserver/selectionData.php?" + type[0] + "=" + selection;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    if (type[0] == "category") {
                        category = selection;
                    }
                    else if (type[0] == "firstconfig") {
                        firstconfig = selection;
                    }
                    else if (type[0] == "secondconfig") {
                        secondconfig = selection;
                    }
                    else if (type[0] == "thirdconfig") {
                        thirdconfig = selection;
                    }
                    else if (type[0] == "forthconfig") {
                        forthconfig = selection;
                    }
                    else if (type[0] == "fifthconfig") {
                        fifthconfig = selection;
                    }
                    else if (type[0] == "sixthconfig") {
                        sixthconfig = selection;
                    }
                    else if (type[0] == "sevenththconfig") {
                        sevenththconfig = selection;
                    }
                    else if (type[0] == "eighthconfig") {
                        eighthconfig = selection;
                    }
                    else if (type[0] == "ninethconfig") {
                        ninethconfig = selection;
                    }
                    else if (type[0] == "tenththconfig") {
                        tenththconfig = selection;
                    }
                    else if (type[0] == "eleventhconfig") {
                        eleventhconfig = selection;
                    }
                    else if (type[0] == "twelveconfig") {
                        twelveconfig = selection;
                    }

                    if (currpage == "categorys") {
                        folder = "front";
                        $("#preconfigimg").attr("src", "../images/preconfig/default0001.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "firstconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0001.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "secondconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0002.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "thirdconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0003.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "forthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0004.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "fifthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0005.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "sixthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0006.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "sevenththconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0007.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "eighthconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0008.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "ninethconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default0009.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "tenththconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00010.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "eleventhconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00011.png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currpage == "twelveconfigs") {
                        $("#preconfigimg").attr("src", "../images/preconfig/default00012.png");
                        $(".loader").fadeOut("slow");
                    }

                }
                else {
                    alert(status);
                    $(".loader").fadeOut("slow");
                }
            }).fail(function () {
                alert("page is not available");
                $(".loader").fadeOut("slow");
            });
        });
        function navigation(switcher) {
            $("#" + currPage).fadeOut("slow");
            if (switcher == "next") {
                if (currPage == "categorys") {
                    $(".steps").removeClass("stepsactive");
                    $("#firstconfigs").fadeIn("slow");
                    $("#firstconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "firstconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#secondconfigs").fadeIn("slow");
                    $("#secondconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "secondconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#thirdconfigs").fadeIn("slow");
                    $("#thirdconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "thirdconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#forthconfigs").fadeIn("slow");
                    $("#forthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "forthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#fifthconfigs").fadeIn("slow");
                    $("#fifthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "fifthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#sixthconfigs").fadeIn("slow");
                    $("#sixthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "sixthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#sevenththconfigs").fadeIn("slow");
                    $("#sevenththconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "sevenththconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#eighthconfigs").fadeIn("slow");
                    $("#eighthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "eighthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#ninethconfigs").fadeIn("slow");
                    $("#ninethconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "ninethconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#tenththconfigs").fadeIn("slow");
                    $("#tenththconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "tenththconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#eleventhconfigs").fadeIn("slow");
                    $("#eleventhconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "eleventhconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#twelveconfigs").fadeIn("slow");
                    $("#twelveconfig").addClass("stepsactive");
                    loadSessionData();
                }

            }
            else if (switcher == "previous") {
                //alert(currPage);
                if (currPage == "categorys") {
                    window.location = "customize.php";
                }
                else if (currPage == "firstconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#categorys").fadeIn("slow");
                    $("#category").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "secondconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#firstconfigs").fadeIn("slow");
                    $("#firstconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "thirdconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#secondconfigs").fadeIn("slow");
                    $("#secondconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "forthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#thirdconfigs").fadeIn("slow");
                    $("#thirdconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "fifthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#forthconfigs").fadeIn("slow");
                    $("#forthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "sixthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#fifthconfigs").fadeIn("slow");
                    $("#fifthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "sevenththconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#sixthconfigs").fadeIn("slow");
                    $("#sixthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "eighthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#sevenththconfigs").fadeIn("slow");
                    $("#sevenththconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "ninththconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#eighthconfigs").fadeIn("slow");
                    $("#eighthconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "tenththconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#ninththconfigs").fadeIn("slow");
                    $("#ninththconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "eleventhconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#tenththconfigs").fadeIn("slow");
                    $("#tenththconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "twelveconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#eleventhconfigs").fadeIn("slow");
                    $("#eleventhconfig").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "thirteenthconfigs") {
                    $(".steps").removeClass("stepsactive");
                    $("#twelveconfigs").fadeIn("slow");
                    $("#twelveconfig").addClass("stepsactive");
                    loadSessionData();
                }

            }
        }

        $(".steps").click(function(){
            $("#"+currPage).hide(1000);
            var newcurrpage = this.id+"s";
            $(".steps").removeClass("stepsactive");
            $("#"+this.id).addClass("stepsactive");
            $("#"+newcurrpage).show(1000);
            currPage = newcurrpage;
            loadSessionData();
            if(currPage == "pantcolors")
            {
                $("#next").html("Add to Cart");
            }
            else
            {
                $("#next").html("Next");
            }
        });
        loadSessionData();

        function buyAccessories(category,specification,rc_no,image_type,pc_product_name,pc_rc_name,pc_image,nc_product_name,nc_rc_name,nc_image){
            buyPocket_square(pc_product_name,pc_rc_name,pc_image);
            buyNeck_ties(nc_product_name,nc_rc_name,nc_image);
            var user_id = $("#user_id").val();
            var randomnum = Math.floor((Math.random() * 600000) + 1);
            var product_name = category + randomnum;
            var url = "../admin/webserver/addto_cart.php?products_type=Collection&category="+category+"&product_name="+
                product_name+"&specification="+specification+"&rc_no="+rc_no+"&user_id="+user_id+"&quantity=1&image_type="+image_type;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    window.location = "order_cart.php?userId="+user_id;
                }
                else {
                    alert(status);
                }
            });
        }

        function buyPocket_square(pc_product_name,pc_rc_name,pc_image){
            var user_id = $("#user_id").val();
            var randomnum = Math.floor((Math.random() * 600000) + 1);
            var product_name = pc_product_name+"_"+randomnum;
            var url = "../admin/webserver/addto_cart.php?products_type=Accessories&category="+pc_product_name+"&product_name="+
                product_name+"&specification="+pc_product_name+"&rc_no="+pc_rc_name+"&user_id="+user_id+"&quantity=1&image_type="+pc_image;

            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    //window.location = "cart.php";
                }
                else {
                    alert(status);
                }
            });
        }
        function buyNeck_ties(nc_product_name,nc_rc_name,nc_image){
            var user_id = $("#user_id").val();
            var randomnum = Math.floor((Math.random() * 600000) + 1);
            var product_name = nc_product_name+"_"+randomnum;
            var url = "../admin/webserver/addto_cart.php?products_type=Accessories&category="+nc_product_name+"&product_name="+
                product_name+"&specification="+nc_product_name+"&rc_no="+nc_rc_name+"&user_id="+user_id+"&quantity=1&image_type="+nc_image;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    //window.location = "cart.php";
                }
                else {
                    alert(status);
                }
            });
        }
        function customsuits(suitlocation,defaultimg){
            $("#"+suitlocation).show();
            $("#preconfigimg").attr("src", "images/preconfig/"+defaultimg+".png");
            $("#showconfig").show();
            $("#ourconfig").hide();
        }
        var user_id = $("#user_id").val();
        getCart(user_id);
    </script>