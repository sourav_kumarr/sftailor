<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/PRIVACY_POLICY.php');
$conn = new \Classes\CONNECT();
$priClass = new \Classes\PRIVACY_POLICY();
$privacyData = $priClass->getPrivacyData();

?>
<style>
    .shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .hideloader {
        display: none;
    }
    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
    #create_stores{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
    #add_admin_data{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
    @media print {
        body * {
            visibility: hidden;
        }
        #section-to-print, #section-to-print * {
            visibility: visible;
        }
        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
    .e_Administrator{display:none;}
</style>
<!-- page content -->
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<div id="loadd" class="loaderr hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <br class="x_panel">
                <div class="x_title">
                    <h2>Privacy Policy <small></small></h2>

                    <div class="clearfix"></div>
                </div>
                <button style="margin-top:5px;float: right;" onclick="editTerms()" class="btn btn-info btn-sm" id="edit-termss">Edit Terms</button>
                <button style="margin-top:5px;float: right;display: none;" onclick="hideterms()" class="btn btn-info btn-sm" id="close-terms">Close Terms</button>
                </br></br></br>
                <form action="" id="update_terms" method="post" style="display: none;">
                    <textarea name="editor1" id="editor1">
                        <?php echo $privacyData['privacyData'][0]['message'];?>
                    </textarea>
                    <input type="hidden" name="type" value="updatePrivacyPolicy"></br>
                    <input type="submit" name="submit" value="Submit">
                </form>
                </br></br></br>
                <div style="float:left;font-weight: bold">PRIVACY POLICY OUTPUT:-</div></br></br>
                <div class="col-md-10" style="padding-top: 26px;padding-left: 32px;">
                    <?php echo $privacyData['privacyData'][0]['message'];?>
                </div>
            </div>
                <?php
                include('footer.php');
                ?>
                <script>
                    function editTerms() {
                        $("#update_terms").show("slow");
                        $("#close-terms").show();
                        $("#edit-termss").hide();
                    }
                    function hideterms() {
                        $("#update_terms").hide("slow");
                        $("#close-terms").hide();
                        $("#edit-termss").show();

                    }
                    CKEDITOR.replace( 'editor1' );

                    $("#update_terms").submit(function(e){
                        e.preventDefault();
                        for (var i in CKEDITOR.instances) {
                            CKEDITOR.instances[i].updateElement();
                        }
                        $(".loaderr").removeClass("hideloader");
                        $.ajax({
                            url: "api/privacyPolicyProcess.php",
                            type: "POST",
                            data: new FormData(this),
                            contentType: false,
                            cache: false,
                            processData:false,
                            success: function(data)
                            {
                                var Status = data.Status;
                                var Message = data.Message;
                                if (Status == "Success"){
                                   location.reload();
                                    $(".loaderr").addClass("hideloader");
                                }
                                else{
                                    showMessage(Message,"red");
                                    $(".loaderr").removeClass("hideloader");
                                }
                            }
                        });
                    });
                    delete CKEDITOR.instances[ 'editor name' ];
                </script>
