<?php
error_reporting(0);
include("Constants/configuration.php");
include("Constants/dbConfig.php");
include("Constants/functions.php");
require_once "Classes/USERS.php";
$userClass = new Modals\USERS();
if(isset($_REQUEST["_"])){
    session_start();
    $_SESSION['capd'] = $_REQUEST['_'];
    header("Location:../index.php?_=cpd");
}
else {
    $requiredFeilds = array("type");
    $response = RequiredFields($_REQUEST, $requiredFeilds);
    if ($response[Status] != Success) {
        $userClass->apiResponse($response);
        return false;
    }
    $type = $_REQUEST['type'];
    if ($type == "getSize") {
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        ($response = $userClass->getSize($user_id));
        if ($response[STATUS] == Error) {
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if ($type == "getDatafromOrd") {
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->getDatafromOrd($user_id);
        if ($response[STATUS] == Error) {
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }

   else if($type == "getAllMatchingData"){
        $response = $userClass->getAllMatchingData();
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }

    else if($type == "getParticularUserData"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->getParticularUserData($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "resetValues"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->resetData($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "updateDimensions"){
        $requiredfields = array('user_id','height','weight','age','gender','race','neck','brand','size','waist');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $height = trim($_REQUEST['height']);
        $weight = trim($_REQUEST['weight']);
        $age = trim($_REQUEST['age']);
        $gender = trim($_REQUEST['gender']);
        $race = trim($_REQUEST['race']);
        $neck = trim($_REQUEST['neck']);
        $brand = trim($_REQUEST['brand']);
        $size = trim($_REQUEST['size']);
        $waist = trim($_REQUEST['waist']);
        $response = $userClass->updateDimensions($user_id,$height,$weight,$age,$gender,$race,$neck,$brand,$size,$waist);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "uploadord"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->uploadOrd($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "isOrdExist"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->isOrdExist($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "getMyFiles"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = trim($_REQUEST['user_id']);
        $response = $userClass->getMyFiles($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "deleteFile"){
        $requiredfields = array('file_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $file_id = trim($_REQUEST['file_id']);
        $response = $userClass->deleteFile($file_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "storeTwinData"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $collar = $_POST['collar'];
        $chest = $_POST['chest'];
        $stomach = $_POST['stoamch'];
        $seat = $_POST['seat'];
        $bicep = $_POST['bicep'];
        $back_shoulder = $_POST['back_shoulder'];
        $front_shoulder = $_POST['front_shoulder'];
        $sleeve_length_right = $_POST['sleeve_length_right'];
        $sleeve_length_left = $_POST['sleeve_length_left'];
        $thigh = $_POST['thigh'];
        $nape_to_waist = $_POST['nape_to_waist'];
        $front_waist_length = $_POST['front_waist_length'];
        $wrist = $_POST['wrist'];
        $waist = $_POST['waist'];
        $calf = $_POST['calf'];
        $back_waist_height = $_POST['back_waist_height'];
        $front_waist_height = $_POST['front_waist_height'];
        $knee = $_POST['knee'];
        $back_jacket_length = $_POST['back_jacket_length'];
        $u_rise = $_POST['u_rise'];
        $pant_left_outseam = $_POST['pant_left_outseam'];
        $pant_right_outseam = $_POST['pant_right_outseam'];
        $bottom = $_POST['bottom'];
        $user_id = $_POST['user_id'];
        $unit_type = $_POST['ut'];
        $shoulder_slope_left = $_POST['shoulder_slope_left'];
        $shoulder_slope_right = $_POST['shoulder_slope_right'];
        $response = $userClass->storeTwinData($collar,$chest,$stomach,$seat,$bicep,$back_shoulder,$front_shoulder,
        $sleeve_length_right,$sleeve_length_left,$thigh,$nape_to_waist,$front_waist_length,$wrist,$waist,$calf,
        $back_waist_height,$front_waist_height,$knee,$back_jacket_length,$u_rise,$pant_left_outseam,$pant_right_outseam,
        $bottom,$user_id,$unit_type,$shoulder_slope_left,$shoulder_slope_right);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "styleGenieUpdate"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $first_ques = $_POST['first_ques'];
        $second_ques = $_POST['second_ques'];
        $third_ques = $_POST['third_ques'];
        $forth_ques = $_POST['forth_ques'];
        $fifth_ques = $_POST['fifth_ques'];
        $sixth_ques = $_POST['sixth_ques'];
        $shirt_que_1 = $_POST['shirt_que_1'];
        $shirt_que_2 = $_POST['shirt_que_2'];
        $shirt_que_3 = $_POST['shirt_que_3'];
        $shirt_que_4 = $_POST['shirt_que_4'];
        $shirt_que_5 = $_POST['shirt_que_5'];
        $shirt_que_6 = $_POST['shirt_que_6'];
        $suit_que_1 = $_POST['suit_que_1'];
        $suit_que_2 = $_POST['suit_que_2'];
        $suit_que_3 = $_POST['suit_que_3'];
        $suit_que_4 = $_POST['suit_que_4'];
        $suit_que_5 = $_POST['suit_que_5'];
        $vest_que_1 = $_POST['vest_que_1'];
        $pant_que_1 = $_POST['pant_que_1'];
        $user_id = $_POST['user_id'];
        $sixth_ques_stoop = $_POST['sixth_ques_stoop'];
        $response = $userClass->styleGenieUpdate($first_ques,$second_ques,$third_ques,$forth_ques,$fifth_ques,$sixth_ques,
        $shirt_que_1,$shirt_que_2,$shirt_que_3,$shirt_que_4,$shirt_que_5,$shirt_que_6,$suit_que_1,$suit_que_2,$suit_que_3,
        $suit_que_4,$suit_que_5,$vest_que_1,$pant_que_1,$user_id,$sixth_ques_stoop);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "getStyleGenieData"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = $_POST['user_id'];
        $response = $userClass->getStyleGenieData($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "queryProcess"){
        $requiredfields = array('garment_type','body_param','question_type','answer');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $garment_type = $_POST['garment_type'];
        $body_param = $_POST['body_param'];
        $question_type = $_POST['question_type'];
        $answer = $_POST['answer'];
        $response = $userClass->queryProcess($garment_type,$body_param,$question_type,$answer);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "queryProcess2"){
        $requiredfields = array('body_param','ques_answer');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $body_param = $_POST['body_param'];
        $ques_answer = $_POST['ques_answer'];
        $response = $userClass->queryProcess2($body_param,$ques_answer);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "pantpleat"){
        $requiredfields = array('pant_answer');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $pant_answer = $_POST['pant_answer'];
        $response = $userClass->pantpleat($pant_answer);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "storeFinalMeasurement"){
        /*$requiredfields = array('shirt_collar','jacket_collar','shirt_chest','jacket_chest','shirt_stomach',
        'jacket_stomach','shirt_seat','jacket_seat','shirt_bicep','jacket_bicep','shirt_back_shoulder',
        'jacket_back_shoulder','shirt_front_shoulder','jacket_front_shoulder','shirt_sleeve_left','jacket_sleeve_left',
        'shirt_sleeve_right','jacket_sleeve_right','shirt_thigh','jacket_thigh','shirt_nape_to_waist',
        'jacket_nape_to_waist','shirt_front_waist_length','jacket_front_waist_length','shirt_wrist','jacket_wrist',
        'shirt_calf','jacket_calf','shirt_back_waist_height','jacket_back_waist_height','shirt_front_waist_height',
        'jacket_front_waist_height','shirt_knee','jacket_knee','shirt_back_jacket_length','jacket_back_jacket_length',
        'shirt_u_rise','jacket_u_rise','shirt_pant_left_outseam','jacket_pant_left_outseam','shirt_pant_right_outseam',
        'jacket_pant_right_outseam','shirt_bottom','jacket_bottom','meas_name','user_id','unit_type','vest_collar',
        'pant_collar','vest_chest','pant_chest','vest_stomach','pant_stomach','vest_seat','pant_seat','vest_bicep',
        'pant_bicep','vest_back_shoulder','pant_back_shoulder','vest_front_shoulder','pant_front_shoulder',
        'vest_sleeve_left','pant_sleeve_left','vest_sleeve_right','pant_sleeve_right','vest_thigh','pant_thigh',
        'vest_nape_to_waist','pant_nape_to_waist','vest_front_waist_length','pant_front_waist_length','vest_wrist',
        'pant_wrist','vest_calf','pant_calf','vest_back_waist_height','pant_back_waist_height','vest_front_waist_height',
        'pant_front_waist_height','vest_knee','pant_knee','vest_back_jacket_length','pant_back_jacket_length',
        'vest_u_rise','pant_u_rise','vest_pant_left_outseam','pant_pant_left_outseam','vest_pant_right_outseam',
        'pant_pant_right_outseam','vest_bottom','pant_bottom');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }*/
        $shirt_collar = $_POST['shirt_collar'];
        $jacket_collar = $_POST['jacket_collar'];
        $shirt_chest = $_POST['shirt_chest'];
        $jacket_chest = $_POST['jacket_chest'];
        $shirt_stomach = $_POST['shirt_stomach'];
        $jacket_stomach = $_POST['jacket_stomach'];
        $shirt_seat = $_POST['shirt_seat'];
        $jacket_seat = $_POST['jacket_seat'];
        $shirt_bicep = $_POST['shirt_bicep'];
        $jacket_bicep = $_POST['jacket_bicep'];
        $shirt_back_shoulder = $_POST['shirt_back_shoulder'];
        $jacket_back_shoulder = $_POST['jacket_back_shoulder'];
        $shirt_front_shoulder = $_POST['shirt_front_shoulder'];
        $jacket_front_shoulder = $_POST['jacket_front_shoulder'];
        $shirt_sleeve_left = $_POST['shirt_sleeve_left'];
        $jacket_sleeve_left = $_POST['jacket_sleeve_left'];
        $shirt_sleeve_right = $_POST['shirt_sleeve_right'];
        $jacket_sleeve_right = $_POST['jacket_sleeve_right'];
        $shirt_thigh = $_POST['shirt_thigh'];
        $jacket_thigh = $_POST['jacket_thigh'];
        $shirt_nape_to_waist = $_POST['shirt_nape_to_waist'];
        $jacket_nape_to_waist = $_POST['jacket_nape_to_waist'];
        $shirt_front_waist_length = $_POST['shirt_front_waist_length'];
        $jacket_front_waist_length = $_POST['jacket_front_waist_length'];
        $shirt_wrist = $_POST['shirt_wrist'];
        $jacket_wrist = $_POST['jacket_wrist'];
        $shirt_waist = $_POST['shirt_waist'];
        $jacket_waist = $_POST['jacket_waist'];
        $shirt_calf = $_POST['shirt_calf'];
        $jacket_calf = $_POST['jacket_calf'];
        $shirt_back_waist_height = $_POST['shirt_back_waist_height'];
        $jacket_back_waist_height = $_POST['jacket_back_waist_height'];
        $shirt_front_waist_height = $_POST['shirt_front_waist_height'];
        $jacket_front_waist_height = $_POST['jacket_front_waist_height'];
        $shirt_knee = $_POST['shirt_knee'];
        $jacket_knee = $_POST['jacket_knee'];
        $shirt_back_jacket_length = $_POST['shirt_back_jacket_length'];
        $jacket_back_jacket_length = $_POST['jacket_back_jacket_length'];
        $shirt_u_rise = $_POST['shirt_u_rise'];
        $jacket_u_rise = $_POST['jacket_u_rise'];
        $shirt_pant_left_outseam = $_POST['shirt_pant_left_outseam'];
        $jacket_pant_left_outseam = $_POST['jacket_pant_left_outseam'];
        $shirt_pant_right_outseam = $_POST['shirt_pant_right_outseam'];
        $jacket_pant_right_outseam = $_POST['jacket_pant_right_outseam'];
        $shirt_bottom = $_POST['shirt_bottom'];
        $jacket_bottom = $_POST['jacket_bottom'];
        $meas_name = $_POST['meas_name'];
        $user_id = $_POST['user_id'];
        $unit_type = $_POST['unit_type'];
        $vest_collar=$_REQUEST['vest_collar'];
        $pant_collar=$_REQUEST['pant_collar'];
        $vest_chest=$_REQUEST['vest_chest'];
        $pant_chest=$_REQUEST['pant_chest'];
        $vest_stomach=$_REQUEST['vest_stomach'];
        $pant_stomach=$_REQUEST['pant_stomach'];
        $vest_seat=$_REQUEST['vest_seat'];
        $pant_seat=$_REQUEST['pant_seat'];
        $pant1_seat=$_REQUEST['pant1_seat'];
        $pant11_seat=$_REQUEST['pant11_seat'];
        $pant2_seat=$_REQUEST['pant2_seat'];
        $pant3_seat=$_REQUEST['pant3_seat'];
        $vest_bicep=$_REQUEST['vest_bicep'];
        $pant_bicep=$_REQUEST['pant_bicep'];
        $vest_back_shoulder=$_REQUEST['vest_back_shoulder'];
        $pant_back_shoulder=$_REQUEST['pant_back_shoulder'];
        $vest_front_shoulder=$_REQUEST['vest_front_shoulder'];
        $pant_front_shoulder=$_REQUEST['pant_front_shoulder'];
        $vest_sleeve_left=$_REQUEST['vest_sleeve_left'];
        $pant_sleeve_left=$_REQUEST['pant_sleeve_left'];
        $vest_sleeve_right=$_REQUEST['vest_sleeve_right'];
        $pant_sleeve_right=$_REQUEST['pant_sleeve_right'];
        $vest_thigh=$_REQUEST['vest_thigh'];
        $pant_thigh=$_REQUEST['pant_thigh'];
        $vest_nape_to_waist=$_REQUEST['vest_nape_to_waist'];
        $pant_nape_to_waist=$_REQUEST['pant_nape_to_waist'];
        $vest_front_waist_length=$_REQUEST['vest_front_waist_length'];
        $pant_front_waist_length=$_REQUEST['pant_front_waist_length'];
        $vest_wrist=$_REQUEST['vest_wrist'];
        $pant_wrist=$_REQUEST['pant_wrist'];
        $vest_waist=$_REQUEST['vest_waist'];
        $pant_waist=$_REQUEST['pant_waist'];
        $vest_calf=$_REQUEST['vest_calf'];
        $pant_calf=$_REQUEST['pant_calf'];
        $vest_back_waist_height=$_REQUEST['vest_back_waist_height'];
        $pant_back_waist_height=$_REQUEST['pant_back_waist_height'];
        $vest_front_waist_height=$_REQUEST['vest_front_waist_height'];
        $pant_front_waist_height=$_REQUEST['pant_front_waist_height'];
        $vest_knee=$_REQUEST['vest_knee'];
        $pant_knee=$_REQUEST['pant_knee'];
        $vest_back_jacket_length=$_REQUEST['vest_back_jacket_length'];
        $pant_back_jacket_length=$_REQUEST['pant_back_jacket_length'];
        $vest_u_rise=$_REQUEST['vest_u_rise'];
        $pant_u_rise=$_REQUEST['pant_u_rise'];
        $vest_pant_left_outseam=$_REQUEST['vest_pant_left_outseam'];
        $pant_pant_left_outseam=$_REQUEST['pant_pant_left_outseam'];
        $vest_pant_right_outseam=$_REQUEST['vest_pant_right_outseam'];
        $pant_pant_right_outseam=$_REQUEST['pant_pant_right_outseam'];
        $vest_bottom=$_REQUEST['vest_bottom'];
        $pant_bottom=$_REQUEST['pant_bottom'];


        $response = $userClass->storeFinalMeasurement($shirt_collar,$jacket_collar,$shirt_chest,$jacket_chest,$shirt_stomach,
        $jacket_stomach,$shirt_seat,$jacket_seat,$shirt_bicep,$jacket_bicep,$shirt_back_shoulder,
        $jacket_back_shoulder,$shirt_front_shoulder,$jacket_front_shoulder,$shirt_sleeve_left,$jacket_sleeve_left,
        $shirt_sleeve_right,$jacket_sleeve_right,$shirt_thigh,$jacket_thigh,$shirt_nape_to_waist,
        $jacket_nape_to_waist,$shirt_front_waist_length,$jacket_front_waist_length,$shirt_wrist,$jacket_wrist,$shirt_waist,$jacket_waist,
        $shirt_calf,$jacket_calf,$shirt_back_waist_height,$jacket_back_waist_height,$shirt_front_waist_height,
        $jacket_front_waist_height,$shirt_knee,$jacket_knee,$shirt_back_jacket_length,$jacket_back_jacket_length,
        $shirt_u_rise,$jacket_u_rise,$shirt_pant_left_outseam,$jacket_pant_left_outseam,$shirt_pant_right_outseam,
        $jacket_pant_right_outseam,$shirt_bottom,$jacket_bottom,$meas_name,$user_id,$unit_type,$vest_collar,
        $pant_collar,$vest_chest,$pant_chest,$vest_stomach,$pant_stomach,$vest_seat,$pant_seat,$vest_bicep,$pant_bicep,
        $vest_back_shoulder,$pant_back_shoulder,$vest_front_shoulder,$pant_front_shoulder,$vest_sleeve_left,
        $pant_sleeve_left,$vest_sleeve_right,$pant_sleeve_right,$vest_thigh,$pant_thigh,$vest_nape_to_waist,
        $pant_nape_to_waist,$vest_front_waist_length,$pant_front_waist_length,$vest_wrist,$pant_wrist,$vest_waist,$pant_waist,$vest_calf,
        $pant_calf,$vest_back_waist_height,$pant_back_waist_height,$vest_front_waist_height,$pant_front_waist_height,
        $vest_knee,$pant_knee,$vest_back_jacket_length,$pant_back_jacket_length,$vest_u_rise,$pant_u_rise,
        $vest_pant_left_outseam,$pant_pant_left_outseam,$vest_pant_right_outseam,$pant_pant_right_outseam,$vest_bottom,
        $pant_bottom,$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
   else if($type == "updateFinalMeasurement"){
        $shirt_collar = $_POST['shirt_collar'];
        $jacket_collar = $_POST['jacket_collar'];
        $shirt_chest = $_POST['shirt_chest'];
        $jacket_chest = $_POST['jacket_chest'];
        $shirt_stomach = $_POST['shirt_stomach'];
        $jacket_stomach = $_POST['jacket_stomach'];
        $shirt_seat = $_POST['shirt_seat'];
        $jacket_seat = $_POST['jacket_seat'];
        $shirt_bicep = $_POST['shirt_bicep'];
        $jacket_bicep = $_POST['jacket_bicep'];
        $shirt_back_shoulder = $_POST['shirt_back_shoulder'];
        $jacket_back_shoulder = $_POST['jacket_back_shoulder'];
        $shirt_front_shoulder = $_POST['shirt_front_shoulder'];
        $jacket_front_shoulder = $_POST['jacket_front_shoulder'];
        $shirt_sleeve_left = $_POST['shirt_sleeve_left'];
        $jacket_sleeve_left = $_POST['jacket_sleeve_left'];
        $shirt_sleeve_right = $_POST['shirt_sleeve_right'];
        $jacket_sleeve_right = $_POST['jacket_sleeve_right'];
        $shirt_thigh = $_POST['shirt_thigh'];
        $jacket_thigh = $_POST['jacket_thigh'];
        $shirt_nape_to_waist = $_POST['shirt_nape_to_waist'];
        $jacket_nape_to_waist = $_POST['jacket_nape_to_waist'];
        $shirt_front_waist_length = $_POST['shirt_front_waist_length'];
        $jacket_front_waist_length = $_POST['jacket_front_waist_length'];
        $shirt_wrist = $_POST['shirt_wrist'];
        $jacket_wrist = $_POST['jacket_wrist'];
        $shirt_waist = $_POST['shirt_waist'];
        $jacket_waist = $_POST['jacket_waist'];
        $shirt_calf = $_POST['shirt_calf'];
        $jacket_calf = $_POST['jacket_calf'];
        $shirt_back_waist_height = $_POST['shirt_back_waist_height'];
        $jacket_back_waist_height = $_POST['jacket_back_waist_height'];
        $shirt_front_waist_height = $_POST['shirt_front_waist_height'];
        $jacket_front_waist_height = $_POST['jacket_front_waist_height'];
        $shirt_knee = $_POST['shirt_knee'];
        $jacket_knee = $_POST['jacket_knee'];
        $shirt_back_jacket_length = $_POST['shirt_back_jacket_length'];
        $jacket_back_jacket_length = $_POST['jacket_back_jacket_length'];
        $shirt_u_rise = $_POST['shirt_u_rise'];
        $jacket_u_rise = $_POST['jacket_u_rise'];
        $shirt_pant_left_outseam = $_POST['shirt_pant_left_outseam'];
        $jacket_pant_left_outseam = $_POST['jacket_pant_left_outseam'];
        $shirt_pant_right_outseam = $_POST['shirt_pant_right_outseam'];
        $jacket_pant_right_outseam = $_POST['jacket_pant_right_outseam'];
        $shirt_bottom = $_POST['shirt_bottom'];
        $jacket_bottom = $_POST['jacket_bottom'];
        $meas_id = $_POST['meas_id'];
        $unit_type = $_POST['unit_type'];
        $vest_collar=$_REQUEST['vest_collar'];
        $pant_collar=$_REQUEST['pant_collar'];
        $vest_chest=$_REQUEST['vest_chest'];
        $pant_chest=$_REQUEST['pant_chest'];
        $vest_stomach=$_REQUEST['vest_stomach'];
        $pant_stomach=$_REQUEST['pant_stomach'];
        $vest_seat=$_REQUEST['vest_seat'];
        $pant_seat=$_REQUEST['pant_seat'];
        $vest_bicep=$_REQUEST['vest_bicep'];
        $pant_bicep=$_REQUEST['pant_bicep'];
        $vest_back_shoulder=$_REQUEST['vest_back_shoulder'];
        $pant_back_shoulder=$_REQUEST['pant_back_shoulder'];
        $vest_front_shoulder=$_REQUEST['vest_front_shoulder'];
        $pant_front_shoulder=$_REQUEST['pant_front_shoulder'];
        $vest_sleeve_left=$_REQUEST['vest_sleeve_left'];
        $pant_sleeve_left=$_REQUEST['pant_sleeve_left'];
        $vest_sleeve_right=$_REQUEST['vest_sleeve_right'];
        $pant_sleeve_right=$_REQUEST['pant_sleeve_right'];
        $vest_thigh=$_REQUEST['vest_thigh'];
        $pant_thigh=$_REQUEST['pant_thigh'];
        $vest_nape_to_waist=$_REQUEST['vest_nape_to_waist'];
        $pant_nape_to_waist=$_REQUEST['pant_nape_to_waist'];
        $vest_front_waist_length=$_REQUEST['vest_front_waist_length'];
        $pant_front_waist_length=$_REQUEST['pant_front_waist_length'];
        $vest_wrist=$_REQUEST['vest_wrist'];
        $pant_wrist=$_REQUEST['pant_wrist'];
        $vest_waist=$_REQUEST['vest_waist'];
        $pant_waist=$_REQUEST['pant_waist'];
        $vest_calf=$_REQUEST['vest_calf'];
        $pant_calf=$_REQUEST['pant_calf'];
        $vest_back_waist_height=$_REQUEST['vest_back_waist_height'];
        $pant_back_waist_height=$_REQUEST['pant_back_waist_height'];
        $vest_front_waist_height=$_REQUEST['vest_front_waist_height'];
        $pant_front_waist_height=$_REQUEST['pant_front_waist_height'];
        $vest_knee=$_REQUEST['vest_knee'];
        $pant_knee=$_REQUEST['pant_knee'];
        $vest_back_jacket_length=$_REQUEST['vest_back_jacket_length'];
        $pant_back_jacket_length=$_REQUEST['pant_back_jacket_length'];
        $vest_u_rise=$_REQUEST['vest_u_rise'];
        $pant_u_rise=$_REQUEST['pant_u_rise'];
        $vest_pant_left_outseam=$_REQUEST['vest_pant_left_outseam'];
        $pant_pant_left_outseam=$_REQUEST['pant_pant_left_outseam'];
        $vest_pant_right_outseam=$_REQUEST['vest_pant_right_outseam'];
        $pant_pant_right_outseam=$_REQUEST['pant_pant_right_outseam'];
        $vest_bottom=$_REQUEST['vest_bottom'];
        $pant_bottom=$_REQUEST['pant_bottom'];
        /*$pant1_seat=$_REQUEST['pant1_seat'];
        $pant11_seat=$_REQUEST['pant11_seat'];
        $pant2_seat=$_REQUEST['pant2_seat'];
        $pant3_seat=$_REQUEST['pant3_seat'];*/


        $response = $userClass->updateFinalMeasurement($shirt_collar,$jacket_collar,$shirt_chest,$jacket_chest,$shirt_stomach,
        $jacket_stomach,$shirt_seat,$jacket_seat,$shirt_bicep,$jacket_bicep,$shirt_back_shoulder,
        $jacket_back_shoulder,$shirt_front_shoulder,$jacket_front_shoulder,$shirt_sleeve_left,$jacket_sleeve_left,
        $shirt_sleeve_right,$jacket_sleeve_right,$shirt_thigh,$jacket_thigh,$shirt_nape_to_waist,
        $jacket_nape_to_waist,$shirt_front_waist_length,$jacket_front_waist_length,$shirt_wrist,$jacket_wrist,$shirt_waist,$jacket_waist,
        $shirt_calf,$jacket_calf,$shirt_back_waist_height,$jacket_back_waist_height,$shirt_front_waist_height,
        $jacket_front_waist_height,$shirt_knee,$jacket_knee,$shirt_back_jacket_length,$jacket_back_jacket_length,
        $shirt_u_rise,$jacket_u_rise,$shirt_pant_left_outseam,$jacket_pant_left_outseam,$shirt_pant_right_outseam,
        $jacket_pant_right_outseam,$shirt_bottom,$jacket_bottom,$meas_id,$unit_type,$vest_collar,
        $pant_collar,$vest_chest,$pant_chest,$vest_stomach,$pant_stomach,$vest_seat,$pant_seat,$vest_bicep,$pant_bicep,
        $vest_back_shoulder,$pant_back_shoulder,$vest_front_shoulder,$pant_front_shoulder,$vest_sleeve_left,
        $pant_sleeve_left,$vest_sleeve_right,$pant_sleeve_right,$vest_thigh,$pant_thigh,$vest_nape_to_waist,
        $pant_nape_to_waist,$vest_front_waist_length,$pant_front_waist_length,$vest_wrist,$pant_wrist,$vest_waist,$pant_waist,$vest_calf,
        $pant_calf,$vest_back_waist_height,$pant_back_waist_height,$vest_front_waist_height,$pant_front_waist_height,
        $vest_knee,$pant_knee,$vest_back_jacket_length,$pant_back_jacket_length,$vest_u_rise,$pant_u_rise,
        $vest_pant_left_outseam,$pant_pant_left_outseam,$vest_pant_right_outseam,$pant_pant_right_outseam,$vest_bottom,
        $pant_bottom/*,$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat*/);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "updateFinalMeasurementName"){
        $shirt_collar = $_POST['shirt_collar'];
        $jacket_collar = $_POST['jacket_collar'];
        $shirt_chest = $_POST['shirt_chest'];
        $jacket_chest = $_POST['jacket_chest'];
        $shirt_stomach = $_POST['shirt_stomach'];
        $jacket_stomach = $_POST['jacket_stomach'];
        $shirt_seat = $_POST['shirt_seat'];
        $jacket_seat = $_POST['jacket_seat'];
        $shirt_bicep = $_POST['shirt_bicep'];
        $jacket_bicep = $_POST['jacket_bicep'];
        $shirt_back_shoulder = $_POST['shirt_back_shoulder'];
        $jacket_back_shoulder = $_POST['jacket_back_shoulder'];
        $shirt_front_shoulder = $_POST['shirt_front_shoulder'];
        $jacket_front_shoulder = $_POST['jacket_front_shoulder'];
        $shirt_sleeve_left = $_POST['shirt_sleeve_left'];
        $jacket_sleeve_left = $_POST['jacket_sleeve_left'];
        $shirt_sleeve_right = $_POST['shirt_sleeve_right'];
        $jacket_sleeve_right = $_POST['jacket_sleeve_right'];
        $shirt_thigh = $_POST['shirt_thigh'];
        $jacket_thigh = $_POST['jacket_thigh'];
        $shirt_nape_to_waist = $_POST['shirt_nape_to_waist'];
        $jacket_nape_to_waist = $_POST['jacket_nape_to_waist'];
        $shirt_front_waist_length = $_POST['shirt_front_waist_length'];
        $jacket_front_waist_length = $_POST['jacket_front_waist_length'];
        $shirt_wrist = $_POST['shirt_wrist'];
        $jacket_wrist = $_POST['jacket_wrist'];
        $shirt_waist = $_POST['shirt_waist'];
        $jacket_waist = $_POST['jacket_waist'];
        $shirt_calf = $_POST['shirt_calf'];
        $jacket_calf = $_POST['jacket_calf'];
        $shirt_back_waist_height = $_POST['shirt_back_waist_height'];
        $jacket_back_waist_height = $_POST['jacket_back_waist_height'];
        $shirt_front_waist_height = $_POST['shirt_front_waist_height'];
        $jacket_front_waist_height = $_POST['jacket_front_waist_height'];
        $shirt_knee = $_POST['shirt_knee'];
        $jacket_knee = $_POST['jacket_knee'];
        $shirt_back_jacket_length = $_POST['shirt_back_jacket_length'];
        $jacket_back_jacket_length = $_POST['jacket_back_jacket_length'];
        $shirt_u_rise = $_POST['shirt_u_rise'];
        $jacket_u_rise = $_POST['jacket_u_rise'];
        $shirt_pant_left_outseam = $_POST['shirt_pant_left_outseam'];
        $jacket_pant_left_outseam = $_POST['jacket_pant_left_outseam'];
        $shirt_pant_right_outseam = $_POST['shirt_pant_right_outseam'];
        $jacket_pant_right_outseam = $_POST['jacket_pant_right_outseam'];
        $shirt_bottom = $_POST['shirt_bottom'];
        $jacket_bottom = $_POST['jacket_bottom'];
        $meas_name = $_POST['meas_name'];
        $unit_type = $_POST['unit_type'];
        $vest_collar=$_REQUEST['vest_collar'];
        $pant_collar=$_REQUEST['pant_collar'];
        $vest_chest=$_REQUEST['vest_chest'];
        $pant_chest=$_REQUEST['pant_chest'];
        $vest_stomach=$_REQUEST['vest_stomach'];
        $pant_stomach=$_REQUEST['pant_stomach'];
        $vest_seat=$_REQUEST['vest_seat'];
        $pant_seat=$_REQUEST['pant_seat'];
        $vest_bicep=$_REQUEST['vest_bicep'];
        $pant_bicep=$_REQUEST['pant_bicep'];
        $vest_back_shoulder=$_REQUEST['vest_back_shoulder'];
        $pant_back_shoulder=$_REQUEST['pant_back_shoulder'];
        $vest_front_shoulder=$_REQUEST['vest_front_shoulder'];
        $pant_front_shoulder=$_REQUEST['pant_front_shoulder'];
        $vest_sleeve_left=$_REQUEST['vest_sleeve_left'];
        $pant_sleeve_left=$_REQUEST['pant_sleeve_left'];
        $vest_sleeve_right=$_REQUEST['vest_sleeve_right'];
        $pant_sleeve_right=$_REQUEST['pant_sleeve_right'];
        $vest_thigh=$_REQUEST['vest_thigh'];
        $pant_thigh=$_REQUEST['pant_thigh'];
        $vest_nape_to_waist=$_REQUEST['vest_nape_to_waist'];
        $pant_nape_to_waist=$_REQUEST['pant_nape_to_waist'];
        $vest_front_waist_length=$_REQUEST['vest_front_waist_length'];
        $pant_front_waist_length=$_REQUEST['pant_front_waist_length'];
        $vest_wrist=$_REQUEST['vest_wrist'];
        $pant_wrist=$_REQUEST['pant_wrist'];
        $vest_waist=$_REQUEST['vest_waist'];
        $pant_waist=$_REQUEST['pant_waist'];
        $vest_calf=$_REQUEST['vest_calf'];
        $pant_calf=$_REQUEST['pant_calf'];
        $vest_back_waist_height=$_REQUEST['vest_back_waist_height'];
        $pant_back_waist_height=$_REQUEST['pant_back_waist_height'];
        $vest_front_waist_height=$_REQUEST['vest_front_waist_height'];
        $pant_front_waist_height=$_REQUEST['pant_front_waist_height'];
        $vest_knee=$_REQUEST['vest_knee'];
        $pant_knee=$_REQUEST['pant_knee'];
        $vest_back_jacket_length=$_REQUEST['vest_back_jacket_length'];
        $pant_back_jacket_length=$_REQUEST['pant_back_jacket_length'];
        $vest_u_rise=$_REQUEST['vest_u_rise'];
        $pant_u_rise=$_REQUEST['pant_u_rise'];
        $vest_pant_left_outseam=$_REQUEST['vest_pant_left_outseam'];
        $pant_pant_left_outseam=$_REQUEST['pant_pant_left_outseam'];
        $vest_pant_right_outseam=$_REQUEST['vest_pant_right_outseam'];
        $pant_pant_right_outseam=$_REQUEST['pant_pant_right_outseam'];
        $vest_bottom=$_REQUEST['vest_bottom'];
        $pant_bottom=$_REQUEST['pant_bottom'];
        $user_id=$_REQUEST['user_id'];
        /*$pant1_seat=$_REQUEST['pant1_seat'];
        $pant11_seat=$_REQUEST['pant11_seat'];
        $pant2_seat=$_REQUEST['pant2_seat'];
        $pant3_seat=$_REQUEST['pant3_seat'];*/
        $response = $userClass->updateFinalMeasurementName($shirt_collar,$jacket_collar,$shirt_chest,$jacket_chest,$shirt_stomach,
        $jacket_stomach,$shirt_seat,$jacket_seat,$shirt_bicep,$jacket_bicep,$shirt_back_shoulder,
        $jacket_back_shoulder,$shirt_front_shoulder,$jacket_front_shoulder,$shirt_sleeve_left,$jacket_sleeve_left,
        $shirt_sleeve_right,$jacket_sleeve_right,$shirt_thigh,$jacket_thigh,$shirt_nape_to_waist,
        $jacket_nape_to_waist,$shirt_front_waist_length,$jacket_front_waist_length,$shirt_wrist,$jacket_wrist,$shirt_waist,$jacket_waist,
        $shirt_calf,$jacket_calf,$shirt_back_waist_height,$jacket_back_waist_height,$shirt_front_waist_height,
        $jacket_front_waist_height,$shirt_knee,$jacket_knee,$shirt_back_jacket_length,$jacket_back_jacket_length,
        $shirt_u_rise,$jacket_u_rise,$shirt_pant_left_outseam,$jacket_pant_left_outseam,$shirt_pant_right_outseam,
        $jacket_pant_right_outseam,$shirt_bottom,$jacket_bottom,$meas_name,$unit_type,$vest_collar,
        $pant_collar,$vest_chest,$pant_chest,$vest_stomach,$pant_stomach,$vest_seat,$pant_seat,$vest_bicep,$pant_bicep,
        $vest_back_shoulder,$pant_back_shoulder,$vest_front_shoulder,$pant_front_shoulder,$vest_sleeve_left,
        $pant_sleeve_left,$vest_sleeve_right,$pant_sleeve_right,$vest_thigh,$pant_thigh,$vest_nape_to_waist,
        $pant_nape_to_waist,$vest_front_waist_length,$pant_front_waist_length,$vest_wrist,$pant_wrist,$vest_waist,$pant_waist,$vest_calf,
        $pant_calf,$vest_back_waist_height,$pant_back_waist_height,$vest_front_waist_height,$pant_front_waist_height,
        $vest_knee,$pant_knee,$vest_back_jacket_length,$pant_back_jacket_length,$vest_u_rise,$pant_u_rise,
        $vest_pant_left_outseam,$pant_pant_left_outseam,$vest_pant_right_outseam,$pant_pant_right_outseam,$vest_bottom,
        $pant_bottom,$user_id/*,$pant1_seat,$pant11_seat,$pant2_seat,$pant3_seat*/);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "isStyleGenieUsed"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = $_POST['user_id'];
        $response = $userClass->isStyleGenieUsed($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getAllMeasurement"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = $_POST['user_id'];
        $response = $userClass->getAllMeasurement($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getParticularMeasurement"){
        $requiredfields = array('meas_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $meas_id = $_POST['meas_id'];
        $response = $userClass->getParticularMeasurement($meas_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "deleteMeasurement"){
        $requiredfields = array('meas_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $meas_id = $_POST['meas_id'];
        $response = $userClass->deleteMeasurement($meas_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getAllowanceData"){
        $requiredfields = array('garment_type');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $garment_type = $_POST['garment_type'];
        $response = $userClass->getAllowanceData($garment_type);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "updateAllowance"){
        $requiredfields = array('id','value');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $id = $_POST['id'];
        $value = $_POST['value'];
        $response = $userClass->updateAllowance($id,$value);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getBodyForwardvalues"){
        $requiredfields = array('user_id');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $user_id = $_POST['user_id'];
        $response = $userClass->getBodyForwardvalues($user_id);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "addCartMeasurement"){
        $requiredfields = array('meas_id','cart_id','meas_name');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $meas_id = $_POST['meas_id'];
        $cart_id = $_POST['cart_id'];
        $meas_name = $_POST['meas_name'];
        $response = $userClass->addCartMeasurement($cart_id,$meas_id,$meas_name);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "getBodyStyle"){
        $requiredfields = array('left_side','right_side','combination');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $left_side = $_POST['left_side'];
        $right_side = $_POST['right_side'];
        $combination = $_POST['combination'];
        $response = $userClass->getBodyStyle($left_side,$right_side,$combination);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getBodyStyleData"){
        $response = $userClass->getBodyStyleData();
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "minmaxdata"){
        $response = $userClass->minmaxdata();
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "getpleatstyledata"){
        $response = $userClass->getpleatstyledata();
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "updateBodyStyle"){
        $requiredfields = array('row_id','value','option_name');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $row_id = $_POST['row_id'];
        $value = $_POST['value'];
        $option_name = $_POST['option_name'];
        $response = $userClass->updateBodyStyle($row_id,$value,$option_name);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "updatePleatData"){
        $requiredfields = array('row_id','value','option_name');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $row_id = $_POST['row_id'];
        $value = $_POST['value'];
        $option_name = $_POST['option_name'];
        $response = $userClass->updatePleatData($row_id,$value,$option_name);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }else if($type == "updateminmaxData"){
        $requiredfields = array('row_id','value','option_name');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $row_id = $_POST['row_id'];
        $value = $_POST['value'];
        $option_name = $_POST['option_name'];
        $response = $userClass->updateminmaxData($row_id,$value,$option_name);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "getMatchingData"){
        $requiredfields = array('size_type');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $size_type = $_POST['size_type'];
        $response = $userClass->getMatchingData($size_type);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "checkFabricStatus"){
        $requiredfields = array('fabricCode');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $fabricCode = $_POST['fabricCode'];
        $response = $userClass->checkFabricStatus($fabricCode);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "updateSizeMatching"){
        $requiredfields = array('row_id','value','colm_val');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $row_id = $_POST['row_id'];
        $value = $_POST['value'];
        $colm_val = $_POST['colm_val'];
        $response = $userClass->updateSizeMatching($row_id,$value,$colm_val);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else if($type == "getMatchingRange"){
        $requiredfields = array('chest','stomach','bicep','waist','u_rise','thigh','collar','shoulder','back_jacket_length');
        $response = RequiredFields($_REQUEST, $requiredfields);
        if ($response['Status'] == 'Failure') {
            $userClass->apiResponse($response);
            return false;
        }
        $chest = $_POST['chest'];
        $stomach = $_POST['stomach'];
        $bicep = $_POST['bicep'];
        $waist = $_POST['waist'];
        $u_rise = $_POST['u_rise'];
        $thigh = $_POST['thigh'];
        $collar = $_POST['collar'];
        $shoulder = $_POST['shoulder'];
        $back_jacket_length = $_POST['back_jacket_length'];
        $response = $userClass->getMatchingRange($chest,$stomach,$bicep,$waist,$u_rise,$thigh,$collar,$shoulder,$back_jacket_length);
        if($response[Status] == Error){
            $userClass->apiResponse($response);
            return false;
        }
        $userClass->apiResponse($response);
    }
    else {
        $response[Status] = Error;
        $response[Message] = "502 UnAuthorized Access";
        $userClass->apiResponse($response);
    }
}
?>