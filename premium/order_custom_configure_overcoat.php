<?php
include('header.php');
include("front_header.php");
include("../admin/webserver/database/db.php");
?>
<style>
    .nav_menu{
        margin-bottom: 0px;
    }
    .top-bar {
        background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
    }
    .active {
        border: 2px solid #555;
        border-radius: 4px;
    }
    .primarysuggestion {
        height: auto;
        margin-left: 13px;
        margin-top: 10px;
        width: auto;
    }

    .steps {
        background: #fff none repeat scroll 0 0;
        border: 2px solid #aeaeae;
        border-radius: 7%;
        cursor: pointer;
        float: left;
        font-size: 13px;
        line-height: 17px;
        margin-bottom: 20px;
        margin-left: 2%;
        margin-top: 10px;
        max-height: 57px;
        padding-top: 9px;
        text-align: center;
        width: 10%;
    }

    .stepsactive {
        background: #c4996c none repeat scroll 0 0;
        border: 2px solid #c4996c;
        color: white;
    }

    .breakingline {
        margin-top: 38px;
        width: 501px;
    }
    .outputsugg {
        position: absolute;
        margin-left: 70px;
        z-index:-1;
    }
    .backbtnsugg
    {
        height:68%;
    }
    .backbtnsuggparentdiv
    {
        height: 130px;
        width: 130px;
    }
    .backbtnsuggparentdiv p
    {
        font-weight:normal;
        font-size: 12px;
        line-height: 18px!important;
    }
    #forcasual .primarysuggestion > img
    {
        padding: 40px;
    }
    #frontpockets{overflow-y: scroll; height: 660px; padding-top: 22px;}
    .okok {
        font-size: 19px;
        height: 52px;
        margin-left: 10px;
        margin-top: 10px;
        padding-top: 6px;
        width: 162px;
    }
    .stylecaption {
        margin-top: -54px;
    }
    .overflow-cmd{overflow-y: auto; height: 660px; padding-top: 22px;}
    .primarysuggestions {
        height: auto!important;
        margin-left: 10px;
        margin-top: 10px;
        width: auto;
    }
    .primarysuggestions {
        border: 2px solid #ccc;
        border-radius: 4px;
        float: left;
        height: 171px;
        margin-left: 42px;
        margin-top: 35px;
        text-align: center;
        width: 165px;
    }
    .nav-md .container.body .right_col{
        padding: 35px  1px  0 0;
        background: #fff;
    }
    .about-bottom{
        padding: 48px;
    }
    .steps{
        font-size: 12px!important;
    }
    .suggestionname{
        font-size: 12px;
    }
    .primarysuggestions img{
        width: 100%;
    }
</style>
<div class="right_col" role="main" style="overflow-y: auto">
    <div class="about-bottom wthree-3">

<div class="loader" style="display: none">
    <img src="../images/spin.gif" class="spinloader"/>
</div>
<div class="col-md-12">
    <div class="col-md-12" style="margin-top: 30px; margin-bottom: 80px;">
        <div class="col-md-12" style="padding:50px 0;display:none;">
            <div class="steps stepsactive" id="firstconfigover"></div>
        </div>
	    <p class="stylecaption">Our Collection</p>
        <div class="col-md-12" id="firstconfigovers" style="display: block; margin-left: -10px;">
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0001" onclick="overconfig('overcoatdefault0001')">
				<img src="../images/preconfig/overcoat/overcoatdefault0001.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0002" onclick="overconfig('overcoatdefault0002')">
				<img src="../images/preconfig/overcoat/overcoatdefault0002.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0003" onclick="overconfig('overcoatdefault0003')">
				<img src="../images/preconfig/overcoat/overcoatdefault0003.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0004" onclick="overconfig('overcoatdefault0004')">
				<img src="../images/preconfig/overcoat/overcoatdefault0004.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0005" onclick="overconfig('overcoatdefault0005')">
				<img src="../images/preconfig/overcoat/overcoatdefault0005.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0006" onclick="overconfig('overcoatdefault0006')">
				<img src="../images/preconfig/overcoat/overcoatdefault0006.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0007" onclick="overconfig('overcoatdefault0007')">
				<img src="../images/preconfig/overcoat/overcoatdefault0007.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0008" onclick="overconfig('overcoatdefault0008')">
				<img src="../images/preconfig/overcoat/overcoatdefault0008.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault0009" onclick="overconfig('overcoatdefault0009')">
				<img src="../images/preconfig/overcoat/overcoatdefault0009.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00010" onclick="overconfig('overcoatdefault00010')">
				<img src="../images/preconfig/overcoat/overcoatdefault00010.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00011" onclick="overconfig('overcoatdefault00011')">
				<img src="../images/preconfig/overcoat/overcoatdefault00011.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00012" onclick="overconfig('overcoatdefault00012')">
				<img src="../images/preconfig/overcoat/overcoatdefault00012.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00013" onclick="overconfig('overcoatdefault00013')">
				<img src="../images/preconfig/overcoat/overcoatdefault00013.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00014" onclick="overconfig('overcoatdefault00014')">
				<img src="../images/preconfig/overcoat/overcoatdefault00014.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00015" onclick="overconfig('overcoatdefault00015')">
				<img src="../images/preconfig/overcoat/overcoatdefault00015.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00016" onclick="overconfig('overcoatdefault00016')">
				<img src="../images/preconfig/overcoat/overcoatdefault00016.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00017" onclick="overconfig('overcoatdefault00017')">
				<img src="../images/preconfig/overcoat/overcoatdefault00017.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00018" onclick="overconfig('overcoatdefault00018')">
				<img src="../images/preconfig/overcoat/overcoatdefault00018.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00019" onclick="overconfig('overcoatdefault00019')">
				<img src="../images/preconfig/overcoat/overcoatdefault00019.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00020" onclick="overconfig('overcoatdefault00020')">
				<img src="../images/preconfig/overcoat/overcoatdefault00020.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00021" onclick="overconfig('overcoatdefault00021')">
				<img src="../images/preconfig/overcoat/overcoatdefault00021.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00022" onclick="overconfig('overcoatdefault00022')">
				<img src="../images/preconfig/overcoat/overcoatdefault00022.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00023" onclick="overconfig('overcoatdefault00023')">
				<img src="../images/preconfig/overcoat/overcoatdefault00023.png"/>
			</div>
			<div class="primarysuggestions" id="firstconfigover_overcoatdefault00024" onclick="overconfig('overcoatdefault00024')">
				<img src="../images/preconfig/overcoat/overcoatdefault00024.png"/>
			</div>
			
        </div>  
		
    </div>
	
	
	 <div class="col-md-12" id="showovercoat" style="display:none;">
        <div class="col-md-2" style="position: relative; padding-top: 57px; padding-bottom: 122px; margin-bottom: 43px;z-index: 1">
            <img src=""  class="outputsugg" id="overcoatimgs" alt=''/>
        </div>
		<div class="col-md-12" style="text-align:center;margin-top: 100px">
			<label id="cart" onclick="getimagecart()" class="prevnextbtn">Add to Cart</label>
		</div>
    </div>	
	 
	
</div>
    <?php
        if(isset($_REQUEST['fromtwo']))
        {
            ?>
            <script>
                 showcolordiv();
            </script>
            <?php
        }
    ?>
</div>
</div>
<?php include "footer.php"; ?>
<script>
    $( document ).ready(function() {
        $(".nav a").removeClass("active");
    });
    var currPage = "";
    var category = "formal";
    var firstconfigover = "overcoatdefault0001";


    function loadSessionData() {
        $(".loader").fadeIn("slow");
        var prevIndex = $(".stepsactive").attr("id");
        currPage = prevIndex + "s";
        var url = "../admin/webserver/selectionData.php?type=getsession&" + prevIndex + "=" + prevIndex;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {

                var img = "";
                var item = json.items;
                var name = json.items;
                item = prevIndex + "_" + item;
                $(".primarysuggestion").removeClass("active");
                $("#" + item).addClass("active");
                if (prevIndex == "category") {
                    category = name;
                }
                else if (prevIndex == "firstconfigover") {
                    firstconfigover = name;
                }



                if (currPage == "categorys") {
                    $("#preconfigimg").attr("src", "../images/preconfig/overcoat/overcoatdefault0006.png");
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "firstconfigovers") {
                    $("#preconfigimg").attr("src", "../images/preconfig/overcoat/overcoatdefault0006.png");
                    $(".loader").fadeOut("slow");
                }




            }
            else {
                var url = "";
                if (currPage == "categorys") {
                    folder = "front";
                    url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + category;
                    $(".loader").fadeOut("slow");
                }
                else if (currPage == "firstconfigovers") {
                    url = "../admin/webserver/selectionData.php?" + prevIndex + "=" + firstconfigover;
                    $(".loader").fadeOut("slow");
                }




                $.get(url, function (data) {
                    var json = $.parseJSON(data);
                    var status = json.status;
                    if (status == "done") {
                        $(".loader").fadeOut("slow");
                    }
                }).fail(function () {
                    alert("page is not available");
                });
            }
        });
    }
    $(".primarysuggestion").click(function () {
        var Img = "";
        $(".loader").fadeIn("slow");
        $(".primarysuggestion").removeClass("active");
        var value = this.id;
        var type = value.split("_");
        var selection = type[1];
        $("#" + value).addClass("active");
        var currpage = type[0] + "s";
        var url = "../admin/webserver/selectionData.php?" + type[0] + "=" + selection;
        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                if (type[0] == "category") {
                    $(".loader").fadeOut("slow");
                    category = selection;
                }
                else if (type[0] == "firstconfigover") {
                    $(".loader").fadeOut("slow");
                    firstconfigover = selection;
                }


            }
            else {
                alert(status);
                $(".loader").fadeOut("slow");
            }
        }).fail(function () {
            alert("page is not available");
            $(".loader").fadeOut("slow");
        });
    });
    function navigation(switcher) {
        $("#" + currPage).fadeOut("slow");
        if (switcher == "next") {
            if (currPage == "categorys") {
                $(".steps").removeClass("stepsactive");
                $("#firstconfigovers").fadeIn("slow");
                $("#firstconfigover").addClass("stepsactive");
                loadSessionData();
            }

            else if (currPage == "pantcolors") {
                var user_id = $("#user_id").val();
                //alert(user_id);
                var randomnum = Math.floor((Math.random() * 600000) + 1);
                var product_name = "CustomPant" + randomnum;
                var url = "../admin/webserver/addto_cart.php?product_type=Custom Pant&product_name=" + product_name +
                    "&pant_category="+category+"&front_pocket=" + frontpocket + "&back_pocket=" + backpocket + "&belt_loop=" + beltloop
                    + "&pant_color=" + pantcolor + "&pant_bottom="+pantbottom+ "&product_price=" + 167.22
                    + "&user_id=" + user_id + "&quantity=" + 1 + "&cart_product_total_amount=" + 167.22;
                $.get(url, function (data) {
                    var json = $.parseJSON(data)
                    var status = json.status;
                    if (status == "done") {
                        window.location = "cart.php";
                    }
                    else {
                        alert(status);
                    }
                });
            }
        }
        else if (switcher == "previous") {
            if (currPage == "categorys") {
                window.location = "customize.php";
            }
            else if (currPage == "firstconfigovers") {
                $(".steps").removeClass("stepsactive");
                $("#categorys").fadeIn("slow");
                $("#category").addClass("stepsactive");
                loadSessionData();
            }


        }
    }

    $(".steps").click(function(){
        $("#"+currPage).hide(1000);
        var newcurrpage = this.id+"s";
        $(".steps").removeClass("stepsactive");
        $("#"+this.id).addClass("stepsactive");
        $("#"+newcurrpage).show(1000);
        currPage = newcurrpage;
        loadSessionData();
        if(currPage == "pantcolors")
        {
            $("#next").html("Add to Cart");
        }
        else
        {
            $("#next").html("Next");
        }
    });
    loadSessionData();

    function overconfig(defaultimgs){
        $("#firstconfigovers").hide();
        $("#showovercoat").show();
        $("#overcoatimgs").attr("src", "../images/preconfig/overcoat/"+defaultimgs+".png");
    }
    function getimagecart(){
        var src = $("#overcoatimgs").attr("src");
        var realsrc = src.split(".png");
        var srcreal = realsrc[0];
        buyAccessories('overcoatCollection','','',srcreal,src);
    }
    function buyAccessories(category,specification,rc_no,image_type){
        var user_id = $("#user_id").val();
        var randomnum = Math.floor((Math.random() * 600000) + 1);
        var product_name = category + randomnum;
        var url = "../admin/webserver/addto_cart.php?products_type=Collection&category="+category+"&product_name="+product_name+
            "&specification="+specification+"&rc_no="+rc_no+"&user_id="+user_id+"&quantity="+1+"&image_type="+image_type;

        $.get(url, function (data) {
            var json = $.parseJSON(data);
            var status = json.status;
            if (status == "done") {
                window.location = "order_cart.php?userId="+user_id;
            }
            else {
                alert(status);
            }
        });
    }
    var user_id = $("#user_id").val();
    getCart(user_id);
</script>