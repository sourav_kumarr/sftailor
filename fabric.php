<?php include("header.php"); ?>
    <div class="banner top-fabricdescription">
        <div class="">
        </div>
    </div>
    <!-- //Slider -->
    </div>
    <!-- //Header -->
    <div class="about-bottom wthree-3">
        <div class="container">
            <h2 class="tittle">OUR "SUPER" <span style="color:black;">FABRICS</span></h2>
            <div class="agileinfo_about_bottom_grids">
                <div class="col-md-12 agileinfo_about_bottom_grid">
                    <p>The right fabric is an important part of your bespoke garment. That's why we have hundreds of
                        colors and patterns to choose from. The fabric type, weight and "super" numbers are other
                        criteria to keep in mind when designing your bespoke suit.
                    </p>
                    </br>
                    <p>The Super number indicates the diameter of the fibers, measured in microns, used in making the
                        cloth. The International Wool Textile Organization (IWTO), the recognized global authority for
                        standards in the wool textile industry which represents the interests of the wool textile trade
                        at a global level, governs the Super designation. The word Super (as in Super 100"s, for
                        example) can only be used to describe fabrics made from pure new wool, and the "S" value is
                        determined by the maximum fiber diameter. The Super "S" description can also be used for fabrics
                        made from wool blended with rare fibers (such as mohair, cashmere and alpaca), and also with
                        silk. The inclusion of elasthane to give the fabric a stretch effect is permitted, as also is
                        the inclusion of up to 5% non-wool yarn for decorative effects.</p></br>
                    <p>A number higher than 150 will typically be a much thinner fabric and will not take as well to the
                        needle as a fabric with a lower Super Number. For day to day wear, we recommend "the sweet spot"
                        between 100 and 150.</p></br>
                    <div class="col-md-12 agileinfo_about_bottom_grid">
                        <img src="images/tab/sweet_spot_copy.jpg" alt="" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- //team -->
<?php include("footer.php"); ?>