<!-- footer content -->
<footer style="margin-left: 0px!important;">
    <div class="pull-right">
        SFTailor || Admin - Developed by <a href="http://scan2fit.com/">TC2 Labs LLC</a>
    </div>
    <div class="clearfix"></div>
</footer>
<!-- /footer content -->
<div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Modal Header</h4>
            </div>
            <div class="modal-body">
                <p>Some text in the modal.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>
<script src="js/jquery.min.js"></script>
<script src="js/location.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/moment.min.js"></script>
<script src="js/custom.min.js"></script>
<script src="js/jquery.dataTables.min.js"></script>
<script src="js/bootstrap-toggle.min.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src="js/myscript.js"></script>
<script src="../js/jquery-ui.js" type="text/javascript"></script>
<script>
    function showError(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
        $(".modal-footer").css({"display": "none"});
        $("#myModal").modal("show");
        setTimeout(function () {
            $("#myModal").modal("hide");
        }, 3000);
    }
</script>



