<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 11-12-2017
 * Time: 16:24
 */
require_once('api/Constants/configuration.php');
require_once('api/Constants/DbConfig.php');
require_once('api/Classes/RESPONSE.php');
require_once('api/Classes/BROADCAST.php');
require_once('api/Classes/USERCLASS.php');
require_once('api/Classes/MESSAGE.php');
require_once('api/Classes/CONNECT.php');

$connect = new Classes\CONNECT();
$msgClass = new Classes\MESSAGE();
$userClass = new Classes\USERCLASS();

/*
 * code to send broadcast messages
 *
 * */

date_default_timezone_set("America/New_York");
$date = date("Y-m-d h:i:s A");
$link = $connect->connect();
if(!$link) {
    echo "connection error ".$connect->sqlError();
    return false;
}
$query = "select msg_id,msg_desc,msg_desc,msg_text,broad_id,broad_users,broad_updated,broad_created_at from wp_broadcasts,wp_messages where wp_broadcasts.broad_msg_id=wp_messages.msg_id and wp_broadcasts.broad_updated='$date'";
$result = mysqli_query($link,$query);
if(!$result) {
    echo "unable to execute query \n";
    return false;
}
$count = mysqli_num_rows($result);
if($count === 0) {
    echo "broadcast data not found\n";
}
if($count>0) {
  while($rows = mysqli_fetch_assoc($result)) {
      $msg_subject = $rows['msg_text'];
      $msg_desc = $rows['msg_desc'];
      $ids = $rows['broad_users'];
      $msgClass->sendMessages($ids,$msg_subject,$msg_desc);
  }
}

/*
 * code to send messages to wp_messages...
 *
 * */

$query = "select * from wp_messages where msg_broadcast_date='$date' ";
$result = mysqli_query($link,$query);
$users_data = array();
if(!$result) {
    echo "unable to execute query \n";
    return false;
}
$count = mysqli_num_rows($result);
$userResponse = $userClass->getAllUsersData();
$status = $userResponse[Status];
$users = $userResponse["usersData"];
foreach($users as $user)  {
    $users_data[] = $user["email"];
}
$users_data = implode(",",$users_data);

if($count === 0) {
    echo "messages record not found \n";
    return false;
}
if($count>0) {
    while($rows = mysqli_fetch_assoc($result)) {
        $msg_subject = $rows['msg_text'];
        $msg_desc = $rows['msg_desc'];
        $ids = $rows['broad_users'];
        $msgClass->sendMessages($users_data,$msg_subject,$msg_desc);
    }
}

?>