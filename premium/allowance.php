<?php
    include('header.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="UTF-8">
<style>
    .boxes {
        background: white;
        min-height: 100px;
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }
    .boxes .tabdiv {
        background: #eee none repeat scroll 0 0;
        border: 1px solid;
        font-weight: bold;
        margin-top: 22px;
        padding: 8px;
        text-align: center;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .savebtn {
        letter-spacing: 1px;
        margin-bottom: 18px;
        text-transform: uppercase;
    }
    .err {
        text-align: center;
        color: red;
    }
    table.table.custom {
        border: 1px solid #ddd;
        text-align:center;
        display:inline-block;
        overflow:scroll;
    }
    table.table.custom th {
        border-right:1px solid #ddd;
        text-align:center
    }
    table.table.custom td {
        border-right:1px solid #ddd;
        font-size: 14px;
    }
    .custom-input{
        width: 100%;
        border: 0;
        text-align: center;
        outline:0;
        background: transparent;
    }
    .table th{
        text-align:center;
    }
    .allowtable{
        height:490px;
        overflow-y:scroll;
        padding:0;
    }
</style>
<!-- page content -->
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<div class="right_col" role="main">
    <div class="row">
        <ul class="nav nav-tabs">
            <li role="presentation" id="shirtallow" class="tabs active" onclick="switchAllowance('shirt')"><a >Shirt Allowance</a></li>
            <li role="presentation" class="tabs" id="jacketallow" onclick="switchAllowance('jacket')"><a>Jacket Allowance</a></li>
            <li role="presentation" class="tabs" id="vestallow" onclick="switchAllowance('vest')"><a>Vest Allowance</a></li>
            <li role="presentation" class="tabs" id="pantallow" onclick="switchAllowance('pant')"><a>Pant Allowance</a></li>
            <li role="presentation" class="tabs" id="bodystyleallow" onclick="switchAllowance('bodystyle')"><a>Body Style</a></li>
            <li role="presentation" class="tabs" id="pleatstyleallow" onclick="switchAllowance('pleatstyle')"><a>Pleat Style(Pant)</a></li>
            <li role="presentation" class="tabs" id="minmaxallow" onclick="switchAllowance('minmax')"><a>Min-Max Values</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 allowtable" id="shirt_table" style="display:block;overflow-x:scroll">
            <table class="table table-bordered" style="width:100%;overflow-x:scroll;">
            <thead>
            <tr>
                <th colspan="2">Style Genie</th>
                <th colspan="3">Question 1</th>
                <th colspan="3">Question 2</th>
                <th colspan="3">Question 3</th>
                <th colspan="3">Question 4</th>
                <th colspan="4">Question 5</th>
                <th colspan="2">Question 6</th>
            </tr>
            <tr>
                <td>S.no</td>
                <td>Body Parameters</td>
                <td>European</td>
                <td> Tailored </td>
                <td> Classic</td>
                <td>European</td>
                <td> Tailored </td>
                <td> Classic</td>
                <td> Tucked</td>
                <td> Untucked</td>
                <td> Both</td>
                <td> Stylist Short</td>
                <td> Standard</td>
                <td> Classic</td>
                <td> Watch Left Wrist</td>
                <td> Watch Right Wrist</td>
                <td> Watch Both Wrist</td>
                <td> No Watch</td>
                <td> Watch Big</td>
                <td> Watch Small</td>

            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Collar</td>
                <td><input type="text" id="shirt_collar_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_collar_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Chest</td>
                <td><input type="text" id="shirt_chest_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_chest_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Waist Horizontal Line</td>
                <td><input type="text" id="shirt_stomach_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_stomach_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Seat</td>
                <td><input type="text" id="shirt_seat_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_seat_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>5</td>
                <td>Bicep</td>
                <td><input type="text" id="shirt_bicep_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bicep_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>6</td>
                <td>Back Shoulder Width</td>
                <td><input type="text" id="shirt_back_shoulder_width_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_shoulder_width_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>7</td>
                <td>Front Shoulder Width</td>
                <td><input type="text" id="shirt_front_shoulder_width_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_shoulder_width_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>8</td>
                <td>Left sleeve length</td>
                <td><input type="text" id="shirt_left_sleeve_length_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_left_sleeve_length_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>9</td>
                <td>Right sleeve length</td>
                <td><input type="text" id="shirt_right_sleeve_length_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_right_sleeve_length_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>10</td>
                <td>Front waist length</td>
                <td><input type="text" id="shirt_front_waist_length_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_length_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>11</td>
                <td>Nape to waist (back)</td>
                <td><input type="text" id="shirt_nape_to_waist_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_nape_to_waist_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>12</td>
                <td>Back jacket length</td>
                <td><input type="text" id="shirt_back_jacket_length_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_jacket_length_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>13</td>
                <td>Wrist</td>
                <td><input type="text" id="shirt_wrist_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_wrist_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>14</td>
                <td>Waist</td>
                <td><input type="text" id="shirt_waist_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_waist_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>15</td>
                <td>Thigh</td>
                <td><input type="text" id="shirt_thigh_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_thigh_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>16</td>
                <td>U-rise</td>
                <td><input type="text" id="shirt_u_rise_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_u_rise_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>17</td>
                <td>Back waist height</td>
                <td><input type="text" id="shirt_back_waist_height_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_back_waist_height_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>18</td>
                <td>Front waist height</td>
                <td><input type="text" id="shirt_front_waist_height_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_front_waist_height_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>19</td>
                <td>Pants left Outseam</td>
                <td><input type="text" id="shirt_pants_left_outseam_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_left_outseam_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>20</td>
                <td>Pants right Outseam</td>
                <td><input type="text" id="shirt_pants_right_outseam_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_pants_right_outseam_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>21</td>
                <td>Knee(Optional)</td>
                <td><input type="text" id="shirt_knee_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_knee_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>22</td>
                <td>Bottom(Optional)</td>
                <td><input type="text" id="shirt_bottom_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_bottom_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>23</td>
                <td>Calf</td>
                <td><input type="text" id="shirt_calf_ques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques3_Always_tucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques3_Always_untucked" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques3_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques4_Stylish_short" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques4_Standard" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques5_Left_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques5_Right_wrist" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques5_Both" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques5_None" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques6_Normal" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="shirt_calf_ques6_Big" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            </tbody>
        </table>
        </div>
        <div class="col-md-12 allowtable" id="jacket_table" style="display:none;overflow-x:scroll">
            <table class="table table-bordered" style="width:100%;overflow-x:scroll">
            <thead>
            <tr>
                <th colspan="2">Style Genie</th>
                <th colspan="3">Question 1</th>
                <th colspan="3">Ques1 < 7</th>
                <th colspan="3">Question 2</th>
                <th colspan="3">Ques2 < 7</th>
                <th colspan="3">Question 3</th>
                <th colspan="3">Question 4</th>

            </tr>
            <tr>
                <td>S.no</td>
                <td>Body Parameters</td>
                <td>European</td>
                <td>Tailored </td>
                <td>Classic</td>
                <td>European</td>
                <td>Tailored </td>
                <td>Classic</td>
                <td>European</td>
                <td>Tailored</td>
                <td>Classic</td>
                <td>European</td>
                <td>Tailored</td>
                <td>Classic</td>
                <td>European</td>
                <td>Tailored</td>
                <td>Classic</td>
                <td>European</td>
                <td>Tailored</td>
                <td>Classic</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Collar</td>
                <td><input type="text" id="jacket_collar_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_collar_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>2</td>
                <td>Chest</td>
                <td><input type="text" id="jacket_chest_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_chest_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>3</td>
                <td>Waist Horizontal Line</td>
                <td><input type="text" id="jacket_stomach_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_stomach_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>4</td>
                <td>Seat</td>
                <td><input type="text" id="jacket_seat_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_seat_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>5</td>
                <td>Bicep</td>
                <td><input type="text" id="jacket_bicep_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bicep_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>6</td>
                <td>Back Shoulder Width</td>
                <td><input type="text" id="jacket_back_shoulder_width_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_shoulder_width_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>7</td>
                <td>Front Shoulder Width</td>
                <td><input type="text" id="jacket_front_shoulder_width_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_shoulder_width_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>8</td>
                <td>Left sleeve length</td>
                <td><input type="text" id="jacket_left_sleeve_length_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_left_sleeve_length_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>9</td>
                <td>Right sleeve length</td>
                <td><input type="text" id="jacket_right_sleeve_length_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_right_sleeve_length_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>10</td>
                <td>Front waist length</td>
                <td><input type="text" id="jacket_front_waist_length_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_length_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>11</td>
                <td>Nape to waist (back)</td>
                <td><input type="text" id="jacket_nape_to_waist_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_nape_to_waist_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>12</td>
                <td>Back jacket length</td>
                <td><input type="text" id="jacket_back_jacket_length_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_jacket_length_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>13</td>
                <td>Wrist</td>
                <td><input type="text" id="jacket_wrist_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_wrist_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>14</td>
                <td>Waist</td>
                <td><input type="text" id="jacket_waist_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_waist_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>15</td>
                <td>Thigh</td>
                <td><input type="text" id="jacket_thigh_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_thigh_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>16</td>
                <td>U-rise</td>
                <td><input type="text" id="jacket_u_rise_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_u_rise_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>17</td>
                <td>Back waist heigh</td>
                <td><input type="text" id="jacket_back_waist_height_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_back_waist_height_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>18</td>
                <td>Front waist height</td>
                <td><input type="text" id="jacket_front_waist_height_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_front_waist_height_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>19</td>
                <td>Pants left Outseam</td>
                <td><input type="text" id="jacket_pants_left_outseam_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_left_outseam_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>20</td>
                <td>Pants right Outseam</td>
                <td><input type="text" id="jacket_pants_right_outseam_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_pants_right_outseam_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>21</td>
                <td>Knee(Optional)</td>
                <td><input type="text" id="jacket_knee_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_knee_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>22</td>
                <td>Bottom(Optional)</td>
                <td><input type="text" id="jacket_bottom_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_bottom_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>
            <tr>
                <td>23</td>
                <td>Calf</td>
                <td><input type="text" id="jacket_calf_jques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques1lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques1lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques1lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques2_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques2_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques2_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques2lessthan7_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques2lessthan7_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques2lessthan7_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques3_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques3_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques3_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques4_European" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques4_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                <td><input type="text" id="jacket_calf_jques4_Classic" class="custom-input" onchange='updateValue(this)' ></td>
            </tr>

            </tbody>
        </table>
        </div>
        <div class="col-md-12 allowtable" id="vest_table" style="display:none">
            <table class="table table-bordered" style="width:100%;">
                <thead>
                <tr>
                    <th colspan="2">Style Genie</th>
                    <th colspan="3">Question 1</th>
                </tr>
                <tr>
                    <td>S.no</td>
                    <td>Body Parameters</td>
                    <td>European</td>
                    <td> Tailored </td>
                    <td> Classic</td>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Collar</td>
                    <td><input type="text" id="vest_collar_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_collar_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_collar_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Chest</td>
                    <td><input type="text" id="vest_chest_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_chest_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_chest_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Waist Horizontal Line</td>
                    <td><input type="text" id="vest_stomach_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_stomach_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_stomach_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Seat</td>
                    <td><input type="text" id="vest_seat_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_seat_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_seat_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Bicep</td>
                    <td><input type="text" id="vest_bicep_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_bicep_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_bicep_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Back Shoulder Width</td>
                    <td><input type="text" id="vest_back_shoulder_width_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_back_shoulder_width_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_back_shoulder_width_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Front Shoulder Width</td>
                    <td><input type="text" id="vest_front_shoulder_width_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_front_shoulder_width_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_front_shoulder_width_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Left sleeve length</td>
                    <td><input type="text" id="vest_left_sleeve_length_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_left_sleeve_length_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_left_sleeve_length_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Right sleeve length</td>
                    <td><input type="text" id="vest_right_sleeve_length_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_right_sleeve_length_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_right_sleeve_length_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Front waist length</td>
                    <td><input type="text" id="vest_front_waist_length_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_front_waist_length_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_front_waist_length_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>Nape to waist (back)</td>
                    <td><input type="text" id="vest_nape_to_waist_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_nape_to_waist_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_nape_to_waist_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>Back jacket length</td>
                    <td><input type="text" id="vest_back_jacket_length_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_back_jacket_length_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_back_jacket_length_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>Wrist</td>
                    <td><input type="text" id="vest_wrist_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_wrist_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_wrist_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>Waist</td>
                    <td><input type="text" id="vest_waist_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_waist_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_waist_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>Thigh</td>
                    <td><input type="text" id="vest_thigh_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_thigh_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_thigh_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>U-rise</td>
                    <td><input type="text" id="vest_u_rise_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_u_rise_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_u_rise_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>Back waist height</td>
                    <td><input type="text" id="vest_back_waist_height_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_back_waist_height_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_back_waist_height_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>Front waist height</td>
                    <td><input type="text" id="vest_front_waist_height_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_front_waist_height_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_front_waist_height_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>Pants left Outseam</td>
                    <td><input type="text" id="vest_pants_left_outseam_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_pants_left_outseam_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_pants_left_outseam_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>Pants right Outseam</td>
                    <td><input type="text" id="vest_pants_right_outseam_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_pants_right_outseam_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_pants_right_outseam_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>Knee(Optional)</td>
                    <td><input type="text" id="vest_knee_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_knee_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_knee_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>22</td>
                    <td>Bottom(Optional)</td>
                    <td><input type="text" id="vest_bottom_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_bottom_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_bottom_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>23</td>
                    <td>Calf</td>
                    <td><input type="text" id="vest_calf_vques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_calf_vques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="vest_calf_vques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 allowtable" id="pant_table" style="display:none">
            <table class="table table-bordered" style="width:100%;">
            <thead>
            <tr>
                <th colspan="2">Style Genie</th>
                <th colspan="3">Question 1</th>
            </tr>
            <tr>
                <td>S.no</td>
                <td>Body Parameters</td>
                <td>European</td>
                <td> Tailored </td>
                <td> Classic</td>
            </tr>
            </thead>
                <tbody>
                <tr>
                    <td>1</td>
                    <td>Collar</td>
                    <td><input type="text" id="pant_collar_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_collar_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_collar_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>2</td>
                    <td>Chest</td>
                    <td><input type="text" id="pant_chest_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_chest_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_chest_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>3</td>
                    <td>Waist Horizontal Line</td>
                    <td><input type="text" id="pant_stomach_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_stomach_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_stomach_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>4</td>
                    <td>Seat</td>
                    <td><input type="text" id="pant_seat_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_seat_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_seat_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>5</td>
                    <td>Bicep</td>
                    <td><input type="text" id="pant_bicep_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_bicep_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_bicep_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>6</td>
                    <td>Back Shoulder Width</td>
                    <td><input type="text" id="pant_back_shoulder_width_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_back_shoulder_width_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_back_shoulder_width_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>7</td>
                    <td>Front Shoulder Width</td>
                    <td><input type="text" id="pant_front_shoulder_width_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_front_shoulder_width_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_front_shoulder_width_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>8</td>
                    <td>Left sleeve length</td>
                    <td><input type="text" id="pant_left_sleeve_length_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_left_sleeve_length_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_left_sleeve_length_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>9</td>
                    <td>Right sleeve length</td>
                    <td><input type="text" id="pant_right_sleeve_length_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_right_sleeve_length_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_right_sleeve_length_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>10</td>
                    <td>Front waist length</td>
                    <td><input type="text" id="pant_front_waist_length_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_front_waist_length_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_front_waist_length_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>11</td>
                    <td>Nape to waist (back)</td>
                    <td><input type="text" id="pant_nape_to_waist_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_nape_to_waist_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_nape_to_waist_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>12</td>
                    <td>Back jacket length</td>
                    <td><input type="text" id="pant_back_jacket_length_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_back_jacket_length_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_back_jacket_length_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>13</td>
                    <td>Wrist</td>
                    <td><input type="text" id="pant_wrist_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_wrist_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_wrist_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>14</td>
                    <td>Waist</td>
                    <td><input type="text" id="pant_waist_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_waist_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_waist_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>15</td>
                    <td>Thigh</td>
                    <td><input type="text" id="pant_thigh_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_thigh_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_thigh_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>16</td>
                    <td>U-rise</td>
                    <td><input type="text" id="pant_u_rise_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_u_rise_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_u_rise_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>17</td>
                    <td>Back waist height</td>
                    <td><input type="text" id="pant_back_waist_height_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_back_waist_height_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_back_waist_height_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>18</td>
                    <td>Front waist height</td>
                    <td><input type="text" id="pant_front_waist_height_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_front_waist_height_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_front_waist_height_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>19</td>
                    <td>Pants left Outseam</td>
                    <td><input type="text" id="pant_pants_left_outseam_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_pants_left_outseam_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_pants_left_outseam_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>20</td>
                    <td>Pants right Outseam</td>
                    <td><input type="text" id="pant_pants_right_outseam_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_pants_right_outseam_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_pants_right_outseam_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>21</td>
                    <td>Knee(Optional)</td>
                    <td><input type="text" id="pant_knee_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_knee_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_knee_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>22</td>
                    <td>Bottom(Optional)</td>
                    <td><input type="text" id="pant_bottom_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_bottom_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_bottom_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                <tr>
                    <td>23</td>
                    <td>Calf</td>
                    <td><input type="text" id="pant_calf_pques1_European" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_calf_pques1_Tailored" class="custom-input" onchange='updateValue(this)' ></td>
                    <td><input type="text" id="pant_calf_pques1_Classic" class="custom-input" onchange='updateValue(this)' ></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-12 allowtable" id="bodystyle_table" style="display:none">
            <table class="table table-bordered" style="width:100%;">
            <thead>
            <tr>
                <th colspan="2">Parameters</th>
                <th colspan="3">Options</th>
            </tr>
            <tr>
                <td>S.no</td>
                <td>Drop Down Options</td>
                <td>Back Shoulder Only</td>
                <td>Front Shoulder Only</td>
                <td>Both Shoulders</td>
            </tr>
            </thead>
                <tbody id="body_style_tbody"></tbody>
            </table>
        </div>
        <div class="col-md-12 allowtable" id="pleatstyle_table" style="display:none">
            <table class="table table-bordered" style="width:100%;">
                <thead>
                    <tr>
                        <th colspan="2">Parameters</th>
                        <th colspan="5">The Allowance for Pant Seam Cm</th>
                        <th colspan="1">U-Rise Cm</th>
                    </tr>
                </thead>
                <tbody id="pleat_style_tbody"></tbody>
            </table>
        </div>
        <div class="col-md-12 allowtable" id="minmax_table" style="display:none">
            <table class="table table-bordered" style="width:100%;">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Parameter</th>
                        <th>Minimum Value</th>
                        <th>Maximum Value</th>
                    </tr>
                </thead>
                <tbody id="minmax_tbody">

                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
    include('footer.php');
?>
<script>
    function switchAllowance(tab){
        $(".allowtable").css("display","none");
        $("#"+tab+"_table").css("display","block");
        $(".tabs").removeClass("active");
        $("#"+tab+"allow").addClass("active");
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        if(tab === "bodystyle"){
            getbodystyledata();
        }else if(tab === "pleatstyle"){
            getpleatstyledata();
        }else if(tab === "minmax"){
            minmaxdata();
        }else {
            var url = "../api/registerUser.php";
            $.post(url, {"type": "getAllowanceData", "garment_type": tab}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    var allowanceData = data.allowanceData;
                    var bodyParams = ['collar', 'chest', 'stomach', 'seat', 'bicep', 'back_shoulder_width', 'front_shoulder_width',
                        'left_sleeve_length', 'right_sleeve_length', 'front_waist_length', 'nape_to_waist', 'back_jacket_length',
                        'wrist', 'thigh', 'u_rise', 'back_waist_height', 'front_waist_height', 'pants_left_outseam',
                        'pants_right_outseam', 'knee', 'bottom', 'calf', 'waist'];
                    for (var i = 0; i < bodyParams.length; i++) {
                        for (var j = 0; j < allowanceData.length; j++) {
                            if (bodyParams[i] == allowanceData[j].body_params) {
                                var answer = allowanceData[j].answers;
                                if (answer.indexOf(" ") > -1) {
                                    answer = answer.replace(" ", "_");
                                }
                                $("#" + tab + "_" + bodyParams[i] + "_" + allowanceData[j].question_type + "_" + answer).val(allowanceData[j].value);
                            }
                        }
                    }
                } else {
                    showMessage(data.Message, "red");
                }
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }
    switchAllowance("shirt");
    function updateValue(obj){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updateAllowance", "id": id, "value": value},function () {
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }
    function getbodystyledata(){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getBodyStyleData"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var styleData = data.styleData;
                var dataShow = "";
                for(var i=0;i<styleData.length;i++){
                    dataShow += "<tr><td>"+(i+1)+"</td><td>"+styleData[i].option_name+"</td><td><input type='text' " +
                    "id='bs_"+styleData[i].row_id+"' value='"+styleData[i].back_shoulder+"' class='custom-input' " +
                    "onchange=updateBodyStyle(this,'back_shoulder')></td><td><input type='text' id='fs_"+styleData[i].row_id+
                    "' class='custom-input' onchange=updateBodyStyle(this,'front_shoulder') " +
                    "value='"+styleData[i].front_shoulder+"'></td><td><input type='text' id='co_"+styleData[i].row_id+"' " +
                    "class='custom-input' value='"+styleData[i].combination+"' onchange=updateBodyStyle(this,'combination')>"+
                    "</td></tr>";
                }
                $("#body_style_tbody").html(dataShow);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function getpleatstyledata(){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getpleatstyledata"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var pleatData = data.pleatData;
                var dataShow = "";
                for(var i=0;i<pleatData.length;i++){
                    if(i == 0) {
                        dataShow += "<tr><th>S.No</th><th>" + pleatData[0].dress_style + "</th><th>"+ pleatData[0].no_pleat+
                        "</th><th>"+pleatData[0].onepleatlesstha2cm+"</th><th>"+pleatData[0].onpleatbetween2cmand25cm+
                        "</th><th>"+pleatData[0].twopleatlessthan25cm+"</th><th>"+pleatData[0].threepleatlessthan25cm+
                        "</th><th>"+pleatData[0].urise+"</th></tr>";
                    }else{
                        dataShow += "<tr><td>"+(i)+"</td><td>" + pleatData[i].dress_style + "</td><td><input type='text'"+
                        "class='custom-input' id='"+pleatData[i].row_id+"_no_pleat' onchange=updatePleatData(this) " +
                        "value='"+ pleatData[i].no_pleat+"' /></td><td><input type='text' class='custom-input' id='"+
                        pleatData[i].row_id+"_onepleatlesstha2cm' onchange=updatePleatData(this) value='"+
                        pleatData[i].onepleatlesstha2cm+"' /></td><td><input type='text' class='custom-input' id='"+
                        pleatData[i].row_id+"_onpleatbetween2cmand25cm' onchange=updatePleatData(this) value='"+
                        pleatData[i].onpleatbetween2cmand25cm+"' /></td><td><input type='text' class='custom-input' id='"+
                        pleatData[i].row_id+"_twopleatlessthan25cm' onchange=updatePleatData(this) value='"+
                        pleatData[i].twopleatlessthan25cm+"' /></td><td><input type='text' class='custom-input' id='"+
                        pleatData[i].row_id+"_threepleatlessthan25cm' onchange=updatePleatData(this) value='"+
                        pleatData[i].threepleatlessthan25cm+"' /></td><td><input type='text' class='custom-input' id='"+
                        pleatData[i].row_id+"_urise' onchange=updatePleatData(this) value='"+
                        pleatData[i].urise+"' /></td></tr>";
                    }
                }
                $("#pleat_style_tbody").html(dataShow);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function minmaxdata(){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "minmaxdata"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var minmaxData = data.minmaxData;
                var dataShow = "";
                for(var i=0;i<minmaxData.length;i++){
                    var param = (minmaxData[i].parameter).split("_");
                    dataShow += "<tr><td>"+(i+1)+"</td><td style='text-transform:capitalize'>"+param[0]+" "+param[1]+
                    "</td><td><input type='text' class='custom-input' id='min_val-"+minmaxData[i].row_id+"' " +
                    "onchange=updateminmax(this) value='"+minmaxData[i].min_val+"' /></td><td>" +
                    "<input type='text' class='custom-input' id='max_val-"+minmaxData[i].row_id+"' " +
                    "onchange=updateminmax(this) value='"+minmaxData[i].max_val+"' /></td></tr>";
                }
                $("#minmax_tbody").html(dataShow);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function updateBodyStyle(obj,option_name){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            id = id.split("_")[1];
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updateBodyStyle", "row_id":id,"value":value,"option_name":option_name},function(){
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }function updatePleatData(obj){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            var option_name = id.split("_")[1];
            id = id.split("_")[0];
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updatePleatData", "row_id":id,"value":value,"option_name":option_name},function(){
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }function updateminmax(obj){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            var option_name = id.split("-")[0];
            id = id.split("-")[1];
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updateminmaxData", "row_id":id,"value":value,"option_name":option_name},function(){
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }
</script>
