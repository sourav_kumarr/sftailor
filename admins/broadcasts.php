<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 11-12-2017
 * Time: 14:02
 */
 include ("header.php");
?>
<style>
    .btn-primary{
        background-color: #202020;
        border:1px #202020 solid;
    }
    .btn-primary:hover{
        background-color: #202020;
        border:1px #202020 solid;
    }
</style>
<link href="css/create_message.css" rel="stylesheet"/>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="msg-body msg-body-1 active">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title" style="padding-bottom:2px; ">
                        <label style="font-size: 22px;letter-spacing: 1px;cursor: pointer" onclick="window.location='draft_message.php'"><i class="fa fa-arrow-circle-left"></i> Broadcasts</label>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <label>List of all broadcasts</label>
                            <button class="btn btn-primary btn-sm pull-right" style="margin-top: -3px;" onclick="window.location='create_message.php'">Create Message</button>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter" id="pbodydata">
                                <table class="table table-hover" id="broadcastTable">
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
 include ("footer.php");
?>
<script>
    function getAllBroadCasts() {
        var url = "api/broadcastProcess.php";
        $.post(url,{type:'getAllBroadCasts'},function(data){
          var status = data.Status;
          var message = data.Message;
          var th_ = '<thead><tr><th>#</th><th>Subject</th><th>Created At</th><th>Updated At</th><th>Actions</th></tr></thead>';
          var td_ = '<tbody>';
          if(status === 'Success') {
              var items = data.data;
              for(var i=0;i<items.length;i++) {
                  var subject = items[i].msg_text;
                  var created_at = items[i].broad_created_at;
                  var broad_updated = items[i].broad_updated;
                  var broad_id = items[i].broad_id;

                  td_ = td_+'<tr><td>'+(i+1)+'</td><td>'+subject+'</td><td>'+created_at+'</td><td>'+broad_updated+'</td><td style="text-align: center"><button class="btn btn-primary btn-sm" onclick="confirmUnscedule('+broad_id+')"><i class="fa fa-clock-o"></i> Unschedule</button></td></tr>';
              }
              $("#broadcastTable").html(th_+td_+'</tbody>');
              $("#broadcastTable").dataTable();
          }
          else{
              $("#pbodydata").html(message);
          }
        });
    }
    function confirmUnscedule(broad_id) {
        $(".modal-title").html("<label style='color: #202020;' >Delete</label>");
        $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>Are you sure want to unscedule message (y/n) ?</label>");
        $(".modal-footer").html('<label id="error" style="color:red;margin-right: 3%;"></label><button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>' +
            '<button type="button" class="btn btn-danger" onclick="removeSchedule('+broad_id+');"><i class="fa fa-trash"></i> Delete</button>');
        $("#myModal").modal("show");
    }

    function removeSchedule(broad_id) {

     var url = "api/broadcastProcess.php";
        $.post(url,{broad_id:broad_id,type:'removeBroadCast'},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status ==="Success") {
               $("#myModal").modal("hide");
              getAllBroadCasts();
            }
            else{
                $("#error").html(message);
            }
        });
    }

    getAllBroadCasts();
</script>