<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:42 PM
 */

require_once('Classes/BRAND_LOGO.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$logoClass = new \Classes\BRAND_LOGO();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $logoClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
if($type === 'addLogo') {
    $logoResponse = $logoClass->addLogo();
    $logoClass->apiResponse($logoResponse);
}
else if($type === 'getLogo') {
    $logoResponse = $logoClass->getLogo();
    $logoClass->apiResponse($logoResponse);
}

else if($type === 'deleteLogo') {
    $requiredfields = array('id','path');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $logoClass->apiResponse($response);
        return false;
    }
    $id = $_REQUEST['id'];
    $path = $_REQUEST['path'];
    $logoResponse = $logoClass->deleteLogo($id,$path);
    $logoClass->apiResponse($logoResponse);
}
else if($type === 'updateSortOrder') {
    $requiredfields = array('sortOrder','idOrder');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $logoClass->apiResponse($response);
        return false;
    }
    $sortOrder = $_REQUEST['sortOrder'];
    $idOrder = $_REQUEST['idOrder'];
    $logoResponse = $logoClass->updateSortOrder($sortOrder,$idOrder);
    $logoClass->apiResponse($logoResponse);
}
else if($type === 'addCollar') {
    $logoResponse = $logoClass->addCollar();
    $logoClass->apiResponse($logoResponse);
}
else if($type === 'deleteCollar') {
    $requiredfields = array('id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $logoClass->apiResponse($response);
        return false;
    }
    $id = $_REQUEST['id'];
    $logoResponse = $logoClass->deleteCollar($id,$path);
    $logoClass->apiResponse($logoResponse);
}

else if($type == "getPartCollar")
{
    $requiredfields = array('clrId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $logoClass->apiResponse($response);
        return false;
    }
    $btnId = $_POST['clrId'];
    $logoResponse = $logoClass->getPartClrs($btnId);
    $logoClass->apiResponse($logoResponse);
}
else if($type === 'editCollars') {
    $requiredfields = array('collar_name','collar_description','oldClrImagechanged','oldClrImage','clrId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $logoClass->apiResponse($response);
        return false;
    }
    $clrId = $_REQUEST['clrId'];
    $collar_name = $_REQUEST['collar_name'];
    $collar_description = $_REQUEST['collar_description'];
    $oldClrImagechanged = $_REQUEST['oldClrImagechanged'];
    $oldClrImage = $_REQUEST['oldClrImage'];
    $logoResponse = $logoClass->editCollars($clrId,$collar_name,$collar_description,$oldClrImagechanged,$oldClrImage);
    $logoClass->apiResponse($logoResponse);
}

else if($type === 'updateClrStatus') {
    $requiredfields = array('clrId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $logoClass->apiResponse($response);
        return false;
    }
    $clrId = $_REQUEST['clrId'];
    $clrStatus = $_REQUEST['clrStatus'];
    $logoResponse = $logoClass->updateCollarsStatus($clrId,$clrStatus);
    $logoClass->apiResponse($logoResponse);
}


?>