<?php
include('header.php');
include("front_header.php");
include("../admin/webserver/database/db.php");
?>
<style>
    .top-bar {
        background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
    }
    .active {
        border: 2px solid #555;
        border-radius: 4px;
    }
    .actives {
        border: 2px solid #555;
        border-radius: 4px;
    }
    .primarysuggestion {
        height: 200px;
        margin-left:5px;
        margin-top:5px;
        width: 160px;
    }
    .primarysuggestion img{
        height: 70%;
        width:100%;
    }
    .colorsuggestion{
        height:75px!important;
        width:75px!important;
    }
    .newsatinfabric{
        font-size: 16px;
        height: 83px;
        line-height: 81px!important;
        text-transform: capitalize;
        width: 171px;
    }
    .dbprim{margin-left: 6px;}
    .steps {
        background: #fff none repeat scroll 0 0;
        border: 2px solid #aeaeae;
        border-radius: 7%;
        cursor: pointer;
        float: left;
        font-size: 13px;
        line-height: 18px;
        margin-bottom:0;
        margin-left: 3px;
        margin-top: 3px;
        max-height: 57px;
        padding-top: 9px;
        text-align: center;
        width: 10%;
    }

    .stepsactive {
        background: #c4996c none repeat scroll 0 0;
        border: 2px solid #c4996c;
        color: white;
    }
    .outputsugg {
        position: absolute;
        margin-left: -70px;
        z-index:-1;
    }
    .backbtnsugg
    {
        height:68%;
    }
    .backbtnsuggparentdiv
    {
        height: 180px;
        width: 130px;
    }
    .backbtnsuggparentdiv p
    {
        font-weight:normal;
        font-size: 12px;
        line-height: 18px!important;
    }
    #forcasual .primarysuggestion > img
    {
        padding: 40px;
    }
    .okok
    {
        font-size: 19px;
        height: 52px;
        margin-left: 10px;
        margin-top: 10px;
        padding-top: 6px;
        width: 162px;
    }
    .overflow-cmd{overflow-y: auto; height: 660px; padding-top: 22px;}
    .liningoptions img {
        height: 40px;
    }
    .padding-size{
        height:80px;
        width:80px;
    }
    .padding-sizer{
        height:80px;
        width:80px;
    }
    .liningoptions{
        height:104px !important;
        width:100px !important;
    }
    .liningoptions img{
        height:75%;
    }
    #lettertype{font-size: font-size: 30px!important;}
    .data_{
        margin-top: 2px;
        padding: 0px;
        border: 1px lightgrey solid;
        border-radius: 5px;
        position: absolute;
        max-height: 100px;
        overflow: auto;
        width: 98%;
    }

    .data_ li{
        padding: 5px;
        border-bottom: 1px lightgrey solid;
    }
    .data_ li:hover{
        cursor: pointer;
        background-color: silver;
    }
    .data-remove{
        display:none;
    }
    .nav_menu{
        margin-bottom: 0px;
    }
    .outputsugg {
        position: absolute;
        margin-left: -77px;
        z-index: -1;
        width: 500px;
    }
    .nav-md .container.body .right_col{
        padding: 35px  1px  0 0;
        background: #fff;
    }
    .about-bottom{
        padding: 48px;
    }
    .steps{
        font-size: 12px!important;
    }
    .suggestionname{
        font-size: 12px;
    }
</style>
<div class="right_col" role="main" style="height: 700px!important;">
    <div class="about-bottom wthree-3">
        <div class="loader" style="display: none">
            <img src="../images/spin.gif" class="spinloader"/>
        </div>
        <div class="col-md-12">
            <div class="col-md-5">
                <div class="col-md-1" style="z-index:99">
                    <label onclick="rotateImage('left')" id="rotateLeft" style="margin-top: 350px">
                        <i class="fa fa-share fa-2x"></i>
                    </label>
                </div>
                <div class="col-md-10" style="padding-top: 50px;position: relative;z-index: 0">
                    <img src=""  class="outputsugg" id="frontbuttonimg" style="z-index: 4" alt=''/>
                    <img src=""  class="outputsugg" id="frontbuttonpointimg" style="z-index: 0" alt=''/>
                    <img src=""  class="outputsugg" id="lapelstyleimg" style="z-index: 1" alt=''/>
                    <img src="" class="outputsugg" id="breastpocketimg" style="z-index: 0" alt=''/>
                    <img src="" class="outputsugg" id="lowerpocketimg" style="z-index: 2" alt=''/>
                    <img src="" class="outputsugg" id="frontbuttoncutimg" style="z-index: 3" alt=''/>
                    <img src="" onerror="this.src='http://scan2fit.com/sftailor/images/jackets/sugg/bawli.png';" class="outputsugg" id="backventimg" alt=''/>
                    <img src="" class="outputsugg" id="shirtimg" alt=''/>
                    <img src="" class="outputsugg" id="tieimg" alt=''/>
                    <img src="" class="outputsugg" id="jacketimg" alt=''/>
                    <img src="" class="outputsugg" id="armbuttonimg" alt=''/>
                    <img src="" class="outputsugg" id="sidearmbuttonimg" alt=''/>
                    <img src="" class="outputsugg" id="sidearmbuttoncutimg" alt=''/>
                    <img src="" class="outputsugg" id="sidearmbuttonpointimg" alt=''/>
                </div>
                <div class="col-md-1" style="z-index:99">
                    <label onclick="rotateImage('right')" id="rotateRight" style="margin-top: 350px"><i class="fa fa-reply fa-2x"></i></label>
                </div>
            </div>
            <div class="col-md-7">
                <div class="col-md-12" style="padding:40px 0 15px">
                    <div class="steps stepsactive" id="suitcategory">Suit<br>Category</div>
                    <div class="steps" id="frontbutton">Front <br>Button</div>
                    <div class="steps" id="lapelstyle">Lapel <br>Style</div>
                    <div class="steps" id="lapelingredient">Ingredient<br>Style</div>
                    <div class="steps" id="chestdart">Chest<br>Dart</div>
                    <div class="steps" id="feltcollar">Felt<br>Collar</div>
                    <div class="steps" id="innerflap">Inner<br>Flap</div>
                    <div class="steps" id="facingstyle">Facing <br>Style</div>
                    <div class="steps" id="sleeveslit">Sleeve <br>Slit</div>
                    <div class="steps" id="breastpocket">Breast <br>Pocket</div>
                    <div class="steps" id="lowerpocket">Lower <br>Pocket</div>
                    <div class="steps" id="coinpocket">Coin <br>Pocket</div>
                    <div class="steps" id="backvent">Back <br> Vents</div>
                    <div class="steps" id="liningstyle">Lining <br> Style</div>
                    <div class="steps" id="shoulderstyle">Shoulder <br>Pad</div>
                    <div class="steps" id="buttonoption">Button <br>Options</div>
                    <div class="steps" id="buttonswatch">Button <br>Swatch</div>
                    <div class="steps" id="threadoption">Button <br>Thread</div>
                    <div class="steps" id="elbowpatch">Elbow <br>Patch</div>
                    <div class="steps" id="canvasoption">Canvas<br>Options</div>
                    <div class="steps" style="padding-top: 10px" id="monogram">Mono<br>gram</div>
                    <div class="steps" id="suitcolor">Jacket <br>Color</div>
                </div>
                <!--
                ---category div
                --->
                <div class="col-md-12 stepdiv" id="suitcategorys" style="display: block;">
                    <p class="stylecaption">Suit Category</p>
                    <div class="primarysuggestion okok active" id="suitcategory_formal" name="formal:formal" style="margin-left:25%">
                        <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Formal / Casual</p>
                    </div>
                    <div class="primarysuggestion okok" id="suitcategory_tuxedo" name="tuxedo:tuxedo">
                        <p class="suggestionname" style="line-height: 35px; font-size: 17px;">Tuxedo</p>
                    </div>
                </div>
                <!--
                -----button div
                --->
                <div class="col-md-12 stepdiv overflow-cmd" id="frontbuttons" style="display: none">
                    <p class="stylecaption">Front Button Style</p>
                    <div class="primarysuggestion active" name="C-0011:singlebreasted_one_buttons" id="frontbutton_onebutton">
                        <img src="../images/jackets/onebutton.png"/>
                        <p class="suggestionname">C-0011 Single Breasted</br>One Button</p>
                    </div>
                    <div class="primarysuggestion" id="frontbutton_twobutton" name="C-0012:singlebreasted_two_buttons" style="margin-left: 10px" title="C-0012 Single Breasted Two Buttons">
                        <img src="../images/jackets/twobutton.png"/>
                        <p class="suggestionname">C-0012 Single Breasted</br>Two Buttons(most popular)</p>
                    </div>
                    <div class="primarysuggestion" id="frontbutton_threebutton" name="C-0013:singlebreasted_three_buttons" style="margin-left: 10px">
                        <img src="../images/jackets/threebutton.png"/>
                        <p class="suggestionname">C-0013 Single Breasted</br>Three Buttons</p>
                    </div>
                    <div class="primarysuggestion" id="frontbutton_fourbutton" name="C-0014:singlebreasted_four_buttons" style="margin-left: 10px">
                        <img src="../images/jackets/fourbutton.png"/>
                        <p class="suggestionname">C-0014 Single Breasted</br>Four Buttons</p>
                    </div>
                    <div class="primarysuggestion" name="C-0017:doublebreasted_four_buttons" id="frontbutton_doublebreasted" style="margin-left: 10px">
                        <img src="../images/jackets/doublebreasted.png"/>
                        <p class="suggestionname">C-0017 Double Breasted</br>Four Buttons</p>
                    </div>
                    <div class="primarysuggestion" name="C-0020:doublebreasted_eight_buttons" id="frontbutton_doublebreastedeightbuttons" style="margin-left: 10px">
                        <img src="../images/jackets/doublebreastedeightbuttons.png"/>
                        <p class="suggestionname">C-0020 Double Breasted</br>Eight Buttons</p>
                    </div>
                    <div class="primarysuggestion" name="C-0015:doublebreasted_four_buttons_two_buttons_fasten" id="frontbutton_doublebreastedfourbuttonstwobuttonsfasten" style="margin-left: 10px" title="Double Breasted four Buttons two Buttons Fasten">
                        <img src="../images/jackets/doublebreastedfourbuttonstwobuttonsfasten.png"/>
                        <p class="suggestionname">C-0015 Double Breasted</br> four Buttons two Buttons Fasten</p>
                    </div>
                    <div class="primarysuggestion" name="C-0016:doublebreasted_six_buttons" id="frontbutton_C-0016doublebreastedsixbuttons" style="margin-left: 10px" title="Double Breasted four Buttons two Buttons Fasten">
                        <img src="../images/jackets/C-0016doublebreastedsixbuttons.png"/>
                        <p class="suggestionname">C-0016 Double Breasted</br>Six buttons</p>
                    </div>
                    <div class="primarysuggestion" name="C-0019:doublebreasted_six_buttons_three_buttons_fasten" id="frontbutton_doublebreastedsixbuttonsthreebuttonsfasten" style="margin-left: 10px" title="Double Breasted six Buttons Three Buttons Fasten">
                        <img src="../images/jackets/doublebreastedsixbuttonsthreebuttonsfasten.png"/>
                        <p class="suggestionname">C-0019 Double Breasted</br>Six Buttons Three Buttons Fasten</p>
                    </div>
                    <div class="primarysuggestion" name="C-002F:doublebreasted_two_buttons_one_button_fasten" id="frontbutton_doublebreastedtwobuttonsonebuttonfasten" style="margin-left: 10px" title="Double Breasted two Buttons One Button Fasten">
                        <img src="../images/jackets/doublebreastedtwobuttonsonebuttonfasten.png"/>
                        <p class="suggestionname">C-002F Double Breasted</br>Two Buttons One Button Fasten</p>
                    </div>
                    <div class="primarysuggestion" name="C-0023:singlebreasted_five_buttons" id="frontbutton_singlebreastedfivebuttons" style="margin-left: 10px">
                        <img src="../images/jackets/singlebreastedfivebuttons.png"/>
                        <p class="suggestionname">C-0023 Single Breasted</br>Five Buttons</p>
                    </div>
                    <div class="primarysuggestion" name="C-0026:singlebreasted_six_buttons" id="frontbutton_singlebreastedsixbuttons" style="margin-left: 10px">
                        <img src="../images/jackets/singlebreastedsixbuttons.png"/>
                        <p class="suggestionname">C-0026 Single Breasted</br>Six Buttons</p>
                    </div>
                    <div class="primarysuggestion" name="C-002C:singlebreasted_three_buttons_lapel_roll" id="frontbutton_singlebreastedthreebuttonslapelroll" style="margin-left: 10px" title="Single Breasted Three Buttons Lapel Roll">
                        <img src="../images/jackets/singlebreastedthreebuttonslapelroll.png"/>
                        <p class="suggestionname">C-002C Single Breasted</br>Three Buttons Buttons Lapel Roll</p>
                    </div>
                    <div class="primarysuggestion" name="C-001Z:singlebreasted_two_buttons_lapel_roll_between" id="frontbutton_singlebreastedtwobuttonslapelrollbetween" style="margin-left: 10px" title="Single Breasted two Buttons Lapel Roll Between">
                        <img src="../images/jackets/singlebreastedtwobuttonslapelrollbetween.png"/>
                        <p class="suggestionname">C-001Z Single Breasted</br>Two Buttons Lapel Roll Between</p>
                    </div>
                </div>
                <!--
                ----lapel style div
                --->
                <div class="col-md-12 overflow-cmd stepdiv" id="lapelstyles" style="display:none">
                    <p class="stylecaption">Lapel Style</p>
                    <div class="primarysuggestion active " id="lapelstyle_notch" name="C-0001:notch">
                        <img src="../images/jackets/notch.png"/>
                        <p class="suggestionname">C-0001 Notch Lapel</br>(most popular)</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_peak" name="C-0002:peak">
                        <img src="../images/jackets/peak.png"/>
                        <p class="suggestionname">C-0002</br>Peak Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_seminotch" name="C-0003:semi_notch">
                        <img src="../images/jackets/seminotch.png"/>
                        <p class="suggestionname">C-0003</br>Semi Notch Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_semipeak" name="C-0004:semi_peak">
                        <img src="../images/jackets/semipeak.png"/>
                        <p class="suggestionname">C-0004</br>Semi Peak Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_shawl" name="C-0005:shawl">
                        <img src="../images/jackets/shawl.png"/>
                        <p class="suggestionname">D-0005</br>Shawl Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_fishmouth" name="C-0006:fish_mouth">
                        <img src="../images/jackets/fishmouth.png"/>
                        <p class="suggestionname">C-0006</br>Fish Mouth lapel</p>
                    </div>
                    <div id="notindoublebreasted">
                        <div class="primarysuggestion" name="C-0681:sport_collar_a" title="Sport Collar A(Notch Lapel With Left Square Tab)" id="lapelstyle_sportcollara">
                            <img src="../images/jackets/sportcollara.png"/>
                            <p class="suggestionname">C-0681</br>Sport Collar A(Notch Lapel With Left Square Tab)</p>
                        </div>
                        <div class="primarysuggestion" name="C-0682:sport_collar_b" title="Sport Collar B(Notch Lapel With Left Pointed Tab)" id="lapelstyle_sportcollarb">
                            <img src="../images/jackets/sportcollarb.png"/>
                            <p class="suggestionname">C-0682</br>Sport Collar B(Notch Lapel With Left Pointed Tab)</p>
                        </div>
                        <div class="primarysuggestion" name="00F2:arc_underpart_shawl_lapel" title="00F2 arc underpart shawl lapel" id="lapelstyle_arcunderpartshawllapel">
                            <img src="../images/jackets/00F2-arc_underpart_shawl_lapel.png"/>
                            <p class="suggestionname">00F2</br>Arc underpart shawl lapel</p>
                        </div>
                        <div class="primarysuggestion" name="C-0685:sport_collar_f" title="Sport Collar F(Notch Lapel With Left Round Tab)" id="lapelstyle_sportcollarf">
                            <img src="../images/jackets/sportcollarf.png"/>
                            <p class="suggestionname">C-0685</br>Sport Collar F(Notch Lapel With Left Round Tab)</p>
                        </div>

                    </div>
                    <div class="primarysuggestion" id="lapelstyle_ducknotchlapel" name="C-001U:duck_notch">
                        <img src="../images/jackets/ducknotchlapel.png"/>
                        <p class="suggestionname">C-001U</br>Duck Notch Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_notchlapelroundcolar" name="C-001M:notch_round_collar">
                        <img src="../images/jackets/notchlapelroundcolar.png"/>
                        <p class="suggestionname">C-001M</br>Notch Lapel Round Colar</p>
                    </div>
                    <div class="primarysuggestion" title="Notch Lapel with Contrast Satin in Middle" name="C-0694:notch_lapel_with_contrast_satin_middle" id="lapelstyle_notchlapelwithcontrastsatininmiddle">
                        <img src="../images/jackets/notchlapelwithcontrastsatininmiddle.png"/>
                        <p class="suggestionname">C-0694</br>Notch Lapel with Contrast Satin in Middle</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_peaklepalroundcolar" name="C-001N:peak_round_collar">
                        <img src="../images/jackets/peaklepalroundcolar.png"/>
                        <p class="suggestionname">C-001N</br>Peak Lapel Round Colar</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_roundpeaklapel" name="C-001H:round_peak">
                        <img src="../images/jackets/roundpeaklapel.png"/>
                        <p class="suggestionname">C-001H</br>Round Peak Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_roundpeaklapelroundcolar" name="C-001J:round_peak_round_colar" title="Round Peak Lapel Round Colar">
                        <img src="../images/jackets/roundpeaklapelroundcolar.png"/>
                        <p class="suggestionname">C-001J</br>Round Peak Lapel Round Colar</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_shawllepalwithsatinonedge" name="C-0691:shawl_with_satin_on_edge" title="Shawl Lepal with Satin on Edge">
                        <img src="../images/jackets/shawllepalwithsatinonedge.png"/>
                        <p class="suggestionname">C-0691</br>Shawl Lapel with Round Colar</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_C-0009bandcollar" name="C-0009:shawl_with_satin_on_edge" title="Shawl Lepal with Satin on Edge">
                        <img src="../images/jackets/C-0009bandcollar.png"/>
                        <p class="suggestionname">C-0009</br>Band Collar</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_C-001Kroundnotchlapelroundcollar" name="C-001K:round_notch_round_collar" title="Round Notch Lapel Round Vollar">
                        <img src="../images/jackets/C-001Kroundnotchlapelroundcollar.png"/>
                        <p class="suggestionname">C-001K</br>Round Notch Lapel Round Vollar</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_C-001Groundnotchlapel" name="C-001G:round_notch">
                        <img src="../images/jackets/C-001Groundnotchlapel.png"/>
                        <p class="suggestionname">C-001G</br>Round Notch Lapel</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_C-0681sportcollaranotchlapelwithleftsquretab" name="C-0681:sport_collar_a_notch" title="Sport Collar A Notch Lapel with left squre tab">
                        <img src="../images/jackets/C-0681sportcollaranotchlapelwithleftsquretab.png"/>
                        <p class="suggestionname">C-0681</br>Sport Collar A Notch Lapel with left squre tab"</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_C-0682sportcollarbnotchlapelwithleftpointedtab" name="C-0682:sport_collar_b_notch" title="Sport Collar B Notch Lapel with left pointed tab">
                        <img src="../images/jackets/C-0682sportcollarbnotchlapelwithleftpointedtab.png"/>
                        <p class="suggestionname">C-0682</br>Sport Collar B Notch Lapel with left pointed tab</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_C-0683sportcollarcnotchlapelwithlefttrapzoidtab" name="C-0683:sport_collar_c_notch" title="Sport Collar C Notch lapel with left trapzoid tab">
                        <img src="../images/jackets/C-0683sportcollarcnotchlapelwithlefttrapzoidtab.png"/>
                        <p class="suggestionname">C-0683</br>Sport Collar C Notch lapel with left trapzoid tab</p>
                    </div>
                    <div class="primarysuggestion" id="lapelstyle_090Aashawllepal" name="090A:a_shawl_lepal" title="090A A Shawl Lepal">
                        <img src="../images/jackets/090A-ashawllepal.png"/>
                        <p class="suggestionname">090A</br>A Shawl Lepal</p>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top:30px;">
                        <p class="stylecaption">Lapel Button Holes</p>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-055Pbuttonholeonleftonlyforshawlanddiamondlapel" name="C-055P:button_hole_on_left" title="buttonhole on left only for shawl and diamond lapel">
                            <img src="../images/jackets/C-055Pbuttonholeonleftonlyforshawlanddiamondlapel.png"/>
                            <p class="suggestionname">C-055P</br>buttonhole on left only for shawl and diamond lapel</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0541left" name="C-0541:lapel_button_hole_left">
                            <img src="../images/jackets/C-0541left.png"/>
                            <p class="suggestionname">C-0541</br>left</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0542right" name="C-0542:lapel_button_hole_right">
                            <img src="../images/jackets/C-0542right.png"/>
                            <p class="suggestionname">C-0542</br>right</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0543rightandleft" name="C-0543:lapel_button_hole_right_and_left">
                            <img src="../images/jackets/C-0543rightandleft.png"/>
                            <p class="suggestionname">C-0543</br>right and left</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0544nolapelbuttonhole" name="C-0544:no_lapel_button_hole">
                            <img src="../images/jackets/C-0544nolapelbuttonhole.png"/>
                            <p class="suggestionname">C-0544</br>no lapel button hole</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0545lefttwo" name="C-0545:lapel_button_holes_left_two">
                            <img src="../images/jackets/C-0545lefttwo.png"/>
                            <p class="suggestionname">C-0545</br>left two</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0546righttwo" name="C-0546:lapel_button_holes_right_two">
                            <img src="../images/jackets/C-0546righttwo.png"/>
                            <p class="suggestionname">C-0546</br>right two</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0549leftthreerighttwo" name="C-0549:lapel_button_holes_left_three_right_two">
                            <img src="../images/jackets/C-0549leftthreerighttwo.png"/>
                            <p class="suggestionname">C-0549</br>left three right two</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholes_C-0550leftthree" name="C-0550:lapel_button_holes_left_three">
                            <img src="../images/jackets/C-0550leftthree.png"/>
                            <p class="suggestionname">C-0550</br>left three</p>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top:30px;">
                        <p class="stylecaption">Buttonhole Style Lapel</p>
                        <div class="primarysuggestion" id="lapelbuttonholesstyle_C-055Krealstraightwith05cmbuttonhole" name="C-055K:real_straight_with_0.5cm_button_hole" title="Real straight with 0.5cm button hole">
                            <img src="../images/jackets/C-055Krealstraightwith05cmbuttonhole.png"/>
                            <p class="suggestionname">C-055K(most recommended)</br>Real straight with 0.5cm button hole</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholesstyle_C-0551straight" name="C-0551:lapel_button_hole_straight" title="">
                            <img src="../images/jackets/C-0551straight.png"/>
                            <p class="suggestionname">C-0551</br>straight</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholesstyle_C-0552round" name="C-0552:lapel_button_hole_round" title="">
                            <img src="../images/jackets/C-0552round.png"/>
                            <p class="suggestionname">C-0552</br>round</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholesstyle_C-0553realstraight" name="C-0553:lapel_button_hole_real_straight" title="">
                            <img src="../images/jackets/C-0553realstraight.png"/>
                            <p class="suggestionname">C-0553</br>real straight</p>
                        </div>
                        <div class="primarysuggestion" id="lapelbuttonholesstyle_C-0554realround" name="C-0554:lapel_button_hole_real_round" title="">
                            <img src="../images/jackets/C-0554realround.png"/>
                            <p class="suggestionname">C-0554</br>real round</p>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top:30px;">
                        <p class="stylecaption" style="height: 35px; line-height: 1.4;">Lapel button holes thread options</p>
                        <div class="col-md-12" style="height: 400px;overflow-y:scroll;">
                            <?php
                            $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $a++;
                                        $colorName = $rows['colorName'];
                                        ?>
                                        <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>'
                                             title="<?php echo $colorName ?>" name="<?php echo $colorName.":".$colorName ?>"
                                             id='lapelbuttonholesthread_<?php echo $colorName ?>'>
                                            <img src='../images/shirts/<?php echo $colorName.".png" ?>' />
                                        </div>
                                        <?php
                                    }
                                }else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!--
                -----lapel ingrediant style div
                -------->
                <div class="col-md-12 overflow-cmd  stepdiv" id="lapelingredients" style="display:none">
                    <p class="stylecaption">Ingredients Style</p>
                    <div class="primarysuggestion active" name="C-0652:satin_lapel" id="lapelingredient_C-0652satinlapel">
                        <img src="../images/jackets/C-0652satinlapel.png"/>
                        <p class="suggestionname">C-0652 Satin Lapel</p>
                    </div>
                    <div class="primarysuggestion active" name="C-065Q:laple_fabric_customer_appointed" id="lapelingredient_C-065Qlaplefabriccustomerappointed" title="C-065Q laple fabric customer appointed">
                        <img src="../images/jackets/C-065Q-laplefabriccustomerappointed.png"/>
                        <p class="suggestionname">C-065Q Laple Fabric</p>
                    </div>

                    <div class="primarysuggestion active" name="C-065R:1.0cm_satin_on_lapel_edge" id="lapelingredient_C-065R10cmsatinonlapeledge" title="C-065R 1.0cm Satin on lapel edge">
                        <img src="../images/jackets/C-065R10cmsatinonlapeledge.png"/>
                        <p class="suggestionname">C-065R 1.0cm Satin</p>
                    </div>
                    <div class="primarysuggestion active" name="09JA:05cm_piping_joined_on_front_lapel_and_lapel_edge_customer_appoint_fabric_for_piping" id="lapelingredient_09JA03cmpipingjoinedonfrontlapelandlapeledgecustomerappointfabricforpiping" title="09JA-03cm  lapel and lapel edge with satin piping customer appoint fabric for piping">
                        <img src="../images/jackets/C-065R10cmsatinonlapeledge.png"/>
                        <p class="suggestionname">09J0 0.5cm lapel and lapel edge with satin piping</p>
                    </div>
                    <div class="primarysuggestion active" name="C-065H:label_with_0.5cm_0.19in_satin_piping" id="lapelingredient_C-065Hlabelwith05cm019insatinpiping" title="label with 0.5cm (0.19)in satin_piping">
                        <img src="../images/jackets/C-065R10cmsatinonlapeledge.png"/>
                        <p class="suggestionname">C-065H label with 0.5cm(0.19in) satin piping</p>
                    </div>


                    <div class="primarysuggestion active" name="C-065T:1.2cm_satin_on_lapel_edge" id="lapelingredient_C-065T12cmsatinonlapeledge" title="C-065T 1.2cm satin on lapel edge">
                        <img src="../images/jackets/C-065T12cmsatinonlapeledge.png"/>
                        <p class="suggestionname">C-065T 1.2cm satin</p>
                    </div>
                    <div class="primarysuggestion active" name="C-0544:1.2cm_satin_on_lapel_edge" id="lapelingredient_C-065T12cmsatinonlapeledges" title="C-065T 1.2cm satin on lapel edge">
                        <img src="../images/jackets/C-0544nolapelbuttonhole.png"/>
                        <p class="suggestionname">standard no ingredient</p>
                    </div>
                    <div class="primarysuggestion active" name="C-0694:notch_lapel_with_contrast_satin_in_middle" id="lapelingredient_C-0694notchlapelwithcontrastsatininmiddle"
                         title="C-0694 notch lapel with contrast satin in middle :: Block in order button holes in lapel">
                        <img src="../images/jackets/C-0544nolapelbuttonhole.png"/>
                        <p class="suggestionname">C-0694 notch lapel with contrast satin in middle :: Block in order button holes in lapel</p>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="satinfabricdiv">
                        <p class="stylecaption">Satin Options</p>
                        <div class="primarysuggestion" name="DXN043A:white" id="lapelsatin_dxn043a_1">
                            <img src="../images/jackets/dxn043a.png" class=""/>
                            <p class="suggestionname">DXN043A White</p>
                        </div>
                        <div class="primarysuggestion" name="DXN032A:grey" id="lapelsatin_dxn032a_1">
                            <img src="../images/jackets/dxn032a.png" class=""/>
                            <p class="suggestionname">DXN032A Grey</p>
                        </div>
                        <div class="primarysuggestion" name="DXN031A:red" id="lapelsatin_dxn031a_1">
                            <img src="../images/jackets/dxn031a.png" class=""/>
                            <p class="suggestionname">DXN031A Red</p>
                        </div>
                        <div class="primarysuggestion" name="DXN030A:wine" id="lapelsatin_dxn030a_1">
                            <img src="../images/jackets/dxn030a.png" class=""/>
                            <p class="suggestionname">DXN030A Wine</p>
                        </div>
                        <div class="primarysuggestion" name="DXN029A:navy" id="lapelsatin_dxn029a_1">
                            <img src="../images/jackets/dxn029a.png" class=""/>
                            <p class="suggestionname">DXN029A Navy</p>
                        </div>
                        <div class="primarysuggestion" name="DXN008A:navy" id="lapelsatin_dxn008a_1">
                            <img src="../images/jackets/dxn008a.png" class=""/>
                            <p class="suggestionname">DXN008A Black</p>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="lapelingredients">
                        <p class="stylecaption">Breast Pocket Ingredients Style</p>
                        <div class="primarysuggestion active" name="C-0654:satin_chest_pocket" id="lapelingredient_C-0654satinchestpocket">
                            <img src="../images/jackets/C-0654satinchestpocket.png"/>
                            <p class="suggestionname">C-0654 Satin Chest Pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-065w:1.0cm_edge_of_opening_of_breast_pocket" id="lapelingredient_C-065w10cmedgeofopeningofbreastpocket">
                            <img src="../images/jackets/C-065w10cmedgeofopeningofbreastpocket.png"/>
                            <p class="suggestionname">C-065w 1.0cm edge of opening of breast pocket</p>
                        </div>

                        <div class="primarysuggestion active" name="C-065N:customer_appointed_brest_pocket_fabric" id="lapelingredient_C-065Ncustomerappointedbrestpocketfabric">
                            <img src="../images/jackets/C-065Ncustomerappointedbrestpocketfabric.png"/>
                            <p class="suggestionname">C-065N customer appointed brest pocket fabric</p>
                        </div>
                        <div class="primarysuggestion active" name="C-066G:piping_on_breast_pocket_lower_edge_customer_appointed_fabric" id="lapelingredient_C-066Gpipingonbreastpocketloweredgecustomerappointedfabric">
                            <img src="../images/jackets/C-066Gpipingonbreastpocketloweredgecustomerappointedfabric.png"/>
                            <p class="suggestionname">C-066G piping on breast pocket lower edge customer appointed fabric</p>
                        </div>
                    </div>

                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="satinfabricdiv">
                        <p class="stylecaption">Satin Options</p>
                        <div class="primarysuggestion" name="DXN043A:white" id="lapelsatin_dxn043a_2">
                            <img src="../images/jackets/dxn043a.png" class=""/>
                            <p class="suggestionname">DXN043A White</p>
                        </div>
                        <div class="primarysuggestion" name="DXN032A:grey" id="lapelsatin_dxn032a_2">
                            <img src="../images/jackets/dxn032a.png" class=""/>
                            <p class="suggestionname">DXN032A Grey</p>
                        </div>
                        <div class="primarysuggestion" name="DXN031A:red" id="lapelsatin_dxn031a_2">
                            <img src="../images/jackets/dxn031a.png" class=""/>
                            <p class="suggestionname">DXN031A Red</p>
                        </div>
                        <div class="primarysuggestion" name="DXN030A:wine" id="lapelsatin_dxn030a_2">
                            <img src="../images/jackets/dxn030a.png" class=""/>
                            <p class="suggestionname">DXN030A Wine</p>
                        </div>
                        <div class="primarysuggestion" name="DXN029A:navy" id="lapelsatin_dxn029a_2">
                            <img src="../images/jackets/dxn029a.png" class=""/>
                            <p class="suggestionname">DXN029A Navy</p>
                        </div>
                        <div class="primarysuggestion" name="DXN008A:navy" id="lapelsatin_dxn008a_2">
                            <img src="../images/jackets/dxn008a.png" class=""/>
                            <p class="suggestionname">DXN008A Black</p>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="lapelingredients">
                        <p class="stylecaption">Outside Lower Pocket Besom Fabric</p>
                        <div class="primarysuggestion active" name="C-0653:satin_besom" id="lapelingredient_C-0653satinbesom">
                            <img src="../images/jackets/C-0653satinbesom.png"/>
                            <p class="suggestionname">C-0653 Satin Besom</p>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="satinfabricdiv">
                        <p class="stylecaption">Satin Options</p>
                        <div class="primarysuggestion" name="DXN043A:white" id="lapelsatin_dxn043a_3">
                            <img src="../images/jackets/dxn043a.png" class=""/>
                            <p class="suggestionname">DXN043A White</p>
                        </div>
                        <div class="primarysuggestion" name="DXN032A:grey" id="lapelsatin_dxn032a_3">
                            <img src="../images/jackets/dxn032a.png" class=""/>
                            <p class="suggestionname">DXN032A Grey</p>
                        </div>
                        <div class="primarysuggestion" name="DXN031A:red" id="lapelsatin_dxn031a_3">
                            <img src="../images/jackets/dxn031a.png" class=""/>
                            <p class="suggestionname">DXN031A Red</p>
                        </div>
                        <div class="primarysuggestion" name="DXN030A:wine" id="lapelsatin_dxn030a_3">
                            <img src="../images/jackets/dxn030a.png" class=""/>
                            <p class="suggestionname">DXN030A Wine</p>
                        </div>
                        <div class="primarysuggestion" name="DXN029A:navy" id="lapelsatin_dxn029a_3">
                            <img src="../images/jackets/dxn029a.png" class=""/>
                            <p class="suggestionname">DXN029A Navy</p>
                        </div>
                        <div class="primarysuggestion" name="DXN008A:navy" id="lapelsatin_dxn008a_3">
                            <img src="../images/jackets/dxn008a.png" class=""/>
                            <p class="suggestionname">DXN008A Black</p>
                        </div>
                    </div>

                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="lapelingredients">
                        <p class="stylecaption">Tuxedo Style</p>
                        <div class="primarysuggestion active" name="C-0650:formal_tuxedo" id="lapelingredient_C-0650formaltuxedo">
                            <img src="../images/jackets/C-0650formaltuxedo.png"/>
                            <p class="suggestionname">C-0650 Formal Tuxedo</p>
                        </div>
                        <div class="primarysuggestion active" name="C-065Y:normal_tuxedo" id="lapelingredient_C-065Ynormaltuxedo">
                            <img src="../images/jackets/C-065Ynormaltuxedo.png"/>
                            <p class="suggestionname">C-065Y Normal Tuxedo</p>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="satinfabricdiv">
                        <p class="stylecaption">Satin Options</p>
                        <div class="primarysuggestion" name="DXN043A:white" id="lapelsatin_dxn043a_4">
                            <img src="../images/jackets/dxn043a.png" class=""/>
                            <p class="suggestionname">DXN043A White</p>
                        </div>
                        <div class="primarysuggestion" name="DXN032A:grey" id="lapelsatin_dxn032a_4">
                            <img src="../images/jackets/dxn032a.png" class=""/>
                            <p class="suggestionname">DXN032A Grey</p>
                        </div>
                        <div class="primarysuggestion" name="DXN031A:red" id="lapelsatin_dxn031a_4">
                            <img src="../images/jackets/dxn031a.png" class=""/>
                            <p class="suggestionname">DXN031A Red</p>
                        </div>
                        <div class="primarysuggestion" name="DXN030A:wine" id="lapelsatin_dxn030a_4">
                            <img src="../images/jackets/dxn030a.png" class=""/>
                            <p class="suggestionname">DXN030A Wine</p>
                        </div>
                        <div class="primarysuggestion" name="DXN029A:navy" id="lapelsatin_dxn029a_4">
                            <img src="../images/jackets/dxn029a.png" class=""/>
                            <p class="suggestionname">DXN029A Navy</p>
                        </div>
                        <div class="primarysuggestion" name="DXN008A:navy" id="lapelsatin_dxn008a_4">
                            <img src="../images/jackets/dxn008a.png" class=""/>
                            <p class="suggestionname">DXN008A Black</p>
                        </div>
                    </div>

                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="lapelingredients">
                        <p class="stylecaption">Collar Ingredients</p>
                        <div class="primarysuggestion active" name="C-0651:satintopcollar" id="lapelingredient_C-0651satintopcollar">
                            <img src="../images/jackets/C-0651satintopcollar.png"/>
                            <p class="suggestionname">C-0651 Satin Top Collar</p>
                        </div>
                        <div class="primarysuggestion active" name="C-065P:satin_edge_1.0cm" id="lapelingredient_C-065Psatinedge10cm">
                            <img src="../images/jackets/C-065Psatinedge10cm.png"/>
                            <p class="suggestionname">C-065P Satin Edge 1.0cm</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0658:collar_satin_edge_1.2cm" id="lapelingredient_C-0658collarsatinedge12cm">
                            <img src="../images/jackets/C-0658collarsatinedge12cm.png"/>
                            <p class="suggestionname">C-0658 Collar Satin Edge 1.2cm</p>
                        </div>
                        <div class="primarysuggestion active" name="C-065Z:collar_satin_edge_2cm" id="lapelingredient_C-065Zcollarsatinedge2cm">
                            <img src="../images/jackets/C-065Zcollarsatinedge2cm.png"/>
                            <p class="suggestionname">C-065Z Collar Satin Edge 2cm</p>
                        </div>
                        <div class="primarysuggestion active" name="C-09V1:collar_edge_customer_appointed_fabric_1.0_cm_wide" id="lapelingredient_C-09V1collaredgecustomerappointedfabric10cmwide">
                            <img src="../images/jackets/C-09V1collaredgecustomerappointedfabric10cmwide.png"/>
                            <p class="suggestionname">C-09V1 collar edge customer appointed fabric 1.0cm wide</p>
                        </div>
                        <div class="primarysuggestion active" name="C-09J7:collar_edge_customer_appointed_fabric_1.0_cm_wide" id="lapelingredient_C-09J7-collaredgecustomerappointedfabric10cmwide">
                            <img src="../images/jackets/C-09J7-collaredgecustomerappointedfabric10cmwide.png"/>
                            <p class="suggestionname">C-09J7 collar edge customer appointed fabric 1.0cm wide</p>
                        </div>
                        <div class="primarysuggestion active" name="C-065V:outside_collar_fabric_customer_appointed" id="lapelingredient_C-065Voutsidecollarfabriccustomerappointed">
                            <img src="../images/jackets/C-065Voutsidecollarfabriccustomerappointed.png"/>
                            <p class="suggestionname">C-065V outside collar fabric customer appointed</p>
                        </div>
                        <div class="primarysuggestion active" name="C-065S:top_collar_with_1.2_cm_satin_edge" id="lapelingredient_C-065Stopcollarwith12cmsatinedge">
                            <img src="../images/jackets/C-065Stopcollarwith12cmsatinedge.png"/>
                            <p class="suggestionname">C-065S top collar with 1.2 cm satin edge</p>
                        </div>
                        <div class="primarysuggestion active" name="066Z:05cm_satin_piping_only" id="lapelingredient_066Z05cmsatinpipingonly" title="066Z 0.5cm satin piping only collar outside">
                            <img src="../images/jackets/C-065R10cmsatinonlapeledge.png"/>
                            <p class="suggestionname">066Z 0.5cm satin piping only collar outside</p>
                        </div>
                    </div>

                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;" id="satinfabricdiv">
                        <p class="stylecaption">Satin Options</p>
                        <div class="primarysuggestion" name="DXN043A:white" id="lapelsatin_dxn043a_5" onclick="setSurplusPrice('DXN043A','SatinLast')">
                            <img src="../images/jackets/dxn043a.png" class=""/>
                            <p class="suggestionname">DXN043A White</p>
                        </div>
                        <div class="primarysuggestion" name="DXN032A:grey" id="lapelsatin_dxn032a_5" onclick="setSurplusPrice('DXN032A','SatinLast')">
                            <img src="../images/jackets/dxn032a.png" class=""/>
                            <p class="suggestionname">DXN032A Grey</p>
                        </div>
                        <div class="primarysuggestion" name="DXN031A:red" id="lapelsatin_dxn031a_5" onclick="setSurplusPrice('DXN031A','SatinLast')">
                            <img src="../images/jackets/dxn031a.png" class=""/>
                            <p class="suggestionname">DXN031A Red</p>
                        </div>
                        <div class="primarysuggestion" name="DXN030A:wine" id="lapelsatin_dxn030a_5" onclick="setSurplusPrice('DXN030A','SatinLast')">
                            <img src="../images/jackets/dxn030a.png" class=""/>
                            <p class="suggestionname">DXN030A Wine</p>
                        </div>
                        <div class="primarysuggestion" name="DXN029A:navy" id="lapelsatin_dxn029a_5" onclick="setSurplusPrice('DXN029A','SatinLast')">
                            <img src="../images/jackets/dxn029a.png" class=""/>
                            <p class="suggestionname">DXN029A Navy</p>
                        </div>
                        <div class="primarysuggestion" name="DXN008A:navy" id="lapelsatin_dxn008a_5" onclick="setSurplusPrice('DXN008A','SatinLast')">
                            <img src="../images/jackets/dxn008a.png" class=""/>
                            <p class="suggestionname">DXN008A Black</p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-6" style="margin-top:20px">
                            <label>Fabric Name</label>
                            <input type="text" class="form-control" value="" id="lapelsatinfabricfield"
                                   onkeyup="searchFabricData(this,'SatinLast')" placeholder="Enter Fabric Name" />
                            <ul class="list-unstyled data_ data-remove" id="list_data" style="background: #fff">
                            </ul>
                        </div>
                        <div class="form-group col-md-6" style="margin-top:20px">
                            <input type="button" class="btn btn-danger pull-right FabricBtnSatinLast"  onclick="lapelsatinfabric()"
                                   style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric" disabled />
                        </div>
                        <div class="row">
                            <label class="surplus_price_label" id="surplus_price_labelSatinLast" style="display: none">
                                Surplus Price :
                                <span class="surplus_price" id="surplus_priceSatinLast"></span>
                            </label>
                        </div>
                    </div>
                </div>
                <!--
                -----Chest dart style div
                --->
                <div class="col-md-12 overflow-cmd  stepdiv" id="chestdarts" style="display:none">
                    <p class="stylecaption">Chest Dart Style</p>
                    <div class="primarysuggestion active" name="C-0521:normal" id="chestdart_C-0521normalchestdart">
                        <img src="../images/jackets/C-0521normalchestdart.png"/>
                        <p class="suggestionname">C-0521 Normal Chest Dart</p>
                    </div>
                    <div class="primarysuggestion active" name="C-0522:no" id="chestdart_C-0522nochestdart">
                        <img src="../images/jackets/C-0522nochestdart.png"/>
                        <p class="suggestionname">C-0522 No Chest Dart</p>
                    </div>
                </div>
                <!--
                ----felt under collar style div
                --->
                <div class="col-md-12 overflow-cmd  stepdiv" id="feltcollars" style="display:none" name="">
                    <p class="stylecaption">Felt under collar Style</p>
                    <div class="primarysuggestion active " id="feltcollar_C-0671callarfeltmatchfabric" name="C-0671:callarfeltmatchfabric">
                        <img src="../images/jackets/C-0671-callarfeltmatchfabric.png"/>
                        <p class="suggestionname">C-0671</br>callar felt..</p>
                    </div>
                    <div class="primarysuggestion active " id="feltcollar_C-0672collarfeltcustomerappointed" name="C-0672:collarfeltcustomerappointed" title="collar felt customer appointed">
                        <img src="../images/jackets/C-0672-collarfeltcustomerappointed.png"/>
                        <p class="suggestionname">C-0672</br>collar felt..</p>
                    </div>
                    <div class="primarysuggestion active " id="feltcollar_C-0673fabriccollarfelt" name="C-0673:fabriccollarfelt" title="C-0673 fabric collar felt">
                        <img src="../images/jackets/C-0673-fabriccollarfelt.png"/>
                        <p class="suggestionname">C-0673</br>fabric collar felt</p>
                    </div>

                    <div class="primarysuggestion active " id="feltcollar_FLD-0110" name="FLD-0110:FLD-0110">
                        <img src="../images/jackets/FLD-0110.png"/>
                        <p class="suggestionname">FLD-0110</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-0118" name="FLD-0118:FLD-0118">
                        <img src="../images/jackets/FLD-0118.png"/>
                        <p class="suggestionname">FLD-0118</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-0916" name="FLD-0916:FLD-0916">
                        <img src="../images/jackets/FLD-0916.png"/>
                        <p class="suggestionname">FLD-0916</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-3803" name="FLD-3803:FLD-3803">
                        <img src="../images/jackets/FLD-3803.png"/>
                        <p class="suggestionname">FLD-3803</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-4011" name="FLD-4011:FLD-4011">
                        <img src="../images/jackets/FLD-4011.png"/>
                        <p class="suggestionname">FLD-4011</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-4813" name="FLD-4813:FLD-4813">
                        <img src="../images/jackets/FLD-4813.png"/>
                        <p class="suggestionname">FLD-4813</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-5419" name="FLD-5419:FLD-5419">
                        <img src="../images/jackets/FLD-5419.png"/>
                        <p class="suggestionname">FLD-5419</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-6544" name="FLD-6544:FLD-6544">
                        <img src="../images/jackets/FLD-6544.png"/>
                        <p class="suggestionname">FLD-6544</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-7417" name="FLD-7417:FLD-7417">
                        <img src="../images/jackets/FLD-7417.png"/>
                        <p class="suggestionname">FLD-7417</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-7420" name="FLD-7420:FLD-7420">
                        <img src="../images/jackets/FLD-7420.png"/>
                        <p class="suggestionname">FLD-7420</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-7421" name="FLD-7421:FLD-7421">
                        <img src="../images/jackets/FLD-7421.png"/>
                        <p class="suggestionname">FLD-7421</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD7436" name="FLD7436:FLD7436">
                        <img src="../images/jackets/FLD7436.png"/>
                        <p class="suggestionname">FLD7436</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-9935" name="FLD-9935:FLD-9935">
                        <img src="../images/jackets/FLD-9935.png"/>
                        <p class="suggestionname">FLD-9935</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD12126" name="FLD12126:FLD12126">
                        <img src="../images/jackets/FLD12126.png"/>
                        <p class="suggestionname">FLD12126</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-12345" name="FLD-12345:FLD-12345">
                        <img src="../images/jackets/FLD-12345.png"/>
                        <p class="suggestionname">FLD-12345</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-012347" name="FLD-012347:FLD-012347">
                        <img src="../images/jackets/FLD-012347.png"/>
                        <p class="suggestionname">FLD-012347</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-012346" name="FLD-012346:FLD-012346">
                        <img src="../images/jackets/FLD-012346.png"/>
                        <p class="suggestionname">FLD-012346</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_FLD-012348" name="FLD-012348:FLD-012348">
                        <img src="../images/jackets/FLD-012348.png"/>
                        <p class="suggestionname">FLD-012348</p>
                    </div>
                    <div class="primarysuggestion" id="feltcollar_nofelt" name="nofelt:nofelt">
                        <img src="../images/jackets/nofelt.png"/>
                        <p class="suggestionname">No Felt</p>
                    </div>
                </div>
                <!--
                ----Inner flap style div
                --->
                <div class="col-md-12 overflow-cmd stepdiv" id="innerflaps" style="display:none">
                    <p class="stylecaption">Inner Flap Style</p>
                    <div class="primarysuggestion active " name="C-0314:exterior_pocket_inner_flap_lining_match_fabric" id="innerflap_C-0314exteriorpocketinnerflapliningmatchfabric" title="C-0314 Exterior Pocket Inner flap lining match fabric">
                        <img src="../images/jackets/C-0314exteriorpocketinnerflapliningmatchfabric.png"/>
                        <p class="suggestionname">C-0314 Exterior Pocket Inner flap lining match fabric</br>(most recommended)</p>
                    </div>

                    <div class="primarysuggestion active " name="C-0316:exterior_pocket_inner_flap_with_pocket" id="innerflap_C-0316exteriorpocketinnerflapwithpocket" title="C-0316 Exterior Pocket Inner flap with pocket">
                        <img src="../images/jackets/C-0316exteriorpocketinnerflapwithpocket.png"/>
                        <p class="suggestionname">C-0316 Exterior Pocket Inner flap with pocket</p>
                    </div>

                    <div class="primarysuggestion active " name="C-0317:no_seal_stitch" id="innerflap_C-0317nosealstitch">
                        <img src="../images/jackets/C-0317nosealstitch.png"/>
                        <p class="suggestionname">C-0317 No Seal Stitch</p>
                    </div>
                </div>
                <!--
                ----facing style inner div
                --->
                <div class="col-md-12 overflow-cmd stepdiv" id="facingstyles" style="display:none">
                    <p class="stylecaption">Facing Style Inside</p>
                    <div class="primarysuggestion active " name="C-0701:inner_facing_a" id="facingstyle_C-0701innerfacinga">
                        <img src="../images/jackets/C-0701innerfacinga.png"/>
                        <p class="suggestionname">C-0701 Inner Facing A</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0702:inner_facing_b" id="facingstyle_C-0702innerfacingb">
                        <img src="../images/jackets/C-0702innerfacingb.png"/>
                        <p class="suggestionname">C-0702 Inner Facing B</br>(most recommended)</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0703:inner_facing_c" id="facingstyle_C-0703innerfacingc">
                        <img src="../images/jackets/C-0703innerfacingc.png"/>
                        <p class="suggestionname">C-0703 Inner Facing C</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0706:deconstructed_suits_or_soft_suit" id="facingstyle_C-0706innerfacingf">
                        <img src="../images/jackets/C-0706innerfacingf.png"/>
                        <p class="suggestionname">"C-0706 deconstructed suits or </br>soft suit"</p>
                    </div>
                </div>
                <!--
                -----Sleeve Slit Style div
                --->
                <div class="col-md-12 overflow-cmd stepdiv" name="" id="sleeveslits" style="display:none">
                    <p class="stylecaption">Sleeve Slit Style</p>
                    <div class="primarysuggestion active " name="C-061C:normal_sleeve_slit_normal_slant_botton_hole" id="sleeveslit_C-061Cnormalsleeveslitnormalslantbottonhole" title="C-061C Normal Sleeve Slit normal slant botton hole">
                        <img src="../images/jackets/C-061Cnormalsleeveslitnormalslantbottonhole.png"/>
                        <p class="suggestionname">C-061C Normal Sleeve Slit normal slant botton hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061D:real_slit_slant_button_hole" id="sleeveslit_C-061Drealslitslantbuttonhole" title="C-061D Real Slit slant button hole">
                        <img src="../images/jackets/C-061Drealslitslantbuttonhole.png"/>
                        <p class="suggestionname">C-061D Real Slit slant  button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061E:real_sleeve_slit_normal_button_hole" id="sleeveslit_C-061Erealsleeveslitnormalbuttonhole" title="C-061E real sleeve slit normal button hole">
                        <img src="../images/jackets/C-061Erealsleeveslitnormalbuttonhole.png"/>
                        <p class="suggestionname">C-061E real sleeve slit normal button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061F:normal_sleeve_slit_without_button_and_button_hole" id="sleeveslit_C-061Fnormalsleeveslitwithoutbuttonandbuttonhole" title="C-061F Normal sleeve slit without button and button hole">
                        <img src="../images/jackets/C-061Fnormalsleeveslitwithoutbuttonandbuttonhole.png"/>
                        <p class="suggestionname">C-061F Normal sleeve slit without button and button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061G:pointed_sleeve_epaulet_with_round_button_hole" id="sleeveslit_C-061Gpointedsleeveepauletwithroundbuttonhole" title="C-061G Pointed sleeve epaulet with round button hole">
                        <img src="../images/jackets/C-061Gpointedsleeveepauletwithroundbuttonhole.png"/>
                        <p class="suggestionname">C-061G Pointed sleeve epaulet with round button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061K:real_sleeve_slit_no_button_hole_no_button" id="sleeveslit_C-061Krealsleeveslitnobuttonholenobutton" title="C-061K Real sleeve slit no button hole no button">
                        <img src="../images/jackets/C-061Krealsleeveslitnobuttonholenobutton.png"/>
                        <p class="suggestionname">C-061K Real sleeve slit no button hole no button</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061M:square_sleeve_epaulet_with_round_button_hole" id="sleeveslit_C-061Msquaresleeveepauletwithroundbuttonhole" title="C-061M Square sleeve epaulet with round button hole">
                        <img src="../images/jackets/C-061Msquaresleeveepauletwithroundbuttonhole.png"/>
                        <p class="suggestionname">C-061M Square sleeve epaulet with round button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-061P:round_sleeve_epaulet_with_round_button_hole" id="sleeveslit_C-061Proundsleeveepauletwithroundbuttonhole" title="C-061P Round sleeve epaulet with round button hole">
                        <img src="../images/jackets/C-061Proundsleeveepauletwithroundbuttonhole.png"/>
                        <p class="suggestionname">C-061P Round sleeve epaulet with round button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0611:normal_sleeve_slit_normal_button_hole" id="sleeveslit_C-0611normalsleeveslitnormalbuttonhole" title="C-0611 Normal sleeve slit normal button hole">
                        <img src="../images/jackets/C-0611normalsleeveslitnormalbuttonhole.png"/>
                        <p class="suggestionname">C-0611 Normal sleeve slit normal button hole</br>(most recommended)</p>
                    </div>
                    <div class="primarysuggestion primarysuggestions1 active " name="C-0612:real_slit" id="sleeveslit_C-0612realslit" title="">
                        <img src="../images/jackets/C-0612realslit.png"/>
                        <p class="suggestionname">C-0612 Real Slit</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0613:no_slit_and_button_hole" id="sleeveslit_C-0613noslitandbuttonhole" title="C-0613 No slit and button hole">
                        <img src="../images/jackets/C-0613noslitandbuttonhole.png"/>
                        <p class="suggestionname">C-0613 No slit and button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0614:normal_sleeve_slit_no_button_hole" id="sleeveslit_C-0614normalsleeveslitnobuttonhole" title="C-0614 Normal sleeve slit no button hole">
                        <img src="../images/jackets/C-0614normalsleeveslitnobuttonhole.png"/>
                        <p class="suggestionname">C-0614 Normal sleeve slit no button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0615:real_sleeve_slit_no_button_hole" id="sleeveslit_C-0615realsleeveslitnobuttonhole" title="C-0615 Real sleeve slit no button hole">
                        <img src="../images/jackets/C-0615realsleeveslitnobuttonhole.png"/>
                        <p class="suggestionname">C-0615 Real sleeve slit no button hole</p>
                    </div>
                    <div class="primarysuggestion active " name="C-0616:real_sleeve_slit_cuff_without_button_and_button_hole" id="sleeveslit_C-0616realsleeveslitcuffwithoutbuttonandbuttonhole" title="C-0616 Real sleeve slit cuff without button and button hole">
                        <img src="../images/jackets/C-0616realsleeveslitcuffwithoutbuttonandbuttonhole.png"/>
                        <p class="suggestionname">C-0616 Real sleeve slit cuff without button and button hole</p>
                    </div>


                    <div class="col-md-12" style="margin-top: 64px;">
                        <p class="stylecaption" style="height: 35px; line-height: 1.4;">Sleeve slit thread</p>
                        <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 50px">
                            <?php
                            $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $a++;
                                        $colorName = $rows['colorName'];
                                        ?>
                                        <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' name="<?php echo $colorName.":".$colorName ?>" title="<?php echo $colorName ?>" id='sleeveslitthread_<?php echo $colorName ?>'>
                                            <img src='../images/shirts/<?php echo $colorName.".png" ?>' />
                                        </div>
                                        <?php
                                    }
                                }else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <p class="stylecaption" style="height: 35px; line-height: 1.4;">Sleeve slit Style</p>
                        <div class="primarysuggestion primarysuggestions2 active " name="C-0619:last_sleeve_button_hole_thread_customer_oppointed" id="sleeveslit_C-0619lastsleevebuttonholethreadcustomeroppointed" title="C-0619 last sleeve button hole thread customer oppointed">
                            <img src="../images/jackets/C-0619lastsleevebuttonholethreadcustomeroppointed.png" alt="" />
                            <p class="suggestionname">C-0619 last sleeve button hole thread customer oppointed</p>
                        </div>
                    </div>
                    <div class="col-md-12" style="margin-top: 64px;">
                        <p class="stylecaption" style="height: 35px; line-height: 1.4;">Sleeve slit thread</p>
                        <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 50px">
                            <?php
                            $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $a++;
                                        $colorName = $rows['colorName'];
                                        ?>
                                        <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' name="<?php echo "c0619_".$colorName.":".$colorName ?>" title="<?php echo $colorName ?>" id='sleeveslitthreadc0619_<?php echo $colorName ?>'>
                                            <img src='../images/shirts/<?php echo $colorName.".png" ?>' />
                                        </div>
                                        <?php
                                    }
                                }else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <!------
                -----breast pocket div
                --->
                <div class="col-md-12 overflow-cmd stepdiv" id="breastpockets" style="display:none">
                    <p class="stylecaption">Breast Pocket Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv active" name="C-0101:normal" id="breastpocket_topnormalbreastpocket">
                        <img src="../images/jackets/topnormalbreastpocket.png" class="backbtnsugg" />
                        <p class="suggestionname">C-0101 (most popular)</br>Normal Breast Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="topshipshape:ship_shape" id="breastpocket_topshipshape">
                        <img src="../images/jackets/topshipshape.png" class="backbtnsugg"/>
                        <p class="suggestionname">Ship Shape Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0100:no" id="breastpocket_topnobreastpocket">
                        <img src="../images/jackets/topnobreastpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0100</br>No Breast Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0102:arc" id="breastpocket_toparcbreastpocket">
                        <img src="../images/jackets/toparcbreastpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0102</br>Arc Breast Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0110:double_besom" id="breastpocket_topdoublebesom">
                        <img src="../images/jackets/topdoublebesom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0110</br>Double Besom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0111:double_besom_with_tab_and_button" id="breastpocket_topdoublebesomwithtabandbutton">
                        <img src="../images/jackets/topdoublebesomwithtabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0111 Double Besom </br>With Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="topdoublebesomwithflap:double_besom_with_flap" id="breastpocket_topdoublebesomwithflap">
                        <img src="../images/jackets/topdoublebesomwithflap.png" class="backbtnsugg"/>
                        <p class="suggestionname">Double Besom With Flap</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="topdoublebesomwithzipper:double_besom_with_zipper" id="breastpocket_topdoublebesomwithzipper">
                        <img src="../images/jackets/topdoublebesomwithzipper.png" class="backbtnsugg"/>
                        <p class="suggestionname">Double Besom With Zipper</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0150:patch" id="breastpocket_toppatchbreastpocket">
                        <img src="../images/jackets/toppatchbreastpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0150</br>Patch Breast Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0151:patch_breast_pocket_with_button_and_button_hole" title="Patch Breast Pocket With Button & Button Hole" id="breastpocket_toppatchbreastpocketwithbuttonandbuttonhole">
                        <img src="../images/jackets/toppatchbreastpocketwithbuttonandbuttonhole.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0151</br>Patch Breast Pocket With Button & Button Hole</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="toppatchpocketonepleatwithdiamondflap:patch_pocket_one_pleat_with_diamond_flap" title="Patch Pocket One Pleat With Diamond Flap" id="breastpocket_toppatchpocketonepleatwithdiamondflap">
                        <img src="../images/jackets/toppatchpocketonepleatwithdiamondflap.png" class="backbtnsugg"/>
                        <p class="suggestionname">Patch Pocket One Pleat With Diamond Flap</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="toppatchpocketwithflap:patch_pocket_with_flap" id="breastpocket_toppatchpocketwithflap">
                        <img src="../images/jackets/toppatchpocketwithflap.png" class="backbtnsugg"/>
                        <p class="suggestionname">Patch Pocket With Flap</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="toppatchpocketonepleatwithdiamondflapandbutton:patch_pocket_one_pleat_with_diamond_flap_and_button" title="Patch Pocket One Pleat With Diamond Flap & Button" id="breastpocket_toppatchpocketonepleatwithdiamondflapandbutton">
                        <img src="../images/jackets/toppatchpocketonepleatwithdiamondflapandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">Patch Pocket One Pleat With Diamond Flap & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-011B:2.0_cm_width_walt_breast_pocket" title="2.0cm width walt Breast Pocket" id="breastpocket_20cmwidthwaltbreastpocket">
                        <img src="../images/jackets/20cmwidthwaltbreastpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-011B</br>2.0cm width walt Breast Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0109:arc_breast_pocket_with_1.5_cm_satin" title="Arc Breast Pocket with 1.0cm Satin" id="breastpocket_arcbreastpocketwith15cmsatin">
                        <img src="../images/jackets/arcbreastpocketwith15cmsatin.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0109</br>Arc Breast Pocket with 1.0cm Satin</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0157:normal_right_breast_pocket_no_left_breast_pocket" title="Normal right breast pocket no left breast pocket" id="breastpocket_normalrightbreastpocketnoleftbreastpocket">
                        <img src="../images/jackets/normalrightbreastpocketnoleftbreastpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0157</br>Normal Right Breast pocket no left breast pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0121:patch_pocket_one_pleat" id="breastpocket_patchpocketonepleat">
                        <img src="../images/jackets/patchpocketonepleat.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0121</br>Patch Pocket One Pleat</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-00J2:trape_zoid_chest_pocket" id="breastpocket_trapezoidchestpocket">
                        <img src="../images/jackets/trapezoidchestpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-00J2</br>Trape Zoid Chest Pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-011A:welt_breast_pocket_with_12_width" title="welt breast pocket with 1.2 width" id="breastpocket_weltbreastpocketwith12width">
                        <img src="../images/jackets/weltbreastpocketwith12width.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-011A</br>welt breast pocket with 1.2 width</p>
                    </div>
                </div>
                <!--
                ----lower pocket div
                --->
                <div class="col-md-12 stepdiv" id="lowerpockets" style="display:none">
                    <p class="stylecaption">Lower Pocket Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv active" name="C-0201:normal" id="lowerpocket_normalpockets">
                        <img src="../images/jackets/normalpockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0201</br>Normal Pockets </br>(most popular)</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0202:normal_lower_pockets_and_normal_right_ticket_pocket" id="lowerpocket_C-0202normallowerpocketsandnormalrightticketpocket">
                        <img src="../images/jackets/C-0202normallowerpocketsandnormalrightticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0202</br>normal lower pockets & normal right ticket pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0204:normal_lower_pockets_and_double_besom_ticket_pocket" id="lowerpocket_C-0204normallowerpocketsanddoublebesomticketpocket">
                        <img src="../images/jackets/C-0204normallowerpocketsanddoublebesomticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0204</br>normal lower pockets & double besom ticket pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0211:diamond_flaps_lower_pockets_with_button_and_buttonhole" id="lowerpocket_C-0211diamondflapslowerpocketswithbuttonandbuttonhole">
                        <img src="../images/jackets/C-0211diamondflapslowerpocketswithbuttonandbuttonhole.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0211</br>diamond flaps lower pockets with button & buttonhole</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0232:double_besom_lower_pockets_with_no_ticket_pocket" id="lowerpocket_C-0232doublebesomlowerpocketswithnoticketpocket">
                        <img src="../images/jackets/C-0232doublebesomlowerpocketswithnoticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0232</br>double besom lower pockets with no ticket pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0234:double_besom_lower_pockets_with_double_besom_ticket_pocket" id="lowerpocket_C-0234doublebesomlowerpocketswithdoublebesomticketpocket">
                        <img src="../images/jackets/C-0234doublebesomlowerpocketswithdoublebesomticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0234</br>double besom lower pockets with double besom ticket pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0237:diamond_flaps" id="lowerpocket_diamondflapspockets">
                        <img src="../images/jackets/diamondflapspockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0237</br>Diamond Flaps Pockets</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0238:normal_lower_pockets_with_button_and_buttohole" id="lowerpocket_C-0238normallowerpocketswithbuttonandbuttohole">
                        <img src="../images/jackets/C-0238normallowerpocketswithbuttonandbuttohole.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0238</br>normal lower pockets with button & buttohole</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0239:diamond_flaps_pockets_with_button_and_button_hole" title="Diamond Flaps Pockets With Button & Hole" id="lowerpocket_diamondflapspocketswithbuttonandbuttonhole">
                        <img src="../images/jackets/diamondflapspocketswithbuttonandbuttonhole.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0239</br>Diamond Flaps Pockets With Button & Hole</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv " name="C-0231:double_besom" id="lowerpocket_doublebesompockets">
                        <img src="../images/jackets/doublebesompockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0231</br>Double Besom Pockets</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-02M1:double_besom_with_tab_and_button" title="Double Besom With Tab & Button" id="lowerpocket_doublebesomwithtabandbutton">
                        <img src="../images/jackets/doublebesomwithtabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02M1</br>Double Besom With Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv"  name="C-02M4:double_besom_with_round_tab_and_button" title="Double Besom With Round Tab & Button" id="lowerpocket_doublebesomwithroundtabandbutton">
                        <img src="../images/jackets/doublebesomwithroundtabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02M4</br>Double Besom With Round Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-02M3:double_besom_with_square_tab_and_button" title="Double Besom With Square Tab & Button" id="lowerpocket_doublebesomwithsquaretabandbutton">
                        <img src="../images/jackets/doublebesomwithsquaretabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02M3</br>Double Besom With Square Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-02M8:normal_slant_patch_lower_pocket" title="" id="lowerpocket_C-02M8normalslantpatchlowerpocket">
                        <img src="../images/jackets/C-02M8normalslantpatchlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02M8</br>normal slant patch lower pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0200:no"  id="lowerpocket_nolowerpockets">
                        <img src="../images/jackets/nolowerpockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0200</br>No Lower Pockets</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-02J1:patch"  id="lowerpocket_patchpockets">
                        <img src="../images/jackets/patchpockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02J1</br>Patch Pockets</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-02K3:patch_pockets_with_double_pleats"  title="Patch Pockets With Double Pleats" id="lowerpocket_patchpocketswithdoublepleats">
                        <img src="../images/jackets/patchpocketswithdoublepleats.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02K3</br>Patch Pockets With Double Pleats</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv"  name="patchpocketswithpointedtabandbutton:patch_pockets_with_pointed_tab_and_button" title="Patch Pockets With Pointed Tab & Button" id="lowerpocket_patchpocketswithpointedtabandbutton">
                        <img src="../images/jackets/patchpocketswithpointedtabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">Patch Pockets With Pointed Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="patchpocketswithroundtabandbutton:patch_pockets_with_round_tab_and_button"  title="Patch Pockets With Round Tab & Button" id="lowerpocket_patchpocketswithroundtabandbutton">
                        <img src="../images/jackets/patchpocketswithroundtabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">Patch Pockets With Round Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv"  name="patchpocketswithsquaretabandbutton:patch_pockets_with_square_tab_and_button" title="Patch Pockets With Square Tab & Button" id="lowerpocket_patchpocketswithsquaretabandbutton">
                        <img src="../images/jackets/patchpocketswithsquaretabandbutton.png" class="backbtnsugg"/>
                        <p class="suggestionname">Patch Pockets With Square Tab & Button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-02K4:patch_pocket_with_single_pleat" title="Patch Pocket With Single Pleat" id="lowerpocket_patchpocketwithsinglepleat">
                        <img src="../images/jackets/patchpocketwithsinglepleat.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02K4</br>Patch Pocket With Single Pleat</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0261:welt" id="lowerpocket_weltpockets">
                        <img src="../images/jackets/weltpockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0261 </br>Welt Pockets</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-0653:satin_besom" id="lowerpocket_satinbesom">
                        <img src="../images/jackets/satinbason.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0653<br>Satin Besom</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02S1:welt_slant_lower_pocket" id="lowerpocket_C-02S1weltslantlowerpocket">
                        <img src="../images/jackets/C-02S1weltslantlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02S1<br>welt slant lower pocket</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02A1:normal_slant_lower_pockets" id="lowerpocket_C-02A1normalslantlowerpockets">
                        <img src="../images/jackets/C-02A1normalslantlowerpockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02A1<br>normal slant lower pockets</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02A2:normal_slant_lower_pockets_and_slant_ticket_pocket" id="lowerpocket_C-02A2normalslantlowerpocketsandslantticketpocket" style="height: 236px;width: 154px;">
                        <img src="../images/jackets/C-02A2normalslantlowerpocketsandslantticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02A2<br>normal slant lower pockets and slant ticket pocket</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02A3:normal_slant_lower_pockets_with_normal_slant_double_besom_ticket_pocket" id="lowerpocket_C-02A3normalslantlowerpocketswithnormalslantdoublebesomticketpocket" style="height: 236px;width: 168px;">
                        <img src="../images/jackets/C-02A3normalslantlowerpocketswithnormalslantdoublebesomticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02A3<br>normal slant lower pockets with normal slant double besom ticket pocket</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02D1:slanted_double_besom_lower_pockets" id="lowerpocket_C-02D1slanteddoublebesomlowerpockets" style="height: 236px;width: 170px;">
                        <img src="../images/jackets/C-02D1slanteddoublebesomlowerpockets.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02D1<br>slanted double besom lower pockets</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02D3:slanted_double_besom_lower_pockets_with_slanted_double_besom_ticket_pocket" id="lowerpocket_C-02D3slanteddoublebesomlowerpocketswithslanteddoublebesomticketpocket" style="height: 236px;width: 164px;">
                        <img src="../images/jackets/C-02D3slanteddoublebesomlowerpocketswithslanteddoublebesomticketpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02D3<br>slanted double besom lower pockets with slanted double besom ticket pocket</p>
                    </div>
                    <div class="primarysuggestion  backbtnsuggparentdiv" name="C-02G1:normal_slant_single_besom_lower_pocket" id="lowerpocket_C-02G1normalslantsinglebesomlowerpocket" style="height: 208px;width: 157px;">
                        <img src="../images/jackets/C-02G1normalslantsinglebesomlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-02G1<br>normal slant single besom lower pocket</p>
                    </div>
                </div>
                <!--
                ----coin pocket div
                --->
                <div class="col-md-12 stepdiv" id="coinpockets" style="display:none">
                    <p class="stylecaption">Coin Pocket Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv active" name="C-320:no" id="coinpocket_C-320nocoinpocket">
                        <img src="../images/jackets/C-320nocoinpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-320</br>no coin pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0321:in_right_lower_pocket" id="coinpocket_C-0321coinpocketinrightlowerpocket" title="coin pocket in right lower pocket">
                        <img src="../images/jackets/C-0321coinpocketinrightlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0321</br>coin pocket in right lower pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-322:in_left_lower_pocket" id="coinpocket_C-322coinpocketinleftlowerpocket" title="coin pocket in left lower pocket">
                        <img src="../images/jackets/C-322coinpocketinleftlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-322</br>coin pocket in left lower pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-323:in_lower_pocket" id="coinpocket_C-323coinpocketinlowerpocket" title="coin pocket in lower pocket">
                        <img src="../images/jackets/C-323coinpocketinlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-323</br>coin pocket in lower pocket</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0324:double_coin_pocket_in_right_lower_pocket" id="coinpocket_C-0324doublecoinpocketinrightlowerpocket" title="double coin pocket in right lower pocket">
                        <img src="../images/jackets/C-0324doublecoinpocketinrightlowerpocket.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0324</br>double coin pocket in right lower pocket</p>
                    </div>
                </div>
                <!--
                ----back vent div
                --->
                <div class="col-md-12 stepdiv" id="backvents" style="display:none;margin-bottom:170px">
                    <p class="stylecaption">Back Vent Style</p>

                    <div class="primarysuggestion" id="backvent_novent" name="C-04N0:no_vent" >
                        <img src="../images/jackets/novent.png"/>
                        <p class="suggestionname">No Vent</br>C-04N0 (standard)</p>
                    </div>
                    <div class="primarysuggestion active" id="backvent_centeralvent" name="C-04N1:centeral_one_vent" >
                        <img src="../images/jackets/centralvent.png"/>
                        <p class="suggestionname">One Vent </br>C-04N1</p>
                    </div>
                    <div class="primarysuggestion" id="backvent_sidevents" name="C-04N2:side_two_vents" >
                        <img src="../images/jackets/sidevents.png"/>
                        <p class="suggestionname">Two Vents (most popular) </br>C-04N2</p>
                    </div>
                    <div class="primarysuggestion" id="backvent_C-04N3sideventswithstrip" name="C-04N3:side_vents_with_strip"  title="C-04N3 side vents with strip">
                        <img src="../images/jackets/C-04N3sideventswithstrip.png"/>
                        <p class="suggestionname">C-04N3 side vents with with strip</p>
                    </div>
                    <div class="primarysuggestion" id="backvent_C-04V0blindpleatnovent" name="C-04V0:blind_pleat_novent" >
                        <img src="../images/jackets/C-04V0blindpleatnovent.png"/>
                        <p class="suggestionname">C-04V0 blind pleat no vent</p>
                    </div>
                    <div class="primarysuggestion" id="backvent_C-04V1blindpleatcentralvent" name="C-04V1:blind_pleat_central_vent"  title="C-04V1 blind pleat central vent">
                        <img src="../images/jackets/C-04V1blindpleatcentralvent.png"/>
                        <p class="suggestionname">C-04V1 blind pleat central vent</p>
                    </div>
                    <div class="primarysuggestion" id="backvent_C-04V2blindpleatwithdoublevents" name="C-04V2L:blind_pleat_with_double_vents"  title="C-04V2 blind pleat with double vents">
                        <img src="../images/jackets/C-04V2blindpleatwithdoublevents.png"/>
                        <p class="suggestionname">C-04V2 blind pleat with double vents</p>
                    </div>
                    <div class="primarysuggestion" id="backvent_C-04V3blindpleatwithdoubleventsandventstrap" name="C-04V3:blind_pleat_with_double_vents_and_vent_strap" title="C-04V3 blind pleat with double vents and vent strap">
                        <img src="../images/jackets/C-04V3blindpleatwithdoubleventsandventstrap.png"/>
                        <p class="suggestionname">C-04V3 blind pleat with double vents and vent strap</p>
                    </div>
                </div>
                <!--
                ----lining Style div
                --->
                <div class="col-md-12 stepdiv overflow-cmd" id="liningstyles" style="display:none;margin-bottom:70px">
                    <p class="stylecaption">Lining Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05M2:1/2_lining_in_seam_with_pinding" id="liningstyle_12lininginseamwithpinding">
                        <img src="../images/jackets/12lininginseamwithpinding.png" class="backbtnsugg"/>
                        <p class="suggestionname">1/2 lining C-05M2</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C1:full_lining" id="liningstyle_fulllining">
                        <img src="../images/jackets/fulllining.png" class="backbtnsugg"/>
                        <p class="suggestionname">Full lining C-05C1 (standard)</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05M4:1/2_lining_pick_stitch_on_lining_in_seam_normal_straight_bottom" title="1/2 lining,Pick stitch on lining inseam'Normal straight bottom" id="liningstyle_12liningpickstitchonlininginseamnormalstraightbottom">
                        <img src="../images/jackets/12liningpickstitchonlininginseamnormalstraightbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">1/2 lining C-05M4</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C1:full_lining_round_bottom" id="liningstyle_C-05C1fullliningroundbottom">
                        <img src="../images/jackets/C-05C1fullliningroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C1 Full Linin ground bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C2:full_lining_reduce_0.6_round_bottom" title="C-05C2 Full Lining Reduce 0.6 round bottom" id="liningstyle_05C2fullliningreduce06roundbottom">
                        <img src="../images/jackets/C-05C2fullliningreduce06roundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C2 Full Lining Reduce 0.6 round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv"  name="C-05C3:full_lining_add_0.6_round_bottom" title="C-05C3 Full Lining add 0.6 round bottom" id="liningstyle_C-05C3fullliningadd06roundbottom">
                        <img src="../images/jackets/C-05C3fullliningadd06roundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C3 Full Lining add 0.6 round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C4:full_lining_1.2_cm_round_bottom"  title="C-05C4 full lining 1.2cm round bottom" id="liningstyle_C-05C4fulllining12cmroundbottom">
                        <img src="../images/jackets/C-05C4fulllining12cmroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C4 full lining 1.2cm round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C4:no_lining_binding_in_seam_small_round_bottom"  title="C-05C4 No lining binding inseam small round bottom" id="liningstyle_C-05C4noliningbindinginseamsmallroundbottom">
                        <img src="../images/jackets/C-05C4noliningbindinginseamsmallroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C4 No lining binding inseam small round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C5:full_lining_straight_bottom"  title="C-05C5 full lining straight bottom" id="liningstyle_C-05C5fullliningstraightbottom">
                        <img src="../images/jackets/C-05C5fullliningstraightbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C5 full lining straight bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C6:full_lining_big_round_bottom"  title="C-05C6 full lining big round bottom" id="liningstyle_C-05C6fullliningbigroundbottom">
                        <img src="../images/jackets/C-05C6fullliningbigroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C6 full lining big round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05C9:lining_pick_stich_on_lining_in_seam_normal_round_bottom"  title="C-05C9 lining pick stich on lining inseam normal round bottom" id="liningstyle_C-05C9liningpickstichonlininginseamnormalroundbottom">
                        <img src="../images/jackets/C-05C9liningpickstichonlininginseamnormalroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05C9 lining pick stich on lining inseam normal round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05M1:lining_pick_stich_on_lining_in_seam_reduce_0.6_round_bottom" title="C-05M1 lining pic stich on lining in seam reduce 0.6 round bottom" id="liningstyle_C-05M1liningpicstichonlininginseamreduce06roundbottom">
                        <img src="../images/jackets/C-05M1liningpicstichonlininginseamreduce06roundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05M1 lining pick stich on lining in seam reduce 0.6 round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05M3:lining_pick_stich_on_lining_in_seam_add_1.2_cm_round_bottom" title="C-05M3 lining pic stich on lining inseam add 1.2cm round bottom" id="liningstyle_C-05M3liningpicstichonlininginseamadd12cmroundbottom">
                        <img src="../images/jackets/C-05M3liningpicstichonlininginseamadd12cmroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05M3 lining pick stich on lining inseam add 1.2cm round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05M5:lining_pick_stich_on_lining_in_seam_large_round_bottom" title="C-05M5 lining pick stich on lining inseam large round bottom" id="liningstyle_C-05M5liningpickstichonlininginseamlargeroundbottom">
                        <img src="../images/jackets/C-05M5liningpickstichonlininginseamlargeroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05M5 lining pick stich on lining inseam large round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05U4:lining_binding_in_seam_normal_round_bottom" title="C-05U4 lining binding in seam normal round bottom" id="liningstyle_C-05U4liningbindinginseamnormalroundbottom">
                        <img src="../images/jackets/C-05U4liningbindinginseamnormalroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05U4 lining binding in seam normal round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05U5:lining_binding_in_seam_smaller_round_bottom" title="C-05U5 lining binding in seam smaller round bottom" id="liningstyle_C-05U5liningbindinginseamsmallerroundbottom">
                        <img src="../images/jackets/C-05U5liningbindinginseamsmallerroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05U5 lining binding in seam  smaller round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05V3:no_lining_binding_in_seam_normal_round_bottom"  title="C-05V3 no lining binding inseam normal round bottom" id="liningstyle_C-05V3noliningbindinginseamnormalroundbottom">
                        <img src="../images/jackets/C-05V3noliningbindinginseamnormalroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05V3 no lining binding inseam  normal round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C05T713:lining_binding_in_seam_1/4_small_rounded_bottom"  title="C-05T7 1/3 lining binding inseam 1/4 small rounded bottom" id="liningstyle_C05T713liningbindinginseam14smallroundedbottom">
                        <img alt="" src="../images/jackets/C-05T713liningbindinginseam14smallroundedbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C05T713 Deconstructed or Soft Suit</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05P7:1/3_lining_round_bottom"  title="" id="liningstyle_C-05P713liningroundbottom">
                        <img alt="" src="../images/jackets/C-05P713liningroundbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05P7 1/3 lining round bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05R1:1/3_lining_straight_bottom"  title="" id="liningstyle_C-05R113liningstraightbottom">
                        <img alt="" src="../images/jackets/C-05R113liningstraightbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05R1 1/3 lining straight bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-05R9:1/4_lining_straight_bottom"  title="" id="liningstyle_C-05R914liningstraightbottom">
                        <img alt="" src="../images/jackets/C-05R914liningstraightbottom.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-05R9 1/4 lining straight bottom</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0711:lining_color_match_fabric"  title="" id="liningstyle_C-0711liningcolormatchfabric">
                        <img alt="" src="../images/jackets/C-0711liningcolormatchfabric.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0711 lining color match fabric</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0723:sleeve_lining_with_white_stripes"  title="" id="liningstyle_C-0723sleeveliningwithwhitestripes">
                        <img alt="" src="../images/jackets/C-0723sleeveliningwithwhitestripes.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0723 sleeve lining with white stripes</p>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
                        <p class="stylecaption">Lining Options</p>
                        <div class="col-md-12" style="height: 350px;overflow-y: scroll;padding: 0" id="liningoptiondiv">
                            <?php
                            $query = "select * from vest_option where type='linning_option' and status = '1'";
                            $result = mysql_query($query);
                            if($result){
                                $num = mysql_num_rows($result);
                                if($num>0){
                                    $status = "done";
                                    $a=0;
                                    while($rows = mysql_fetch_array($result)){
                                        $option_name = $rows['option_name'];
                                        ?>
                                        <div class="primarysuggestion" style="height:100px;width:18%"
                                             id="liningoption_<?php echo $option_name ?>" onclick=setSurplusPrice('<?php echo rawurlencode($option_name); ?>','LiningOption1')>
                                            <img src="../images/vest/<?php echo $option_name ?>.jpg" style="min-width:100%;max-width:100%"/>
                                            <p class="suggestionname"><?php echo $option_name ?></p>
                                        </div>
                                        <?php
                                    }
                                }
                                else{
                                    echo "No Color Found";
                                }
                            }else{
                                echo mysql_error();
                            }
                            ?>
                        </div>
                        <div class="col-md-12" style="padding: 0">
                            <div class="form-group col-md-6" style="margin-top:20px;padding:0">
                                <label>Fabric Name</label>
                                <input type="text" class="form-control" value="" id="liningoptionfield"
                                       onkeyup="searchFabricData(this,'LiningOption1')" placeholder="Enter Fabric Name" />
                                <ul class="list-unstyled data_ data-remove" id="list_data" style="background: #fff">
                                </ul>
                            </div>
                            <div class="form-group col-md-6" style="margin-top:20px">
                                <input type="button" class="btn btn-danger pull-right FabricBtnLiningOption1"  onclick="addliningoption()"
                                       style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" disabled />
                            </div>
                            <div class="row">
                                <label class="surplus_price_label" id="surplus_price_labelLiningOption1" style="display: none">
                                    Surplus Price :
                                    <span class="surplus_price" id="surplus_priceLiningOption1"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12 contrastdiv" style="margin-top: 15px;">
                            <p class="stylecaption">Lining</p>
                            <div class="primarysuggestion" id="lining_lining13" name="240FLLDL059:240FLLDL059">
                                <img src="../images/jackets/lining13.png" class=""/>
                                <p class="suggestionname">240FLLDL059</p>
                            </div>
                            <div class="primarysuggestion" id="lining_lining14" name="240FLLDL059:240FLLDL059">
                                <img src="../images/jackets/lining14.png" class=""/>
                                <p class="suggestionname">240FLLDL059</p>
                            </div>
                            <div class="primarysuggestion" id="lining_lining15" name="263FLLDL068:263FLLDL068">
                                <img src="../images/jackets/lining15.png" class=""/>
                                <p class="suggestionname">263FLLDL068</p>
                            </div>
                            <div class="primarysuggestion" id="lining_lining16" name="264FLLDL069:264FLLDL069">
                                <img src="../images/jackets/lining16.png" class=""/>
                                <p class="suggestionname">264FLLDL069</p>
                            </div>
                            <div class="primarysuggestion" id="lining_lining17" name="265FLLDL070:265FLLDL070">
                                <img src="../images/jackets/lining17.png" class=""/>
                                <p class="suggestionname">265FLLDL070</p>
                            </div>
                        </div>
                    </div>
                </div>
                <!--
                ----shoulder Style div
                --->
                <div class="col-md-12 stepdiv" id="shoulderstyles" style="display:none;margin-bottom:70px">
                    <p class="stylecaption">Shoulder Pad Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-060Ahighsleevhead" name="C-060A:a_high_sleev_head">
                        <img src="../images/jackets/C-060Ahighsleevhead.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-060A high sleev head</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-060Nniapolitanshoulder" name="C-060N:niapol_itan_shoulder">
                        <img src="../images/jackets/C-060Nniapolitanshoulder.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-060N niapol itan shoulder</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0601normalshoulder" name="C-0601:normal_shoulder">
                        <img src="../images/jackets/C-0601normalshoulder.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0601 normal shoulder</br>(most standard)</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0602snubbyshoulder" name="C-0602:snubby_shoulder">
                        <img src="../images/jackets/C-0602snubbyshoulder.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0602 snubby shoulder</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0603naturalshoulder" name="C-0603:natural_shoulder">
                        <img src="../images/jackets/C-0603naturalshoulder.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0603 natural shoulder</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0604causashoulderbrunecucine" name="C-0604:causa_shoulder_brune_cucine">
                        <img src="../images/jackets/C-0604causashoulderbrunecucine.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0604 causa shoulder brune cucine</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" id="shoulderstyle_C-0607shouldertempraryfixedbyhand" name="C-0607:shoulder_temprary_fixed_by_hand">
                        <img src="../images/jackets/C-0607shouldertempraryfixedbyhand.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0607 shoulder temprary fixed by hand</p>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
                        <p class="stylecaption">Left shoulder padding</p>
                        <div class="col-md-5">
                            <img src="../images/jackets/shoulderpadding.png" class="padding-size" />
                        </div>
                        <div class="col-md-7">
                            <p style="font-weight: bold; margin-bottom: 22px;">Four levels of shoulder padding</p>
                            <select id="paddingpad">
                                <option selected="selected" value="03cm">060P left shoulder pad-FDJ-A32044 0.3cm</option>
                                <option value="06cm">060Q left shoulder pad-A22025 0.6CM</option>
                                <option value="12cm">060R left shoulder pad-A20005 1.2cm (recommended)</option>
                                <option value="2cm">060S left shoulder pad-A20595 2CM</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
                        <p class="stylecaption">Right shoulder padding</p>
                        <div class="col-md-5">
                            <img src="../images/jackets/shoulderpadding.png" class="padding-sizer" />
                        </div>
                        <div class="col-md-7">
                            <p style="font-weight: bold; margin-bottom: 22px;">Four levels of shoulder padding</p>
                            <select id="paddingpadr">
                                <option selected="selected" value="03cm">060T right shoulder pad-FDJ-A32044 0.3cm</option>
                                <option value="06cm">060U right shoulder pad-A22025 0.6CM</option>
                                <option value="12cm">060V right shoulder pad-A20005 1.2cm </option>
                                <option value="2cm">060W right shoulder pad-A20595 2CM</option>

                            </select>
                        </div>
                    </div>
                </div>
                <!--
                ----buttoning option div
                --->
                <div class="col-md-12 stepdiv" id="buttonoptions" style="display:none;margin-bottom:70px">
                    <p class="stylecaption">Buttoning Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="buttonoption_C-0643buttoningstyle" name="C-0643:buttoning_style_C-0643">
                        <img src="../images/jackets/C-0643buttoningstyle.png" />
                        <p class="suggestionname">C-0643 Buttoning Style</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" id="buttonoption_C-0644buttoingstyle" name="C-0644:buttoing_style_C-0644">
                        <img src="../images/jackets/C-0644buttoingstyle.png" />
                        <p class="suggestionname">C-0644 Buttoing Style</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0645:claw_shape_only_for_handmade" id="buttonoption_C-0645clawshapeonlyforhandmade" title="C-0645clawshape (only for hand made)">
                        <img src="../images/jackets/C-0645clawshapeonlyforhandmade.png" />
                        <p class="suggestionname">C-0645 Claw Shape</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0646:square_buttoning" id="buttonoption_C-0646squarebuttoning" title="">
                        <img src="../images/jackets/C-0646squarebuttoning.png" />
                        <p class="suggestionname">C-0646(most popular) </br>Square Buttoning </p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" name="C0632:nut_button" id="buttonoption_C0632nutbutton" title="">
                        <img src="../images/jackets/C0632nutbutton.png" />
                        <p class="suggestionname">C0632</br>nut button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0633:horn_button" id="buttonoption_C-0633hornbutton" title="">
                        <img src="../images/jackets/C-0633hornbutton.png" />
                        <p class="suggestionname">C-0633</br>horn button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0634:pearl_button" id="buttonoption_C-0634pearlbutton" title="">
                        <img src="../images/jackets/C-0634pearlbutton.png" />
                        <p class="suggestionname">C-0634</br>pearl button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0635:fabric_covered_button" id="buttonoption_C-0635fabriccoveredbutton" title="">
                        <img src="../images/jackets/C-0635fabriccoverdbutton.png" />
                        <p class="suggestionname">C-0635</br>fabric covered button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-065C:satin_covered_button" id="buttonoption_C-065Csatincoveredbutton" title="">
                        <img src="../images/jackets/C-065Csatincoveredbutton.png" />
                        <p class="suggestionname">C-065C</br>satin covered button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-065C:satin_covered_button" id="buttonoption_C-065Csatincoveredbutton" title="">
                        <img src="../images/jackets/C-065Csatincoveredbutton.png" />
                        <p class="suggestionname">C-065C</br>satin covered button</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-0638:button_customer_appointed" id="buttonoption_C-0638buttoncustomerappointed" title="">
                        <img src="../images/jackets/C-0638buttoncustomerappointed.png" />
                        <p class="suggestionname">C-0638</br>button customer appointed</p>
                    </div>
                </div>
                <!--
                ----button swatch options style div
                --->
                <div class="col-md-12 stepdiv" id="buttonswatchs" style="display:none">
                    <p class="stylecaption">Button Swatch Style</p>
                    <div class="col-md-12" style="height: 400px;overflow-y: scroll">
                        <?php
                        $query = "select * from button where btn_type='jacket' and status = '1'";
                        $result = mysql_query($query);
                        if($result){
                            $num = mysql_num_rows($result);
                            if($num>0){
                                $status = "done";
                                $a=0;
                                while($rows = mysql_fetch_array($result)){
                                    $a++;
                                    $btn_name = $rows['btn_name'];
                                    $btn_image = $rows['btn_image'];
                                    echo "<input type='hidden' id='buttonswatchVar' value='".$btn_name."'>";
                                    ?>
                                    <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>'
                                         name="<?php  echo $btn_name ?>" title="<?php  echo $btn_name ?>"
                                         id='buttonswatch_<?php echo $btn_name ?>'>
                                        <img src='api/Files/images/buttons/<?php  echo $btn_image; ?>' />
                                        <p class="suggestionname" style="text-transform:uppercase;"><?php echo $btn_name; ?></p>
                                    </div>
                                    <?php
                                }
                            }else{
                                echo "No button Found";
                            }
                        }else{
                            echo mysql_error();
                        }
                        ?>
                    </div>
                </div>
                <!--
                ----Thread options style div
                --->
                <div class="col-md-12 stepdiv" id="threadoptions" style="display:none">
                    <p class="stylecaption">Button Thread Options</p>
                    <div class="col-md-12" style="height: 400px;overflow-y: scroll">
                        <?php
                        $query = "select * from wp_colors where type='shirt' and subtype='buttoncut' and status = '1'";
                        $result = mysql_query($query);
                        if($result){
                            $num = mysql_num_rows($result);
                            if($num>0){
                                $status = "done";
                                $a=0;
                                while($rows = mysql_fetch_array($result)){
                                    $a++;
                                    $colorName = $rows['colorName'];
                                    ?>
                                    <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' name="<?php echo $colorName.":".$colorName ?>" title="<?php echo $colorName ?>" id='threadoption_<?php echo $colorName ?>'>
                                        <img src='../images/shirts/<?php echo $colorName.".png" ?>' />

                                    </div>
                                    <?php
                                }
                            }else{
                                echo "No Color Found";
                            }
                        }else{
                            echo mysql_error();
                        }
                        ?>
                    </div>
                </div>
                <!--
                ----elbow patch Style div
                --->
                <div class="col-md-12 stepdiv" id="elbowpatchs" style="display:none;margin-bottom:70px">
                    <p class="stylecaption">Elbow Patch Style</p>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_roundelbowpatch" name="C-0609:round_elbow_patch">
                        <img src="../images/jackets/roundelbowpatch.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0609 Round Elbow Patch</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_ovalelbowpatch" name="C-0610:oval_elbow_patch">
                        <img src="../images/jackets/ovalelbowpatch.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-0610 Oval Elbow Patch</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatch_noelbowpatch" name="noelbowpatch:no_elbow_patch">
                        <img src="../images/jackets/noelbowpatch.jpg" class="backbtnsugg"/>
                        <p class="suggestionname">No Elbow Patch</p>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top: 55px;">
                        <p class="stylecaption">Elbow Patch Color Options</p>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor1" name="QXZP504:QXZP504">
                            <img src="../images/jackets/elbowcolor1.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP504</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor2" name="QXZP511:QXZP511">
                            <img src="../images/jackets/elbowcolor2.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP511</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor3" name="QXZP510:QXZP510">
                            <img src="../images/jackets/elbowcolor3.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP510</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor4" name="QXZP527:QXZP527">
                            <img src="../images/jackets/elbowcolor4.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP527</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor5" name="QXZP509:QXZP509">
                            <img src="../images/jackets/elbowcolor5.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP509</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor6" name="QXZP519:QXZP519">
                            <img src="../images/jackets/elbowcolor6.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP519</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor7" name="QXZP508:QXZP508">
                            <img src="../images/jackets/elbowcolor7.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP508</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor8" name="QXZP507:QXZP508">
                            <img src="../images/jackets/elbowcolor8.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP507</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor9" name="QXZP522:QXZP522">
                            <img src="../images/jackets/elbowcolor9.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP522</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor10" name="QXZP521:QXZP521">
                            <img src="../images/jackets/elbowcolor10.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP521</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor11" name="QXZP513:QXZP513">
                            <img src="../images/jackets/elbowcolor11.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP513</p>
                        </div>
                        <div class="primarysuggestion backbtnsuggparentdiv" id="elbowpatchcolor_elbowcolor12" name="QXZP506:QXZP506">
                            <img src="../images/jackets/elbowcolor12.png" class="backbtnsugg"/>
                            <p class="suggestionname">QXZP506</p>
                        </div>
                    </div>
                </div>
                <!--
         ----Monograms Style Div
         --->
                <div class="col-md-12 stepdiv overflow-cmd" id="monograms" style="display:none">
                    <p class="stylecaption">Suit Monogram</p>
                    <div class="col-md-12 contrastdiv">
                        <div class="col-md-4 form-group" style="padding:0">
                            <label>Monogram Letters</label>
                            <input type="text" style="font-weight:normal" class="form-control" onKeyUp="edValueKeyPress(this.value)" id="monolatters" value="" />
                        </div>
                        <div class="col-md-1"></div>
                        <div class="col-md-4 form-group" style="padding:0">
                            <label>Monogram Output</label>
                            <br>
                            <span id="lettertype"></span>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv">
                        <label>Monogram Font</label><br><br>
                        <label style="font-family:ZapfChancery;font-size:18px;width: 70px;"><input checked type="radio" name="font" value="RC01" class="rccode" id="ZapfChancery"/>&nbsp;RC01</label>
                        <label style="font-family:times-roman;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC02" class="rccode" id="times-roman"/> RC02</label>
                        <label style="font-family:MicrosoftYaHeiBold;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC03" class="rccode" id="MicrosoftYaHeiBold"/> RC03</label>
                        <label style="font-family:commercial-script-bt;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC04" class="rccode" id="commercial-script-bt"/>&nbsp;RC04</label>
                        <label style="font-family:bradley-hand-itc;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC05" class="rccode" id="bradley-hand-itc"/>&nbsp;RC05</label>
                        <label style="font-family:iowan-old-style;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC06" class="rccode" id="iowan-old-style"/>&nbsp;RC06</label>
                        <label style="font-family:FangSong;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC07" class="rccode" id="FangSong"/>&nbsp;RC07</label>
                        <label style="font-family:old-english-text-mt;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC08" class="rccode" id="old-english-text-mt"/>&nbsp;RC08</label>
                        <label style="font-family:Gradl-Regular;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC09" class="rccode" id="Gradl-Regular"/>&nbsp;RC09</label>
                        <label style="font-family:Raavi;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC10" class="rccode" id="Raavi"/> RC10</label>
                        <label style="font-family:pierre;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC11" class="rccode" id="pierre"/> RC11</label>
                        <label style="font-family:bradley-hand-itc;font-size:18px;width: 70px;"><input type="radio" name="font" value="RC12" class="rccode" id="bradley-hand-itc"/> RC12</label>

                    </div>
                    <div class="col-md-12">
                        <hr>
                        <p class="contrastheading">Monogram Thread</p>
                        <?php
                        $query = "select * from wp_colors where type='shirt' and subtype = 'monothread' and status = '1'";
                        $result = mysql_query($query);
                        if ($result) {
                            $num = mysql_num_rows($result);
                            if ($num > 0) {
                                $status = "done";
                                $a = 0;
                                while ($rows = mysql_fetch_array($result)) {
                                    $a++;
                                    $colorName = $rows['colorName'];
                                    $colorCode = $rows['onlyfor'];
                                    ?>
                                    <div class='primarysuggestion colorsuggestion<?php if ($a == 1) echo ' active' ?>'
                                         name="<?php echo $colorName ?>" title="<?php echo $colorName ?>"
                                         onclick="getColorCode('<?php echo $colorCode; ?>')"
                                         id='monogram_<?php echo $colorName ?>'>
                                        <input type="hidden" id="colorcode_<?php echo $colorName ?>"
                                               value="<?php echo $colorCode; ?>">
                                        <img src='../images/shirts/<?php echo $colorName . ".png" ?>'/>
                                    </div>
                                    <?php
                                }
                            } else {
                                echo "No Color Found";
                            }
                        } else {
                            echo mysql_error();
                        }
                        ?>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top:30px;">
                        <p class="contrastheading">Monogram Options</p>
                        <div class="primarysuggestion active" name="C-0870:No_brand_Label"
                             id="monogramoption_C-0870NobrandLabel" style="padding: 12px; height: 100px;">
                            <p class="suggestionname">C-0870 No brand Label</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0871:brand_Label_Below_left_pen_pocket"
                             id="monogramoption_C-0871brandLabelBelowleftpenpocket" style="padding: 12px; height: 100px;"
                             title="C-0871 brand Label Below left pen pocket">
                            <p class="suggestionname">C-0871 brand Label Below left pen pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-087M:brand_Label_2cm_below_left_inner_breast_pocket"
                             id="monogramoption_C-087MbrandLabel2cmbelowleftinnerbreastpocket" style="padding: 12px; height: 100px;"
                             title="C-087M brand Label 2cm below left inner breast pocket">
                            <p class="suggestionname">C-087M brand Label 2cm  below left inner breast pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-087L:brand_Label_2cm_below_left_inner_breast_pocket"
                             id="monogramoption_C-087LbrandLabel2cmbelowleftinnerbreastpocket" style="padding: 12px; height: 100px;"
                             title="C-087L brand Label 2cm below left inner breast pocket">
                            <p class="suggestionname">C-087L brand Label 2cm  below left inner breast pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0875:brand_Label_is_below_the_left_card_pocket"
                             id="monogramoption_C-0875brandLabelisbelowtheleftcardpocket" style="padding: 12px; height: 100px;"
                             title="C-0875 brand Label is below the left card pocket">
                            <p class="suggestionname">C-0875 brand Label is  below the left card pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0876:brand_Label_is_below_the_right_inner_pocket"
                             id="monogramoption_C-0876brandLabelisbelowtherightinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-0876 brand Label is below the right inner pocket">
                            <p class="suggestionname">C-0876 brand Label is below the right inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-087S:brand_Label_position_is_10cm_below_the_right_inner_pocket_in_the_middle"
                             id="monogramoption_C-087SbrandLabelpositionis10cmbelowtherightinnerpocketinthemiddle" style="padding: 12px; height: 100px;"
                             title="C-087S brand Label position is 10cm below the right inner pocket (in the middle)">
                            <p class="suggestionname">C-087S brand Label position is 10cm below the right inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08S1:brand_Label_is_above_the_left_card_pocket"
                             id="monogramoption_C-08S1brandLabelisabovetheleftcardpocket" style="padding: 12px; height: 100px;"
                             title="C-08S1 brand Label is above the left card pocket">
                            <p class="suggestionname">C-08S1 brand Label is above the left card pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08S2:brand_Label_is_2cm_above_the_left_inner_pocket"
                             id="monogramoption_C-08S2brandLabelis2cmabovetheleftinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-08S2 brand Label is 2cm above the left inner pocket">
                            <p class="suggestionname">C-08S2 brand Label is 2cm  above the left inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08S3:brand_Label_is_position_is_above_the_right_card_pocket"
                             id="monogramoption_C-08S3brandLabelispositionisabovetherightcardpocket" style="padding: 12px; height: 100px;"
                             title="C-08S3 brand Label is position is above the right card pocket">
                            <p class="suggestionname">C-08S3 brand Label is position is above the right card pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08S4:brand_Label_position_is_below_the_right_card_pocket"
                             id="monogramoption_C-08S4brandLabelpositionisbelowtherightcardpocket" style="padding: 12px; height: 100px;"
                             title="C-08S4 brand Label position is below the right card pocket">
                            <p class="suggestionname">C-08S4 brand Label position is below the right card pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-087H:No_fabric_label"
                             id="monogramoption_C-087HNofabriclabel" style="padding: 12px; height: 100px;"
                             title="C-087H No fabric label">
                            <p class="suggestionname">C-087H No fabric label</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0872:Brand_lable_is_10cm_below_the_right_inner_pocket"
                             id="monogramoption_C-0872Brandlableis10cmbelowtherightinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-0872 Brand lable is 10cm below the right inner pocket">
                            <p class="suggestionname">C-0872 Brand lable is 10cm  below the right inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0877:Fabric_label_position_is_5cm_below_the_left_pen_pocket"
                             id="monogramoption_C-0877Fabriclabelpositionis5cmbelowtheleftpenpocket" style="padding: 12px; height: 100px;"
                             title="C-0877 Fabric label position is 5cm below the left pen pocket">
                            <p class="suggestionname">C-0877 Fabric label position  is 5cm below the left pen pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0879:Brand_label_is_2cm_above_the_left_inner_pocket"
                             id="monogramoption_C-0879Brandlabelis2cmabovetheleftinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-0879 Brand label is 2cm above the left inner pocket">
                            <p class="suggestionname">C-0879 Brand label is 2cm  above the left inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08M3:Fabric_label_position_is_2cm_above_the_left_inner_pocket"
                             id="monogramoption_C-08M3Fabriclabelpositionis2cmabovetheleftinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-08M3 Fabric label position is 2cm above the left inner pocket">
                            <p class="suggestionname">C-08M3 Fabric label position is 2cm above the left inner pocket</p>
                        </div>
                    </div>
                    <div class="col-md-12 contrastdiv" style="margin-top:30px;">
                        <p class="contrastheading">Name Label Position</p>
                        <div class="primarysuggestion active" name="C-087H:No_Name_Label"
                             id="monogramposition_C-087HNoNameLabel" style="padding: 12px; height: 100px;">
                            <p class="suggestionname">C-087H No Name Label</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0873:2cm_above_left_inner_pocket"
                             id="monogramposition_C-08732cmaboveleftinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-0873 2cm above left inner pocket">
                            <p class="suggestionname">C-0873 2cm above left inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-0878:Name_label_below_right_inner_breasted_pocket"
                             id="monogramposition_C-0878Namelabelbelowrightinnerbreastedpocket" style="padding: 12px; height: 100px;"
                             title="C-0878 Name label below right inner breasted pocket">
                            <p class="suggestionname">C-0878 Name label below right  inner breasted pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-087B:Customer_name_label_2cm_above_right_inner_pocket"
                             id="monogramposition_C-087BCustomernamelabel2cmaboverightinnerpocket" style="padding: 12px; height: 100px;"
                             title="C-087B Customer name label 2cm above right inner pocket">
                            <p class="suggestionname">C-087B Customer name label 2cm  above right inner pocket</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08K1:the_embroided_position_of_customer_name_tag_is_10cm_below_left_inside"
                             id="monogramposition_C-08K1theembroidedpositionofcustomernametagis10cmbelowleftinside" style="padding: 12px; height: 100px;"
                             title="C-08K1 the embroided position of customer name tag is 10cm below left inside">
                            <p class="suggestionname">C-08K1 the embroided position  of customer name tag is 10cm below left inside</p>
                        </div>
                        <div class="primarysuggestion active" name="C-08K2:Name_label_above_left_name_card_pocket"
                             id="monogramposition_C-08K2Namelabelaboveleftnamecardpocket" style="padding: 12px; height: 100px;"
                             title="C-08K2 Name label above left name card pocket">
                            <p class="suggestionname">C-08K2 Name label above left name card pocket</p>
                        </div>
                    </div>

                </div>
                <!--
                ----canvas options div
                --->
                <div class="col-md-12 stepdiv" id="canvasoptions" style="display:none;margin-bottom:70px">
                    <p class="stylecaption">Canvas Options</p>
                    <div class="primarysuggestion active backbtnsuggparentdiv" name="C-000A:full_canvas"  id="canvasoption_C-000Afullcanvas" onclick="additional_values('canvasoption_C-000A')">
                        <img src="../images/jackets/C-000Afullcanvas.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-000A Full Canvas</br>(most popular)</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-000B:half_canvas"  id="canvasoption_C-000Bhalfcanvas">
                        <img src="../images/jackets/C-000Bhalfcanvas.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-000B Half Canvas</p>
                    </div>

                    <div class="primarysuggestion backbtnsuggparentdiv" name="C-00C1:fused"  id="canvasoption_C-00C1fused">
                        <img src="../images/jackets/C-00C1fused.png" class="backbtnsugg"/>
                        <p class="suggestionname">C-00C1 Fused</p>
                    </div>
                    <div class="primarysuggestion backbtnsuggparentdiv"  name="C-00C3:deconstructed_suits_or_soft_suit" id="canvasoption_C-00C3singlelayerchestfusing">
                        <img src="../images/jackets/C-00C3singlelayerchestfusing.png" class="backbtnsugg"/>
                        <p class="suggestionname">"C-00C3 Deconstructed suits or soft suit".</p>
                    </div>
                </div>
                <!--
                ----suit color div
                --->
                <div class="col-md-12 stepdiv" id="suitcolors" style="display:none;margin-bottom:70px">
                    <p class="stylecaption">Suit Color Style</p>
                    <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0" id="mainfabricdiv">
                        <?php
                        $query = "select * from wp_colors where type='suit' and status = '1'";
                        $result = mysql_query($query);
                        if($result){
                            $num = mysql_num_rows($result);
                            if($num>0){
                                $status = "done";
                                $a=0;
                                while($rows = mysql_fetch_array($result)){
                                    $a++;
                                    $colorName = $rows['colorName'];
                                    $displayImage = $rows['displayImage'];

                                    ?>
                                    <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>
                                    'name="<?php echo $colorName.":".$colorName ?>" title="<?php echo $colorName ?>"
                                         id='suitcolor_<?php echo $colorName ?>' onclick=setSurplusPrice('<?php echo rawurlencode($colorName); ?>','JacketColor')>
                                        <img src='../images/jackets/<?php echo $displayImage; ?>' />
                                        <p class="suggestionname"><?php echo $colorName;?></p>
                                    </div>
                                    <?php
                                }
                            }else{
                                echo "No Color Found";
                            }
                        }else{
                            echo mysql_error();
                        }
                        ?>
                    </div>
                    <div class="col-md-12" style="padding: 0">
                        <div class="form-group col-md-6" style="margin-top:20px;padding:0">
                            <label>Fabric Name</label>
                            <input type="text" class="form-control" value="" id="mainfabricfield" onkeyup="searchFabricData(this,'JacketColor')"
                                   placeholder="Enter Fabric Name" />
                            <ul class="list-unstyled data_ data-remove" id="list_data1" style="background: #fff">
                            </ul>
                        </div>
                        <div class="form-group col-md-6" style="margin-top:20px">
                            <input type="button" class="btn btn-danger pull-right FabricBtnJacketColor"  onclick="mainfabric()"
                                   style="margin-right: 44px;margin-top: 20px;width: 175px;" value="Find Fabric" id="findFabric1" disabled />
                        </div>
                        <div class="row">
                            <label class="surplus_price_label" id="surplus_price_labelJacketColor" style="display: none">
                                Surplus Price :
                                <span class="surplus_price" id="surplus_priceJacketColor"></span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="text-align: center; margin-top: 100px; float: right;">
                <label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
                <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
            </div>
        </div>
    </div>
    <div id ="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-4">.col-md-4</div>
                        <div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
        <?php include("footer.php");?>
    <script>
        var user_id = $("#user_id").val();
        function setSurplusPrice(colorName,fabricId){
            colorName = decodeURI(colorName);
            $("#surplus_price_label"+fabricId).show();
            var url = "../admin/webserver/getSurplusPrice.php";
            $.post(url, {"type":"Custom Suit","colorName":colorName},function(data){
                var data = JSON.parse(data);
                $("#surplus_price"+fabricId).html("$"+data.extra_price);
            });
        }
        $(".nav a").removeClass("active");
        var currPage = "";
        var suitcategory = "formal";
        var csuitcategory = "formal:formal";//for cart
        var frontbutton = "onebutton";
        var cfrontbutton = "C-0011:singlebreasted_one_buttons";// for cart
        var lapelstyle = "notch";
        var clapelstyle = "C-0001:notch";// for cart
        var lapelbuttonholes = "C-055P:button_hole_on_left";
        var lapelbuttonholesstyle = "C-055K:real_straight_with_0.5cm_button_hole";//
        var lapelbuttonholesthread = "buttoncut1202yellow:buttoncut1202yellow";
        var lapelingredient = "C-0652:satin_lapel";
        var lapelsatin = "DXN043A:White";
        var chestdart = "C-0521:normal";
        var feltcollar = "FLD-0110";
        var cfeltcollar = "FLD-0110:FLD-0110"; //for cart
        var innerflap = "C-0314:exterior_pocket_inner_flap_lining_match_fabric";
        var facingstyle = "C-0701:inner_facing_a";
        var sleeveslit = "C-061C:normal_sleeve_slit_normal_slant_botton_hole";
        var sleeveslitthreadc0619 = "C-061C:normal_sleeve_slit_normal_slant_botton_hole";

        var sleeveslitthread = "buttoncut1202yellow:buttoncut1202yellow";
        var breastpocket = "topnormalbreastpocket";
        var cbreastpocket = "C-0101:normal";// for cart
        var lowerpocket = "normalpockets";
        var clowerpocket = "C-0201:normal";// for cart
        var coinpocket = "C-320:no";
        var backvent = "novent";
        var cbackvent = "C-04N0:no_vent";// for cart
        var liningstyle = "12lininginseamwithpinding";
        var cliningstyle = "C-05M2:1/2_lining_in_seam_with_pinding";// for cart
        var liningoption = "FLL181:FLL181";
        var lining = "240FLLDL059:240FLLDL059";
        var shoulderstyle = "C-060A:a_high_sleev_head";
        var shoulderpadding = "C-060P:0.3cm";
        var buttonoption = "C-0643:buttoning_style_C-0643";
        var buttonswatch = $("#buttonswatchVar").val();
        var threadoption = "buttoncut1202yellow";
        var cthreadoption = "buttoncut1202yellow:buttoncut1202yellow";//for cart
        var elbowpatch = "C-0609:round_elbow_patch";
        var elbowpatchcolor = "QXZP504:QXZP504";
        var canvasoption = "C-000A:full_canvas";
        var suitcolor = "dbk053a";
        var csuitcolor = "dbk053a:dbk053a";//for cart
        var shirt = "shirt";
        var tie = "tie";
        var jacket = "jacket";
        var folder = "front";
        var armbutton = "armbutton";
        var sidearmbutton = "sidearmbutton";
        var sidearmbuttoncut = "sidearmbuttoncut";
        var sidearmbuttonpoint = "sidearmbuttonpoint";
        var monogramtext = "";
        var monogramfont = "Times New Roman";
        var monogramcode = "RC01";
        var monogramcolor = "buttoncut1202yellow";
        var monogramposition = "left_bottom_of_shirt_2cm_from_front_and_2cm_from_edge";
        var monogramoption = "attach_fabric_inside_yoke";

        function getPrevVal(prevIndex){
            var result = "";
            switch(prevIndex){
                case "suitcategory":
                    result = suitcategory;
                    break;
                case"frontbutton":
                    result = frontbutton;
                    break;
                case"lapelstyle":
                    result = lapelstyle;
                    break;
                case"lapelbuttonholes":
                    result = lapelbuttonholes;
                    break;
                case"lapelbuttonholesstyle":
                    result = lapelbuttonholesstyle;
                    break;
                case"lapelbuttonholesthread":
                    result = lapelbuttonholesthread;
                    break;
                case"lapelingredient":
                    result = lapelingredient;
                    break;
                case"lapelsatin":
                    result = lapelsatin;
                    break;
                case"chestdart":
                    result = chestdart;
                    break;
                case"feltcollar":
                    result = feltcollar;
                    break;
                case"innerflap":
                    result = innerflap;
                    break;
                case"facingstyle":
                    result = facingstyle;
                    break;
                case"sleeveslit":
                    result = sleeveslit;
                    break;
                case"sleeveslitthread":
                    result = sleeveslitthread;
                    break;
                case"breastpocket":
                    result = breastpocket;
                    break;
                case"lowerpocket":
                    result = lowerpocket;
                    break;
                case"coinpocket":
                    result = coinpocket;
                    break;
                case"backvent":
                    result = backvent;
                    break;
                case"liningstyle":
                    result = liningstyle;
                    break;
                case"liningoption":
                    result = liningoption;
                    break;
                case"lining":
                    result = lining;
                    break;
                case"shoulderstyle":
                    result = shoulderstyle;
                    break;
                case"shoulderpadding":
                    result = shoulderpadding;
                    break;
                case"buttonoption":
                    result = buttonoption;
                    break;
                case"buttonswatch":
                    result = buttonswatch;
                    break;
                case "threadoption":
                    result = threadoption;
                    break;
                case"elbowpatch":
                    result = elbowpatch;
                    break;
                case"elbowpatchcolor":
                    result = elbowpatchcolor;
                    break;
                case"canvasoption":
                    result = canvasoption;
                    break;
                case "monogramcolor":
                    result=monogramcolor;
                    break;
                case "monogramposition":
                    result=monogramposition;
                    break;
                case "monogramoption":
                    result=monogramoption;
                    break;
                case"suitcolor":
                    result = suitcolor;
                    break;
            }
            return result;
        }
        function loadSessionData() {
            $(".surplus_price_label").hide();
            $(".loader").fadeIn("slow");
            var prevIndex = $(".stepsactive").attr("id");
            currPage = prevIndex + "s";
            var name = getPrevVal(prevIndex);
            var item = name;
            var img = "";
            item = prevIndex + "_" + item;
            $(".primarysuggestion").removeClass("active");
            $("#" + item).addClass("active");
            if (currPage === "suitcategorys") {
                folder = "front";
                if(suitcategory == "tuxedo"){
                    tie = "bow";
                }
                else{
                    tie = "tie";
                }
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" +frontbutton+lapelstyle+".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "frontbuttons") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" +frontbutton+lapelstyle+".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "lapelstyles") {
                folder="front";
                if(frontbutton == "doublebreasted")
                {
                    $("#notindoublebreasted").hide();
                }
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "breastpockets") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "lowerpockets") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "backvents") {
                folder = "back";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "liningstyles") {
                folder = "back";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + liningstyle + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "shoulderstyles") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "elbowpatchs") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "canvasoptions") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "lapelingredients") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "chestdarts") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "feltcollars") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "innerflaps") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "facingstyles") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "sleeveslits") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "buttonoptions") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "buttonswatchs") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "threadoptions") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "coinpockets") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "monograms") {
                $(".loader").fadeOut("slow");
            }
            else if (currPage == "suitcolors") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }

        }

        $(".primarysuggestion").click(function (){
            var Img = "";
            $(".loader").fadeIn("slow");
            $(".primarysuggestion").removeClass("active");
            var value = this.id;
            var name = $("#"+value).attr("name");
            var type = value.split("_");
            var selection = type[1];
            $("#" + value).addClass("active");
            var currpage = type[0] + "s";
            switch(type[0]) {
                case "suitcategory":
                    suitcategory = selection;
                    csuitcategory = name;
                    break;
                case"frontbutton":
                    cfrontbutton = name;
                    frontbutton = selection;
                    break;
                case"lapelstyle":
                    clapelstyle = name;
                    lapelstyle = selection;
                    break;
                case"lapelbuttonholes":
                    lapelbuttonholes = name;
                    break;
                case"lapelbuttonholesstyle":
                    lapelbuttonholesstyle = name;
                    break;
                case"lapelbuttonholesthread":
                    lapelbuttonholesthread = name;
                    break;
                case"lapelingredient":
                    lapelingredient = name;
                    break;
                case"lapelsatin":
                    lapelsatin = name;
                    break;
                case"chestdart":
                    chestdart = name;
                    break;
                case"feltcollar":
                    feltcollar = selection;
                    cfeltcollar = name;
                    break;
                case"innerflap":
                    innerflap = name;
                    break;
                case"facingstyle":
                    facingstyle = name;
                    break;
                case"sleeveslit":
                    sleeveslit = name;
                    switch(sleeveslit){
                        case "C-0612:real_slit":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            sleeveslit= "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed";
                            break;
                        case "C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            sleeveslit= "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit";
                            break;
                    }
                    break;
                case "sleeveslitthreadc0619":
                    sleeveslitthreadc0619 = name;
                    $( "div[name^='"+sleeveslitthread+"']" ).addClass("active");
                    switch(sleeveslit){
                        case "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                        case "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                    }
                    break;

                case"sleeveslitthread":
                    sleeveslitthread = name;
                    switch(sleeveslit){
                        case "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                        case "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                    }
                    break;
                case"breastpocket":
                    cbreastpocket = name;
                    breastpocket = selection;
                    break;
                case"lowerpocket":
                    clowerpocket = name;
                    lowerpocket = selection;
                    if(chestdart === "C-0521:normal") {
                        if (lowerpocket === "nolowerpockets") {
                            chestdart = "C-0522:no";
                        }else{
                            chestdart = "C-0521:normal";
                        }
                    }
                    break;
                case"coinpocket":
                    coinpocket = name;
                    break;
                case"backvent":
                    cbackvent = name;
                    backvent = selection;
                    break;
                case"liningstyle":
                    liningstyle = selection;
                    cliningstyle = name;
                    break;
                case"liningoption":
                    liningoption = name;
                    break;
                case"lining":
                    lining = name;
                    break;
                case"shoulderstyle":
                    shoulderstyle = name;
                    break;
                case"shoulderpadding":
                    shoulderpadding = name;
                    break;
                case"buttonoption":
                    buttonoption = name;
                    break;
                case"buttonswatch":
                    buttonswatch = name;
                    break;
                case "threadoption":
                    threadoption = selection;
                    cthreadoption = name;
                    break;
                case"elbowpatch":
                    elbowpatch = name;
                    break;
                case"elbowpatchcolor":
                    elbowpatchcolor = name;
                    break;
                case"canvasoption":
                    canvasoption = name;
                    break;
                case "monogramcolor":
                    monogramcolor = name;
                    break;
                case "monogramposition":
                    monogramposition = name;
                    break;
                case "monogramoption":
                    monogramoption = name;
                    break;
                case"suitcolor":
                    suitcolor = selection;
                    csuitcolor = name;
                    break;
            }
            if (currpage == "suitcategorys") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                if(suitcategory == "tuxedo"){
                    tie = "bow";
                }else{
                    tie = "tie";
                }
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "frontbuttons") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "lapelstyles") {
                folder = "front";
                if(frontbutton == "doublebreasted")
                {
                    $("#notindoublebreasted").hide();
                }
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "breastpockets") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "lowerpockets") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "backvents") {
                folder = "back";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "liningstyles") {
                folder = "back";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + liningstyle + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "shoulderstyles") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "elbowpatchs") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "canvasoptions") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "lapelingredients") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "chestdarts") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "feltcollars") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "innerflaps") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "facingstyles") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "sleeveslits") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "buttonoptions") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "buttonswatchs") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "threadoptions") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "coinpockets") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "monograms") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "suitcolors") {
                folder = "front";
                $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+jacket + ".png");
                $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + shirt + ".png");
                $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + breastpocket + ".png");
                $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + lowerpocket + ".png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton+lapelstyle+ ".png");
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + ".png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "point.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + frontbutton + "cut.png");
                $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $(".loader").fadeOut("slow");
            }
            else{
                $(".loader").fadeOut("slow");
            }
            if(frontbutton === "doublebreasted"){
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
            }
            else if(frontbutton === "doublebreastedeightbuttons"){
                $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
            }
            /*
            * data store all values
            * */

            var url = '../admin/webserver/selectionData.php';

            $.post(url,{suitcategory:suitcategory,csuitcategory:csuitcategory,frontbutton:frontbutton,cfrontbutton:cfrontbutton,
                lapelstyle:lapelstyle,clapelstyle:clapelstyle,lapelbuttonholes:lapelbuttonholes,lapelbuttonholesstyle:lapelbuttonholesstyle,
                lapelbuttonholesthread:lapelbuttonholesthread,lapelingredient:lapelingredient,lapelsatin:lapelsatin,chestdart:chestdart,
                feltcollar:feltcollar,cfeltcollar:cfeltcollar,innerflap:innerflap,facingstyle:facingstyle,sleeveslit:sleeveslit,
                sleeveslitthreadc0619:sleeveslitthreadc0619,sleeveslitthread:sleeveslitthread,breastpocket:breastpocket,cbreastpocket:cbreastpocket,
                lowerpocket:lowerpocket,clowerpocket:clowerpocket,coinpocket:coinpocket,backvent:backvent,cbackvent:cbackvent,liningstyle:liningstyle,
                cliningstyle:cliningstyle,liningoption:liningoption,lining:lining,shoulderstyle:shoulderstyle,shoulderpadding:shoulderpadding,buttonoption:buttonoption,
                buttonswatch:buttonswatch,threadoption:threadoption,cthreadoption:cthreadoption,elbowpatch:elbowpatch,elbowpatchcolor:elbowpatchcolor,
                canvasoption:canvasoption,suitcolor:suitcolor,csuitcolor:csuitcolor,shirt:shirt,tie:tie,jacket:jacket,folder:folder,armbutton:armbutton,
                sidearmbutton:sidearmbutton,sidearmbuttoncut:sidearmbuttoncut,sidearmbuttonpoint:sidearmbuttonpoint,monogramtext:monogramtext,monogramfont:monogramfont,
                monogramcode:monogramcode,monogramcolor:monogramcolor,monogramposition:monogramposition,monogramoption:monogramoption

            },function(data){
                console.log('data --- '+data);
            });

        });
        function navigation(switcher) {

            $("#" + currPage).fadeOut("slow");

            if (switcher == "next") {
                if (currPage == "suitcategorys") {
                    $(".steps").removeClass("stepsactive");
                    $("#frontbuttons").fadeIn("slow");
                    $("#frontbutton").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "frontbuttons") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelstyles").fadeIn("slow");
                    $("#lapelstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lapelstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelingredients").fadeIn("slow");
                    $("#lapelingredient").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lapelingredients") {
                    $(".steps").removeClass("stepsactive");
                    $("#chestdarts").fadeIn("slow");
                    $("#chestdart").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "chestdarts") {
                    $(".steps").removeClass("stepsactive");
                    $("#feltcollars").fadeIn("slow");
                    $("#feltcollar").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "feltcollars") {
                    $(".steps").removeClass("stepsactive");
                    $("#innerflaps").fadeIn("slow");
                    $("#innerflap").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "innerflaps") {
                    $(".steps").removeClass("stepsactive");
                    $("#facingstyles").fadeIn("slow");
                    $("#facingstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "facingstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#sleeveslits").fadeIn("slow");
                    $("#sleeveslit").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "sleeveslits") {
                    $(".steps").removeClass("stepsactive");
                    $("#breastpockets").fadeIn("slow");
                    $("#breastpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "breastpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#lowerpockets").fadeIn("slow");
                    $("#lowerpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lowerpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#coinpockets").fadeIn("slow");
                    $("#coinpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "coinpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#backvents").fadeIn("slow");
                    $("#backvent").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "backvents") {
                    $(".steps").removeClass("stepsactive");
                    $("#liningstyles").fadeIn("slow");
                    $("#liningstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "liningstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#shoulderstyles").fadeIn("slow");
                    $("#shoulderstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "shoulderstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonoptions").fadeIn("slow");
                    $("#buttonoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "buttonoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonswatchs").fadeIn("slow");
                    $("#buttonswatch").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "buttonswatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#threadoptions").fadeIn("slow");
                    $("#threadoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "threadoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#elbowpatchs").fadeIn("slow");
                    $("#elbowpatch").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "elbowpatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#canvasoptions").fadeIn("slow");
                    $("#canvasoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "canvasoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#monograms").fadeIn("slow");
                    $("#monogram").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "monograms") {
                    $(".steps").removeClass("stepsactive");
                    $("#suitcolors").fadeIn("slow");
                    $("#suitcolor").addClass("stepsactive");
                    $("#next").html("Add to Cart");
                    loadSessionData();
                }
                else if (currPage == "suitcolors") {
                        /*api for check status of stock active or not*/
                        var fabricCode = csuitcolor;
                        if(fabricCode.indexOf(":") >0 ){
                            fabricCode = fabricCode.split(":")[0];
                        }
                        var url = "../api/registerUser.php";
                        $.post(url,{"type":"checkFabricStatus","fabricCode":fabricCode},function(fabricdata){
                            var status = fabricdata.Status;
                            var message = fabricdata.Message;
                            if(status == "Success"){
                                /*check for multiple select sleeve length and colors start here*/
                                switch(sleeveslit){
                                    case "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                                        sleeveslitthreadc0619 = sleeveslitthreadc0619.split("0619_")[1];
                                        sleeveslitthread = sleeveslitthread+","+sleeveslitthreadc0619;
                                        break;

                                    case "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit":
                                        sleeveslitthreadc0619 = sleeveslitthreadc0619.split("0619_")[1];
                                        sleeveslitthread = sleeveslitthreadc0619+","+sleeveslitthread;
                                        break;
                                }
                                /*check for multiple select sleeve length and colors end   here*/

                                var user_id = $("#user_id").val();
                                var randomnum = Math.floor((Math.random() * 600000) + 1);
                                var product_name = "CustomSuit" + randomnum;
                                var url = "../admin/webserver/addto_cart.php?product_type=Custom Suit&category="+csuitcategory+
                                "&product_name="+product_name+"&frontbutton="+cfrontbutton+"&lapelstyle="+clapelstyle+
                                "&lapelbuttonholes="+lapelbuttonholes+"&lapelbuttonholesstyle="+lapelbuttonholesstyle+
                                "&lapelbuttonholesthread="+lapelbuttonholesthread+"&lapelingredient="+lapelingredient+
                                "&lapelsatin="+lapelsatin+"&chestdart="+chestdart+"&feltcollar="+cfeltcollar+"&innerflap="+innerflap+
                                "&facingstyle="+facingstyle+"&sleeveslit="+sleeveslit+"&sleeveslitthread="+sleeveslitthread+
                                "&breastpocket="+cbreastpocket+"&lowerpocket="+clowerpocket+"&coinpocket="+coinpocket+
                                "&backvent="+cbackvent+"&liningstyle="+cliningstyle+"&liningoption="+liningoption+"&lining="+lining+
                                "&shoulderstyle="+shoulderstyle+"&shoulderpadding="+shoulderpadding+"&buttonoption="+buttonoption+
                                "&buttonswatch="+buttonswatch+"&threadoption="+cthreadoption+"&elbowpatch="+elbowpatch+"&elbowpatchcolor="+
                                elbowpatchcolor+"&canvasoption="+canvasoption+"&suitcolor="+fabricCode+":"+fabricCode+"&shirt="+shirt+"&tie="+tie+
                                "&jacket="+jacket+"&armbutton="+armbutton+"&sidearmbutton="+sidearmbutton+"&sidearmbuttoncut="+
                                sidearmbuttoncut+"&sidearmbuttonpoint="+sidearmbuttonpoint+"&user_id="+user_id+"&quantity=1" +
                                "&imgcategory="+suitcategory+"&imgfrontbutton="+frontbutton+"&imglapelstyle="+lapelstyle+
                                "&imgfeltcollar="+feltcollar+"&imgbreastpocket="+breastpocket+"&imglowerpocket="+lowerpocket
                                +"&imgbackvent="+backvent+"&imgliningstyle="+liningstyle+"&imgthreadoption="+threadoption+
                                "&imgsuitcolor="+fabricCode + "&monogramtext=" + monogramtext +"&monogramfont=" +
                                monogramfont +"&monogramcolor=" + monogramcolor +"&monogramposition=" + monogramposition +
                                "&monogramoption=" +monogramoption+"&monogramcode=" +monogramcode;
                                $.get(url, function (data) {
                                    var json = $.parseJSON(data);
                                    var status = json.status;
                                    if (status == "done") {
                                        window.location = "order_cart.php?userId="+user_id;
                                    }
                                });
                            }
                            else{
                                showMessages(message,"red");
                                $("#suitcolors").fadeIn("slow");
                                $(".colorsuggestion ").removeClass("active");
                                $("#suitcolor_dbk053a ").addClass("active");
                                $("#suitcolor_dbk053a ").trigger("click");
                                suitcolor = "dbK053a";
                            }
                        });

                }
            }
            else if (switcher == "previous") {
                if (currPage == "suitcategorys") {
                    window.location = "customize.php";
                }
                else if (currPage == "frontbuttons") {
                    $(".steps").removeClass("stepsactive");
                    $("#suitcategorys").fadeIn("slow");
                    $("#suitcategory").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lapelstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#frontbuttons").fadeIn("slow");
                    $("#frontbutton").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lapelingredients") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelstyles").fadeIn("slow");
                    $("#lapelstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "chestdarts") {
                    $(".steps").removeClass("stepsactive");
                    $("#lapelingredients").fadeIn("slow");
                    $("#lapelingredient").addClass("stepsactive");
                    loadSessionData();
                }

                else if (currPage == "feltcollars") {
                    $(".steps").removeClass("stepsactive");
                    $("#chestdarts").fadeIn("slow");
                    $("#chestdart").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "innerflaps") {
                    $(".steps").removeClass("stepsactive");
                    $("#feltcollars").fadeIn("slow");
                    $("#feltcollar").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "facingstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#innerflaps").fadeIn("slow");
                    $("#innerflap").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "sleeveslits") {
                    $(".steps").removeClass("stepsactive");
                    $("#facingstyles").fadeIn("slow");
                    $("#facingstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "breastpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#sleeveslits").fadeIn("slow");
                    $("#sleeveslit").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "lowerpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#breastpockets").fadeIn("slow");
                    $("#breastpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "coinpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#lowerpockets").fadeIn("slow");
                    $("#lowerpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "backvents") {
                    $(".steps").removeClass("stepsactive");
                    $("#coinpockets").fadeIn("slow");
                    $("#coinpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "liningstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#backvents").fadeIn("slow");
                    $("#backvent").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "shoulderstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#liningstyles").fadeIn("slow");
                    $("#liningstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "buttonoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#shoulderstyles").fadeIn("slow");
                    $("#shoulderstyle").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "buttonswatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonoptions").fadeIn("slow");
                    $("#buttonoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "threadoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#buttonswatchs").fadeIn("slow");
                    $("#buttonswatch").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "elbowpatchs") {
                    $(".steps").removeClass("stepsactive");
                    $("#threadoptions").fadeIn("slow");
                    $("#threadoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "canvasoptions") {
                    $(".steps").removeClass("stepsactive");
                    $("#elbowpatchs").fadeIn("slow");
                    $("#elbowpatch").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "monograms") {
                    $(".steps").removeClass("stepsactive");
                    $("#canvasoptions").fadeIn("slow");
                    $("#canvasoption").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "suitcolors") {
                    $(".steps").removeClass("stepsactive");
                    $("#monograms").fadeIn("slow");
                    $("#monogram").addClass("stepsactive");
                    $("#next").html("Next");
                    loadSessionData();
                }
            }

            storeData();
        }

        function showMessages(message, color) {
            $(".modal-header").css({"display": "none"});
            $(".modal-body").css({"display": "block"});
            $(".modal-body").css({"padding-top": "0px"});
            $(".modal-body").html("<div class='row' style='background:#000;'><h4 style='color:#fff;text-align: center" +
                ";padding:10px;font-size: 18px;' >" +
                "Fabric out of stock !!!</h4></div><div class='row'><h4 style='text-align: center;font-size: 18px;margin: 14px 0;'>"
                +message+"</h4></div>" +
                "<div class='row' style='text-align: center'><input type='button' data-dismiss='modal' " +
                "style='padding:6px;margin:6px 0;text-align: center;font-size: 16px;width: 200px;text-transform: uppercase;" +
                "background: #D9534F;border: none;color:#fff;" +
                "' value='ok'" +
                " /></div> ");
            $(".modal-footer").css({"display": "none"});
            $("#modal").modal("show");
        }

        function storeData() {
            var value = $(".stepsactive").attr("id");
            var name = $("#"+value).attr("name");
            var type = value.split("_");
            var selection = type[1];

            switch(type[0]) {
                case "suitcategory":
                    suitcategory = selection;
                    csuitcategory = name;
                    break;
                case"frontbutton":
                    cfrontbutton = name;
                    frontbutton = selection;
                    break;
                case"lapelstyle":
                    clapelstyle = name;
                    lapelstyle = selection;
                    break;
                case"lapelbuttonholes":
                    lapelbuttonholes = name;
                    break;
                case"lapelbuttonholesstyle":
                    lapelbuttonholesstyle = name;
                    break;
                case"lapelbuttonholesthread":
                    lapelbuttonholesthread = name;
                    break;
                case"lapelingredient":
                    lapelingredient = name;
                    break;
                case"lapelsatin":
                    lapelsatin = name;
                    break;
                case"chestdart":
                    chestdart = name;
                    break;
                case"feltcollar":
                    feltcollar = selection;
                    cfeltcollar = name;
                    break;
                case"innerflap":
                    innerflap = name;
                    break;
                case"facingstyle":
                    facingstyle = name;
                    break;
                case"sleeveslit":
                    sleeveslit = name;
                    switch(sleeveslit){
                        case "C-0612:real_slit":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            sleeveslit= "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed";
                            break;
                        case "C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            sleeveslit= "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit";
                            break;
                    }
                    break;
                case "sleeveslitthreadc0619":
                    sleeveslitthreadc0619 = name;
                    $( "div[name^='"+sleeveslitthread+"']" ).addClass("active");
                    switch(sleeveslit){
                        case "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                        case "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                    }
                    break;

                case"sleeveslitthread":
                    sleeveslitthread = name;
                    switch(sleeveslit){
                        case "C-0612:real_slit,C-0619:last_sleeve_button_hole_thread_customer_oppointed":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                        case "C-0619:last_sleeve_button_hole_thread_customer_oppointed,C-0612:real_slit":
                            $("div[name^='C-0619:last_sleeve_button_hole_thread_customer_oppointed']" ).addClass("active");
                            $("div[name^='C-0612:real_slit']" ).addClass("active");
                            break;
                    }
                    break;
                case"breastpocket":
                    cbreastpocket = name;
                    breastpocket = selection;
                    break;
                case"lowerpocket":
                    clowerpocket = name;
                    lowerpocket = selection;
                    break;
                case"coinpocket":
                    coinpocket = name;
                    break;
                case"backvent":
                    cbackvent = name;
                    backvent = selection;
                    break;
                case"liningstyle":
                    liningstyle = selection;
                    cliningstyle = name;
                    break;
                case"liningoption":
                    liningoption = name;
                    break;
                case"lining":
                    lining = name;
                    break;
                case"shoulderstyle":
                    shoulderstyle = name;
                    break;
                case"shoulderpadding":
                    shoulderpadding = name;
                    break;
                case"buttonoption":
                    buttonoption = name;
                    break;
                case"buttonswatch":
                    buttonswatch = name;
                    break;
                case "threadoption":
                    threadoption = selection;
                    cthreadoption = name;
                    break;
                case"elbowpatch":
                    elbowpatch = name;
                    break;
                case"elbowpatchcolor":
                    elbowpatchcolor = name;
                    break;
                case"canvasoption":
                    canvasoption = name;
                    break;
                case "monogramcolor":
                    monogramcolor = name;
                    break;
                case "monogramposition":
                    monogramposition = name;
                    break;
                case "monogramoption":
                    monogramoption = name;
                    break;
                case"suitcolor":
                    suitcolor = selection;
                    csuitcolor = name;
                    break;
            }
            savetoSession('tab',type[0]);
        }

        function rotateImage(button) {
            $(".loader").fadeIn("slow");
            var source=[];
            source.push($("#frontbuttonimg").attr("src"));
            source.push($("#frontbuttonpointimg").attr("src"));
            source.push($("#lapelstyleimg").attr("src"));
            source.push($("#breastpocketimg").attr("src"));
            source.push($("#lowerpocketimg").attr("src"));
            source.push($("#frontbuttoncutimg").attr("src"));
            source.push($("#shirtimg").attr("src"));
            source.push($("#tieimg").attr("src"));
            source.push($("#jacketimg").attr("src"));
            source.push($("#armbuttonimg").attr("src"));
            for(var i = 0;i<source.length;i++) {
                if (source[i].indexOf("front/") >=0){
                    if (button == "left") {
                        $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                    }
                    else if (button == "right") {
                        $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + backvent + ".png");
                        $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + sidearmbutton + ".png");
                        $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttoncut + ".png");
                        $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttonpoint + ".png");
                    }
                }
                else if (source[i].indexOf("side/") >= 0) {
                    if (button == "left") {
                        $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + shirt + ".png");
                        $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                    }
                    else if (button == "right") {
                        $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + backvent + ".png");
                        $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/back/" + armbutton + ".png");
                        $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                    }
                }
                else if (source[i].indexOf("back/") >= 0) {
                    if (button == "left") {
                        $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + backvent + ".png");
                        $("#shirtimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#tieimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#breastpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lowerpocketimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#lapelstyleimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + sidearmbutton + ".png");
                        $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttoncut + ".png");
                        $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/side/" + sidearmbuttonpoint + ".png");
                    }
                    else if (button == "right") {
                        $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+jacket + ".png");
                        $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + shirt + ".png");
                        $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + tie + ".png");
                        $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + breastpocket + ".png");
                        $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + lowerpocket + ".png");
                        $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton+lapelstyle+ ".png");
                        $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + ".png");
                        $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "point.png");
                        $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/front/" + frontbutton + "cut.png");
                        $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                        $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                    }
                }
            }
            $(".loader").fadeOut("slow");
        }

        $(".steps").click(function() {
            savetoSession('tab',this.id);
            $("#"+currPage).hide(1000);
            var newcurrpage = this.id+"s";
            $(".steps").removeClass("stepsactive");
            $("#"+this.id).addClass("stepsactive");
            $("#"+newcurrpage).show(1000);
            currPage = newcurrpage;
//            getSession();
            loadSessionData();
            if(currPage == "suitcolors"){
                $("#next").html("Add to Cart");
            }
            else{
                $("#next").html("Next");
            }
        });

        function savetoSession(type,content_id) {
            var url = "../admin/webserver/selectionData.php?sessionType=storeSession&type=custom_suits&content_id="+content_id;
            $.get(url,function(data){
              console.log('response --- '+data);
            });
        }

        function getSession() {
            var url = "../admin/webserver/selectionData.php?type=custom_suit&sessionType=getSession";
            currPage = "suitcategory";
            $.get(url,function(data) {
                data = $.parseJSON(data);
                if(data.status ==='done') {
                    currPage = data.content;
                    suitcategory = data.suitcategory;
                    csuitcategory = data.csuitcategory;//for cart
                    frontbutton = data.frontbutton;
                    cfrontbutton = data.cfrontbutton;// for cart
                    lapelstyle = data.lapelstyle;
                    clapelstyle = data.clapelstyle;// for cart
                    lapelbuttonholes = data.lapelbuttonholes;
                    lapelbuttonholesstyle = data.lapelbuttonholesstyle;//
                    lapelbuttonholesthread = data.lapelbuttonholesthread;
                    lapelingredient = data.lapelingredient;
                    lapelsatin = data.lapelsatin;
                    chestdart = data.chestdart;
                    feltcollar = data.feltcollar;
                    cfeltcollar = data.cfeltcollar; //for cart
                    innerflap = data.innerflap;
                    facingstyle = data.facingstyle;
                    sleeveslit = data.sleeveslit;
                    sleeveslitthreadc0619 = data.sleeveslitthreadc0619;

                    sleeveslitthread = data.sleeveslitthread;
                    breastpocket = data.breastpocket;
                    cbreastpocket = data.cbreastpocket;// for cart
                    lowerpocket = data.lowerpocket;
                    clowerpocket = data.clowerpocket;// for cart
                    coinpocket = data.coinpocket;
                    backvent = data.backvent;
                    cbackvent = data.cbackvent;// for cart
                    liningstyle = data.liningstyle;
                    cliningstyle = data.cliningstyle;// for cart
                    liningoption = data.liningoption;
                    lining = data.lining;
                    shoulderstyle = data.shoulderstyle;
                    shoulderpadding = data.shoulderpadding;
                    buttonoption = data.buttonoption;
                    buttonswatch = data.buttonswatch;
                    threadoption = data.threadoption;
                    cthreadoption = data.cthreadoption;//for cart
                    elbowpatch = data.elbowpatch;
                    elbowpatchcolor = data.elbowpatchcolor;
                    canvasoption = data.canvasoption;
                    suitcolor = data.suitcolor;
                    csuitcolor = data.csuitcolor;//for cart
                    shirt = data.shirt;
                    tie = data.tie;
                    jacket = data.jacket;
                    folder = data.folder;
                    armbutton = data.armbutton;
                    sidearmbutton = data.sidearmbutton;
                    sidearmbuttoncut = data.sidearmbuttoncut;
                    sidearmbuttonpoint = data.sidearmbuttonpoint;
                    monogramtext = data.monogramtext;
                    monogramfont = data.monogramfont;
                    monogramcode = data.monogramcode;
                    monogramcolor = data.monogramcolor;
                    monogramposition = data.monogramposition;
                    monogramoption = data.monogramoption;
                }

                $(".steps").removeClass("stepsactive");
                $(".stepdiv").fadeOut("slow");
                $("#"+currPage+"s").fadeIn("slow");
                $("#"+currPage).addClass("stepsactive");
                loadSessionData();
            });
        }
        loadSessionData();
        var not_found = 0;
        $(".outputsugg").error(function(){
            switch(this.id) {
                case "jacketimg":
                    $("#jacketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/onebuttonjacket.png");
                    break;
                case "shirtimg":
                    $("#shirtimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/shirt.png");
                    break;
                case "tieimg":
                    $("#tieimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/" + tie + ".png");
                    break;
                case "breastpocketimg":
                    $("#breastpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/topnormalbreatpocket.png");
                    break;
                case "lowerpocketimg":
                    $("#lowerpocketimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/normalpockets.png");
                    break;
                case "frontbuttonimg":
                    $("#frontbuttonimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/onebutton.png");
                    break;
                case "frontbuttonpointimg":
                    $("#frontbuttonpointimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/onebuttonpoint.png");
                    break;
                case "frontbuttoncutimg":
                    $("#frontbuttoncutimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/onebuttoncut.png");
                    break;
                case "armbuttonimg":
                    $("#armbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                    break;
                case "sidearmbuttoncutimg":
                    $("#sidearmbuttoncutimg").attr("src", "../images/jackets/sugg/bawli.png");
                    break;
                case "sidearmbuttonpointimg":
                    $("#sidearmbuttonpointimg").attr("src", "../images/jackets/sugg/bawli.png");
                    break;
                case "sidearmbuttonimg":
                    $("#sidearmbuttonimg").attr("src", "../images/jackets/sugg/bawli.png");
                    break;
                case "lapelstyleimg":
                    $("#lapelstyleimg").attr("src", "../images/jackets/sugg/" + suitcolor + "/" + folder + "/onebuttonnotch.png");
                    break;
            }
            not_found++;
        });
        $('#paddingpad').on('change',function(){
            var opt = this.value;
            if(opt =="03cm"){
                $(".padding-size").css({"height":"80px","width":"80px"});
                shoulderpadding = "C-060P:0.3cm";
            }
            if(opt =="06cm"){
                $(".padding-size").css({"height":"120px","width":"120px"});
                shoulderpadding = "C-060Q:0.6cm";
            }
            if(opt =="12cm"){
                $(".padding-size").css({"height":"150px","width":"150px"});
                shoulderpadding = "C-060R:1.2cm";
            }
            if(opt =="2cm") {
                $(".padding-size").css({"height":"200px","width":"200px"});
                shoulderpadding = "C-060S:2.0cm";
            }
        });
        $('#paddingpadr').on('change',function(){
            var opt = this.value;
            if(opt =="03cm"){
                $(".padding-sizer").css({"height":"80px","width":"80px"});
                shoulderpadding = "C-060P:0.3cm";
            }
            if(opt =="06cm"){
                $(".padding-sizer").css({"height":"120px","width":"120px"});
                shoulderpadding = "C-060Q:0.6cm";
            }
            if(opt =="12cm"){
                $(".padding-sizer").css({"height":"150px","width":"150px"});
                shoulderpadding = "C-060R:1.2cm";
            }
            if(opt =="12cm"){
                $(".padding-sizer").css({"height":"150px","width":"150px"});
                shoulderpadding = "C-060V:1.2cm";
            }
            if(opt =="2cm"){
                $(".padding-sizer").css({"height":"200px","width":"200px"});
                shoulderpadding = "C-060S:2.0cm";
            }
        });
        function addfabrics(typp){
            if(typp =="first"){
                var textfab = $("#textfab").val();
            }
            if(typp =="second"){
                var textfab = $("#textfabr").val();
            }
            var url="../admin/webserver/addfabric.php?type=allfabric&fabno="+textfab;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status != "done") {
                    alert(status);
                }
            });
        }
        var count = 0;
        function lapelsatinfabric(){
            $(".FabricBtnSatinLast").attr("disabled",true);
            var lapelsatinfabricfield = $("#lapelsatinfabricfield").val();
            if(lapelsatinfabricfield != ""){
                setSurplusPrice(encodeURI(lapelsatinfabricfield),'SatinLast');
                var newdiv = "<div class='primarysuggestion newsatinfabric' onclick=selectFabric('lapelsatin',this);setSurplusPrice('"+encodeURI(lapelsatinfabricfield)+"','SatinLast'); " +
                    "name='"+lapelsatinfabricfield+"' id='lapelsatin_"+lapelsatinfabricfield+"'><p class='suggestionname' " +
                    "style='line-height:80px'>"+lapelsatinfabricfield+"</p></div>";
                $("#satinError").remove();
                $("#satinfabricdiv").append(newdiv);
            }else{
                if(count == 0) {
                    count++;
                    $("#satinfabricdiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;margin:20px 0 -20px 0' id='satinError'>" +
                        "Please Enter Fabric Name First</p>");
                }
            }
        }
        var count2 = 0;
        function mainfabric(){
            $(".FabricBtnJacketColor").attr("disabled",true);
            var mainfabricfield = $("#mainfabricfield").val();
            if(mainfabricfield != ""){
                setSurplusPrice(encodeURI(mainfabricfield),'JacketColor');
                var newdiv = "<div class='primarysuggestion colorsuggestion' onclick=selectFabric('main',this);setSurplusPrice('"+encodeURI(mainfabricfield)+"','JacketColor'); " +
                    "name='"+mainfabricfield+"' id='suitcolor_"+mainfabricfield+"'><p class='suggestionname' " +
                    "style='line-height:80px'>"+mainfabricfield+"</p></div>";
                $("#mainError").remove();
                $("#mainfabricdiv").append(newdiv);
            }else{
                if(count2 == 0) {
                    count2++;
                    $("#mainfabricdiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;margin:20px 0 -20px 0' id='mainError'>" +
                        "Please Enter Fabric Name First</p>");
                }
            }
        }
        var count3=0;
        function addliningoption(){
            $(".FabricBtnLiningOption1").attr("disabled",true);
            var liningoptionfield = $("#liningoptionfield").val();
            if(liningoptionfield != ""){
                setSurplusPrice(encodeURI(liningoptionfield),'LiningOption1');
                var newdiv = "<div class='primarysuggestion liningoptions' onclick=selectFabric('liningoption',this);setSurplusPrice('"+encodeURI(liningoptionfield)+"','LiningOption1'); " +
                    "name='"+liningoptionfield+"' id='liningoption_"+liningoptionfield+"'><p class='suggestionname' " +
                    "style='line-height:80px'>"+liningoptionfield+"</p></div>";
                $("#liningoptionError").remove();
                $("#liningoptiondiv").append(newdiv);
            }else{
                if(count3 == 0) {
                    count3++;
                    $("#liningoptiondiv").append("<div style='clear:both'></div><p style='text-align:center;color:red;" +
                    "margin:20px 0 -20px 0' id='liningoptionError'>Please Enter Fabric Name First</p>");
                }
            }
        }
        function selectFabric(type,obj){
            $(".primarysuggestion").removeClass("active");
            $("#"+obj.id).addClass("active");
            if(type == "lapelsatin"){
                lapelsatin = $("#"+obj.id).attr('name')+":"+$("#"+obj.id).attr('name');
            }
            else if(type == "main"){
                suitcolor = $("#"+obj.id).attr('name')+":"+$("#"+obj.id).attr('name');
            }
            else if(type == "liningoption"){
                liningoption = $("#"+obj.id).attr('name')+":"+$("#"+obj.id).attr('name');
            }
        }
        function searchFabricData(ref,fabricBtn) {
            var fabricBtn = fabricBtn;
            var value = $('#'+ref.id).val();
            if(ref.id === 'lapelsatinfabricfield')
                $("#findFabric").attr("disabled",true);
            else
                $("#findFabric1").attr("disabled",true);

            var url = "../admins/api/fabricProcess.php";
            $.post(url,{'dataType':'searchFabricData','cameo_code':value},function(response){
                var results = [];
                var status = response.Status;
                var message = response.Message;
                var li_ = '';
                if(status === 'Success' ) {
                    results = [];
                    var dataa = response.data;

                    for(var i=0;i<dataa.length;i++) {
                        li_ = li_ + '<li onclick=fabricSelected("' + ref.id + '","' + dataa[i].fabric_code + '")>' + dataa[i].fabric_code + '</li>';
                    }
                    if(ref.id === 'lapelsatinfabricfield') {
                        $('#list_data').removeClass('data-remove');
                        $('#list_data').html(li_);
                    }
                    else{
                        $('#list_data1').removeClass('data-remove');
                        $('#list_data1').html(li_);
                    }
                }
                else if(status === 'Failure') {
                    li_ = li_+'<li>No data found</li>';
                    if(ref.id === 'lapelsatinfabricfield') {
                        $('#list_data').removeClass('data-remove');
                        $('#list_data').html(li_);
                    }
                    else{
                        $('#list_data1').removeClass('data-remove');
                        $('#list_data1').html(li_);
                    }
                }
            });
        }

        function fabricSelected(id,value) {
            $('#'+id).val(value);

            if(id == 'lapelsatinfabricfield') {
                $("#findFabric").attr("disabled",false);
                $('#list_data').addClass('data-remove');
            }
            else{
                $("#findFabric1").attr("disabled",false);
                $('#list_data1').addClass('data-remove');
            }
        }

        function edValueKeyPress(test) {
            $("#lettertype").html(test);
            monogramtext = test;
        }
        $('.rccode').click(function(){
            var font = this.id;
            var code = this.value;
            $("#lettertype").attr("style","font-family:"+font+"!important;");
            monogramfont = font;
            monogramcode = code;

        });
        function getColorCode(colorCode) {
            $("#lettertype").css("color", "#" + colorCode);
            monogramcolor = colorCode;
        }

        window.onclick = function () {
            if(!$("#list_data").hasClass('data-remove')) {
                $("#list_data").addClass('data-remove');
            }
            if(!$("#list_data1").hasClass('data-remove')) {
                $("#list_data1").addClass('data-remove');
            }
        };
        getCart(user_id);
    </script>


