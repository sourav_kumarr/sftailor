<?php
require_once('Classes/PRICE_CAT.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$priceClass = new \Classes\PRICE_CAT();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $priceClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
if($type === 'getCategories') {
    $response = $priceClass->getCategories();
    $priceClass->apiResponse($response);
}
else if($type === 'getCategoriesPart') {
    $requiredfields = array('priceId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $priceClass->apiResponse($response);
        return false;
    }
    $priceId = $_REQUEST['priceId'];
    $logoResponse = $priceClass->getCategoriesPart($priceId);
    $priceClass->apiResponse($logoResponse);
}
else if($type === 'editCategory') {
    $requiredfields = array('priceId','new_title','oldcategoryImagechanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $priceClass->apiResponse($response);
        return false;
    }
    $priceId = $_REQUEST['priceId'];
    $new_title = $_REQUEST['new_title'];
    $oldcategoryImagechanged = $_REQUEST['oldcategoryImagechanged'];

    $oldcategoryImage = $_REQUEST['oldcategoryImage'];
    $logoResponse = $priceClass->editPriceCat($priceId,$new_title,$oldcategoryImagechanged);
    $priceClass->apiResponse($logoResponse);
}

?>