<!--/ w3l-1 -->
<?php
$userClass = new \Classes\ADMIN();
$getSocialLinks = $userClass->getAllSocialLinks();

?>
<div class="clear"></div>
<style type="text/css">
    .agileits-w3layouts-1 {
        display: none;
    }
    .newsletterh3{
        color:#d4504f;
        font-size:36px;
        text-align:center;
        text-transform:uppercase;
        margin-bottom:20px;
    }
    .bodyText {
        padding-top:30px;
    }
    .nbs-flexisel-item > img{
        height: 180px;
    }
</style>
<div class="gallery agileinfo" id="partner">
    <h2 class="tittle">Awards / <span style="color:#000;">Member</span></h2>
    <div class="container awardSlider" >

    </div>
</div>
<form method="post" class="form-inline" accept-charset="UTF-8" action="https://www.aweber.com/scripts/addlead.pl"
      style="margin-bottom:-10px;padding-bottom:20px;background:#eee">
    <div style="display: none;">
        <input type="hidden" name="meta_web_form_id" value="1418525566"/>
        <input type="hidden" name="meta_split_id" value=""/>
        <input type="hidden" name="listname" value="awlist4794823"/>
        <input type="hidden" name="redirect" value="https://www.aweber.com/thankyou-coi.htm?m=text"
               id="redirect_e415e0084725aac39f1cc3917d4a8fb8"/>

        <input type="hidden" name="meta_adtracking" value="SFT_website"/>
        <input type="hidden" name="meta_message" value="1"/>
        <input type="hidden" name="meta_required" value="name (awf_first),name (awf_last),email"/>

        <input type="hidden" name="meta_tooltip" value=""/>
    </div>
    <div id="af-form-1418525566" class="af-form">
        <div id="af-header-1418525566" class="af-header">
            <div class="bodyText">
                <h3 class="newsletterh3">Newsletter</h3>
                <p style="text-align: center;">
                    <span style="font-size: 20px;color:#555">Sign up for our newsletter to get style Advice and
                    Exclusive Offers!</span>
                </p>
            </div>
        </div>
        <div style="clear:both"></div>
        <div id="af-body-1418525566" class="row" style="margin:30px 0">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <div class="form-group col-md-3">
                    <label for="awf_field-92859248-first">First Name:</label><br>
                    <input id="awf_field-92859248-first" type="text" class="form-control" name="name (awf_first)" value=""
                           onfocus=" if (this.value == '') { this.value = ''; }"
                           onblur="if (this.value == '') { this.value='';} " tabindex="500" style="width:100%"/>
                </div>
                <div class="form-group col-md-3">
                    <label for="awf_field-92859248-last">Last Name:</label><br>
                    <input id="awf_field-92859248-last" class="form-control" type="text" name="name (awf_last)" value=""
                           onfocus=" if (this.value == '') { this.value = ''; }"
                           onblur="if (this.value == '') { this.value='';} " tabindex="501" style="width:100%"/>
                </div>
                <div class="form-group col-md-3">
                    <label for="awf_field-92859249">Email: </label><br>
                    <input class="form-control" id="awf_field-92859249" type="text" name="email" value=""
                           tabindex="502" onfocus=" if (this.value == '') { this.value = ''; }" onblur="if(this.value == '')
                    { this.value='';} " style="width:100%"/>
                </div>
                <div class="form-group col-md-3">
                    <br>
                    <input name="submit" style="width:100%;text-transform:capitalize" id="af-submit-image-1418525566" type="submit" class="btn btn-danger" tabindex="503"/>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
        <div class="af-element privacyPolicy" style="text-align: center"><p>We respect your <a
                        title="Privacy Policy" href="https://www.aweber.com/permission.htm" target="_blank"
                        rel="nofollow">email privacy</a></p>
            <div class="af-clear"></div>
        </div>
       
    </div>
    <div style="display: none;"><img src="https://forms.aweber.com/form/displays.htm?id=jCyMHKxMrKxsbA==" alt=""/></div>
</form>
<script type="text/javascript" src="js/jquery.flexisel.js"></script>
<script type="text/javascript">
    // Special handling for facebook iOS since it cannot open new windows
    (function () {
        if (navigator.userAgent.indexOf('FBIOS') !== -1 || navigator.userAgent.indexOf('Twitter for iPhone') !== -1) {
            document.getElementById('af-form-1418525566').parentElement.removeAttribute('target');
        }
    })();
</script>
<script type="text/javascript">
    <!--
    (function () {
        var IE = /*@cc_on!@*/false;
        if (!IE) {
            return;
        }
        if (document.compatMode && document.compatMode == 'BackCompat') {
            if (document.getElementById("af-form-1418525566")) {
                document.getElementById("af-form-1418525566").className = 'af-form af-quirksMode';
            }
            if (document.getElementById("af-body-1418525566")) {
                document.getElementById("af-body-1418525566").className = "af-body inline af-quirksMode";
            }
            if (document.getElementById("af-header-1418525566")) {
                document.getElementById("af-header-1418525566").className = "af-header af-quirksMode";
            }
            if (document.getElementById("af-footer-1418525566")) {
                document.getElementById("af-footer-1418525566").className = "af-footer af-quirksMode";
            }
        }
    })();
    -->
    function getBrandData(){
        var url = "admins/api/brandLogoProcess.php";
        $.post(url,{"type":"getLogo"},function(data){
            var status = data.Status;
            var logoData = data.data;
            if(status == "Success"){
                var logoBox = "";
                for(var a= 0; a < logoData.length; a++){
                    logoBox = logoBox+'<li><img src="admins/api/Files/images/'+logoData[a].logo_file+'" /></li>';
                }
                logoBox = '<ul id="brandLogo">'+logoBox+'</ul>';
                $(".awardSlider").html(logoBox);
                    $("#brandLogo").flexisel({
                        visibleItems: 5,
                        animationSpeed: 1000,
                        autoPlay: true,
                        autoPlaySpeed: 3000,
                        pauseOnHover: true,
                        enableResponsiveBreakpoints: true,
                        responsiveBreakpoints: {
                            portrait: {
                                changePoint:480,
                                visibleItems: 1
                            },
                            landscape: {
                                changePoint:640,
                                visibleItems: 2
                            },
                            tablet: {
                                changePoint:768,
                                visibleItems: 3
                            }
                        }
                    });
            }
            else{
                showMessage(data.Message, "red");
            }
        }).fail(function () {
            showMessage("Unable to process the request","red");
        });
    }
    getBrandData();
</script>

<!-- footer -->
<div class="footer w3-agile-1">
	<div class="container">

        <ul class="fb_icons2 agile-1">
            <?php

            $allLink = $getSocialLinks['socialData'];
            for($k=0;$k<count($allLink);$k++){
            ?>
                <li>
                    <a target="_blank" class="<?php echo $allLink[$k]['social_class'];?>" href="<?php echo $allLink[$k]['social_link'];?>">
                    </a>
                </li>
          <?php } ?>
        </ul>
	</div>
	<p class="copyright">@ 2017 sftailor.com | All Rights Reserved. </a></p>
	<p class="copyright"><a href="faq.php">FAQ</a></p>
	<p class="copyright"><a href="terms.php">Sales Terms</a></p>
	<p class="copyright"><a href="privacyPolicy.php">Privacy Policy</a></p>
</div>
<!-- footer -->
<style>
#modal {top: 120px;}
</style>
<div id ="modal" class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<h4 class="modal-title" id="gridSystemModalLabel">Modal title</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-md-4">.col-md-4</div>
					<div class="col-md-4 col-md-offset-4">.col-md-4 .col-md-offset-4</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</body>
<!-- //Body -->
</html>