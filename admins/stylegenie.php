<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
$query="select * from style_genie order by genie_id DESC";
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Style Genie <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of User's Having Style Genie Used
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User Profile</th>
                                <th>Unique Id</th>
                                <th>User Name</th>
                                <th>User E-Mail</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $link = $conn->connect();//for scan2tailor
                            if ($link) {
                                $result = mysqli_query($link, $query);
                                if ($result){
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($genieData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <?php
                                                $userResp = $userClass->getParticularUserData($genieData['user_id']);
                                                $userName = $userResp['UserData']['name'];
                                                $userEmail = $userResp['UserData']['email'];
                                                ?>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <td data-title='User Profile'>
                                                    <img style="height:40px;width:40px" class="img-thumbnail"
                                                         src="<?php echo $userResp['UserData']['profile_pic'] ?>" />
                                                </td>
                                                <td data-title='Unique Id'><?php echo $genieData['user_id'] ?></td>
                                                <td data-title='User Name'><?php echo $userName ?></td>
                                                <td data-title='User E-Mail'><?php echo $userEmail ?></td>
                                                <td data-title='Action'>
                                                    <button class='btn btn-sm btn-info' onclick="viewSaved('<?php echo $genieData['user_id']?>')">View Saved Measurements</button>
<!--                                                    <button class='btn btn-sm btn-danger'>Delete Style Genie</button>-->
                                                    <!--//<a href="_=<?php /*echo $genieData['user_id'] */?>"></a>-->
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    function viewSaved(user_id){
        var url = "../api/registerUser.php";
        $.post(url,{"type":"getAllMeasurement","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                var measurements = data.measurements;
                var datashow = "<div class='row'><div class='container-fluid'><table class='table table-bordered'><tr>" +
                "<th>S.No</th><th>Measurement Name</th><th>Added On</th><th>Action</th>";
                for(var i=0;i<measurements.length;i++){
                    var date = new Date(measurements[i].added_on*1000);
                    var year    = date.getFullYear();
                    var month   = date.getMonth()+1;
                    var dated     = date.getDate();
                    var added_on = dated+"/"+month+"/"+year;
                    datashow += "<tr><td>"+(i+1)+"</td><td>"+measurements[i].meas_name+"</td><td>"+added_on+"</td>" +
                    "<td><input type='button' value='View Measurement' class='btn btn-danger btn-sm' " +
                    "onclick=finalSelection('"+measurements[i].measurement_id+"','"+user_id+"') />" +
                        "<i class='fa fa-2x fa-trash pull-right' style='cursor: pointer' onclick=confirmMeasure('"+encodeURI(measurements[i].meas_name)+"','"+measurements[i].measurement_id+ "')></i></td></tr>";
                }
                datashow+="</table></div></div>";
                $(".modal-title").html("<span style='color:green'>Select Measurement File</span>");
                $(".modal-body").html(datashow);
                $(".modal-footer").css("display","none");
                $("#myModal").modal("show");
            }
            else{
                showMessage("No measurements saved yet","red");
            }
        }).fail(function(){
            alert("Server Error !!! Please Try After Some Time");
        });
    }
    function confirmMeasure(measureName,measureId) {
        $("#myModal").modal("hide");
        setTimeout(function(){
            $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
            $(".modal-body").html("<p class='errorMess'></p><label style='font-family: monospace'>Are you sure want to delete "+decodeURI(measureName)+" measurement File ?</label>");
            $(".modal-footer").css("display","block");
            $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
                'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
                'onclick=deleteData_measure("'+measureId+'")>Delete</button>');
            $("#myModal").modal("show");
        },1000);

    }
    function deleteData_measure(id) {
        var url ='../api/registerUser.php';
        $.post(url,{"type":'deleteMeasurement','meas_id':id},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $(".errorMess").html("Measurement delete successfully");
                $(".errorMess").css("color","green");
                setTimeout(function(){
                    location.reload();
                },1000);
            }
            else{
                $("#myModal").modal("hide");
                showMessage(message,"green");
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    function finalSelection(measurement_id,user_id){
        window.location="sdet.php?_="+user_id+"&m="+measurement_id;
    }
</script>
