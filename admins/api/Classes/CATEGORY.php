<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');
class CATEGORY
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }
    public function addCategory($cat_type,$coupon_name,$coupon_condition,$coupon_discount,$freebies_item_with,$freebies_item,$coupon_for)
    {
        $link = $this->link->connect();
        if ($link) {
            if(strpos($coupon_for, 'All') !== false){
                $coupon_for = "All";
            }
            $query = "insert into categories (cat_name,cat_price,added_on,cat_type,coupon_infinity,coupon_for,coupon_condition,freebies_item_with,freebies_item,cat_status)
            VALUES ('$coupon_name','$coupon_discount','$this->currentDateTime','$cat_type','once','$coupon_for','$coupon_condition','$freebies_item_with','$freebies_item','')";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "New Coupon Added SuccessFully";
                $this->response['catId'] = $this->link->getLastId();
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function editCategory($cat_status,$cat_id,$cat_price,$cat_name)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "update categories set cat_status='$cat_status',cat_price='$cat_price',cat_name='$cat_name' where cat_id = '$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Category Updated SuccessFully";
                $this->response['catId'] = $cat_id;
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function checkCategoryExistence($cat_name)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from categories where cat_name = '$cat_name'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Category Name Already Existance Please Use Diffrent One";
                    $row = mysqli_fetch_array($result);
                    $this->response['catId'] = $row['cat_id'];
                } else {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Valid Name";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getParticularCatData($catId)
    {
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories where cat_id='$catId'";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {

                    $catData = mysqli_fetch_assoc($result);


                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Category Exist";
                    $this->response['catData'] = $catData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Category";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllCategories()
    {
        $catArray = array();
        $link = $this->link->connect();
        if($link) {
            $query="select * from categories order by cat_id DESC";
            $result = mysqli_query($link,$query);
            if($result)
            {
                $num = mysqli_num_rows($result);
                if($num>0) {
                    while($catData = mysqli_fetch_array($result)) {
                        $catArray[]=array(
                            "cat_id"=>$catData['cat_id'],
                            "cat_name"=>$catData['cat_name'],
                            "cat_image"=>$catData['cat_image'],
                            "added_on"=>$catData['added_on'],
                            "cat_status"=>$catData['cat_status']
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['data'] = $catArray;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Categories Found";
                }
            }
            else
            {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else
        {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function statusChange($cat_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE categories SET cat_status='$value' WHERE cat_id='$cat_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }


    public function updateInfinity($cat_id,$value){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE categories SET coupon_infinity='$value' WHERE cat_id='$cat_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function updateCoupon($cat_id,$coupon_condition,$coupon_discount,$coupon_for,$freebies_item_with,$freebies_item){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    if(strpos($coupon_for, 'All') !== false){
                        $value = "All";
                    }
                    $update = mysqli_query($link, "UPDATE categories SET coupon_condition = '$coupon_condition',
                    cat_price='$coupon_discount',coupon_for='$coupon_for',freebies_item_with='$freebies_item_with',freebies_item=
                    '$freebies_item' WHERE cat_id='$cat_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Status Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function deleteCategory($cat_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from categories where cat_id='$cat_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from categories WHERE cat_id='$cat_id'");
                    if ($update) {
                        $row = mysqli_fetch_array($result);
                        unlink("Files/images/".$row['cat_image']);
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Category Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function updateRange($cat_id, $start,$end)
    {


        $link = $this->link->connect();

        if ($link) {

            $update = mysqli_query($link, "UPDATE categories SET price_range_start='$start',price_range_end='$end' WHERE cat_id='$cat_id'");

            if ($update) {
                $count = mysqli_affected_rows($link);
                if($count>0) {
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Range has been updated successfully ! ! ! !";
                    return $this->response;
                }
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Nothing changed yet";

            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }


        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }

        return $this->response;
    }

    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}