<?php include("header.php");?>
		<div class="banner top-aboutbanner">
			<div class="">
			</div>
		</div>
		<!-- //Slider -->
	</div>
	<!-- //Header -->
<div class="about-bottom wthree-3">
		<div class="container">
		<h2 class="tittle">ABOUT US</h2>
			<div class="agileinfo_about_bottom_grids">
				<div class="col-md-7 agileinfo_about_bottom_grid" style="text-align:justify;">
					<h5> Bespoke </h5>
					<p>adjective: (of goods, especially clothing) made to order. "a bespoke suit".</p></br>

						<p>Imagine going to a store to buy a suit. You have your heart set on a single-breasted jacket with one button in a blue pinstripe. You browse the collection and see that in your size only a gray pinstripe model is available, with the stripes closer together than you’d like and with two buttons. But you really need a new suit and you don’t feel like going to 5 different stores, so you settle. Sounds familiar.</p></br>
						
						<p>With SF Tailors, that's a thing of the past. We bring you bespoke suits at an affordable price. No more settling for what's hanging on the rack, you get to create the suit of your dreams, made specifically for you! When you buy your suit in a department store, you're basically settling for another mans' suit: stores use 8 to 10 sizes based on the measurements of a model to create garments that you choose from, hoping one of them will fit you.</p></br>
						
						<p>SF Tailors doesn't just alter an existing pre-made garment, we create your suit or shirt from scratch, tailored to your measurements. The pattern is made based on your body and every detail is tailored to your specifications, from the fabric to the width of the lapels, from the style and color of the buttons to even the buttonholes! <b>Every SF Tailors suit is unique!</b></p></br>
						
						<p><i>Why wear another man's suit when you can wear you own for almost the same price?!</i></p></br>
						<p><b>Welcome to the world of bespoke garments! </b></p></br></br>
						
						
					 </p>
				</div>
				<div class="col-md-5 agileinfo_about_bottom_grid">
					<img src="images/slider/threewaysuit.jpg" alt=" " class="img-responsive"></br>
					<h3 class="tittle" style="font-size:2em;">BE YOURSELF. EVERYONE ELSE IS ALREADY TAKEN. OSCAR WILDE</h3>
				</div>
			</div>
		</div>
	</div>

<!-- //team -->
<?php include("footer.php");?>