<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/SUPPLIER.php');
$conn = new \Classes\CONNECT();
$supplierClass = new \Classes\SUPPLIER();
$allSupplierData = $supplierClass->getSupplierDetails();
?>
<style>.shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .hideloader {
        display: none;
    }
    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
#create_supplier{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
#add_supplier_data{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
.e_Administrator{display:none;}
</style>
<!-- page content -->
<div id="loadd" class="loaderr hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Suppliers <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
					<button style="margin-top:5px;float: right;" onclick="creatSupplier()" class="btn btn-info btn-sm">Add Supplier</button>
				<!----------------add admin---------------->	
					<div class="shadoows" onclick="close_diolog()" style="display: none;"></div>
						<div class="col-md-12 suppliers" id="add_supplier_data" style="padding: 0;display: none;" >
                        <form method="Post" enctype="multipart/form-data" id="add_supplier">
							<div class="form-group col-md-4">
								<label>Company Name</label>
								<input type="text"  name="company_name" class="form-control" value="" placeholder="Enter Your Company Name" required="true"  />
							</div>
                            <input type="hidden" name="type" value="addSupplier">
                            <div class="form-group col-md-4">
                                <label>Company Address 1</label>
                                <input type="text" name="company_address1" class="form-control" value="" placeholder="Enter Your Company Address 1" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Company Address 2</label>
                                <input type="text" name="company_address2" class="form-control" value="" placeholder="Enter Your Company Address 2" required="true"/>
                            </div>
							<div class="form-group col-md-4">
                                <label>Work Phone</label>
                                <input type="text" name="company_work_phone" class="form-control" value="" placeholder="Enter Work Phone no" required="true"/>
                            </div>
							<div class="form-group col-md-4">
                                <label>Mobile Number</label>
                                <input type="text" name="company_mobile_no" class="form-control" value="" placeholder="Enter Mobile no" />
                            </div>
							<div class="form-group col-md-4">
                                <label>Fax</label>
                                <input type="text" name="company_fax" class="form-control" value="" placeholder="Enter Fax no" />
                            </div>
                           <div class="form-group col-md-4">
                                <label>First Name</label>
                                <input type="text" name="contact_first_name" class="form-control" value="" placeholder="Enter First name" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Family Name</label>
                                <input type="text" name="family_name" class="form-control" value="" placeholder="Enter Family name" />
                            </div>
							<div class="form-group col-md-4">
                                <label>	Contact Email</label>
                                <input type="text" name="contact_email" class="form-control" value="" placeholder="Enter Email" required="true"/>
                            </div>
							
                            <div style='clear:both' ></div>

                            <div class="form-group">
                                <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Submit</button>
                            </div>
                        </form>
                    </div>
					
			<!----------edit admin info------------------->		
				<?php if($_REQUEST["type"] =="edit"){  ?>
					<div class="shadoows" onclick="close_diolog()" style="display: block;"></div>
						<div class="col-md-12 create_supplier" id="create_supplier" style="padding: 0;display: block;" >
                        <?php
                        $link = $conn->connect();//for sftailor
                        if ($link) {
                            $supId = $_REQUEST['supId'];
                            $query = "select * from wp_supplier where sup_id='$supId'";
                            $result = mysqli_query($link, $query);
                            if ($result) {
                                $num = mysqli_num_rows($result);
                                if ($num > 0) {
                                    $supplierData = mysqli_fetch_assoc($result);
                                }
                            }
                        }
                        ?>
                        <form method="Post" enctype="multipart/form-data" id="edit_supplier">
						<h2>Update Supplier Info</h2></br>
							<div class="form-group col-md-4">
								<label>Company Name</label>
								<input type="text"  name="company_name" class="form-control" value="<?php echo $supplierData['company_name'];?>" placeholder="Enter Your Company Name" required="true"  />
							</div>
                            <input type="hidden" name="supplier_id" value="<?php echo $supplierData['sup_id'];?>">
                            <input type="hidden" name="type" value="updateSupplier">
                            <div class="form-group col-md-4">
                                <label>Company Address 1</label>
                                <input type="text" name="company_address1" class="form-control" value="<?php echo $supplierData['company_address1'];?>" placeholder="Enter Your Company Address 1" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Company Address 2</label>
                                <input type="text" name="company_address2" class="form-control" value="<?php echo $supplierData['company_address2'];?>" placeholder="Enter Your Company Address 2" />
                            </div>
							<div class="form-group col-md-4">
                                <label>Work Phone</label>
                                <input type="text" name="company_work_phone" class="form-control" value="<?php echo $supplierData['company_work_phone'];?>" placeholder="Enter Work Phone no" required="true"/>
                            </div>
							<div class="form-group col-md-4">
                                <label>Mobile Number</label>
                                <input type="text" name="company_mobile_no" class="form-control" value="<?php echo $supplierData['company_mobile_no'];?>" placeholder="Enter Mobile no" />
                            </div>
							<div class="form-group col-md-4">
                                <label>Fax</label>
                                <input type="text" name="company_fax" class="form-control" value="<?php echo $supplierData['company_fax'];?>" placeholder="Enter Fax no" />
                            </div>
                           <div class="form-group col-md-4">
                                <label>First Name</label>
                                <input type="text" name="contact_first_name" class="form-control" value="<?php echo $supplierData['contact_first_name'];?>" placeholder="Enter First name" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Family Name</label>
                                <input type="text" name="family_name" class="form-control" value="<?php echo $supplierData['family_name'];?>" placeholder="Enter Family name" />
                            </div>
							<div class="form-group col-md-4">
                                <label>	Contact Email</label>
                                <input type="text" name="contact_email" class="form-control" value="<?php echo $supplierData['contact_email'];?>" placeholder="Enter Email name" required="true"/>
                            </div>
							
                            <div style='clear:both' ></div>

                            <div class="form-group">
                                <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Submit</button>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
					
					
					
					<!----------Email admin info------------------->		
				<?php if($_REQUEST["type"] =="email"){  ?>
					<div class="shadoows" onclick="close_diolog()" style="display: block;"></div>
						<div class="col-md-12 create_supplier" id="create_supplier" style="padding: 0;display: block;" >
                        <?php
                        $link = $conn->connect();//for sftailor
                        if ($link) {
                            $supId = $_REQUEST['supId'];
                            $query = "select * from wp_supplier where sup_id='$supId'";
                            $result = mysqli_query($link, $query);
                            if ($result) {
                                $num = mysqli_num_rows($result);
                                if ($num > 0) {
                                    $supplierData = mysqli_fetch_assoc($result);
                                }
                            }
                        }
                        ?>
						<style>
							.flat-table {
								display: block!important;
								font-family: sans-serif!important;
								-webkit-font-smoothing: antialiased!important;
								font-size: 115%!important;
								overflow: auto!important;
								width: auto!important;
							}

							.flat-table	th {
								background-color: rgb(112, 196, 105)!important;
								color: white!important;
								font-weight: normal!important;
								padding: 20px 30px!important;
								text-align: center!important;
								}
							.flat-table	td {
								background-color: rgb(238, 238, 238);
								border: 1px solid;
								color: rgb(111, 111, 111);
								padding: 14px 25px;
								}
							

						</style>
                        <form method="Post" enctype="multipart/form-data" id="edit_supplier">
						<h2>Update Supplier Info</h2></br>
							<div class="form-group col-md-4">
								<label>Company Email</label>
								<div><?php echo $supplierData['contact_email'];?></div>
							</div>
                           <?php if($supId == 3){ ?>
							<label>Supplier Detail :</label>
							<div><table class="flat-table"><tr><td>Neck Tie(One)</td><td>Pocket Square</td><td>Bow Tie(One)</td><td>Tie Bar</td><td>Jacket</td></tr><tr><td>Outfits</td><td>Pants</td><td>Vest</td><td>Shirts</td><td>Overcoat</td></tr></table></div>
							<?php } if($supId == 2){ ?>
							<label>Supplier Detail :</label>
							<div><table class="flat-table"><tr><td>Neck Tie(Two)</td><td>Bow Tie(Two)</td></tr></table></div>
							<?php } ?>
                            <div style='clear:both' ></div>

                            <div class="form-group">
                                <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px" onclick="suppPage()"> Ok</button>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
					
					
					
					
				    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Available Users
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Company Name</th>
                                <th>Address</th>
                                <th>Work Phone</th>
                                <th>First Name</th>
                                <th>Family Name</th>
                                <th>Email</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
								$j=1;
								$supplierData = $allSupplierData['supplierData'];
								for($i=0;$i<count($supplierData);$i++){
                                ?>
								<tr>
									<td data-title='#'><?php echo $j ?></td>
									<td><a href='supplier.php?type=email&supId=<?php echo $supplierData[$i]['sup_id'];?>' style="border-bottom: 1px solid blue;"><?php echo $supplierData[$i]['company_name']; ?></a></td>
									<td><?php echo $supplierData[$i]['company_address1'].','.$supplierData[$i]['company_address2']; ?></td>
									<td><?php echo $supplierData[$i]['company_work_phone']; ?></td>
									<td><?php echo $supplierData[$i]['contact_first_name']; ?></td>
									<td><?php echo $supplierData[$i]['family_name']; ?></td>
									<td><?php echo $supplierData[$i]['contact_email']; ?></td>
									<td>
										<a href='supplier.php?type=edit&supId=<?php echo $supplierData[$i]['sup_id'];?>' style="float: right; font-size: 18px; padding: 2px 4px; background: tomato none repeat scroll 0% 0%; color: white;"><i class="fa fa-pencil"></i></a>
										<a href='#' onclick=confirmDelete_supplier('<?php echo $supplierData[$i]['sup_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>
									</td>
								</tr>
							<?php $j++ ;}  ?>				
                                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
$(document).ready(function () {
    $('#orderTable').DataTable({});
});
function creatSupplier(){
	$(".shadoows").show();
	$(".suppliers").show();
}
function confirmDelete_supplier(supplier_id) {
    $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
    $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
    $(".modal-footer").css("display","block");
    $(".modal-footer").css("margin-top","0px");
    $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
        'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
        'onclick=delSupplier("'+supplier_id+'")>Delete</button>');
    $("#myModal").modal("show");
}
function delSupplier(supplier_id){
    var url = "api/supplierProcess.php";
    $.post(url,{"type":"deleteSupplier","supplier_id":supplier_id}, function (data) {
        var Status = data.Status;
			var Message = data.Message;
			if (Status == "Success"){
				window.location="supplier.php";
			}
    });
}
$("#add_supplier").submit(function(e){
	e.preventDefault();
	$('.loader').fadeIn("slow");
	$.ajax({
		url: "api/supplierProcess.php",
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data)
		{
			var Status = data.Status;
			var Message = data.Message;
			if (Status == "Success"){
				alert(Message);
				window.location="supplier.php";
			}
		}
	});
});

$("#edit_supplier").submit(function(e){
	e.preventDefault();
	$('.loader').fadeIn("slow");
	$.ajax({
		url: "api/supplierProcess.php",
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data)
		{
			var Status = data.Status;
			var Message = data.Message;
			if (Status == "Success"){
				alert(Message);
				window.location="supplier.php";
			}
		}
	});
});

function close_diolog() {
	window.location = "supplier.php"
}
function suppPage() {
	window.location = "supplier.php"
}


	
</script>
