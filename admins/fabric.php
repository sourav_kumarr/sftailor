<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
if(isset($_POST['filterButton'])){
    $startDate = strtotime($_POST['startDate']);
    $endDate = strtotime($_POST['endDate']);
    $query="select * from orders where order_dated>='$startDate' and order_dated<='$endDate' order by order_id DESC";
}else{
    $query="select * from orders order by order_id DESC";
}

?>
<style>
    .nav-tabs > li.active > a, .nav-tabs > li.active > a:focus, .nav-tabs > li.active > a:hover{
        background: #eee;
    }
    #orderTable_filter{
        display:none;
    }
    .search-container{
        display: flex;
        flex-direction: row;
    }
    .search-remove{
        display: none;
    }
    .no-gutter{
        margin: 0px;
        padding: 0px;
    }
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
    #err_message{
        font-weight: bold;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .sty{float: right;color: white;background: green;padding: 5px;border-radius: 2px;}
</style>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count"></div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Fabrics <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox">
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <label id="message" style="color: red"></label>
                                        <input type="file" class="form-control" name="fabricFile" id="fabricFile" />
                                        <input type="button" Value="+ Add Fabric" class="btn btn-primary btn-sm" name="filterButton"  onclick="addfabricdata()" style="margin-bottom: -1px"/>
                                        <a href="api/Files/excel/File211395.xls"><input type="button" Value="Download Format" class="btn btn-primary btn-sm"  style="margin-bottom: -1px"/></a>
                                        <input type="button" onclick="viewPrice()" Value="View Price" class="btn btn-danger btn-sm"  style="margin-bottom: -1px"/>
                                        <input type="button" onclick="uploadStock()" Value="Upload Stock" class="btn btn-danger btn-sm"  style="margin-bottom: -1px"/>

                                    </div>


                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <style>
                        .layui-layer-shade {
                            height: 100%;
                            left: 0;
                            top: 0;
                            width: 100%;
                        }
                        .layui-layer, .layui-layer-shade {
                            pointer-events: auto;
                            position: fixed;
                        }
                    </style>
                    <div id="layui-layer-shade5" onclick="close_diolog()" class="layui-layer-shade" times="5"
                         style="z-index: 19891018; background-color: rgb(0, 0, 0); opacity: 0.7;display: none;" onclick="close_diolog()"></div>
                    <div class="x_content" id="view-price" style="display: none;z-index: 29891018;">
                        <div class="tab-content" style="background-color: rgb(238, 238, 238); padding: 28px 38px; border-radius: 10px; margin-top: -70px;">
                            <div class="tab-pane active" id="tab1">
                                <label style="color: red" id="fabric_error">Price</label>

                                <table id="priceTable" class="table table-striped table-bordered">
                                        <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Price Amount</th>
                                            <th>Price Per Meter</th>
                                            <th>Product Type</th>
                                            <th>Created At</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php

                                        $link = $conn->connect();//for scan2tailor
                                        //$link2 = $conn->connect2();//for tailormade

                                        if ($link) {
                                            $query = "select * from wp_prices order by price_id DESC";
                                            $result = mysqli_query($link, $query);
                                            if ($result) {

                                                $num = mysqli_num_rows($result);
                                                if ($num > 0) {
                                                    $j = 0;
                                                    while ($priceData = mysqli_fetch_array($result)) {
                                                        $j++;
                                                        ?>
                                                        <tr>
                                                            <td data-title='#'><?php echo $j ?></td>
                                                            <td><input type="text" id="price_amount_<?php echo $priceData['price_id'];?>" value="<?php echo $priceData['price_amount']; ?>" style="border:none;"></td>
                                                            <td><input type="text" id="price_amount_meter_<?php echo $priceData['price_id'];?>" value="<?php echo $priceData['price_per_meter']; ?>" style="border:none;"></td>
                                                            <td><?php echo $priceData['price_type']; ?></td>
                                                            <td><?php echo $priceData['price_created_at']; ?></td>
                                                            <td><a href='#' onclick=editPrice('<?php echo $priceData['price_id'];?>') style="font-size: 18px; padding: 2px 4px; background: green none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-floppy-o"></i> </a> </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="x_content">

                        <ul class="nav nav-tabs">
                            <li class="alltab active" id="sw120s" onclick="onTabClicked('sw120s')">
                                <a href="#tab1" data-toggle="tab">Summer & Winter Formal 120S</a>
                            </li>
                            <li class="alltab" id="sw130s" onclick="onTabClicked('sw130s')"><a href="#tab1" data-toggle="tab">Summer & Winter Formal 130S</a>
                            </li>
                            <li class="alltab" id="sa" onclick="onTabClicked('sa')"><a href="#tab1" data-toggle="tab">Spring & Autumn Formal</a>
                            </li>
                            <li class="alltab" id="tux" onclick="onTabClicked('tux')"><a href="#tab1" data-toggle="tab">Tuxedo</a>
                            </li>
                            <li class="alltab" id="sac" onclick="onTabClicked('sac')"><a href="#tab1" data-toggle="tab">Spring & Autumn Causal</a>
                            </li>
                            <li class="alltab" id="swc" onclick="onTabClicked('swc')"><a href="#tab1" data-toggle="tab">Summer & Winter Casual</a>
                            </li>
                            <li class="alltab" id="over" onclick="onTabClicked('over')"><a href="#tab1" data-toggle="tab">Overcoat</a>
                            </li>
                            <li class="alltab" id="shirt" onclick="onTabClicked('shirt')"><a href="#tab1" data-toggle="tab">Custom Shirt</a>
                            </li>
                            <li class="alltab" id="tie" onclick="onTabClicked('tie')"><a href="#tab1" data-toggle="tab">Tie</a>
                            </li>

                        </ul>
                        <div class="tab-content" style="background-color:#eee ;padding:7px 5px 7px 5px">
                            <div class="tab-pane active" id="tab1">
                                <label style="color: red" id="fabric_error"></label>
                                <div class="col-md-6">
                                    <label>List of all fabrics</label>
                                </div>
                                <div class="col-md-6 no-gutter">
                                    <div class="pull-right search-container" >
                                        <input type="text" class="form-control search-query " id="search-text" placeholder="Search">
                                        <button type="button" class="btn btn-md btn-primary " style="height: 34px" onclick="searchFabricData();"><i class="fa fa-search"></i></button>
                                    </div>
                                </div>
                                <table id="orderTable" class="table table-striped table-bordered">


                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function uploadStock(){
        $(".modal-title").html("<label style='color: green'>Upload stock File!!!</label>");
        $(".modal-body").html("<p id='err_message'></p><div class='row'><div class='col-md-6'>" +
            "<div class='form-group'><label>Stock file</label><br><input type='file'  id='stockFile'" +
            " class='form-control'/></div></div><div class='col-md-6'>" +
            "<input type='button' value='Upload stock file' class='btn btn-danger pull-right'" +
            " style='margin-top:20px' onclick=uploadStockFile() /></div></div>");
        $(".modal-footer").css("display","none");
        $("#myModal").modal("show");
    }
    function uploadStockFile(){
        var stockFile = document.getElementById('stockFile');
        var data = new FormData();
        data.append('dataType','addStockData');
        data.append('stockFile',stockFile.files[0]);
//        data.append("fabricType",type);
        if($("#stockFile").val() == ''){
            $("#err_message").html("Please select .xls file");
            $("#err_message").css("color","red");
            return false;
        }
        $(".loaderdiv").removeClass("loaderhidden");
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            // alert(request.readyState);
            if(request.readyState === 4) {
                var response = $.parseJSON(request.response);
                var status = response.Status;
                console.log(status);
                if(status == "Success")
                {
//                    location.reload(true);
                    $("#err_message").html(response.Message);
                    $("#err_message").css("color","green");
                    getAllFabricData();
                    setTimeout(function () {
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#err_message").html('');
                        $("#myModal").modal("hide");
                    },1000);
                }
                else
                {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#err_message").html(response.Message);
                }
            }
        };
        request.open('POST', 'api/fabricProcess.php');
        request.send(data);
    }
    function close_diolog() {
        window.location = "fabric.php"
    }
    $(document).ready(function () {
        $('#priceTable').DataTable({});
    });
    function viewPrice() {
        $("#view-price").show();
        $("#layui-layer-shade5").show();
    }
    function editPrice(price_id){
        var url = "api/fabricProcess.php";
        var amount = $("#price_amount_"+price_id).val();
        var per_meter  = $("#price_amount_meter_"+price_id).val();

        $.post(url,{"dataType":"editPrice","price_id":price_id,"price_amount":amount,"price_amount_meter":per_meter}, function (data) {
            var Status = data.Status;
            if (Status == "Success"){
                window.location="fabric.php";
            }
        });
    }
    var fabric_list = [];

    var type = 'sw120s';

    $(document).ready(function () {
        getAllFabricData(type);
    });

    function addfabricdata() {
        var fabricFile = document.getElementById('fabricFile');
        var data = new FormData();
        data.append('dataType','storeData');
        data.append('fabricFile',fabricFile.files[0]);
        data.append("fabricType",type);
        if($("#fabricFile").val() == '') {

            $("#message").html("Please select .xls file");
            return false;
        }
        $(".loaderdiv").removeClass("loaderhidden");
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            // alert(request.readyState);

            if(request.readyState === 4) {

                var response = $.parseJSON(request.response);
                var status = response.Status;
                console.log(status);

                if(status == "Success")
                {
//                    $("#myModal").modal("hide");
//                    location.reload(true);
                    $("#message").html(response.Message);
                    $("#message").css("color","green");
                    getAllFabricData();
                    setTimeout(function () {
                        $("#message").html('');
                    },1000);
                }
                else
                {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#message").html(response.Message);
                }
            }

        };
        request.open('POST', 'api/fabricProcess.php');
        request.send(data);
    }
    function getAllFabricData() {
        fabric_list= [];
        var dataa = new FormData();
        dataa.append('fabricType',type);
        dataa.append('dataType','getFabricData');

        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            // alert(request.readyState);
            if(request.readyState === 4) {
                var response = $.parseJSON(request.response);
                var status = response.Status;

                if(status === "Success") {
                    $(".loaderdiv").addClass("loaderhidden");
                 var th_ = '<thead><tr><th style="display: none;">#</th><th>#</th><th>Cameo</th><th>RC</th><th>RCMTM </th><th>Yarn</th>'+
                 '<th>Weight</th><th>Catalog</th><th>Pattern</th><th>Attribute</th><th>Color</th><th>Price</th><th>Retail Price</th><th>Total stock</th><th>Status</th><th>Action</th></tr></thead><tbody>';
                 var data = response.data;
                 var td_ = '';

                  for(var i=0;i<data.length;i++) {
                     var counter = i+1;
//                     console.log(data[i].fabric_cameo);

                     if(data[i]) {
                         td_ = td_ + '<tr><td style="display: none;"></td><td><span id="fab_span_'+data[i].fabric_id+'">'+data[i].fabric_inc+'</span>'+
                             '<input type="text" style="display: none;width: 40px;" id="fab_input_'+data[i].fabric_id+'" value="'+data[i].fabric_inc+'">'+
                             '<i id="fab_edit_'+data[i].fabric_id+'" class="fa fa-pencil" style="float: right;color: white;background: chocolate;padding: 5px;border-radius: 2px;" onclick=editRow('+data[i].fabric_id+')></i>'+
                             '<i style="display: none;" id="fab_update_'+data[i].fabric_id+'" class="fa fa-floppy-o sty" onclick=updateRow('+data[i].fabric_id+')></i>'+
                             '</td>' + '<td>' + data[i].fabric_cameo + '</td>' + '<td>' + data[i].fabric_rc_code + '</td>' + '<td>' + data[i].fabric_rcmtm_composition + '</td>' +
                             '<td>' + data[i].fabric_yarn + '</td>' + '<td>' + data[i].fabric_weight + '</td>' + '<td>' + data[i].fabric_catalog + '</td>' + '<td>' + data[i].fabric_pattern + '</td>' +
                             '<td>' + data[i].fabric_attribute + '</td>' + '<td>' + data[i].fabric_color + '</td>' + '<td>' + data[i].fabric_price + '</td><td>'+data[i].fabric_retail_price+'</td><td>'+data[i].total_stock+'</td><td>'+data[i].stock_status+'</td><td style="width: 100px"><button class="btn btn-sm btn-primary" onclick=editFabricData("'+i+'")><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger" onclick=confirmDeleteFabricData("'+i+'")><i class="fa fa-trash"></i></button></td></tr>';
                         fabric_list.push(data[i]);
                     }
                  }

                  $('#orderTable').html(th_+td_);
                  $('#orderTable').DataTable({destroy:true});
                }
                else
                {
                    $(".loaderdiv").addClass("loaderhidden");
//                 $("#fabric_error").html(response.Message);
                    $('#orderTable').html('');
                    $('#orderTable').DataTable({destroy:true});
                }
            }
        };
        request.open('POST', 'api/fabricProcess.php');
        request.send(dataa);
    }
    function editRow(fab_id) {
        $("#fab_span_"+fab_id).hide();
        $("#fab_input_"+fab_id).show();
        $("#fab_edit_"+fab_id).hide();
        $("#fab_update_"+fab_id).show();

    }

    function updateRow(fab_id){
        $(".loader").show();
        var code = $("#fab_input_"+fab_id).val();
        var url = "api/fabricProcess.php";
        $.post(url, {
            "dataType": "updateManualRow",
            "fabric_id": fab_id,
            "fabric_inc": code
        }, function (data) {
           window.location="fabric.php";
        });
    }

    function editFabricData(index) {
        $(".modal-footer").show();
     $(".modal-title").html("Update Fabric");
     $(".modal-body").html("<div class='row'><div class='col-md-12' style='margin:0px;padding: 0px;'>" +
         "<div class='col-md-4'><label>RCMTM</label><input type='text' id='rcmtm' class='form-control' placeholder='enter rcmtm'" +
         " value='"+fabric_list[index].fabric_rcmtm_composition+"'/></div><div class='col-md-4'><label>Yarn</label>" +
         "<input type='text' id='yarn' class='form-control' placeholder='enter yarn' value='"+fabric_list[index].fabric_yarn+"'/></div>" +
         "<div class='col-md-4'><label>Weight</label><input type='text' id='weight' class='form-control' placeholder='enter weight'" +
         " value='"+fabric_list[index].fabric_weight+"'/></div><div class='col-md-12' style='margin-top:5px;padding: 0px;'>" +
         "<div class='col-md-4'><label>Catalog</label><input type='text' id='catalog' class='form-control' placeholder='enter catalog' value='"+fabric_list[index].fabric_catalog+"'/></div>" +
         "<div class='col-md-4'><label>Pattern</label><input type='text' id='pattern' class='form-control' placeholder='enter pattern' value='"+fabric_list[index].fabric_pattern+"'/></div>" +
         "<div class='col-md-4'><label>Attribute</label><input type='text' id='attribute' class='form-control' placeholder='enter attribute' value='"+fabric_list[index].fabric_attribute+"'/></div></div>" +
         "<div class='col-md-12' style='margin-top:5px;padding: 0px;'><div class='col-md-4'><label>Color</label><input type='text' id='color' class='form-control' placeholder='enter color' value='"+fabric_list[index].fabric_color+"'/></div>" +
         "<div class='col-md-4'><label>Price</label><input type='text' id='price' class='form-control' placeholder='enter price' " +
         "value='"+fabric_list[index].fabric_price+"'/></div><div class='col-md-4'><label>Total stock</label>" +
         "<input type='text' id='total_stock' class='form-control' value='"+fabric_list[index].total_stock+"' placeholder='total stock' />" +
         "</div><div class='col-md-4'><label>Stock Status</label><select class='form-control' id='stock_status'>" +
         "<option value=''>Select stock status</option></select></div> </div></div>");
    $(".modal-footer").html('<label id="messagee" style="margin-right: 23%;color:red"></label><button type="button" ' +
        'class="btn btn-primary" onclick="updateFabricData('+index+')"><i class="fa fa-edit"></i> Update</button>' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
     $('#myModal').modal('show');

    var stockStatus = ['Continued','Discontinued'];

        for (var i=0; i<stockStatus.length; i++){
            $("#stock_status").append(new Option(stockStatus[i], stockStatus[i]));
        }
        $('#stock_status > option[value="'+fabric_list[index].stock_status+'"]').attr("selected", "selected");

    }

    function confirmDeleteFabricData(index) {
        $(".modal-title").html("Delete Fabric");
        $(".modal-body").html("<div class='row'><div class='col-md-12' >" +
            "<label>Do you want delete this fabric</label></div>");
        $(".modal-footer").html('<label id="messagee" style="margin-right: 23%;color:red"></label><button type="button" class="btn btn-danger" onclick=deleteFabric("'+index+'")><i class="fa fa-trash"></i> Delete</button>' +
            '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
        $('#myModal').modal('show');

    }

    function deleteFabric(index) {
        var fabric_id = fabric_list[index].fabric_id;
        var dataa = new FormData();
        dataa.append('dataType','deleteFabricData');
        dataa.append('id',fabric_id);

        var request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            // alert(request.readyState);
            var response = $.parseJSON(request.response);
            var status = response.Status;
            var message = response.Message;
            console.log('response -- ' + message);
            $("#messagee").html(message);
            if (request.readyState === 4) {
              if(status === 'Success') {
                  $("#messagee").css('color','green');
                  setTimeout(function () {
                      $("#myModal").modal("hide");
                      getAllFabricData();
                  },1000);

              }
              else if(status === 'Failure') {
                  $("#messagee").css('color','red');
                  $("#messagee").html(message);
              }
            }
        };

        request.open('POST', 'api/fabricProcess.php');
        request.send(dataa);
    }

    function updateFabricData(index) {
        var rcmtm = $('#rcmtm').val();
        var yarn = $('#yarn').val();
        var weight = $('#weight').val();
        var catalog = $('#catalog').val();
        var pattern = $('#pattern').val();
        var attribute = $('#attribute').val();
        var color = $('#color').val();
        var price = $('#price').val();
        var total_stock = $('#total_stock').val();
        var stock_status = $('#stock_status').val();

        if(rcmtm === '' || yarn === '' || weight ==='' || catalog=== '' || pattern ==='' || attribute ==='' || color ==='' || price ==='') {
            $("#messagee").html("Please enter all fields...");
            return false;
        }
        else {

            var dataa = new FormData();
            dataa.append('dataType','updateFabricData');
            dataa.append('rcmtm',rcmtm);
            dataa.append('yarn',yarn);
            dataa.append('catalog',catalog);
            dataa.append('pattern',pattern);
            dataa.append('attribute',attribute);
            dataa.append('color',color);
            dataa.append('price',price);
            dataa.append('weight',weight);
            dataa.append('total_stock',total_stock);
            dataa.append('stock_status',stock_status);
            dataa.append('id',fabric_list[index].fabric_id);
            $(".loader").show();
            var request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                // alert(request.readyState);
                var response = $.parseJSON(request.response);
                var status = response.Status;
                var message = response.Message;
                console.log('response -- '+message);
                if(request.readyState === 4) {
                    $(".loader").hide();
                    $("#messagee").html(message);
                    if(status === 'Success') {
                      $("#messagee").css('color','green');
                      setTimeout(function(){
                          $("#myModal").modal("hide");
                          getAllFabricData();
                      },2000);
                    }
                    else if(status === 'Failure') {
                        $("#message").css('color','red');
                    }
                }
                else{
                    $("#messagee").html("Unable request to server");
                    $("#messagee").css('color','red');
                }
            };

            request.open('POST', 'api/fabricProcess.php');
            request.send(dataa);
        }
    }

    function searchFabricData() {
        var search_text = $("#search-text").val();
        if(search_text === '') {
            $(".modal-title").html("<label style='color: green'>Error !!!</label>");
            $(".modal-body").html("<div class='row'><div class='col-md-12' >" +
                "<label >Please enter search text</label></div>");
            $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $('#myModal').modal('show');
            return false;
        }
        $(".loader").show();
        var url = 'api/fabricProcess.php';
        $.post(url,{'dataType':'searchFabricData1','cameo_code':search_text},function (data) {
            var status = data.Status;
            var message = data.Message;
            fabric_list = [];
            $(".loader").hide();
            if(status === 'Success') {

                var th_ = '<thead><tr><th>#</th><th>Cameo</th><th>RC</th><th>RCMTM </th><th>Yarn</th>'+
                '<th>Weight</th><th>Catalog</th><th>Pattern</th><th>Attribute</th><th>Color</th><th>Price</th><th>Retail Price</th><th>Total Stock</th><th>Stock Status</th><th>Action</th></tr></thead><tbody>';
                var data = data.data;
                var td_ = '';
                var i=0;
                var counter = i+1;
                if(data[i]) {
                    td_ = td_ + '<tr><td style="display: none;"></td><td><span id="fab_span_'+data[i].fabric_id+'">'+data[i].fabric_inc+'</span>'+
                        '<input type="text" style="display: none;width: 40px;" id="fab_input_'+data[i].fabric_id+'" value="'+data[i].fabric_inc+'">'+
                        '<i id="fab_edit_'+data[i].fabric_id+'" class="fa fa-pencil" style="float: right;color: white;background: chocolate;padding: 5px;border-radius: 2px;" onclick=editRow('+data[i].fabric_id+')></i>'+
                        '<i style="display: none;" id="fab_update_'+data[i].fabric_id+'" class="fa fa-floppy-o sty" onclick=updateRow('+data[i].fabric_id+')></i>' + '<td>' + data[i].fabric_cameo + '</td>' + '<td>' + data[i].fabric_rc_code + '</td>' + '<td>' + data[i].fabric_rcmtm_composition + '</td>' +
                    '<td>' + data[i].fabric_yarn + '</td>' + '<td>' + data[i].fabric_weight + '</td>' + '<td>' + data[i].fabric_catalog + '</td>' + '<td>' + data[i].fabric_pattern + '</td>' +
                    '<td>' + data[i].fabric_attribute + '</td>' + '<td>' + data[i].fabric_color + '</td>' + '<td>' + data[i].fabric_price + '</td><td>' + data[i].fabric_retail_price + '</td>' +
                        '<td>' + data[i].total_stock + '</td><td>' + data[i].stock_status + '</td><td style="width: 100px"><button class="btn btn-sm btn-primary" onclick=editFabricData("'+i+'")><i class="fa fa-edit"></i></button><button class="btn btn-sm btn-danger" onclick=confirmDeleteFabricData("'+i+'")><i class="fa fa-trash"></i></button></td></tr>';
                    $(".alltab").removeClass("active");
                    $("#"+data[i].fabric_type).addClass("active");
                    fabric_list.push(data[i]);
                }
                $('#orderTable').html(th_+td_);
                $('#orderTable').DataTable({destroy:true});
            }
            else if(status === 'Failure') {
                $(".modal-title").html("<label style='color: green'>Error !!!</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12' >" +
                "<label >"+message+"</label></div>");
                $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
                $('#myModal').modal('show');
            }
        });
    }
    function onTabClicked (id) {
        console.log('id -- '+id);
        type = id;
        getAllFabricData();
    }




</script>
