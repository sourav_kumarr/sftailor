<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 12:45 PM
 */
namespace Classes;
class STORECONNECT{
    public $link = null;
    public $response = null;
    public function connect(){
        $this->link = mysqli_connect(DBHOST,DBUSER,DBPASSWORD,DBNAME);
        return $this->link;
    }
    public function closeConnection(){
        mysqli_close($this->link);
    }
    public function getLastId(){
        return mysqli_insert_id($this->link);
    }
    public function sqlError(){
        return mysqli_error($this->link);
    }
    function __destruct(){
        $this->link = null;
        $this->response = null;
    }
}