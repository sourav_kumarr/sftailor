<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 05-12-2017
 * Time: 13:48
 */
include("header.php");
?>
<link rel="stylesheet" href="css/create_message.css"/>
<style>
    .btn-primary{
        background-color: #202020;
        border:1px #202020 solid;
    }
    .btn-primary:hover{
        background-color: #202020;
        border:1px #202020 solid;
    }
</style>
<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="msg-body msg-body-1 active">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title" style="padding-bottom:2px; ">
                        <label style="    font-size: 29px;letter-spacing: 2px;">Drafts</label>
                        <button class="btn btn-sm btn-primary pull-right" onclick="window.location='create_message.php'">Create Message</button>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12 no-gutter">
                        <div class="col-md-8 col-sm-8 col-xs-8 no-gutter msg-container">
                            <table class="table table-hover" id="msgTable">
                            </table>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 no-gutter">
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter  msgs-header">
                                <div class="msgs-header-item">
                                    <label>Preview : HTML | </label>
                                </div>
                                <div class="msgs-header-item">
                                    <label>Plain Text</label>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12 col-xs-12 no-gutter " style="padding: 1%;height: 400px;overflow: auto;overflow-x: hidden;" id="rightContent">

                            </div>
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<script>

    var items = [];

    function getAllMessages() {
        var url = "api/messageProcess.php";
        $.post(url, {type: 'allMessages'}, function (data) {
            console.log(JSON.stringify(data));
            var status = data.Status;
            var message = data.Message;
            var th_ = '<thead><tr><th>#</th><th>Title</th><th>Created at</th><th>Updated at</th><th>Actions</th></tr></thead><tbody>';
            var td_ = "";
            if (status === 'Success') {
                items = data.data;

                for (var i = 0; i < items.length; i++) {
                    /*td_ = td_ + '<tr onclick="onMessageLoaded('+i+')"><td>'+(i+1)+'</td><td>' + items[i].msg_text + '</td><td>' + items[i].msg_date + '</td><td>' + items[i].msg_date + '</td><td><button class="btn btn-primary btn-sm" onclick="edit_message('+i+');"><i class="fa fa-pencil"></i></button>' +
                        '<button class="btn btn-danger btn-sm" onclick="confirmDeletee('+i+');"><i class="fa fa-trash-o"></i></button><button class="btn btn-success btn-sm" onclick=window.location="send_message.php?id='+btoa(items[i].msg_id)+'"><i class="fa fa-envelope"></i></button></td>' +
                        '</tr>';*/
                    td_ = td_ + '<tr onclick="onMessageLoaded('+i+')"><td>'+(i+1)+'</td><td>' + items[i].msg_text + '</td><td>' + items[i].msg_date + '</td><td>' + items[i].msg_date + '</td><td><div class="dropdown">'+
                        '<button class="btn btn-primary dropdown-toggle" style="margin: 0px;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"> Options '+
                        '<span class="caret"></span></button><ul class="dropdown-menu" aria-labelledby="dropdownMenu1"><li onclick="edit_message('+i+');"><a href="#"><i class="fa fa-pencil"></i> Edit</a></li>'+
                         '<li onclick="confirmDeletee('+i+');"><a href="#"><i class="fa fa-trash-o"></i>  Delete</a></li><li onclick=window.location="send_message.php?id='+btoa(items[i].msg_id)+'"><a href="#"><i class="fa fa-envelope"></i> Send Mail</a></li><li onclick=window.location="broadcast_message.php?id='+btoa(items[i].msg_id)+'"><a href="#"><i class="fa fa-clock-o"></i> Schedule a broadcast</a></li></ul></div></td></tr>';
                    if (i === 0) {
                        var msg_desc = items[i].msg_desc;
                        var msg_signature = items[i].msg_signature;

                        $("#rightContent").html(msg_desc);
                        $("#rightContent").append(msg_signature);

                    }
                }
            }
            $("#msgTable").html(th_ + td_);

        });
    }

    function onMessageLoaded(index) {
        var msg_desc = items[index].msg_desc;
        var msg_signature = items[index].msg_signature;

        $("#rightContent").html(msg_desc);
        $("#rightContent").append(msg_signature);

    }
    function edit_message(index) {
        var msg_id = btoa(items[index].msg_id);
        window.location = 'edit_message.php?id='+msg_id;
    }

    function remove_message(index) {
        var msg_id = items[index].msg_id;
        var url = "api/messageProcess.php";
        console.log(msg_id);
        $.post(url,{type:'removeMessage',msg_id:msg_id},function(data){
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
              getAllMessages();
              $("#myModal").modal("hide");
            }
            else{
                $("#error").html(message);
            }


        });

    }

    function confirmDeletee(index) {
        $(".modal-title").html("<label style='color: #202020;' >Error</label>");
        $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>Are you sure want to delete message (y/n) ?</label>");
        $(".modal-footer").html('<label id="error" style="color:red;margin-right: 3%;"></label><button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>' +
            '<button type="button" class="btn btn-danger" onclick="remove_message('+index+');"><i class="fa fa-trash"></i> Delete</button>');
        $("#myModal").modal("show");
    }

    function broadcastMessage(index) {
        var subject = items[index].msg_text;
        $(".modal-title").html("<label style='color: #202020;' >Broadcast Setting</label>");
        $(".modal-body").html('<div class="col-md-12"><div class="panel panel-primary"><div class="panel-heading"><label>When should this message be sent?</label></div>' +
            '<div class="panel-body"><div class="col-md-12 no-gutter"><label style="width: 17%;">Subject </label><span>&nbsp;:&nbsp;'+subject+' </span></div> ' +
            '<div class="col-md-12 no-gutter"><label style="width: 17%;">Select Date</label> :&nbsp; <input type="text" id="start_date" placeholder= "Select schedule date"/></div></div></div>');
        $(".modal-footer").html('<label id="error" style="color:red;margin-right: 3%;"></label><button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-remove"></i> Close</button>' +
            '<button type="button" class="btn btn-success" ><i class="fa fa-clock-o"></i> Schedule</button>');
        $(".modal-body").css("height","170px");
        $("#myModal").modal("show");
        $('#start_date').datetimepicker({
            dayOfWeekStart : 1,
            lang:'en',
            value: "",
            step:30,
            format:"d M Y h:i:s A",
            startDate:	new Date()
        });
    }

    window.onload = function () {
        getAllMessages();
    };
</script>
<?php
include("footer.php");
?>
