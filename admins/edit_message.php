<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 04-12-2017
 * Time: 14:36
 */
include("header.php");
require_once ('api/Constants/functions.php');
require_once('api/Constants/configuration.php');
require_once('api/Constants/DbConfig.php');
require_once('api/Classes/MESSAGE.php');
require_once('api/Classes/CONNECT.php');
$msgClass = new \Classes\MESSAGE();
$id = base64_decode($_REQUEST['id']);
$msgClass->setMsgId($id);
$response = $msgClass->getParticularMessage();

$msg_text = $response['data']["msg_text"];
$msg_desc = $response['data']["msg_desc"];
$msg_id = $response['data']["msg_id"];

?>
<link rel="stylesheet" href="css/create_message.css"/>
<script src="//cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="msg-body msg-body-1 active">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">

                        <label onclick="window.location='draft_message.php'" style="cursor: pointer;font-size: 22px;"><i class="fa fa-arrow-circle-left"></i>&nbsp;Update Message</label>
                    </div>
                    <div class="col-md-12 no-gutter" style="margin-bottom: 1%">
                        <div class="col-md-6 no-gutter" >
                            <input type="text" id="msg" placeholder="Insert you subject here" value="<?php echo $msg_text;?>" class="form-control"/>
                        </div>
                        <div class="col-md-6 no-gutter ">
                            <button class="btn btn-primary btn-sm pull-right" onclick="onLevelLoaded(2)">Preview & Test</button>
                        </div>
                    </div>
                    <div class="col-md-12 no-gutter">
                        <textarea name="text" id="text" style="resize: none"><?php echo $msg_desc;?></textarea>
                        <script>
                            CKEDITOR.replace( 'text', {
                                height: 300
                            });
                        </script>
                    </div>
                    <div class="col-md-12 msg-footer">
                        <button type="button" class="btn btn-primary btn-md" onclick="onSaveData(false);">Save</button>
                        <button type="button" class="btn btn-success btn-md" onclick="onSaveData(true);">Save & Exit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="msg-body msg-body-2">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="col-md-12 x_title no-gutter" style="border: none">
                        <ul class="list-unstyled list-inline">
                            <li style="font-size: 21px;cursor: pointer;" onclick="onLevelLoaded(1);"><i class="fa fa-arrow-circle-left"></i></li>
                            <li><h2>Preview & Test</h2><label style="margin-top: 6px;margin-left: 6px;">Enter an email to send your test message to.</label></li>

                    </div>
                    <div class="col-md-12 no-gutter send-box content-box">
                        <div class="col-md-6">
                            <input id="receiver-email" placeholder="Enter e-mail address" class="form-control" style="border-radius: 5px;"/>
                        </div>
                        <div class="col-md-6" style="text-align: right;">
                            <button class="btn btn-sm btn-success">Send Test</button>
                        </div>
                    </div>
                    <div class="col-md-12 " style="margin-top: 2%;" id="data-txt">
                        <label>Follow</label>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
</div>
</div>

<script>
    var mess_id = "<?php echo $msg_id;?>";
    console.log(mess_id);
    var msg_signature = '<div id="aweber_rem" style="background-color:#ffffff !important; color:#000000 !important;font-family:Verdana !important; font-size:10px !important; max-width:600px; padding:8px !important; text-align:left; width:100%;">'+
        '2500 Reliance Avenue Apex NC 27539 USA<br/><a href="#" style="color: #00f !important;">Unsubscribe</a> | <a href="#" style="color: #00f !important;">Change Subscriber Options</a><br/><br/></div>';

    function onLevelLoaded(stepno) {
        $(".msg-body").removeClass("active");
        $(".msg-body.msg-body-"+stepno).addClass("active");
        if(stepno === 2) {
            var editorData= CKEDITOR.instances['text'].getData();
            $("#data-txt").html(editorData);
            $("#data-txt").append(msg_signature);

        }
    }

    function onSaveData(isExit) {
        var subject = $("#msg").val();
        var msgData = CKEDITOR.instances['text'].getData();
        if(subject === '' || msgData === '') {
            $(".modal-title").html("<label style='color: #202020;' >Error</label>");
            $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>Please enter all fields ! ! !</label>");
            $("#myModal").modal("show");
            return false;
        }
        var url = "api/messageProcess.php";
        $(".loader").css("display","block");
        var data = {msg_text:subject,msg_desc:msgData,msg_signature:msg_signature,type:'saveMessage'};
        if(mess_id!=="") {
            data = {msg_text:subject,msg_desc:msgData,msg_signature:msg_signature,type:'saveMessage',msg_id:mess_id};
        }
        $.post(url,data,function(data){
            $(".loader").css("display","none");

            var status = data.Status;
            var message = data.Message;
            if(status === 'Failure') {
                $(".modal-title").html("<label style='color: #202020;' >Error</label>");
                $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>"+message+"</label>");
                $("#myModal").modal("show");

            }
            else {
                if(data.msg_id)
                    mess_id = data.msg_id;
                $(".modal-title").html("<label style='color: #202020;'>Success</label>");
                $(".modal-body").html("<label style='color: green;letter-spacing: 1px;'>"+message+"</label>");
                $("#myModal").modal("show");
                setTimeout(function () {
                    $("#myModal").modal("hide");
                    if(isExit) {
                        // go to drafts page here ....
                        window.location = "draft_message.php";
                    }
                },2000);
            }
        });
    }
</script>
<?php
include("footer.php");
?>
