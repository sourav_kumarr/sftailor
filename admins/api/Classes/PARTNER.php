<?php
/**
 * Created by PhpStorm.
 * User: harry
 * Date: 10/24/2017
 * Time: 11:21 AM
 */


namespace Classes;
require_once('CONNECT.php');
require_once('USERCLASS.php');
class PARTNER{
    public $link = null;
    public $userClass = null;
    public $response = array();
    public function __construct(){
        $this->link = new CONNECT();
        $this->userClass = new USERCLASS();
    }
    public function addGalleryImg($event,$iseventimg,$oldeventimg,$partnerlink,$partnername){
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $event_img = "";
        if ($iseventimg == "yes") {
            if(isset($_FILES['eventimg'])) {
                $evefile_tmp = $_FILES['eventimg']['tmp_name'];
                $eve_extensions = array("jpg", "png", "gif", "jpeg");
                $evefile_ext = strtolower(end(explode('.', $_FILES['eventimg']['name'])));
                if (in_array($evefile_ext, $eve_extensions) === true) {
                    $eve_db_file_name = rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$evefile_ext;
                    if (move_uploaded_file($evefile_tmp, getcwd() . "/Files/images/partner/" . $eve_db_file_name)) {
                        $event_img = $eve_db_file_name;
                    }
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Please Select Event Image";
                return $this->response;
            }
        }
        else {
            $event_img = $oldeventimg;
        }
        foreach($_FILES['file_']['tmp_name'] as $key => $tmp_name ) {
            $file_name = $_FILES['file_']['name'][$key];
            if (strpos($file_name, "'") >= 0) {
                $file_name = str_replace("'", "", $file_name);
            }
            $file_tmp = $_FILES['file_']['tmp_name'][$key];
            $extensions = array("jpg", "png", "gif", "jpeg");
            $file_ext = strtolower(end(explode('.', $_FILES['file_']['name'][$key])));
            if (in_array($file_ext, $extensions) === true) {
                $db_file_name = rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$file_ext;
                if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/partner/" . $db_file_name)) {
                    $gallery_title = explode('.',$file_name)[0];
                    $getCat =  "SELECT * FROM partner where category = '$event'";
                    $getCat_result = mysqli_query($link, $getCat);
                    if($getCat_result) {
                        $getCat_num = mysqli_num_rows($getCat_result);
                        if ($getCat_num > 0) {
                            $getCat_row = mysqli_fetch_assoc($getCat_result);
                            $sort_order_cat = $getCat_row['sort_order_cat'];
                        }
                        else{
                            $getMaxCatSort = "SELECT MAX(sort_order_cat) AS sort_order_cat FROM partner";
                            $getMaxCatSort_result = mysqli_query($link, $getMaxCatSort);
                            if ($getMaxCatSort_result) {
                                $sort_order_cat = 0;
                                $getMaxCatSort_num = mysqli_num_rows($getMaxCatSort_result);
                                if($getMaxCatSort_num > 0){
                                    $sort_row = mysqli_fetch_assoc($getMaxCatSort_result);
                                    $sort_order_cat = $sort_row["sort_order_cat"];
                                    $sort_order_cat++;
                                }
                            }
                            else{
                                $this->response[STATUS] = Error;
                                $this->response[MESSAGE] = $this->link->sqlError();
                                return false;
                            }
                        }
                        $getMaxSort = "select MAX(sort_order)  AS sort_max from partner";
                        $getMaxSort_result = mysqli_query($link, $getMaxSort);
                        if ($getMaxSort_result) {
                            $sort_max = 0;
                            $sortNum = mysqli_num_rows($getMaxSort_result);
                            if ($sortNum > 0) {
                                $sort_row = mysqli_fetch_assoc($getMaxSort_result);
                                $sort_max = $sort_row["sort_max"];
                                $sort_max++;
                            }
                            $file_path = "http://scan2fit.com/sftailor/admins/api/Files/images/partner/" . $db_file_name;
                            $query = "insert into partner (status,category,fairTitle, fairLink,fairLogo,fairLogoPath,sort_order,category_img,sort_order_cat)
                            values ('1','$event','$gallery_title','$partnerlink','$db_file_name','$file_path','$sort_max','$event_img','$sort_order_cat')";
                            $result = mysqli_query($link, $query);
                            if (!$result) {
                                $this->response[STATUS] = Error;
                                $this->response[MESSAGE] = $this->link->sqlError();
                                return $this->response;
                            }
                            $this->response[STATUS] = Success;
                            $this->response[MESSAGE] = "File uploaded successfully";
                        }
                        else {
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = $this->link->sqlError();
                        }
                    }
                    else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                        return false;
                    }

                }else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = " getcwd is  ".getcwd();
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "Wrong File Selected";
            }
        }
        return $this->response;
    }
    public function isFileExists($file_name) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from gallery where gallery_file='$file_name'";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $this->response[STATUS] = Error;
        $this->response[MESSAGE] = "File already exists with this name ".$file_name;
        $this->response["data"] = $data;
        return $this->response;
    }
    public function getPartner() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from partner ORDER BY sort_order ASC";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;
    }
    public function getCategories() {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select distinct(category) from partner ORDER BY sort_order_cat";
        $result = mysqli_query($link,$query);
        $count = mysqli_num_rows($result);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }
        while($rows = mysqli_fetch_assoc($result)) {
            $category = $rows['category'];
            $qq = "select * from partner where category = '$category' limit 1";
            $rr = mysqli_query($link,$qq);
            $roww = mysqli_fetch_assoc($rr);
            $data[] = $roww;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;
    }
    public function updateSortOrder($sortOrder,$idOrder) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        for($j = 0;$j < count($idOrder); $j++){
            $update_sort = "update partner set sort_order= '$sortOrder[$j]' where id = '$idOrder[$j]' ";
            $result = mysqli_query($link,$update_sort);
            $count = mysqli_affected_rows($link);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update_sort;

                return $this->response;
            }
        }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }
    public function updateCatSortOrder($sortOrder,$idOrder) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        for($j = 0;$j < count($idOrder); $j++){
            $update_sort = "update partner set sort_order_cat= '$sortOrder[$j]' where category = '$idOrder[$j]' ";
            $result = mysqli_query($link,$update_sort);
            $count = mysqli_affected_rows($link);
            if(!$result) {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
                $this->response["sql"] = $update_sort;

                return $this->response;
            }
        }


        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data update successfully";

        return $this->response;

    }
    public function deletePartner($id,$path) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from partner where id='$id'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);

        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        $this->response["data"] = $data;
        unlink(getcwd()."/Files/images/partner/".$path);
        return $this->response;
    }
    public function deleteCategory($category) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }

        $query = "select * from partner where category='$category'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }
        $categoryfilepath = "";
        while($rows = mysqli_fetch_assoc($result)){
            $fairLogo = $rows['fairLogo'];
            $category_img = $rows['category_img'];
            $filepath = getcwd()."/Files/images/partner/".$fairLogo;
            $categoryfilepath = getcwd()."/Files/images/partner/".$category_img;
            unlink($filepath);
        }
        $query = "delete from partner where category='$category'";
        $result = mysqli_query($link,$query);
        if($result) {
            unlink($categoryfilepath);
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "Data deleted successfully";
            return $this->response;
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable To Delete Event";
            return $this->response;
        }
    }
    public function editCategory($new_category,$old_category,$oldcategoryImagechanged,$oldcategoryImage) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldcategoryImagechanged == "yes") {
            if(isset($_FILES['newcatimg'])) {
                $file_tmp = $_FILES['newcatimg']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['newcatimg']['name'])));
                if (in_array($file_ext, $extensions) === true) {
                    $db_file_name = rand(0000, 9999) . strtotime(date("d M Y h:i:s A")) . "." . $file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/partner/" . $db_file_name)) {
                        $catimg = $db_file_name;
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "Server Error to Upload File";
                        return $this->response;
                    }
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }else{
            $catimg = $oldcategoryImage;
        }
        $query = "update partner set category = '$new_category',category_img = '$catimg' where category = '$old_category'";
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }
    public function editGallery($gallery_id,$gallery_title,$oldGalleryImagechanged,$partnerLink) {
        $link = $this->link->connect();
        if(!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($oldGalleryImagechanged == "yes") {
            if(isset($_FILES['newgalleryimg']['tmp_name'])) {
                $file_name = $_FILES['newgalleryimg']['name'];
                if (strpos($file_name, "'") >= 0) {
                    $file_name = str_replace("'", "", $file_name);
                }
                $file_tmp = $_FILES['newgalleryimg']['tmp_name'];
                $extensions = array("jpg", "png", "gif", "jpeg");
                $file_ext = strtolower(end(explode('.', $_FILES['newgalleryimg']['name'])));
                if (in_array($file_ext, $extensions) === true) {
                    $db_file_name = rand(0000,9999).strtotime(date("d M Y h:i:s A")).".".$file_ext;
                    if (move_uploaded_file($file_tmp, getcwd() . "/Files/images/partner/" . $db_file_name)) {
//                        move_uploaded_file($file_tmp, getcwd() . "/Files/images/partner/" . $file_name);
                        $file_path = "http://scan2fit.com/sftailor/admins/api/Files/images/partner/" . $db_file_name;
                        $query = "update partner set fairTitle='$gallery_title',fairLink='$partnerLink',fairLogo='$db_file_name',fairLogoPath='$file_path'
                        where id = '$gallery_id'";
                    }else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = " getcwd is  ".getcwd();
                        return $this->response;
                    }
                }
                else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Wrong File Selected";
                    return $this->response;
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = "No File Selected";
                return $this->response;
            }
        }else{
            $query = "update partner set fairTitle='$gallery_title',fairLink='$partnerLink' where id = '$gallery_id'";
        }
        $result = mysqli_query($link,$query);
        $count = mysqli_affected_rows($link);
        if(!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to update data";
            $this->response["sql"] = $query;

            return $this->response;
        }
        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data Updated successfully";
        return $this->response;
    }
    public function apiResponse($response,$code=null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}