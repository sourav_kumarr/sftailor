function ajaxCall() {
    this.send = function (data, url, method, success, type) {
        type = type || 'json';
        var successRes = function (data) {
            success(data);
        }

        var errorRes = function (e) {
            console.log(e);
            //alert("Error found \nError Code: "+e.status+" \nError Message: "+e.statusText);
            $('#loader').modal('hide');
        }
        $.ajax({
            url: url,
            type: method,
            data: data,
            success: successRes,
            error: errorRes,
            dataType: type,
            timeout: 60000
        });

    }

}

function locationInfo() {
    var rootUrl = "http://stsmentor.com/locationapi/locationApi.php";
    //var rootUrl = "http://stsmentor.com/locationapi/citydemo/api.php";
    var call = new ajaxCall();
    this.getCities = function (id) {
        $(".shipcities option:gt(0)").remove();
        var url = rootUrl + '?type=getCities&stateId=' + id;
        var method = "post";
        var data = {};
        $('.shipcities').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function (data) {
            $('.shipcities').find("option:eq(0)").html("Select City");
            if (data.tp === 1) {
                var count = 0;
                $.each(data['result'], function (key, val) {
                    $.each(val, function (keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('cityid', keys);
                        count = count + (option.length);
                        $('.shipcities').append(option);
                    });
                });
                if (count !== 0) {
                    $("#shipcityfeild").html("<select class='custom_signup_feild shipcities form-control' id='shipCityId'></select>");
                    $.each(data['result'], function (key, val) {
                        $.each(val, function (keys, vals) {
                            var option = $('<option />');
                            option.attr('value', vals).text(vals);
                            option.attr('cityid', keys);
                            count = count + (option.length);
                            $('.shipcities').append(option);
                        });
                    });
                }
                else {
                    $("#shipcityfeild").html("<input type='text' id='shipCityId' class='custom_signup_feild form-control' placeholder='Enter City Manually' />");
                }
                $(".shipcities").prop("disabled", false);
            }
            else {
                //alert("no city found");
            }
        });
    };

    this.getStates = function (id) {
        $(".shipstates option:gt(0)").remove();
        $(".shipcities option:gt(0)").remove();
        var url = rootUrl + '?type=getStates&countryId=' + id;
        var method = "post";
        var data = {};
        $('.shipstates').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function (data) {
            $('.shipstates').find("option:eq(0)").html("Select State");
            if (data.tp == 1) {
                $.each(data['result'], function (key, val) {
                    $.each(val, function (keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('stateid', keys);
                        $('.shipstates').append(option);
                    });
                });
                $(".shipstates").prop("disabled", false);
            }
            else {
                //alert(data.msg);
            }
        });
    };

    this.getCountries = function () {
        var url = rootUrl + '?type=getCountries';
        var method = "post";
        var data = {};
        $('.shipcountries').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function (data) {
            $('.shipcountries').find("option:eq(0)").html("Select Country");
            console.log(data);
            if (data.tp == 1) {
                $.each(data['result'], function (key, val) {
                    //$.each(data.result, function(key, val) {
                    $.each(val, function (keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('countryid', keys);
                        $('.shipcountries').append(option);
                    });

                });
                $(".shipcountries").prop("disabled", false);
            }
            else {
                //alert(data.msg);
            }
        });
    };

}

$(function () {
    var loc = new locationInfo();
    loc.getCountries();
    $(".shipcountries").on("change", function (ev) {
        var countryId = $("option:selected", this).attr('countryid');
        if (countryId != '') {
            loc.getStates(countryId);
        }
        else {
            $(".shipstates option:gt(0)").remove();
        }
    });
    $(".shipstates").on("change", function (ev) {
        var stateId = $("option:selected", this).attr('stateid');
        if (stateId != '') {
            loc.getCities(stateId);
        }
        else {
            $(".shipcities option:gt(0)").remove();
        }
    });
});


