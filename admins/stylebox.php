<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/CATEGORY.php');
$conn = new \Classes\CONNECT();
$category = new \Classes\CATEGORY();
?>
<style>
    .cop-price {
        float: left;
        height: 28px;
        width: 55px;
    }

    .couponForCheck {
        margin: 0 10px !important;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
</style>
<script src="https://cdn.ckeditor.com/4.7.3/standard/ckeditor.js"></script>
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
    <!-- page content -->
    <div class="right_col" role="main">
        <!-- top tiles -->
        <div class="row tile_count">

        </div>
        <div class="row" role="main">
            <div class="">
                <div class="clearfix"></div>
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel">
                            <div class="x_title">
                                <h2><span style="color:#1ABB9C;"></span>
                                    StyleBox
                                    <small></small>
                                </h2>
                                <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <div class="form-group form-inline">
                                            <input type="button" class="btn btn-danger btn-sm" onclick="addStylebox()"
                                            value=" + Add StyleBox" />
                                        </div>
                                    </li>
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <div class="x_content">
                                <p class="text-muted font-13 m-b-30">
                                    From here admin can manage/modify the content of the stylebox
                                </p>
                                <div id="styleboxData"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- /page content -->
<?php
include('footer.php');
?>
<style>
    .stylebox_img{
        height: 100px;
        width: 100px;
        border: 1px solid #ccc;
    }
    .old_stylebox_img{
        height: 100px;
        width: 100px;
        border: 1px solid #ccc;
    }
    .filebtn{
        height: 100px;
        width: 100px;
        position: absolute;
        top: 24px;
        opacity: 0;
    }
</style>
<script>
    function addStylebox(){
        $(".modal-title").html("<label style='color: green'>Add New Style Box</label>");
        $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='styleboxError'" +
        " style='color:red'></p><div class='col-md-6 form-group'><label>Title</label>" +
        "<input type='text' class='form-control' id='title' placeholder='Enter Title of StyleBox' /></div>" +
        "<div class='col-md-6 form-group'><label>Price</label>" +
        "<input type='text' id='price' class='form-control' placeholder='Enter Price in $' /></div>" +
        "<div class='col-md-12 form-group'><label>Style Box Description</label>" +
        "<textarea rows='2' placeholder='Enter Description of StyleBox' class='form-control' id='editor2' >" +
        "</textarea></div><div class='col-md-6 form-group'><label>Style Box Image</label><br>" +
        "<img src='' alt='StyleBox Image' class='stylebox_img' /><input type='file' " +
        "name='styleboxfile' id='stylebox_file' onchange=readURL2(this,'stylebox_img') class='filebtn' /></div>" +
        "<div class='col-md-12'><input type='button' class='btn btn-danger pull-right' style='margin-top:30px' " +
        "onclick=loadTextArea('') id='subbtn' value='Submit'/></div></div><p class='percentprog' " +
        "style='color:green;text-align:center'></div>");
        $(".modal-footer").css('display', 'none');
        CKEDITOR.replace( 'editor2');
        $("#myModal").modal("show");
    }
    function readURL2(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                //oldEventImagechanged = "yes";
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    function addStyleboxFinal(){
        var title = $("#title").val();
        var price = $("#price").val();
        var description = $("#editor2").val();
        var stylebox_file = $("#stylebox_file").val();
        if(title === "" || price === "" || description === "" || stylebox_file === ""){
            $("#styleboxError").html("Please fill all required fields");
            $("#styleboxError").css("color","red");
            return false;
        }
        var data = new FormData();
        var stylebox_file_obj = document.getElementById('stylebox_file');
        var stylebox_file_ext = stylebox_file.split(".");
        stylebox_file_ext = stylebox_file_ext[stylebox_file_ext.length - 1];
        if (stylebox_file_ext === "jpg" || stylebox_file_ext === "png" || stylebox_file_ext === "gif" || stylebox_file_ext === "jpeg") {
            data.append('stylebox_file', stylebox_file_obj.files[0]);
        }
        else{
            $("#styleboxError").html("Image File Should Be JPG, PNG, GIF Formats Only");
            $("#styleboxError").css("color","red");
        }
        data.append('type', "addStylebox");
        data.append('title', title);
        data.append('description', description);
        data.append('price', price);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState === 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status === "Success"){
                    $("#styleboxError").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else{
                    $("#styleboxError").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/userProcess.php');
        request.send(data);
    }
    function getStyleboxData(){
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "getStyleboxData"}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var StyleboxData = data.StyleboxData;
            if (Status === "Success") {
                var tbodyData ="";
                var table = '<table id="catTable" class="table table-striped table-bordered display" >' +
                '<thead><tr><th>#</th><th>Image</th><th>Title</th><th>Description</th><th>Price</th><th>Action</th></tr></thead>';
                for(var a=0;a<StyleboxData.length;a++){
                    var styleboxImg = StyleboxData[a].image;
                    if(styleboxImg === ""){
                        styleboxImg = "images/suit-accessories.jpg";
                    }else{
                        styleboxImg = "api/Files/images/stylebox/"+styleboxImg;
                    }
                    tbodyData = tbodyData + "<tr><td>"+(parseInt(a) +1)+"</td><td><img src='"+styleboxImg+"' style='height:40px' /></td>" +
                    "<td>"+StyleboxData[a].title+"</td><td>"+StyleboxData[a].description+"</td><td>$"+StyleboxData[a].price+"</td>" +
                    "<td><a  onclick=confirmDelete_stylebox('"+StyleboxData[a].u_id+"'); style='float:right;font-size:18px;" +
                    "padding:2px 4px;background: red;color:white;margin-left: 5px'><i class='fa fa-trash-o'></i></a>" +
                    "<a  onclick=editStylebox('"+StyleboxData[a].u_id+"'); style='float: right; font-size:18px;" +
                    "padding:2px 4px; background:red;color: white'><i class='fa fa-pencil'></i></a></td></tr>";
                }
                $("#styleboxData").html(table+"<tbody>"+tbodyData+"</tbody></table>");
                $("#catTable").dataTable();
                $(".loaderdiv").addClass("loaderhidden");
            }
            else{
                $(".loaderdiv").addClass("loaderhidden");
                $("#styleboxData").html("<p class='text-center'><label>No Data Found</label></p>");
            }
        });
    }
    getStyleboxData();
    function editStylebox(u_id){
        var url = "api/userProcess.php";
        $.post(url, {"type": "editStyleData", "u_id": u_id}, function (data) {
            var Status = data.Status;
            var Message = data.Message;
            var styleboxData = data.styleData;
            if (Status === "Success") {
                $(".modal-title").html("<label style='color: green'>Update Style Box</label>");
                $(".modal-body").html("<div class='row'><div class='col-md-12'><p id='styleboxError'" +
                " style='color:red'></p><div class='col-md-6 form-group'><label>Title</label>" +
                "<input type='text' class='form-control' id='e_title' value='"+styleboxData.title+"' " +
                "placeholder='Enter Title' /></div><div class='col-md-6 form-group'><label>Price</label>" +
                "<input type='text' id='e_price' class='form-control' placeholder='Enter Price in USD' " +
                "value='"+styleboxData.price+"' /></div><div class='col-md-12 form-group'><label>Description</label>" +
                "<textarea name='editor1' rows='2' placeholder='Enter Description of StyleBox' class='form-control' " +
                "id='editor1'>"+styleboxData.description+"</textarea></div><div class='col-md-6 form-group'>" +
                "<label>StyleBox Image</label><br><img src='api/Files/images/stylebox/"+styleboxData.image+"' " +
                "alt='StyleBox Image' class='old_stylebox_img' /><input type='file' name='styleboxfile' " +
                "id='stylebox_file2' onchange=readURL2(this,'old_stylebox_img') class='filebtn' /></div>" +
                "<div class='col-md-12'><input type='button' class='btn btn-danger pull-right' " +
                "style='margin-top:30px' onclick=loadTextArea('"+styleboxData.u_id+"') id='subbtn' value='Update'/>" +
                "</div></div><p class='percentprog' style='color:green;text-align:center'></div>");
                $(".modal-footer").css('display', 'none');
                CKEDITOR.replace( 'editor1' );
                $("#myModal").modal("show");
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function loadTextArea(u_id){
        $(".loaderdiv").addClass("block");
        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].updateElement();
        }
        setTimeout(function(){
            if(u_id === ""){
                addStyleboxFinal()
            }else {
                updateStylebox(u_id)
            }
        },1000);
    }
    function updateStylebox(u_id){
        var e_title = $("#e_title").val();
        var e_price = $("#e_price").val();
        var e_description = $("#editor1").val();
        if(e_title === "" || e_price === "" || e_description === "" ){
            $("#styleboxError").html("please fill all required fields");
            $("#styleboxError").css("color","red");
            return false;
        }
        var data = new FormData();
        var ImageChanged = "no";
        var stylebox_file2 = $("#stylebox_file2").val();
        if(stylebox_file2 !== ""){
            ImageChanged = "yes";
            var stylebox_file_obj = document.getElementById('stylebox_file2');
            var stylebox_file_ext = stylebox_file2.split(".");
            stylebox_file_ext = stylebox_file_ext[stylebox_file_ext.length - 1];
            if (stylebox_file_ext === "jpg" || stylebox_file_ext === "png" || stylebox_file_ext === "gif" || stylebox_file_ext === "jpeg") {
                data.append('stylebox_file2', stylebox_file_obj.files[0]);
            }
            else{
                $("#styleboxError").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#styleboxError").css("color","red");
            }
        }
        data.append('type', "updateStylebox");
        data.append('title', e_title);
        data.append('description', e_description);
        data.append('price', e_price);
        data.append('u_id', u_id);
        data.append('ImageChanged', ImageChanged);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState === 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status === "Success"){
                    $("#styleboxError").html("");
                    $(".loaderdiv").addClass("loaderhidden");
                    location.reload();
                }
                else{
                    $("#styleboxError").html(response.Message);
                    $(".loaderdiv").addClass("loaderhidden");
                    return false;
                }
            }
        };
        request.open('POST', 'api/userProcess.php');
        request.send(data);
    }
    function confirmDelete_stylebox(id) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
        'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
        'onclick=delStylebox("'+id+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function delStylebox(id) {
        var url = "api/userProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $.post(url, {"type": "deleteStylebox", "u_id": id}, function (data) {
            var Status = data.Status;
            if (Status === "Success") {
                $("#myModal").modal("hide");
                getStyleboxData();
            }
            else{
                showMessage(Message,"red");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
</script>
