<?php include("header.php");?>
	<div class="banner top-canvasbanners">
		<div class="">
		</div>
	</div>
	<!-- //Slider -->
</div>
	<!-- //Header -->
<div class="about-bottom wthree-3">
	<div class="container">
		<div class="col-md-12"><h2 class="tittle">YOUR SUIT IS YOUR CANVAS</h2>
			<p>The quality of your suit doesn't just depend on fit and choice of fabric, but also on the way it is constructed. Real beauty comes from within and in the case of your suit that depends on it being "canvassed". </p></br>
			
			<p>What does that mean? A canvassed suit jacket has layers of canvassing material, generally wool and horsehair, that sit between the outer suit fabric and the inner lining. While this canvas is hidden from view, it plays an important role in keeping the balance, structure and shape of the jacket.</p></br>
			<p><b>How does SF Tailors construct your jacket? </b>Your wish is our command. From fabric, to buttons, style and even construction, the choice is yours! We offer:</p>
			<ul>
				<li>1. Full-canvas suit jackets</li>
				<li>2. Fused suit jackets</li>
				<li>3. Half-canvas suit jackets</li>
				<li>4. Deconstructed suit jackets</li>
			</ul>
			</br>
			<p><b>Let's explore the difference between all these jackets.</b></p></br>
			<img src="images/tab/suits_canvas_types.jpg" class="img-responsive">
		</div>
			
	</div>
</div>
<?php include("footer.php");?>