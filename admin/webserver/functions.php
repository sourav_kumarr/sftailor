<?php
function RequiredFields($getvars, $requiredfields) 
{
	$error = "";
	$keys = array_keys($getvars);
	if(count($getvars) == count($requiredfields))
	{
		for($i=0;$i<count($requiredfields);$i++) 
		{
			$feild = $requiredfields[$i];
			if(in_array($feild, $keys))
			{
				if($getvars[$feild]=="") 
				{
					$error = $error.$feild.",";
				}
			}
			else
			{
				$error = $error.$feild.",";
			}
		}
		$error = trim($error);
	}
	else
	{
		for($i=0;$i<count($requiredfields);$i++) 
		{
			$feild = $requiredfields[$i];
			if(in_array($feild, $keys))
			{
				if($getvars[$feild]=="") 
				{
					$error = $error.$feild.",";
				}
			}
			else
			{
				$error = $error.$feild.",";
			}
		}
		$error = trim($error);
	}
	if($error != "")
	{
		$error = rtrim($error,",");
		errorMessage(errorCode::$generic_param_missing.$error,errorCode::$generic_param_missing_code);
		return 0;
	}
	return 1;
}
function errorMessage($message,$errorcode =1) {
	echo json_encode(array(
		'status' => "error",
		'message' => $message
	));
} 
?>