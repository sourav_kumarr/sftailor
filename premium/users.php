<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();
$allUsersData = $userClass->getAllUsersData();
?>
<style>.shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .hideloader {
        display: none;
    }
    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
#create_stores{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
@media print {
  body * {
    visibility: hidden;
  }
  #section-to-print, #section-to-print * {
    visibility: visible;
  }
  #section-to-print {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>
<!-- page content -->
<div id="loadd" class="loaderr hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Orders <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>

					
					
					
			<?php if($_REQUEST["type"] =="edit"){  ?>
                <div class="shadoows" onclick="close_diolog()" style="display: block;"></div>
                    <div class="col-md-12 create_stores" id="create_stores" style="padding: 0;display: block;" >
                        <?php
                        $link = $conn->connect();//for sftailor
                        if ($link) {
                            $userId = $_REQUEST['userId'];
                            $query = "select * from wp_customers where user_id='$userId'";
                            $result = mysqli_query($link, $query);
                            if ($result) {
                                $num = mysqli_num_rows($result);
                                if ($num > 0) {
                                    $orderDatas = mysqli_fetch_assoc($result);
                                    $name = $orderDatas['name'];
                                    $fname = explode(" ",$name)[0];
                                    $lname = explode(" ",$name)[1];
                                }
                            }
                        }
                        ?>
                        <form method="Post" enctype="multipart/form-data" id="update_user">
						<div class="form-group col-md-4">
                            <label>First Name</label>
                            <input type="text"  name="user_fname" class="form-control" value="<?php echo $fname;?>" placeholder="Enter Your First Name" required="true"  />
                        </div>
						<div class="form-group col-md-4">
                            <label>Last Name</label>
                            <input type="text"  name="user_lname" class="form-control" value="<?php echo $lname;?>" placeholder="Enter Your Last Name" required="true"  />
                        </div>
                            <input type="hidden" name="userId" value="<?php echo $userId;?>">
                            <div class="form-group col-md-4">
                                <label>E-Mail</label>
                                <input type="text" name="user_email" class="form-control" value="<?php echo $orderDatas['email'];?>" placeholder="Enter Your E-Mail Address" required="true"/>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Password</label>
                                <input type="text" name="user_password" class="form-control" value="<?php echo $orderDatas['password'];?>" placeholder="Enter Your Password"required="true" />
                                <input type="hidden" name="store_status" value="0">
                            </div>
                           
                            <div class="form-group col-md-4">
                                <label>Contact</label>
                                <input type="text" name="contact_name" class="form-control" value="<?php echo $orderDatas['mobile'];?>"   placeholder="Enter Your Contact Number" required="true"/>
                            </div>
                            <div style='clear:both' ></div>

                            <div class="form-group">
                                <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Update User Info</button>
                            </div>
                        </form>
                    </div>
                    <?php } ?>
				    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Available Users
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Profile Pic</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Email</th>
                                <th>Password</th>
                                <th>Contact</th>
                                <th>Gender</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
								$allusersData = $allUsersData['usersData'];
								for($i=0;$i<count($allusersData);$i++){
                                ?>
								<tr>
									<td data-title='#'><?php echo $j ?></td>
									<td><img src="<?php echo $allusersData[$i]['profile_pic']; ?>" style="width:20px;"></td>
									<td><?php echo explode(" ",$allusersData[$i]['name'])[0]; ?></td>
									<td><?php echo explode(" ",$allusersData[$i]['name'])[1]; ?></td>
									<td><?php echo $allusersData[$i]['email']; ?></td>
									<td><?php echo $allusersData[$i]['password']; ?></td>
									<td><?php echo $allusersData[$i]['mobile']; ?></td>
									<td><?php echo $allusersData[$i]['gender']; ?></td>
									<td>
										<a href='users.php?type=edit&userId=<?php echo $allusersData[$i]['user_id'];?>' style="float: right; font-size: 18px; padding: 2px 4px; background: tomato none repeat scroll 0% 0%; color: white;"><i class="fa fa-pencil"></i></a>
										<a href='#' onclick=delUser('<?php echo $allusersData[$i]['user_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>
									</td>
								</tr>
							<?php } ?>				
                                          
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
$(document).ready(function () {
    $('#orderTable').DataTable({});
});
function delUser(user_id){
    var url = "api/userProcess.php";
    $.post(url,{"type":"deleteUser","user_id":user_id}, function (data) {
        var Status = data.Status;
        if (Status == "Success"){
            window.location="users.php";
        }
    });
}
$("#update_user").submit(function(e){
	e.preventDefault();
	$('.loader').fadeIn("slow");
	$.ajax({
		url: "update_users.php?type=update",
		type: "POST",
		data: new FormData(this),
		contentType: false,
		cache: false,
		processData:false,
		success: function(data)
		{
			$('.loader').fadeOut("slow");
			var json = $.parseJSON(data)
			{
				var status = json.status;
				var message = json.message;
				if(status == "done"){
					alert(message);
					window.location="users.php";
				}
				else{
					alert(message);
				}
			}
		}
	});
});

function close_diolog() {
	window.location = "users.php"
}


	
</script>
