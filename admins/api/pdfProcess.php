<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:42 PM
 */

require_once('Classes/PDF.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$pdfClass = new \Classes\PDF();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
 if($response['Status'] == 'Failure'){
     $pdfClass->apiResponse($response);
    return false;
 }
 $type = $_REQUEST['type'];
 if($type === 'createPdf') {

     $file_type = $_REQUEST['file_type'];
     $pdfResponse = $pdfClass->save($file_type);
     $pdfClass->apiResponse($pdfResponse);
 }
 else if($type === 'getAllPDF') {
     $pdfResponse = $pdfClass->getAllPDF();
     $pdfClass->apiResponse($pdfResponse);
 }
 else if($type === 'deleteData') {
     $requiredfields = array('id','path');
     $response = RequiredFields($_POST, $requiredfields);
     if($response['Status'] == 'Failure'){
         $pdfClass->apiResponse($response);
         return false;
     }
     $id = $_REQUEST['id'];
     $path = $_REQUEST['path'];

     $pdfClass->setPdfId($id);
     $pdfClass->setPdfFile($path);

     $pdfResponse = $pdfClass->deletePDF();
     $pdfClass->apiResponse($pdfResponse);
 }

else if($type === 'update_tab') {
    $update_videos = $_REQUEST['update_videos'];
    $update_images = $_REQUEST['update_images'];
    $update_pdf = $_REQUEST['update_pdf'];
    $pdfResponse = $pdfClass->update_menus($update_videos,$update_images,$update_pdf);
    $pdfClass->apiResponse($pdfResponse);
}

?>