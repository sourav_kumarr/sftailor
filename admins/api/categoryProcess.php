<?php
require_once ('Classes/CATEGORY.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$catClass = new \Classes\CATEGORY();
$requiredfields = array('type');

($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $catClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addCategory")
{
    $requiredfields = array('coupon_name','coupon_condition','dataType');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_type = trim($_POST['dataType']);
    $coupon_name = trim($_POST['coupon_name']);
    $coupon_condition = trim($_POST['coupon_condition']);
    if($coupon_condition == "discount"){
        $requiredfields = array('coupon_discount','coupon_for');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $catClass->apiResponse($response);
            return false;
        }
        $coupon_discount = trim($_POST['coupon_discount']);
        $coupon_for = trim($_POST['coupon_for']);

        $freebies_item_with = "";
        $freebies_item = "";
    }
    else if($coupon_condition == "freebies"){
        $requiredfields = array('freebies_item_with','freebies_item');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $catClass->apiResponse($response);
            return false;
        }
        $coupon_discount = "";
        $coupon_for = "";
        $freebies_item_with = trim($_POST['freebies_item_with']);
        $freebies_item = trim($_POST['freebies_item']);
    }



    ($response = $catClass->checkCategoryExistence($coupon_name));
    if($response[STATUS] == Success) {
        $response = $catClass->addCategory($cat_type,$coupon_name,$coupon_condition,$coupon_discount,$freebies_item_with,$freebies_item,
            $coupon_for);
    }
    if($response[STATUS] == Error){
        $catClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $catClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    unset($response['catId']);
    $catClass->apiResponse($response);
}
else if($type == "editCategory"){
    $requiredfields = array('cat_price','cat_status','cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_name = trim($_POST['cat_name']);
    $cat_status = trim($_POST['cat_status']);
    $cat_id = trim($_POST['cat_id']);
    $cat_price = trim($_POST['cat_price']);

    $response = $catClass->editCategory($cat_status,$cat_id,$cat_price,$cat_name);
    if($response[STATUS] == Error){
        $catClass->apiResponse($response);
        return false;
    }
    $catId = $response['catId'];
    $temp = $catClass->getParticularCatData($catId);
    $response['catData'] = $temp['catData'];
    unset($response['catId']);
    $catClass->apiResponse($response);
}
else if($type == "updateInfinity"){
    $requiredfields = array('cat_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->updateInfinity($cat_id,$value);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type == "updateCouponFor"){
    $requiredfields = array('cat_id','coupon_condition');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $coupon_condition = $_REQUEST['coupon_condition'];
    if($coupon_condition == "discount"){
        $requiredfields = array('coupon_discount','coupon_for');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $catClass->apiResponse($response);
            return false;
        }
        $freebies_item_with = "";
        $freebies_item = "";
        $coupon_discount = $_REQUEST['coupon_discount'];
        $coupon_for = $_REQUEST['coupon_for'];

    }
    else
    {
        $requiredfields = array('freebies_item_with','freebies_item');
        $response = RequiredFields($_POST, $requiredfields);
        if($response['Status'] == 'Failure'){
            $catClass->apiResponse($response);
            return false;
        }
        $coupon_discount = "";
        $coupon_for = "";
        $freebies_item_with = $_REQUEST['freebies_item_with'];
        $freebies_item = $_REQUEST['freebies_item'];
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->updateCoupon($cat_id,$coupon_condition,$coupon_discount,$coupon_for,$freebies_item_with,$freebies_item);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type == "getCategories")
{
    $response = $catClass->getAllCategories();
    $catClass->apiResponse($response);
}
else if($type == "getCategory"){
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->getParticularCatData($cat_id);
    $catClass->apiResponse($response);
}
else if($type == "statusChange")
{
    $requiredfields = array('cat_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->statusChange($cat_id,$value);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type == "deleteCategory")
{
    $requiredfields = array('cat_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $response = $catClass->deleteCategory($cat_id);
    if($response[STATUS] == Error) {
        $catClass->apiResponse($response);
        return false;
    }
    $catClass->apiResponse($response);
}
else if($type === "priceRange") {
    $requiredfields = array('cat_id','start','end');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $catClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['cat_id'];
    $start = $_REQUEST['start'];
    $end = $_REQUEST['end'];


    $responsee = $catClass->updateRange($cat_id,$start,$end);

    $catClass->apiResponse($responsee);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $catClass->apiResponse($response);
}
?>