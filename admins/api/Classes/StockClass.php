<?php
/**
 * Created by PhpStorm.
 * User: Sourav
 * Date: 9/11/2017
 * Time: 3:37 PM
 */

namespace Classes;


class StockClass
{
    private $validData = array();
    private $connect = null;
    private $totalStock ;
    private $stockSatus ;
    private $stockType ;
    private $cameo_code ;
    private $rc_code ;
    private $recmtm ;
    private $yarn ;
    private $weight ;
    private $catalog ;
    private $pattern ;
    private $attribute;
    private $color ;
    private $us_dollar ;
    private $id;
    private $type;

    public function __construct()
    {

        $this->connect = new CONNECT();
        $this->validData = array('2017 New','NO.','RC#','Stock','Under Inspection','Total Stock','Note','Next Arriving Date');
    }

    public function __call($function, $args)
    {
        $functionType = strtolower(substr($function, 0, 3));
        $propName = lcfirst(substr($function, 3));
        switch ($functionType) {
            case 'get':
                if (property_exists($this, $propName)) {
                    return $this->$propName;
                }
                break;
            case 'set':
                if (property_exists($this, $propName)) {
                    $this->$propName = $args[0];
                }
                break;
        }
    }


    /**
     * @param mixed $type
     */
    public function setTotalStock($type)
    {
        $this->totalStock = $type;
    }
    public function setStockSatus($type)
    {
        $this->stockSatus = $type;
    }
    public function setStockType($type)
    {
        $this->stockType = $type;
    }
    public function setType($type)
    {
        $this->type = $type;
    }
    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
    /**
     * @param mixed $cameo_code
     */
    public function setCameoCode($cameo_code)
    {
        $this->cameo_code = $cameo_code;
    }

    /**
     * @param mixed $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * @param mixed $catalog
     */
    public function setCatalog($catalog)
    {
        $this->catalog = $catalog;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @param mixed $rc_code
     */
    public function setRcCode($rc_code)
    {
        $this->rc_code = $rc_code;
    }

    /**
     * @param mixed $pattern
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;
    }

    /**
     * @param mixed $us_dollar
     */
    public function setUsDollar($us_dollar)
    {
        $this->us_dollar = $us_dollar;
    }

    /**
     * @param mixed $weight
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    }

    /**
     * @param mixed $yarn
     */
    public function setYarn($yarn)
    {
        $this->yarn = $yarn;
    }

    /**
     * @param mixed $recmtm
     */
    public function setRecmtm($recmtm)
    {
        $this->recmtm = $recmtm;
    }

    public function validExcelSheet($data) {
        $response = array();
        if(count($data->sheets)>1) {
            $response[Status] = Error;
            $response[Message] = "Excel sheet should contain only one sheet";
            return $response;

        }

        if(count($data->sheets[0]['cells'])>0) // checking sheet not empty
        {
            $header = $data->sheets[0]['cells'][1];

            if(count($header) == 0 || count($header)< count($this->validData)) {
                $response[Status] = Error;
                $response[Message] = "Please attach valid params to your excel sheet ";
                $response["validParams"] = implode(',',$this->validData);
                return $response;
            }
            for($j=1;$j<=count($data->sheets[0]['cells']);$j++) // loop used to get each row of the sheet
            {

                for($k=1;$k<count($header);$k++) {
                    if($j == 1 )  {
                        $item = $header[$k];

                        if(!in_array(trim($item),$this->validData)) {
                            $response[Status] = Error;
                            $response[Message] = "Excel sheet should contain valid params. Invalid param found in excel : ".$item;
                            $response["validParams"] = implode(',',$this->validData);
                            $response["items"] = $item;

                            return $response;
                        }
                    }
                    else{
                        break;
                    }
                }
            }
        }
        $response[Status] = Success;
        $response[Message] = "Excel sheet is ok.";

        return $response;
    }

    public function validCameoCode($code) {
        $link = $this->connect->connect();
        $response = array();
        if($link) {
            $query = "select * from fabric where fabric_cameo='$code'";
            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_num_rows($res);
                if($num>0) {
                    $response[Status] = Error;
                    $response[Message] = "Fabric already exists with this cameo code. ".$code;
                    return $response;
                }
            }
            $response[Status] = Success;
            $response[Message] = "New Fabric found";
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }
    public function validCameoCodeShirt($code) {
        $link = $this->connect->connect();
        $response = array();
        if($link) {
            $query = "select * from fabric where fabric_rc_code='$code'";
            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_num_rows($res);
                if($num>0) {
                    $response[Status] = Error;
                    $response[Message] = "Fabric already exists with this cameo code. ".$code;
                    return $response;
                }
            }
            $response[Status] = Success;
            $response[Message] = "New Fabric found";
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }

    public function getFabricData() {
        $link = $this->connect->connect();
        $response = array();
        if($link) {
            $query = "select * from fabric where fabric_type='$this->type'";

            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_num_rows($res);
                if($num>0) {
                    $data = array();
                    while($rows = mysqli_fetch_array($res)) {

                        $data [] = array("fabric_id"=>$rows['fabric_id'],
                            "fabric_cameo"=>$rows['fabric_cameo'],
                            "fabric_rc_code"=>$rows['fabric_rc_code'],
                            "fabric_rcmtm_composition"=>$rows['fabric_rcmtm_composition'],
                            "fabric_yarn"=>$rows['fabric_yarn'],
                            "fabric_weight"=>$rows['fabric_weight'],
                            "fabric_catalog"=>$rows['fabric_catalog'],
                            "fabric_pattern"=>$rows['fabric_pattern'],
                            "fabric_attribute"=>$rows['fabric_attribute'],
                            "fabric_color"=>$rows['fabric_color'],
                            "fabric_price"=>$rows['fabric_price'],
                            "fabric_retail_price"=>$rows['fabric_retail_price'],
                            "fabric_type"=>$rows['fabric_type']);
                    }
                    $response[Status] = Success;
                    $response[Message] = "Fabric data found ";
                    $response["data"] = $data;

                    return $response;
                }
            }
            $response[Status] = Error;
            $response[Message] = "No Fabric found";
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }

    public function searchFabricData() {
        $link = $this->connect->connect();
        $response = array();
        if($link) {

            $query = "select * from fabric where fabric_cameo like '%".$this->cameo_code."%'";
            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_num_rows($res);
                if($num>0) {
                    $data = array();
                    while($rows = mysqli_fetch_array($res)) {

                        $data [] = array("fabric_id"=>$rows['fabric_id'],
                            "fabric_code"=>$rows['fabric_cameo']);
                    }
                    $response[Status] = Success;
                    $response[Message] = "Fabric data found ";
                    $response["data"] = $data;

                    return $response;
                }
                else{
                    $query = "select * from fabric where fabric_rc_code like '%".$this->cameo_code."%'";
                    $res = mysqli_query($link,$query);

                    $data = array();
                    while($rows = mysqli_fetch_array($res)) {

                        $data [] = array("fabric_id"=>$rows['fabric_id'],
                            "fabric_code"=>$rows['fabric_rc_code']);
                    }
                    $response[Status] = Success;
                    $response[Message] = "Fabric data found ";
                    $response["data"] = $data;

                    return $response;
                }
            }
            $response[Status] = Error;
            $response[Message] = "No Fabric found";
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }

    public function searchFabricData1() {
        $link = $this->connect->connect();
        $response = array();
        if($link) {

            $query = "select * from fabric where fabric_cameo like '%".$this->cameo_code."%' or fabric_rc_code like '%".$this->cameo_code."%'";
            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_num_rows($res);
                if($num>0) {
                    $data = array();
                    while($rows = mysqli_fetch_assoc($res)) {

                        $data [] = $rows;
                    }
                    $response[Status] = Success;
                    $response[Message] = "Fabric data found ";
                    $response["data"] = $data;

                    return $response;
                }

                $response[Status] = Error;
                $response[Message] = "No data found";
                return $response;
            }
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }


    public function updateFabric() {
        $link = $this->connect->connect();
        if($link) {

            $query = "update fabric  set fabric_rcmtm_composition='$this->recmtm', fabric_yarn='$this->yarn', fabric_weight='$this->weight', fabric_catalog='$this->catalog', fabric_pattern='$this->pattern', fabric_attribute='$this->attribute', fabric_color='$this->color', fabric_price='$this->us_dollar'".
                " where fabric_id='$this->id'";

            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_affected_rows($link);
                if($num>0) {
                    $response[Status] = Success;
                    $response[Message] = "Fabric updated successfully ";
                }
                else{
                    $response[Status] = Error;
                    $response[Message] = "No changes found in record";
                }
                return $response;
            }
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;

        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }

    }

    public function deleteFabric() {
        $link = $this->connect->connect();
        if($link) {

            $query = "delete from fabric where fabric_id='$this->id'";
            $res = mysqli_query($link,$query);
            if($res) {
                $num = mysqli_affected_rows($link);
                if($num>0) {
                    $response[Status] = Success;
                    $response[Message] = "Fabric updated successfully ";
                }
                else{
                    $response[Status] = Error;
                    $response[Message] = "Unable to delete fabric";
                }
                return $response;
            }
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
        else{
            $response[Status] = Error;
            $response[Message] = mysqli_error($link);
            return $response;
        }
    }
    public function addStock() {
        $link = $this->connect->connect();
        if($link) {
            if($this->stockType == "Shirt"){
                $validCode =  $this->validCameoCodeShirt($this->cameo_code);
            }
            else{
                $validCode =  $this->validCameoCode($this->cameo_code);
            }

            if($validCode[Status] == Error) {
                if($this->stockType == "Shirt"){
                    $query = "update fabric set total_stock = '$this->totalStock', stock_status='$this->stockSatus' where
                    fabric_rc_code ='$this->cameo_code'";
                }
                else{
                    $query = "update fabric set total_stock = '$this->totalStock', stock_status='$this->stockSatus' where
                    fabric_cameo ='$this->cameo_code'";
                }
                $res = mysqli_query($link,$query);
                if($res) {
                    $num = mysqli_affected_rows($link);
                    if($num>0) {
                        return true;
                    }
                    else{
                        return "unable to upload data ".$query;
                    }
                    return $response;
                }

            }
            return mysqli_error($link);

        }
        else{
            return mysqli_error($link);
        }
    }
}