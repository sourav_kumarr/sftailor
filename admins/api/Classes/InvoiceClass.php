<?php
namespace Classes;
class InvoiceClass{
    private $connect = null;
    private $inv_id = "";
    private $inv_number = "";
    private $inv_date = "";
    private $inv_due_date = "";
    private $inv_footer = "";
    private $inv_notes = "";
    private $inv_title = "";
    private $inv_summary = "";
    private $contact_customer = "";
    private $contact_email = "";
    private $contact_phone = "";
    private $contact_contact = "";
    private $billing_add_1 = "";
    private $billing_add_2 = "";
    private $billing_city = "";
    private $billing_postal = "";
    private $billing_country = "";
    private $billing_state = "";
    private $shipping_shiptocontact = "";
    private $shipping_phone = "";
    private $shipping_add_1 = "";
    private $shipping_add_2 = "";
    private $shipping_city = "";
    private $shipping_postal = "";
    private $shipping_country = "";
    private $shipping_state = "";
    private $more_acc_number = "";
    private $more_fax = "";
    private $more_mobile = "";
    private $more_website = "";
    private $more_tollfree= "";
    private $cust_status = "";
    private $inv_type = "";
    private $items= "";
    private $response = array();
    public function __construct(){
        $this->connect = new CONNECT();
    }
    public function __call($function, $args){
        $functionType = strtolower(substr($function, 0, 3));
        $propName = lcfirst(substr($function, 3));
        switch ($functionType) {
            case 'get':
                if (property_exists($this, $propName)) {
                    return $this->$propName;
                }
                break;
            case 'set':
                if (property_exists($this, $propName)) {
                    $this->$propName = $args[0];
                }
                break;
        }
    }
    public function setConnect($connect){
        $this->connect = $connect;
    }
    public function getConnect(){
        return $this->connect;
    }
    public function setInvId($inv_id){
        $this->inv_id = $inv_id;
    }
    public function setInvTitle($inv_title){
        $this->inv_title = $inv_title;
    }
    public function setInvSummary($inv_summary){
        $this->inv_summary = $inv_summary;
    }
    public function setInvNumber($inv_number){
        $this->inv_number = $inv_number;
    }
    public function setInvDate($inv_date){
        $this->inv_date = $inv_date;
    }
    public function setInvDueDate($inv_due_date){
        $this->inv_due_date = $inv_due_date;
    }
    public function setInvNotes($inv_notes){
        $this->inv_notes = $inv_notes;
    }
    public function setInvFooter($inv_footer){
        $this->inv_footer = $inv_footer;
    }
    public function setItems($items){
        $this->items = $items;
    }
    ///////contact part start here///////////////
    public function setContactCustomer($contact_customer){
        $this->contact_customer = $contact_customer;
    }
    public function setContactEmail($contact_email){
        $this->contact_email = $contact_email;
    }
    public function setContactPhone($contact_phone){
        $this->contact_phone = $contact_phone;
    }
    public function setContactContact($contact_contact){
        $this->contact_contact = $contact_contact;
    }
    ///////billing part start here //////////////
    public function setBillingAdd1($billing_add_1){
        $this->billing_add_1 = $billing_add_1;
    }
    public function setBillingAdd2($billing_add_2){
        $this->billing_add_2 = $billing_add_2;
    }
    public function setBillingCountry($billing_country){
        $this->billing_country = $billing_country;
    }
    public function setBillingState($billing_state){
        $this->billing_state = $billing_state;
    }
    public function setBillingCity($billing_city){
        $this->billing_city = $billing_city;
    }
    public function setBillingPostal($billing_postal){
        $this->billing_postal = $billing_postal;
    }
    ///shipping setter start here////
    public function setShippingShiptocontact($shipping_shiptocontact){
        $this->shipping_shiptocontact = $shipping_shiptocontact;
    }
    public function setShippingPhone($shipping_phone){
        $this->shipping_phone = $shipping_phone;
    }
    public function setShippingAdd1($shipping_add_1){
        $this->shipping_add_1 = $shipping_add_1;
    }
    public function setShippingAdd2($shipping_add_2){
        $this->shipping_add_2 = $shipping_add_2;
    }
    public function setShippingCountry($shipping_country){
        $this->shipping_country = $shipping_country;
    }
    public function setShippingState($shipping_state){
        $this->shipping_state = $shipping_state;
    }
    public function setShippingCity($shipping_city){
        $this->shipping_city = $shipping_city;
    }
    public function setShippingPostal($shipping_postal){
        $this->shipping_postal = $shipping_postal;
    }
    ////////////////more part start here
    public function setMoreAccNumber($more_acc_number){
        $this->more_acc_number = $more_acc_number;
    }
    public function setMoreFax($more_fax){
        $this->more_fax = $more_fax;
    }
    public function setMoreMobile($more_mobile){
        $this->more_mobile = $more_mobile;
    }
    public function setMoreTollfree($more_tollfree){
        $this->more_tollfree = $more_tollfree;
    }
    public function setMoreWebsite($more_website){
        $this->more_website = $more_website;
    }
    /////////common part start here////////////
    public function setCustStatus($cust_status){
        $this->cust_status = $cust_status;
    }
    public function setInvType($inv_type){
        $this->inv_type = $inv_type;
    }
    public function saveInvoice() {
        $link = $this->getConnect()->connect();
        if($link) {
            $query="insert into wp_invoice(inv_number,inv_date,inv_due_date,inv_footer,inv_notes,inv_title,inv_summary,
            contact_customer,contact_email,contact_phone,contact_contact,billing_add_1,billing_add_2,billing_city,
            billing_postal,billing_country,billing_state,shipping_shiptocontact,shipping_phone,shipping_add_1,
            shipping_add_2,shipping_city,shipping_postal,shipping_country,shipping_state,more_acc_number,more_fax,
            more_mobile,more_website,more_tollfree,cust_status,inv_type)values('$this->inv_number','$this->inv_date',
            '$this->inv_due_date','$this->inv_footer','$this->inv_notes','$this->inv_title','$this->inv_summary',
            '$this->contact_customer','$this->contact_email','$this->contact_phone','$this->contact_contact',
            '$this->billing_add_1','$this->billing_add_2','$this->billing_city','$this->billing_postal',
            '$this->billing_country','$this->billing_state', '$this->shipping_shiptocontact','$this->shipping_phone',
            '$this->shipping_add_1','$this->shipping_add_2','$this->shipping_city','$this->shipping_postal',
            '$this->shipping_country','$this->shipping_state','$this->more_acc_number','$this->more_fax',
            '$this->more_mobile', '$this->more_website','$this->more_tollfree','$this->cust_status','$this->inv_type')";
            $result =mysqli_query($link,$query);
            $inv_id = mysqli_insert_id($link);
            if($result) {
                $items = json_decode($this->items);
                for($i=0;$i<count($items);$i++) {
                    $item_name = $items[$i]->item_name;
                    $item_name_other = $items[$i]->item_name_other;
                    $item_desc = $items[$i]->item_desc;
                    $item_price = $items[$i]->item_price;
                    $item_quantity = $items[$i]->item_quantity;
                    $total_without_tax = $items[$i]->total_without_tax;
                    $tax_amount = $items[$i]->tax_amount;
                    $tax_perc = $items[$i]->tax_perc;
                    $total_with_tax = $items[$i]->total_with_tax;
                    $item_query = "insert into wp_invoice_items(item_name,item_name_other,item_desc,item_quantity,item_price,item_total,
                    item_tax,total_with_tax,item_inv_id,tax_perc) values('$item_name','$item_name_other','$item_desc','$item_quantity','$item_price',
                    '$total_without_tax','$tax_amount','$total_with_tax','$inv_id','$tax_perc')";
                    mysqli_query($link, $item_query);
                }
                $this->response[STATUS] = Success;
                $this->response[Message] = "Invoice added successfully";
                $this->response['inv_id'] = $inv_id;
                return $this->response;
            }
            else{
                $this->response[STATUS] = Error;
                $this->response[Message] = $this->getConnect()->sqlError();
                return $this->response;
            }
        }
        $this->response[STATUS] = Error;
        $this->response[Message] = $this->getConnect()->sqlError();
        return $this->response;
    }
    public function updateInvoice() {
        $link = $this->getConnect()->connect();
        if($link) {
            $query="update wp_invoice set inv_number='$this->inv_number', inv_date = '$this->inv_date',
            inv_due_date = '$this->inv_due_date', inv_footer = '$this->inv_footer', inv_notes = '$this->inv_notes',
            inv_title='$this->inv_title',inv_summary='$this->inv_summary',contact_customer='$this->contact_customer',
            contact_email='$this->contact_email',contact_phone='$this->contact_phone',contact_contact='$this->contact_contact',
            billing_add_1='$this->billing_add_1',billing_add_2 = '$this->billing_add_2', billing_city='$this->billing_city',
            billing_postal='$this->billing_postal',billing_country='$this->billing_country',billing_state='$this->billing_state',
            shipping_shiptocontact = '$this->shipping_shiptocontact',shipping_phone = '$this->shipping_phone',
            shipping_add_1='$this->shipping_add_1',shipping_add_2='$this->shipping_add_2', shipping_city = '$this->shipping_city',
            shipping_postal = '$this->shipping_postal', shipping_country = '$this->shipping_country', 
            shipping_state = '$this->shipping_state',more_acc_number='$this->more_acc_number',more_fax='$this->more_fax',
            more_mobile = '$this->more_mobile',more_website='$this->more_website',more_tollfree='$this->more_tollfree',
            cust_status='$this->cust_status',inv_type='$this->inv_type' where inv_id='$this->inv_id'";
            $result =mysqli_query($link,$query);
            if($result) {
                mysqli_query($link,"delete from wp_invoice_items where item_inv_id='$this->inv_id'");
                $items = json_decode($this->items);
                for($i=0;$i<count($items);$i++) {
                    $item_name = $items[$i]->item_name;
                    $item_name_other = $items[$i]->item_name_other;
                    $item_desc = $items[$i]->item_desc;
                    $item_price = $items[$i]->item_price;
                    $item_quantity = $items[$i]->item_quantity;
                    $total_without_tax = $items[$i]->total_without_tax;
                    $tax_amount = $items[$i]->tax_amount;
                    $tax_perc = $items[$i]->tax_perc;
                    $total_with_tax = $items[$i]->total_with_tax;
                    $item_query = "insert into wp_invoice_items(item_name,item_name_other,item_desc,item_quantity,item_price,item_total,
                    item_tax,tax_perc,total_with_tax,item_inv_id) values('$item_name','$item_name_other','$item_desc','$item_quantity','$item_price',
                    '$total_without_tax','$tax_amount','$tax_perc','$total_with_tax','$this->inv_id')";
                    mysqli_query($link,$item_query);
                }
                $this->response[STATUS] = Success;
                $this->response[Message] = "Invoice added successfully";
                return $this->response;
            }
        }
        $this->response[STATUS] = Error;
        $this->response[Message] = $this->getConnect()->sqlError();
        return $this->response;
    }
    public function getAllInvoices() {
        $link = $this->connect->connect();
        if($link){
            $customers = array();
            $custResponse = $this->allDistinctUsers();
            if($custResponse[Status] == "Success"){
                $customers = $custResponse['customers'];
                $this->response['customers'] = $customers;
            }
            $query = "select * from wp_invoice where cust_status = '$this->cust_status'  ORDER  BY inv_id DESC";
            if($this->cust_status === 'all'){
                $query = "select * from wp_invoice ORDER  BY inv_id DESC";
            }
            $result  = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $Invoices = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $inv_id = $rows["inv_id"];
                        $inv_items = array();
                        $itemsResponse = $this->getInvoiceItems($inv_id);
                        if($itemsResponse[Status] == Success) {
                            $inv_items = $itemsResponse['inv_items'];
                        }
                        $Invoices[] = array(
                            "inv_id"=>$rows["inv_id"],
                            "inv_number"=>$rows["inv_number"],
                            "inv_date"=>$rows["inv_date"],
                            "inv_due_date"=>$rows["inv_due_date"],
                            "inv_footer"=>$rows["inv_footer"],
                            "inv_notes"=>$rows["inv_notes"],
                            "inv_title"=>$rows["inv_title"],
                            "inv_summary"=>$rows["inv_summary"],
                            "contact_customer"=>$rows["contact_customer"],
                            "contact_email"=>$rows["contact_email"],
                            "contact_phone"=>$rows["contact_phone"],
                            "contact_contact"=>$rows["contact_contact"],
                            "billing_add_1"=>$rows["billing_add_1"],
                            "billing_add_2"=>$rows["billing_add_2"],
                            "billing_city"=>$rows["billing_city"],
                            "billing_postal"=>$rows["billing_postal"],
                            "billing_country"=>$rows["billing_country"],
                            "billing_state"=>$rows["billing_state"],
                            "shipping_shiptocontact"=>$rows["shipping_shiptocontact"],
                            "shipping_phone"=>$rows["shipping_phone"],
                            "shipping_add_1"=>$rows["shipping_add_1"],
                            "shipping_add_2"=>$rows["shipping_add_2"],
                            "shipping_country"=>$rows["shipping_country"],
                            "shipping_state"=>$rows["shipping_state"],
                            "shipping_city"=>$rows["shipping_city"],
                            "shipping_postal"=>$rows["shipping_postal"],
                            "more_acc_number"=>$rows["more_acc_number"],
                            "more_fax"=>$rows["more_fax"],
                            "more_mobile"=>$rows["more_mobile"],
                            "more_tollfree"=>$rows["more_tollfree"],
                            "more_website"=>$rows["more_website"],
                            "cust_status"=>$rows["cust_status"],
                            "inv_type"=>$rows["inv_type"],
                            "inv_items"=>$inv_items
                        );
                    }
                    $this->response[Status] = Success;
                    $this->response[Message] = "Invoices Data Found";
                    $this->response['invoices'] = $Invoices;
                }else{
                    $this->response[Status] = Error;
                    $this->response[Message] = "Not Any Invoice Found Yet";
                }
            }else{
                $this->response[Status] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }else{
            $this->response[Status] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function getParticularInvoiceData($inv_id) {
        $link = $this->connect->connect();
        if($link){
            $query = "select * from wp_invoice where inv_id = '$inv_id'";
            $result  = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $rows = mysqli_fetch_assoc($result);
                    $inv_items = array();
                    $customers = array();
                    $custResponse = $this->allDistinctUsers();
                    if($custResponse[Status] == "Success"){
                        $customers = $custResponse['customers'];
                    }
                    $itemsResponse = $this->getInvoiceItems($inv_id);
                    if($itemsResponse[Status] == Success) {
                        $inv_items = $itemsResponse['inv_items'];
                    }
                    $Invoices = array(
                        "inv_id"=>$rows["inv_id"],
                        "inv_number"=>$rows["inv_number"],
                        "inv_date"=>$rows["inv_date"],
                        "inv_due_date"=>$rows["inv_due_date"],
                        "inv_footer"=>$rows["inv_footer"],
                        "inv_notes"=>$rows["inv_notes"],
                        "inv_title"=>$rows["inv_title"],
                        "inv_summary"=>$rows["inv_summary"],
                        "contact_customer"=>$rows["contact_customer"],
                        "contact_email"=>$rows["contact_email"],
                        "contact_phone"=>$rows["contact_phone"],
                        "contact_contact"=>$rows["contact_contact"],
                        "billing_add_1"=>$rows["billing_add_1"],
                        "billing_add_2"=>$rows["billing_add_2"],
                        "billing_city"=>$rows["billing_city"],
                        "billing_postal"=>$rows["billing_postal"],
                        "billing_country"=>$rows["billing_country"],
                        "billing_state"=>$rows["billing_state"],
                        "shipping_shiptocontact"=>$rows["shipping_shiptocontact"],
                        "shipping_phone"=>$rows["shipping_phone"],
                        "shipping_add_1"=>$rows["shipping_add_1"],
                        "shipping_add_2"=>$rows["shipping_add_2"],
                        "shipping_country"=>$rows["shipping_country"],
                        "shipping_state"=>$rows["shipping_state"],
                        "shipping_city"=>$rows["shipping_city"],
                        "shipping_postal"=>$rows["shipping_postal"],
                        "more_acc_number"=>$rows["more_acc_number"],
                        "more_fax"=>$rows["more_fax"],
                        "more_mobile"=>$rows["more_mobile"],
                        "more_tollfree"=>$rows["more_tollfree"],
                        "more_website"=>$rows["more_website"],
                        "cust_status"=>$rows["cust_status"],
                        "inv_type"=>$rows["inv_type"],
                        "inv_items"=>$inv_items
                    );
                    $this->response[Status] = Success;
                    $this->response[Message] = "Invoices Data Found";
                    $this->response['invoices'] = $Invoices;
                    $this->response['customers'] = $customers;
                }
                else{
                    $this->response[Status] = Error;
                    $this->response[Message] = "Not Any Invoice Found Yet";
                }
            }else{
                $this->response[Status] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
        else{
            $this->response[Status] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function allDistinctUsers() {
        $link = $this->connect->connect();
        if($link){
            $customers = array();
            $query = "SELECT max(inv_id),contact_customer,contact_email,contact_phone,contact_contact,billing_add_1,
            billing_add_2,billing_city,billing_postal,billing_country,billing_state,shipping_shiptocontact,
            shipping_phone,shipping_add_1,shipping_add_2,shipping_city,shipping_postal,shipping_country,shipping_state,
            more_acc_number,more_fax,more_mobile,more_website,more_tollfree FROM wp_invoice GROUP by contact_customer 
            ORDER BY inv_id DESC";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    while($rows = mysqli_fetch_assoc($result)) {
                        $customers[] = array(
                            "contact_customer"=>$rows["contact_customer"],
                            "contact_email"=>$rows["contact_email"],
                            "contact_phone"=>$rows["contact_phone"],
                            "contact_contact"=>$rows["contact_contact"],
                            "billing_add_1"=>$rows["billing_add_1"],
                            "billing_add_2"=>$rows["billing_add_2"],
                            "billing_city"=>$rows["billing_city"],
                            "billing_postal"=>$rows["billing_postal"],
                            "billing_country"=>$rows["billing_country"],
                            "billing_state"=>$rows["billing_state"],
                            "shipping_shiptocontact"=>$rows["shipping_shiptocontact"],
                            "shipping_phone"=>$rows["shipping_phone"],
                            "shipping_add_1"=>$rows["shipping_add_1"],
                            "shipping_add_2"=>$rows["shipping_add_2"],
                            "shipping_country"=>$rows["shipping_country"],
                            "shipping_state"=>$rows["shipping_state"],
                            "shipping_city"=>$rows["shipping_city"],
                            "shipping_postal"=>$rows["shipping_postal"],
                            "more_acc_number"=>$rows["more_acc_number"],
                            "more_fax"=>$rows["more_fax"],
                            "more_mobile"=>$rows["more_mobile"],
                            "more_tollfree"=>$rows["more_tollfree"],
                            "more_website"=>$rows["more_website"]
                        );
                    }
                    $this->response[Status] = Success;
                    $this->response[Message] = 'Customers Data Found';
                }else{
                    $this->response[Status] = Error;
                    $this->response[Message] = "Not Any Customer Found Yet";
                }
            }
            else{
                $this->response[Status] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
            $query2 = "select * from wp_customers";
            $result2 = mysqli_query($link,$query2);
            if($result2){
                while($rows2 = mysqli_fetch_assoc($result2)){
                    $customers[] = array(
                        "contact_customer"=>explode(" ",$rows2["name"])[0],
                        "contact_email"=>$rows2["email"],
                        "contact_phone"=>$rows2["mobile"],
                        "contact_contact"=>$rows2["name"],
                        "billing_add_1"=>$rows2["address"],
                        "billing_add_2"=>'',
                        "billing_city"=>$rows2["b_city"],
                        "billing_postal"=>$rows2["pincode"],
                        "billing_country"=>$rows2["b_country"],
                        "billing_state"=>$rows2["b_state"],
                        "shipping_shiptocontact"=>$rows2["name"],
                        "shipping_phone"=>$rows2["mobile"],
                        "shipping_add_1"=>$rows2["address"],
                        "shipping_add_2"=>'',
                        "shipping_country"=>$rows2["b_country"],
                        "shipping_state"=>$rows2["b_state"],
                        "shipping_city"=>$rows2["b_city"],
                        "shipping_postal"=>$rows2["pincode"],
                        "more_acc_number"=>"",
                        "more_fax"=>"",
                        "more_mobile"=>$rows2["mobile"],
                        "more_tollfree"=>"",
                        "more_website"=>""
                    );
                }
            }
            $this->response['customers'] = $customers;
        }
        else{
            $this->response[Status] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function getInvoiceItems($inv_id){
        $link = $this->connect->connect();
        if($link){
            $query = "select * from wp_invoice_items where item_inv_id = '$inv_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    $inv_items = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $inv_items[] = $rows;
                    }
                    $this->response[Status] = Success;
                    $this->response[Message] = "Data Found";
                    $this->response['inv_items'] = $inv_items;
                }else{
                    $this->response[Status] = Error;
                    $this->response[Message] = "No Data Found";
                }
            }else{
                $this->response[Status] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
        else{
            $this->response[Status] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function deleteInvoice($inv_id){
        $link = $this->connect->connect();
        if($link){
            $query = "delete from wp_invoice_items where item_inv_id = '$inv_id'";
            $result = mysqli_query($link,$query);
            if($result){
                $query2 = "delete from wp_invoice where inv_id = '$inv_id'";
                $result2 = mysqli_query($link,$query2);
                if($result2){
                    $this->response[Status] = Success;
                    $this->response[Message] = "Invoice Deleted";
                }else{
                    $this->response[Status] = Error;
                    $this->response[Message] = $this->connect->sqlError();
                }
            }else{
                $this->response[Status] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
        else{
            $this->response[Status] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function sendInvoiceEmail($inv_id,$to,$addCC,$msg,$copytoadmin,$adminMail){
        $inv_resp = $this->getParticularInvoiceData($inv_id);
        if ($inv_resp[Status] == Success) {
            $invoices = $inv_resp['invoices'];
            $inv_items = "";
            $total = 0;
            $sub_total = 0;
            $tax_total = 0;
            for($i=0;$i<sizeof($invoices['inv_items']);$i++){
                $inv_items = $inv_items.'<tr style="text-align: center;padding:5px 0;"><td 
                style="border-bottom:1px solid #eee;padding:6px 0">'.$invoices['inv_items'][$i]['item_name'].'</td>
                <td style="border-bottom:1px solid #eee">'.$invoices['inv_items'][$i]['item_desc'].'</td>
                <td style="border-bottom:1px solid #eee">'.$invoices['inv_items'][$i]['item_quantity'].'</td>
                <td style="border-bottom:1px solid #eee">$'.$invoices['inv_items'][$i]['item_price'].'</td>
                <td style="border-bottom:1px solid #eee">$'.$invoices['inv_items'][$i]['item_total'].'</td></tr>';
                $total = $total+$invoices['inv_items'][$i]['total_with_tax'];
                $sub_total = $sub_total+$invoices['inv_items'][$i]['item_total'];
                $tax_total = $tax_total+$invoices['inv_items'][$i]['item_tax'];
            }
            require 'SMTP/PHPMailerAutoload.php';
            $mail = new \PHPMailer();
            $mail->isSMTP();
            $mail->Host = 'scan2fit.com';
            $mail->SMTPAuth = true;
            $mail->Username = 'scan2fitsftailor@scan2fit.com';
            $mail->Password = 'sftailor';
            $mail->SMTPSecure = 'TLS';
            $mail->Port = 587;
            $mail->setFrom('invoice@scan2fit.com', 'SFTailor');
            $mail->addAddress($to);
            $mail->AddCC($addCC);
            if($copytoadmin){
                $mail->addAddress($adminMail);
            }
            $mail->isHTML(true);
            $mail->Subject = 'invoice@SFTailor';
            $mail->Body = $msg.'<br><br><br><body style="width:90%;margin-left:5%;box-shadow:0 0 5px #eee;padding:10px">
            <div style="width:100%"><div style="width:50%;float:left;text-align: left">
            <img class="" src="http://scan2fit.com/sftailor/images/logo/logo.png"><div><span class="">SFTAILOR</span>
            </div></div><div style="width:50%;float:right;text-align: right"><label style="font-size:22px">
            '.$invoices['inv_title'].'</label><div><label>SFTAILOR</label></div><p>2500 Reliance Ave<br>
            2500 Reliance Ave, Apex, NC 27539,<br>USA</p></div></div><div style="clear:both"></div><hr>
            <div style="width:100%;border:none"><div style="width:50%;float:left"><ul style="list-style:none;padding:0">
            <li><label style="font-weight: bold;font-size:18px">BILL TO</label></li><br><li>
            <label style="text-transform:uppercase">Sunil</label></li><li><span>'.$invoices['contact_contact'].'</span>
            </li><li><span>'.$invoices['billing_city'].', '.$invoices['billing_state'].' '.$invoices['billing_postal'].'
            </span></li><li><span>'.$invoices['billing_country'].'</span></li><li><span></span></li><li><span></span>
            </li><li><span>'.$invoices['contact_phone'].'</span></li><li><span>'.$invoices['contact_email'].'</span>
            </li></ul></div><div style="padding-top:50px;width:50%;float:right;text-align: right"><label>Invoice Number :
            </label><label>'.$invoices['inv_number'].'</label> <br><br> <label>Invoice Date :</label> 
            <label>'.$invoices['inv_date'].'</label> <br><br> <label>Payment Due :</label>
            <label>'.$invoices['inv_due_date'].'</label> <br><br> <label>Amount Due (USD):</label>
            <label>$'.$total.'</label></div></div><div style="clear:both"></div><div style="width:100%">
            <table style="width:100%;margin-top:20px;border:1px solid #eee">
            <tr style="background:#666;border:hidden;color:white"><th style="padding:10px 0">Item Name</th>
            <th>Item Description</th><th>Quantity</th><th>Price</th><th>Amount</th></tr>'.$inv_items.'</table>
            </div><hr><div style="width:100%"><div style="width:50%;float:left;"></div><div style="width:50%;
            float:right;text-align: right;margin:30px 30px; 0 0"> <label style="font-weight:bold">Subtotal : </label>
            <label>$'.$sub_total.'</label><br><br> <label style="font-weight:bold">Tax % :</label>
            <label>$'.$tax_total.'</label><br><br> <label style="font-weight:bold">Total : </label>
            <label>$'.$total.'</label><br><br> <label style="font-weight:bold">Amout Due (USD) : </label>
            <label>$'.$total.'</label></div><div style="clear:both"></div><hr></div><div style="clear:both"></div>
            <div style="margin-bottom:100px;"><p style="font-weight:bold">Notes</p><label>'.$invoices['inv_notes'].'</label>
            </div><div style="clear:both"></div><div style="margin-bottom:30px;width: 100%"><p style="text-align:center;
            color:#444">'.$invoices['inv_footer'].'</p></div></body>';
            $result = $mail->send();
            $this->response[STATUS] = Success;
            $this->response[Message] = "Mail send successfully.";
            $this->changeMailStatus($inv_id);
        }
        else{
            $this->response = $inv_resp;
        }
        return $this->response;
    }
    public function changeMailStatus($inv_id){
        $link = $this->connect->connect();
        if($link) {
            $query = "update wp_invoice set cust_status = 'sent' where inv_id = '$inv_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[Message] = 'Status Changed';
            }else{
                $this->response[STATUS] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
    }
    public function getCategories(){
        $link = $this->connect->connect();
        if($link) {
            $query = "select * from categories where cat_type != 'Coupons'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $categories = array();
                    while($rows = mysqli_fetch_assoc($result)){
                        $categories[] = $rows;
                    }
                    $this->response[STATUS] = Success;
                    $this->response[Message] = 'Data Found';
                    $this->response['categories'] = $categories;
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[Message] = 'No Categories Found';
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function storeInvoiceSchedule($inv_id,$repeat_type,$date,$day,$month,$firstInv,$endon,$timezone){
        $link = $this->connect->connect();
        if($link) {
            $quer = "select * from wp_invoice_sch where inv_id = '$inv_id'";
            $resul = mysqli_query($link,$quer);
            if($resul){
                $num = mysqli_num_rows($resul);
                if($num>0){
                    $query = "update wp_invoice_sch set repeat_type='$repeat_type',inv_date='$date',inv_day='$day',
                    inv_month='$month',first_inv='$firstInv',endon='$endon',timezone='$timezone'";
                }
                else{
                    $query = "insert into wp_invoice_sch (inv_id,repeat_type,inv_date,inv_day,inv_month,first_inv,endon,
                    timezone) values ('$inv_id','$repeat_type','$date','$day','$month','$firstInv','$endon','$timezone')";
                }
                $result = mysqli_query($link, $query);
                if ($result) {
                    $this->response[STATUS] = Success;
                    $this->response[Message] = 'Data Found';
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[Message] = $this->connect->sqlError();
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function getInvoiceSchedule($inv_id){
        $link = $this->connect->connect();
        if($link) {
            $quer = "select * from wp_invoice_sch where inv_id = '$inv_id'";
            $resul = mysqli_query($link,$quer);
            if($resul){
                $num = mysqli_num_rows($resul);
                if($num>0){
                    $row = mysqli_fetch_assoc($resul);
                    $this->response[STATUS] = Success;
                    $this->response[Message] = 'Data Found';
                    $this->response['sch_data'] = $row;
                }else{
                    $this->response[STATUS] = Error;
                    $this->response[Message] = "No data Found";
                }
            }else{
                $this->response[STATUS] = Error;
                $this->response[Message] = $this->connect->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[Message] = $this->connect->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response,$code=null){
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}
?>