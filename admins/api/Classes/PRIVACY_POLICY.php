<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/20/2017
 * Time: 11:23 AM
 */

namespace Classes;
require_once('CONNECT.php');

class PRIVACY_POLICY
{
    public $link = null;
    public $response = array();

    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }

    public function updateSalesTerms($editor1)
    {
        $link = $this->link->connect();
        if ($link) {
            $query = "select * from privacy_policy";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $query = "update privacy_policy set message='$editor1'";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Privacy Policy  Details Updated SuccessFully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
                else{
                    $query = "insert into privacy_policy (message) values('$editor1')";
                    $result = mysqli_query($link, $query);
                    if ($result) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Privacy Policy  Details Add SuccessFully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        } else {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getPrivacyData(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from privacy_policy";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while($rows = mysqli_fetch_array($result)){
                        $privacyData[]=array(
                            "terms_id"=>$rows["id"],
                            "message"=>$rows["message"]
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "message Found";
                    $this->response['privacyData'] = $privacyData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}