<?php
include("header.php");
include_once "admins/api/Classes/CONNECT.php";
$conn = new Classes\CONNECT();
?>
<style>
    mark {
        background: yellow;
    }

    mark.current {
        background: orange;
    }

    .header {
        width: 100%;
        background: #eee;
        position: fixed;
        top: 0;
        left: 0;
    }

    .content {
        margin-top: 50px;
    }
    .searchBox{
        width: 400px;
        position: fixed;
        right: 10px;
        z-index: 99;
        margin-top: 60px;
    }
    .searchField{
        width: 50%;
        box-shadow: 0 1px 1px rgba(0, 0, 0, 0.075) inset;
        color: #555;
        font-size: 14px;
        line-height: 1.42857;
        padding: 6px 12px;
    }
    #partner{
        display: none;
    }
    #ray-all_text span.active {
        background-color:yellow;
    }
</style>
<div class="about-bottom wthree-3 ">
    <div class="searchBox pull-right">
        <div class="ray-search">
            <div class="field" id="ray-search-form">
                <input type="text" id="searchfor" placeholder="what are you searching for?" style="width: 205px;"/>
                <button type="button" id="search">Press to Find!</button>
            </div>
        </div>
    </div>

    <div class="container " style="margin-top:50px">
		<h2 class="tittle">F.A.Q</h2>
        <article id="ray-all_text">
        <div class="agileinfo_about_bottom_grids content" style="margin-top:100px" id="content">
            <?php
            $query = "select * from faqs where type = 'Placing Order'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Placing Order</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <?php
            $query = "select * from faqs where type = 'Placing Order2'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Placing Order2</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <?php
            $query = "select * from faqs where type = 'Placing Order3'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Placing Order3</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <?php
            $query = "select * from faqs where type = 'Payment'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Payment</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <?php
            $query = "select * from faqs where type = 'Delivery and Shipping'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Delivery and Shipping</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <?php
            $query = "select * from faqs where type = 'Fit'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Fit</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <?php
            $query = "select * from faqs where type = 'Product'";
            $result = mysqli_query($conn->connect(),$query);
            if($result){
                $num = mysqli_num_rows($result);
                if($num>0){
                    echo "<h1 style='text-decoration: underline'>Product</h1><br>";
                    $a=0;
                    while($rows = mysqli_fetch_assoc($result)){
                        $a++
                        ?>
                        <h4><label><?php echo $a." : ". $rows['ques']?></label></h4>
                        <p style="margin-left:25px"><?php echo $rows['ans']?></p><br>
                        <?php
                    }
                }else{
                    echo "No Faq Question's Found";
                }
            }else{
                echo mysqli_error($conn->connect());
            }
            ?>
            <hr>
            <h1 style='text-decoration: underline'>Terms & Privacy Policy</h1><br>
            <div class="text-center" style="padding-top:30px">
                <a href="terms.php">Click Here. See Sales Terms</a><br>
                <a href="privacyPolicy.php">Click Here. See Privacy Policy</a>
            </div>

        </div>
        </article>
    </div>
</div>
<?php include("footer.php");?>
<script>
    $('body').on('keydown', '#searchfor', function(e) {
        if (e.which === 32 &&  e.target.selectionStart === 0) {
            return false;
        }
    });
    var searching,
        limitsearch,
        countsearch;
    $('button#search').click(function() {
        var searchedText = $('#searchfor').val();
        var page = $('#ray-all_text');

        if (searchedText != "") {
            if(searching != searchedText) {
                page.find('span').contents().unwrap();
                var pageText = page.html();
                var theRegEx = new RegExp("(" + searchedText + ")", "igm");
                var newHtml = pageText.replace(theRegEx, "<span>$1</span>");
                page.html(newHtml);
                searching = searchedText;
                limitsearch = page.find('span').length;
                countsearch=0;
            } else {
                countsearch<limitsearch-1 ? countsearch++ : countsearch=0;
                console.log(countsearch+'---'+limitsearch)
            }
            var actual = $("#ray-all_text span").eq(countsearch);
            $('.active').removeClass('active');
            actual.addClass('active');
            $('body').animate({
                    scrollTop: actual.offset().top - 130},
                200);
        } else {
            alert('empty search')
        }
    });

    $(window).scroll(function() {
        if ($(window).scrollTop() > 60) {
           /* $('.ray-search').css('position', 'fixed');*/
            $('.ray-search').addClass('ray-width').css('top', 0);
        } else {
           /* $('.ray-search').css('position', 'relative');*/
            $('.ray-search').removeClass('ray-width').css('top', 0);
        }
    });


</script>
<!-- //team -->
<!--<script type="text/javascript" src="js/find.js"></script>-->


