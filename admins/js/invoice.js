var itemsCount = 1;
var items="";
var categories;
var invoiceData = {
    customers: "",
    invoices: "",
};
var tax=0;
function getTax(){
    var url="api/userProcess.php";
    $.post(url,{"type":"getSalesData"},function(data){
        var Status = data.Status;
        if(Status === "Success"){
            tax = data.paymentData[0].sales_amount;
        }
        else{
            tax = 7.25;
        }
    });
}
window.onload = function () {
    tabClicked('all');
};
/*view create invoices start here*/
function createInvoices() {
    $(".invoicesData").hide();
    $(".previewInvoicesData").hide();
    $(".createInvoicesData").show();
}
function goBack() {
    $(".invoicesData").show();
    $(".createInvoicesData").hide();
}
function closePreview () {
    location.reload();
}
function onAddItem() {
    var url = 'api/invoiceProcess.php';
    $.post(url,{"type":"getCategories"},function(data){
        var Status = data.Status;
        if(Status === "Success"){
            categories = data.categories;
            var datashow = "<option value='' selected='selected'>Select Article</option>";
            for(var i = 0;i<categories.length;i++){
                datashow += "<option value='"+categories[i].cat_name+"'>"+categories[i].cat_name+"</option>";
            }
            datashow += "<option value='other'>Add Other Article</option>";
            $("#customerTable").append('<tr id="tr'+itemsCount+'a"><td class="td-item" id="dynatr'+itemsCount+'"><select class="form-control" ' +
            'id="tritemname'+itemsCount+'" onchange=otherarticle('+itemsCount+')>'+datashow+'</select></td><td class="td-item"><input type="text" class="form-control" ' +
            'placeholder="Enter Item Description" id="tritemdesc'+itemsCount+'"></td><td class="td-item"><input type="number" style="width:100px" value="1" min="1" ' +
            'class="form-control" onchange=maintainAmount('+itemsCount+') onkeyup=maintainAmount('+itemsCount+') ' +
            'id="trquant'+itemsCount+'" /></td><td class="td-item"><input type="number" style="width:100px" min="0" ' +
            'onkeyup=maintainAmount('+itemsCount+') onchange=maintainAmount('+itemsCount+') id="trprice'+itemsCount+'" ' +
            'value="0" class="form-control" /></td><td><label id="trtotal'+itemsCount+'" style="color:#333">$0.00</label>' +
            '</td><td><i onclick=deleteTR("tr'+itemsCount+'") class="fa fa-2x fa-trash"></i></td></tr>' +
            '<tr style="margin-bottom:10px" id="tr'+itemsCount+'b"><td colspan="4" align="right" style="padding-right:60px">' +
            '<label>Service Tax % </label><input type="number" value="'+tax+'" onkeyup=maintainAmount('+ itemsCount +') ' +
            'class="form-control" style="width:90px" onchange=maintainAmount('+itemsCount+') step="0.1" ' +
            'id="taxperc'+itemsCount+'" min="0" max="99"/></td><td colspan="2"><label id="trtax'+itemsCount+
            '">$0.00</label></td></tr>');
            itemsCount=itemsCount+1;
        }
    });
}
function otherarticle(idd){
    if($("#tritemname"+idd).val() === "other"){
        $("#dynatr"+idd).append("<input style='margin-top:10px' type='text' id='tritemnameother"+idd+"' class='form-control' value='' placeholder='Enter Article Name'/>")
        $("#tritemdesc"+idd).val("");
    }else{
        for(var i=0;i<categories.length;i++){
            if(categories[i].cat_name === $("#tritemname"+idd).val()){
                $("#tritemdesc"+idd).val(categories[i].cat_description);
            }
        }
        $("#tritemnameother"+idd).remove();
    }
}
function findtotal(){
    var totalRows = $("#customerTable").children().length;
    var total = 0;
    var json = "";
    for(var i=1;i<=totalRows;i++) {
        if(i%2 !== 0){
            var idd = ($("#customerTable").children()[i].id).split("tr")[1];
            idd = idd.split('b')[0];
            var itemname = $("#tritemname"+idd).val();
            var itemnameother = "";
            if(itemname === "other"){
                itemnameother = $("#tritemnameother"+idd).val();
            }
            var itemdesc = $("#tritemdesc"+idd).val();
            var itemquant = $("#trquant"+idd).val();
            var itemprice = $("#trprice"+idd).val();
            var totalwithouttax = parseFloat($("#trtotal"+idd).text().split("$")[1]);
            var tax = parseFloat($("#trtax"+idd).text().split("$")[1]);
            var tax_perc = $("#taxperc"+idd).val();
            var totalwithtax = tax+totalwithouttax;
            if(totalRows === i+1){
                json = json+'{"item_name":"'+itemname+'","item_name_other":"'+itemnameother+'","item_desc":"'+itemdesc+'","item_price":"'+
                itemprice+'","item_quantity":"'+itemquant+'","total_without_tax":"'+totalwithouttax+
                '","tax_perc":"'+tax_perc+'","tax_amount":"'+tax+'","total_with_tax":"'+totalwithtax+'"}';
                total = total+totalwithtax;
            }else {
                json = json + '{"item_name":"' + itemname + '","item_name_other":"'+itemnameother+'","item_desc":"' + itemdesc + '","item_price":"' +
                itemprice + '","item_quantity":"' + itemquant + '","total_without_tax":"' + totalwithouttax +
                '","tax_perc":"'+tax_perc+'","tax_amount":"' + tax + '","total_with_tax":"' + totalwithtax + '"},';
                total = total + totalwithtax;
            }
        }
    }
    $("#finaltotal").html("$"+parseFloat(total).toFixed(2));
    items = "["+json+"]";
}
function maintainAmount(u_id){
    var price = $("#trprice"+u_id).val();
    var quantity = $("#trquant"+u_id).val();
    var total = price*quantity;
    var taxperc = $("#taxperc"+u_id).val();
    var tax = ((total*taxperc)/100);
    $("#trtotal"+u_id).html("$"+(total).toFixed(2));
    $("#trtax"+u_id).html("$"+(tax).toFixed(2));
    findtotal();
}
function deleteTR(trnumber){
    $("#"+trnumber+"a").remove();
    $("#"+trnumber+"b").remove();
    findtotal();
}
function getAllItems(type) {
    var url = "api/invoiceProcess.php";
    $.post(url, {type: 'getAllInvoices', status: type}, function (data) {
        var status = data.Status;
        var tableData = '<thead><tr><th>S.No</th><th>Date</th><th>Invoice Number</th><th>Customer Contact</th><th>Customer E-Mail</th>' +
        '<th>Invoice Amount</th><th>Action</th></tr></thead><tbody>';
        invoiceData.customers = data.customers;
        if (status === 'Success') {
            var invoices = data.invoices;
            invoiceData.invoices = invoices;
            for (var i = 0; i < invoices.length; i++) {
                var total = 0;
                var obj = invoices[i];
                var inv_items = obj.inv_items;
                for(var j=0;j<inv_items.length;j++){
                   total = parseFloat(total)+parseFloat(inv_items[j].total_with_tax);
                }
                tableData = tableData + '<tr><td>' +(i+1)+ '</td><td>' + obj.inv_date + '</td><td>' + obj.inv_number + '</td> ' +
                '<td>' + obj.contact_customer+ '</td><td>'+obj.contact_email+'</td><td>$'+parseFloat(total).toFixed(2)+'</td><td>'+
                '<button onclick=sendEmailForm("'+obj.inv_id+'") class="btn btn-success"><i class="fa fa-envelope-o">' +
                '</i></button><button onclick=editInvoice("'+obj.inv_id+'") class="btn btn-primary"><i class="fa fa-edit">' +
                '</i></button><button class="btn btn-danger" onclick="confirmInvoice('+obj.inv_id+')">' +
                '<i class="fa fa-trash"></i></button></td></tr>';
            }
        }else{
            tableData = tableData+"<tr><td colspan='7' style='text-align: center'>Not any Invoice Found</td>";
        }
        $("#" + type + "Table").html(tableData);
        $("#" + type + "Table").dataTable({destroy: true});
        var inv_number = Math.floor(Math.random()*10000000);
        $("#inv_number").val(inv_number);
    });
}
function sendEmailForm(inv_id){
    $("#inv_id").val(inv_id);
    var url = "api/invoiceProcess.php";
    $.post(url,{"type":"getSelectedInvoice","inv_id":inv_id},function(data){
       var Status = data.Status;
       if(Status === "Success"){
           var invoiceData = data.invoices;
           var email = invoiceData.contact_email;
           $("#email-id").val(email);
           $("#sendMailModal").modal("show");
       }else{
           alert("SomeThing Went Wrong Please Try After Some Time")
       }
    });
}
function tabClicked(_id) {
    $(".tabli").removeClass('active');
    $("#"+_id).addClass('active');
    $(".tab-pane").css('display','none');
    $("#"+_id+"div").css('display','block');
    getAllItems(_id);
}
function editInvoice(inv_id){
    window.location='update_invoice.php?u_id='+inv_id;
}
function confirmInvoice(inv_id) {
    $(".modal-titledyna").html("<label style='letter-spacing:1px;font-size: 20px;'>Permission</label>");
    $(".modal-bodydyna").html("<label style='letter-spacing:1px'>Would you like to remove invoice (y/n)?</label>");
    $(".modal-footerdyna").html('<button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-remove">' +
    '</i> Close</button><button type="button" class="btn btn-danger" onclick=removeInvoice("'+inv_id+'") >' +
    '<i class="fa fa-trash"></i> Delete Now</button>');
    $("#myModal").modal("show");
}
function removeInvoice(inv_id){
    $("#myModal").modal("hide");
    var url = 'api/invoiceProcess.php';
    $.post(url,{"type":"deleteInvoice","inv_id":inv_id},function(data){
        var Status = data.Status;
        if(Status === "Success"){
            location.reload();
        }else{
            $(".modal-titledyna").html("<label style='letter-spacing: 1px;font-size: 20px;'>Error</label>");
            $(".modal-bodydyna").html("<label style='letter-spacing: 1px'>"+data.Message+"</label>");
            $(".modal-footerdyna").html('<button type="button" class="btn btn-primary" data-dismiss="modal">' +
            '<i class="fa fa-remove"></i> Close</button>');
            $("#myModal").modal("show");
        }
    });
}
/*customer add start here*/
function customerAdd(type) {
    if(type === 'new') {
        $("#contact_customer").val("");
        $("#contact_email").val("");
        $("#contact_phone").val("");
        $("#contact_contact_fname").val("");
        $("#contact_contact_lname").val("");
        $("#billing_add_1").val("");
        $("#billing_add_2").val("");
        $("#billing_country").val("");
        $("#billing_state").val("");
        $("#billing_city").val("");
        $("#billing_postal").val("");
        $("#shipping_shiptocontact").val("");
        $("#shipping_phone").val("");
        $("#shipping_add_1").val("");
        $("#shipping_add_2").val("");
        $("#shipping_country").val("");
        $("#shipping_state").val("");
        $("#shipping_city").val("");
        $("#shipping_postal").val("");
        $("#more_acc_number").val("");
        $("#more_fax").val("");
        $("#more_mobile").val("");
        $("#more_tollfree").val("");
        $("#more_website").val("");
    }
    $("#customerAdd").modal("show");
}
/*customer add end here*/
function hideShowSameBilling() {
    if ($("#sameBilling").is(":checked")) {
        $(".billingBoxData").hide();
        $("#shipping_add_1").val($("#billing_add_1").val());
        $("#shipping_add_2").val($("#billing_add_2").val());
        $("#shipping_country").val($("#billing_country").val());
        $("#shipping_state").val($("#billing_state").val());
        $("#shipping_city").val($("#billing_city").val());
        $("#shipping_postal").val($("#billing_postal").val());
    }
    else {
        $(".billingBoxData").show();
        $("#shipping_add_1").val('');
        $("#shipping_add_2").val('');
        $("#shipping_country").val('');
        $("#shipping_state").val('');
        $("#shipping_city").val('');
        $("#shipping_postal").val('');
    }
}
function loadStep(stepno,data) {
    if(stepno === "3") {
        if(invoiceData.customers.length === 0){
            customerAdd('new');
        }else{
            var dataShow = "";
            for(var i=0;i<invoiceData.customers.length;i++){
                dataShow += "<li onclick=loadStep('1','"+escape(invoiceData.customers[i].contact_customer)+"')><label>"+invoiceData.customers[i].contact_customer+"</label><br>" +
                "<span>"+invoiceData.customers[i].contact_contact+"</span></li>";
            }
            dataShow += "<li onclick=customerAdd('new')><i class='fa fa-plus'></i> Add Customer</li>";
            $("#allUsersList").html(dataShow);
            $(".invoice-customer-box").removeClass("active");
            $(".invoice-customer-box.step-" + stepno).addClass("active");
        }
    }
    else if(stepno === "1") {
        var contact_customer = unescape(data);
        for(var j=0;j<invoiceData.customers.length;j++) {
            if(invoiceData.customers[j].contact_customer === contact_customer){
                $("#contact_customer").val(invoiceData.customers[j].contact_customer);
                $("#contact_email").val(invoiceData.customers[j].contact_email);
                $("#contact_phone").val(invoiceData.customers[j].contact_phone);
                $("#contact_contact_fname").val((invoiceData.customers[j].contact_contact).split(" ")[0]);
                $("#contact_contact_lname").val((invoiceData.customers[j].contact_contact).split(" ")[1]);
                $("#billing_add_1").val(invoiceData.customers[j].billing_add_1);
                $("#billing_add_2").val(invoiceData.customers[j].billing_add_2);
                $("#billing_country").val(invoiceData.customers[j].billing_country);
                $("#billing_state").val(invoiceData.customers[j].billing_state);
                $("#billing_city").val(invoiceData.customers[j].billing_city);
                $("#billing_postal").val(invoiceData.customers[j].billing_postal);
                $("#shipping_shiptocontact").val(invoiceData.customers[j].shipping_shiptocontact);
                $("#shipping_phone").val(invoiceData.customers[j].shipping_phone);
                $("#shipping_add_1").val(invoiceData.customers[j].shipping_add_1);
                $("#shipping_add_2").val(invoiceData.customers[j].shipping_add_2);
                $("#shipping_country").val(invoiceData.customers[j].shipping_country);
                $("#shipping_state").val(invoiceData.customers[j].shipping_state);
                $("#shipping_city").val(invoiceData.customers[j].shipping_city);
                $("#shipping_postal").val(invoiceData.customers[j].shipping_postal);
                $("#more_acc_number").val(invoiceData.customers[j].more_acc_number);
                $("#more_fax").val(invoiceData.customers[j].more_fax);
                $("#more_mobile").val(invoiceData.customers[j].more_mobile);
                $("#more_tollfree").val(invoiceData.customers[j].more_tollfree);
                $("#more_website").val(invoiceData.customers[j].more_website);
                $("#name").html($("#contact_customer").val());
                $("#full_name").html($("#contact_contact_fname").val()+" "+$("#contact_contact_lname").val());
                $("#address").html($("#billing_city").val()+", "+$("#billing_state").val()+" "+$("#billing_postal").val());
                $("#country").html($("#billing_country").val());
                $("#mobile_no").html($("#contact_phone").val());
                $("#email").html($("#contact_email").val());
                $("#edit_cust_label").html($("#contact_customer").val());
            }
        }
        $(".invoice-customer-box").removeClass("active");
        $(".invoice-customer-box.step-" + stepno).addClass("active");
    }
    else {
        $(".invoice-customer-box").removeClass("active");
        $(".invoice-customer-box.step-" + stepno).addClass("active");
    }
}
function validateInvoiceData(){
    var contact_customer = $("#contact_customer").val();
    var contact_email = $("#contact_email").val();
    var contact_phone = $("#contact_phone").val();
    var contact_contact_fname = $("#contact_contact_fname").val();
    var contact_contact_lname = $("#contact_contact_lname").val();
    var billing_add_1 = $("#billing_add_1").val();
    var billing_add_2 = $("#billing_add_2").val();
    var billing_country = $("#billing_country").val();
    var billing_state = $("#billing_state").val();
    var billing_city = $("#billing_city").val();
    var billing_postal = $("#billing_postal").val();
    var shipping_shiptocontact = $("#shipping_shiptocontact").val();
    var shipping_phone = $("#shipping_phone").val();
    var shipping_add_1 = $("#shipping_add_1").val();
    var shipping_add_2 = $("#shipping_add_2").val();
    var shipping_country = $("#shipping_country").val();
    var shipping_state = $("#shipping_state").val();
    var shipping_city = $("#shipping_city").val();
    var shipping_postal = $("#shipping_postal").val();
    var more_acc_number = $("#more_acc_number").val();
    var more_fax = $("#more_fax").val();
    var more_mobile = $("#more_mobile").val();
    var more_tollfree = $("#more_tollfree").val();
    var more_website = $("#more_website").val();
    if(contact_customer === ""){
        alert("Please Fill Business Name or Person...");
        return false;
    }
    $("#name").html($("#contact_customer").val());
    $("#full_name").html($("#contact_contact_fname").val()+" "+$("#contact_contact_lname").val());
    $("#address").html($("#billing_city").val()+", "+$("#billing_state").val()+" "+$("#billing_postal").val());
    $("#country").html($("#billing_country").val());
    $("#mobile_no").html($("#contact_phone").val());
    $("#email").html($("#contact_email").val());
    $("#edit_cust_label").html($("#contact_customer").val());
    $(".invoice-customer-box").removeClass("active");
    $(".step-1").addClass("active");
    $("#customerAdd").modal("hide");
}
function storeInvoice(){
    var inv_number = $("#inv_number").val();
    if(inv_number === ""){
        inv_number = Math.floor(Math.random()*10000000);
    }
    var invoice_date = $("#invoice_date").val();
    var invoice_due_date = $("#invoice_due_date").val();
    var inv_notes = $("#inv_notes").val();
    var inv_footer = $("#inv_footer").val();
    var inv_title = $("#inv_title").val();
    var inv_summary = $("#inv_summary").val();
    var contact_customer = $("#contact_customer").val();
    var contact_email = $("#contact_email").val();
    var contact_phone = $("#contact_phone").val();
    var contact_contact = $("#contact_contact_fname").val()+" "+$("#contact_contact_lname").val();
    var billing_add_1 = $("#billing_add_1").val();
    var billing_add_2 = $("#billing_add_2").val();
    var billing_country = $("#billing_country").val();
    var billing_state = $("#billing_state").val();
    var billing_city = $("#billing_city").val();
    var billing_postal = $("#billing_postal").val();
    var shipping_shiptocontact = $("#shipping_shiptocontact").val();
    var shipping_phone = $("#shipping_phone").val();
    var shipping_add_1 = $("#shipping_add_1").val();
    var shipping_add_2 = $("#shipping_add_2").val();
    var shipping_country = $("#shipping_country").val();
    var shipping_state = $("#shipping_state").val();
    var shipping_city = $("#shipping_city").val();
    var shipping_postal = $("#shipping_postal").val();
    var more_acc_number = $("#more_acc_number").val();
    var more_fax = $("#more_fax").val();
    var more_mobile = $("#more_mobile").val();
    var more_tollfree = $("#more_tollfree").val();
    var more_website = $("#more_website").val();
    var url = "api/invoiceProcess.php";
    $.post(url,{
            'type':'createInvoice',
            'inv_number':inv_number,
            'inv_date':invoice_date,
            'inv_due_date':invoice_due_date,
            'inv_footer':inv_footer,
            'inv_notes':inv_notes,
            'inv_title':inv_title,
            'inv_summary':inv_summary,
            'inv_type':'unpaid',
            'contact_customer':contact_customer,
            'contact_email':contact_email,
            'contact_phone':contact_phone,
            'contact_contact':contact_contact,
            'billing_add_1':billing_add_1,
            'billing_add_2':billing_add_2,
            'billing_city':billing_city,
            'billing_postal':billing_postal,
            'billing_country':billing_country,
            'billing_state':billing_state,
            'shipping_shiptocontact':shipping_shiptocontact,
            'shipping_phone':shipping_phone,
            'shipping_add_1':shipping_add_1,
            'shipping_add_2':shipping_add_2,
            'shipping_postal':shipping_postal,
            'shipping_country':shipping_country,
            'shipping_state':shipping_state,
            'shipping_city':shipping_city,
            'more_acc_number':more_acc_number,
            'more_fax':more_fax,
            'more_mobile':more_mobile,
            'more_tollfree':more_tollfree,
            'more_website':more_website,
            'cust_status':'Unsent',
            'items':items
        },
        function(data){
            var Status = data.Status;
            if(Status === "Success"){
                var inv_id = data.inv_id;
                preview(inv_id);
            }else{
                alert("Please Fill all the Required Fields");
            }
        }).fail(function(){
        alert("Server Error !!! Please Try After Some Time...");
    });
}
function preview(inv_id){
    $(".createInvoicesData").hide();
    $(".invoicesData").hide();
    $(".previewInvoicesData").show();
    $("#inv_id").val(inv_id);
    loadPreviewData();
}
function loadPreviewData() {
    var cust_name = $("#contact_customer").val();
    var inv_date = $("#invoice_date").val();
    var inv_duedate = $("#invoice_due_date").val();
    $("#r_name").html("You have not add any customer yet");
    $("#r_number").html($('#inv_number').val());
    var human_date = today.getDate()+"-"+today.getMonth()+"-"+today.getFullYear();
    $("#r_invoice_date").html(human_date);
    $("#r_invoicedue_date").html(human_date);
    if(inv_date!=="") {
        $("#r_invoice_date").html(inv_date);
        $("#r_invoicedue_date").html(inv_duedate);
    }
    if(cust_name!=='') {
        $("#r_name").html($("#contact_customer").val());
        $("#r_full_name").html($("#contact_contact_fname").val()+" "+$("#contact_contact_lname").val());
        $("#r_address").html($("#billing_city").val()+", "+$("#billing_state").val()+" "+$("#billing_postal").val());
        $("#r_country").html($("#billing_country").val());
        $("#r_mobile_no").html($("#contact_phone").val());
        $("#r_email").html($("#contact_email").val());
        if($("#inv_notes").val() !== "") {
            $(".rec_notes").html("<p><label>Notes</label></p><span>"+$("#inv_notes").val()+"</span>");
        }
        $(".footerContent").html($("#inv_footer").val());
    }
    loadTable();
}
function loadTable() {
    var td_='';
    var amount = 0;
    var tax = 0;
    var n=0;
    $("#customerTable tr").each(function() {
        if(this.id.indexOf('a')!==-1){
            var tr = document.getElementById(this.id);
            var id = $('#'+this.id+' td input').attr('id');
            id = id.substr(id.length-1,id.length);
            var name = "tritemname"+id;
            name = $('#'+name).val();
            var desc = "tritemdesc"+id;
            desc = $('#'+desc).val();
            var quantity = "trquant"+id;
            quantity = $('#'+quantity).val();
            var price = "trprice"+id;
            price = $('#'+price).val();
            var tax_perc = "taxperc"+id;
            tax_perc = $('#'+tax_perc).val();
            var tax_amount = "trtax"+id;
            tax_amount = parseFloat($('#'+tax_amount).text().split("$")[1]).toFixed(2);
            var total_with_tax = parseFloat(quantity*price)+parseFloat(tax_amount);
            var sub_price = parseFloat(quantity*price).toFixed(2);
            if(name === '') {
                $("#templateTableBody").html('You have not added any items');
                $("#templateTableBody").css('padding','10px');
                return false;
            }
            amount = amount+total_with_tax;
            td_= td_+'<tr><td>'+(n+1)+'</td><td>'+name+'</td><td>'+desc+'</td><td>'+quantity+'</td><td>$'+parseFloat(price).toFixed(2)+
            '</td><td>$'+parseFloat(sub_price).toFixed(2)+'</td><td>$'+parseFloat(tax_amount).toFixed(2)+'</td>' +
            '<td>$'+total_with_tax+'</td></tr>';
            n++;
        }
        var final_amount = amount;
        $('#templateTableBody').html(td_);
        $('#r_total').html("$"+parseFloat(final_amount).toFixed(2));
        $('#r_amount_duee').text("$"+parseFloat(final_amount).toFixed(2));
        $('#r_amount_due').text("$"+parseFloat(final_amount).toFixed(2));
    });
}
function tabswitch(tab_id){
    $('.modaltab').removeClass('active');
    $('#'+tab_id).addClass('active');
    $(".addCusttab").css('display','none');
    $("#"+tab_id+"Tab").css('display','block');
}
function show_mail_box() {
    var inv_id = $("#inv_id").val();
    var url = "api/invoiceProcess.php";
    $.post(url,{"type":"getSelectedInvoice","inv_id":inv_id},function(data){
        var Status = data.Status;
        if(Status === "Success"){
            var invoiceData = data.invoices;
            var email = invoiceData.contact_email;
            $("#email-id").val(email);
            $("#sendMailModal").modal("show");
        }else{
            alert("SomeThing Went Wrong Please Try After Some Time")
        }
    });
}
function sendEmail() {
    var copytoadmin = $("#copy").is(':checked');
    var inv_id = $("#inv_id").val();
    var to = $("#email-id").val();
    var cc = $("#cc").val();
    var adminMail = $("#adminMail").val();
    var message_text = $("#message-text").val();
    if(to ==='') {
        $("#err-txt").html('Please Enter Recipient E-Mail');
        return false;
    }
    $("#err-txt").html('');
    var url ='api/invoiceProcess.php';
    $.post(url,{inv_id:inv_id,to:to,cc:cc,msg:message_text,type:'sendInvoiceMail',copytoadmin:copytoadmin,adminMail:adminMail},function (data) {
        var status = data.Status;
        var message = data.Message;
        if(status === 'Success') {
            $('#err-txt').html(message);
            $('#err-txt').css('color','green');
            setTimeout(function () {
                closePreview();
            },1000);
        }
        else{
            $('#err-txt').html(message);
        }
    });
}
var today = new Date();
$("#invoice_date").datetimepicker({
    format: "MM/DD/YYYY",
    minDate: today
});
$("#invoice_due_date").datetimepicker({
    format: "MM/DD/YYYY",
    minDate: new Date()
});
$("#firstInv").datetimepicker({
    format: "MM/DD/YYYY",
    minDate: today
});
function repeatInvoice(){
    var repeat_type = $("#repeat_type").val();
    if(repeat_type === 'Monthly'){
        $("#helpingVerb").html("on the");
        $("#dayselector").hide();
        $("#dateselector").show();
        $("#helpingVerb2").html("Day of Every Month");
        $("#yearlydiv").hide();
        $("#monthselector").hide();
    }
    else if(repeat_type === 'Weekly'){
        $("#helpingVerb").html("on Every");
        $("#dateselector").hide();
        $("#dayselector").show();
        $("#helpingVerb2").html("of Every Week");
        $("#yearlydiv").hide();
        $("#monthselector").hide();
    }
    else if(repeat_type === 'Yearly'){
        $("#dateselector").hide();
        $("#dayselector").hide();
        $("#monthselector").show();
        $("#helpingVerb").html("Every");
        $("#helpingVerb2").html("on the");
        $("#yearlydiv").show();
    }
    else if(repeat_type === 'Once'){
        $("#dateselector").hide();
        $("#dayselector").hide();
        $("#monthselector").hide();
        $("#helpingVerb").html("");
        $("#helpingVerb2").html("");
        $("#yearlydiv").hide();
    }
}
function save_sch_data(sendmail){
    var repeat_type=$("#repeat_type").val();
    var inv_id=$("#inv_id").val();
    var date = "";
    var day = "";
    var month = "";
    if(repeat_type === "Monthly"){
        date = $("#dateselector").val();
    }
    else if(repeat_type === "Weekly"){
        day = $("#dayselector").val();
    }
    else if(repeat_type === "Yearly"){
        month = $("#monthselector").val();
        date = $("#yearlydateselector").val();
    }
    var firstInv = $("#firstInv").val();
    var endon = $("#endon").val();
    var timezone = $("#timezone").val();
    var url="api/invoiceProcess.php";
    $.post(url,{"type":"storeInvoiceSchedule","inv_id":inv_id,"repeat_type":repeat_type,"date":date,"day":day,"month":month
        ,"firstInv":firstInv,"endon":endon,"timezone":timezone},function(data){
        var Status = data.Status;
        if(Status === "Success") {
            if (sendmail === "yes") {
                show_mail_box()
            } else {
                window.location = 'invoice.php';
            }
        }else{
            $("#sch_error").html(data.Message);
        }
    });
}
getTax();