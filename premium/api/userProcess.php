<?php
require_once ('Classes/USERCLASS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$userClass = new \Classes\USERCLASS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $userClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];
if($type == "register")
{
    $requiredfields = array('username','email','contactNumber','address','socialType');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $username = trim($_POST['username']);
    $email = trim($_POST['email']);
    $contactNumber = trim($_POST['contactNumber']);
    $address = trim($_POST['address']);
    $socialType = trim($_POST['socialType']);
    $image_url = trim($_POST['image_url']);
    $response = $userClass->registerUser($username, $email, $contactNumber, $address,$socialType,$image_url);
    $userClass->apiResponse($response);
}
else if($type == "getParticularUserData")
{
    $requiredfields = array('userId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $response = $userClass->getParticularUserData($userId);
    $userClass->apiResponse($response);
}
else if($type == "getAllUsersData")
{
    $requiredfields = array('store_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $store_name = $_POST['store_name'];
    $response = $userClass->getAllUsersDataofStore($store_name);
    $userClass->apiResponse($response);
}
else if($type == "getAllUsersDataOfStoreUsesStyleGenie")
{
    $requiredfields = array('store_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $store_name = $_POST['store_name'];
    $response = $userClass->getAllUsersDataOfStoreUsesStyleGenie($store_name);
    $userClass->apiResponse($response);
}
else if($type == "getAllUsers") {
    $userResponse = $userClass->getAllUsers();
    if($userResponse[STATUS] == Error) {
        $userClass->apiResponse($userResponse);
        return false;
    }
    $userClass->apiResponse($userResponse);
}
else if($type == "getAllUsersMeasure") {
    $userResponse = $userClass->getAllUsersMeasure();
    if($userResponse[STATUS] == Error) {
        $userClass->apiResponse($userResponse);
        return false;
    }
    $userClass->apiResponse($userResponse);
}
else if($type == "statusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->statusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "renewalStatusChange")
{
    $requiredfields = array('user_id','value');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $value = $_REQUEST['value'];
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->renewalStatusChange($user_id,$value);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
else if($type == "deleteUser"){
    $requiredfields = array('user_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $user_id = $_REQUEST['user_id'];
    $response = $userClass->deleteUser($user_id);
    if($response[STATUS] == Error) {
        $userClass->apiResponse($response);
        return false;
    }
    $userClass->apiResponse($response);
}
///////////////////////////////////////////////////*/
else if($type == "updatePlan")
{
    $requiredfields = array('userId','plan_id','renewal_type','auto_renewal');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $_POST['userId'];
    $plan_id = $_POST['plan_id'];
    $renewal_type = $_POST['renewal_type'];
    $auto_renewal = $_POST['auto_renewal'];
    $response = $userClass->updatePlan($userId, $plan_id, $renewal_type, $auto_renewal);
    if($response['Status'] == 'Failure')
    {
        $userClass->apiResponse($response);
        return false;
    }
    $userId = $response['userId'];
    $temp = $userClass->getParticularUserData($userId);
    $response['UserData'] = $temp['UserData'];
    $response['ImagesBaseURL'] = ImagesBaseURL;
    unset($response['userId']);
    $userClass->apiResponse($response);
}
else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $userClass->apiResponse($response);
}
?>