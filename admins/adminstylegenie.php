<?php
include('header.php');
?>
<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Check Measurement<small></small></h2>
                    </div>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of User's Having Check Measurement Used.
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>S.No</th>
                                <th>User Profile</th>
                                <th>User Name</th>
                                <th>User E-Mail</th>
                                <th>User Contact</th>
                                <th>User Gender</th>
                                <th>User D.O.B</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody id="userData"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    function loadUsersData(){
        var url = "api/userProcess.php";
        $.post(url,{"type":"getAllUsers"},function(data){
            var Status = data.Status;
            var dataShow = "";
            var ids = [];
            if(Status === "Success"){
                var userData = data.usersData;
                for(var i=0;i<userData.length;i++){
                    dataShow = dataShow + "<tr><td>"+(i+1)+"</td><td><img alt='Image' src='" + userData[i].user_profile + "' " +
                        "style='height:40px !important;width:40px;border:2px solid #333'/></td><td>" +
                        userData[i].fname+" "+userData[i].lname + "</td><td>" + userData[i].email + "</td><td>" +
                        userData[i].contact + "</td><td>" + userData[i].gender + "</td>" +
                        "<td>" + userData[i].dob + "</td><td><button class='btn btn-sm btn-info' " +
                        "onclick=checkstylegenie('" + userData[i].user_id + "')>Check Style Genie</button></td></tr>";
                }
            }
            else{
                dataShow = "<tr><td colspan=8 style='text-align:center'>"+data.Message+"</td></tr>";
            }
            $("#userData").html(dataShow);
        }).fail(function(){
            showError("Server Error !!! Please Try After Some Time...","red");
        });
    }
    function checkstylegenie(user_id){
        window.location='adminsdet.php?_='+user_id;
    }
    loadUsersData();
</script>