<?php
include('header.php');
include("admin/webserver/database/db.php");
?>
</div>
<div class="about-bottom wthree-3">
<style>
.top-bar {
    background: rgba(0, 0, 0, 0.50) none repeat scroll 0 0;
}
.active {
    border: 2px solid #555;
    border-radius: 4px;
}
    .primarysuggestion {
        height: auto;
        margin-left: 10px;
        margin-top: 10px;
        width: auto;
    }

   .steps {
    background: #fff none repeat scroll 0 0;
    border: 2px solid #aeaeae;
    border-radius: 7%;
    cursor: pointer;
    float: left;
    font-size: 13px;
    line-height: 18px;
    margin-bottom: 20px;
    margin-left: 2%;
    margin-top: 10px;
    max-height: 57px;
    padding-top: 9px;
    text-align: center;
    width: 10%;
}

   .stepsactive {
    background: #c4996c none repeat scroll 0 0;
    border: 2px solid #c4996c;
    color: white;
}

    .breakingline {
        width: 525px;
		margin-top: 38px;
    }
    .outputsugg {
         position: absolute;
         margin-left: -70px;
         z-index:-1;
    }
    .backbtnsugg
    {
        height:68%;
    }
    .backbtnsuggparentdiv
    {
        height: 130px;
        width: 130px;
    }
    .backbtnsuggparentdiv p
    {
        font-weight:normal;
        font-size: 12px;
        line-height: 18px!important;
    }
    #forcasual .primarysuggestion > img
    {
        padding: 40px;
    }
	#frontpockets{overflow-y: scroll; height: 660px; padding-top: 22px;}
</style>
<div class="loader" style="display: none">
    <img src="images/spin.gif" class="spinloader"/>
</div>
<div class="col-md-12">
    <div class="col-md-6">
        <div class="col-md-1">
            <label onclick="rotateImage('left')" id="rotateLeft" style="margin-top: 350px"><i class="fa fa-share fa-2x"></i></label>
        </div>
        <div class="col-md-10" style="padding-top: 150px;position: relative">
            <img src=""  class="outputsugg" id="pantimg" alt=''/>
            <img src=""  class="outputsugg" id="beltimg" alt=''/>
            <img src=""  class="outputsugg" id="beltloopimg" alt=''/>
            <img src=""  class="outputsugg" id="pleatimg" alt=''/>
            <img src=""  class="outputsugg" id="frontpocketimg" alt=''/>
            <img src=""  class="outputsugg" id="backpocketimg" alt=''/>
            <img src=""  class="outputsugg" id="pantbottomimg" alt=''/>
        </div>
        <div class="col-md-1">
            <label onclick="rotateImage('right')" id="rotateRight" style="margin-top: 350px"><i class="fa fa-reply fa-2x"></i></label>
        </div>
    </div>
    <div class="col-md-6">
        <div class="col-md-12" style="padding:50px 0">
            <div class="steps stepsactive" id="category">Pant Category</div>
            <div class="steps" id="frontpocket">Front Pocket</div>
			<div class="steps" id="pleatstyle">Pleats Style</div>
			<div class="steps" id="watchpocket">Watch Pocket</div>
            <div class="steps" id="backpocket">Back Pocket</div>
            <div class="steps" id="beltloop">Belt <br>Loop</div>
            <div class="steps" id="pantbottom">Pant Bottom</div>
            <div class="steps" id="pantcolor">Pant <br>Color</div>
            <div class="breakingline"></div>
        </div>
        <!--
        ------
        -----category div
        ------
        -------->
        <div class="col-md-12" id="categorys" style="display: block">
            <p class="stylecaption">Pant Category</p>

            <div class="primarysuggestion active" id="category_formal" name="formal" style="margin-left: 140px">
                <img src="images/pants/formal.jpg"/>
                <p class="suggestionname">Formal</p>
            </div>
            <div class="primarysuggestion" id="category_casual"  name="casual"  style="margin-left: 10px">
                <img src="images/pants/casual.jpg"/>
                <p class="suggestionname">Casual</p>
            </div>
            <!--<div class="primarysuggestion" id="category_tuxedo" style="margin-left: 10px">
                <img src="images/pants/tuxedo.jpg"/>
                <p class="suggestionname">Tuxedo</p>
            </div>-->
        </div>
        <!--
       ------
       -----front pocket div
       ------
       -------->
        <div class="col-md-12" id="frontpockets" style="display:none">
            <p class="stylecaption">Front Pocket Style</p>
            <div id="forcasual" style="display: none">
                <div class="primarysuggestion" id="frontpocket_arcjeanspocket" name="arcjeanspocket">
                    <img src="images/pants/arcjeanspocket.png"/>
                    <p class="suggestionname">Arc Jeans Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_diamondjeanspocket" name="diamondjeanspocket">
                    <img src="images/pants/diamondjeanspocket.png"/>
                    <p class="suggestionname">Diamond Jeans Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_squarejeanspocket" name="squarejeanspocket">
                    <img src="images/pants/squarejeanspocket.png"/>
                    <p class="suggestionname">Square Jeans Pocket</p>
                </div>
                <div class="primarysuggestion" id="frontpocket_roundjeanspocket" name="roundjeanspocket">
                    <img src="images/pants/roundjeanspocket.png"/>
                    <p class="suggestionname">Round Jeans Pocket</p>
                </div>
            </div>
            <div class="primarysuggestion active " id="frontpocket_nopocket" name="T-313C">
                <img src="images/pants/nopocket.png"/>
                <p class="suggestionname">T-313C</br>No Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_20cmslantpocket" name="T-3100">
                <img src="images/pants/20cmslantpocket.png"/>
                <p class="suggestionname">T-3100</br>2.0 cm Slant Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_25slantpocket" name="T-3104">
                <img src="images/pants/25slantpocket.png"/>
                <p class="suggestionname">T-3104</br>2.5 cm Slant Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_32cmslantpocket" name="T-3101">
                <img src="images/pants/32cmslantpocket.png"/>
                <p class="suggestionname">T-3101</br>3.2 cm Slant Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_51cmslantpocket" name="T-3102">
                <img src="images/pants/51cmslantpocket.png"/>
                <p class="suggestionname">T-3102</br>5.1 cm Slant Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_doublebesompocket" name="T-3131">
                <img src="images/pants/doublebesompocket.png"/>
                <p class="suggestionname">T-3131</br>Double Besom Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_sideseampocket" name="T-3130">
                <img src="images/pants/sideseampocket.png"/>
                <p class="suggestionname">T-3130</br>Side Seam Pocket</p>
            </div>
            <div class="primarysuggestion" id="frontpocket_weltpocket" name="T-3132">
                <img src="images/pants/weltpocket.png"/>
                <p class="suggestionname">T-3132</br>Welt Pocket</p>
            </div>
			<!-------disabled------------>
			<div class="primarysuggestion" id="frontpocket_diamondjeanspocket_1" name="T-3134">
                <img src="images/pants/diamondjeanspocket.png"/>
                <p class="suggestionname">T-3134</br>Diamond Jeans Pocket</p>
            </div>
			<div class="primarysuggestion" id="frontpocket_arcjeanspocket_1" name="T-3135">
                <img src="images/pants/arcjeanspocket.png"/>
                <p class="suggestionname">T-3135</br>Arc Jeans Pocket</p>
            </div>
			<div class="primarysuggestion" id="frontpocket_veryslantpocketwithsrip_1" name="T-3124">
                <img src="images/pants/veryslantpocketwithsrip.png"/>
                <p class="suggestionname">T-3124</br>Very Slant Pocket with Strip</p>
            </div>
			<div class="primarysuggestion" id="frontpocket_veryslantpocket_1" name="T-3103">
                <img src="images/pants/veryslantpocket.png"/>
                <p class="suggestionname">T-3103</br>Very Slant Pocket</p>
            </div>
			<div class="primarysuggestion" title="2.0 cm Slant Pocket with Strip" id="frontpocket_20cmslantpocketwithstrip_1" name="T-3120">
                <img src="images/pants/20cmslantpocketwithstrip.png"/>
                <p class="suggestionname">T-3120</br>2.0 cm Slant Pocket with..</p>
            </div>
			<div class="primarysuggestion" title="3.2 cm Slant Pocket with Strip" id="frontpocket_32cmslantpocketwithstrip_1" name="T-3121">
                <img src="images/pants/32cmslantpocketwithstrip.png"/>
                <p class="suggestionname">T-3121</br>3.2 cm Slant Pocket with..</p>
            </div>
			<div class="primarysuggestion" title="3.2 cm Slant Pocket with Strip" id="frontpocket_32cmslantpocketwithstrip_1" name="T-3121">
                <img src="images/pants/32cmslantpocketwithstrip.png"/>
                <p class="suggestionname">T-3121</br>3.2 cm Slant Pocket with..</p>
            </div>
			
        </div>  
		<!--
       ------
       -----Pleats style div
       ------
       -------->
        <div class="col-md-12" id="pleatstyles" style="display:none">
            <p class="stylecaption">Pleats Style</p>
            <div id="forcasual" style="display: none">
               
            </div>
			<!-------disabled------------>
            <div class="primarysuggestion" id="pleatstyle_plainfront" name="T-3020">
                <img src="images/pants/plainfront.png"/>
                <p class="suggestionname">T-3020</br>Plain Front</p>
            </div>
		
			<div class="primarysuggestion" title="Single Pleat Forward Sideseam" id="pleatstyle_singlepleatforwardsideseam_1" name="T-3021">
                <img src="images/pants/singlepleatforwardsideseam.png"/>
                <p class="suggestionname">T-3021</br>Single Pleat Forward..</p>
            </div>
			
			<div class="primarysuggestion" id="pleatstyle_singlepleatforwardfly_1" name="T-3022">
                <img src="images/pants/singlepleatforwardfly.png"/>
                <p class="suggestionname">T-3022</br>Single Pleat Forward Fly</p>
            </div>
			
			<div class="primarysuggestion" id="pleatstyle_doublepleatforwardsideseam_1" title="Double Pleat Forward Sideseam" name="T-3023">
                <img src="images/pants/doublepleatforwardsideseam.png"/>
                <p class="suggestionname">T-3023</br>Double Pleat Forward..</p>
            </div>
			
			<div class="primarysuggestion" title="One Opposite Pleat one Pleat forward Sideseam" id="pleatstyle_oneoppositepleatonepleatforwardsideseam_1" name="T-3028">
                <img src="images/pants/oneoppositepleatonepleatforwardsideseam.png"/>
                <p class="suggestionname">T-3028</br>One Opposite Pleat one..</p>
            </div>
			
			<div class="primarysuggestion" title="One Opposite Pleat one Pleat Forward Fly" id="pleatstyle_oneoppositepleatonepleatforwardfly_1" name="T-3029">
                <img src="images/pants/oneoppositepleatonepleatforwardfly.png"/>
                <p class="suggestionname">T-3029</br>One Opposite Pleat one..</p>
            </div>
			
			<div class="primarysuggestion" title="One Pleat Forward Sideseam one Opposite Pleat" id="pleatstyle_onepleateforwardsideseamoneoppositepleat_1" name="T-302A">
                <img src="images/pants/onepleateforwardsideseamoneoppositepleat.png"/>
                <p class="suggestionname">T-302A</br>One Pleat Forward Side..</p>
            </div>
		
			<div class="primarysuggestion" title="One Pleat Forward Fly One Opposite Pleat" id="pleatstyle_onepleateforwardflyoneoppositepleat_1"  name="T-302B">
                <img src="images/pants/onepleateforwardflyoneoppositepleat.png"/>
                <p class="suggestionname">T-302B</br>One Pleat Forward Fly..</p>
            </div>
			
			<div class="primarysuggestion"  id="pleatstyle_doublepleatsforwardfly_1" title="Double Pleats Forward Fly"  name="T-3024">
                <img src="images/pants/doublepleatsforwardfly.png"/>
                <p class="suggestionname">T-3024</br>Double Pleats Forward..</p>
            </div>
			
			<div class="primarysuggestion"  id="pleatstyle_oneoppositepleat_1"  name="T-3027">
                <img src="images/pants/oneoppositepleat.png"/>
                <p class="suggestionname">T-3027</br>One Opposite Pleat</p>
            </div>
			
			<div class="primarysuggestion" id="pleatstyle_oneboxpleat_1"  name="T-302P">
                <img src="images/pants/oneboxpleat.png"/>
                <p class="suggestionname">T-302P</br>One Box Pleat</p>
            </div>
		
        </div>
		<!--
       ------
       -----watch pocket style div
       ------
       -------->
        <div class="col-md-12" id="watchpockets" style="display:none">
            <p class="stylecaption">Watch Pocket Style</p>
            <div id="forcasual" style="display: none">
               
            </div>
			<!-------disabled------------>
            <div class="primarysuggestion" id="watchpocket_normalleftwatchpocket_1" name="T-3170">
                <img src="images/pants/normalleftwatchpocket.png"/>
                <p class="suggestionname">T-3170</br>Normal Left Watch Pocket</p>
            </div>

			<div class="primarysuggestion" id="watchpocket_leftbesomwatchpocket_1" name="T-3171">
                <img src="images/pants/leftbesomwatchpocket.png"/>
                <p class="suggestionname">T-3171</br>Left Besom Watch Pocket</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_watchleftpocketwithdiamondflap_1" title="Left Watch Pocket with Diamond Flap" name="T-3174">
                <img src="images/pants/watchleftpocketwithdiamondflap.png"/>
                <p class="suggestionname">T-3174</br>Left Watch Pocket with..</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_rightwatchpocketwithsquareflap_1" title="Right watch Pocket with Square Flap" name="T-3175">
                <img src="images/pants/rightwatchpocketwithsquareflap.png"/>
                <p class="suggestionname">T-3175</br>Right watch Pocket with..</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_leftdobublebesomwatchpocket_1" title="Left Double Besom watch Pocket" name="T-3178">
                <img src="images/pants/leftdobublebesomwatchpocket.png"/>
                <p class="suggestionname">T-3178</br>Left Double Besom..</p>
            </div>
		
			<div class="primarysuggestion" id="watchpocket_rightweltwatchpocket_1" title="Left Double Besom watch Pocket" name="T-3181">
                <img src="images/pants/rightweltwatchpocket.png"/>
                <p class="suggestionname">T-3181</br>Right Welt watch Pocket</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_normalrightwatchpocket_1" title="Normal Right watch Pocket" name="T-3180">
                <img src="images/pants/normalrightwatchpocket.png"/>
                <p class="suggestionname">T-3180</br>Normal Right watch Pocket</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_rightwatchpocketwithtrangleflap_1" title="Right watch Pocket with Trangle Flap" name="T-3182">
                <img src="images/pants/rightwatchpocketwithtrangleflap.png"/>
                <p class="suggestionname">T-3182</br>Right watch Pocket..</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_rightwatchpocketwithdiomondflap_1" title="Right watch Pocket with Diomond Flap" name="T-3184">
                <img src="images/pants/rightwatchpocketwithdiomondflap.png"/>
                <p class="suggestionname">T-3184</br>Right watch Pocket..</p>
            </div>
			
			<div class="primarysuggestion" id="watchpocket_doublebesomrightwatchpocket_1" title="Double Besom Right Watch Pocket" name="T-3188">
                <img src="images/pants/doublebesomrightwatchpocket.png"/>
                <p class="suggestionname">T-3188</br>Double Besom Right..</p>
            </div>
        </div>
        <!------
        -----back pocket div
        ------
        -------->
        <div class="col-md-12" id="backpockets" style="display:none">
            <p class="stylecaption">Back Pocket Style</p>

            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_nobackpocket" name="nobackpocket">
                <img src="images/pants/nobackpocket.png" class="backbtnsugg" />
                <p class="suggestionname">No Back Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv active" id="backpocket_diamondflapbackpocket" name="diamondflapbackpocket">
                <img src="images/pants/diamond_flap_back_pocket.png" class="backbtnsugg"/>
                <p class="suggestionname">Dianond Flap Back Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_diamondflapwithbutton" name="diamondflapwithbutton">
                <img src="images/pants/diamond_flap_with_button.png" class="backbtnsugg"/>
                <p class="suggestionname">Diamond Flap With Button</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesombackpocket" name="doublebesombackpocket">
                <img src="images/pants/double_besom_back_pocket.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom Back Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandbuttonhole" name="doublebesomwithbuttonandbuttonhole">
                <img src="images/pants/doublebesomwithbuttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom With Button Hole</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandloop" name="doublebesomwithbuttonandloop">
                <img src="images/pants/double_besom_with_button_loop.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom With Button Loop</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandpointtab" name="doublebesomwithbuttonandpointtab">
                <img src="images/pants/double_besom_with_button_point_tab%20.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom With Button And Point Tab</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_doublebesomwithbuttonandsquaretab" name="doublebesomwithbuttonandsquaretab">
                <img src="images/pants/double_besom_with_button_square_tab.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Besom With Button And Square Tab</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_flapwithbutton" name="flapwithbutton">
                <img src="images/pants/flap_with_button.png" class="backbtnsugg"/>
                <p class="suggestionname">Flap With Button</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_pointedflapbackpocket"  name="pointedflapbackpocket">
                <img src="images/pants/pointed_flap_back_pocket.png" class="backbtnsugg"/>
                <p class="suggestionname">Pointed Flap Back Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_pointedflapwithbutton"  name="pointedflapwithbutton">
                <img src="images/pants/pointed_flap_with_button.png" class="backbtnsugg"/>
                <p class="suggestionname">Pointed Flap With Button</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_standardflapbackpocket"  name="standardflapbackpocket">
                <img src="images/pants/standard_flap_back_pocket.png" class="backbtnsugg"/>
                <p class="suggestionname">Standard Flap Back Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_weltbackpocket"  name="weltbackpocket">
                <img src="images/pants/weltbackpocket.png" class="backbtnsugg"/>
                <p class="suggestionname">Welt Back Pocket</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="backpocket_weltpocketwithbuttonandbuttonhole"  name="weltpocketwithbuttonandbuttonhole">
                <img src="images/pants/welt_pocket_with_button_buttonhole.png" class="backbtnsugg"/>
                <p class="suggestionname" title="Welt Pocket With Button and Button Hole">Welt Pocket With Button and...</p>
            </div>
        </div>
        <!------
        -----belt loop div
        ------
        -------->
        <div class="col-md-12" id="beltloops" style="display:none">
            <p class="stylecaption">Belt Loop Style</p>

            <div class="primarysuggestion backbtnsuggparentdiv active" id="beltloop_nobeltloop" name="nobeltloop">
                <img src="images/pants/nobeltloop.png" class="backbtnsugg"/>
                <p class="suggestionname">No Belt Loop</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_standardbeltloop" name="standardbeltloop">
                <img src="images/pants/standardbeltloop.png" class="backbtnsugg"/>
                <p class="suggestionname">Standard Belt Loop</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv " id="beltloop_doublebeltloop" name="doublebeltloop">
                <img src="images/pants/doublebeltloop.png" class="backbtnsugg"/>
                <p class="suggestionname">double Belt Loop</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_doubleslantbeltloop" name="doubleslantbeltloop">
                <img src="images/pants/doubleslantbeltloop.png" class="backbtnsugg"/>
                <p class="suggestionname">Double Slant Belt Loop</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_normal" name="normal">
                <img src="images/pants/normal.png" class="backbtnsugg"/>
                <p class="suggestionname">Normal</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_20" name="20">
                <img src="images/pants/20.png" class="backbtnsugg"/>
                <p class="suggestionname">2.0</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_38" name="38">
                <img src="images/pants/38.png" class="backbtnsugg"/>
                <p class="suggestionname">3.8</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_50" name="50">
                <img src="images/pants/50.png" class="backbtnsugg"/>
                <p class="suggestionname">5.0</p>
            </div>
            <div class="primarysuggestion backbtnsuggparentdiv" id="beltloop_beltloopwidth32cm" name="beltloopwidth32cm">
                <img src="images/pants/beltloopwidth32cm.png" class="backbtnsugg"/>
                <p class="suggestionname">Belt Loop Width 3.2cm</p>
            </div>
        </div>
        <!------
        -----pant bottom div
        ------
        -------->
        <div class="col-md-12" id="pantbottoms" style="display:none">
            <p class="stylecaption">Pant Bottom Style</p>

            <div class="primarysuggestion active" id="pantbottom_32cmcuff" name="32cmcuff">
                <img src="images/pants/32cmcuff.png"/>
                <p class="suggestionname">3.2cm Cuff</p>
            </div>
            <div class="primarysuggestion" id="pantbottom_38cmcuff" name="38cmcuff">
                <img src="images/pants/38cmcuff.png"/>
                <p class="suggestionname">3.8cm Cuff</p>
            </div>
            <div class="primarysuggestion" id="pantbottom_44cmcuff" name="44cmcuff">
                <img src="images/pants/44cmcuff.png"/>
                <p class="suggestionname">4.4cm Cuff</p>
            </div>
            <div class="primarysuggestion" id="pantbottom_51cmcuff" name="51cmcuff">
                <img src="images/pants/51cmcuff.png"/>
                <p class="suggestionname">5.1cm Cuff</p>
            </div>
        </div>
        <!------
        -----pant color div
        ------
        -------->
        <div class="col-md-12" id="pantcolors" style="display:none">
            <p class="stylecaption">Pant Color Style</p>
            <div class="col-md-12" style="height: 400px;overflow-y: scroll;padding: 0">
                <?php
                $query = "select * from wp_colors where type='pant'";
                $result = mysql_query($query);
                if($result)
                {
                    $num = mysql_num_rows($result);
                    if($num>0)
                    {
                        $status = "done";
                        $a=0;
                        while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            $displayImage = $rows['displayImage'];
                            ?>
                            <div class='primarysuggestion colorsuggestion<?php if($a == 1) echo ' active' ?>' name='<?php echo $colorName ?>' id='pantcolor_<?php echo $colorName ?>'>
                                <img title="<?php echo $displayImage ?> " src='images/pants/<?php echo $displayImage ?>' />
                            </div>
                            <?php
                        }
                    }
                    else
                    {
                        echo "No Color Found";
                    }
                }
                else
                {
                    echo mysql_error();
                }
                ?>
            </div>
        </div>
    </div>
    <?php
    $query = "select * from categories where cat_type='Pants' order by cat_id ASC";
    $result = mysql_query($query);
    if($result)
    {
        $num = mysql_num_rows($result);
        if($num>0)
        {
            while($rows = mysql_fetch_array($result))
            {
                $product_amount = $rows['cat_price'];
                ?>
                <input type="hidden" id="product_amount" value="<?php echo $product_amount;?>">
                <?php
            }
        }
        else
        {
            echo "";
        }
    }
    else
    {
        echo mysql_error();
    }
    ?>
    <div class="col-md-12" style="text-align:center;margin-top: 100px">
        <label id="previous" onclick="navigation('previous')" class="prevnextbtn">Previous</label>
        <label id="next" onclick="navigation('next')" class="prevnextbtn">Next</label>
    </div>
    <script>
	$( document ).ready(function() {
		$(".nav a").removeClass("active");
	});
        var currPage = "";
        var category = "formal";
        var frontpocket = "20cmslantpocket";
        var pleatstyle = "plainfront";
        var watchpocket = "normalleftwatchpocket";
        var backpocket = "diamondflapbackpocket";
        var belt = "belt";
        var pleat = "pleat";
        var beltloop = "standardbeltloop";
        var pantbottom = "32cmcuff";
        var pantcolor = "dbk053a";
        var folder = "front";
        function loadSessionData() {
            $(".loader").fadeIn("slow");
            var prevIndex = $(".stepsactive").attr("id");
            currPage = prevIndex + "s";
            var url = "admin/webserver/selectionData.php?type=getsession&" + prevIndex + "=" + prevIndex;
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                var status = json.status;
                if (status == "done") {
                    var img = "";
                    var item = json.items;
                    var name = json.items;
                    item = prevIndex + "_" + item;
                    $(".primarysuggestion").removeClass("active");
                    $("#" + item).addClass("active");
                    if (prevIndex == "category") {
                        category = name;
                    }
                    else if (prevIndex == "frontpocket") {
                        frontpocket = name;
                    }
                    else if (prevIndex == "pleatstyle") {
                        pleatstyle = name;
                    }
					else if (prevIndex == "watchpocket") {
                        watchpocket = name;
                    }
					else if (prevIndex == "backpocket") {
                        backpocket = name;
                    }
                    else if (prevIndex == "beltloop") {
                        beltloop = name;
                    }
                    else if (prevIndex == "pantbottom") {
                        pantbottom = name;
                    }
                    else if (prevIndex == "pantcolor") {
                        pantcolor = name;
                    }
                    if (currPage == "categorys") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            if(frontpocket == "20cmslantpocket" || frontpocket == "25slantpocket" || frontpocket == "32cmslantpocket"
                            || frontpocket == "51cmslantpocket" || frontpocket == "doublebesompocket" || frontpocket == "sideseampocket"
                            || frontpocket == "weltpocket" || frontpocket == "nopocket")
                            {
                                frontpocket = "arcjeanspocket";
                                $("#frontpocket_arcjeanspocket").addClass("active");
                            }
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "frontpockets") {
                        folder = "side";
                        if(category == "casual")
                        {
                            $("#forcasual").show();
                        }
                        else if(category == "formal")
                        {
                            $("#forcasual").hide();
                            if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                             frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                             {
                                frontpocket = "20cmslantpocket";
                                $("#frontpocket_20cmslantpocket").addClass("active");
                             }
                        }
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "pleatstyles") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "watchpockets") {
                        $(".loader").fadeOut("slow");
                    }
					else if (currPage == "backpockets") {
                        folder = "back";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src","images/pants/sugg/bawli.png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "beltloops") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "pantbottoms") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        $(".loader").fadeOut("slow");
                    }
                    else if (currPage == "pantcolors") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");

                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        $(".loader").fadeOut("slow");
                    }
                }
                else {
                    var url = "";
                    if (currPage == "categorys") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            if(frontpocket == "20cmslantpocket" || frontpocket == "25slantpocket" || frontpocket == "32cmslantpocket"
                            || frontpocket == "51cmslantpocket" || frontpocket == "doublebesompocket" || frontpocket == "sideseampocket"
                            || frontpocket == "weltpocket" || frontpocket == "nopocket")
                            {
                                frontpocket = "arcjeanspocket";
                                $("#frontpocket_arcjeanspocket").addClass("active");
                            }
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + category;
                    }
                    else if (currPage == "frontpockets") {
                        folder = "side";
                        if(category == "casual")
                        {
                            $("#forcasual").show();
                        }
                        else if(category == "formal")
                        {
                            $("#forcasual").hide();
                            if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                             frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                             {
                                frontpocket = "20cmslantpocket";
                                $("#frontpocket_20cmslantpocket").addClass("active");
                             }
                        }
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + frontpocket;
			
                    }
					else if (currPage == "pleatstyles") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + pleatstyle;
                    }
					else if (currPage == "watchpockets") {
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + watchpocket;
                    }
                    else if (currPage == "backpockets") {
                        folder = "back";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src","images/pants/sugg/bawli.png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + backpocket;
                    }
					
                    else if (currPage == "beltloops") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + beltloop;
                    }
                    else if (currPage == "pantbottoms") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + pantbottom;
                    }
                    else if (currPage == "pantcolors") {
                        folder = "front";
                        $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                        $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                        url = "admin/webserver/selectionData.php?" + prevIndex + "=" + pantcolor;
                    }
					
                    $.get(url, function (data) {
                        var json = $.parseJSON(data);
                        var status = json.status;
                        if (status == "done") {
                            $(".loader").fadeOut("slow");
                        }
                    }).fail(function () {
                        alert("page is not available");
                    });
                }
            });
        }
        $(".primarysuggestion").click(function () {
            var Img = "";
            $(".loader").fadeIn("slow");
            $(".primarysuggestion").removeClass("active");
            var value = this.id;
            var name = $("#"+value).attr("name");
            var type = value.split("_");
            var selection = type[1];
            $("#" + value).addClass("active");
            var currpage = type[0] + "s";
            switch(type[0]) {
                case "category":
                    category = name;
                    break;
                case "frontpocket":
                    frontpocket = name;
                    break;

                case "pleatstyle":
                    pleatstyle = name;
                    break;

                case "watchpocket":
                    watchpocket = name;
                    break;

                case "backpocket":
                    backpocket = name;
                    break;

                case "beltloop":
                    beltloop = name;
                    break;

                case "pantbottom":
                    pantbottom = name;
                    break;

                case "pantcolor":
                    pantcolor = name;
                    break;
            }
            if (currpage == "categorys") {
                folder = "front";
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                if(category == "casual")
                {
                    if(frontpocket == "20cmslantpocket" || frontpocket == "25slantpocket" || frontpocket == "32cmslantpocket"
                    || frontpocket == "51cmslantpocket" || frontpocket == "doublebesompocket" || frontpocket == "sideseampocket"
                    || frontpocket == "weltpocket" || frontpocket == "nopocket")
                    {
                        frontpocket = "arcjeanspocket";
                        $("#frontpocket_arcjeanspocket").addClass("active");
                    }
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                }
                else
                {
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                }
                $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "frontpockets") {
                folder = "side";
                if(category == "casual")
                {
                    $("#forcasual").show();
                }
                else if(category == "formal")
                {
                    $("#forcasual").hide();
                    if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                     frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                     {
                        frontpocket = "20cmslantpocket";
                        $("#frontpocket_20cmslantpocket").addClass("active");
                     }
                }
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "pleatstyles") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "watchpockets") {
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "backpockets") {
                folder = "back";
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                $("#frontpocketimg").attr("src","images/pants/sugg/bawli.png");
                $("#backpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + backpocket + ".png");
                $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "beltloops") {
                folder = "front";
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                if(category == "casual")
                {
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                }
                else
                {
                    $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                }
                $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "pantbottoms") {
                folder = "front";
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                if(category == "casual")
                {
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                }
                else
                {
                    $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                }
                $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                $(".loader").fadeOut("slow");
            }
            else if (currpage == "pantcolors") {
                folder = "front";
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                if(category == "casual")
                {
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + frontpocket + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                }
                else
                {
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                    $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pleat + ".png");
                }
                $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                $(".loader").fadeOut("slow");
            }
        else{
            $(".loader").fadeOut("slow");
        }

        });
        function navigation(switcher) {
            $("#" + currPage).fadeOut("slow");
            if (switcher == "next") {
                if (currPage == "categorys") {
                    $(".steps").removeClass("stepsactive");
                    $("#frontpockets").fadeIn("slow");
                    $("#frontpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "frontpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#pleatstyles").fadeIn("slow");
                    $("#pleatstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "pleatstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#watchpockets").fadeIn("slow");
                    $("#watchpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "watchpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#backpockets").fadeIn("slow");
                    $("#backpocket").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "backpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#beltloops").fadeIn("slow");
                    $("#beltloop").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "beltloops") {
                    $(".steps").removeClass("stepsactive");
                    $("#pantbottoms").fadeIn("slow");
                    $("#pantbottom").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "pantbottoms") {
                    $(".steps").removeClass("stepsactive");
                    $("#pantcolors").fadeIn("slow");
                    $("#pantcolor").addClass("stepsactive");
                    $("#next").html("Add to Cart");
                    loadSessionData();
                }
                else if (currPage == "pantcolors") {
                    var user_id = $("#user_id").val();
                    var product_amount = $("#product_amount").val();
                    //alert(user_id);
                    var randomnum = Math.floor((Math.random() * 600000) + 1);
                    var product_name = "CustomPant" + randomnum;
                    var url = "admin/webserver/addto_cart.php?product_type=Custom Pant&product_name=" + product_name +
                    "&pant_category="+category+"&front_pocket=" + frontpocket + "&back_pocket=" + backpocket + "&belt_loop=" + beltloop
                    + "&pant_color=" + pantcolor + "&pant_bottom="+pantbottom+ "&product_price=" + product_amount
                    + "&user_id=" + user_id + "&quantity=" + 1 + "&cart_product_total_amount=" + product_amount;
                    $.get(url, function (data) {
                        var json = $.parseJSON(data)
                        var status = json.status;
                        if (status == "done") {
                            window.location = "cart.php";
                        }
                        else {
                            alert(status);
                        }
                    });
                }
            }
            else if (switcher == "previous") {
                if (currPage == "categorys") {
                    window.location = "customize.php";
                }
                else if (currPage == "frontpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#categorys").fadeIn("slow");
                    $("#category").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "pleatstyles") {
                    $(".steps").removeClass("stepsactive");
                    $("#frontpockets").fadeIn("slow");
                    $("#frontpocket").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "watchpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#pleatstyles").fadeIn("slow");
                    $("#pleatstyle").addClass("stepsactive");
                    loadSessionData();
                }
				else if (currPage == "backpockets") {
                    $(".steps").removeClass("stepsactive");
                    $("#watchpockets").fadeIn("slow");
                    $("#watchpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "beltloops") {
                    $(".steps").removeClass("stepsactive");
                    $("#backpockets").fadeIn("slow");
                    $("#backpocket").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "pantbottoms") {
                    $(".steps").removeClass("stepsactive");
                    $("#beltloops").fadeIn("slow");
                    $("#beltloop").addClass("stepsactive");
                    loadSessionData();
                }
                else if (currPage == "pantcolors") {
                    $(".steps").removeClass("stepsactive");
                    $("#pantbottoms").fadeIn("slow");
                    $("#pantbottom").addClass("stepsactive");
                    $("#next").html("Next");
                    loadSessionData();
                }
            }
        }
        function rotateImage(button) {
            $(".loader").fadeIn("slow");
            var source=[];
            source.push($("#pantimg").attr("src"));
            source.push($("#beltimg").attr("src"));
            source.push($("#beltloopimg").attr("src"));
            source.push($("#pleatimg").attr("src"));
            source.push($("#frontpocketimg").attr("src"));
            source.push($("#backpocketimg").attr("src"));
            source.push($("#pantbottomimg").attr("src"));
            /*alert(JSON.stringify(source));
            return false;*/
            for(var i = 0;i<source.length;i++) {
                if (source[i].indexOf("front/") >=0){
                    if (button == "left") {
                        $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+category+".png");
                        $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+belt+".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+beltloop+".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+backpocket+".png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+pantbottom+".png");
                    }
                    else if (button == "right") {
                        $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+category+".png");
                        $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+belt+".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+beltloop+".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                         if(category == "formal")
                        {
                             if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                             frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                             {
                                frontpocket = "20cmslantpocket";
                             }
                        }
                        $("#frontpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+frontpocket+".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+backpocket+".png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                }
                else if (source[i].indexOf("side/") >= 0) {
                    if (button == "left") {
                        $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+category+".png");
                        $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+belt+".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+beltloop+".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+pantbottom+".png");
                    }
                    else if (button == "right") {
                        $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+category+".png");
                        $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+belt+".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+beltloop+".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+backpocket+".png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/back/"+pantbottom+".png");
                    }
               }
                else if (source[i].indexOf("back/") >= 0) {
                    if (button == "left") {
                        $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+category+".png");
                        $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+belt+".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+beltloop+".png");
                        $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        if(category == "formal")
                        {
                             if(frontpocket == "arcjeanspocket" || frontpocket == "roundjeanspocket" ||
                             frontpocket == "diamondjeanspocket" || frontpocket == "squarejeanspocket")
                             {
                                frontpocket = "20cmslantpocket";
                             }
                        }
                        $("#frontpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+frontpocket+".png");
                        $("#backpocketimg").attr("src", "images/pants/sugg/"+pantcolor+"/side/"+backpocket+".png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/bawli.png");
                    }
                    else if (button == "right") {
                        $("#pantimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+category+".png");
                        $("#beltimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+belt+".png");
                        $("#beltloopimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+beltloop+".png");
                        if(category == "casual")
                        {
                            $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + frontpocket + ".png");
                            $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        else
                        {
                            $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/front/" + pleat + ".png");
                            $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        }
                        $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                        $("#pantbottomimg").attr("src", "images/pants/sugg/"+pantcolor+"/front/"+pantbottom+".png");
                    }
                }
            }
            $(".loader").fadeOut("slow");
        }
        $(".steps").click(function(){
        $("#"+currPage).hide(1000);
        var newcurrpage = this.id+"s";
        $(".steps").removeClass("stepsactive");
        $("#"+this.id).addClass("stepsactive");
        $("#"+newcurrpage).show(1000);
        currPage = newcurrpage;
        loadSessionData();
        if(currPage == "pantcolors")
        {
            $("#next").html("Add to Cart");
        }
        else
        {
            $("#next").html("Next");
        }
    });
        function showcolordiv()
        {
            $(".loader").fadeIn("slow");
            $("#categorys").hide();
            $("#frontpockets").hide();
            $("#pleatstyles").hide();
            $("#watchpockets").hide();
            $("#backpockets").hide();
            $("#beltloops").hide();
            $("#pantbottoms").hide();
            $("#pantcolors").show();
            $(".steps").removeClass("stepsactive");
            $("#pantcolor").addClass("stepsactive");
            currPage = "pantcolors";
            $("#next").html("Add to Cart");
            var url="admin/webserver/selectionData.php?type=getParicularSessionpant";
            $.get(url, function (data) {
                var json = $.parseJSON(data);
                folder = "front";
                category = json.category;
                frontpocket = json.frontpocket;
                backpocket = json.backpocket;
                beltloop = json.beltloop;
                pantbottom = json.pantbottom;
                pantcolor = json.pantcolor;
                $("#pantimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + category + ".png");
                $("#beltimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + belt + ".png");
                $("#backpocketimg").attr("src", "images/pants/sugg/bawli.png");
                if(category == "casual")
                {
                    $("#frontpocketimg").attr("src", "images/pants/sugg/" + pantcolor + "/"+folder+"/" + frontpocket + ".png");
                    $("#pleatimg").attr("src", "images/pants/sugg/bawli.png");
                }
                else
                {
                    $("#pleatimg").attr("src", "images/pants/sugg/" + pantcolor + "/"+folder+"/" + pleat + ".png");
                    $("#frontpocketimg").attr("src", "images/pants/sugg/bawli.png");
                }
                $("#beltloopimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + beltloop + ".png");
                $("#pantbottomimg").attr("src", "images/pants/sugg/" + pantcolor + "/" + folder + "/" + pantbottom + ".png");
                $(".loader").fadeOut("slow");
            });
        }
        loadSessionData();
    </script>
    <?php
        if(isset($_REQUEST['fromtwo']))
        {
            ?>
            <script>
                 showcolordiv();
            </script>
            <?php
        }
    ?>
</div>	