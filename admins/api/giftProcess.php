<?php
require_once ('Classes/GIFTCLASS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$giftClass = new \Classes\GIFTCLASS();
$requiredfields = array('type');
($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $giftClass->apiResponse($response);
    return false;
}
$type = $_POST['type'];

if($type == "addGiftbox"){
    $requiredfields = array('title','description','code','price');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $giftClass->apiResponse($response);
        return false;
    }
    $gift_for = $_POST['title'];
    $gift_code = $_POST['code'];
    $description = $_POST['description'];
    $price = $_POST['price'];
    ($response = $giftClass->addGiftbox($gift_for,$gift_code,$price,$description));
    $giftClass->apiResponse($response);
}
else if($type == "getGiftboxData"){
    ($response = $giftClass->getGiftboxData());
    $giftClass->apiResponse($response);
}
else if($type == "editGiftbox")
{
    $requiredfields = array('g_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $giftClass->apiResponse($response);
        return false;
    }
    $g_id = $_REQUEST['g_id'];
    $response = $giftClass->editGiftbox($g_id);
    if($response[STATUS] == Error) {
        $giftClass->apiResponse($response);
        return false;
    }
    $giftClass->apiResponse($response);
}
else if($type == "updateGiftbox")
{
    $requiredfields = array('title','description','code','price','g_id','ImageChanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $giftClass->apiResponse($response);
        return false;
    }
    $title = trim($_POST['title']);
    $description = trim($_POST['description']);
    $code = trim($_POST['code']);
    $g_id = trim($_POST['g_id']);
    $ImageChanged = trim($_POST['ImageChanged']);
    $price = trim($_POST['price']);
    $response = $giftClass->updateGiftbox($title,$description,$code,$price,$g_id,$ImageChanged);
    $giftClass->apiResponse($response);
}
else if($type == "deleteGiftbox")
{
    $requiredfields = array('g_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $giftClass->apiResponse($response);
        return false;
    }
    $g_id = $_REQUEST['g_id'];
    $response = $giftClass->deleteGiftbox($g_id);
    if($response[STATUS] == Error) {
        $giftClass->apiResponse($response);
        return false;
    }
    $giftClass->apiResponse($response);
}

else{
    $response[STATUS] = Error;
    $response[MESSAGE] = "502 UnAuthorised Request";
    $giftClass->apiResponse($response);
}
?>