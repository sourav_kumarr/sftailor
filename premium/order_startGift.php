<?php
include "header.php";
include("front_header.php");
include "../api/Constants/dbConfig.php";
include "../api/Classes/CONNECT.php";
?>
    <style>
        .nav_menu{
            margin-bottom: 0px;
        }
        .headingstartshirt{
            font-size: 19px;
            font-weight: bold;
            margin-bottom: 20px;
            margin-left: 15px;
            text-align: left;
            text-decoration: underline;
        }
        .col-md-6 > li {
            font-size: 18px;
            list-style: square;
        }
        html.js {
            opacity: 1;
        }
        .grid li {
            width: 350px !important;
            padding: 10px !important;
            margin:2px!important;
        }
        .cs-style-4 figcaption {
            width: 70% !important;
        }
        .grid figcaption a{
            margin-top: 5px;
        }
    </style>
<?php
$conn = new \Modals\CONNECT();
$link = $conn->Connect();
?>
<div class="right_col" role="main" style="overflow-y: auto">
    <link rel="stylesheet" type="text/css" href="../css/component1.css" />
    <div class="col-md-12" style="margin-top:50px;text-align:center;">
        <p style="color: #555;font-size:30px;background: wheat;">SFTAILOR GIFT CERTIFICATE &nbsp; <img src="../admins/images/gift_marvel.gif" style="height:68px;width:84px;margin-top: -16px;">
        </p>
        <hr>
        <div class="container demo-3">
            <ul class="grid cs-style-4">
                <?php
                $query = mysqli_query($link,"select * from wp_gift_certificate where status = '1'");
                if($query){
                    while($rows = mysqli_fetch_assoc($query)) { ?>
                        <li style="background: url('../images/gift_pattern.jpg');height: 245px;background-repeat: no-repeat;">
                            <figure>
                                <div style="background: url(../admins/api/Files/images/giftbox/<?php echo $rows['gift_image'];?>);
                                        background-position: center center;background-size: 100% auto;height: 100%;width: 100%;background-repeat: no-repeat"></div>
                                <figcaption>
                                    <h3>Gift For : <?php echo $rows['gift_for'];?></h3>
                                    <div style="min-height: 85px;">Specification : <?php echo substr($rows['gift_description'],0,110);?></div>
                                    <div style="width: 200px;">
                                        <h2 style="color: white;float: left;font-size: 20px;">PRICE :$<?php echo $rows['gift_price'];?></h2>
                                        <a href="#" style="float: right;position: initial;">BUY NOW</a>
                                    </div>
                                </figcaption>
                            </figure>
                        </li>
                    <?php } } ?>
            </ul>
        </div><!-- /container -->

        <div class="col-md-1"></div>
    </div>
</div>
<?php
include "footer.php";
?>
<script src="../js/modernizr.custom1.js"></script>
<script src="../js/toucheffects1.js"></script>
