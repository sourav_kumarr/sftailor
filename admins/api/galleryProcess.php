<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:42 PM
 */

require_once('Classes/GALLERY.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$galleryClass = new \Classes\GALLERY();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $galleryClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
if($type === 'addGalleryImg') {
    $requiredfields = array('event','iseventimg');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $event = $_REQUEST['event'];
    $iseventimg = $_REQUEST['iseventimg'];
    $oldeventimg = $_REQUEST['oldeventimg'];
    $logoResponse = $galleryClass->addGalleryImg($event,$iseventimg,$oldeventimg);
    $galleryClass->apiResponse($logoResponse);
}
else if($type === 'getDistinctEvent') {
    $logoResponse = $galleryClass->getDistinctEvent();
    $galleryClass->apiResponse($logoResponse);
}
else if($type === 'getGallery') {
    $logoResponse = $galleryClass->getGallery();
    $galleryClass->apiResponse($logoResponse);
}

else if($type === 'deleteGallery') {
    $requiredfields = array('id','path');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $id = $_REQUEST['id'];
    $path = $_REQUEST['path'];
    $logoResponse = $galleryClass->deleteGallery($id,$path);
    $galleryClass->apiResponse($logoResponse);
}
else if($type === 'deleteEvent') {
    $requiredfields = array('event');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $event = $_REQUEST['event'];
    $logoResponse = $galleryClass->deleteEvent($event);
    $galleryClass->apiResponse($logoResponse);
}else if($type === 'editEvent') {
    $requiredfields = array('new_event','old_event','oldEventImagechanged','oldEventImage',);
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $new_event = $_REQUEST['new_event'];
    $old_event = $_REQUEST['old_event'];
    $oldEventImagechanged = $_REQUEST['oldEventImagechanged'];
    $oldEventImage = $_REQUEST['oldEventImage'];
    $logoResponse = $galleryClass->editEvent($new_event,$old_event,$oldEventImagechanged,$oldEventImage);
    $galleryClass->apiResponse($logoResponse);
}else if($type === 'editGallery') {
    $requiredfields = array('gallery_id','gallery_title','oldGalleryImagechanged');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $gallery_id = $_REQUEST['gallery_id'];
    $gallery_title = $_REQUEST['gallery_title'];
    $oldGalleryImagechanged = $_REQUEST['oldGalleryImagechanged'];
    $logoResponse = $galleryClass->editGallery($gallery_id,$gallery_title,$oldGalleryImagechanged);
    $galleryClass->apiResponse($logoResponse);
}
else if($type === 'updateSortOrder') {
    $requiredfields = array('sortOrder','idOrder');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $sortOrder = $_REQUEST['sortOrder'];
    $idOrder = $_REQUEST['idOrder'];
    $logoResponse = $galleryClass->updateSortOrder($sortOrder,$idOrder);
    $galleryClass->apiResponse($logoResponse);
}
else if($type === 'updateCatSortOrder') {
    $requiredfields = array('sortOrder','idOrder');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $galleryClass->apiResponse($response);
        return false;
    }
    $sortOrder = $_REQUEST['sortOrder'];
    $idOrder = $_REQUEST['idOrder'];
    $logoResponse = $galleryClass->updateCatSortOrder($sortOrder,$idOrder);
    $galleryClass->apiResponse($logoResponse);
}

?>