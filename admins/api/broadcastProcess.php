<?php
/**
 * Created by PhpStorm.
 * User: SachTech
 * Date: 11-12-2017
 * Time: 12:38
 */

require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
require_once('Classes/RESPONSE.php');
require_once('Classes/BROADCAST.php');
require_once('Classes/CONNECT.php');

$broadClass = new \Classes\BROADCAST();
$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure') {
    \Classes\RESPONSE::apiResponse($response);
    return false;
}

$type = $_REQUEST['type'];

if($type === 'createBroadCast') {
    $requiredfields = array('broad_msg_id','broad_users','broad_created_at','broad_updated');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        \Classes\RESPONSE::apiResponse($response);
        return false;
    }
    $broadClass->setBroadMsgId($_REQUEST['broad_msg_id']);
    $broadClass->setBroadUsers($_REQUEST['broad_users']);
    $broadClass->setBroadCreatedAt($_REQUEST['broad_created_at']);
    $broadClass->setBroadUpdated($_REQUEST['broad_updated']);
    $response = $broadClass->save();
    \Classes\RESPONSE::apiResponse($response);

}
else if($type === 'getAllBroadCasts') {
    $response = $broadClass->getAllBroadCast();
    \Classes\RESPONSE::apiResponse($response);

}
else if($type === 'removeBroadCast') {

    $requiredfields = array('broad_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        \Classes\RESPONSE::apiResponse($response);
        return false;
    }
    $broadClass->setBroadId($_REQUEST['broad_id']);
    $response = $broadClass->removeSchedule();
    \Classes\RESPONSE::apiResponse($response);

}