<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:44 PM
 */

namespace Classes;
require_once('STORECONNECT.php');
require_once('USERCLASS.php');

class PDF
{
    public $link = null;
    public $userClass = null;
    public $response = array();
    private $pdf_id;
    private $pdf_title;
    private $pdf_file;
    private $pdf_created_at;

    public function __construct()
    {
        $this->link = new STORECONNECT();
        $this->userClass = new USERCLASS();
    }

    public function __call($function, $args)
    {
        $functionType = strtolower(substr($function, 0, 3));
        $propName = lcfirst(substr($function, 3));
        switch ($functionType) {
            case 'get':
                if (property_exists($this, $propName)) {
                    return $this->$propName;
                }
                break;
            case 'set':
                if (property_exists($this, $propName)) {
                    $this->$propName = $args[0];
                }
                break;
        }
    }

    /**
     * @param mixed $pdf_created_at
     */
    public function setPdfCreatedAt($pdf_created_at)
    {
        $this->pdf_created_at = $pdf_created_at;
    }

    /**
     * @param mixed $pdf_id
     */
    public function setPdfId($pdf_id)
    {
        $this->pdf_id = $pdf_id;
    }

    /**
     * @param mixed $pdf_file
     */
    public function setPdfFile($pdf_file)
    {
        $this->pdf_file = $pdf_file;
    }

    /**
     * @param mixed $pdf_title
     */
    public function setPdfTitle($pdf_title)
    {
        $this->pdf_title = $pdf_title;
    }

    public function save()
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $fileResponse = $this->link->storeImage('file', 'pdf');
        if ($fileResponse[Status] === Error) {
            return $fileResponse;
        }
        $file_name = $fileResponse["File_Name"];
        $file_path = $fileResponse["File_Path"];
        $pdf_title = explode('.', $file_name)[0];
        $fileExists = $this->isFileExists($file_name);

        if ($fileExists[Status] === Error) {
            return $fileExists;
        }
        $query = "insert into wp_pdf (pdf_title, pdf_file,pdf_file_path) values('$pdf_title','$file_name','$file_path')";
        $result = mysqli_query($link, $query);
        if (!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $this->response[STATUS] = Status;
        $this->response[MESSAGE] = "File uploaded successfully";
        return $this->response;
    }

    public function isFileExists($file_name)
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from wp_pdf where pdf_file='$file_name'";
        $result = mysqli_query($link, $query);
        $count = mysqli_num_rows($result);

        if (!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if ($count === 0) {
            $this->response[STATUS] = Success;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        $this->response[STATUS] = Error;
        $this->response[MESSAGE] = "File already exists with this name " . $file_name;
        $this->response["data"] = $data;
        return $this->response;
    }

    public function getAllPDF()
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "select * from wp_pdf";
        $result = mysqli_query($link, $query);
        $count = mysqli_num_rows($result);

        if (!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if ($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "No data found";
            return $this->response;
        }

        while ($rows = mysqli_fetch_assoc($result)) {
            $data[] = $rows;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data found";
        $this->response["data"] = $data;
        return $this->response;

    }

    public function deletePDF()
    {
        $link = $this->link->connect();
        if (!$link) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        $data = array();
        $query = "delete from wp_pdf where pdf_id='$this->pdf_id'";
        $result = mysqli_query($link, $query);
        $count = mysqli_affected_rows($link);

        if (!$result) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
            return $this->response;
        }
        if ($count === 0) {
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Unable to delete data";
            return $this->response;
        }

        $this->response[STATUS] = Success;
        $this->response[MESSAGE] = "Data deleted successfully";
        $this->response["data"] = $data;

        return $this->response;

    }
    public function apiResponse($response, $code = null)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}