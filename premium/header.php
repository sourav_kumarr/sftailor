<?php
session_start();
include('api/Classes/STORECONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/STYLIST.php');
if(!(isset($_SESSION['SFT_Store_Id']))){
	header('Location:login.php');
}
$store_name = "";
if(isset($_SESSION['SFT_Store_Name'])){
    $store_name = $_SESSION['SFT_Store_Name'];
}
echo "<input type='hidden' value='".$store_name."' id='store_name' />";
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SF Tailor || Stylist</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
    <link href="css/custom.css" rel="stylesheet">
    <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
    <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
    <link href="css/jquery.dataTables.min.css" rel="stylesheet">
    <link href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" rel="stylesheet"/>
</head>
<style> .sub {
        padding: 5px 0px 9px 32px;
        margin-left: -22%;
    }
    .treeview a {color:white!important;}</style>
<body class="nav-md">
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index" class="site_title"><img src="images/logo.png" style="height: 51px;"></a>
                    </div>
                    <div class="clearfix"></div>
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome</span>
                            <h2 id="storename1"><?php echo $store_name ?></h2>
                        </div>
                    </div><br />
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa fa-dashboard"></i>
                                        <span>Customers</span>
                                        <span class="pull-right-container">
                                            <i class="fa fa-angle-left pull-right"></i>
                                        </span>
                                    </a>
                                    <ul class="treeview-menu" style="display: none">
                                        <li class="sub"><a href="index.php"><i class="fa fa-caret-right"></i> Customer Management</a></li>
                                        <li class="sub"><a href="admintwingenie.php"><i class="fa fa-caret-right"></i>Twin Genie </a></li>
                                        <li class="sub"><a href="adminstylegenie.php"><i class="fa fa-caret-right"></i> Style Genie</a></li>
                                        <li class="sub"><a href="adminbody.php"><i class="fa fa-caret-right"></i> Body Style</a></li>
                                        <li class="sub"><a href="admincheckmeas.php"><i class="fa fa-caret-right"></i> Check Measurements</a></li>
                                        <li class="sub"><a href="stylegenie.php"><i class="fa fa-caret-right"></i> Customer Measurements</a></li>
                                        <li class="sub"><a href="order_generate.php"><i class="fa fa-caret-right"></i> Order Generate</a></li>
                                    </ul>
                                </li>

                                <li><a href="crm"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>CRM</a></li>
                                <li><a href="bodystyle"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i> Body Style</a></li>
                                <li><a href="stylegenie"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i> Saved Measurement</a></li>
                                <li><a href="check_measurement"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i> Check Measurement</a></li>
                                <li><a href="myord"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i> My ORD Files</a></li>
                                <li><a href="orders"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i> Orders</a></li>
                                <li><a href="pdf"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i>Download Document's</a></li>

                            </ul>
                        </div>
                    </div>









                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <img src="images/img.png" alt=""><span id="storename2"><?php echo $store_name ?></span><span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    <li>
                                        <a onclick=changeAdminPassword('<?php echo $_SESSION['SFT_Store_Id'] ?>')>
                                            <i class="fa fa-lock pull-right"></i> Change Password
                                        </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
