<?php
require_once ('Classes/EMAILMANAGERS.php');
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
$supClass = new \Classes\EMAILMANAGERS();
$requiredfields = array('type');

($response = RequiredFields($_POST, $requiredfields));
if($response['Status'] == 'Failure'){
    $supClass->apiResponse($response);
    return false;
}
error_reporting(0);
$type = $_POST['type'];
if($type == "addEmployee")
{
    $requiredfields = array('employee_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $employee_name = $_REQUEST['employee_name'];
    $employee_email = $_REQUEST['employee_email'];
    $contact = $_REQUEST['contact'];
    $status = $_REQUEST['status'];
    $response = $supClass->addEmployee($employee_name,$employee_email,$contact,$status);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
elseif($type == "addTemplate")
{
    $requiredfields = array('template_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $template_name = $_REQUEST['template_name'];
    $template_subject = $_REQUEST['template_subject'];
    $message_box_template = $_REQUEST['message_box_template'];
    $status_template = $_REQUEST['status_template'];
    $response = $supClass->addTemplate($template_name,$template_subject,$message_box_template,$status_template);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
elseif($type == "updateTemplate")
{
    $requiredfields = array('template_name');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $template_name = $_REQUEST['template_name'];
    $template_name = $_REQUEST['template_name'];
    $template_subject = $_REQUEST['template_subject'];
    $message_box_template = $_REQUEST['message_box_template'];
    $status_template = $_REQUEST['status_template'];
    $template_id = $_REQUEST['template_id'];
    $response = $supClass->updateTemplate($template_name,$template_subject,$message_box_template,$status_template,$template_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
elseif($type == "updateEmployee")
{
    $requiredfields = array('employee_name_up');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $employee_name_up = $_REQUEST['employee_name_up'];
    $employee_email_up = $_REQUEST['employee_email_up'];
    $contact_up = $_REQUEST['contact_up'];
    $employee_status_up = $_REQUEST['employee_status_up'];
    $employee_id = $_REQUEST['employee_id'];
    $response = $supClass->updateEmployee($employee_name_up,$employee_email_up,$contact_up,$employee_status_up,$employee_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
elseif($type == "selectTemplate")
{
    $requiredfields = array('template_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $template_id = $_REQUEST['template_id'];
    $response = $supClass->selectTemplate($template_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
elseif($type == "selectEmployee")
{
    $requiredfields = array('employee_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $employee_id = $_REQUEST['employee_id'];
    $status = $_REQUEST['status'];
    $response = $supClass->selectEmployee($employee_id,$status);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "deleteRecord")
{
    $requiredfields = array('e_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $e_id = $_REQUEST['e_id'];
    $response = $supClass->deleteRecord($e_id);

    if($response[STATUS] == Error){
        $supClass->apiResponse($response);
        return false;
    }
    $supClass->apiResponse($response);
}
else if($type == "getTemplate")
{
    $requiredfields = array('category');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $supClass->apiResponse($response);
        return false;
    }
    $cat_id = $_REQUEST['category'];
    $response = $supClass->getEmailData($cat_id);

    $supClass->apiResponse($response);
}
?>