<?php
include("header.php");
?>
<script src="js/location.js"></script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.0/themes/smoothness/jquery-ui.css">
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<script src="js/bootstrap-datetimepicker.js"></script>
<script src="js/moment.js"></script>
<div class="loader" style="display: none;">
    <img src="images/spin.gif" class="spinloader">
</div>
<div class="banner top-joinourteam">
	<div class="">
	</div>
</div>
<div class="about-bottom wthree-3">
	<div class="container">
	<h2 class="tittle">Join Our Team</h2>
		<div class="agileinfo_about_bottom_grids">
			<div class="col-md-7 agileinfo_about_bottom_grid pull-right" style="text-align:justify;">
					<p>Just like our garments, our stylist opportunity is custom made to your wishes. Whether you're looking for something to earn a little extra on the side or for a whole new way to fill your days, SF Tailors can be the answer you've been waiting for. As a SF Tailors stylist, you develop and maintain relationships with your clients, men who want to look and feel their best for any occasion, by helping them build their perfect wardrobe.</p><br>
					<p>You can join our team at any time, no experience necessary: we will train you and provide you with the tools you need to make your business a success. You don’t work for SF Tailors, but with us! You set your own schedule and choose how many hours a week you want to spend being a stylist. </p><br>
					<p>We offer several ways to start with minimal investment.</p><br>
					<p>Interested? Contact us at opportunity@scan2tailor.com.</p><br>
					<p>We look forward to meeting you!</p><br>
			</div>
			<div class="col-md-5 pull-left agileinfo_about_bottom_grid">
				<img src="images/tab/startkitcopy.jpg" alt=" " class="img-responsive"><br>
			</div>
		</div>
	</div>	
<div class="clear"></div>			
<style>
    .addressheading {
        color: white;
        font-family: calibri;
        font-size: 45px;
        font-weight: normal;
    }
    .tr {
        color: #666;
        font-family: Trebuchet MS;
        letter-spacing: 1px;
        line-height: 40px;
    }
    .comtactpageupperpart{
        border-bottom: 2px solid #2095f2;
        padding: 50px 0;
    }
    td, th{
        //color:white;
        font-family: Calibri;
    }
    .videotopdiv {
        height: 500px;
        position: absolute;
        top: 0;
        width: 100%;
    }
    .heritageheader {
        height: 500px;
        margin-bottom: 20px;
        margin-top: -21px;
        overflow: hidden;
        padding: 0;
    }
    a{color:white!important;}
	.cf {
    margin-top: 30px;
}
.ui-state-default {
    background: #2095f2 none repeat scroll 0 0!important;
}

</style>
	<div class="col-md-12 heritageheader" style="height: 700px; margin-bottom: 50px;">
		<video autoplay loop style="width: 100%">
			<Source src="video/contactmovie.mp4" />
		</video>
		<div class="videotopdiv">
				<div class="" style="position: absolute; margin-top: 65px;">
				<div class="col-md-2"></div>
					<div class="col-md-8" style="border-radius: 10px; background: rgb(255, 255, 255) none repeat scroll 0px 0px; padding: 50px;">
						<div class="col-md-12" style="padding: 0">
						<form method="Post" enctype="multipart/form-data" id="create_store">
							<div class="form-group col-md-4">
								<label>First Name</label>
								<input type="text" name="user_name" class="form-control" value="" placeholder="Enter Your Name" required="true"  />
							</div>
							<div class="form-group col-md-4">
								<label>Last Name</label>
								<input type="text" name="last_name" class="form-control" value="" placeholder="Enter Your Name" required="true"  />
							</div>
							
							<div class="form-group col-md-4">
								<label>Stylist E-Mail</label>
								<input type="text" name="user_email" class="form-control" value="" placeholder="Enter Your E-Mail Address" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label>Stylist Phone Number</label>
								<input type="text" name="store_phone" class="form-control" value="" placeholder="Enter Your Phone"required="true" />
								<input type="hidden" name="store_status" value="0">
							</div>
							 <div class="form-group col-md-4">
								<label>Password</label>
								<input type="password" name="store_password" class="form-control" value="" placeholder="Enter Your Password" required="true"/>
							</div>
							 <div class="form-group col-md-4">
								<label>Company Name</label>
								<input type="text" name="company_name" class="form-control" value=""  placeholder="Enter Your company Name"/>
							</div>
							 <div class="form-group col-md-4">
								<label>Profile Picture</label>
								<input type="file" name="store_logo" class="form-control" value="Upload" required="true"/>
							</div>
							<div class="form-group col-md-4">
								<label>About Company</label>
								<textarea type="text" name="store_about" class="form-control" value=""  placeholder="Company description"/></textarea>
							</div>
							<div class="form-group col-md-4">
								<label>Stylist Address</label>
								<textarea type="text" name="store_contact_address" class="form-control" value=""  placeholder="Contact description"/></textarea>
							</div>
							<div class="form-group col-md-4" style="margin-bottom: 40px; margin-top: 12px;">
								<label>Zip Code</label>
								<input type="text" name="zip_code" class="form-control" value=""  placeholder="Enter Your Zip Code" required="true"/>
							</div>
							<div style='clear:both' ></div>
							<div class="form-group col-md-4">
								<label>Select Country</label>
								<select name="b_country" class="custom_signup_feild countries form-control" id="countryId" onchange='datedate()'>
										<option value="">Select Country</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Select State</label>
							   <select name="b_state" class=" custom_signup_feild states form-control" id="stateId">
										<option value="">Select State</option>
								</select>
							</div>
							<div class="form-group col-md-4">
								<label>Select City</label>
								<div id="cityfeild">
									<select name="b_city" class=" custom_signup_feild cities form-control" id="cityId">
											<option value="">Select City</option>
									</select>
								</div>
							</div>
							<div class="form-group col-md-4" id="cityfeild">
								<label>Enter D.O.B</label>
								<input id="inputdob" name="dob" placeholder="Enter D.O.B" class=" custom_signup_feild form-control frm_ctrl" type="text">
							</div>
							<div class="form-group col-md-4" id="cityfeild">
								<label>Reference( optional )</label>
								<input id="referal" name="referal" placeholder="Enter Reference Store Code"
								class="custom_signup_feild form-control frm_ctrl" type="text" />
							</div>
							<div class="form-group">
								<button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
								width:200px;border:0;margin-top:30px;margin-right:15px"> Create Stylist</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="clear"></div>
<script>
$(document).ready(function()
{
	$("#create_store").submit(function(e)
	{
		e.preventDefault();
		$('.loader').fadeIn("slow");
		$.ajax({
			url: "admin/webserver/create_store.php?type=storeinsert",
			type: "POST",
			data: new FormData(this),
			contentType: false,
			cache: false,
			processData:false,
			success: function(data)
			{
				$('.loader').fadeOut("slow");
				var json = $.parseJSON(data)
				{
					var status = json.status;
					var message = json.message;
					if(status == "done"){
						alert(message);
						window.location="index.php";
					}
					else{
						alert(message);
					}
				}
			}
		});
	});
});

$(".menuitems").removeClass("activemenuitem");
$("#contact").addClass("activemenuitem");
function datedate(){
	var country = $("#countryId").val();
	if(country == "United States"){
		$('#inputdob').datetimepicker({
			minView : 2,
			format:"mm/dd/yyyy",
			autoclose: true
		});
	}else{
		$('#inputdob').datetimepicker({
			minView : 2,
			format:"dd/mm/yyyy",
			autoclose: true
		});
	}
}
</script>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
<?php include("footer.php");?>