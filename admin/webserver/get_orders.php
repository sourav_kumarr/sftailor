<?php
session_start();
include('database/db.php');
include('errorCodes.php');
include('functions.php');
$curr_tz = date_default_timezone_get();
date_default_timezone_set($curr_tz);
$status="error";
$order_id="";
$message="unable to get data....";
$id = $_REQUEST['id'];
$requiredfields = array('id');
// validate required fields
if(!RequiredFields($_REQUEST, $requiredfields)){
   return false;
}
if($id==1)  //to get order of particular user only userid required
{
	$data = array();
	$requiredfields = array(
		'user_id'
	);
	// validate required fields
	if(!RequiredFields($_REQUEST, $requiredfields))
	{
		return false;
	}
	$user_id = $_REQUEST['user_id'];
	$query = "select * from wp_orders where order_user_id = '$user_id' order by order_id DESC";
	$result = mysql_query($query);
	if ($result) 
	{
		$count = mysql_num_rows($result);
		if ($count > 0) 
		{
			$status = "done";
			$message = "data found successfully";
			while ($rows = mysql_fetch_array($result)) 
			{
				unset($detail);
				$order_id = $rows['order_id'];
				$detailQuery = "select * from wp_orders_detail where det_order_id = '$order_id'";
				$detailresult = mysql_query($detailQuery);
				if($detailresult)
				{
					while($detailrow = mysql_fetch_array($detailresult))
					{
						$product_type = $detailrow['product_type'];
						if($product_type == "Custom Suit")
						{
							$det_product_name = $detailrow['det_product_name'];
							$det_price = $detailrow['det_price'];
							$det_quantity = $detailrow['det_quantity'];
							$product_button = $detailrow['product_button'];
							$product_vent = $detailrow['product_vent'];
							$product_collor = $detailrow['product_collor'];
							$product_color = $detailrow['product_color'];
							$product_show_type = $detailrow['product_show_type'];
							$product_image = $detailrow['product_image'];
							$color_image = $detailrow['color_image'];
							$detail[]=array("det_product_name"=>$det_product_name,"det_price"=>$det_price,
							"det_quantity"=>$det_quantity,"product_button"=>$product_button,"product_vent"=>$product_vent,
							"product_collor"=>$product_collor,"product_color"=>$product_color,"product_type"=>$product_type,
							"product_show_type"=>$product_show_type,"product_image"=>$product_image,"color_image"=>$color_image);
						}
						elseif($product_type == "Custom Pant")
						{
							$det_product_name = $detailrow['det_product_name'];
							$det_price = $detailrow['det_price'];
							$det_quantity = $detailrow['det_quantity'];
							$pant_category = $detailrow['pant_category'];
							$front_pocket = $detailrow['pant_front_pocket'];
							$back_pocket = $detailrow['pant_back_pocket'];
							$belt_loop = $detailrow['pant_belt_loop'];
							$pant_bottom = $detailrow['pant_bottom'];
							$pant_color = $detailrow['pant_color'];
							$product_show_type = $detailrow['product_show_type'];
							$product_image = $detailrow['product_image'];
							$detail[]=array("det_product_name"=>$det_product_name,"det_price"=>$det_price,
							"det_quantity"=>$det_quantity,"pant_category"=>$pant_category,"front_pocket"=>$front_pocket,
							"back_pocket"=>$back_pocket,"belt_loop"=>$belt_loop,"pant_bottom"=>$belt_loop,
							"pant_color"=>$pant_color,"product_type"=>$product_type,
							"product_show_type"=>$product_show_type,"product_image"=>$product_image);
						}
						elseif($product_type == "Custom Shirt")
						{
							$det_product_name = $detailrow['det_product_name'];
							$det_price = $detailrow['det_price'];
							$det_quantity = $detailrow['det_quantity'];
							$collar_belt = $rows['collar_belt'];
							$collar_button = $rows['collar_button'];
							$button_cut_thread = $rows['button_cut_thread'];
							$button_holding_thread = $rows['button_holding_thread'];
							$contrast_fabric = $rows['contrast_fabric'];
							$contrast_fabric_selection = $rows['contrast_fabric_selection'];
							$shirt_category = $detailrow['shirt_category'];
							$shirt_collar = $detailrow['shirt_collar'];
							$shirt_cuff = $detailrow['shirt_cuff'];
							$shirt_placket = $detailrow['shirt_placket'];
							$shirt_pocket = $detailrow['shirt_pocket'];
							$shirt_pleat = $detailrow['shirt_pleat'];
							$shirt_color = $detailrow['shirt_color'];
							$button_color = $detailrow['button_color'];
							$product_show_type = $detailrow['product_show_type'];
							$product_image = $detailrow['product_image'];
							$detail[]=array("det_product_name"=>$det_product_name,"det_price"=>$det_price,
							"det_quantity"=>$det_quantity,"shirt_category"=>$shirt_category,"shirt_collar"=>$shirt_collar,
							"shirt_cuff"=>$shirt_cuff,"button_color"=>$button_color,"shirt_placket"=>$shirt_placket,"shirt_pocket"=>$shirt_pocket,
							"shirt_pleat"=>$shirt_pleat,"shirt_color"=>$shirt_color,"product_type"=>$product_type,
							"product_show_type"=>$product_show_type,"product_image"=>$product_image,"collar_button"=>$collar_button,"collar_belt"=>$collar_belt,
							"contrast_fabric_selection"=>$contrast_fabric_selection,"contrast_fabric"=>$contrast_fabric,
							"button_cut_thread"=>$button_cut_thread,"button_holding_thread"=>$button_holding_thread);
						}
						elseif($product_type == "Accessories")
						{
							$det_product_name = $detailrow['det_product_name'];
							$det_price = $detailrow['det_price'];
							$det_quantity = $detailrow['det_quantity'];
							$product_image = $detailrow['product_image'];
							$detail[]=array("det_product_name"=>$det_product_name,"det_price"=>$det_price,
							"det_quantity"=>$det_quantity,"product_image"=>$product_image,"product_type"=>$product_type);
						}
					}
				}
				$data[] = array("order_id" => $rows['order_id'],
				"order_date" => $rows['order_date'],
				"order_state" => $rows['order_state'],
				"order_number" => $rows['order_number'],
				"order_total" => $rows['order_total'],
				"order_payment_status" => $rows['order_payment_status'],
				"order_user_id" => $rows['order_user_id'],
				"order_detail" => $detail);
			}
		}
		else 
		{
			$status = "error";
			$message = "Order not found ";
		}
	}
	else
	{
		$status = "error";
		$message = "unable to find data " . mysql_error();
	}
}
else if($id==3) {
	$order_id = "";
	$requiredfields = array('json');
	if (!RequiredFields($_REQUEST, $requiredfields)) {
		return false;
	}
	try {
		$json = json_decode($_REQUEST['json']);
		$userid = $json->user_id;
		$total = $json->total;
		$couponCode = $json->couponCode;
        /*$meas_id = $json->meas_id;*/
		$store_name = "";
		if(isset($json->store_name)){
		    $store_name = $json->store_name;
        }
        $order_number = mt_rand(000000, 999999);
		$order_date = date("M d, Y H:i");
		$order_query = "insert into wp_orders(order_state,order_payment_status,order_number,order_total,
		order_user_id,order_date,paid_amount,coupon,store_name)values('open','Pending','$order_number','$total','$userid',
		'$order_date','0.00','$couponCode','$store_name')";
		$orderResult = mysql_query($order_query);
		if ($orderResult) {
			$order_id = mysql_insert_id();
			$selectQ = "select * from wp_cart where user_id = '$userid'";
			$resultQ = mysql_query($selectQ);
			if($resultQ){
				$num = mysql_num_rows($resultQ);
				if($num>0){
					while($ros = mysql_fetch_array($resultQ)){
                        $cart_id                    =   $ros['cart_id'];
                        $product_type               =   $ros['product_type'];
                        $product_name               =   $ros['product_name'];
                        $user_id                    =   $ros['user_id'];
                        $quantity                   =   $ros['quantity'];
                        $product_price              =   $ros['product_price'];
                        $cart_product_total_amount  =   $ros['cart_product_total_amount'];
                        $product_image              =   $ros['product_image'];
                        $product_meas               =   $ros['product_meas'];
                        $meas_name                  =   $ros['meas_name'];
                        if($product_meas == "" && $meas_name == ""){
                            $querym = mysql_query("select * from final_measurements where user_id='$user_id'");
                            $roww = mysql_fetch_array($querym);
                            $product_meas               =   $roww['measurement_id'];
                            $meas_name                  =   $roww['meas_name'];
                        }
                        $j_category                 =   $ros['j_category'];
                        $j_frontbutton              =   $ros['j_frontbutton'];
                        $j_lapelstyle               =   $ros['j_lapelstyle'];
                        $j_lapelbuttonholes         =   $ros['j_lapelbuttonholes'];
                        $j_lapelbuttonholesstyle    =   $ros['j_lapelbuttonholesstyle'];
                        $j_lapelbuttonholesthread   =   $ros['j_lapelbuttonholesthread'];
                        $j_lapelingredient          =   $ros['j_lapelingredient'];
                        $j_lapelsatin               =   $ros['j_lapelsatin'];
                        $j_chestdart                =   $ros['j_chestdart'];
                        $j_feltcollar               =   $ros['j_feltcollar'];
                        $j_innerflap                =   $ros['j_innerflap'];
                        $j_facingstyle              =   $ros['j_facingstyle'];
                        $j_sleeveslit               =   $ros['j_sleeveslit'];
                        $j_sleeveslitthread         =   $ros['j_sleeveslitthread'];
                        $j_breastpocket             =   $ros['j_breastpocket'];
                        $j_lowerpocket              =   $ros['j_lowerpocket'];
                        $j_coinpocket               =   $ros['j_coinpocket'];
                        $j_backvent                 =   $ros['j_backvent'];
                        $j_liningstyle              =   $ros['j_liningstyle'];
                        $j_liningoption             =   $ros['j_liningoption'];
                        $j_lining                   =   $ros['j_lining'];
                        $j_shoulderstyle            =   $ros['j_shoulderstyle'];
                        $j_shoulderpadding          =   $ros['j_shoulderpadding'];
                        $j_buttonoption             =   $ros['j_buttonoption'];
                        $j_buttonswatch             =   $ros['j_buttonswatch'];
                        $j_threadoption             =   $ros['j_threadoption'];
                        $j_elbowpatch               =   $ros['j_elbowpatch'];
                        $j_elbowpatchcolor          =   $ros['j_elbowpatchcolor'];
                        $j_canvasoption             =   $ros['j_canvasoption'];
                        $j_suitcolor                =   $ros['j_suitcolor'];
                        $j_shirt                    =   $ros['j_shirt'];
                        $j_tie                      =   $ros['j_tie'];
                        $j_jacket                   =   $ros['j_jacket'];
                        $j_armbutton                =   $ros['j_armbutton'];
                        $j_sidearmbutton            =   $ros['j_sidearmbutton'];
                        $j_sidearmbuttoncut         =   $ros['j_sidearmbuttoncut'];
                        $j_sidearmbuttonpoint       =   $ros['j_sidearmbuttonpoint'];
                        $p_category                 =   $ros['p_category'];
                        $p_frontpocket              =   $ros['p_frontpocket'];
                        $p_pleatstyle               =   $ros['p_pleatstyle'];
                        $p_watchpocket              =   $ros['p_watchpocket'];
                        $p_backpocket               =   $ros['p_backpocket'];
                        $p_backpocketsatin          =   $ros['p_backpocketsatin'];
                        $p_belt                     =   $ros['p_belt'];
                        $p_pleat                    =   $ros['p_pleat'];
                        $p_beltloop                 =   $ros['p_beltloop'];
                        $p_pantbottom               =   $ros['p_pantbottom'];
                        $p_pantcolor                =   $ros['p_pantcolor'];
                        $o_category                 =   $ros['o_category'];
                        $o_canvasstyle              =   $ros['o_canvasstyle'];
                        $o_lapelstyle               =   $ros['o_lapelstyle'];
                        $o_pocketstype              =   $ros['o_pocketstype'];
                        $o_buttonstyle              =   $ros['o_buttonstyle'];
                        $o_sweatpadstyle            =   $ros['o_sweatpadstyle'];
                        $o_elbowpadstyle            =   $ros['o_elbowpadstyle'];
                        $o_shoulderstyle            =   $ros['o_shoulderstyle'];
                        $o_shoulderpatchstyle       =   $ros['o_shoulderpatchstyle'];
                        $o_shouldertabstyle         =   $ros['o_shouldertabstyle'];
                        $o_sleeveslitstyle          =   $ros['o_sleeveslitstyle'];
                        $o_sleevebuttonstyle        =   $ros['o_sleevebuttonstyle'];
                        $o_backvent                 =   $ros['o_backvent'];
                        $o_stitchstyle              =   $ros['o_stitchstyle'];
                        $o_backsideofarmhole        =   $ros['o_backsideofarmhole'];
                        $o_topstichstyle            =   $ros['o_topstichstyle'];
                        $o_breastpocket             =   $ros['o_breastpocket'];
                        $o_coinpocket               =   $ros['o_coinpocket'];
                        $o_facingstyle              =   $ros['o_facingstyle'];
                        $o_pocketstyle              =   $ros['o_pocketstyle'];
                        $o_collarstyle              =   $ros['o_collarstyle'];
                        $o_liningstyle              =   $ros['o_liningstyle'];
                        $o_liningoption             =   $ros['o_liningoption'];
                        $o_threadcolor              =   $ros['o_threadcolor'];
                        $o_overcoatfabric           =   $ros['o_overcoatfabric'];
                        $s_category                 =   $ros['s_category'];
                        $s_collar                   =   $ros['s_collar'];
                        $s_collarbutton             =   $ros['s_collarbutton'];
                        $s_collarbuttondown         =   $ros['s_collarbuttondown'];
                        $s_collarlayeroption        =   $ros['s_collarlayeroption'];
                        $s_frontcollar              =   $ros['s_frontcollar'];
                        $s_collarbelt               =   $ros['s_collarbelt'];
                        $s_cuff                     =   $ros['s_cuff'];
                        $s_cuffwidth                =   $ros['s_cuffwidth'];
                        $s_placket                  =   $ros['s_placket'];
                        $s_placketbutton            =   $ros['s_placketbutton'];
                        $s_pocket                   =   $ros['s_pocket'];
                        $s_pleat                    =   $ros['s_pleat'];
                        $s_bottom                   =   $ros['s_bottom'];
                        $s_shirtcolor               =   $ros['s_shirtcolor'];
                        $s_buttoncolor              =   $ros['s_buttoncolor'];
                        $s_buttoningstyle           =   $ros['s_buttoningstyle'];
                        $s_buttoncutthread          =   $ros['s_buttoncutthread'];
                        $s_holestichthread          =   $ros['s_holestichthread'];
                        $s_monogramtext             =   $ros['s_monogramtext'];
                        $s_monogramfont             =   $ros['s_monogramfont'];
                        $s_monogramcolor            =   $ros['s_monogramcolor'];
                        $s_monogramposition         =   $ros['s_monogramposition'];
                        $s_monogramoption           =   $ros['s_monogramoption'];
                        $s_monogramcode             =   $ros['s_monogramcode'];
                        $s_contrastfabric           =   $ros['s_contrastfabric'];
                        $s_contrast                 =   $ros['s_contrast'];
                        $s_contrastposition         =   $ros['s_contrastposition'];
                        $s_sleeve                   =   $ros['s_sleeve'];
                        $s_shoulder                 =   $ros['s_shoulder'];
                        $a_category                 =   $ros['a_category'];
                        $a_specification            =   $ros['a_specification'];
                        $a_rc_no                    =   $ros['a_rc_no'];
                        $v_category                 =   $ros['v_category'];
                        $v_collarstyle              =   $ros['v_collarstyle'];
                        $v_lapelbuttonhole          =   $ros['v_lapelbuttonhole'];
                        $v_buttonholestyle          =   $ros['v_buttonholestyle'];
                        $v_buttonstyle              =   $ros['v_buttonstyle'];
                        $v_bottomstyle              =   $ros['v_bottomstyle'];
                        $v_lowerpocket              =   $ros['v_lowerpocket'];
                        $v_ticketpocket             =   $ros['v_ticketpocket'];
                        $v_backstyle                =   $ros['v_backstyle'];
                        $v_chestdart                =   $ros['v_chestdart'];
                        $v_topstitch                =   $ros['v_topstitch'];
                        $v_placketstyle             =   $ros['v_placketstyle'];
                        $v_tuxedostyle              =   $ros['v_tuxedostyle'];
                        $v_breastpocket             =   $ros['v_breastpocket'];
                        $v_chestpocket              =   $ros['v_chestpocket'];
                        $v_vestfabric               =   $ros['v_vestfabric'];
                        $subtype                    =   $ros['access_category'];

						$insQ = "INSERT INTO wp_orders_detail(cart_id,det_product_name,det_order_id,det_price,det_quantity,det_user_id,
                        product_type,product_image,meas_id,meas_name,j_category,j_frontbutton,j_lapelstyle,j_lapelbuttonholes,j_lapelbuttonholesstyle,
                        j_lapelbuttonholesthread,j_lapelingredient,j_lapelsatin,j_chestdart,j_feltcollar,j_innerflap,
                        j_facingstyle,j_sleeveslit,j_sleeveslitthread,j_breastpocket,j_lowerpocket,j_coinpocket,j_backvent,
                        j_liningstyle,j_liningoption,j_lining,j_shoulderstyle,j_shoulderpadding,j_buttonoption,j_buttonswatch,
                        j_threadoption,j_elbowpatch,j_elbowpatchcolor,j_canvasoption,j_suitcolor,j_shirt,j_tie,j_jacket,j_armbutton,
                        j_sidearmbutton,j_sidearmbuttoncut,j_sidearmbuttonpoint,p_category,p_frontpocket,p_pleatstyle,p_watchpocket,
                        p_backpocket,p_backpocketsatin,p_belt,p_pleat,p_beltloop,p_pantbottom,p_pantcolor,o_category,o_canvasstyle,o_lapelstyle,
                        o_pocketstype,o_buttonstyle,o_sweatpadstyle,o_elbowpadstyle,o_shoulderstyle,o_shoulderpatchstyle,
                        o_shouldertabstyle,o_sleeveslitstyle,o_sleevebuttonstyle,o_backvent,o_stitchstyle,o_backsideofarmhole,
                        o_topstichstyle,o_breastpocket,o_coinpocket,o_facingstyle,o_pocketstyle,o_collarstyle,o_liningstyle,
                        o_liningoption,o_threadcolor,o_overcoatfabric,s_category,s_collar,
                        s_collarbutton,s_collarbuttondown,s_collarlayeroption,s_frontcollar,s_collarbelt,s_cuff,s_cuffwidth,
                        s_placket,s_placketbutton,s_pocket,s_pleat,s_bottom,s_shirtcolor,s_buttoncolor,s_buttoningstyle,
                        s_buttoncutthread,s_holestichthread,s_monogramtext,s_monogramfont,s_monogramcolor,s_monogramposition,
                        s_monogramoption,s_monogramcode,s_contrastfabric,s_contrast,s_contrastposition,s_sleeve,s_shoulder,a_category,
                        a_specification,a_rc_no,v_category,v_collarstyle,v_lapelbuttonhole,v_buttonholestyle,v_buttonstyle,
                        v_bottomstyle,v_lowerpocket,v_ticketpocket,v_backstyle,v_chestdart,v_topstitch,v_placketstyle,
                        v_tuxedostyle,v_breastpocket,v_chestpocket,v_vestfabric,access_category) VALUES ('$cart_id','$product_name','$order_id','$product_price','$quantity',
                        '$user_id','$product_type','$product_image','$product_meas','$meas_name','$j_category','$j_frontbutton','$j_lapelstyle',
                        '$j_lapelbuttonholes','$j_lapelbuttonholesstyle','$j_lapelbuttonholesthread','$j_lapelingredient',
                        '$j_lapelsatin','$j_chestdart','$j_feltcollar','$j_innerflap','$j_facingstyle','$j_sleeveslit',
                        '$j_sleeveslitthread','$j_breastpocket','$j_lowerpocket','$j_coinpocket','$j_backvent','$j_liningstyle',
                        '$j_liningoption','$j_lining','$j_shoulderstyle','$j_shoulderpadding','$j_buttonoption','$j_buttonswatch',
                        '$j_threadoption','$j_elbowpatch','$j_elbowpatchcolor','$j_canvasoption','$j_suitcolor','$j_shirt',
                        '$j_tie','$j_jacket','$j_armbutton','$j_sidearmbutton','$j_sidearmbuttoncut','$j_sidearmbuttonpoint',
                        '$p_category','$p_frontpocket','$p_pleatstyle','$p_watchpocket','$p_backpocket','$p_backpocketsatin','$p_belt','$p_pleat',
                        '$p_beltloop','$p_pantbottom','$p_pantcolor','$o_category','$o_canvasstyle','$o_lapelstyle','$o_pocketstype',
                        '$o_buttonstyle','$o_sweatpadstyle','$o_elbowpadstyle','$o_shoulderstyle','$o_shoulderpatchstyle',
                        '$o_shouldertabstyle','$o_sleeveslitstyle','$o_sleevebuttonstyle','$o_backvent','$o_stitchstyle',
                        '$o_backsideofarmhole','$o_topstichstyle','$o_breastpocket','$o_coinpocket','$o_facingstyle','$o_pocketstyle','$o_collarstyle',
                        '$o_liningstyle','$o_liningoption','$o_threadcolor','$o_overcoatfabric','$s_category','$s_collar','$s_collarbutton','$s_collarbuttondown','$s_collarlayeroption',
                        '$s_frontcollar','$s_collarbelt','$s_cuff','$s_cuffwidth','$s_placket','$s_placketbutton','$s_pocket',
                        '$s_pleat','$s_bottom','$s_shirtcolor','$s_buttoncolor','$s_buttoningstyle','$s_buttoncutthread',
                        '$s_holestichthread','$s_monogramtext','$s_monogramfont','$s_monogramcolor','$s_monogramposition',
                        '$s_monogramoption','$s_monogramcode','$s_contrastfabric','$s_contrast','$s_contrastposition','$s_sleeve','$s_shoulder',
                        '$a_category','$a_specification','$a_rc_no','$v_category','$v_collarstyle','$v_lapelbuttonhole',
                        '$v_buttonholestyle','$v_buttonstyle','$v_bottomstyle','$v_lowerpocket','$v_ticketpocket','$v_backstyle',
                        '$v_chestdart','$v_topstitch','$v_placketstyle','$v_tuxedostyle','$v_breastpocket','$v_chestpocket',
                        '$v_vestfabric','$subtype')";
						$resultinsQ = mysql_query($insQ);
						if($resultinsQ){
							$status = 'done';
							$message = 'Order Placed Successfully';
						}
						else{
							$status = "Error";
							$message = mysql_error();
						}
					}
					///here clear cart code
					$clearcart = mysql_query("delete from wp_cart where user_id = '$userid'");
					if($couponCode != "") {
						$couponstat = mysql_query("update used_coupons set used_status = 'Redeemed',order_id='$order_id'
						where coupon_code='$couponCode' and user_id = '$userid' and used_status = 'Pending'");
					}
				}else{
					$status = "error";
					$message = 'Cart Should Have Atleast 1 Item';
				}
			}else{
				$status = "error";
				$message = mysql_error();
			}
		}else{
			$status = "error";
			$message = mysql_error();
		}
	}
	catch (Exception $ex) {
		$status = "error4";
		$message = "json parsing error " . $ex;
	}
}
echo json_encode(array("status"=>$status,"message"=>$message,"order_id"=>$order_id));
?>