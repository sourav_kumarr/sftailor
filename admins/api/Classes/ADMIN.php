<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 3/23/2017
 * Time: 3:46 PM
 */

namespace Classes;
require_once('CONNECT.php');
class ADMIN
{
    public $link = null;
    public $response = array();
    function __construct()
    {
        $this->link = new CONNECT();
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
        session_start();
    }
    public function adminLogin($userName,$Password){
        $link = $this->link->connect();
        if ($link){
            $stmt = $link->prepare("SELECT admin_id,admin_name,admin_email FROM admin_login WHERE admin_email=? AND admin_password=? LIMIT 1");
            $stmt->bind_param('ss', $userName, $Password);
            $stmt->execute();
            $stmt->bind_result($admin_id, $admin_name, $admin_email);
            $stmt->store_result();
            if($stmt->num_rows == 1)  //To check if the row exists
            {
                if($stmt->fetch()) //fetching the contents of the row
                {
                    $this->response[STATUS]=Success;
                    $this->response[MESSAGE]="Login Success";
                    $_SESSION['SFTAdminId']=$admin_id;
                    $_SESSION['SFTAdminEmail']=$admin_email;
                    $_SESSION['SFTAdminName']=$admin_name;
                }
            }
            else
            {
                $this->response[STATUS]=Error;
                $this->response[MESSAGE]="Invalid Credentials";
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAdminEmail(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from admin_login";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $row = mysqli_fetch_array($result);
                    $adminEmail = $row['admin_email'];
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Email Found";
                    $this->response['admin_email'] = $adminEmail;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function changeAdminPassword($oldPassword,$newPassword,$adminId){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from admin_login where admin_id = '$adminId' and admin_password='$oldPassword'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE admin_login SET admin_password='$newPassword' WHERE 
                    admin_password='$oldPassword' and admin_id='$adminId'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Old Password";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function changeForgotPassword($newPassword,$admin_email){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from admin_login where admin_email = '$admin_email'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "UPDATE admin_login SET admin_password='$newPassword' WHERE admin_email='$admin_email'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Password Has Been Changed Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Old Password";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function sendMaill($admin_email)
    {
        require 'SMTP/PHPMailerAutoload.php';
        $mail = new \PHPMailer();
        $mail->isSMTP();
        $mail->Host = 'md-in-68.webhostbox.net';
        $mail->SMTPAuth = true;
        $mail->Username = 'noreply@stsmentor.com';
        $mail->Password = 'noreply@sts';
        $mail->SMTPSecure = 'TLS';
        $mail->Port = 587;
        $mail->setFrom('noreply@stsmentor.com', 'Bolti Kitaab');   // sender
        $mail->addAddress($admin_email);
        //$mail->addCC('sbsunilbhatia9@gmail.com');				//to add cc
        $mail->addAttachment('');         // Add attachments
        $mail->addAttachment('');    // Optional name
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'noreply@Bolti Kitaab';
        $mail->Body='<div style="width:100%;background:#ccc;padding-bottom:36px"><div style="width:80%;margin: 0 10%">
        <div style="background:#eee;border-bottom:1px solid #ccc;height:63px;padding:10px 0 0 10px"><img style="float:left;
        height:50px" src="" alt="Bolti Kitaab" />
        <p style="float: left;color:#666;font-size:15px;font-weight:bold;margin-left:10px">Hello ! Admin</p>
        </div><div style="background:#fff;margin-top:-15px;padding-left:69px;padding-bottom:35px">
        <p style="color:green;line-height:40px;font-weight:bold;font-size:15px">Greetings of the Day,</p>
        <p>Welcome To STS Mentor Family</p><p>Here Is The Change Password link . Click here to Change your Account Password.</p>
        <a style="text-decoration:none" href="'.MainServer.'/admin/api/admin_login.php?_='.$admin_email.'" >
        <input type="button" style="background:#d14130;border-radius:3px;color: white;font-weight:600;margin: 10px 0;
        padding: 10px 20px" value="Change Password" /></a>
        <div style="background:#eee;border:1px solid #ccc;height:63px;padding:0 0 10px 10px">
        <p style="font-weight:bold;color:#666">Thanks & Regards<br><br>Bolti Kitaab Team</p>
        </div></div></div>';
        $mail->AltBody = '';
        if(!$mail->send())
        {
            return $mail->ErrorInfo;
        }
        else {
            return "Email has been sent Successfully to your registered email address";
        }
    }

	public function getAllMenus(){
		$link = $this->link->connect();
		if ($link){
			$query = "select DISTINCT (category) from wp_menu ORDER BY sort_cat ASC";
			$result = mysqli_query($link, $query);
			if ($result) {
				$num = mysqli_num_rows($result);
				if ($num > 0) {
						 while($rows = mysqli_fetch_array($result)){
							 $cat = $rows['category'];
                             $querys = "select * from wp_menu where category ='$cat' ORDER BY sort_sub_cat ASC ";
                             $results = mysqli_query($link, $querys);
                             $menuData=array();
                             while($rows1 = mysqli_fetch_array($results)){
                                 $menuData[]=array(
                                     "menu_id"=>$rows1["menu_id"],
                                     "menu_name"=>$rows1["menu_name"],
                                     "menu_link"=>$rows1["menu_link"]
                                 );
                             }
                             $this->response[STATUS] = Success;
                             $this->response[MESSAGE] = "Menu Found";
                             $this->response['menuData'][$cat] = $menuData;

                    }

				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = $this->link->sqlError();
			}
		}
		else{
			$this->response[STATUS] = Error;
			$this->response[MESSAGE] = $this->link->sqlError();
		}
		return $this->response;
	}
    public function getAllMenusAccessData(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from wp_menu";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while($rows = mysqli_fetch_array($result)){
                        $menuData[]=array(
                            "menu_id"=>$rows["menu_id"],
                            "menu_name"=>$rows["menu_name"],
                            "menu_link"=>$rows["menu_link"]
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Menu Found";
                    $this->response['menuData'] = $menuData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
    public function getAllSocialLinks(){
        $link = $this->link->connect();
        if ($link){
            $query = "select * from wp_social_link";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    while($rows = mysqli_fetch_array($result)){
                        $socialData[]=array(
                            "s_id"=>$rows["s_id"],
                            "social_name"=>$rows["social_name"],
                            "social_link"=>$rows["social_link"],
                            "social_class"=>$rows["social_class"]
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Link Found";
                    $this->response['socialData'] = $socialData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = $this->link->sqlError();
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }
	public function getPerticularAdminData($admin_id){
		$link = $this->link->connect();
		if ($link){
			$query = "select * from admin_login where status='1' AND admin_id='$admin_id'";
			$result = mysqli_query($link, $query);
			if ($result) {
				$num = mysqli_num_rows($result);
				if ($num > 0) {
						$adminData = mysqli_fetch_assoc($result);
						$this->response[STATUS] = Success;
						$this->response[MESSAGE] = "Admin User";
						$this->response['adminData'] = $adminData;
                    }

				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = $this->link->sqlError();
			}

		return $this->response;
	}
	public function getAllAdminData(){
		$link = $this->link->connect();
		if ($link){
			$query = "select * from admin_login where status='1' AND admin_level !='Super Admin'";
			$result = mysqli_query($link, $query);
			if ($result) {
				$num = mysqli_num_rows($result);
				if ($num > 0) {
						 while($rows = mysqli_fetch_array($result)){
							$menuData[]=array(
							  "admin_id"=>$rows["admin_id"],
							  "admin_name"=>$rows["admin_name"],
							  "admin_email"=>$rows["admin_email"],
							  "admin_password"=>$rows["admin_password"],
							  "password2"=>$rows["password2"],
							  "admin_access_menu"=>$rows["admin_access_menu"],
							  "status"=>$rows["status"]
							);
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Admin Found";
                    $this->response['adminData'] = $menuData;
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = $this->link->sqlError();
			}
		}
		else{
			$this->response[STATUS] = Error;
			$this->response[MESSAGE] = $this->link->sqlError();
		}
		return $this->response;
	}
	public function addAdministratorData($adminName,$Email,$password,$addminAccess,$password2){
		$link = $this->link->connect();
		if ($link){
			 $query = "insert into admin_login (admin_name,admin_email,admin_password,admin_level,admin_access_menu,status,password2) values('$adminName','$Email','$password','simple admin','$addminAccess','1','$password2')";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Administrator Added successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Data";
			}

        return $this->response;
	}
	public function updateAdministratorData($adminName,$Email,$password,$addminAccess,$adminId,$password2){
		$link = $this->link->connect();
		if ($link){
			 $query = "update admin_login set admin_name='$adminName',admin_email='$Email',admin_password='$password',admin_access_menu='$addminAccess',password2='$password2' where admin_id='$adminId'";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Administrator Info updated successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Admin Data";
			}

        return $this->response;
	}
	public function updateSocialData($admin_name,$social_link,$s_id){
		$link = $this->link->connect();
		if ($link){
			 $query = "update wp_social_link set social_name='$admin_name',social_link='$social_link' where s_id='$s_id'";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "updated successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Admin Data";
			}

        return $this->response;
	}

	public function delAdministratorData($adminId){
		$link = $this->link->connect();
		if ($link){
			 $query = "delete from admin_login where admin_id='$adminId'";
			$result = mysqli_query($link, $query);
			if ($result) {
					$this->response[STATUS] = Success;
					$this->response[MESSAGE] = "Administrator Deleted successfully.";
				} else {
					$this->response[STATUS] = Error;
					$this->response[MESSAGE] = $this->link->sqlError();
				}
			} else {
				$this->response[STATUS] = Error;
				$this->response[MESSAGE] = "Invalid Admin Data";
			}

        return $this->response;
	}


    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}
?>