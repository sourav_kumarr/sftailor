<?php
require_once ('Constants/functions.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');
require_once('Classes/InvoiceClass.php');
require_once('Classes/CONNECT.php');
error_reporting(0);
$invoiceClass = new Classes\InvoiceClass();
$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $invoiceClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
if($type === 'createInvoice') {
    $requiredfields = array('inv_number','inv_date','inv_due_date','inv_title','contact_customer','items');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $invoiceClass->apiResponse($response);
        return false;
    }
    $invoiceClass->setInvNumber($_REQUEST['inv_number']);
    $invoiceClass->setInvDate($_REQUEST['inv_date']);
    $invoiceClass->setInvDueDate($_REQUEST['inv_due_date']);
    $invoiceClass->setInvFooter($_REQUEST['inv_footer']);
    $invoiceClass->setInvNotes($_REQUEST['inv_notes']);
    $invoiceClass->setInvTitle($_REQUEST['inv_title']);
    $invoiceClass->setInvSummary($_REQUEST['inv_summary']);
    $invoiceClass->setContactCustomer($_REQUEST['contact_customer']);
    $invoiceClass->setContactEmail($_REQUEST['contact_email']);
    $invoiceClass->setContactPhone($_REQUEST['contact_phone']);
    $invoiceClass->setContactContact($_REQUEST['contact_contact']);
    $invoiceClass->setBillingAdd1($_REQUEST['billing_add_1']);
    $invoiceClass->setBillingAdd2($_REQUEST['billing_add_2']);
    $invoiceClass->setBillingCity($_REQUEST['billing_city']);
    $invoiceClass->setBillingPostal($_REQUEST['billing_postal']);
    $invoiceClass->setBillingCountry($_REQUEST['billing_country']);
    $invoiceClass->setBillingState($_REQUEST['billing_state']);
    $invoiceClass->setShippingShiptocontact($_REQUEST['shipping_shiptocontact']);
    $invoiceClass->setShippingPhone($_REQUEST['shipping_phone']);
    $invoiceClass->setShippingAdd1($_REQUEST['shipping_add_1']);
    $invoiceClass->setShippingAdd2($_REQUEST['shipping_add_2']);
    $invoiceClass->setShippingPostal($_REQUEST['shipping_postal']);
    $invoiceClass->setShippingCountry($_REQUEST['shipping_country']);
    $invoiceClass->setShippingState($_REQUEST['shipping_state']);
    $invoiceClass->setShippingCity($_REQUEST['shipping_city']);
    $invoiceClass->setMoreAccNumber($_REQUEST['more_acc_number']);
    $invoiceClass->setMoreFax($_REQUEST['more_fax']);
    $invoiceClass->setMoreMobile($_REQUEST['more_mobile']);
    $invoiceClass->setMoreWebsite($_REQUEST['more_website']);
    $invoiceClass->setMoreTollfree($_REQUEST['more_tollfree']);
    $invoiceClass->setInvType($_REQUEST['inv_type']);
    $invoiceClass->setCustStatus($_REQUEST['cust_status']);
    $invoiceClass->setItems($_REQUEST['items']);
    $invoiceResponse = $invoiceClass->saveInvoice();
    $invoiceClass->apiResponse($invoiceResponse);
}
else if($type === 'updateInvoice') {
    $requiredfields = array('inv_id','inv_number','inv_date','inv_due_date','inv_title' ,'contact_customer','items');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $invoiceClass->apiResponse($response);
        return false;
    }
    $invoiceClass->setInvId($_REQUEST['inv_id']);
    $invoiceClass->setInvNumber($_REQUEST['inv_number']);
    $invoiceClass->setInvDate($_REQUEST['inv_date']);
    $invoiceClass->setInvDueDate($_REQUEST['inv_due_date']);
    $invoiceClass->setInvFooter($_REQUEST['inv_footer']);
    $invoiceClass->setInvNotes($_REQUEST['inv_notes']);
    $invoiceClass->setInvTitle($_REQUEST['inv_title']);
    $invoiceClass->setInvSummary($_REQUEST['inv_summary']);
    $invoiceClass->setContactCustomer($_REQUEST['contact_customer']);
    $invoiceClass->setContactEmail($_REQUEST['contact_email']);
    $invoiceClass->setContactPhone($_REQUEST['contact_phone']);
    $invoiceClass->setContactContact($_REQUEST['contact_contact']);
    $invoiceClass->setBillingAdd1($_REQUEST['billing_add_1']);
    $invoiceClass->setBillingAdd2($_REQUEST['billing_add_2']);
    $invoiceClass->setBillingCity($_REQUEST['billing_city']);
    $invoiceClass->setBillingPostal($_REQUEST['billing_postal']);
    $invoiceClass->setBillingCountry($_REQUEST['billing_country']);
    $invoiceClass->setBillingState($_REQUEST['billing_state']);
    $invoiceClass->setShippingShiptocontact($_REQUEST['shipping_shiptocontact']);
    $invoiceClass->setShippingPhone($_REQUEST['shipping_phone']);
    $invoiceClass->setShippingAdd1($_REQUEST['shipping_add_1']);
    $invoiceClass->setShippingAdd2($_REQUEST['shipping_add_2']);
    $invoiceClass->setShippingPostal($_REQUEST['shipping_postal']);
    $invoiceClass->setShippingCountry($_REQUEST['shipping_country']);
    $invoiceClass->setShippingState($_REQUEST['shipping_state']);
    $invoiceClass->setShippingCity($_REQUEST['shipping_city']);
    $invoiceClass->setMoreAccNumber($_REQUEST['more_acc_number']);
    $invoiceClass->setMoreFax($_REQUEST['more_fax']);
    $invoiceClass->setMoreMobile($_REQUEST['more_mobile']);
    $invoiceClass->setMoreWebsite($_REQUEST['more_website']);
    $invoiceClass->setMoreTollfree($_REQUEST['more_tollfree']);
    $invoiceClass->setInvType($_REQUEST['inv_type']);
    $invoiceClass->setCustStatus($_REQUEST['cust_status']);
    $invoiceClass->setItems($_REQUEST['items']);
    $invoiceResponse = $invoiceClass->updateInvoice();
    $invoiceClass->apiResponse($invoiceResponse);
}
else if($type === 'getAllInvoices') {
    $requiredfields = array('status');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $invoiceClass->apiResponse($response);
        return false;
    }
    $invoiceClass->setCustStatus($_REQUEST['status']);
    $response = $invoiceClass->getAllInvoices();
    unset($response['inv_items']);
    $invoiceClass->apiResponse($response);
}
else if($type === 'getSelectedInvoice') {
    $requiredfields = array('inv_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $invoiceClass->apiResponse($response);
        return false;
    }
    $inv_id = $_REQUEST['inv_id'];
    $response = $invoiceClass->getParticularInvoiceData($inv_id);
    unset($response['inv_items']);
    $invoiceClass->apiResponse($response);
}
else if($type === 'deleteInvoice'){
    $requiredfields = array('inv_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $invoiceClass->apiResponse($response);
        return false;
    }
    $inv_id = $_REQUEST['inv_id'];
    $response = $invoiceClass->deleteInvoice($inv_id);
    $invoiceClass->apiResponse($response);
}
else if($type === 'sendInvoiceMail') {
    $requiredfields = array('inv_id','to','cc','msg');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $invoiceClass->apiResponse($response);
        return false;
    }
    $inv_id = $_REQUEST['inv_id'];
    $to = $_REQUEST['to'];
    $cc = $_REQUEST['cc'];
    $msg = $_REQUEST['msg'];
    $copytoadmin = $_REQUEST['copytoadmin'];
    $adminMail = $_REQUEST['adminMail'];
    $response = $invoiceClass->sendInvoiceEmail($inv_id,$to,$cc,$msg,$copytoadmin,$adminMail);
    $invoiceClass->apiResponse($response);
}
else if($type === 'storeInvoiceSchedule') {
    $requiredfields = array('inv_id','repeat_type','firstInv','endon','timezone');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $invoiceClass->apiResponse($response);
        return false;
    }
    $inv_id = $_REQUEST['inv_id'];
    $repeat_type = $_REQUEST['repeat_type'];
    $date = $_REQUEST['date'];
    $day = $_REQUEST['day'];
    $month = $_REQUEST['month'];
    $firstInv = $_REQUEST['firstInv'];
    $endon = $_REQUEST['endon'];
    $timezone = $_REQUEST['timezone'];
    $response = $invoiceClass->storeInvoiceSchedule($inv_id,$repeat_type,$date,$day,$month,$firstInv,$endon,$timezone);
    $invoiceClass->apiResponse($response);
}
else if($type === 'getInvoiceSchedule') {
    $requiredfields = array('inv_id');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure') {
        $invoiceClass->apiResponse($response);
        return false;
    }
    $inv_id = $_REQUEST['inv_id'];
    $response = $invoiceClass->getInvoiceSchedule($inv_id);
    $invoiceClass->apiResponse($response);
}
else if($type == "getCategories"){
    $response = $invoiceClass->getCategories();
    $invoiceClass->apiResponse($response);
}
else{
    $response[Status] = Error;
    $response[Message] = "502 Server Error !!! Invalid Request...";
    $invoiceClass->apiResponse($response);
}
?>