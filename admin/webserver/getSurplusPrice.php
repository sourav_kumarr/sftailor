<?php
error_reporting(0);
include('database/db.php');
session_start();
$status = "init";
$abc=array();
$curr_timezone = date_default_timezone_get();
date_default_timezone_set($curr_timezone);
$dated = date("d/M/Y h:i:s A");

$product_type = $_REQUEST['type'];
$colorName = $_REQUEST['colorName'];
$priceResponse = isFabricColorExists($colorName,$product_type);
$extra_price = 0;
if($priceResponse["status"]) {
    $status = "done";
    $message = "Surplus price get successfully.";
    $extra_price = $priceResponse["price"];
}
else {
    $status = "error";
    $message = "error in get Surplus price";
}
$response['status'] = $status;
$response['message'] = $message;
$response['extra_price'] = $extra_price;
echo json_encode($response);

function isFabricColorExists($colorCode,$prod_type) {
    $response = array();
    $price_query = "select * from wp_prices where price_type='$prod_type'";
    $price_result = mysql_query($price_query);
    if($price_result) {
        $price_count = mysql_num_rows($price_result);
        if($price_count == 0) {
            $response['status'] = false;
            return $response;
        }
        $row = mysql_fetch_array($price_result);
        $price = $row['price_amount'];
        $unit = $row['price_per_meter'];

        $query ="select * from fabric where fabric_cameo='$colorCode' or fabric_rc_code='$colorCode'";
        $result = mysql_query($query);
        if($result) {
            $num = mysql_num_rows($result);
            if($num>0) {
                $fabric_price = mysql_fetch_array($result)['fabric_price'];
                if($fabric_price == '') {
                    $response['status'] = false;
                    return $response;
                }
                $fabric_price = intval(substr($fabric_price,1,strlen($fabric_price)));
                $price = intval($price);
                $unit = floatval($unit);
                if($fabric_price>$price) {
                    $extra_price = ($fabric_price-$price)*$unit;
                    $response['status'] = true;
                    $response['price'] = $extra_price;
                    return $response;
                }
            }
        }
    }
    $response['status'] = false;
    return $response;
}
?>