<?php
error_reporting(0);
include('header.php');
require_once('api/Classes/ADMIN.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\ADMIN();
$allAdminData = $userClass->getAllAdminData();
$getMenuData = $adminClass->getAllMenus();
$getSocialLinks = $adminClass->getAllSocialLinks();
?>
<style>.shadoows {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 100%;
        left: 0;
        margin: 0;
        padding: 0;
        position: fixed;
        right: 0;
        top: 0;
        width: 100%;
        display: none;
        z-index: 9999;
    }
    .hideloader {
        display: none;
    }
    .loaderr {
        background: #fff none repeat scroll 0 0;
        height: 900px;
        position: fixed;
        width: 100%;
        z-index: 999;
    }
    .loaderimg {
        background-attachment: fixed;
        height: 50px;
        margin-left: 49%;
        margin-top: 20%;
        width: 50px;
    }
    #create_stores{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
    #add_admin_data{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 999999;}
    @media print {
        body * {
            visibility: hidden;
        }
        #section-to-print, #section-to-print * {
            visibility: visible;
        }
        #section-to-print {
            position: absolute;
            left: 0;
            top: 0;
        }
    }
    .e_Administrator{display:none;}
</style>
<!-- page content -->
<div id="loadd" class="loaderr hideloader">
    <img class="loaderimg" src="images/762.gif">
    <p style="margin-left:49%;font-size:18px">Loading....</p>
</div>
<div class="right_col" role="main">
    <div class="row tile_count">
    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Admins <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>


                    <!----------edit admin info------------------->
                    <?php if($_REQUEST["type"] =="edit"){  ?>
                        <div class="shadoows" onclick="close_diolog()" style="display: block;"></div>
                        <div class="col-md-12 create_stores" id="create_stores" style="padding: 0;display: block;" >
                            <?php
                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $sId = $_REQUEST['s_id'];
                                $query = "select * from wp_social_link where s_id='$sId'";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $adminData = mysqli_fetch_assoc($result);
                                    }
                                }
                            }
                            ?>
                            <form method="Post" enctype="multipart/form-data" id="update_social">
                                <div class="form-group col-md-4">
                                    <label>Name</label>
                                    <input type="text"  name="admin_name" class="form-control" value="<?php echo $adminData['social_name'];?>" placeholder="Enter Social Name" required="true"  />
                                </div>
                                <input type="hidden" name="s_id" value="<?php echo $sId;?>">
                                <input type="hidden" name="type" value="updateSocial">
                                <div class="form-group col-md-4">
                                    <label>Link</label>
                                    <input type="text" name="social_link" class="form-control" value="<?php echo $adminData['social_link'];?>" placeholder="Enter Link" required="true"/>
                                </div>

                                <div style='clear:both' ></div>

                                <div class="form-group">
                                    <button class="btn btn-info pull-right" style="background:rgb(32,149,242)none repeat scroll 0 0;
                            width:200px;border:0;margin-top:30px;margin-right:15px"> Update Admin Info</button>
                                </div>
                            </form>
                        </div>
                    <?php } ?>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Available Users
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Link</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $j=1;
                            $socialData = $getSocialLinks['socialData'];
                            for($i=0;$i<count($socialData);$i++){
                                ?>
                                <tr>
                                    <td data-title='#'><?php echo $j ?></td>
                                    <td><?php echo $socialData[$i]['social_name']; ?></td>
                                    <td><?php echo $socialData[$i]['social_link']; ?></td>
                                    <td>
                                        <a href='social.php?type=edit&s_id=<?php echo $socialData[$i]['s_id'];?>' style="float: right; font-size: 18px; padding: 2px 4px; background: tomato none repeat scroll 0% 0%; color: white;"><i class="fa fa-pencil"></i></a>

                                    </td>
                                </tr>
                                <?php $j++; } ?>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="css/bootstrap-datetimepicker.css" />
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
    $(document).ready(function () {
        $('#orderTable').DataTable({});
    });

    $("#update_social").submit(function(e){
        e.preventDefault();
        $('.loader').fadeIn("slow");
        $.ajax({
            url: "api/admin_login.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                var Status = data.Status;
                var Message = data.Message;
                if (Status == "Success"){
                    alert(Message);
                    window.location="social.php";
                }
            }
        });
    });

    function close_diolog() {
        window.location = "social.php"
    }



</script>
