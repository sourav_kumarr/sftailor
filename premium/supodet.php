<?php
error_reporting(0);
include('api/Classes/STORECONNECT.php');
include('api/Constants/DbConfig.php');
include('api/Constants/configuration.php');
require_once('api/Classes/STYLIST.php');
$conn = new \Classes\CONNECT();
$adminClass = new \Classes\ADMIN();
if($_SESSION['SFTAdminId'] ==""){
    echo "<script>document.location.href='login.php'</script>";
}
$allAdminData = $adminClass->getPerticularAdminData($_SESSION['SFTAdminId']);
$getMenuData = $adminClass->getAllMenus();
?>
    <!DOCTYPE html>
    <style>
        nav.topmateri, .container {
            margin: 0 auto;
            width: auto !important;
        }

        .myCart table td {
            text-align: left !important;
        }

        .x_panel {
            max-height: 555px;
            overflow-y: auto;
        }

        .ddkfxx ul li {
            border: 1px solid #ccc;
            width: 100% !important;
        }

        .ddkfxx ul li .p1 {
            border-right: 1px solid #ccc;
            padding-left: 4px;
            width: 30% !important;
        }
        .ddkfxx ul li .p1 img{
            height:37px;
        }
        .ddkfxx ul li .p2 {
            border-bottom: medium none !important;
        }

        .ddkfxx ul li {
            border-left: none !important;
            margin-bottom: 0 !important;
        }

        .ddkfxx ul {
            border-left: 1px solid;
        }

        .nextg li {
            border-bottom: medium none !important;
        }

        .layui-layer-shade {
            height: 100vh;
            width: 221vh;
        }

        .ddkfxx ul li .p2 {
            width: auto !important;
        }

        .layui-layer-title {
            overflow: inherit !important;
        }
        .ipfield {
            width: 400px;
        }
        .ipfields {
            width: 90px;
        }
        .cartimg {
            height: 60px!important;
        }
        .nav.side-menu > li > a, .nav.child_menu > li > a {
            font-size:13px;
        }
        .loader {
            background: #eee none repeat scroll 0 0;
            height: 830px;
            opacity: 0.7;
            position: fixed;
            width: 100%;
            z-index: 2147483647;
            top:0;
        }
        .spinloader {
            left: 34%;
            position: absolute;
            top: 14%;
        }
    </style>
    <body class="myCart nav-md">
    <div class="loader" style="display: none">
        <img src="images/slack_load.gif" class="spinloader"/>
    </div>
    <div class="container body">
        <div class="main_container">
            <div class="col-md-3 left_col">
                <div class="left_col scroll-view">
                    <div class="navbar nav_title" style="border: 0;">
                        <a href="index" class="site_title"><img src="images/logo.png" style="height: 51px;"></a>
                    </div>
                    <div class="clearfix"></div>
                    <!-- menu profile quick info -->
                    <div class="profile clearfix">
                        <div class="profile_pic">
                            <img src="images/img.png" alt="..." class="img-circle profile_img">
                        </div>
                        <div class="profile_info">
                            <span>Welcome</span>
                            <h2>Administrator</h2>
                        </div>
                    </div>
                    <br/>
                    <!-- sidebar menu -->
                    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
                        <div class="menu_section">
                            <h3>General</h3>
                            <ul class="nav side-menu">
                                <?php
                                //for simple admin
                                $checkAdmin = $allAdminData['adminData'];
                                $admin_type = $checkAdmin['admin_level'];
                                if($admin_type !='Super Admin'){
                                    $admin_access_menu = $checkAdmin['admin_access_menu'];
                                    $simpmenu =  explode(",",$admin_access_menu);
                                    for($i=0;$i<count($simpmenu);$i++){
                                        $menuNameS = $simpmenu[$i];
                                        $mainMenu = $getMenuData['menuData'];
                                        foreach($mainMenu as $mMenu){
                                            $menuNameP = $mMenu['menu_name'];
                                            if($menuNameS == $menuNameP){
                                                $menuNameL = $mMenu['menu_link'];
                                                ?>
                                                <li><a href="<?php echo $menuNameL;?>"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i><?php echo $menuNameS;?></a></li>
                                            <?php } } }	}
                                //for super admin
                                if($admin_type =='Super Admin') {
                                    $mainMenu = $getMenuData['menuData'];
                                    for($i=0;$i<count($mainMenu);$i++){
                                        $menuName = $mainMenu[$i]['menu_name'];
                                        $menuLink = $mainMenu[$i]['menu_link'];
                                        ?>
                                        <li><a href="<?php echo $menuLink;?>"><i class="fa fa-shirtsinbulk" aria-hidden="true"></i><?php echo $menuName;?></a></li>
                                    <?php } } ?>
                            </ul>
                        </div>
                    </div>
                    <!-- /sidebar menu -->
                </div>
            </div>
            <!-- top navigation -->
            <div class="top_nav">
                <div class="nav_menu">
                    <nav>
                        <div class="nav toggle">
                            <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                        </div>
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown"
                                   aria-expanded="false">
                                    <img src="images/img.png" alt="">Admin
                                    <span class=" fa fa-angle-down"></span>
                                </a>
                                <ul class="dropdown-menu dropdown-usermenu pull-right">
                                    <li><a href="logout"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                                    <li><a onclick=changeAdminPassword('<?php echo $_SESSION['SFTAdminId'] ?>')><i
                                                    class="fa fa-lock pull-right"></i> Change Password</a></li>
                                </ul>
                            </li>
                        </ul>
                    </nav>
                </div>
            </div>
            <html lang='en'>
            <head>
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
                <title>sftailor</title>
                <link href="css/bootstrap.min.css" rel="stylesheet">
                <link href="css/font-awesome.css" rel="stylesheet">
                <link href="css/bootstrap-datetimepicker.css" rel="stylesheet">
                <link href="css/custom.css" rel="stylesheet">
                <link href="css/bootstrap-toggle.min.css" rel="stylesheet">
                <link href="css/responsive.bootstrap.min.css" rel="stylesheet">
                <link href="css/scroller.bootstrap.min.css" rel="stylesheet">
                <link href="css/jquery.dataTables.min.css" rel="stylesheet">
                <link rel="stylesheet" type="text/css" href="css/defaulten.css">
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
                      integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
                      crossorigin="anonymous">
            </head>

            <div class="right_col" role="main">
                <div class="row tile_count">
                </div>
                <div class="">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="x_panel">
                                <div class="w1 cart-w1">
                                    <div class="gwc_box" id="order_box">
                                        <div class="trade clearfix">
                                        </div>
                                        <table class="gwc_tb" width="100%" cellspacing="0" cellpadding="0" border="0">
                                            <tbody>
                                            <tr>
                                                <th width="7%">NO.</th>
                                                <th name="order" id="cartProductImg" class=" zicartProductImg"
                                                    style="cursor: pointer;" width="18%">Category
                                                </th>
                                                <th name="order" id="cartOrderId" class="order1  zicartOrderId"
                                                    style="cursor: pointer;" width="12%">OrderNo. | Order
                                                </th>
                                                <th name="order" class="date1  zicartOrderDate" id="cartOrderDate"
                                                    style="cursor: pointer;" width="10%">Order date
                                                </th>
                                                <th class="customer1" width="10%">Customer</th>
                                                <th name="order" id="cartCount" class=" zicartCount"
                                                    style="cursor: pointer;" width="10%">
                                                    Quantity
                                                </th>
                                                <!--<th name="order" id="cartCount" class="NormalCss zicartCount" style="cursor: pointer;" width="10%">
                                                    Amount
                                                </th>-->
                                                <th class="mycart-11" id="end1" width="10%">Operation</th>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="gwc_box" id="cartList" data-price="￥">
                                        <table class="gwc_tb productitem-view-table" width="100%" cellspacing="0"
                                               cellpadding="0" border="0"></table>
                                    </div>
                                    <div class="qjs_tb cart_off" style="visibility: hidden;">
                                        <table style="margin:44px 0px" width="100%" cellspacing="0" cellpadding="0"
                                               border="0">
                                            <tbody>
                                            <tr style="height:"></tr>
                                            <tr>
                                                <td width="3%">
                                                    <div class="check_box" id="selectNO" name="chkRowAll"
                                                         style="margin:20px 0px 0px 25px"><i class="icon2"
                                                                                             style="background-color:white"></i>
                                                    </div>
                                                </td>
                                                <td style="text-align:left; padding-left:1%;" width="30%"><a
                                                            href="javascript:void(0);" class="doDelete">Delete Order</a><a
                                                            href="javascript:void(0);" class="exportOrden">Export
                                                        Details</a><a href="javascript:void(0);" class="exportList">Export
                                                        List</a></td>
                                                <td class="yxzsp" style="text-align:center;" width="34%"><p
                                                            style="height:0px;">selected<span id="amountAll"
                                                                                              class="amountAll">0</span>pieces
                                                        of goods</p><br></td>
                                                <td class="tuoguan" width="9%">
                                                    <a href="javascript:void(0);" id="deposit" class="deposit">Trusting</a>
                                                </td>
                                                <td class="jiesuan" width="10%"><a href="javascript:void(0);"
                                                                                   id="payAdvance"
                                                                                   class="payAdvance">Pay</a><span
                                                            id="payAdvance1" class="payAdvance1">Pay</span></td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <form id="exportOrder" action="/order/exportdetails2" method="post" target="_blank">
                                            <input id="model" name="model" value="orderDetail" type="hidden">
                                            <input id="template" name="template" value="orderDetail.ftl" type="hidden">
                                            <input id="ids" name="ordenids" type="hidden">
                                        </form>
                                        <form id="exportOrderList" action="/order/exportlist" target="_blank" method="post">
                                            <input id="model" name="model" value="qorder" type="hidden">
                                            <input id="template" name="template" value="orderList.ftl" type="hidden">
                                            <input id="estatusID" name="statusid" value="10039" type="hidden">
                                            <input id="ememberID" name="userid" value="" type="hidden">
                                            <input id="eclothingID" name="clothingid" value="" type="hidden">
                                            <input id="estrKey" name="keyword" value="" type="hidden">
                                            <input id="eordentype" name="ordentype" value="qorder" type="hidden">
                                            <input id="eblerrorno" name="blerrorno" value="" type="hidden">
                                            <input id="eordenlabelid" name="ordenlabelid" value="" type="hidden">
                                        </form>
                                    </div>
                                </div>
                                <div class="layui-layer-shade" id="layui-layer-shade5" times="5"
                                     style="z-index:19891018; display:none;background-color:#000; opacity:0.3; filter:alpha(opacity=30);"
                                     onclick="close_diolog()"></div>

                                <div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5"
                                     type="page" times="5"
                                     showtime="0" contype="string"
                                     style="z-index: 19891019; top: 25px; height: 500px; left: 5%; width:90%;display:none;">

                                    <div style="float: right; text-align: center; width: 87px; height: 26px; margin-top: 6px; border-radius: 2px; line-height: 1.8; font-weight: 600; background: darksalmon none repeat scroll 0% 0%; color: rgb(238, 238, 238); margin-right: 19px;">
                                        <input type='button' id='btn' value='Print' onclick='printDiv();'></div>

                                    <div>
                                        <button style="float: right; margin-right: 31px; line-height: 1; margin-top: 6px; height: 27px;" id="suppEmail" onclick=sendEmail('<?php echo $_REQUEST['_'];?>') class="btn btn-info btn-sm email">Send Mail</button></div>
                                    <div class="layui-layer-title" style="cursor: move;" move="ok">Order Detail</div>

                                    <div class="layui-layer-content order-detail-modal" style="height: 556px;background:#fff"></div>
                                    <span class="layui-layer-setwin">
                                    <a class="layui-layer-ico layui-layer-close layui-layer-close1"
                                       href="javascript:;"></a>
                                </span>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

    </body>

    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script>
        function printDiv() {
            var divToPrint = document.getElementById('layui-layer5');
            var newWin = window.open('', 'Print-Window');
            newWin.document.open();
            newWin.document.write('<html><style>.ddkfxx ul li .p2{width:auto!important;}' +
                '.ddkfxx ul li .p1 {border-right: 1px solid #ccc!important;padding-left: 4px!important;width: 35%!important;}' +
                '.ddkfxx ul li {border: 1px solid #ccc!important;width: 100%!important;}.ddkfxx ul {border-left: 1px solid!important;}' +
                '.ddkfxx ul li {border-left: medium none!important;margin-bottom: 0!important;}.ddkfxx ul li .p2 {width: auto!important;}' +
                '.ddkfxx ul li .p2 {border-bottom: medium none!important;}.ipfield {width: 400px!important;}.ipfields {width: 90px!important;}</style>' +
                '<link rel="stylesheet" type="text/css" href="css/defaulten.css">' +
                '<body onload="window.print()">' + divToPrint.innerHTML + '</body></html>');
            newWin.document.close();
            setTimeout(function () {
                newWin.close();
            }, 1000);
        }

        function orderListData() {
            var urlData = window.location.href;
            if (urlData.indexOf("?") == -1) {
                window.location = 'supplier_order.php';
            }
            urlData = urlData.split("?_=");
            var oderIds = urlData[1];
            var url = "api/orderProcess.php";
            $.post(url, {"type": "getOrder", "order_id": +oderIds}, function (datas) {
                var status = datas.Status;
                var Message = datas.Message;
                var order = datas.order;
                var UserData = datas.UserData;
                var orderData = order.orderDetail;
                var datashow = "";
                if (status == "Success") {
                    for (var a = 0; a < orderData.length; a++) {
                        var det_row_id = orderData[a].det_id;
                        var det_user_id = orderData[a].det_user_id;
                        var det_order_id = orderData[a].det_order_id;
                        var product_image = orderData[a].product_image;
                        var product_type = orderData[a].product_type;
                        meas_id = orderData[a].meas_id;
                        var det_quantity = orderData[a].det_quantity;
                        var det_price = orderData[a].cart_product_total_amount;
                        var det_product_name = orderData[a].det_product_name;
                        var order_number = order.order_number;
                        var order_date = order.order_date;
                        var store_name = order.store_name;
                        var payment_status = order.order_payment_status;
                        var userName = UserData.name;
                        var order_product_image = "";
                        switch (product_type) {
                            case "Custom Suit":
                                order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                                break;
                            case "Accessories":
                                order_product_image = product_image;
                                break;
                            case "Collection":
                                order_product_image = product_image;
                                break;
                            case "Custom 2Pc Suit":
                                order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                                break;
                            case "Custom Shirt":
                                order_product_image = "http://scan2fit.com/sftailor/images/shirts/57bandcollar.png";
                                break;
                            case "Custom Pant":
                                order_product_image = "http://scan2fit.com/sftailor/images/pants/formal.jpg";
                                break;
                            case "Custom Overcoat":
                                order_product_image = "http://scan2fit.com/sftailor/images/overcoat/overcoat_default.png";
                                break;
                        }
                        datashow = datashow + '<tbody><tr style="position:relative;"><td class="yh" style="height: 110px;" ' +
                            'width="7%">1</td><td class="spxq" style="height: 110px;" width="18%"><div class="middle">' +
                            '<p class="p1" style="display: block;"><a class="showDetail"><img class="cartimg" src="' +
                            order_product_image + '"></a></p><div><p class="p2" style="text-align: left; width: 100px;">' +
                            '<a class="showDetail">'+product_type+'</a></p><p class="p3" style="text-align: left; width: 100px;">'
                            +order_number+'</p></div></div></td><td class="fail" style="height: 110px;width:12%">' +
                            '<a class="showFormulainfo">'+order_number+'</a><p></p></td><td class="date" style="height: 110px; width:10%"><div class="datez"><p>'+order_date+'</p><p></p></div></td><td title="'+userName+'"'+
                            'class="customername" style="height: 110px; max-height: 110px; width:10%"><p style="max-height:110px;">' +
                            userName+'</p></td><td class="onlyspace" style="height: 110px; width:10%"><div class="jiajia">'+
                            det_quantity+'</div></td><td class="endtd" style="height: 110px; width:10%"><input type="hidden"><p>' +
                            '<a id="clickonButton"  onclick=get_product_d("' + det_user_id + '","' + det_row_id + '","' + encodeURI(product_type) +
                            '","'+order_number+'","'+encodeURI(order_date)+'","'+det_quantity+'","'+encodeURI(det_price) + '",'+
                            '"'+encodeURI(det_product_name)+'","'+encodeURI(store_name)+'","'+payment_status+'") ' +
                            'class="buyAgain" >View Details</a></p></td> </tr></tbody>';
                    }
                    ///* <td class="states" style="height: 110px;" width="10%">$' + det_price + '</td>'*/
                    $(".productitem-view-table").html(datashow);
                }
            });
        }
        orderListData();
        function get_product_d(userId, orderRowId, product_type, order_number, order_date, det_quantity, det_price,
                               det_product_name, store_name, payment_status) {
            product_type = decodeURI(product_type);
            order_number = decodeURI(order_number);
            order_date = decodeURI(order_date);
            det_quantity = decodeURI(det_quantity);
            det_price = decodeURI(det_price);
            det_product_name = decodeURI(det_product_name);
            store_name = decodeURI(store_name);
            payment_status = decodeURI(payment_status);
            userId = decodeURI(userId);
            if (store_name == "") {
                store_name = "SFTailor (Main)";
            }
            var url = "api/orderProcess.php";
            $.post(url, {"type": "getAllOrderData","orderRowId": +orderRowId,"user_id": +userId}, function (data) {
                var status = data.Status;
                var Message = data.Message;
                var UserData = data.UserData;
                var orderDetRowData = data.orderDetRowData[0];
                var access_category = data.orderDetRowData[0].access_category;
                var datashow = "";
                if (status == "Success") {
                    var userName = UserData.name;
                    var userEmail = UserData.email;
                    var userTelephone = UserData.mobile;
                    var userWeight = UserData.weight;
                    userWeight = userWeight.split(" ")[0];
                    var userHeight = UserData.height;
                    userHeight = userHeight.split(" ")[0];
                    var meas_id = orderDetRowData.meas_id;
                    var measurementData = "";
                    var measurementsitemData = "";
                    var customizationInformation = "";
                    switch (product_type) {
                        case "Custom Suit":
                            customizationInformation = '<div class="ddkfxx nextg"><ul>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Made In China Label</p>' +
                                '<p class="p2">088E no made in china label in jacket</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Category</p>' +
                                '<p class="p2">' +
                                '<input id="j_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_category",this) class="ipfield" value="' + orderDetRowData.j_category + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/frontbutton.png" />Front Button</p>' +
                                '<p class="p2">' +
                                '<input id="j_frontbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_frontbutton",this) class="ipfield" value="' + orderDetRowData.j_frontbutton + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Style</p>' +
                                '<p class="p2">' +
                                '<input id="j_lapelstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Button Holes</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelbuttonholes" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholes",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholes + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Lapel Button Holes Style</p>' +
                                '<p class="p2">' +
                                '<input id="j_lapelbuttonholesstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesstyle + '" /> ' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonholethread.png" />Lapel Button Holes Thread</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelbuttonholesthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesthread",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesthread + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Ingredient</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelingredient" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelingredient",this) class="ipfield" value="' + orderDetRowData.j_lapelingredient + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Satin</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelsatin",this) class="ipfield" value="' + orderDetRowData.j_lapelsatin + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Chest Dart</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_chestdart",this) class="ipfield" value="' + orderDetRowData.j_chestdart + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Felt Collar</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_feltcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_feltcollar",this) class="ipfield" value="' + orderDetRowData.j_feltcollar + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Inner Flap</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_innerflap" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_innerflap",this) class="ipfield" value="' + orderDetRowData.j_innerflap + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Facing Style</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_facingstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_facingstyle",this) class="ipfield" value="' + orderDetRowData.j_facingstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_sleeveslit" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslit",this) class="ipfield" value="' + orderDetRowData.j_sleeveslit + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit Thread</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_sleeveslitthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslitthread",this) class="ipfield" value="' + orderDetRowData.j_sleeveslitthread + '" />' +
                                ' </p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/breastpocket.png" />Breast Pocket</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_breastpocket",this) class="ipfield" value="' + orderDetRowData.j_breastpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lowerpocket.png" />Lower Pocket</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lowerpocket",this) class="ipfield" value="' + orderDetRowData.j_lowerpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/coinpocket.png" />Coin Pocket</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_coinpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_coinpocket",this) class="ipfield" value="' + orderDetRowData.j_coinpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/backvent.png" />Back Vent</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_backvent" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_backvent",this) class="ipfield" value="' + orderDetRowData.j_backvent + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Style</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_liningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningstyle",this) class="ipfield" value="' + orderDetRowData.j_liningstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_liningoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningoption",this) class="ipfield" value="' + orderDetRowData.j_liningoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lining" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lining",this) class="ipfield" value="' + orderDetRowData.j_lining + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Style</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_shoulderstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderstyle",this) class="ipfield" value="' + orderDetRowData.j_shoulderstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Padding</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_shoulderpadding" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderpadding",this) class="ipfield" value="' + orderDetRowData.j_shoulderpadding + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_buttonoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonoption",this) class="ipfield" value="' + orderDetRowData.j_buttonoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Swatch</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_buttonswatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonswatch",this) class="ipfield" value="' + orderDetRowData.j_buttonswatch + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Thread Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_threadoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_threadoption",this) class="ipfield" value="' + orderDetRowData.j_threadoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_elbowpatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatch",this) class="ipfield" value="' + orderDetRowData.j_elbowpatch + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch Color</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_elbowpatchcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatchcolor",this) class="ipfield" value="' + orderDetRowData.j_elbowpatchcolor + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/canvas.png" />Canvas Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_canvasoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_canvasoption",this) class="ipfield" value="' + orderDetRowData.j_canvasoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Suit Color</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_suitcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_suitcolor",this) class="ipfield" value="' + orderDetRowData.j_suitcolor + '" />' + '' +
                                '</p></li>' +
                                '</ul> </div>';
                            $.post(url, {"type": "getUserMeasurement","meas_id": +meas_id}, function (data) {
                                var Status = data.Status;
                                if(Status == "Success") {
                                    measurementData = data.finalMeasurement;
                                    measurementsitemData += "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Back length):&nbsp;&nbsp;" + '<input id="jacket_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_jacket_length",this) class="ipfields" value="' + measurementData.jacket_back_jacket_length + '" /> '+ "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Back shoulder):&nbsp;&nbsp;" +'<input id="jacket_back_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_shoulder",this) class="ipfields" value="' + measurementData.jacket_back_shoulder + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Back waist height):" + measurementData.jacket_back_waist_height + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Bicep):&nbsp;&nbsp;" +'<input id="jacket_bicep" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_bicep",this) class="ipfields" value="' + measurementData.jacket_bicep + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Bottom):" + measurementData.jacket_bottom + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Calf):" + measurementData.jacket_calf + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Chest):&nbsp;&nbsp;" +'<input id="jacket_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_chest",this) class="ipfields" value="' + measurementData.jacket_chest + '" /> '  + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Collar):" + measurementData.jacket_collar + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Front shoulder):&nbsp;&nbsp;" + '<input id="jacket_front_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_front_shoulder",this) class="ipfields" value="' + measurementData.jacket_front_shoulder + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Front waist height):" + measurementData.jacket_front_waist_height + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Front waist length):" + measurementData.jacket_front_waist_length + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Knee):" + measurementData.jacket_knee + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Nape to waist):" + measurementData.jacket_nape_to_waist + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Pant left outsteam):" + measurementData.jacket_pant_left_outseam + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Jacket(Pant right outsteam):" + measurementData.jacket_pant_right_outseam + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Seat):&nbsp;&nbsp;" + '<input id="jacket_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_seat",this) class="ipfields" value="' + measurementData.jacket_seat + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve left):&nbsp;&nbsp;" + '<input id="jacket_sleeve_left" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_left",this) class="ipfields" value="' + measurementData.jacket_sleeve_left + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve right):&nbsp;&nbsp;" + '<input id="jacket_sleeve_right" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_right",this) class="ipfields" value="' + measurementData.jacket_sleeve_right + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Stomach):&nbsp;&nbsp;" + '<input id="jacket_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_stomach",this) class="ipfields" value="' + measurementData.jacket_stomach + '" /> ' + "        </dd>";
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Thigh):" + measurementData.jacket_thigh + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(U-rise):" + measurementData.jacket_u_rise + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Jacket(Wrist):" + measurementData.jacket_wrist + "        </dd>";
                                }else{
                                    measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                                }
                            });
                            break;
                        case "Custom 2Pc Suit":
                            customizationInformation = '<div class="ddkfxx nextg"><ul>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Made In China Label</p>' +
                                '<p class="p2">088E no made in china label in 2Pc Suit</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Category</p>' +
                                '<p class="p2">' +
                                '<input id="j_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_category",this) class="ipfield" value="' + orderDetRowData.j_category + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/frontbutton.png" />Front Button</p>' +
                                '<p class="p2">' +
                                '<input id="j_frontbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_frontbutton",this) class="ipfield" value="' + orderDetRowData.j_frontbutton + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Style</p>' +
                                '<p class="p2">' +
                                '<input id="j_lapelstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Button Holes</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelbuttonholes" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholes",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholes + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Lapel Button Holes Style</p>' +
                                '<p class="p2">' +
                                '<input id="j_lapelbuttonholesstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesstyle + '" /> ' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonholethread.png" />Lapel Button Holes Thread</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelbuttonholesthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesthread",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesthread + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Ingredient</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelingredient" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelingredient",this) class="ipfield" value="' + orderDetRowData.j_lapelingredient + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Satin</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lapelsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelsatin",this) class="ipfield" value="' + orderDetRowData.j_lapelsatin + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Chest Dart</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_chestdart",this) class="ipfield" value="' + orderDetRowData.j_chestdart + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Felt Collar</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_feltcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_feltcollar",this) class="ipfield" value="' + orderDetRowData.j_feltcollar + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Inner Flap</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_innerflap" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_innerflap",this) class="ipfield" value="' + orderDetRowData.j_innerflap + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Facing Style</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_facingstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_facingstyle",this) class="ipfield" value="' + orderDetRowData.j_facingstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_sleeveslit" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslit",this) class="ipfield" value="' + orderDetRowData.j_sleeveslit + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit Thread</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_sleeveslitthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslitthread",this) class="ipfield" value="' + orderDetRowData.j_sleeveslitthread + '" />' +
                                ' </p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/breastpocket.png" />Breast Pocket</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_breastpocket",this) class="ipfield" value="' + orderDetRowData.j_breastpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lowerpocket.png" />Lower Pocket</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lowerpocket",this) class="ipfield" value="' + orderDetRowData.j_lowerpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/coinpocket.png" />Coin Pocket</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_coinpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_coinpocket",this) class="ipfield" value="' + orderDetRowData.j_coinpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/backvent.png" />Back Vent</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_backvent" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_backvent",this) class="ipfield" value="' + orderDetRowData.j_backvent + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Style</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_liningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningstyle",this) class="ipfield" value="' + orderDetRowData.j_liningstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_liningoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningoption",this) class="ipfield" value="' + orderDetRowData.j_liningoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_lining" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lining",this) class="ipfield" value="' + orderDetRowData.j_lining + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Style</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_shoulderstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderstyle",this) class="ipfield" value="' + orderDetRowData.j_shoulderstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Padding</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_shoulderpadding" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderpadding",this) class="ipfield" value="' + orderDetRowData.j_shoulderpadding + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_buttonoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonoption",this) class="ipfield" value="' + orderDetRowData.j_buttonoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Swatch</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_buttonswatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonswatch",this) class="ipfield" value="' + orderDetRowData.j_buttonswatch + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Thread Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_threadoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_threadoption",this) class="ipfield" value="' + orderDetRowData.j_threadoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_elbowpatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatch",this) class="ipfield" value="' + orderDetRowData.j_elbowpatch + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch Color</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_elbowpatchcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatchcolor",this) class="ipfield" value="' + orderDetRowData.j_elbowpatchcolor + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/canvas.png" />Canvas Option</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_canvasoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_canvasoption",this) class="ipfield" value="' + orderDetRowData.j_canvasoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Suit Color</p>' +
                                '<p class="p2"> ' +
                                '<input id="j_suitcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_suitcolor",this) class="ipfield" value="' + orderDetRowData.j_suitcolor + '" />' + '' +
                                '</p></li><li>' +
                                '<p class="p1"><img src="images/pantIcon/pant.png" />Category</p>' +
                                '<p class="p2">' +
                                '<input id="p_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_category",this) class="ipfield" value="' + orderDetRowData.p_category + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/sidepocket.png" />Front Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="p_frontpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_frontpocket",this) class="ipfield" value="' + orderDetRowData.p_frontpocket + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/category.png" />Pleat Style </p>' +
                                '<p class="p2">' +
                                '<input id="p_pleatstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleatstyle",this) class="ipfield" value="' + orderDetRowData.p_pleatstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/watch.png" />Watch Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="p_watchpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_watchpocket",this) class="ipfield" value="' + orderDetRowData.p_watchpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket </p>' +
                                '<p class="p2">' +
                                '<input id="p_backpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocket",this) class="ipfield" value="' + orderDetRowData.p_backpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt</p>' +
                                '<p class="p2">' +
                                '<input id="p_belt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_belt",this) class="ipfield" value="' + orderDetRowData.p_belt + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/category.png" />Pleat</p>' +
                                '<p class="p2">' +
                                '<input id="p_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleat",this) class="ipfield" value="' + orderDetRowData.p_pleat + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt Loop</p>' +
                                '<p class="p2">' +
                                '<input id="p_beltloop" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_beltloop",this) class="ipfield" value="' + orderDetRowData.p_beltloop + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/cuff.png" />Pant Bottom </p>' +
                                '<p class="p2">' +
                                '<input id="p_pantbottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantbottom",this) class="ipfield" value="' + orderDetRowData.p_pantbottom + '" />' +
                                '</p> </li>' +
                                '<li style="border-bottom: 1px solid !important;">' +
                                '<p class="p1"><img src="images/pantIcon/pant.png" />Pant Color</p>' +
                                '<p class="p2">' +
                                '<input id="p_pantcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantcolor",this) class="ipfield" value="' + orderDetRowData.p_pantcolor + '" />' +
                                '</p>' +
                                '</li></ul></div>';
                            $.post(url, {"type": "getUserMeasurement","meas_id": +meas_id}, function (data) {
                                var Status = data.Status;
                                if (Status == "Success") {
                                    measurementData = data.finalMeasurement;
                                    measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Back length):&nbsp;&nbsp;" + '<input id="jacket_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_jacket_length",this) class="ipfields" value="' + measurementData.jacket_back_jacket_length + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Back shoulder):&nbsp;&nbsp;" + '<input id="jacket_back_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_back_shoulder",this) class="ipfields" value="' + measurementData.jacket_back_shoulder + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Bicep):&nbsp;&nbsp;" + '<input id="jacket_bicep" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_bicep",this) class="ipfields" value="' + measurementData.jacket_bicep + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Chest):&nbsp;&nbsp;" + '<input id="jacket_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_chest",this) class="ipfields" value="' + measurementData.jacket_chest + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Front shoulder):&nbsp;&nbsp;" + '<input id="jacket_front_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_front_shoulder",this) class="ipfields" value="' + measurementData.jacket_front_shoulder + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Seat):&nbsp;&nbsp;" + '<input id="jacket_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_seat",this) class="ipfields" value="' + measurementData.jacket_seat + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve left):&nbsp;&nbsp;" + '<input id="jacket_sleeve_left" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_left",this) class="ipfields" value="' + measurementData.jacket_sleeve_left + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve right):&nbsp;&nbsp;" + '<input id="jacket_sleeve_right" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_sleeve_right",this) class="ipfields" value="' + measurementData.jacket_sleeve_right + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Jacket(Stomach):&nbsp;&nbsp;" + '<input id="jacket_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","jacket_stomach",this) class="ipfields" value="' + measurementData.jacket_stomach + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):&nbsp;&nbsp;" + '<input id="pant_back_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_back_waist_height",this) class="ipfields" value="' + measurementData.pant_back_waist_height + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):&nbsp;&nbsp;" + '<input id="pant_front_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_front_waist_height",this) class="ipfields" value="' + measurementData.pant_front_waist_height + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Bottom):&nbsp;&nbsp;" + '<input id="pant_bottom" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_bottom",this) class="ipfields" value="' + measurementData.pant_bottom + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Calf):&nbsp;&nbsp;" + '<input id="pant_calf" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_calf",this) class="ipfields" value="' + measurementData.pant_calf + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Knee):&nbsp;&nbsp;" + '<input id="pant_knee" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_knee",this) class="ipfields" value="' + measurementData.pant_knee + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):&nbsp;&nbsp;" + '<input id="pant_pant_left_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_left_outseam",this) class="ipfields" value="' + measurementData.pant_pant_left_outseam + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):&nbsp;&nbsp;" + '<input id="pant_pant_right_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_right_outseam",this) class="ipfields" value="' + measurementData.pant_pant_right_outseam + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Seat):&nbsp;&nbsp;" + '<input id="pant_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_seat",this) class="ipfields" value="' + measurementData.pant_seat + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Thigh):&nbsp;&nbsp;" + '<input id="pant_thigh" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_thigh",this) class="ipfields" value="' + measurementData.pant_thigh + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Waist):&nbsp;&nbsp;" + '<input id="pant_waist" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_waist",this) class="ipfields" value="' + measurementData.pant_waist + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(U-rise):&nbsp;&nbsp;" + '<input id="pant_u_rise" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_u_rise",this) class="ipfields" value="' + measurementData.pant_u_rise + '" /> ' + "        </dd>";
                                }else{
                                    measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                                }
                            });
                            break;
                        case "Collection":
                            customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                                '<li>' +
                                '<p class="p1">Made In China</p>' +
                                '<p class="p2">No</p></li><li>' +
                                '<p class="p1">Category#</p>' +
                                '<p class="p2"><input readonly type="text" class="ipfield" value="Accessory" />' +
                                '</p></li> ' +
                                '<li>' +
                                '<p class="p1">Rc-Number</p>' +
                                '<p class="p2"><input id="a_rc_no" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_rc_no",this) class="ipfield" value="' + orderDetRowData.a_rc_no + '" />' +
                                '</p></li>' +
                                '<li style="border-bottom: 1px solid !important;">' +
                                '<p class="p1">Specification</p>' +
                                '<p class="p2"><input id="a_specification" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_specification",this) class="ipfield" value="' + orderDetRowData.a_specification + '" />' +
                                '</p></li>' +
                                '</ul> </div>';
                            break;
                        case "Accessories":
                            customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                                '<li>' +
                                '<p class="p1">Made In China</p>' +
                                '<p class="p2">No</p></li><li>' +
                                '<p class="p1">Category#</p>' +
                                '<p class="p2"><input readonly type="text" class="ipfield" value="Accessory" />' +
                                '</p></li> ' +
                                '<li>' +
                                '<p class="p1">Rc-Number</p>' +
                                '<p class="p2"><input id="a_rc_no" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_rc_no",this) class="ipfield" value="' + orderDetRowData.a_rc_no + '" />' +
                                '</p></li>' +
                                '<li style="border-bottom: 1px solid !important;">' +
                                '<p class="p1">Specification</p>' +
                                '<p class="p2"><input id="a_specification" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_specification",this) class="ipfield" value="' + orderDetRowData.a_specification + '" />' +
                                '</p></li>' +
                                '</ul> </div>';
                            break;
                        case "Custom Shirt":
                            customizationInformation = '<div class="ddkfxx nextg"><ul><li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" />Made In China Label</p>' +
                                '<p class="p2">58S9 no made in china label in shirt</p></li><li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" />Category</p>' +
                                '<p class="p2"><input id="s_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_category",this) class="ipfield" value="' + orderDetRowData.s_category + '" />' +
                                '</p></li> ' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar</p>' +
                                '<p class="p2"><input id="s_collar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collar",this) class="ipfield" value="' + orderDetRowData.s_collar + '" />' + '' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar Button</p>' +
                                '<p class="p2"><input id="s_collarbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbutton",this) class="ipfield" value="' + orderDetRowData.s_collarbutton + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/collarbuttondown.png" />Collar Button Down</p>' +
                                '<p class="p2"><input id="s_collarbuttondown" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbuttondown",this) class="ipfield" value="' + orderDetRowData.s_collarbuttondown + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/collarbuttondown.png" />Collar Layer Option</p>' +
                                '<p class="p2"><input id="s_collarlayeroption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarlayeroption",this) class="ipfield" value="' + orderDetRowData.s_collarlayeroption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/collar.png" />Front Collar</p>' +
                                '<p class="p2"><input id="s_frontcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_frontcollar",this) class="ipfield" value="' + orderDetRowData.s_frontcollar + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar Belt</p>' +
                                '<p class="p2"><input id="s_collarbelt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbelt",this) class="ipfield" value="' + orderDetRowData.s_collarbelt + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/cuff.png" />Cuff</p>' +
                                '<p class="p2"><input id="s_cuff" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_cuff",this) class="ipfield" value="' + orderDetRowData.s_cuff + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/cuffwidth.png" />Cuff Width</p>' +
                                '<p class="p2"><input id="s_cuffwidth" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_cuffwidth",this) class="ipfield" value="' + orderDetRowData.s_cuffwidth + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" />Placket</p>' +
                                '<p class="p2"><input id="s_placket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_placket",this) class="ipfield" value="' + orderDetRowData.s_placket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" />Placket Button</p>' +
                                '<p class="p2"><input id="s_placketbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_placketbutton",this) class="ipfield" value="' + orderDetRowData.s_placketbutton + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/pocket.png" />Pocket</p>' +
                                '<p class="p2"><input id="s_pocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_pocket",this) class="ipfield" value="' + orderDetRowData.s_pocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/pleat.png" />Pleat</p>' +
                                '<p class="p2"><input id="s_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_pleat",this) class="ipfield" value="' + orderDetRowData.s_pleat + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/bottom.png" />Bottom</p>' +
                                '<p class="p2"><input id="s_bottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_bottom",this) class="ipfield" value="' + orderDetRowData.s_bottom + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" />Shirt Color</p>' +
                                '<p class="p2"><input id="s_shirtcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_shirtcolor",this) class="ipfield" value="' + orderDetRowData.s_shirtcolor + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" />Button Color</p>' +
                                '<p class="p2"><input id="s_buttoncolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoncolor",this) class="ipfield" value="' + orderDetRowData.s_buttoncolor + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Buttoning Style</p>' +
                                '<p class="p2"><input id="s_buttoningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoningstyle",this) class="ipfield" value="' + orderDetRowData.s_buttoningstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Button Cut Thread</p>' +
                                '<p class="p2"><input id="s_buttoncutthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoncutthread",this) class="ipfield" value="' + orderDetRowData.s_buttoncutthread + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Hole Stich Thread</p>' +
                                '<p class="p2"><input id="s_holestichthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_holestichthread",this) class="ipfield" value="' + orderDetRowData.s_holestichthread + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" />Mono Gram Code</p>' +
                                '<p class="p2"><input id="s_monogramcode" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramcode",this) class="ipfield" value="' + orderDetRowData.s_monogramcode + '" />' +
                                '</p> </li>' + '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" />Mono Gram Text</p>' +
                                '<p class="p2"><input id="s_monogramtext" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramtext",this) class="ipfield" value="' + orderDetRowData.s_monogramtext + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" alt="font" />Mono Gram Font</p>' +
                                '<p class="p2"><input id="s_monogramfont" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramfont",this) class="ipfield" value="' + orderDetRowData.s_monogramfont + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Color" />Mono Gram Color</p>' +
                                '<p class="p2"><input id="s_monogramcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramcolor",this) class="ipfield" value="' + orderDetRowData.s_monogramcolor + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Position" />Mono Gram Position</p>' +
                                '<p class="p2"><input id="s_monogramposition" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramposition",this) class="ipfield" value="' + orderDetRowData.s_monogramposition + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Option" />Mono Gram Option</p>' +
                                '<p class="p2"><input id="s_monogramoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramoption",this) class="ipfield" value="' + orderDetRowData.s_monogramoption + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast Fabric</p>' +
                                '<p class="p2"><input id="s_contrastfabric" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrastfabric",this) class="ipfield" value="' + orderDetRowData.s_contrastfabric + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast</p>' +
                                '<p class="p2"><input id="s_contrast" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrast",this) class="ipfield" value="' + orderDetRowData.s_contrast + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast Position</p>' +
                                '<p class="p2"><input id="s_contrastposition" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrastposition",this) class="ipfield" value="' + orderDetRowData.s_contrastposition + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/shirtIcon/cuffwidth.png" alt="" />Sleeve</p>' +
                                '<p class="p2"><input id="s_sleeve" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_sleeve",this) class="ipfield" value="' + orderDetRowData.s_sleeve + '" />' +
                                '</p> </li>' +
                                '<li style="border-bottom: 1px solid !important;">' +
                                '<p class="p1"><img src="images/shirtIcon/placket.png" alt="" />Shoulder</p>' +
                                '<p class="p2"><input id="s_shoulder" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_shoulder",this) class="ipfield" value="' + orderDetRowData.s_shoulder + '" />' +
                                '</p></li>' +
                                '</ul> </div>';
                            $.post(url, {"type": "getUserMeasurement","meas_id": +meas_id}, function (data) {
                                var Status = data.Status;
                                if (Status == "Success") {
                                    measurementData = data.finalMeasurement;
                                    measurementsitemData = "" +
                                        "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Back shoulder):" + '<input id="shirt_back_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_back_shoulder",this) class="ipfields" value="' + measurementData.shirt_back_shoulder + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Back Jacket Length):" + '<input id="shirt_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_back_jacket_length",this) class="ipfields" value="' + measurementData.shirt_back_jacket_length + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Back waist height):" + measurementData.shirt_back_waist_height + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Bicep):" + '<input id="shirt_bicep" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_bicep",this) class="ipfields" value="' + measurementData.shirt_bicep + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Bottom):" + measurementData.shirt_bottom + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Calf):" + measurementData.shirt_calf + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Chest):" + '<input id="shirt_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_chest",this) class="ipfields" value="' + measurementData.shirt_chest + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Collar):" + '<input id="shirt_collar" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_collar",this) class="ipfields" value="' + measurementData.shirt_collar + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Front shoulder):" + '<input id="shirt_front_shoulder" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_front_shoulder",this) class="ipfields" value="' + measurementData.shirt_front_shoulder + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Front waist height):" + measurementData.shirt_front_waist_height + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Front waist length):" + measurementData.shirt_front_waist_length + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Knee):" + measurementData.shirt_knee + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Nape to waist):" + measurementData.shirt_nape_to_waist + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Left-outsteam):" + measurementData.shirt_pant_left_outseam + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Shirt(Right-outsteam):" + measurementData.shirt_pant_right_outseam + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Seat):" + '<input id="shirt_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_seat",this) class="ipfields" value="' + measurementData.shirt_seat + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Sleeve left):" + '<input id="shirt_sleeve_left" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_sleeve_left",this) class="ipfields" value="' + measurementData.shirt_sleeve_left + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Sleeve right):" + '<input id="shirt_sleeve_right" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_sleeve_right",this) class="ipfields" value="' + measurementData.shirt_sleeve_right + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Shirt(Stomach):" + '<input id="shirt_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","shirt_stomach",this) class="ipfields" value="' + measurementData.shirt_stomach + '" /> ' + "        </dd>";
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Thigh):" + measurementData.shirt_thigh + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(U-rise):" + measurementData.shirt_u_rise + "        </dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Shirt(Wrist):" + measurementData.shirt_wrist + "        </dd>";
                                }else{
                                    measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                                }
                            });
                            break;
                        case "Custom Pant":
                            customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/pant.png" />Made In China Label</p>' +
                                '<p class="p2">3662 no made in china label in pants</p></li><li>' +
                                '<p class="p1"><img src="images/pantIcon/pant.png" />Category</p>' +
                                '<p class="p2">' +
                                '<input id="p_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_category",this) class="ipfield" value="' + orderDetRowData.p_category + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/sidepocket.png" />Front Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="p_frontpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_frontpocket",this) class="ipfield" value="' + orderDetRowData.p_frontpocket + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/category.png" />Pleat Style </p>' +
                                '<p class="p2">' +
                                '<input id="p_pleatstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleatstyle",this) class="ipfield" value="' + orderDetRowData.p_pleatstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/watch.png" />Watch Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="p_watchpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_watchpocket",this) class="ipfield" value="' + orderDetRowData.p_watchpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket </p>' +
                                '<p class="p2">' +
                                '<input id="p_backpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocket",this) class="ipfield" value="' + orderDetRowData.p_backpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt</p>' +
                                '<p class="p2">' +
                                '<input id="p_belt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_belt",this) class="ipfield" value="' + orderDetRowData.p_belt + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/category.png" />Pleat</p>' +
                                '<p class="p2">' +
                                '<input id="p_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleat",this) class="ipfield" value="' + orderDetRowData.p_pleat + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt Loop</p>' +
                                '<p class="p2">' +
                                '<input id="p_beltloop" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_beltloop",this) class="ipfield" value="' + orderDetRowData.p_beltloop + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="images/pantIcon/cuff.png" />Pant Bottom </p>' +
                                '<p class="p2">' +
                                '<input id="p_pantbottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantbottom",this) class="ipfield" value="' + orderDetRowData.p_pantbottom + '" />' +
                                '</p> </li>' +
                                '<li style="border-bottom: 1px solid !important;">' +
                                '<p class="p1"><img src="images/pantIcon/pant.png" />Pant Color</p>' +
                                '<p class="p2">' +
                                '<input id="p_pantcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantcolor",this) class="ipfield" value="' + orderDetRowData.p_pantcolor + '" />' +
                                '</p>' +
                                '</li></ul> </div>';
                            $.post(url, {"type": "getUserMeasurement","meas_id": +meas_id}, function (data) {
                                var Status = data.Status;
                                if (Status == "Success") {
                                    measurementData = data.finalMeasurement;
                                    measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Back length):" + measurementData.pant_back_jacket_length + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Back shoulder):" + measurementData.pant_back_shoulder + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):" + '<input id="pant_back_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_back_waist_height",this) class="ipfields" value="' + measurementData.pant_back_waist_height + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):" + '<input id="pant_front_waist_height" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_front_waist_height",this) class="ipfields" value="' + measurementData.pant_front_waist_height + '" /> ' + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Bicep):" + measurementData.pant_bicep + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Bottom):" + '<input id="pant_bottom" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_bottom",this) class="ipfields" value="' + measurementData.pant_bottom + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Calf):" + '<input id="pant_calf" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_calf",this) class="ipfields" value="' + measurementData.pant_calf + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Chest):" + measurementData.pant_chest + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Collar):" + measurementData.pant_collar + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Front shoulder):" + measurementData.pant_front_shoulder + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Front waist length):" + measurementData.pant_front_waist_length + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Knee):" + '<input id="pant_knee" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_knee",this) class="ipfields" value="' + measurementData.pant_knee + '" /> ' + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Nape to waist):" + measurementData.pant_nape_to_waist + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):" + '<input id="pant_pant_left_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_left_outseam",this) class="ipfields" value="' + measurementData.pant_pant_left_outseam + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):" + '<input id="pant_pant_right_outseam" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_pant_right_outseam",this) class="ipfields" value="' + measurementData.pant_pant_right_outseam + '" /> ' + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Seat):" + '<input id="pant_seat" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_seat",this) class="ipfields" value="' + measurementData.pant_seat + '" /> ' + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve left):" + measurementData.pant_sleeve_left + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve right):" + measurementData.pant_sleeve_right + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Stomach):" + measurementData.pant_stomach + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Thigh):" + '<input id="pant_thigh" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_thigh",this) class="ipfields" value="' + measurementData.pant_thigh + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(Waist):" + '<input id="pant_waist" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_waist",this) class="ipfields" value="' + measurementData.pant_waist + '" /> ' + "</dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Pant(U-rise):" + '<input id="pant_u_rise" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","pant_u_rise",this) class="ipfields" value="' + measurementData.pant_u_rise + '" /> ' + "        </dd>";
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Wrist):" + measurementData.pant_wrist + "        </dd>";
                                }else{
                                    measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                                }
                            });
                            break;
                        case "Custom Vest":
                            customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                                '<li>' +
                                '<p class="p1"><img src="" />Made In China Label</p>' +
                                '<p class="p2">4219 no made in china label in vest</p></li><li>' +
                                '<p class="p1"><img src="" />Category</p>' +
                                '<p class="p2">' +
                                '<input id="v_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_category",this) class="ipfield" value="' + orderDetRowData.v_category + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Collar Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_collarstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_collarstyle",this) class="ipfield" value="' + orderDetRowData.v_collarstyle + '" />' +
                                '</p></li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Lapel Button Hole</p>' +
                                '<p class="p2">' +
                                '<input id="v_lapelbuttonhole" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_lapelbuttonhole",this) class="ipfield" value="' + orderDetRowData.v_lapelbuttonhole + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Button Hole Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_buttonholestyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_buttonholestyle",this) class="ipfield" value="' + orderDetRowData.v_buttonholestyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Button Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_buttonstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_buttonstyle",this) class="ipfield" value="' + orderDetRowData.v_buttonstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Bottom Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_bottomstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_bottomstyle",this) class="ipfield" value="' + orderDetRowData.v_bottomstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Lower Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="v_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_lowerpocket",this) class="ipfield" value="' + orderDetRowData.v_lowerpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Ticket Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="v_ticketpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_ticketpocket",this) class="ipfield" value="' + orderDetRowData.v_ticketpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Back Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_backstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_backstyle",this) class="ipfield" value="' + orderDetRowData.v_backstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Chest Dart</p>' +
                                '<p class="p2">' +
                                '<input id="v_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_chestdart",this) class="ipfield" value="' + orderDetRowData.v_chestdart + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Top Stitch</p>' +
                                '<p class="p2">' +
                                '<input id="v_topstitch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_topstitch",this) class="ipfield" value="' + orderDetRowData.v_topstitch + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Placket Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_placketstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_placketstyle",this) class="ipfield" value="' + orderDetRowData.v_placketstyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Tuxedo Style</p>' +
                                '<p class="p2">' +
                                '<input id="v_tuxedostyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_tuxedostyle",this) class="ipfield" value="' + orderDetRowData.v_tuxedostyle + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Breast Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="v_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_breastpocket",this) class="ipfield" value="' + orderDetRowData.v_breastpocket + '" />' +
                                '</p> </li>' +
                                '<li>' +
                                '<p class="p1"><img src="" />Chest Pocket</p>' +
                                '<p class="p2">' +
                                '<input id="v_chestpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_chestpocket",this) class="ipfield" value="' + orderDetRowData.v_chestpocket + '" />' +
                                '</p> </li>' +
                                '<li style="border-bottom: 1px solid !important;">' +
                                '<p class="p1"><img src="" />Vest Fabric</p>' +
                                '<p class="p2">' +
                                '<input id="v_vestfabric" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","v_vestfabric",this) class="ipfield" value="' + orderDetRowData.v_vestfabric + '" />' +
                                '</p>' +
                                '</li></ul> </div>';
                            $.post(url, {"type": "getUserMeasurement","meas_id": +meas_id}, function (data) {
                                var Status = data.Status;
                                if (Status == "Success") {
                                    measurementData = data.finalMeasurement;
                                    measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                                        "<dd title= \"Seat(Accessory):\"> Vest(Back Jacket length):" + '<input id="vest_back_jacket_length" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","vest_back_jacket_length",this) class="ipfields" value="' + measurementData.vest_back_jacket_length + '" /> ' + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Back shoulder):" + measurementData.pant_back_shoulder + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):" + measurementData.pant_back_waist_height + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):" + measurementData.pant_front_waist_height + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Bicep):" + measurementData.pant_bicep + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Bottom):" + measurementData.pant_bottom + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Calf):" + measurementData.pant_calf + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Vest(Chest):" + '<input id="vest_chest" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","vest_chest",this) class="ipfields" value="' + measurementData.vest_chest + '" /> ' + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Collar):" + measurementData.pant_collar + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Front shoulder):" + measurementData.pant_front_shoulder + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Front waist length):" + measurementData.pant_front_waist_length + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Knee):" + measurementData.pant_knee + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Nape to waist):" + measurementData.pant_nape_to_waist + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):" + measurementData.pant_pant_left_outseam + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):" + measurementData.pant_pant_right_outseam + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Seat):" + measurementData.pant_seat + "</dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve left):" + measurementData.pant_sleeve_left + "        </dd>" +
                                        //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve right):" + measurementData.pant_sleeve_right + "        </dd>" +
                                        "<dd title= \"Seat(Accessory):\"> Vest(Stomach):" + '<input id="vest_stomach" type="text" onchange=updateData("' + measurementData.measurement_id + '","final_measurements","vest_stomach",this) class="ipfields" value="' + measurementData.vest_stomach + '" /> ' + "        </dd>";
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Thigh):" + measurementData.pant_thigh + "</dd>" +
                                    ///"<dd title= \"Seat(Accessory):\"> Pant(Waist):" + measurementData.pant_waist + "</dd>" +
                                    //"<dd title= \"Seat(Accessory):\"> Pant(U-rise):" + measurementData.pant_u_rise + "        </dd>";
                                    //"<dd title= \"Seat(Accessory):\"> Pant(Wrist):" + measurementData.pant_wrist + "        </dd>";
                                }else{
                                    measurementsitemData = "<p style='text-align:center'>No Measurement Selected</p>";
                                }
                            });
                            break;
                        case "Custom Overcoat":
                            customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                                '<li><p class="p1">  category</p> <p class="p2" title="' + orderDetRowData.o_category + '"> ' + orderDetRowData.o_category + '    </p></li> ' +
                                '<li><p class="p1">  canvasstyle</p> <p class="p2" title="' + orderDetRowData.o_canvasstyle + '"> ' + orderDetRowData.o_canvasstyle + '    </p></li>' +
                                '<li><p class="p1">  lapelstyle</p> <p class="p2" title="' + orderDetRowData.o_lapelstyle + '"> ' + orderDetRowData.o_lapelstyle + '    </p> </li>' +
                                '<li><p class="p1">  pocketstype</p> <p class="p2" title="' + orderDetRowData.o_pocketstype + '"> ' + orderDetRowData.o_pocketstype + '    </p> </li>' +
                                '<li><p class="p1">  buttonstyle</p> <p class="p2" title="' + orderDetRowData.o_buttonstyle + '"> ' + orderDetRowData.o_buttonstyle + '    </p> </li>' +
                                '<li><p class="p1">  sweatpadstyle</p> <p class="p2" title="' + orderDetRowData.o_sweatpadstyle + '"> ' + orderDetRowData.o_sweatpadstyle + '    </p> </li>' +
                                '<li><p class="p1">  elbowpadstyle</p> <p class="p2" title="' + orderDetRowData.o_elbowpadstyle + '"> ' + orderDetRowData.o_elbowpadstyle + '    </p> </li>' +
                                '<li><p class="p1">  shoulderstyle</p> <p class="p2" title="' + orderDetRowData.o_shoulderstyle + '"> ' + orderDetRowData.o_shoulderstyle + '    </p> </li>' +
                                '<li><p class="p1">  shoulderpatchstyle</p> <p class="p2" title="' + orderDetRowData.o_shoulderpatchstyle + '"> ' + orderDetRowData.o_shoulderpatchstyle + '    </p> </li>' +
                                '<li><p class="p1">  shouldertabstyle</p> <p class="p2" title="' + orderDetRowData.o_shouldertabstyle + '"> ' + orderDetRowData.o_shouldertabstyle + '    </p> </li>' +
                                '<li><p class="p1">  sleeveslitstyle</p> <p class="p2" title="' + orderDetRowData.o_sleeveslitstyle + '"> ' + orderDetRowData.o_sleeveslitstyle + '    </p> </li>' +
                                '<li><p class="p1">  sleevebuttonstyle</p> <p class="p2" title="' + orderDetRowData.o_sleevebuttonstyle + '"> ' + orderDetRowData.o_sleevebuttonstyle + '    </p> </li>' +
                                '<li><p class="p1">  backvent</p> <p class="p2" title="' + orderDetRowData.o_backvent + '"> ' + orderDetRowData.o_backvent + '    </p> </li>' +
                                '<li><p class="p1">  stitchstyle</p> <p class="p2" title="' + orderDetRowData.o_stitchstyle + '"> ' + orderDetRowData.o_stitchstyle + '    </p> </li>' +
                                '<li><p class="p1">  backsideofarmhole</p> <p class="p2" title="' + orderDetRowData.o_backsideofarmhole + '"> ' + orderDetRowData.o_backsideofarmhole + '    </p> </li>' +
                                '<li><p class="p1">  topstichstyle</p> <p class="p2" title="' + orderDetRowData.o_topstichstyle + '"> ' + orderDetRowData.o_topstichstyle + '    </p> </li>' +
                                '<li><p class="p1">  breastpocket</p> <p class="p2" title="' + orderDetRowData.o_breastpocket + '"> ' + orderDetRowData.o_breastpocket + '    </p> </li>' +
                                '<li><p class="p1">  facingstyle</p> <p class="p2" title="' + orderDetRowData.o_facingstyle + '"> ' + orderDetRowData.o_facingstyle + '    </p> </li>' +
                                '<li><p class="p1">  pocketstyle</p> <p class="p2" title="' + orderDetRowData.o_pocketstyle + '"> ' + orderDetRowData.o_pocketstyle + '    </p> </li>' +
                                '<li><p class="p1">  collarstyle</p> <p class="p2" title="' + orderDetRowData.o_collarstyle + '"> ' + orderDetRowData.o_collarstyle + '    </p> </li>' +
                                '<li style="border-bottom: 1px solid !important;"><p class="p1">  liningstyle</p> <p class="p2" title="' + orderDetRowData.o_liningstyle + '"> ' + orderDetRowData.o_liningstyle + '    </p> </li>' +
                                '</ul> </div>';
                            break;
                    }
                }
                setTimeout(function () {
                    var supplierRowData1 = data.supplierData;
                    for (var i = 0; i < supplierRowData1.length; i++) {
                        var sup_id = supplierRowData1[i].sup_id;
                        if(sup_id=="2" && (access_category =="neck_tie_two" || access_category =="bow_tie_two")) {
                            var supplier_name = supplierRowData1[i].company_name;
                            var company_address1 = supplierRowData1[i].company_address1;
                            var company_address2 = supplierRowData1[i].company_address2;
                            var company_work_phone = supplierRowData1[i].company_work_phone;
                            var	company_mobile_no  = supplierRowData1[i].company_mobile_no;
                            var contact_first_name  = supplierRowData1[i].contact_first_name;
                            var family_name  = supplierRowData1[i].family_name;
                            var contact_email  = supplierRowData1[i].contact_email;
                            var city  = 'Los Angles';
                            var country  = 'USA';
                        }
                        if(sup_id=="3"){
                            var supplier_name = supplierRowData1[i].company_name;
                            var company_address1 = supplierRowData1[i].company_address1;
                            var company_address2 = supplierRowData1[i].company_address2;
                            var company_work_phone = supplierRowData1[i].company_work_phone;
                            var	company_mobile_no  = supplierRowData1[i].company_mobile_no;
                            var contact_first_name  = supplierRowData1[i].contact_first_name;
                            var family_name  = supplierRowData1[i].family_name;
                            var contact_email  = supplierRowData1[i].contact_email;
                            var city  = 'Jimo Qingdao';
                            var country  = 'CHINA';
                        }
                    }

                    var url = "../api/registerUser.php";
                    $.post(url, {"type": "getStyleGenieData", "user_id": userId}, function (data) {
                        data = data.userData;
                        var shoulder_slope = data.shoulder_slope;
                        var shoulder_slope = shoulder_slope.split(",");
                        var shoulder_slope_left = shoulder_slope[0];
                        var shoulder_slope_right = shoulder_slope[1];
                        var shoulder_forward = data.shoulder_forward;
                        var body_forward = data.body_forward;
                        var shoulder_slope_left = shoulder_slope_left;
                        var shoulder_slope_right = shoulder_slope_right;
                        var belly_shape = data.belly_shape;
                        var style_arms = data.style_arms;
                        var style_seat = data.style_seat;
                        var sixth_ques_stoop = data.sixth_ques_stoop;
                        var shirt_que_1 = data.shirt_que_1;
                        var shirt_que_2 = data.shirt_que_2;
                        var shirt_que_3 = data.shirt_que_3;
                        var shirt_que_4 = data.shirt_que_4;
                        var shirt_que_5 = data.shirt_que_5;
                        var shirt_que_6 = data.shirt_que_6;
                        var suit_que_1 = data.suit_que_1;
                        var suit_que_2 = data.suit_que_2;
                        var suit_que_3 = data.suit_que_3;
                        var suit_que_4 = data.suit_que_4;
                        var suit_que_5 = data.suit_que_5;
                        var vest_que_1 = data.vest_que_1;
                        var pant_que_1 = data.pant_que_1;


                    var orderModalData = '' +
                        '</br><img src="images/logo.png" style="margin-bottom: 20px; margin-left: 2%; width: 60px;"><div class="ddxq_tc"><h2>Supplier Information</h2><div class="ddkfxx"><ul><li style="border-bottom: medium none;"><p class="p1">' +
                        'Name Company</p><p class="p2">' + supplier_name + '</p></li><li style="border-bottom: medium none;"><p class="p1">Contact Person</p>' +
                        '<p class="p2">' + company_mobile_no + '</p></li><li class="gender" style="border-bottom: medium none;"><p class="p1">Address</p>' +
                        '<p class="p2">' + company_address1 + ','+ company_address2 +'</p></li><li style="border-bottom: medium none;"><p class="p1">City</p><p class="p2">' +
                        city + '</p></li><li style="border-bottom: medium none;"><p class="p1">Country</p><p class="p2">' +
                        country + '</p></li><li style="border-bottom: medium none"><p class="p1">Phone Number</p><p class="p2">' +
                        company_mobile_no + '</p></li><li><p class="p1">Email</p><p class="p2">' + contact_email + '</p></li></ul>' +
                        '</div><h2>Customer Information</h2><div class="ddkfxx"><ul><li style="border-bottom: medium none;"><p class="p1">' +
                        'Order#</p><p class="p2">' + order_number + '</p></li><li style="border-bottom: medium none;"><p class="p1">Customer Name</p>' +
                        '<p class="p2">' + userName + '</p></li><li class="gender" style="border-bottom: medium none;"><p class="p1">Email</p>' +
                        '<p class="p2" id="user_email">' + userEmail + '</p></li><li style="border-bottom: medium none;"><p class="p1">Phone</p><p class="p2">' + userTelephone + '</p></li><li style="border-bottom: medium none;"><p class="p1">Weight</p><p class="p2">' + userWeight + ' Pounds</p></li><li style="border-bottom: medium none"><p class="p1">Height</p><p class="p2">' + userHeight + ' Inches</p></li><li><p class="p1">Salesman</p><p class="p2">' + store_name + '</p></li></ul>' +
                        '</div><h2>Delivery address</h2><div class="ddkfxx"><ul><li style=""><p class="p1">' +
                        'Address</p><p class="p2">SF Tailors 272 Hogans, Valley Way, Cary NC 27513 USA</p></li></ul></div><h2>Order Details</h2><div class="ddkfxx"><ul><li style="border-bottom: medium none;"><p class="p1">OrderNO.</p>' +
                        '<p class="p2">' + order_number + '</p></li><li style="border-bottom: medium none;"><p class="p1">User</p>' +
                        '<p class="p2">' + store_name + '</p></li><li style="border-bottom: medium none;"><p class="p1">OrderDate</p>' +
                        '<p class="p2">' + order_date + '</p></li><li style="border-bottom: medium none;"><p class="p1">Payment Status</p>' +
                        '<p class="p2">' + payment_status + '</p></li><li style="border-bottom: medium none;"><p class="p1">Product Type</p>' +
                        '<p class="p2">' + product_type + '</p></li><li><p class="p1" style="overflow:hidden;text-overflow:ellipsis">Product Name</p>' +
                        '<p class="p2"><input class="ipfield" type="text" id="det_product_name" ' +
                        'onchange=updateData("' + orderRowId + '","wp_orders_detail","det_product_name",this) value="' + det_product_name + '"></p></li>' +
                        '</ul></div><h2 class="img">Customization Information</h2><div class="ddkfxx">' +
                        '<div class="leftimg"></div>' + customizationInformation + '</div><h2>Monogram</h2><div class="ddkfxx"><ul><li style=""><p class="p1">' +
                        'Mono Gram Text</p><p class="p2">'+ orderDetRowData.s_monogramtext +'</p></li><li style=""><p class="p1">' +
                        'Mono Gram Code</p><p class="p2">'+ orderDetRowData.s_monogramcode +'</p></li><li style=""><p class="p1">' +
                        'Mono Gram Font</p><p class="p2">'+ orderDetRowData.s_monogramfont +'</p></li><li style=""><p class="p1">' +
                        'Mono Gram Color</p><p class="p2">'+ orderDetRowData.s_monogramcolor +'</p></li><li style=""><p class="p1">' +
                        'Mono Gram Position</p><p class="p2">'+ orderDetRowData.s_monogramposition +'</p></li><li style=""><p class="p1">' +
                        'Mono Gram Option</p><p class="p2">'+ orderDetRowData.s_monogramoption +'</p></li></ul></div><h2>Body Style</h2><div class="ddkfxx"><ul><li style=""><p class="p1">' +
                        'Shoulder Forward</p><p class="p2">'+ shoulder_forward +'</p></li><li style=""><p class="p1">' +
                        'Body Forward</p><p class="p2">'+ body_forward +'</p></li><li style=""><p class="p1">' +
                        'Shoulder Slope Left</p><p class="p2">'+ shoulder_slope_left +'</p></li><li style=""><p class="p1">' +
                        'Shoulder Slope Right</p><p class="p2">'+ shoulder_slope_right +'</p></li><li style=""><p class="p1">' +
                        'Belly Shape</p><p class="p2">'+ belly_shape +'</p></li><li style=""><p class="p1">' +
                        'Style Arms</p><p class="p2">'+ style_arms +'</p></li><li style=""><p class="p1">' +
                        'Style Seat</p><p class="p2">'+ style_seat +'</p></li><li style=""><p class="p1">' +
                        'Sixth Question Slope</p><p class="p2">'+ sixth_ques_stoop +'</p></li><li style=""><p class="p1">' +
                        'Shirt Question 1</p><p class="p2">'+ shirt_que_1 +'</p></li><li style=""><p class="p1">' +
                        'Shirt Question 2</p><p class="p2">'+ shirt_que_2 +'</p></li><li style=""><p class="p1">' +
                        'Shirt Question 3</p><p class="p2">'+ shirt_que_3 +'</p></li><li style=""><p class="p1">' +
                        'Shirt Question 4</p><p class="p2">'+ shirt_que_4 +'</p></li><li style=""><p class="p1">' +
                        'Shirt Question 5</p><p class="p2">'+ shirt_que_5 +'</p></li><li style=""><p class="p1">' +
                        'Shirt Question 6</p><p class="p2">'+ shirt_que_6 +'</p></li><li style=""><p class="p1">' +
                        'Suit Question 1</p><p class="p2">'+ suit_que_1 +'</p></li><li style=""><p class="p1">' +
                        'Suit Question 2</p><p class="p2">'+ suit_que_2 +'</p></li><li style=""><p class="p1">' +
                        'Suit Question 3</p><p class="p2">'+ suit_que_3 +'</p></li><li style=""><p class="p1">' +
                        'Suit Question 4</p><p class="p2">'+ suit_que_4 +'</p></li><li style=""><p class="p1">' +
                        'Suit Question 5</p><p class="p2">'+ suit_que_5 +'</p></li><li style=""><p class="p1">' +
                        'Vest Question 1</p><p class="p2">'+ vest_que_1 +'</p></li><li style=""><p class="p1">' +
                        'Pant Question 1</p><p class="p2">'+ pant_que_1 +'</p></li></ul></div><h2>Measurements</h2><div class="size_xx">' +
                        measurementsitemData + '</dl></div></div>';
                    $(".order-detail-modal").html(orderModalData);
                    });
                },1000);
            });

            $("#layui-layer-shade5").show();
            $("#layui-layer5").show();

        }
        function sendEmail(orderid){
            $(".loader").show();
            var SupplierEmail = $("#user_email").text();
            var url = "api/supplierProcess.php";
            $.post(url,{"type":"singleUserEmail","supplier_email":SupplierEmail,"orderid":orderid}, function (data) {
                //alert(data);
                window.location="supplier_order.php";
            });
        }
        function close_diolog() {
            $("#layui-layer-shade5").hide();
            $("#layui-layer5").hide();
        }
        function editDiv() {
            $(".inputv").show();
            $(".pt").hide();
        }
        function updateDiv(orderId, userId) {
            var inputs = $(".inputv");
            var levels = [];
            for (var i = 0; i < inputs.length; i++) {
                var level1 = $(inputs[i]).val();
                levels.push(level1);
            }
            var levelss = levels.join(',');
            var url = "api/orderProcess.php";
            $.post(url, {
                "type": "updateOrder",
                "order_id": orderId,
                "all_fields": levelss,
                "user_id": userId
            }, function (data) {
                var Status = data.Status;
                var Message = data.Message;
                if (Status == "Success") {
                    alert(Message);
                    window.location = 'supplier_order.php';
                }
            });
        }
        function updateData(det_id, table, column, obj) {
            var value = $("#" + obj.id).val();
            var url = "api/orderProcess.php";
            $.post(url, {
                "type": "updateEditDetail",
                "order_detail_id": det_id,
                "column": column,
                "value": value,
                "table": table
            });
        }
    </script>
<?php
include("footer.php");
?>