<?php
include('header.php');
$meas_id = "";
if(isset($_REQUEST['m'])){
    $meas_id = $_REQUEST['m'];
}
echo "<input type='hidden' value='".$meas_id."' id='meas_id' />";
?>
<style>
    .boxes {
        background: white;
        min-height: 100px;
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }
    .boxes .tabdiv {
        background: #eee none repeat scroll 0 0;
        border: 1px solid;
        font-weight: bold;
        margin-top: 22px;
        padding: 8px;
        text-align: center;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .savebtn {
        letter-spacing: 1px;
        margin-bottom: 18px;
        text-transform: uppercase;
    }
    .err {
        text-align: center;
        color: red;
    }

    .fabric-banner {
        background: rgba(0, 0, 0, 0) url("images/slide4.jpg") no-repeat scroll 0 0 / cover !important;
        min-height: 495px;
    }

    .bs-docs-example {
        margin: 1em -5em;
    }

</style>
<div class="loaderdiv">
    <img src="images/preloader.gif"/>
</div>
<div class="banner fabric-banner">
    <div class="bann-info"></div>
</div>
<div class="clear"></div>
<div class="about-bottom wthree-3">
    <div class="container">
        <div class="col-md-12 step" id="step10" style="text-align: left;">
            <h2 class="tittle">Save <span style="color:#000;">Measurement</span></h2>
            <h3 onclick="save()" style="margin-bottom: 50px; background: rgb(212, 80, 79) none repeat scroll 0% 0%; text-align: center;
            color: white; padding: 8px; width:45%;margin-left:27%;cursor:pointer;">Save Measurement</h3>
            <div class="col-md-12">
            </div>
            <!--<div class="col-md-7" style="margin-bottom:30px;margin-top:60px;">
                <input type="button" value="Save Measurement" onclick="save()" class="btn btn-primary pull-right" style="font-size: 19px; padding: 3px 53px; border-radius: 2px;"/>
            </div>-->
        </div>
    </div>
</div>

<div class="about-bottom wthree-3" style="display:none">
    <!-- top tiles -->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="col-md-12 boxes">
                <hr>
                <div class="col-md-12">
                    <div class="col-md-12" id="dimes_ul">
                        <div class="form-inline pull-right" style="margin-bottom:10px;">
                            <label>Unit Parameter's : </label>
                            <select class="form-control paramselect" onchange="convertData()" style="width:200px" id="params">
                                <option value="Inches" selected="selected">Inches</option>
                                <option value="Cm">Cm</option>
                            </select>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                            <th colspan="3" style="text-align: center">Body Measurement</th>
                            <th colspan="1" style="text-align: center">Body Style Adjustment</th>
                            <th colspan="4" style="text-align: center">Extra Allowance</th>
                            <th colspan="8" style="text-align: center">Final Measurement</th>
                            </thead>
                            <tbody>
                            <tr>
                                <th>S.No.</th>
                                <th>Parameters</th>
                                <th>Values</th>
                                <th>Body Style</th>
                                <th>Shirt</th>
                                <th>Jacket</th>
                                <th>Vest</th>
                                <th>Pant</th>
                                <th>Shirt</th>
                                <th>Jacket</th>
                                <th>Vest</th>
                                <th>Pant<br>(No Pleat)</th>
                                <th>Pant<br>(One Pleat < 2.0 cm)</th>
                                <th>Pant<br>(One Pleat in b/w 2.0 and 2.5 cm)</th>
                                <th>Pant<br>(Two Pleat <= 2.5cm)</th>
                                <th>Pant<br>(Three Pleat <= 2.5cm)</th>
                            </tr>
                            <tr>
                                <td>1.</td>
                                <td><span>Collar</span><span id="neck_text"></span></td>
                                <td id="collar"></td>
                                <td id="collar_body"></td>
                                <td id="collar_shirt_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_collar')"></i>
                                    <span id="final_shirt_collar">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_collar')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>2.</td>
                                <td><span>Chest</span><span id="chest_text"></span></td>
                                <td id="chest_front"></td>
                                <td id="chest_body"></td>
                                <td id="chest_shirt_allow">0</td>
                                <td id="chest_jacket_allow">0</td>
                                <td id="chest_vest_allow">0</td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_chest')"></i>
                                    <span id="final_shirt_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_chest')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_chest')"></i>
                                    <span id="final_jacket_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_chest')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_chest')"></i>
                                    <span id="final_vest_chest">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_chest')"></i>
                                </td>
                                <td></td>
                                <td id="chest_pant_1allow"></td>
                                <td id="chest_pant_11allow"></td>
                                <td id="chest_pant_2allow"></td>
                                <td id="chest_pant_3allow"></td>
                            </tr>
                            <tr>
                                <td>3.</td>
                                <td><span>Waist Horizontal Line (stomach)</span><span id="stomach_text"></span></td>
                                <td id="stomach"></td>
                                <td id="stomach_body"></td>
                                <td id="stomach_shirt_allow">0</td>
                                <td id="stomach_jacket_allow">0</td>
                                <td id="stomach_vest_allow">0</td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_stomach')"></i>
                                    <span id="final_shirt_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_stomach')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_stomach')"></i>
                                    <span id="final_jacket_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_stomach')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_stomach')"></i>
                                    <span id="final_vest_stomach">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_stomach')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>4.</td>
                                <td><span>Seat</span><span id="seat_text"></span></td>
                                <td id="seat_front"></td>
                                <td id="seat_body"></td>
                                <td id="seat_shirt_allow">0</td>
                                <td id="seat_jacket_allow">0</td>
                                <td></td>
                                <td id="seat_pant_allow">0</td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_seat')"></i>
                                    <span id="final_shirt_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_seat')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_seat')"></i>
                                    <span id="final_jacket_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_seat')"></i>
                                </td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_seat')"></i>
                                    <span id="final_pant_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_seat')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant1_seat')"></i>
                                    <span id="final_pant1_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant1_seat')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant11_seat')"></i>
                                    <span id="final_pant11_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant11_seat')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant2_seat')"></i>
                                    <span id="final_pant2_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant2_seat')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant3_seat')"></i>
                                    <span id="final_pant3_seat">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant3_seat')"></i>
                                </td>
                            </tr>
                            <tr>
                                <td>5.</td>
                                <td><span>Bicep</span><span id="bicep_text"></span></td>
                                <td id="bicep_front"></td>
                                <td id="bicep_body"></td>
                                <td id="bicep_shirt_allow">0</td>
                                <td id="bicep_jacket_allow">0</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_bicep')"></i>
                                    <span id="final_shirt_bicep">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_bicep')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_bicep')"></i>
                                    <span id="final_jacket_bicep">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_bicep')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>6.</td>
                                <td><span>Back Shoulder</span><span id="shoulder_textb"></span></td>
                                <td id="shoulder_back"></td>
                                <td id="back_shoulder_body"></td>
                                <td id="back_shoulder_shirt_allow">0</td>
                                <td id="back_shoulder_jacket_allow">0</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_back_shoulder')"></i>
                                    <span id="final_shirt_back_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_back_shoulder')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_back_shoulder')"></i>
                                    <span id="final_jacket_back_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_back_shoulder')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>7.</td>
                                <td><span>Front Shoulder</span><span id="shoulder_textf"></span></td>
                                <td id="shoulder_front"></td>
                                <td id="front_shoulder_body"></td>
                                <td id="front_shoulder_shirt_allow">0</td>
                                <td id="front_shoulder_jacket_allow">0</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_front_shoulder')"></i>
                                    <span id="final_shirt_front_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_front_shoulder')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_front_shoulder')"></i>
                                    <span id="final_jacket_front_shoulder">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_front_shoulder')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>8.</td>
                                <td><span>Sleeve length (right)</span><span id="sleeve_righttext"></span></td>
                                <td id="shoulder_sleeve_right"></td>
                                <td id="sleeve_right_body"></td>
                                <td id="sleeve_right_shirt_allow">0</td>
                                <td id="sleeve_right_jacket_allow">0</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_sleeve_right')"></i>
                                    <span id="final_shirt_sleeve_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_sleeve_right')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_sleeve_right')"></i>
                                    <span id="final_jacket_sleeve_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_sleeve_right')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>9.</td>
                                <td><span>Sleeve length (left)</span><span id="sleeve_lefttext"></span></td>
                                <td id="shoulder_sleeve_left"></td>
                                <td id="sleeve_left_body"></td>
                                <td id="sleeve_left_shirt_allow">0</td>
                                <td id="sleeve_left_jacket_allow">0</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_sleeve_left')"></i>
                                    <span id="final_shirt_sleeve_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_sleeve_left')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_sleeve_left')"></i>
                                    <span id="final_jacket_sleeve_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_sleeve_left')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>10.</td>
                                <td><span>Thigh</span></td>
                                <td id="thigh"></td>
                                <td id="thigh_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="thigh_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_thigh')"></i>
                                    <span id="final_pant_thigh">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_thigh')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>11.</td>
                                <td><span>Nape to waist (back)</span></td>
                                <td id="napetowaist_back"></td>
                                <td id="nape_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>12.</td>
                                <td><span>Front waist length</span></td>
                                <td id="front_waist_length"></td>
                                <td id="front_waist_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>13.</td>
                                <td><span>Wrist</span></td>
                                <td id="wrist"></td>
                                <td id="wrist_body"></td>
                                <td id="wrist_shirt_allow">0</td>
                                <td id="wrist_jacket_allow">0</td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_wrist')"></i>
                                    <span id="final_shirt_wrist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_wrist')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_wrist')"></i>
                                    <span id="final_jacket_wrist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_wrist')"></i>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>14.</td>
                                <td><span>Waist</span></td>
                                <td id="waist"></td>
                                <td id="waist_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="waist_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_waist')"></i>
                                    <span id="final_pant_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_waist')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>15.</td>
                                <td><span>Calf</span><span id="calf_text"></span></td>
                                <td id="calf"></td>
                                <td id="calf_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="calf_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_calf')"></i>
                                    <span id="final_pant_calf">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_calf')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>16.</td>
                                <td><span>Back Waist Height</span></td>
                                <td id="back_waist_height"></td>
                                <td id="b_waist_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="b_waist_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_b_waist')"></i>
                                    <span id="final_pant_b_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_b_waist')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>17.</td>
                                <td><span>Front Waist Height</span></td>
                                <td id="front_waist_height"></td>
                                <td id="f_waist_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="f_waist_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_f_waist')"></i>
                                    <span id="final_pant_f_waist">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_f_waist')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>18.</td>
                                <td><span>Knee</span></td>
                                <td id="knee_right"></td>
                                <td id="knee_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="knee_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_knee')"></i>
                                    <span id="final_pant_knee">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_knee')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>19.</td>
                                <td><span>Back_Jacket_Length</span><span id="back_lengthtext"></span></td>
                                <td id="back_jacket_length"></td>
                                <td id="jacket_body"></td>
                                <td id="jacket_shirt_allow">0</td>
                                <td id="jacket_jacket_allow">0</td>
                                <td id="jacket_vest_allow">0</td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_shirt_jacket')"></i>
                                    <span id="final_shirt_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_shirt_jacket')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_jacket_jacket')"></i>
                                    <span id="final_jacket_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_jacket_jacket')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_vest_jacket')"></i>
                                    <span id="final_vest_jacket">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_vest_jacket')"></i>
                                </td>
                                <td></td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>20.</td>
                                <td><span>U-rise</span></td>
                                <td id="u_rise"></td>
                                <td id="rise_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="rise_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_rise')"></i>
                                    <span id="final_pant_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_rise')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant1_rise')"></i>
                                    <span id="final_pant1_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant1_rise')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant11_rise')"></i>
                                    <span id="final_pant11_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant11_rise')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant2_rise')"></i>
                                    <span id="final_pant2_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant2_rise')"></i>
                                </td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant3_rise')"></i>
                                    <span id="final_pant3_rise">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant3_rise')"></i>
                                </td>

                            </tr>
                            <tr>
                                <td>21.</td>
                                <td><span>Pant Left Outseam</span></td>
                                <td id="outseam_l_pants"></td>
                                <td id="p_left_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="p_left_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_p_left')"></i>
                                    <span id="final_pant_p_left">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_p_left')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>22.</td>
                                <td><span>Pant Right Outseam</span></td>
                                <td id="outseam_r_pants"></td>
                                <td id="p_right_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="p_right_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_p_right')"></i>
                                    <span id="final_pant_p_right">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_p_right')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            <tr>
                                <td>23.</td>
                                <td><span>Bottom</span></td>
                                <td id="bottom"></td>
                                <td id="bottom_body"></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td id="bottom_pant_allow">0</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <i class="fa fa-minus" style="margin-right:5px;cursor:pointer"
                                       onclick="incdec('minus','final_pant_bottom')"></i>
                                    <span id="final_pant_bottom">0</span>
                                    <i class="fa fa-plus" style="margin-left:5px;cursor:pointer"
                                       onclick="incdec('plus','final_pant_bottom')"></i>
                                </td>
                                <td></td><td></td><td></td><td></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="col-md-12" style="padding:0">
                            <input type="button" class="btn btn-info pull-right savebtn" value="Save Measurements" onclick="save()"/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /page content -->
<?php
    include('footer.php');
?>
<script>
    var answer = "";
    var ques1,ques2,ques3,ques4,ques5,ques6,svalue,jvalue,pvalue,vvalue,value,answer1,answer2,answer3,answer4,
    answer5,answer6,janswer1,janswer2,janswer3,vanswer1,panswer1,collarshirt,chestshirt,chestjacket,chestvest,
    stomachshirt,stomachjacket,stomachvest,seatshirt,seatjacket,seatpant,bicepshirt,bicepjacket,backshouldershirt,
    backshoulderjacket,frontshouldershirt,frontshoulderjacket,sleeveleftshirt,sleeveleftjacket,sleeverightshirt,
    sleeverightjacket,thighpant,wristshirt,wristjacket,calfpant,backwaistheightpant,frontwaistheightpant,kneepant,
    backjacketlengthshirt,backjacketlengthjacket,backjacketlengthvest,risepant,pantleftpant,pantrightpant,bottompant,
    frontshoulderbody,backshoulderbody,chest_min,chest_max,bicep_min,bicep_max,back_jacket_length_min,
    back_jacket_length_max,stomach_min,stomach_max,seat_min,seat_max,sleeve_left_min,sleeve_left_max,sleeve_right_min,
    sleeve_right_max,back_shoulder_min,back_shoulder_max,front_shoulder_min,front_shoulder_max,collar_min,collar_max,
    calf_min,calf_max;
    function getMeasureData(sizeData) {
        answer1 = sizeData.shirt_que_1;
        answer2 = sizeData.shirt_que_2;
        answer3 = sizeData.shirt_que_3;
        answer4 = sizeData.shirt_que_4;
        answer5 = sizeData.shirt_que_5;
        answer6 = sizeData.shirt_que_6;
        janswer1 = sizeData.suit_que_1;
        janswer2 = sizeData.suit_que_2;
        janswer3 = sizeData.suit_que_3;
        vanswer1 = sizeData.vest_que_1;
        panswer1 = sizeData.pant_que_1;
        if (sizeData.unit_type == "Inches") {
            var chest = parseFloat(sizeData.chest)*2.54;///cm conversion
            var stomach = parseFloat(sizeData.stomach)*2.54;///cm conversion
            ////collar shirt
            if (chest-stomach < 7) {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            collarshirt = svalue;
            jvalue = value.split(";")[1];
            collarjacket = jvalue;
            vvalue = value.split(";")[2];
            collarvest = vvalue;
            pvalue = value.split(";")[3];
            collarpant = pvalue;
            var collar_shirt = parseFloat(svalue);
            $("#collar_shirt_allow").html((collar_shirt / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.collar) + parseFloat(collar_shirt / 2.54)).toFixed(1);
            $("#final_shirt_collar").html(val + " Inches");
            if(val<((collar_min / 2.54).toFixed(1)) || val>((collar_max / 2.54).toFixed(1))){
                $("#final_shirt_collar").css("color","red");
            }
            // for collar jacket
            var collar_jacket = (parseFloat(jvalue));
            $("#collar_jacket_allow").html((collar_jacket / 2.54).toFixed(1) + " Inches");
            // for collar vest
            var collar_vest = (parseFloat(vvalue));
            $("#collar_vest_allow").html((collar_vest / 2.54).toFixed(1) + " Inches");

            //for chest shirt
            if (chest-stomach < 7) {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            chestshirt = svalue;
            jvalue = value.split(";")[1];
            chestjacket = jvalue;
            vvalue = value.split(";")[2];
            chestvest = vvalue;
            pvalue = value.split(";")[3];
            chestpant = pvalue;
            // for chest shirt
            var chest_shirt = parseFloat(svalue);
            $("#chest_shirt_allow").html((chest_shirt / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.chest) + parseFloat(chest_shirt / 2.54)).toFixed(1);
            $("#final_shirt_chest").html(val + " Inches");
            if(val<((chest_min / 2.54).toFixed(1)) || val>((chest_max / 2.54).toFixed(1))){
                $("#final_shirt_chest").css("color","red");
            }
            // for chest jacket
            var chest_jacket = (parseFloat(jvalue));
            $("#chest_jacket_allow").html((chest_jacket / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.chest) + parseFloat(chest_jacket / 2.54)).toFixed(1);
            $("#final_jacket_chest").html(val + " Inches");
            if(val<((chest_min / 2.54).toFixed(1)) || val>((chest_max / 2.54).toFixed(1))){
                $("#final_jacket_chest").css("color","red");
            }
            // for chest vest
            var chest_vest = parseFloat(vvalue);
            $("#chest_vest_allow").html((chest_vest / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.chest) + parseFloat(chest_vest / 2.54)).toFixed(1);
            $("#final_vest_chest").html(val+ " Inches");
            if(val<((chest_min / 2.54).toFixed(1)) || val>((chest_max / 2.54).toFixed(1))){
                $("#final_vest_chest").css("color","red");
            }

            ////////for Stomach shirt
            if (chest-stomach < 7) {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            stomachshirt = svalue;
            jvalue = value.split(";")[1];
            stomachjacket = jvalue;
            vvalue = value.split(";")[2];
            stomachvest = vvalue;
            pvalue = value.split(";")[3];
            stomachpant = pvalue;
            var stomach_shirt = parseFloat(svalue);
            $("#stomach_shirt_allow").html((stomach_shirt / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.stomach) + parseFloat(stomach_shirt / 2.54)).toFixed(1);
            $("#final_shirt_stomach").html(val+ " Inches");
            if(val<((stomach_min / 2.54).toFixed(1)) || val>((stomach_max / 2.54).toFixed(1))){
                $("#final_shirt_stomach").css("color","red");
            }
            // for stomach jacket
            var stomach_jacket = (parseFloat(jvalue));
            $("#stomach_jacket_allow").html((stomach_jacket / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.stomach) + parseFloat(stomach_jacket / 2.54)).toFixed(1);
            $("#final_jacket_stomach").html(val + " Inches");
            if(val<((stomach_min / 2.54).toFixed(1)) || val>((stomach_max / 2.54).toFixed(1))){
                $("#final_jacket_stomach").css("color","red");
            }
            // for stomach vest
            var stomach_vest = (parseFloat(vvalue));
            $("#stomach_vest_allow").html((stomach_vest / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.stomach) + parseFloat(stomach_vest / 2.54)).toFixed(1);
            $("#final_vest_stomach").html( val+ " Inches");
            if(val<((stomach_min / 2.54).toFixed(1)) || val>((stomach_max / 2.54).toFixed(1))){
                $("#final_vest_stomach").css("color","red");
            }
            // for stomach pant
            var stomach_pant = (parseFloat(pvalue));
            $("#stomach_pant_allow").html((stomach_pant / 2.54).toFixed(1) + " Inches");

            //for seat shirt
            if (chest-stomach < 7) {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            seatshirt = svalue;
            jvalue = value.split(";")[1];
            seatjacket = jvalue;
            vvalue = value.split(";")[2];
            seatvest = vvalue;
            pvalue = value.split(";")[3];
            seatpant = pvalue;
            // for seat shirt
            var seat_shirt = parseFloat(svalue);
            $("#seat_shirt_allow").html((seat_shirt / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.seat) + parseFloat(seat_shirt / 2.54)).toFixed(1);
            $("#final_shirt_seat").html( val+ " Inches");
            if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                $("#final_shirt_seat").css("color","red");
            }
            // for seat jacket
            var seat_jacket = (parseFloat(jvalue));
            $("#seat_jacket_allow").html((seat_jacket / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.seat) + parseFloat(seat_jacket / 2.54)).toFixed(1);
            $("#final_jacket_seat").html(val + " Inches");
            if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                $("#final_jacket_seat").css("color","red");
            }
            // for seat vest
            var seat_vest = parseFloat(vvalue);
            $("#seat_vest_allow").html((seat_vest / 2.54).toFixed(1) + " Inches");
            // for seat_pant
            var seat_pant = (parseFloat(pvalue));
            $("#seat_pant_allow").html((seat_pant / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.seat) + parseFloat(seat_pant / 2.54)).toFixed(1);
            $("#final_pant_seat").html(val+ " Inches");
            if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))) {
                $("#final_pant_seat").css("color", "red");
            }

            ///////for bicep shirt
            if (chest-stomach < 7) {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bicepshirt = svalue;
            jvalue = value.split(";")[1];
            bicepjacket = jvalue;
            vvalue = value.split(";")[2];
            bicepvest = vvalue;
            pvalue = value.split(";")[3];
            biceppant = pvalue;
            var bicep_shirt = parseFloat(svalue);
            $("#bicep_shirt_allow").html((bicep_shirt / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.bicep) + parseFloat(bicep_shirt / 2.54)).toFixed(1);
            $("#final_shirt_bicep").html(val+ " Inches");
            if(val<((bicep_min / 2.54).toFixed(1)) || val>((bicep_max / 2.54).toFixed(1))){
                $("#final_shirt_bicep").css("color","red");
            }
            //for bicep jacket
            var bicep_jacket = (parseFloat(jvalue));
            $("#bicep_jacket_allow").html((bicep_jacket / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.bicep) + parseFloat(bicep_jacket / 2.54)).toFixed(1);
            $("#final_jacket_bicep").html(val+" Inches");
            if(val<((bicep_min / 2.54).toFixed(1)) || val>((bicep_max / 2.54).toFixed(1))){
                $("#final_shirt_bicep").css("color","red");
            }
            // for bicep vest
            var bicep_vest = parseFloat(vvalue);
            $("#bicep_vest_allow").html((bicep_vest / 2.54).toFixed(1) + " Inches");

            //for back shoulder shirt
            if (chest-stomach < 7) {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backshouldershirt = svalue;
            jvalue = value.split(";")[1];
            backshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            backshouldervest = vvalue;
            pvalue = value.split(";")[3];
            backshoulderpant = pvalue;
            //for back shoulder shirt
            var back_shoulder_shirt = parseFloat(svalue);
            $("#back_shoulder_shirt_allow").html((back_shoulder_shirt / 2.54).toFixed(1) + " Inches");
            var temp_back_shoulder_shirt = parseFloat(sizeData.back_sholuder)+(parseFloat($("#back_shoulder_body").text().split(" ")[0]));
            var val = parseFloat(parseFloat(temp_back_shoulder_shirt) + parseFloat(back_shoulder_shirt / 2.54)).toFixed(1);
            $("#final_shirt_back_shoulder").html(val + " Inches");
            if(val<((back_shoulder_min / 2.54).toFixed(1)) || val>((back_shoulder_max / 2.54).toFixed(1))){
                $("#final_shirt_back_shoulder").css("color","red");
            }
            //for back shoulder jacket
            var back_shoulder_jacket = (parseFloat(jvalue));
            $("#back_shoulder_jacket_allow").html((back_shoulder_jacket / 2.54).toFixed(1) + " Inches");
            var temp_back_shoulder_jacket = parseFloat(sizeData.back_sholuder)+(parseFloat($("#back_shoulder_body").text().split(" ")[0]));
            var val = parseFloat(parseFloat(temp_back_shoulder_jacket) + parseFloat(back_shoulder_jacket / 2.54)).toFixed(1)
            $("#final_jacket_back_shoulder").html(val + " Inches");
            if(val<((back_shoulder_min / 2.54).toFixed(1)) || val>((back_shoulder_max / 2.54).toFixed(1))){
                $("#final_jacket_back_shoulder").css("color","red");
            }
            // for back shoulder vest
            var back_shoulder_vest = parseFloat(vvalue);
            $("#back_shoulder_vest_allow").html((back_shoulder_vest / 2.54).toFixed(1) + " Inches");

            ////////for front shoulder shirt
            if (chest-stomach < 7) {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontshouldershirt = svalue;
            jvalue = value.split(";")[1];
            frontshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            frontshouldervest = vvalue;
            pvalue = value.split(";")[3];
            frontshoulderpant = pvalue;
            // for front shoulder shirt
            var front_shoulder_shirt = parseFloat(svalue);
            $("#front_shoulder_shirt_allow").html((front_shoulder_shirt / 2.54).toFixed(1) + " Inches");
            var temp_front_shoulder_shirt = parseFloat(sizeData.front_shoulder)+(parseFloat($("#front_shoulder_body").text().split(" ")[0]));
            var val = parseFloat(parseFloat(temp_front_shoulder_shirt) + parseFloat(front_shoulder_shirt / 2.54)).toFixed(1);
            $("#final_shirt_front_shoulder").html(val + " Inches");
            if(val<((front_shoulder_min / 2.54).toFixed(1)) || val>((front_shoulder_max / 2.54).toFixed(1))){
                $("#final_shirt_front_shoulder").css("color","red");
            }
            //for front shoulder jacket
            var front_shoulder_jacket = (parseFloat(jvalue));
            $("#front_shoulder_jacket_allow").html((front_shoulder_jacket / 2.54).toFixed(1) + " Inches");
            var temp_front_shoulder_jacket = parseFloat(sizeData.front_shoulder)+(parseFloat($("#front_shoulder_body").text().split(" ")[0]));
            var val = parseFloat(parseFloat(temp_front_shoulder_jacket) + parseFloat(front_shoulder_jacket / 2.54)).toFixed(1);
            $("#final_jacket_front_shoulder").html(val + " Inches");
            if(val<((front_shoulder_min / 2.54).toFixed(1)) || val>((front_shoulder_max / 2.54).toFixed(1))){
                $("#final_jacket_front_shoulder").css("color","red");
            }
            // for front shoulder vest
            var front_shoulder_vest = parseFloat(vvalue);
            $("#front_shoulder_vest_allow").html((front_shoulder_vest / 2.54).toFixed(1) + " Inches");

            //for left_sleeve_length shirt
            if (chest-stomach < 7) {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeveleftshirt = svalue;
            jvalue = value.split(";")[1];
            sleeveleftjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeveleftvest = vvalue;
            pvalue = value.split(";")[3];
            sleeveleftpant = pvalue;
            var left_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_left_shirt_allow").html((left_sleeve_length_shirt / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_shirt / 2.54)).toFixed(1);
            $("#final_shirt_sleeve_left").html(val + " Inches");
            val = parseFloat(val);
            if(val<((sleeve_left_min / 2.54).toFixed(1)) || val>((sleeve_left_max / 2.54).toFixed(1))){
                $("#final_shirt_sleeve_left").css("color","red");
            }
            //for front shoulder jacket
            var left_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_left_jacket_allow").html((left_sleeve_length_jacket / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_jacket / 2.54)).toFixed(1);
            $("#final_jacket_sleeve_left").html(val + " Inches");
            val = parseFloat(val);
            if(val<((sleeve_left_min / 2.54).toFixed(1)) || val>((sleeve_left_max / 2.54).toFixed(1))){
                $("#final_jacket_sleeve_left").css("color","red");
            }
            //for front shoulder vest
            var left_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_left_vest_allow").html((left_sleeve_length_vest / 2.54).toFixed(1) + " Inches");

            /////for right_sleeve_length shirt
            if (chest-stomach < 7) {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeverightshirt = svalue;
            jvalue = value.split(";")[1];
            sleeverightjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeverightvest = vvalue;
            pvalue = value.split(";")[3];
            sleeverightpant = pvalue;
            //for right_sleeve_length shirt
            var right_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_right_shirt_allow").html((right_sleeve_length_shirt / 2.54).toFixed(1) + " Inches");
            var val = (parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_shirt / 2.54)).toFixed(1);
            $("#final_shirt_sleeve_right").html(val + " Inches");
            val = parseFloat(val);
            if(val<((sleeve_right_min / 2.54).toFixed(1)) || val>((sleeve_right_max / 2.54).toFixed(1))){
                $("#final_shirt_sleeve_right").css("color","red");
            }
            //for right_sleeve_length jacket
            var right_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_right_jacket_allow").html((right_sleeve_length_jacket / 2.54).toFixed(1) + " Inches");
            var val = (parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_jacket / 2.54)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_sleeve_right").html(val + " Inches");
            if(val<((sleeve_right_min / 2.54).toFixed(1)) || val>((sleeve_right_max / 2.54).toFixed(1))){
                $("#final_jacket_sleeve_right").css("color","red");
            }
            //for right_sleeve_length vest
            var right_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_right_vest_allow").html((right_sleeve_length_vest / 2.54).toFixed(1) + " Inches");

            //// for thigh shirt
            if (chest-stomach < 7) {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            thighshirt = svalue;
            jvalue = value.split(";")[1];
            thighjacket = jvalue;
            vvalue = value.split(";")[2];
            thighvest = vvalue;
            pvalue = value.split(";")[3];
            thighpant = pvalue;
            //for thigh shirt
            var thigh_shirt = parseFloat(svalue);
            $("#thigh_shirt_allow").html((thigh_shirt / 2.54).toFixed(1) + " Inches");
            //for thigh jacket
            var thigh_jacket = (parseFloat(jvalue));
            $("#thigh_jacket_allow").html((thigh_jacket / 2.54).toFixed(1) + " Inches");
            // for thigh vest
            var thigh_vest = parseFloat(vvalue);
            $("#thigh_vest_allow").html((thigh_vest / 2.54).toFixed(1) + " Inches");
            //for thigh pant
            var thigh_pant = (parseFloat(pvalue));
            $("#thigh_pant_allow").html((thigh_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_pant / 2.54)).toFixed(1) + " Inches");

            //for nape shirt
            if (chest-stomach < 7) {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            napetowaistshirt = svalue;
            jvalue = value.split(";")[1];
            napetowaistjacket = jvalue;
            vvalue = value.split(";")[2];
            napetowaistvest = vvalue;
            pvalue = value.split(";")[3];
            napetowaistpant = pvalue;
            var nape_to_waist_shirt = parseFloat(svalue);
            $("#nape_shirt_allow").html((nape_to_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var nape_to_waist_jacket = (parseFloat(jvalue));
            $("#nape_jacket_allow").html((nape_to_waist_jacket / 2.54).toFixed(1) + " Inches");
            // for nape_to_waist vest
            var nape_to_waist_vest = parseFloat(vvalue);
            $("#nape_vest_allow").html((nape_to_waist_vest / 2.54).toFixed(1) + " Inches");
            //for nape_to_waist pant
            var nape_to_waist_pant = (parseFloat(pvalue));
            $("#nape_pant_allow").html((nape_to_waist_pant / 2.54).toFixed(1) + " Inches");

            ////////for front_waist shirt
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistlengthshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistlengthvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistlengthpant = pvalue;
            //for front shoulder shirt
            var front_waist_shirt = parseFloat(svalue);
            $("#front_waist_shirt_allow").html((front_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var front_waist_jacket = (parseFloat(jvalue));
            $("#front_waist_jacket_allow").html((front_waist_jacket / 2.54).toFixed(1) + " Inches");
            // for front shoulder vest
            var front_waist_vest = parseFloat(vvalue);
            $("#front_waist_vest_allow").html((front_waist_vest / 2.54).toFixed(1) + " Inches");
            // for front shoulder pant
            var front_waist_pant = (parseFloat(jvalue));
            $("#front_waist_pant_allow").html((front_waist_pant / 2.54).toFixed(1) + " Inches");

            //for wrist shirt
            if (chest-stomach < 7) {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            wristshirt = svalue;
            jvalue = value.split(";")[1];
            wristjacket = jvalue;
            vvalue = value.split(";")[2];
            wristvest = vvalue;
            pvalue = value.split(";")[3];
            wristpant = pvalue;
            var wrist_shirt = parseFloat(svalue);
            $("#wrist_shirt_allow").html((wrist_shirt / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_shirt / 2.54)).toFixed(1) + " Inches");
            //for front shoulder jacket
            var wrist_jacket = (parseFloat(jvalue));
            $("#wrist_jacket_allow").html((wrist_jacket / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_jacket / 2.54)).toFixed(1) + " Inches");
            // for wrist vest
            var wrist_vest = parseFloat(vvalue);
            $("#wrist_vest_allow").html((wrist_vest / 2.54).toFixed(1) + " Inches");
            //for wrist pant
            var wrist_pant = (parseFloat(pvalue));
            $("#wrist_pant_allow").html((wrist_pant / 2.54).toFixed(1) + " Inches");

            ///////for waist shirt
            if (chest-stomach < 7) {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            waistshirt = svalue;
            jvalue = value.split(";")[1];
            waistjacket = jvalue;
            vvalue = value.split(";")[2];
            waistvest = vvalue;
            pvalue = value.split(";")[3];
            waistpant = pvalue;
            var waist_shirt = parseFloat(svalue);
            $("#waist_shirt_allow").html((waist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var waist_jacket = (parseFloat(jvalue));
            $("#waist_jacket_allow").html((waist_jacket / 2.54).toFixed(1) + " Inches");
            // for waist vest
            var waist_vest = parseFloat(vvalue);
            $("#waist_vest_allow").html((waist_vest / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var waist_pant = (parseFloat(pvalue));
            $("#waist_pant_allow").html((waist_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_pant / 2.54)).toFixed(1) + " Inches");

            //for calf shirt
            if (chest-stomach < 7) {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            calfshirt = svalue;
            jvalue = value.split(";")[1];
            calfjacket = jvalue;
            vvalue = value.split(";")[2];
            calfvest = vvalue;
            pvalue = value.split(";")[3];
            calfpant = pvalue;
            var calf_shirt = parseFloat(svalue);
            $("#calf_shirt_allow").html((calf_shirt / 2.54).toFixed(1) + " Inches");
            //for calf pant
            var calf_pant = (parseFloat(pvalue));
            $("#calf_pant_allow").html((calf_pant / 2.54).toFixed(1) + " Inches");
            var val = parseFloat(parseFloat(sizeData.calf) + parseFloat(calf_pant / 2.54).toFixed(1));
            $("#final_pant_calf").html(val+ " Inches");
            if(val<((calf_min / 2.54).toFixed(1)) || val>((calf_max / 2.54).toFixed(1))){
                $("#final_pant_calf").css("color","red");
            }

            ////////for b_waist_shirt
            if (chest-stomach < 7) {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            backwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            backwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            backwaistheightpant = pvalue;
            //for b_waist shirt
            var b_waist_shirt = parseFloat(svalue);
            $("#b_waist_shirt_allow").html((b_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for b_waist jacket
            var b_waist_jacket = (parseFloat(jvalue));
            $("#b_waist_jacket_allow").html((b_waist_jacket / 2.54).toFixed(1) + " Inches");
            //for b_waist vest
            var b_waist_vest = parseFloat(vvalue);
            $("#b_waist_vest_allow").html((b_waist_vest / 2.54).toFixed(1) + " Inches");
            //for b_waist_pant
            var b_waist_pant = (parseFloat(pvalue));
            $("#b_waist_pant_allow").html((b_waist_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_pant / 2.54)).toFixed(1) + " Inches");

            //for f_waist_shirt
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistheightpant = pvalue;
            //for f_waist shirt
            var f_waist_shirt = parseFloat(svalue);
            $("#f_waist_shirt_allow").html((f_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for f_waist jacket
            var f_waist_jacket = (parseFloat(jvalue));
            $("#f_waist_jacket_allow").html((f_waist_jacket / 2.54).toFixed(1) + " Inches");
            //for f_waist vest
            var f_waist_vest = parseFloat(vvalue);
            $("#f_waist_vest_allow").html((f_waist_vest / 2.54).toFixed(1) + " Inches");
            //for f_waist pant
            var f_waist_pant = (parseFloat(pvalue));
            $("#f_waist_pant_allow").html((f_waist_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_pant / 2.54)).toFixed(1) + " Inches");

            /////for knee shirt
            if (chest-stomach < 7) {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            kneeshirt = svalue;
            jvalue = value.split(";")[1];
            kneejacket = jvalue;
            vvalue = value.split(";")[2];
            kneevest = vvalue;
            pvalue = value.split(";")[3];
            kneepant = pvalue;
            //for knee shirt
            var knee_shirt = parseFloat(svalue);
            $("#knee_shirt_allow").html((knee_shirt / 2.54).toFixed(1) + " Inches");
            //for knee pant
            var knee_pant = (parseFloat(pvalue));
            $("#knee_pant_allow").html((knee_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_pant / 2.54)).toFixed(1) + " Inches");

            //for back_jacket_length shirt
            if (chest-stomach < 7) {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backjacketlengthshirt = svalue;
            jvalue = value.split(";")[1];
            backjacketlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            backjacketlengthvest = vvalue;
            pvalue = value.split(";")[3];
            backjacketlengthpant = pvalue;
            //for back_jacket_length shirt
            var jacket_shirt = parseFloat(svalue);
            $("#jacket_shirt_allow").html((jacket_shirt / 2.54).toFixed(1) + " Inches");
            var val=parseFloat(parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_shirt / 2.54)).toFixed(1);
            $("#final_shirt_jacket").html(val+ " Inches");
            if(val<((back_jacket_length_min / 2.54).toFixed(1)) || val>((back_jacket_length_max / 2.54).toFixed(1))){
                $("#final_shirt_jacket").css("color","red");
            }
            //for back_jacket_length jacket
            var jacket_jacket = (parseFloat(jvalue));
            $("#jacket_jacket_allow").html((jacket_jacket / 2.54).toFixed(1) + " Inches");
            var val=parseFloat(parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_jacket / 2.54)).toFixed(1);
            $("#final_jacket_jacket").html(val + " Inches");
            if(val<((back_jacket_length_min / 2.54).toFixed(1)) || val>((back_jacket_length_max / 2.54).toFixed(1))){
                $("#final_jacket_jacket").css("color","red");
            }
            //for back_jacket_length vest
            var jacket_vest = parseFloat(vvalue);
            $("#jacket_vest_allow").html((jacket_vest / 2.54).toFixed(1) + " Inches");
            var val=parseFloat(parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_vest / 2.54)).toFixed(1);
            $("#final_vest_jacket").html(val+ " Inches");
            if(val<((back_jacket_length_min / 2.54).toFixed(1)) || val>((back_jacket_length_max / 2.54).toFixed(1))){
                $("#final_vest_jacket").css("color","red");
            }

            //for u_rise shirt
            if (chest-stomach < 7) {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            riseshirt = svalue;
            jvalue = value.split(";")[1];
            risejacket = jvalue;
            vvalue = value.split(";")[2];
            risevest = vvalue;
            pvalue = value.split(";")[3];
            risepant = pvalue;
            //for rise shirt
            var rise_shirt = parseFloat(svalue);
            $("#rise_shirt_allow").html((rise_shirt / 2.54).toFixed(1) + " Inches");
            //for rise jacket
            var rise_jacket = (parseFloat(jvalue));
            $("#rise_jacket_allow").html((rise_jacket / 2.54).toFixed(1) + " Inches");
            // for rise vest
            var rise_vest = parseFloat(vvalue);
            $("#rise_vest_allow").html((rise_vest / 2.54).toFixed(1) + " Inches");
            //for rise pant
            var rise_pant = (parseFloat(pvalue));
            $("#rise_pant_allow").html((rise_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_pant / 2.54)).toFixed(1) + " Inches");

            ////////for pant_left_shirt
            if (chest-stomach < 7) {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantleftshirt = svalue;
            jvalue = value.split(";")[1];
            pantleftjacket = jvalue;
            vvalue = value.split(";")[2];
            pantleftvest = vvalue;
            pvalue = value.split(";")[3];
            pantleftpant = pvalue;
            var p_left_shirt = parseFloat(svalue);
            $("#p_left_shirt_allow").html((p_left_shirt / 2.54).toFixed(1) + " Inches");
            //for pant_left_outseam pant
            var p_left_pant = (parseFloat(pvalue));
            $("#p_left_pant_allow").html((p_left_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_pant / 2.54)).toFixed(1) + " Inches");

            //for p_right shirt
            if (chest-stomach < 7) {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantrightshirt = svalue;
            jvalue = value.split(";")[1];
            pantrightjacket = jvalue;
            vvalue = value.split(";")[2];
            pantrightvest = vvalue;
            pvalue = value.split(";")[3];
            pantrightpant = pvalue;
            var p_right_shirt = parseFloat(svalue);
            $("#p_right_shirt_allow").html((p_right_shirt / 2.54).toFixed(1) + " Inches");
            //for pant_right_outseam pant
            var p_right_pant = (parseFloat(pvalue));
            $("#p_right_pant_allow").html((p_right_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_pant / 2.54)).toFixed(1) + " Inches");

            /////for bottom shirt
            if (chest-stomach < 7) {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bottomshirt = svalue;
            jvalue = value.split(";")[1];
            bottomjacket = jvalue;
            vvalue = value.split(";")[2];
            bottomvest = vvalue;
            pvalue = value.split(";")[3];
            bottompant = pvalue;
            var bottom_shirt = parseFloat(svalue);
            $("#bottom_shirt_allow").html((bottom_shirt / 2.54).toFixed(1) + " Inches");
            // for bottom pant
            var bottom_pant = (parseFloat(pvalue));
            $("#bottom_pant_allow").html((bottom_pant / 2.54).toFixed(1) + " Inches");
            $("#final_pant_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_pant / 2.54)).toFixed(1) + " Inches");

            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
        else {
            ///cm from backend need to conversion into inches
            var chest = parseFloat(sizeData.chest);///cm conversion
            var stomach = parseFloat(sizeData.stomach);///cm conversion
            ////collar shirt
            if (chest-stomach < 7) {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            collarshirt = svalue;
            jvalue = value.split(";")[1];
            collarjacket = jvalue;
            vvalue = value.split(";")[2];
            collarvest = vvalue;
            pvalue = value.split(";")[3];
            collarpant = pvalue;
            //for collar shirt
            var collar_shirt = parseFloat(svalue);
            $("#collar_shirt_allow").html(collar_shirt+ " Cm");
            var val = (parseFloat(sizeData.collar) + parseFloat(collar_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_collar").html(val + " Cm");
            if(val<collar_min || val>collar_max){
                $("#final_shirt_collar").css("color","red");
            }
            // for collar jacket
            var collar_jacket = (parseFloat(jvalue));
            $("#collar_jacket_allow").html(collar_jacket+ " Cm");
            // for collar vest
            var collar_vest = (parseFloat(vvalue));
            $("#collar_vest_allow").html(collar_vest+ " Cm");

            //for chest shirt
            if (chest-stomach < 7) {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            chestshirt = svalue;
            jvalue = value.split(";")[1];
            chestjacket = jvalue;
            vvalue = value.split(";")[2];
            chestvest = vvalue;
            pvalue = value.split(";")[3];
            chestpant = pvalue;
            //for chest shirt
            var chest_shirt = parseFloat(svalue);
            $("#chest_shirt_allow").html(chest_shirt+ " Cm");
            var val = (parseFloat(sizeData.chest) + parseFloat(chest_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_chest").html(val+ " Cm");
            if(val<chest_min || val>chest_max){
                $("#final_shirt_chest").css("color","red");
            }
            // for chest jacket
            var chest_jacket = (parseFloat(jvalue));
            $("#chest_jacket_allow").html(chest_jacket+ " Cm");
            var val = (parseFloat(sizeData.chest) + parseFloat(chest_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_chest").html(val + " Cm");
            if(val<chest_min || val>chest_max){
                $("#final_jacket_chest").css("color","red");
            }
            //for chest vest
            var chest_vest = parseFloat(vvalue);
            $("#chest_vest_allow").html(chest_vest+ " Cm");
            var val = (parseFloat(sizeData.chest) + parseFloat(chest_vest)).toFixed(1);
            val = parseFloat(val);
            $("#final_vest_chest").html(val + " Cm");
            if(val<chest_min || val>chest_max){
                $("#final_vest_chest").css("color","red");
            }

            ////////for Stomach shirt
            if (chest-stomach < 7) {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            stomachshirt = svalue;
            jvalue = value.split(";")[1];
            stomachjacket = jvalue;
            vvalue = value.split(";")[2];
            stomachvest = vvalue;
            pvalue = value.split(";")[3];
            stomachpant = pvalue;
            // for stomach shirt
            var stomach_shirt = parseFloat(svalue);
            $("#stomach_shirt_allow").html(stomach_shirt+ " Cm");
            var val = (parseFloat(sizeData.stomach) + parseFloat(stomach_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_stomach").html(val + " Cm");
            if(val<stomach_min || val>stomach_max){
                $("#final_shirt_stomach").css("color","red");
            }
            // for stomach jacket
            var stomach_jacket = (parseFloat(jvalue));
            $("#stomach_jacket_allow").html(stomach_jacket+ " Cm");
            var val = (parseFloat(sizeData.stomach) + parseFloat(stomach_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_stomach").html(val + " Cm");
            if(val<stomach_min || val>stomach_max){
                $("#final_jacket_stomach").css("color","red");
            }
            // for stomach vest
            var stomach_vest = parseFloat(vvalue);
            $("#stomach_vest_allow").html(stomach_vest+ " Cm");
            var val = (parseFloat(sizeData.stomach) + parseFloat(stomach_vest)).toFixed(1);
            val = parseFloat(val);
            $("#final_vest_stomach").html(val + " Cm");
            if(val<stomach_min || val>stomach_max){
                $("#final_vest_stomach").css("color","red");
            }
            // for stomach pant
            var stomach_pant = (parseFloat(pvalue));
            $("#stomach_pant_allow").html(stomach_pant+ " Cm");

            //for seat shirt
            if (chest-stomach < 7) {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            seatshirt = svalue;
            jvalue = value.split(";")[1];
            seatjacket = jvalue;
            vvalue = value.split(";")[2];
            seatvest = vvalue;
            pvalue = value.split(";")[3];
            seatpant = pvalue;
            var seat_shirt = parseFloat(svalue);
            $("#seat_shirt_allow").html(seat_shirt+ " Cm");
            var val = (parseFloat(sizeData.seat) + parseFloat(seat_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_seat").html(val + " Cm");
            if(val<seat_min || val>seat_max){
                $("#final_shirt_seat").css("color","red");
            }
            // for seat jacket
            var seat_jacket = (parseFloat(jvalue));
            $("#seat_jacket_allow").html(seat_jacket+ " Cm");
            var val = (parseFloat(sizeData.seat) + parseFloat(seat_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_seat").html(val+ " Cm");
            if(val<seat_min || val>seat_max){
                $("#final_jacket_seat").css("color","red");
            }
            //for seat vest
            var seat_vest = parseFloat(vvalue);
            $("#seat_vest_allow").html(seat_vest+ " Cm");
            // for seat pant
            var seat_pant = (parseFloat(pvalue));
            $("#seat_pant_allow").html(seat_pant+ " Cm");
            var val = (parseFloat(sizeData.seat) + parseFloat(seat_pant)).toFixed(1) ;
            val = parseFloat(val);
            $("#final_pant_seat").html(val+ " Cm");
            if(val<seat_min || val>seat_max){
                $("#final_pant_seat").css("color","red");
            }
            ///////for bicep shirt
            if (chest-stomach < 7) {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bicepshirt = svalue;
            jvalue = value.split(";")[1];
            bicepjacket = jvalue;
            vvalue = value.split(";")[2];
            bicepvest = vvalue;
            pvalue = value.split(";")[3];
            biceppant = pvalue;
            var bicep_shirt = parseFloat(svalue);
            $("#bicep_shirt_allow").html(bicep_shirt+ " Cm");
            var val = (parseFloat(sizeData.bicep) + parseFloat(bicep_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_bicep").html(val + " Cm");
            if(val<bicep_min || val>bicep_max){
                $("#final_shirt_bicep").css("color","red");
            }
            //for bicep jacket
            var bicep_jacket = (parseFloat(jvalue));
            $("#bicep_jacket_allow").html(bicep_jacket+ " Cm");
            var val = (parseFloat(sizeData.bicep) + parseFloat(bicep_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_bicep").html(val + " Cm");
            if(val<bicep_min || val>bicep_max){
                $("#final_jacket_bicep").css("color","red");
            }
            //for bicep vest
            var bicep_vest = parseFloat(vvalue);
            $("#bicep_vest_allow").html(bicep_vest+ " Cm");

            //for back shoulder shirt
            if (chest-stomach < 7) {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backshouldershirt = svalue;
            jvalue = value.split(";")[1];
            backshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            backshouldervest = vvalue;
            pvalue = value.split(";")[3];
            backshoulderpant = pvalue;
            var back_shoulder_shirt = parseFloat(svalue);
            $("#back_shoulder_shirt_allow").html(back_shoulder_shirt+ " Cm");
            var temp_back_shoulder_shirt = parseFloat(sizeData.back_sholuder)+(parseFloat($("#back_shoulder_body").text().split(" ")[0]));
            var val = (parseFloat(temp_back_shoulder_shirt) + parseFloat(back_shoulder_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_back_shoulder").html(val + " Cm");
            if(val<back_shoulder_min || val>back_shoulder_max){
                $("#final_shirt_back_shoulder").css("color","red");
            }
            //for back shoulder jacket
            var back_shoulder_jacket = (parseFloat(jvalue));
            $("#back_shoulder_jacket_allow").html(back_shoulder_jacket+ " Cm");
            var temp_back_shoulder_jacket = parseFloat(sizeData.back_sholuder)+(parseFloat($("#back_shoulder_body").text().split(" ")[0]));
            var val = (parseFloat(temp_back_shoulder_jacket) + parseFloat(back_shoulder_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_back_shoulder").html(val + " Cm");
            if(val<back_shoulder_min || val>back_shoulder_max){
                $("#final_jacket_back_shoulder").css("color","red");
            }
            //for back shoulder vest
            var back_shoulder_vest = parseFloat(vvalue);
            $("#back_shoulder_vest_allow").html(back_shoulder_vest+ " Cm");

            ////////for front shoulder shirt
            if (chest-stomach < 7) {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontshouldershirt = svalue;
            jvalue = value.split(";")[1];
            frontshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            frontshouldervest = vvalue;
            pvalue = value.split(";")[3];
            frontshoulderpant = pvalue;
            var front_shoulder_shirt = parseFloat(svalue);
            $("#front_shoulder_shirt_allow").html(front_shoulder_shirt+ " Cm");
            var temp_front_shoulder_shirt = parseFloat(sizeData.front_shoulder)+(parseFloat($("#front_shoulder_body").text().split(" ")[0]));
            var val = (parseFloat(temp_front_shoulder_shirt) + parseFloat(front_shoulder_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_front_shoulder").html(val + " Cm");
            if(val<front_shoulder_min || val>front_shoulder_max){
                $("#final_shirt_front_shoulder").css("color","red");
            }
            //for front shoulder jacket
            var front_shoulder_jacket = (parseFloat(jvalue));
            $("#front_shoulder_jacket_allow").html(front_shoulder_jacket+ " Cm");
            var temp_front_shoulder_jacket = parseFloat(sizeData.front_shoulder)+(parseFloat($("#front_shoulder_body").text().split(" ")[0]));
            var val = (parseFloat(temp_front_shoulder_jacket) + parseFloat(front_shoulder_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_front_shoulder").html(val + " Cm");
            if(val<front_shoulder_min || val>front_shoulder_max){
                $("#final_jacket_front_shoulder").css("color","red");
            }
            // for front shoulder vest
            var front_shoulder_vest = parseFloat(vvalue);
            $("#front_shoulder_vest_allow").html(front_shoulder_vest+ " Cm");

            //for left_sleeve_length shirt
            if (chest-stomach < 7) {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeveleftshirt = svalue;
            jvalue = value.split(";")[1];
            sleeveleftjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeveleftvest = vvalue;
            pvalue = value.split(";")[3];
            sleeveleftpant = pvalue;
            var left_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_left_shirt_allow").html(left_sleeve_length_shirt+ " Cm");
            var val = (parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_sleeve_left").html(val + " Cm");
            if(val<sleeve_left_min || val>sleeve_left_max){
                $("#final_shirt_sleeve_left").css("color","red");
            }
            //for sleeve length left jacket
            var left_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_left_jacket_allow").html(left_sleeve_length_jacket+ " Cm");
            var val = (parseFloat(sizeData.sleeve_length_left) + parseFloat(left_sleeve_length_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_sleeve_left").html(val + " Cm");
            if(val<sleeve_left_min || val>sleeve_left_max){
                $("#final_jacket_sleeve_left").css("color","red");
            }
            //for sleeve length left vest
            var left_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_left_vest_allow").html(left_sleeve_length_vest+ " Cm");

            /////for right_sleeve_length shirt
            if (chest-stomach < 7) {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeverightshirt = svalue;
            jvalue = value.split(";")[1];
            sleeverightjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeverightvest = vvalue;
            pvalue = value.split(";")[3];
            sleeverightpant = pvalue;
            var right_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_right_shirt_allow").html(right_sleeve_length_shirt+ " Cm");
            var val = (parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_sleeve_right").html(val + " Cm");
            if(val<sleeve_right_min || val>sleeve_right_max){
                $("#final_shirt_sleeve_right").css("color","red");
            }
            //for sleeve length right jacket
            var right_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_right_jacket_allow").html(right_sleeve_length_jacket+ " Cm");
            var val = (parseFloat(sizeData.sleeve_length_right) + parseFloat(right_sleeve_length_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_sleeve_right").html(val + " Cm");
            if(val<sleeve_right_min || val>sleeve_right_max){
                $("#final_jacket_sleeve_right").css("color","red");
            }
            //for sleeve length right vest
            var right_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_right_vest_allow").html(right_sleeve_length_vest+ " Cm");

            //// for thigh shirt
            if (chest-stomach < 7) {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            thighshirt = svalue;
            jvalue = value.split(";")[1];
            thighjacket = jvalue;
            vvalue = value.split(";")[2];
            thighvest = vvalue;
            pvalue = value.split(";")[3];
            thighpant = pvalue;
            //for thigh shirt
            var thigh_shirt = parseFloat(svalue);
            $("#thigh_shirt_allow").html(thigh_shirt+ " Cm");
            //for thigh jacket
            var thigh_jacket = (parseFloat(jvalue));
            $("#thigh_jacket_allow").html(thigh_jacket+ " Cm");
            //for thigh vest
            var thigh_vest = parseFloat(vvalue);
            $("#thigh_vest_allow").html(thigh_vest+ " Cm");
            //for thigh pant
            var thigh_pant = (parseFloat(pvalue));
            $("#thigh_pant_allow").html(thigh_pant+ " Cm");
            $("#final_pant_thigh").html((parseFloat(sizeData.thigh) + parseFloat(thigh_pant)).toFixed(1) + " Cm");

            //for nape shirt
            if (chest-stomach < 7) {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            napetowaistshirt = svalue;
            jvalue = value.split(";")[1];
            napetowaistjacket = jvalue;
            vvalue = value.split(";")[2];
            napetowaistvest = vvalue;
            pvalue = value.split(";")[3];
            napetowaistpant = pvalue;
            //for nape to waist shirt
            var nape_to_waist_shirt = parseFloat(svalue);
            $("#nape_shirt_allow").html(nape_to_waist_shirt+ " Cm");
            //for nape to waist jacket
            var nape_to_waist_jacket = (parseFloat(jvalue));
            $("#nape_jacket_allow").html(nape_to_waist_jacket+ " Cm");
            //for nape to waist vest
            var nape_to_waist_vest = parseFloat(vvalue);
            $("#nape_vest_allow").html(nape_to_waist_vest+ " Cm");
            //for nape to waist pant
            var nape_to_waist_pant = (parseFloat(pvalue));
            $("#nape_pant_allow").html(nape_to_waist_pant+ " Cm");

            ////////for front_waist shirt
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistlengthshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistlengthvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistlengthpant = pvalue;
            //for front shoulder shirt
            var front_waist_shirt = parseFloat(svalue);
            $("#front_waist_shirt_allow").html(front_waist_shirt+ " Cm");
            //for front shoulder jacket
            var front_waist_jacket = (parseFloat(jvalue));
            $("#front_waist_jacket_allow").html(front_waist_jacket+ " Cm");
            //for front shoulder vest
            var front_waist_vest = parseFloat(vvalue);
            $("#front_waist_vest_allow").html(front_waist_vest+ " Cm");
            //for front shoulder pant
            var front_waist_pant = (parseFloat(pvalue));
            $("#front_waist_pant_allow").html(front_waist_pant+ " Cm");

            //for wrist shirt
            if (chest-stomach < 7) {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            wristshirt = svalue;
            jvalue = value.split(";")[1];
            wristjacket = jvalue;
            vvalue = value.split(";")[2];
            wristvest = vvalue;
            pvalue = value.split(";")[3];
            wristpant = pvalue;
            //for front shoulder shirt
            var wrist_shirt = parseFloat(svalue);
            $("#wrist_shirt_allow").html(wrist_shirt+ " Cm");
            $("#final_shirt_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_shirt)).toFixed(1) + " Cm");
            //for front shoulder jacket
            var wrist_jacket = (parseFloat(jvalue));
            $("#wrist_jacket_allow").html(wrist_jacket+ " Cm");
            $("#final_jacket_wrist").html((parseFloat(sizeData.wrist) + parseFloat(wrist_jacket)).toFixed(1) + " Cm");
            //for front shoulder vest
            var wrist_vest = parseFloat(vvalue);
            $("#wrist_vest_allow").html(wrist_vest+ " Cm");
            //for front shoulder pant
            var wrist_pant = (parseFloat(pvalue));
            $("#wrist_pant_allow").html(wrist_pant+ " Cm");

            ///////for waist shirt
            if (chest-stomach < 7) {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            waistshirt = svalue;
            jvalue = value.split(";")[1];
            waistjacket = jvalue;
            vvalue = value.split(";")[2];
            waistvest = vvalue;
            pvalue = value.split(";")[3];
            waistpant = pvalue;
            //for front shoulder shirt
            var waist_shirt = parseFloat(svalue);
            $("#waist_shirt_allow").html(waist_shirt+ " Cm");
            //for front shoulder jacket
            var waist_jacket = (parseFloat(jvalue));
            $("#waist_jacket_allow").html(waist_jacket+ " Cm");
            //for front shoulder vest
            var waist_vest = parseFloat(vvalue);
            $("#waist_vest_allow").html(waist_vest+ " Cm");
            //for front shoulder pant
            var waist_pant = (parseFloat(pvalue));
            $("#waist_pant_allow").html(waist_pant+ " Cm");
            $("#final_pant_waist").html((parseFloat(sizeData.waist) + parseFloat(waist_pant)).toFixed(1) + " Cm");

            //for calf shirt
            if (chest-stomach < 7) {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            calfshirt = svalue;
            jvalue = value.split(";")[1];
            calfjacket = jvalue;
            vvalue = value.split(";")[2];
            calfvest = vvalue;
            pvalue = value.split(";")[3];
            calfpant = pvalue;
            //for front shoulder shirt
            var calf_shirt = parseFloat(svalue);
            $("#calf_shirt_allow").html(calf_shirt+ " Cm");
            //for front shoulder pant
            var calf_pant = (parseFloat(pvalue));
            $("#calf_pant_allow").html(calf_pant+ " Cm");
            var val =(parseFloat(sizeData.calf) + parseFloat(calf_pant)).toFixed(1);
            val = parseFloat(val);
            $("#final_pant_calf").html(val + " Cm");
            if(val<calf_min || val>calf_max){
                $("#final_pant_calf").css("color","red");
            }

            ////////for b_waist_shirt
            if (chest-stomach < 7) {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            backwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            backwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            backwaistheightpant = pvalue;
            //for waist shirt
            var b_waist_shirt = parseFloat(svalue);
            $("#b_waist_shirt_allow").html(b_waist_shirt+ " Cm");
            //for waist jacket
            var b_waist_jacket = (parseFloat(jvalue));
            $("#b_waist_jacket_allow").html(b_waist_jacket+ " Cm");
            //for waist vest
            var b_waist_vest = parseFloat(vvalue);
            $("#b_waist_vest_allow").html(b_waist_vest+ " Cm");
            //for waist pant
            var b_waist_pant = (parseFloat(pvalue));
            $("#b_waist_pant_allow").html(b_waist_pant+ " Cm");
            $("#final_pant_b_waist").html((parseFloat(sizeData.back_waist_height) + parseFloat(b_waist_pant)).toFixed(1) + " Cm");

            //for f_waist_shirt
            if (chest-stomach < 7) {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistheightpant = pvalue;
            //for front shoulder shirt
            var f_waist_shirt = parseFloat(svalue);
            $("#f_waist_shirt_allow").html(f_waist_shirt+ " Cm");
            //for front shoulder jacket
            var f_waist_jacket = parseFloat(jvalue);
            $("#f_waist_jacket_allow").html(f_waist_jacket+ " Cm");
            //for front shoulder vest
            var f_waist_vest = parseFloat(vvalue);
            $("#f_waist_vest_allow").html(f_waist_vest+ " Cm");
            //for front shoulder pant
            var f_waist_pant = parseFloat(pvalue);
            $("#f_waist_pant_allow").html(f_waist_pant+ " Cm");
            $("#final_pant_f_waist").html((parseFloat(sizeData.front_waist_height) + parseFloat(f_waist_pant)).toFixed(1) + " Cm");

            /////for knee shirt
            if (chest-stomach < 7) {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            kneeshirt = svalue;
            jvalue = value.split(";")[1];
            kneejacket = jvalue;
            vvalue = value.split(";")[2];
            kneevest = vvalue;
            pvalue = value.split(";")[3];
            kneepant = pvalue;
            //for front shoulder shirt
            var knee_shirt = parseFloat(svalue);
            $("#knee_shirt_allow").html(knee_shirt+ " Cm");
            //for front shoulder pant
            var knee_pant = (parseFloat(pvalue));
            $("#knee_pant_allow").html(knee_pant+ " Cm");
            $("#final_pant_knee").html((parseFloat(sizeData.knee) + parseFloat(knee_pant)).toFixed(1) + " Cm");

            //for back_jacket_length shirt
            if (chest-stomach < 7) {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backjacketlengthshirt = svalue;
            jvalue = value.split(";")[1];
            backjacketlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            backjacketlengthvest = vvalue;
            pvalue = value.split(";")[3];
            backjacketlengthpant = pvalue;
            //for back_jacket_length shirt
            var jacket_shirt = parseFloat(svalue);
            $("#jacket_shirt_allow").html(jacket_shirt+ " Cm");
            var val = (parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_shirt)).toFixed(1);
            val = parseFloat(val);
            $("#final_shirt_jacket").html(val+ " Cm");
            if(val<back_jacket_length_min || val>back_jacket_length_max){
                $("#final_shirt_jacket").css("color","red");
            }
            //for back_jacket_length jacket
            var jacket_jacket = (parseFloat(jvalue));
            $("#jacket_jacket_allow").html(jacket_jacket+ " Cm");
            var val = (parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_jacket)).toFixed(1);
            val = parseFloat(val);
            $("#final_jacket_jacket").html(val + " Cm");
            if(val<back_jacket_length_min || val>back_jacket_length_max){
                $("#final_jacket_jacket").css("color","red");
            }
            //for back_jacket_length vest
            var jacket_vest = parseFloat(vvalue);
            $("#jacket_vest_allow").html(jacket_vest+ " Cm");
            var val = (parseFloat(sizeData.back_jacket_length) + parseFloat(jacket_vest)).toFixed(1);
            val = parseFloat(val);
            $("#final_vest_jacket").html(val + " Cm");
            if(val<back_jacket_length_min || val>back_jacket_length_max){
                $("#final_vest_jacket").css("color","red");
            }

            //for u_rise shirt
            if (chest-stomach < 7) {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            riseshirt = svalue;
            jvalue = value.split(";")[1];
            risejacket = jvalue;
            vvalue = value.split(";")[2];
            risevest = vvalue;
            pvalue = value.split(";")[3];
            risepant = pvalue;
            //for back_jacket_length shirt
            var rise_shirt = parseFloat(svalue);
            $("#rise_shirt_allow").html(rise_shirt+ " Cm");
            //for back_jacket_length jacket
            var rise_jacket = (parseFloat(jvalue));
            $("#rise_jacket_allow").html(rise_jacket+ " Cm");
            //for back_jacket_length vest
            var rise_vest = parseFloat(vvalue);
            $("#rise_vest_allow").html(rise_vest+ " Cm");
            //for back_jacket_length pant
            var rise_pant = (parseFloat(pvalue));
            $("#rise_pant_allow").html(rise_pant+ " Cm");
            $("#final_pant_rise").html((parseFloat(sizeData.u_rise) + parseFloat(rise_pant)).toFixed(1) + " Cm");


            ////////for pant_left_shirt
            if (chest-stomach < 7) {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantleftshirt = svalue;
            jvalue = value.split(";")[1];
            pantleftjacket = jvalue;
            vvalue = value.split(";")[2];
            pantleftvest = vvalue;
            pvalue = value.split(";")[3];
            pantleftpant = pvalue;
            //for back_jacket_length shirt
            var p_left_shirt = parseFloat(svalue);
            $("#p_left_shirt_allow").html(p_left_shirt+ " Cm");
            //for back_jacket_length pant
            var p_left_pant = (parseFloat(pvalue));
            $("#p_left_pant_allow").html(p_left_pant+ " Cm");
            $("#final_pant_p_left").html((parseFloat(sizeData.pant_left_outseam) + parseFloat(p_left_pant)).toFixed(1) + " Cm");

            //for p_right shirt
            if (chest-stomach < 7) {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantrightshirt = svalue;
            jvalue = value.split(";")[1];
            pantrightjacket = jvalue;
            vvalue = value.split(";")[2];
            pantrightvest = vvalue;
            pvalue = value.split(";")[3];
            pantrightpant = pvalue;
            //for back_jacket_length shirt
            var p_right_shirt = parseFloat(svalue);
            $("#p_right_shirt_allow").html(p_right_shirt+ " Cm");
            //for back_jacket_length pant
            var p_right_pant = (parseFloat(pvalue));
            $("#p_right_pant_allow").html(p_right_pant+ " Cm");
            $("#final_pant_p_right").html((parseFloat(sizeData.pant_right_outseam) + parseFloat(p_right_pant)).toFixed(1) + " Cm");

            /////for bottom shirt
            if (chest-stomach < 7) {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 +"@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bottomshirt = svalue;
            jvalue = value.split(";")[1];
            bottomjacket = jvalue;
            vvalue = value.split(";")[2];
            bottomvest = vvalue;
            pvalue = value.split(";")[3];
            bottompant = pvalue;
            //for bottom shirt
            var bottom_shirt = parseFloat(svalue);
            $("#bottom_shirt_allow").html(bottom_shirt+ " Cm");
            //for bottom pant
            var bottom_pant = (parseFloat(pvalue));
            $("#bottom_pant_allow").html(bottom_pant+ " Cm");
            $("#final_pant_bottom").html((parseFloat(sizeData.bottom) + parseFloat(bottom_pant)).toFixed(1) + " Cm");

            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
        getPantvaluepleat(panswer1);
    }
    function getResponse2(body_param, ques_answer) {
        var url = 'api/registerUser.php';
        var jvalue = 0;
        var svalue = 0;
        var vvalue = 0;
        var pvalue = 0;
        var data = {"type": "queryProcess2", "body_param": body_param, "ques_answer": ques_answer};
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            async: false,
            success: function (data) {
                jvalue = data.jvalue;
                svalue = data.svalue;
                vvalue = data.vvalue;
                pvalue = data.pvalue;
            }
        });
        return svalue + ";" + jvalue + ";" + vvalue + ";" + pvalue;
    }
    function getPantvaluepleat(pant_answer) {
        var url = 'api/registerUser.php';
        var result = 0;
        var data = {"type": "pantpleat", "pant_answer": pant_answer};
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            async: false,
            success: function (data) {
                result = data.pleatData;
                var oldpantseat = parseFloat($("#final_pant_seat").html().split(" ")[0]);
                var oldpantrise = parseFloat($("#final_pant_rise").html().split(" ")[0]);
                if($("#params").val() == "Inches") {
                    var val = parseFloat(oldpantseat+parseFloat((result.no_pleat)/2.54)).toFixed(1);
                    $("#final_pant_seat").html(val+" "+$("#params").val());
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#final_pant_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat+parseFloat((result.onepleatlesstha2cm)/2.54)).toFixed(1);
                    $("#final_pant1_seat").html(val+" "+$("#params").val());
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#final_pant1_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat+parseFloat((result.onpleatbetween2cmand25cm)/2.54)).toFixed(1);
                        $("#final_pant11_seat").html(val+" "+$("#params").val());
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#final_pant11_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat+parseFloat((result.twopleatlessthan25cm)/2.54)).toFixed(1);
                    $("#final_pant2_seat").html(val+" "+$("#params").val());
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#final_pant2_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat+parseFloat((result.threepleatlessthan25cm)/2.54)).toFixed(1);
                    $("#final_pant3_seat").html(val+" "+$("#params").val());
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#final_pant3_seat").css("color","red");
                    }
                    $("#final_pant_rise").html(parseFloat(oldpantrise+parseFloat((result.urise)/2.54)).toFixed(1)+" "+$("#params").val());
                    $("#final_pant1_rise").html(parseFloat(oldpantrise+parseFloat((result.urise)/2.54)).toFixed(1)+" "+$("#params").val());
                    $("#final_pant11_rise").html(parseFloat(oldpantrise+parseFloat((result.urise)/2.54)).toFixed(1)+" "+$("#params").val());
                    $("#final_pant2_rise").html(parseFloat(oldpantrise+parseFloat((result.urise)/2.54)).toFixed(1)+" "+$("#params").val());
                    $("#final_pant3_rise").html(parseFloat(oldpantrise+parseFloat((result.urise)/2.54)).toFixed(1)+" "+$("#params").val());
                }else{
                    var val = parseFloat(oldpantseat)+parseFloat(result.no_pleat);
                    $("#final_pant_seat").html(val+" "+$("#params").val());
                    if(val<seat_min || val>seat_max){
                        $("#final_pant_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat)+parseFloat(result.onepleatlesstha2cm);
                    $("#final_pant1_seat").html(val+" "+$("#params").val());
                    if(val<seat_min || val>seat_max){
                        $("#final_pant1_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat)+parseFloat(result.onpleatbetween2cmand25cm);
                    $("#final_pant11_seat").html(val+" "+$("#params").val());
                    if(val<seat_min || val>seat_max){
                        $("#final_pant11_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat)+parseFloat(result.twopleatlessthan25cm);
                    $("#final_pant2_seat").html(val+" "+$("#params").val());
                    if(val<seat_min || val>seat_max){
                        $("#final_pant2_seat").css("color","red");
                    }
                    var val = parseFloat(oldpantseat)+parseFloat(result.threepleatlessthan25cm);
                    $("#final_pant3_seat").html(val+" "+$("#params").val());
                    if(val<seat_min || val>seat_max){
                        $("#final_pant3_seat").css("color","red");
                    }
                    $("#final_pant_rise").html(parseFloat(oldpantrise)+parseFloat(result.urise)+" "+$("#params").val());
                    $("#final_pant1_rise").html(parseFloat(oldpantrise)+parseFloat(result.urise)+" "+$("#params").val());
                    $("#final_pant11_rise").html(parseFloat(oldpantrise)+parseFloat(result.urise)+" "+$("#params").val());
                    $("#final_pant2_rise").html(parseFloat(oldpantrise)+parseFloat(result.urise)+" "+$("#params").val());
                    $("#final_pant3_rise").html(parseFloat(oldpantrise)+parseFloat(result.urise)+" "+$("#params").val());
                }
            }
        });
    }
    function getData() {
        var user_id = $("#user_id").val();
        var params = $("#params").val();
        var url = "api/registerUser.php";
        $.post(url, {"type": "getStyleGenieData", "user_id": user_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                $(".loaderdiv").removeClass("loaderhidden");
                $(".loaderdiv").addClass("block");
                var sizeData = data.userData;
                if (sizeData.unit_type == "Inches") {
                    $("#params").prop('selectedIndex',0);
                    $("#collar").html((parseFloat(sizeData.collar)).toFixed(1) + " Inches");
                    $("#chest_front").html((parseFloat(sizeData.chest)).toFixed(1) + " Inches");
                    $("#stomach").html((parseFloat(sizeData.stomach)).toFixed(1) + " Inches");
                    $("#seat_front").html((parseFloat(sizeData.seat)).toFixed(1) + " Inches");
                    $("#bicep_front").html((parseFloat(sizeData.bicep)).toFixed(1) + " Inches");
                    $("#shoulder_back").html((parseFloat(sizeData.back_sholuder)).toFixed(1) + " Inches");
                    $("#shoulder_front").html((parseFloat(sizeData.front_shoulder)).toFixed(1) + " Inches");
                    $("#shoulder_sleeve_right").html((parseFloat(sizeData.sleeve_length_left)).toFixed(1) + " Inches");
                    $("#shoulder_sleeve_left").html((parseFloat(sizeData.sleeve_length_right)).toFixed(1) + " Inches");
                    $("#thigh").html((parseFloat(sizeData.thigh)).toFixed(1) + " Inches");
                    $("#napetowaist_back").html((parseFloat(sizeData.nape_to_waist)).toFixed(1) + " Inches");
                    $("#front_waist_length").html((parseFloat(sizeData.front_waist_length)).toFixed(1) + " Inches");
                    $("#wrist").html((parseFloat(sizeData.wrist)).toFixed(1) + " Inches");
                    $("#waist").html((parseFloat(sizeData.waist)).toFixed(1) + " Inches");
                    $("#calf").html((parseFloat(sizeData.calf)).toFixed(1) + " Inches");
                    $("#back_waist_height").html((parseFloat(sizeData.back_waist_height)).toFixed(1) + " Inches");
                    $("#front_waist_height").html((parseFloat(sizeData.front_waist_height)).toFixed(1) + " Inches");
                    $("#knee_right").html((parseFloat(sizeData.knee)).toFixed(1) + " Inches");
                    $("#back_jacket_length").html((parseFloat(sizeData.back_jacket_length)).toFixed(1) + " Inches");
                    $("#u_rise").html((parseFloat(sizeData.u_rise)).toFixed(1) + " Inches");
                    $("#outseam_l_pants").html((parseFloat(sizeData.pant_left_outseam)).toFixed(1) + " Inches");
                    $("#outseam_r_pants").html((parseFloat(sizeData.pant_right_outseam)).toFixed(1) + " Inches");
                    $("#bottom").html((parseFloat(sizeData.bottom)).toFixed(1) + " Inches");
                } else {
                    $("#params").prop('selectedIndex',1);
                    $("#collar").html(parseFloat(sizeData.collar).toFixed(1) + " Cm");
                    $("#chest_front").html(parseFloat(sizeData.chest).toFixed(1) + " Cm");
                    $("#stomach").html(parseFloat(sizeData.stomach).toFixed(1) + " Cm");
                    $("#seat_front").html(parseFloat(sizeData.seat).toFixed(1) + " Cm");
                    $("#bicep_front").html(parseFloat(sizeData.bicep).toFixed(1) + " Cm");
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder).toFixed(1) + " Cm");
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder).toFixed(1) + " Cm");
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left).toFixed(1) + " Cm");
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right).toFixed(1) + " Cm");
                    $("#thigh").html(parseFloat(sizeData.thigh).toFixed(1) + " Cm");
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist).toFixed(1) + " Cm");
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length).toFixed(1) + " Cm");
                    $("#wrist").html(parseFloat(sizeData.wrist).toFixed(1) + " Cm");
                    $("#waist").html(parseFloat(sizeData.waist).toFixed(1) + " Cm");
                    $("#calf").html(parseFloat(sizeData.calf).toFixed(1) + " Cm");
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height).toFixed(1) + " Cm");
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height).toFixed(1) + " Cm");
                    $("#knee_right").html(parseFloat(sizeData.knee).toFixed(1) + " Cm");
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length).toFixed(1) + " Cm");
                    $("#u_rise").html(parseFloat(sizeData.u_rise).toFixed(1) + " Cm");
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam).toFixed(1) + " Cm");
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam).toFixed(1) + " Cm");
                    $("#bottom").html(parseFloat(sizeData.bottom).toFixed(1) + " Cm");
                }
                var shoulder_slope = sizeData.shoulder_slope;
                var shoulder_slopeArray = shoulder_slope.split(",");
                var url = "api/registerUser.php";
                if (shoulder_slopeArray[0] == shoulder_slopeArray[1]) {
                    $.post(url,{"type":"getBodyStyle","left_side":shoulder_slopeArray[0],
                        "right_side":shoulder_slopeArray[1],"combination":"both"},function(data){
                        var Status = data.Status;
                        if(Status == "Success"){
                            if($("#params").val() == "Inches"){
                                frontshoulderbody = data.front
                                backshoulderbody = data.back
                                $("#back_shoulder_body").html((data.back/2.54).toFixed(1)+" Inches");
                                $("#front_shoulder_body").html((data.front/2.54).toFixed(1)+" Inches");
                            }else{
                                $("#back_shoulder_body").html(data.back+" Cm");
                                $("#front_shoulder_body").html(data.front+" Cm");
                                frontshoulderbody = data.front
                                backshoulderbody = data.back
                            }

                            writeminmax(sizeData);
                        }
                    });
                } else {
                    $.post(url,{"type":"getBodyStyle","left_side":shoulder_slopeArray[0],
                        "right_side":shoulder_slopeArray[1],"combination":"single"},function(data){
                        var Status = data.Status;
                        if(Status == "Success"){
                            if($("#params").val() == "Inches"){
                                frontshoulderbody = data.front
                                backshoulderbody = data.back
                                $("#back_shoulder_body").html((data.back/2.54).toFixed(1)+" Inches");
                                $("#front_shoulder_body").html((data.front/2.54).toFixed(1)+" Inches");
                            }else{
                                $("#back_shoulder_body").html(data.back+" Cm");
                                $("#front_shoulder_body").html(data.front+" Cm");
                                frontshoulderbody = data.front
                                backshoulderbody = data.back
                            }
                            writeminmax(sizeData);
                        }
                    });
                }
            }
            else {
                showMessage(data.Message, 'red');
            }
        }).fail(function () {
            showMessage("Server Error !!! Please Try After Some Time....", 'red');
        });
    }
    function save() {
        $(".modal-header").css({"display": "block"});
        $(".modal-body").css({"display": "block"});
        $(".modal-footer").css({"display": "block"});
        $(".modal-title").html("<span>Enter Measurement Details</span>");
        $(".modal-body").html("<div class='row'><p class='err'></p><div class='col-md-6 form-group'><label>Enter Measurement Name</label>" +
            "<input type='text' placeholder='Measurement Name' class='form-control' id='meas_name'/></div></div>");
        $(".modal-footer").html("<input type='button' value='Cancel' class='btn btn-default' data-dismiss='modal'/>" +
            "<input type='button' value='Save Now' class='btn btn-primary' onclick='confirmSave()' />");
        $("#modal").modal("show");
    }
    function confirmSave() {
        var meas_name = $("#meas_name").val();
        if (meas_name == "") {
            $(".err").html("Please Enter Measurement Name First");
            return false;
        }
        var user_id = $("#user_id").val();
        if (user_id == "") {
            $(".err").html("Session Expired Please Login Again....");
            return false;
        }
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        var shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        var shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var vest_chest = $("#final_vest_chest").text().split(" ")[0];
        var shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        var shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var jacket_seat = $("#final_jacket_seat").text().split(" ")[0];
        var pant_seat = $("#final_pant_seat").text().split(" ")[0];
        var pant1_seat = $("#final_pant1_seat").text().split(" ")[0];
        var pant11_seat = $("#final_pant11_seat").text().split(" ")[0];
        var pant2_seat = $("#final_pant2_seat").text().split(" ")[0];
        var pant3_seat = $("#final_pant3_seat").text().split(" ")[0];
        var shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        var shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        var shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        var shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        var shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        var pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        var shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        var jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        var pant_waist = $("#final_pant_waist").text().split(" ")[0];
        var pant_calf = $("#final_pant_calf").text().split(" ")[0];
        var pant_back_waist_height = $("#final_pant_b_waist").text().split(" ")[0];
        var pant_front_waist_height = $("#final_pant_f_waist").text().split(" ")[0];
        var pant_knee = $("#final_pant_knee").text().split(" ")[0];
        var shirt_back_jacket_length = $("#final_shirt_jacket").text().split(" ")[0];
        var jacket_back_jacket_length = $("#final_jacket_jacket").text().split(" ")[0];
        var vest_back_jacket_length = $("#final_vest_jacket").text().split(" ")[0];
        var pant_u_rise = $("#final_pant_rise").text().split(" ")[0];
        var pant_pant_left_outseam = $("#final_pant_p_left").text().split(" ")[0];
        var pant_pant_right_outseam = $("#final_pant_p_right").text().split(" ")[0];
        var pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        var url = 'api/registerUser.php';
        $.post(url,
            {
                'type': 'storeFinalMeasurement',
                'shirt_collar': shirt_collar,
                'shirt_chest': shirt_chest,
                'jacket_chest': jacket_chest,
                'vest_chest': vest_chest,
                'shirt_stomach': shirt_stomach,
                'jacket_stomach': jacket_stomach,
                'vest_stomach': vest_stomach,
                'shirt_seat': shirt_seat,
                'jacket_seat': jacket_seat,
                'pant_seat': pant_seat,
                'pant1_seat': pant1_seat,
                'pant11_seat': pant11_seat,
                'pant2_seat': pant2_seat,
                'pant3_seat': pant3_seat,
                'shirt_bicep': shirt_bicep,
                'jacket_bicep': jacket_bicep,
                'shirt_back_shoulder': shirt_back_shoulder,
                'jacket_back_shoulder': jacket_back_shoulder,
                'shirt_front_shoulder': shirt_front_shoulder,
                'jacket_front_shoulder': jacket_front_shoulder,
                'shirt_sleeve_left': shirt_sleeve_left,
                'jacket_sleeve_left': jacket_sleeve_left,
                'shirt_sleeve_right': shirt_sleeve_right,
                'jacket_sleeve_right': jacket_sleeve_right,
                'pant_thigh': pant_thigh,
                'shirt_wrist': shirt_wrist,
                'jacket_wrist': jacket_wrist,
                'pant_waist': pant_waist,
                'pant_calf': pant_calf,
                'pant_back_waist_height': pant_back_waist_height,
                'pant_front_waist_height': pant_front_waist_height,
                'pant_knee': pant_knee,
                'shirt_back_jacket_length': shirt_back_jacket_length,
                'jacket_back_jacket_length': jacket_back_jacket_length,
                'vest_back_jacket_length': vest_back_jacket_length,
                'pant_u_rise': pant_u_rise,
                'pant_pant_left_outseam': pant_pant_left_outseam,
                'pant_pant_right_outseam': pant_pant_right_outseam,
                'pant_bottom': pant_bottom,
                'meas_name': meas_name,
                'user_id': user_id,
                'unit_type': unit_type
            },
            function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    $("#modal").modal("hide");
                    window.location = "index.php";
                } else {
                    $(".err").html(data.Message);
                    return false;
                }
            }).fail(function () {
            $(".err").html("Server Error !!! Please Try After Some Time...");
            return false;
        });
    }
    function convertData() {
        showminmaxrange();
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        //old data of collar
        var collar = $("#collar").text().split(" ")[0];
        var collar_shirt_allow = $("#collar_shirt_allow").text().split(" ")[0];
        var final_shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        //old data of chest
        var chest_front = $("#chest_front").text().split(" ")[0];
        var chest_shirt_allow = $("#chest_shirt_allow").text().split(" ")[0];
        var chest_jacket_allow = $("#chest_jacket_allow").text().split(" ")[0];
        var chest_vest_allow = $("#chest_vest_allow").text().split(" ")[0];
        var final_shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var final_jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var final_vest_chest = $("#final_vest_chest").text().split(" ")[0];
        //old data of stomach
        var stomach = $("#stomach").text().split(" ")[0];
        var stomach_shirt_allow = $("#stomach_shirt_allow").text().split(" ")[0];
        var stomach_jacket_allow = $("#stomach_jacket_allow").text().split(" ")[0];
        var stomach_vest_allow = $("#stomach_vest_allow").text().split(" ")[0];
        var final_shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var final_jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var final_vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        //old data of seat
        var seat_front = $("#seat_front").text().split(" ")[0];
        var seat_shirt_allow = $("#seat_shirt_allow").text().split(" ")[0];
        var seat_jacket_allow = $("#seat_jacket_allow").text().split(" ")[0];
        var seat_pant_allow = $("#seat_pant_allow").text().split(" ")[0];
        var final_shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var final_jacket_seat = $("#final_jacket_seat").text().split(" ")[0];
        var final_pant_seat = $("#final_pant_seat").text().split(" ")[0];
        var final_pant1_seat = $("#final_pant1_seat").text().split(" ")[0];
        var final_pant11_seat = $("#final_pant11_seat").text().split(" ")[0];
        var final_pant2_seat = $("#final_pant2_seat").text().split(" ")[0];
        var final_pant3_seat = $("#final_pant3_seat").text().split(" ")[0];
        //old data of bicep
        var bicep_front = $("#bicep_front").text().split(" ")[0];
        var bicep_shirt_allow = $("#bicep_shirt_allow").text().split(" ")[0];
        var bicep_jacket_allow = $("#bicep_jacket_allow").text().split(" ")[0];
        var final_shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var final_jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        //old data of shoulder_back
        var shoulder_back = $("#shoulder_back").text().split(" ")[0];
        var back_shoulder_body = $("#back_shoulder_body").text().split(" ")[0];
        var back_shoulder_shirt_allow = $("#back_shoulder_shirt_allow").text().split(" ")[0];
        var back_shoulder_jacket_allow = $("#back_shoulder_jacket_allow").text().split(" ")[0];
        var final_shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var final_jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        //old data of shoulder_front
        var shoulder_front = $("#shoulder_front").text().split(" ")[0];
        var front_shoulder_body = $("#front_shoulder_body").text().split(" ")[0];
        var front_shoulder_shirt_allow = $("#front_shoulder_shirt_allow").text().split(" ")[0];
        var front_shoulder_jacket_allow = $("#front_shoulder_jacket_allow").text().split(" ")[0];
        var final_shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var final_jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        //old data of shoulder_sleeve_right
        var shoulder_sleeve_right = $("#shoulder_sleeve_right").text().split(" ")[0];
        var sleeve_right_shirt_allow = $("#sleeve_right_shirt_allow").text().split(" ")[0];
        var sleeve_right_jacket_allow = $("#sleeve_right_jacket_allow").text().split(" ")[0];
        var final_shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var final_jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        //old data of shoulder_sleeve_left
        var shoulder_sleeve_left = $("#shoulder_sleeve_left").text().split(" ")[0];
        var sleeve_left_shirt_allow = $("#sleeve_left_shirt_allow").text().split(" ")[0];
        var sleeve_left_jacket_allow = $("#sleeve_left_jacket_allow").text().split(" ")[0];
        var final_shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var final_jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        //old data of thigh
        var thigh = $("#thigh").text().split(" ")[0];
        var thigh_pant_allow = $("#thigh_pant_allow").text().split(" ")[0];
        var final_pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        //old data of napetowaist_back
        var napetowaist_back = $("#napetowaist_back").text().split(" ")[0];
        //old data of front_waist_length
        var front_waist_length = $("#front_waist_length").text().split(" ")[0];
        //old data of wrist
        var wrist = $("#wrist").text().split(" ")[0];
        var wrist_shirt_allow = $("#wrist_shirt_allow").text().split(" ")[0];
        var wrist_jacket_allow = $("#wrist_jacket_allow").text().split(" ")[0];
        var final_shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        var final_jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        //old data of waist
        var waist = $("#waist").text().split(" ")[0];
        var waist_pant_allow = $("#waist_pant_allow").text().split(" ")[0];
        var final_pant_waist = $("#final_pant_waist").text().split(" ")[0];
        //old data of calf
        var calf = $("#calf").text().split(" ")[0];
        var calf_pant_allow = $("#calf_pant_allow").text().split(" ")[0];
        var final_pant_calf = $("#final_pant_calf").text().split(" ")[0];
        //old data of back_waist_height
        var back_waist_height = $("#back_waist_height").text().split(" ")[0];
        var b_waist_pant_allow = $("#b_waist_pant_allow").text().split(" ")[0];
        var final_pant_b_waist = $("#final_pant_b_waist").text().split(" ")[0];
        //old data of front_waist_height
        var front_waist_height = $("#front_waist_height").text().split(" ")[0];
        var f_waist_pant_allow = $("#f_waist_pant_allow").text().split(" ")[0];
        var final_pant_f_waist = $("#final_pant_f_waist").text().split(" ")[0];
        //old data of knee_right
        var knee_right = $("#knee_right").text().split(" ")[0];
        var knee_pant_allow = $("#knee_pant_allow").text().split(" ")[0];
        var final_pant_knee = $("#final_pant_knee").text().split(" ")[0];
        //old data of back_jacket_length
        var back_jacket_length = $("#back_jacket_length").text().split(" ")[0];
        var jacket_shirt_allow = $("#jacket_shirt_allow").text().split(" ")[0];
        var jacket_jacket_allow = $("#jacket_jacket_allow").text().split(" ")[0];
        var jacket_vest_allow = $("#jacket_vest_allow").text().split(" ")[0];
        var final_shirt_jacket = $("#final_shirt_jacket").text().split(" ")[0];
        var final_jacket_jacket = $("#final_jacket_jacket").text().split(" ")[0];
        var final_vest_jacket = $("#final_vest_jacket").text().split(" ")[0];
        //old data of u_rise
        var u_rise = $("#u_rise").text().split(" ")[0];
        var rise_pant_allow = $("#rise_pant_allow").text().split(" ")[0];
        var final_pant_rise = $("#final_pant_rise").text().split(" ")[0];
        var final_pant1_rise = $("#final_pant1_rise").text().split(" ")[0];
        var final_pant11_rise = $("#final_pant11_rise").text().split(" ")[0];
        var final_pant2_rise = $("#final_pant2_rise").text().split(" ")[0];
        var final_pant3_rise = $("#final_pant3_rise").text().split(" ")[0];
        //old data of outseam_l_pants
        var outseam_l_pants = $("#outseam_l_pants").text().split(" ")[0];
        var p_left_pant_allow = $("#p_left_pant_allow").text().split(" ")[0];
        var final_pant_p_left = $("#final_pant_p_left").text().split(" ")[0];
        //old data of outseam_r_pants
        var outseam_r_pants = $("#outseam_r_pants").text().split(" ")[0];
        var p_right_pant_allow = $("#p_right_pant_allow").text().split(" ")[0];
        var final_pant_p_right = $("#final_pant_p_right").text().split(" ")[0];
        //old data of bottom
        var bottom = $("#bottom").text().split(" ")[0];
        var bottom_pant_allow = $("#bottom_pant_allow").text().split(" ")[0];
        var final_pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        if (unit_type == "Inches") {
            //show in centimeter means conversion required from inches to centimeter
            //for collar
            $("#collar").text(parseFloat(collar * 2.54).toFixed(1) + " Cm");
            $("#collar_shirt_allow").text(collarshirt+ " Cm");
            $("#final_shirt_collar").text(parseFloat(final_shirt_collar * 2.54).toFixed(1) + " Cm");
            //for chest_front
            $("#chest_front").text(parseFloat(chest_front * 2.54).toFixed(1) + " Cm");
            $("#chest_shirt_allow").text(chestshirt+ " Cm");
            $("#chest_jacket_allow").text(chestjacket+ " Cm");
            $("#chest_vest_allow").text(chestvest+ " Cm");
            $("#final_shirt_chest").text(parseFloat(final_shirt_chest * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_chest").text(parseFloat(final_jacket_chest * 2.54).toFixed(1) + " Cm");
            $("#final_vest_chest").text(parseFloat(final_vest_chest * 2.54).toFixed(1) + " Cm");
            //for stomach
            $("#stomach").text(parseFloat(stomach * 2.54).toFixed(1) + " Cm");
            $("#stomach_shirt_allow").text(stomachshirt+ " Cm");
            $("#stomach_jacket_allow").text(stomachjacket+ " Cm");
            $("#stomach_vest_allow").text(stomachvest+ " Cm");
            $("#final_shirt_stomach").text(parseFloat(final_shirt_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_stomach").text(parseFloat(final_jacket_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_vest_stomach").text(parseFloat(final_vest_stomach * 2.54).toFixed(1) + " Cm");
            //for seat_front
            $("#seat_front").text(parseFloat(seat_front * 2.54).toFixed(1) + " Cm");
            $("#seat_shirt_allow").text(seatshirt+ " Cm");
            $("#seat_jacket_allow").text(seatjacket+ " Cm");
            $("#seat_pant_allow").text(seatpant+ " Cm");
            $("#final_shirt_seat").text(parseFloat(final_shirt_seat * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_seat").text(parseFloat(final_jacket_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant_seat").text(parseFloat(final_pant_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant1_seat").text(parseFloat(final_pant1_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant11_seat").text(parseFloat(final_pant11_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant2_seat").text(parseFloat(final_pant2_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant3_seat").text(parseFloat(final_pant3_seat * 2.54).toFixed(1) + " Cm");
            //for bicep_front
            $("#bicep_front").text(parseFloat(bicep_front * 2.54).toFixed(1) + " Cm");
            $("#bicep_shirt_allow").text(bicepshirt+ " Cm");
            $("#bicep_jacket_allow").text(bicepjacket+ " Cm");
            $("#final_shirt_bicep").text(parseFloat(final_shirt_bicep * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_bicep").text(parseFloat(final_jacket_bicep * 2.54).toFixed(1) + " Cm");
            //for shoulder_back
            $("#shoulder_back").text(parseFloat(shoulder_back * 2.54).toFixed(1) + " Cm");
            $("#back_shoulder_body").text(backshoulderbody+ " Cm");
            $("#back_shoulder_shirt_allow").text(backshouldershirt+ " Cm");
            $("#back_shoulder_jacket_allow").text(backshoulderjacket+ " Cm");
            $("#final_shirt_back_shoulder").text(parseFloat(final_shirt_back_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_back_shoulder").text(parseFloat(final_jacket_back_shoulder * 2.54).toFixed(1) + " Cm");
            //for shoulder_front
            $("#shoulder_front").text(parseFloat(shoulder_front * 2.54).toFixed(1) + " Cm");
            $("#front_shoulder_body").text(frontshoulderbody+ " Cm");
            $("#front_shoulder_shirt_allow").text(frontshouldershirt+ " Cm");
            $("#front_shoulder_jacket_allow").text(frontshoulderjacket+ " Cm");
            $("#final_shirt_front_shoulder").text(parseFloat(final_shirt_front_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_front_shoulder").text(parseFloat(final_jacket_front_shoulder * 2.54).toFixed(1) + " Cm");
            //for shoulder_sleeve_right
            $("#shoulder_sleeve_right").text(parseFloat(shoulder_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#sleeve_right_shirt_allow").text(sleeverightshirt+ " Cm");
            $("#sleeve_right_jacket_allow").text(sleeverightjacket+ " Cm");
            $("#final_shirt_sleeve_right").text(parseFloat(final_shirt_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_sleeve_right").text(parseFloat(final_jacket_sleeve_right * 2.54).toFixed(1) + " Cm");
            //for shoulder_sleeve_left
            $("#shoulder_sleeve_left").text(parseFloat(shoulder_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#sleeve_left_shirt_allow").text(sleeveleftshirt+ " Cm");
            $("#sleeve_left_jacket_allow").text(sleeveleftjacket+ " Cm");
            $("#final_shirt_sleeve_left").text(parseFloat(final_shirt_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_sleeve_left").text(parseFloat(final_jacket_sleeve_left * 2.54).toFixed(1) + " Cm");
            //for thigh
            $("#thigh").text(parseFloat(thigh * 2.54).toFixed(1) + " Cm");
            $("#thigh_pant_allow").text(thighpant+ " Cm");
            $("#final_pant_thigh").text(parseFloat(final_pant_thigh * 2.54).toFixed(1) + " Cm");
            //for napetowaist_back
            $("#napetowaist_back").text(parseFloat(napetowaist_back * 2.54).toFixed(1) + " Cm");
            //for front_waist_length
            $("#front_waist_length").text(parseFloat(front_waist_length * 2.54).toFixed(1) + " Cm");
            //for wrist
            $("#wrist").text(parseFloat(wrist * 2.54).toFixed(1) + " Cm");
            $("#wrist_shirt_allow").text(wristshirt+ " Cm");
            $("#wrist_jacket_allow").text(wristjacket+ " Cm");
            $("#final_shirt_wrist").text(parseFloat(final_shirt_wrist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_wrist").text(parseFloat(final_jacket_wrist * 2.54).toFixed(1) + " Cm");
            //for waist
            $("#waist").text(parseFloat(waist * 2.54).toFixed(1) + " Cm");
            $("#waist_pant_allow").text(waistpant+ " Cm");
            $("#final_pant_waist").text(parseFloat(final_pant_waist * 2.54).toFixed(1) + " Cm");
            //for calf
            $("#calf").text(parseFloat(calf * 2.54).toFixed(1) + " Cm");
            $("#calf_pant_allow").text(calfpant+ " Cm");
            $("#final_pant_calf").text(parseFloat(final_pant_calf * 2.54).toFixed(1) + " Cm");
            //for back_waist_height
            $("#back_waist_height").text(parseFloat(back_waist_height * 2.54).toFixed(1) + " Cm");
            $("#b_waist_pant_allow").text(backwaistheightpant+ " Cm");
            $("#final_pant_b_waist").text(parseFloat(final_pant_b_waist * 2.54).toFixed(1) + " Cm");
            //for front_waist_height
            $("#front_waist_height").text(parseFloat(front_waist_height * 2.54).toFixed(1) + " Cm");
            $("#f_waist_pant_allow").text(frontwaistheightpant+ " Cm");
            $("#final_pant_f_waist").text(parseFloat(final_pant_f_waist * 2.54).toFixed(1) + " Cm");
            //for knee_right
            $("#knee_right").text(parseFloat(knee_right * 2.54).toFixed(1) + " Cm");
            $("#knee_pant_allow").text(kneepant+ " Cm");
            $("#final_pant_knee").text(parseFloat(final_pant_knee * 2.54).toFixed(1) + " Cm");
            //for back_jacket_length
            $("#back_jacket_length").text(parseFloat(back_jacket_length * 2.54).toFixed(1) + " Cm");
            $("#jacket_shirt_allow").text(backjacketlengthshirt+ " Cm");
            $("#jacket_jacket_allow").text(backjacketlengthjacket+ " Cm");
            $("#jacket_vest_allow").text(backjacketlengthvest+ " Cm");
            $("#final_shirt_jacket").text(parseFloat(final_shirt_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_jacket").text(parseFloat(final_jacket_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_vest_jacket").text(parseFloat(final_vest_jacket * 2.54).toFixed(1) + " Cm");
            //for u_rise
            $("#u_rise").text(parseFloat(u_rise * 2.54).toFixed(1) + " Cm");
            $("#rise_pant_allow").text(risepant+ " Cm");
            $("#final_pant_rise").text(parseFloat(final_pant_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant1_rise").text(parseFloat(final_pant1_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant11_rise").text(parseFloat(final_pant11_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant2_rise").text(parseFloat(final_pant2_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant3_rise").text(parseFloat(final_pant3_rise * 2.54).toFixed(1) + " Cm");
            //for outseam_l_pants
            $("#outseam_l_pants").text(parseFloat(outseam_l_pants * 2.54).toFixed(1) + " Cm");
            $("#p_left_pant_allow").text(pantleftpant+ " Cm");
            $("#final_pant_p_left").text(parseFloat(final_pant_p_left * 2.54).toFixed(1) + " Cm");
            //for outseam_r_pants
            $("#outseam_r_pants").text(parseFloat(outseam_r_pants * 2.54).toFixed(1) + " Cm");
            $("#p_right_pant_allow").text(pantrightpant+ " Cm");
            $("#final_pant_p_right").text(parseFloat(final_pant_p_right * 2.54).toFixed(1) + " Cm");
            //for bottom
            $("#bottom").text(parseFloat(bottom * 2.54).toFixed(1) + " Cm");
            $("#bottom_pant_allow").text(bottompant+ " Cm");
            $("#final_pant_bottom").text(parseFloat(final_pant_bottom * 2.54).toFixed(1) + " Cm");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        } else {
            //show in Inches means conversion required from centimeter to Inches
            //for collar
            $("#collar").text(parseFloat(collar / 2.54).toFixed(1) + " Inches");
            $("#collar_shirt_allow").text(parseFloat(collar_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_collar").text(parseFloat(final_shirt_collar / 2.54).toFixed(1) + " Inches");
            //for chest_front
            $("#chest_front").text(parseFloat(chest_front / 2.54).toFixed(1) + " Inches");
            $("#chest_shirt_allow").text(parseFloat(chest_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#chest_jacket_allow").text(parseFloat(chest_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#chest_vest_allow").text(parseFloat(chest_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_chest").text(parseFloat(final_shirt_chest / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_chest").text(parseFloat(final_jacket_chest / 2.54).toFixed(1) + " Inches");
            $("#final_vest_chest").text(parseFloat(final_vest_chest / 2.54).toFixed(1) + " Inches");
            //for stomach
            $("#stomach").text(parseFloat(stomach / 2.54).toFixed(1) + " Inches");
            $("#stomach_shirt_allow").text(parseFloat(stomach_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#stomach_jacket_allow").text(parseFloat(stomach_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#stomach_vest_allow").text(parseFloat(stomach_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_stomach").text(parseFloat(final_shirt_stomach / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_stomach").text(parseFloat(final_jacket_stomach / 2.54).toFixed(1) + " Inches");
            $("#final_vest_stomach").text(parseFloat(final_vest_stomach / 2.54).toFixed(1) + " Inches");
            //for seat_front
            $("#seat_front").text(parseFloat(seat_front / 2.54).toFixed(1) + " Inches");
            $("#seat_shirt_allow").text(parseFloat(seat_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#seat_jacket_allow").text(parseFloat(seat_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#seat_pant_allow").text(parseFloat(seat_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_seat").text(parseFloat(final_shirt_seat / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_seat").text(parseFloat(final_jacket_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant_seat").text(parseFloat(final_pant_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant1_seat").text(parseFloat(final_pant1_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant11_seat").text(parseFloat(final_pant11_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant2_seat").text(parseFloat(final_pant2_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant3_seat").text(parseFloat(final_pant3_seat / 2.54).toFixed(1) + " Inches");
            //for bicep_front
            $("#bicep_front").text(parseFloat(bicep_front / 2.54).toFixed(1) + " Inches");
            $("#bicep_shirt_allow").text(parseFloat(bicep_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#bicep_jacket_allow").text(parseFloat(bicep_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_bicep").text(parseFloat(final_shirt_bicep / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_bicep").text(parseFloat(final_jacket_bicep / 2.54).toFixed(1) + " Inches");
            //for shoulder_back
            $("#shoulder_back").text(parseFloat(shoulder_back / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_body").text(parseFloat(back_shoulder_body / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_shirt_allow").text(parseFloat(back_shoulder_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_jacket_allow").text(parseFloat(back_shoulder_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_back_shoulder").text(parseFloat(final_shirt_back_shoulder / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_back_shoulder").text(parseFloat(final_jacket_back_shoulder / 2.54).toFixed(1) + " Inches");
            //for shoulder_front
            $("#shoulder_front").text(parseFloat(shoulder_front / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_body").text(parseFloat(front_shoulder_body / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_shirt_allow").text(parseFloat(front_shoulder_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_jacket_allow").text(parseFloat(front_shoulder_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_front_shoulder").text(parseFloat(final_shirt_front_shoulder / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_front_shoulder").text(parseFloat(final_jacket_front_shoulder / 2.54).toFixed(1) + " Inches");
            //for shoulder_sleeve_right
            $("#shoulder_sleeve_right").text(parseFloat(shoulder_sleeve_right / 2.54).toFixed(1) + " Inches");
            $("#sleeve_right_shirt_allow").text(parseFloat(sleeve_right_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_right_jacket_allow").text(parseFloat(sleeve_right_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_sleeve_right").text(parseFloat(final_shirt_sleeve_right / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_sleeve_right").text(parseFloat(final_jacket_sleeve_right / 2.54).toFixed(1) + " Inches");
            //for shoulder_sleeve_left
            $("#shoulder_sleeve_left").text(parseFloat(shoulder_sleeve_left / 2.54).toFixed(1) + " Inches");
            $("#sleeve_left_shirt_allow").text(parseFloat(sleeve_left_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_left_jacket_allow").text(parseFloat(sleeve_left_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_sleeve_left").text(parseFloat(final_shirt_sleeve_left / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_sleeve_left").text(parseFloat(final_jacket_sleeve_left / 2.54).toFixed(1) + " Inches");
            //for thigh
            $("#thigh").text(parseFloat(thigh / 2.54).toFixed(1) + " Inches");
            $("#thigh_pant_allow").text(parseFloat(thigh_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_thigh").text(parseFloat(final_pant_thigh / 2.54).toFixed(1) + " Inches");
            //for napetowaist_back
            $("#napetowaist_back").text(parseFloat(napetowaist_back / 2.54).toFixed(1) + " Inches");
            //for front_waist_length
            $("#front_waist_length").text(parseFloat(front_waist_length / 2.54).toFixed(1) + " Inches");
            //for wrist
            $("#wrist").text(parseFloat(wrist / 2.54).toFixed(1) + " Inches");
            $("#wrist_shirt_allow").text(parseFloat(wrist_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#wrist_jacket_allow").text(parseFloat(wrist_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_wrist").text(parseFloat(final_shirt_wrist / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_wrist").text(parseFloat(final_jacket_wrist / 2.54).toFixed(1) + " Inches");
            //for wrist
            $("#waist").text(parseFloat(waist / 2.54).toFixed(1) + " Inches");
            $("#waist_pant_allow").text(parseFloat(waist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_waist").text(parseFloat(final_pant_waist / 2.54).toFixed(1) + " Inches");
            //for calf
            $("#calf").text(parseFloat(calf / 2.54).toFixed(1) + " Inches");
            $("#calf_pant_allow").text(parseFloat(calf_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_calf").text(parseFloat(final_pant_calf / 2.54).toFixed(1) + " Inches");
            //for back_waist_height
            $("#back_waist_height").text(parseFloat(back_waist_height / 2.54).toFixed(1) + " Inches");
            $("#b_waist_pant_allow").text(parseFloat(b_waist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_b_waist").text(parseFloat(final_pant_b_waist / 2.54).toFixed(1) + " Inches");
            //for front_waist_height
            $("#front_waist_height").text(parseFloat(front_waist_height / 2.54).toFixed(1) + " Inches");
            $("#f_waist_pant_allow").text(parseFloat(f_waist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_f_waist").text(parseFloat(final_pant_f_waist / 2.54).toFixed(1) + " Inches");
            //for knee_right
            $("#knee_right").text(parseFloat(knee_right / 2.54).toFixed(1) + " Inches");
            $("#knee_pant_allow").text(parseFloat(knee_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_knee").text(parseFloat(final_pant_knee / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length
            $("#back_jacket_length").text(parseFloat(back_jacket_length / 2.54).toFixed(1) + " Inches");
            $("#jacket_shirt_allow").text(parseFloat(jacket_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#jacket_jacket_allow").text(parseFloat(jacket_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#jacket_vest_allow").text(parseFloat(jacket_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_jacket").text(parseFloat(final_shirt_jacket / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_jacket").text(parseFloat(final_jacket_jacket / 2.54).toFixed(1) + " Inches");
            $("#final_vest_jacket").text(parseFloat(final_vest_jacket / 2.54).toFixed(1) + " Inches");
            //for u_rise
            $("#u_rise").text(parseFloat(u_rise / 2.54).toFixed(1) + " Inches");
            $("#rise_pant_allow").text(parseFloat(rise_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant1_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant11_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant2_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant3_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            //for outseam_l_pants
            $("#outseam_l_pants").text(parseFloat(outseam_l_pants / 2.54).toFixed(1) + " Inches");
            $("#p_left_pant_allow").text(parseFloat(p_left_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_p_left").text(parseFloat(final_pant_p_left / 2.54).toFixed(1) + " Inches");
            //for outseam_r_pants
            $("#outseam_r_pants").text(parseFloat(outseam_r_pants / 2.54).toFixed(1) + " Inches");
            $("#p_right_pant_allow").text(parseFloat(p_right_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_p_right").text(parseFloat(final_pant_p_right / 2.54).toFixed(1) + " Inches");
            //for bottom
            $("#bottom").text(parseFloat(bottom / 2.54).toFixed(1) + " Inches");
            $("#bottom_pant_allow").text(parseFloat(bottom_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_bottom").text(parseFloat(final_pant_bottom / 2.54).toFixed(1) + " Inches");

            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    function showMessage(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
        $(".modal-footer").css({"display": "none"});
        $("#modal").modal("show");
        setTimeout(function () {
            $("#modal").modal("hide");
        }, 3000);
    }
    function incdec(op, _id) {
        if (op == "minus") {
            var val = $("#" + _id).text().split(" ")[0];
            if ($("#params").val() == "Inches") {
                val = parseFloat(val) - parseFloat(0.25);
                if(_id == "final_shirt_collar"){
                    if(val<((collar_min/2.54).toFixed(1)) || val>((collar_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_chest"){
                    if(val<((chest_min/2.54).toFixed(1)) || val>((chest_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_stomach"){
                    if(val<((stomach_min/2.54).toFixed(1)) || val>((stomach_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val>((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_bicep"){
                    if(val<((bicep_min/2.54).toFixed(1)) || val>((bicep_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_back_shoulder"){
                    if(val<((back_shoulder_min/2.54).toFixed(1)) || val>((back_shoulder_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_front_shoulder"){
                    if(val<((front_shoulder_min/2.54).toFixed(1)) || val>((front_shoulder_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_right"){
                    if(val<((sleeve_right_min/2.54).toFixed(1)) || val>((sleeve_right_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_left"){
                    if(val<((sleeve_left_min/2.54).toFixed(1)) || val>((sleeve_left_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_jacket"){
                    if(val<((back_jacket_length_min/2.54).toFixed(1)) || val>((back_jacket_length_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_chest"){
                    if(val<((chest_min/2.54).toFixed(1)) || val>((chest_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_stomach"){
                    if(val<((stomach_min/2.54).toFixed(1)) || val>((stomach_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val>((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_bicep"){
                    if(val<((bicep_min/2.54).toFixed(1)) || val>((bicep_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_back_shoulder"){
                    if(val<((back_shoulder_min/2.54).toFixed(1)) || val>((back_shoulder_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_front_shoulder"){
                    if(val<((front_shoulder_min/2.54).toFixed(1)) || val>((front_shoulder_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_right"){
                    if(val<((sleeve_right_min/2.54).toFixed(1)) || val>((sleeve_right_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_left"){
                    if(val<((sleeve_left_min/2.54).toFixed(1)) || val>((sleeve_left_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_jacket"){
                    if(val<((back_jacket_length_min/2.54).toFixed(1)) || val>((back_jacket_length_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_chest"){
                    if(val<((chest_min/2.54).toFixed(1)) || val>((chest_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_stomach"){
                    if(val<((stomach_min/2.54).toFixed(1)) || val>((stomach_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_jacket"){
                    if(val<((back_jacket_length_min/2.54).toFixed(1)) || val>((back_jacket_length_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val >((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_calf"){
                    if(val<((calf_min/2.54).toFixed(1)) || val >((calf_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant1_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val >((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant11_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val >((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant2_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val >((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant3_seat"){
                    if(val<((seat_min/2.54).toFixed(1)) || val >((seat_max/2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Inches");
            } else {
                val = parseFloat(val) - parseFloat(0.5);
                if(_id == "final_shirt_collar"){
                    if(val<collar_min || val>collar_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_chest"){
                    if(val < chest_min || val > chest_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_stomach"){

                    if(val < stomach_min || val > stomach_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_bicep"){
                    if(val < bicep_min || val > bicep_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_back_shoulder"){
                    if(val < back_shoulder_min || val > back_shoulder_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_front_shoulder"){
                    if(val < front_shoulder_min || val > front_shoulder_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_right"){
                    if(val < sleeve_right_min || val > sleeve_right_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_left"){
                    if(val < sleeve_left_min || val > sleeve_left_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_jacket"){
                    if(val < back_jacket_length_min || val > back_jacket_length_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_chest"){
                    if(val < chest_min || val > chest_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_stomach"){
                    if(val < stomach_min || val > stomach_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_bicep"){
                    if(val < bicep_min || val > bicep_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_back_shoulder"){
                    if(val < back_shoulder_min || val > back_shoulder_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_front_shoulder"){
                    if(val < front_shoulder_min || val > front_shoulder_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_right"){
                    if(val < sleeve_right_min || val > sleeve_right_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_left"){
                    if(val < sleeve_left_min || val > sleeve_left_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_jacket"){
                    if(val < back_jacket_length_min || val > back_jacket_length_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_chest"){
                    if(val < chest_min || val > chest_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_stomach"){
                    if(val < stomach_min || val > stomach_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_jacket"){
                    if(val < back_jacket_length_min || val > back_jacket_length_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_calf"){
                    if(val < calf_min || val > calf_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant1_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant11_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant2_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant3_seat"){
                    if(val < seat_min || val > seat_max){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Cm");
            }
        } else if (op == "plus") {
            var val = $("#" + _id).text().split(" ")[0];
            if ($("#params").val() == "Inches") {
                val = parseFloat(val) + parseFloat(0.25);
                if(_id == "final_shirt_collar"){
                    if(val<((collar_min / 2.54).toFixed(1)) || val>((collar_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_chest"){
                    if(val<((chest_min / 2.54).toFixed(1)) || val>((chest_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_stomach"){
                    if(val<((stomach_min / 2.54).toFixed(1)) || val>((stomach_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_bicep"){
                    if(val<((bicep_min / 2.54).toFixed(1)) || val>((bicep_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_back_shoulder"){
                    if(val<((back_shoulder_min / 2.54).toFixed(1)) || val>((back_shoulder_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_front_shoulder"){
                    if(val<((front_shoulder_min / 2.54).toFixed(1)) || val>((front_shoulder_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_right"){
                    if(val<((sleeve_right_min / 2.54).toFixed(1)) || val>((sleeve_right_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_left"){
                    if(val<((sleeve_left_min / 2.54).toFixed(1)) || val>((sleeve_left_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_jacket"){
                    if(val<((back_jacket_length_min / 2.54).toFixed(1)) || val>((back_jacket_length_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_chest"){
                    if(val<((chest_min / 2.54).toFixed(1)) || val>((chest_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_stomach"){
                    if(val<((stomach_min / 2.54).toFixed(1)) || val>((stomach_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_bicep"){
                    if(val<((bicep_min / 2.54).toFixed(1)) || val>((bicep_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_back_shoulder"){
                    if(val<((back_shoulder_min / 2.54).toFixed(1)) || val>((back_shoulder_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_front_shoulder"){
                    if(val<((front_shoulder_min / 2.54).toFixed(1)) || val>((front_shoulder_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_right"){
                    if(val<((sleeve_right_min / 2.54).toFixed(1)) || val>((sleeve_right_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_left"){
                    if(val<((sleeve_left_min / 2.54).toFixed(1)) || val>((sleeve_left_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_jacket"){
                    if(val<((back_jacket_length_min / 2.54).toFixed(1)) || val>((back_jacket_length_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_chest"){
                    if(val<((chest_min / 2.54).toFixed(1)) || val>((chest_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_stomach"){
                    if(val<((stomach_min / 2.54).toFixed(1)) || val>((stomach_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_jacket"){
                    if(val<((back_jacket_length_min / 2.54).toFixed(1)) || val>((back_jacket_length_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_calf"){
                    if(val<((calf_min / 2.54).toFixed(1)) || val>((calf_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant1_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant11_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant2_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant3_seat"){
                    if(val<((seat_min / 2.54).toFixed(1)) || val>((seat_max / 2.54).toFixed(1))){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Inches");
            } else {
                val = parseFloat(val) + parseFloat(0.5);
                if(_id == "final_shirt_collar"){
                    if(val<collar_min || val>collar_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_chest"){
                    if(val< chest_min || val> chest_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_stomach"){
                    if(val< stomach_min || val> stomach_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_bicep"){
                    if(val< bicep_min || val> bicep_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_back_shoulder"){
                    if(val< back_shoulder_min || val> back_shoulder_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_front_shoulder"){
                    if(val< front_shoulder_min || val> front_shoulder_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_right"){
                    if(val< sleeve_right_min || val> sleeve_right_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_sleeve_left"){
                    if(val< sleeve_left_min || val> sleeve_left_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_shirt_jacket"){
                    if(val< back_jacket_length_min || val> back_jacket_length_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_chest"){
                    if(val< chest_min || val> chest_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_stomach"){
                    if(val< stomach_min || val> stomach_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_bicep"){
                    if(val< bicep_min || val> bicep_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_back_shoulder"){
                    if(val< back_shoulder_min || val> back_shoulder_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_front_shoulder"){
                    if(val< front_shoulder_min || val> front_shoulder_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_right"){
                    if(val< sleeve_right_min || val> sleeve_right_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_sleeve_left"){
                    if(val< sleeve_left_min || val> sleeve_left_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_jacket_jacket"){
                    if(val< back_jacket_length_min || val> back_jacket_length_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_chest"){
                    if(val< chest_min || val> chest_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_stomach"){
                    if(val< stomach_min || val> stomach_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_vest_jacket"){
                    if(val< back_jacket_length_min || val> back_jacket_length_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant_calf"){
                    if(val< calf_min || val> calf_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant1_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant11_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant2_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                else if(_id == "final_pant3_seat"){
                    if(val< seat_min || val> seat_max ){
                        $("#" + _id).css("color","red");
                    }else{
                        $("#" + _id).css("color","#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Cm");
            }
        }
    }
    function subtractshoulder(user_id,backshoulder,frontshoulder){
        var url = 'api/registerUser.php';
        $.post(url,{'type': 'subtractshoulder',"user_id":user_id,"backshoulder":backshoulder,"frontshoulder":frontshoulder},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                alert("done");
            }
        });
    }
    function writeminmax(sizeData){
        var url = 'api/registerUser.php';
        $.post(url,{'type': 'minmaxdata'},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                var minmaxdata = data.minmaxData;
                chest_min = parseFloat(minmaxdata[0].min_val);
                chest_max = parseFloat(minmaxdata[0].max_val);
                bicep_min = parseFloat(minmaxdata[1].min_val);
                bicep_max = parseFloat(minmaxdata[1].max_val);
                back_jacket_length_min = parseFloat(minmaxdata[2].min_val);
                back_jacket_length_max = parseFloat(minmaxdata[2].max_val);
                stomach_min = parseFloat(minmaxdata[3].min_val);
                stomach_max = parseFloat(minmaxdata[3].max_val);
                sleeve_left_min = parseFloat(minmaxdata[4].min_val);
                sleeve_left_max = parseFloat(minmaxdata[4].max_val);
                sleeve_right_min = parseFloat(minmaxdata[5].min_val);
                sleeve_right_max = parseFloat(minmaxdata[5].max_val);
                back_shoulder_min = parseFloat(minmaxdata[6].min_val);
                back_shoulder_max = parseFloat(minmaxdata[6].max_val);
                front_shoulder_min = parseFloat(minmaxdata[6].min_val);
                front_shoulder_max = parseFloat(minmaxdata[6].max_val);
                calf_min = parseFloat(minmaxdata[7].min_val);
                calf_max = parseFloat(minmaxdata[7].max_val);
                collar_min = parseFloat(minmaxdata[8].min_val);
                collar_max= parseFloat(minmaxdata[8].max_val);
                seat_min = parseFloat(minmaxdata[9].min_val);
                seat_max= parseFloat(minmaxdata[9].max_val);
                showminmaxrange();
                getMeasureData(sizeData);
            }
        });
    }
    function showminmaxrange(){
        var url = 'api/registerUser.php';
        $.post(url,{'type': 'minmaxdata'},function(data) {
            var Status = data.Status;
            if (Status == "Success") {
                var minmaxdata = data.minmaxData;
                if ($("#params").val() == "Inches") {
                    for (var i = 0; i < minmaxdata.length; i++) {
                        if (minmaxdata[i].parameter == "shoulder_") {
                            var min = (minmaxdata[i].min_val / 2.54).toFixed(1);
                            var max = (minmaxdata[i].max_val / 2.54).toFixed(1);
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("#" + minmaxdata[i].parameter + "textb").html(text);
                            $("#" + minmaxdata[i].parameter + "textf").html(text);
                        } else {
                            var min = (minmaxdata[i].min_val / 2.54).toFixed(1);
                            var max = (minmaxdata[i].max_val / 2.54).toFixed(1);
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("#" + minmaxdata[i].parameter + "text").html(text);
                        }
                    }
                } else {
                    for (var i = 0; i < minmaxdata.length; i++) {
                        if (minmaxdata[i].parameter == "shoulder_") {
                            var min = minmaxdata[i].min_val;
                            var max = minmaxdata[i].max_val;
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("#" + minmaxdata[i].parameter + "textb").html(text);
                            $("#" + minmaxdata[i].parameter + "textf").html(text);
                        } else {
                            var min = minmaxdata[i].min_val;
                            var max = minmaxdata[i].max_val;
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("#" + minmaxdata[i].parameter + "text").html(text);
                        }
                    }
                }
            }
        });
    }
getData();
</script>