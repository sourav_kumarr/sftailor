<?php
include('header.php');
$conn = new \Classes\CONNECT();
?>
<style xmlns="http://www.w3.org/1999/html">
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
    .panel_toolboxs > li {
        float: left;
        cursor: pointer;
        padding: 10px 31px 0px 31px;
        background: #f1f1f1;
        border: 1px solid #ddd;
        margin-left: 15px;
    }
    .matchingTabs{cursor: pointer;}
    .allowtable{overflow: hidden}
    .pdf-imgr img{height: 261px;width: 255px;"}
</style>

<link rel="stylesheet" href="css/waiting.css"/>
<div class="loader" style="display: none" >
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<div class="right_col" role="main" id="container">
    <div class="row tile_count"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>All DOCUMENTS </h2>
                    <span class="edit_menu" onclick="edit_tab()" style="float: right;background: #48b557;padding: 5px 13px;color: white;border-radius: 3px;">Edit Tabs</span>

                    <!--  <ul class="nav navbar-right panel_toolbox">
                          <!--<li>
                              <form method="post" class="form-inline">
                                  <div class="form-group form-inline">
                                      <input type="file" class="form-control" name="file" id="file"/>
                                  </div>
                                  <input type="button" Value="Go" class="btn btn-primary btn-sm" name="filterButton"
                                         style="margin-top: 5px" onclick="uploadData();"/>
                              </form>
                          </li>-->
                    </ul>-->
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        List of Orders Generated
                    </p>
                    <div class="row">
                        <form method="Post" enctype="multipart/form-data" id="update_menustab">
                        <ul class="nav nav-tabs">

                            <?php
                            $link = $conn->connect();//for scan2tailor
                            if ($link) {
                                $query = "SELECT DISTINCT menu_name,file_type FROM wp_pdf order by pdf_id desc";
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        while ($menuData = mysqli_fetch_assoc($result)) {
                                            $menu_name = $menuData['menu_name'];
                                            $file_type = $menuData['file_type'];
                                            ?>

                                <li role="presentation" id="<?php echo $file_type;?>allow" class="matchingTabs tabs" onclick=switchMatching('<?php echo $file_type;?>') >
                                    <a ><span class="menu_names"><?php echo $menu_name;?></span></a>
                                        <input type="text" name="update_<?php echo $file_type;?>" value="<?php echo $menu_name;?>" class="update_menu" style="display: none;border: 1px solid rgb(218, 206, 206) !important; padding: 5px; background: #f6f2ec none repeat scroll 0% 0%; margin-top: 0px; border-radius: 2px;" ></li>
                                <?php } } } } ?>
                                <button class="update_menus btn btn-primary btn-sm"  style="display:none;float: right;background: #48b557;padding: 5px 13px;color: white;border-radius: 3px;">Update Tabs</button>
                            <input type="hidden" name="type" value="update_tab">
                        </ul>
                        </form>
                        </div>

                        <div class="row">
                            <?php
                            $link1 = $conn->connect();//for scan2tailor
                            if ($link1) {
                            $query1 = "SELECT DISTINCT menu_name,file_type FROM wp_pdf order by pdf_id desc";
                            $result1 = mysqli_query($link1, $query1);
                            if ($result1) {
                            $num1 = mysqli_num_rows($result1);
                            if ($num1 > 0) {
                            while ($menuData1 = mysqli_fetch_assoc($result1)) {
                            $menu_name1 = $menuData1['menu_name'];
                            $file_type1 = $menuData1['file_type'];
                            ?>
                                <div class="col-md-12 allowtable" id="<?php echo $file_type1;?>_table" style="display:none;">
                                    <form method="post" class="form-inline pos-right"
                                          enctype="multipart/form-data" name="conversionform_<?php echo $file_type1;?>" id="conversionform_<?php echo $file_type1;?>" style="margin-bottom: -11px;width: 23%;margin-top: 10px;display: flow-root;position: inherit;float: right;">
                                        <div class="form-group form-inline">
                                            <input type="hidden" id="type" name="type" value="createPdf"/>

                                            <input type="hidden" id="type_sort_<?php echo $file_type1;?>" name="type_sort_<?php echo $file_type1;?>" value=""/>

                                            <input type="hidden" id="file_type" name="file_type" value="<?php echo $file_type1;?>"/>
                                            <input type="file" class="form-control" name="filee_<?php echo $file_type1;?>" id="filee_<?php echo $file_type1;?>" style="opacity: 0;float: right;position: absolute;cursor: pointer;width: 153px;"/>
                                            <div style="background: cadetblue;padding: 7px;width: 180px;text-align: center;color: white;border-radius: 2px;float: right;">Upload <?php echo $menu_name1;?></div>
                                        </div>
                                        <input type="button" Value="Go" class="btn btn-primary btn-sm" name="filterButton"
                                               style="margin-top: 5px" onclick=uploadData('<?php echo $file_type1;?>'); />
                                    </form>
                                    <div id="<?php echo $file_type1;?>_tables" style="margin-top: 65px;">
                                        <div class="row"></div>
                                    </div>
                                </div>
                            <?php } } } } ?>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="resultbody"></div>
<style>
    .main-grid-box{height: 288px;
        padding-top: 10px;
        text-align: center;
        margin-left: 5%;
        margin-bottom: 24px;
        border: 1px solid #999;
        width: 28%;
    }
    .pdf-img img {
        margin-bottom: 12px;
    }
    .pdf-title{margin-bottom: 12px;margin-top: 9px;}
    .ext-img{height: 240px;
        width: 272px;
        margin-bottom: 15px;
        background-position: center center;
        background-size: cover;}
</style>

<?php
include('footer.php');
?>
<script src="js/jquery.waiting.js"></script>
<script src="js/jquery.form.js"></script>
<script>
    function edit_tab() {
        $(".update_menu").show();
        $(".update_menus").show();
        $(".menu_names").hide();
        $(".edit_menu").hide();
    }
    function update_tab() {
    }
    $("#videosallow").addClass("active");
    $("#videos_table").show();
    function switchMatching(tab) {
        $(".allowtable").css("display","none");
        $("#"+tab+"_table").css("display","block");
        $(".tabs").removeClass("active");
        $("#"+tab+"allow").addClass("active");
    }
    function getAllPDF() {
        $(".loader").show();
        var url = 'api/pdfProcess.php';
        $.post(url,{'type':'getAllPDF'},function(data){
            var status = data.Status;
            var message = data.Message;
            var th_ = '<div class="row">';
            var td_ = '';
            var th1_ = '<div class="row">';
            var td1_ = '';
            var th2_ = '<div class="row">';
            var td2_ = '';

            if(status === 'Success') {
                var items = data.data;
                for(var i=0;i<items.length;i++) {
                    var obj = items[i];
                    var title = obj.pdf_title;
                    var pdftitle = title.replace(/_/g, " ");
                    var file_type = obj.file_type;
                    if(file_type =="pdf"){
                        var ext = obj.pdf_file.split('.').pop();
                        if(ext =="mp4" || ext =="3gp" || ext =="avi" || ext =="mov"){
                            td_ = td_+'<div class="col-sm-3 main-grid-box" style="height: 300px;"><div class="pdf-imgr"><video width="273" height="240" style="margin-bottom: 11px;" controls><source src="http://scan2fit.com/sftailor/admins/api/Files/videos/'+obj.pdf_file+'" type="video/mp4" style="margin-bottom: 15px;width: 267px;"><source src="movie.ogg" type="video/ogg"></video>' +
                                '</div><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }else if(ext =="jpg" || ext =="jpeg" || ext =="png"){
                            td_ = td_+'<div class="col-sm-3 main-grid-box" style="height: 310px;"><div class="pdf-imgr ext-img" style=background-image:url("http://scan2fit.com/sftailor/admins/api/Files/images/'+obj.pdf_file+'")>' +
                                '</div><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }
                        else if(ext =="pdf"){
                            td_ = td_+'<div class="col-sm-3 main-grid-box"><div class="pdf-img"><iframe src="http://scan2fit.com/sftailor/admins/api/Files/pdf/'+title+'.pdf#scrollbar=0&scrolling=0" width="270" height="205"  frameborder="0" scrolling="no"></iframe>' +
                                '</div><p class="pdf-title"> '+pdftitle+' </p><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }
                        $("#pdf_tables").html(th_+td_+'</div>');
                    }
                    else if(file_type =="images"){
                        var ext = obj.pdf_file.split('.').pop();
                        if(ext =="mp4" || ext =="3gp" || ext =="avi" || ext =="mov"){
                            td1_ = td1_+'<div class="col-sm-3 main-grid-box" style="height: 300px;"><div class="pdf-imgr"><video width="273" height="240" style="margin-bottom: 11px;" controls><source src="http://scan2fit.com/sftailor/admins/api/Files/videos/'+obj.pdf_file+'" type="video/mp4" style="margin-bottom: 15px;width: 267px;"><source src="movie.ogg" type="video/ogg"></video>' +
                                '</div><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }else if(ext =="jpg" || ext =="jpeg" || ext =="png"){
                            td1_ = td1_+'<div class="col-sm-3 main-grid-box" style="height: 310px;"><div class="pdf-imgr ext-img" style=background-image:url("http://scan2fit.com/sftailor/admins/api/Files/images/'+obj.pdf_file+'")>' +
                                '</div><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }
                        else if(ext =="pdf"){
                            td1_ = td1_+'<div class="col-sm-3 main-grid-box"><div class="pdf-img"><iframe src="http://scan2fit.com/sftailor/admins/api/Files/pdf/'+title+'.pdf#scrollbar=0&scrolling=0" width="270" height="205"  frameborder="0" scrolling="no"></iframe>' +
                                '</div><p class="pdf-title"> '+pdftitle+' </p><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }
                        $("#images_tables").html(th1_+td1_+'</div>');
                    }
                    else if(file_type =="videos"){
                        var ext = obj.pdf_file.split('.').pop();
                        if(ext =="mp4" || ext =="3gp" || ext =="avi" || ext =="mov"){
                            td2_ = td2_+'<div class="col-sm-3 main-grid-box" style="height: 300px;"><div class="pdf-imgr"><video width="273" height="240" style="margin-bottom: 11px;" controls><source src="http://scan2fit.com/sftailor/admins/api/Files/videos/'+obj.pdf_file+'" type="video/mp4" style="margin-bottom: 15px;width: 267px;"><source src="movie.ogg" type="video/ogg"></video>' +
                                '</div><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }else if(ext =="jpg" || ext =="jpeg" || ext =="png"){
                            td2_ = td2_+'<div class="col-sm-3 main-grid-box" style="height: 310px;"><div class="pdf-imgr ext-img" style=background-image:url("http://scan2fit.com/sftailor/admins/api/Files/images/'+obj.pdf_file+'")>' +
                            '</div><div class="action-point">' +
                            '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                            '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                            '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }
                        else if(ext =="pdf"){
                            td_ = td_+'<div class="col-sm-3 main-grid-box"><div class="pdf-img"><iframe src="http://scan2fit.com/sftailor/admins/api/Files/videos/'+title+'.pdf#scrollbar=0&scrolling=0" width="270" height="205"  frameborder="0" scrolling="no"></iframe>' +
                                '</div><p class="pdf-title"> '+pdftitle+' </p><div class="action-point">' +
                                '<button class="btn btn-md btn-danger" onclick=confirmDelete("'+obj.pdf_id+'","'+encodeURI(obj.pdf_file_path)+'");>' +
                                '<i class="fa fa-trash"></i></button><a href="'+obj.pdf_file_path+'" target="_blank">' +
                                '<button class="btn btn-md btn-primary "><i class="fa fa-download"></i></button></a></div></div>';
                        }
                        $("#videos_tables").html(th2_+td2_+'</div>');
                    }

                }
                $(".loader").hide();
            }

            $('#pdfTable').DataTable({destroy:true});
        }).fail(function () {
            $(".modal-title").html("<label style='color: green'>Error !!!</label>");
            $(".modal-body").html("<label style='font-family: monospace'>Unable to process the request</label>");
            $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $("#myModal").modal("show");
        });
    }
    function showResponse(responseText, statusText, xhr, $form) {

        var str = '';
        var istr = '';
        var data = responseText;

        $('#container').waiting('done');
        var status = data.Status;
        if (status) {
            getAllPDF();
        }
    }
    function showError(responseText, statusText, xhr, $form) {
        // alert(JSON.stringify(statusText));
        $('#container').waiting('done');
        var num = 0;
        var str = '<tr id=tr' + num + '><td>' + num + '</td><td><a href="#">Uploading Failed</a></td></tr>';
        $("#resultbody").append(str);
    }
    function showUploadProgress(event, position, total, percentComplete) {
        var percentVal = percentComplete + '%';
        if (percentVal === '100%') {
            $('.waiting-percent').html("Loading..");
        }
        else
            $('.waiting-percent').html(percentVal);
    }
    function uploadData(typs) {

        if(file === ''){
            $(".modal-title").html("<label style='color: green'>Error !!!</label>");
            $(".modal-body").html("<label style='font-family: monospace'>Please select file before upload</label>");
            $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
            $("#myModal").modal("show");
            return false;
        }
        var file = $("#filee_"+typs).val();

        var fileType = '';
        var ext = file.split('.').pop();
        var images = ['jpeg','jpg','png'];
        var videos = ['avi','mp4','3gp'];
        var pdf = ['pdf'];

        if(images.indexOf(ext)>0) {
            fileType = 'images';
        }
        else if(videos.indexOf(ext)>0) {
            fileType = 'videos';
        }
        else if(pdf.indexOf(ext)>0) {
            fileType = 'pdf';
        }


       // $("#file_type").val(fileType);

        var file_data = document.getElementById("filee_"+typs);

        var options = {
            success: showResponse,
            error: showError,
            uploadProgress: showUploadProgress,
            url: "api/pdfProcess.php"
        };

        $('#conversionform_'+typs).ajaxForm(options);
        $('#conversionform_'+typs).ajaxSubmit(options);
        $('#container').waiting();
        $('.waiting-percent').html('0%');

        return false;
        /* var request = new XMLHttpRequest();
         var data = new FormData();
         var url = 'api/pdfProcess.php';
         request.open("POST", url, true);
         $(".loader").show();
         request.onreadystatechange = function () {
             $(".loader").hide();
             if(request.readyState === 4 && request.status === 200) {
                 var response = $.parseJSON(request.responseText);
                 var status = response.Status;
                 var message = response.Message;
                 console.log(status);
                 if(status === 'Failure') {
                     $(".modal-title").html("<label style='color: green'>Error !!!</label>");
                     $(".modal-body").html("<label style='font-family: monospace'>"+message+"</label>");
                     $(".modal-footer").html('<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>');
                     $("#myModal").modal("show");
                     return false;
                 }
                 getAllPDF();
             }
         };
         data.append("type","createPdf");
         data.append("file_type",filetype);
         data.append("file",file_data.files[0]);
         request.send(data);*/
    }

    function confirmDelete(id,path) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete ?</label>");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" class="btn btn-primary" data-dismiss="modal"> Close</button>'+
            '<button type="button" class="btn btn-danger" onclick=deleteData("'+id+'","'+path+'")>Delete</button>');

        $("#myModal").modal("show");
    }

    function deleteData(id,path) {
        var url ='api/pdfProcess.php';
        path = decodeURI(path);
        $.post(url,{type:'deleteData','id':id,path:path},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                getAllPDF();
            }
            $("#message").html(message);
        }).fail(function () {
            $("#message").html("Unable to process the request");
        });
    }

    $("#update_menustab").submit(function (e) {
        e.preventDefault();
        $('.loader').fadeIn("slow");
        $.ajax({
            url: "api/pdfProcess.php",
            type: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                $('.loader').fadeOut("slow");
                var status = data.Status;
                if (status == "Success") {
                    window.location = "pdf.php";
                }
                else {
                    alert(message);
                }
            }
        });
    });


    getAllPDF();
</script>