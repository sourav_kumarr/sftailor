<?php
include('header.php');
require_once('api/Classes/USERCLASS.php');
require_once('api/Classes/SUPPLIER.php');
$conn = new \Classes\STORECONNECT();
$userClass = new \Classes\USERCLASS();
$supplierClass = new \Classes\SUPPLIER();
$allSupplierData = $supplierClass->getSupplierDetails();
?>
<style>
.shadoows {
	background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
	height: 100%;
	left: 0;
	margin: 0;
	padding: 0;
	position: fixed;
	right: 0;
	top: 0;
	width: 100%;
	display: none;
	z-index: 9999;
}
.loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
#create_supplier{background: white none repeat scroll 0% 0%; padding: 37px 25px!important; border-radius: 4px; top: -100px;z-index: 9999999;}
#send_supplier_data{ background: white none repeat scroll 0 0;
    border-radius: 4px;
    left: 182px;
    padding: 37px 25px!important;
    top: 19px;
    z-index: 999999;}
.loader {
    background: #eee none repeat scroll 0 0;
    height: 830px;
    opacity: 0.7;
    position: fixed;
    width: 100%;
    z-index: 2147483647;
    top:0;
}
.spinloader {
    left: 34%;
    position: absolute;
    top: 14%;
}
</style>
<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<!-- page content -->
<div class="right_col" role="main">
    <div class="row tile_count">

    </div>
    <div class="">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                    <div class="x_title">
                        <h2>All Orders <small></small></h2>
                        <ul class="nav navbar-right panel_toolbox" style="display: none;">
                            <li>
                                <button style="margin-top:5px" onclick="window.location='api/excelProcess.php?dataType=allOrders'" class="btn btn-info btn-sm">Download Excel File</button>
                            </li>
                            <li>
                                <form method="post" class="form-inline">
                                    <div class="form-group form-inline">
                                        <input type="text" placeholder="Start Date" class="form-control" name="startDate" id="startFilter" />
                                    </div>
                                    <div class="form-group">
                                        <input type="text" placeholder="End Date" class="form-control" name="endDate" id="endFilter" />
                                    </div>
                                    <input type="submit" Value="Go" class="btn btn-warning btn-sm" name="filterButton" style="margin-top: 5px" />
                                </form>
                            </li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
					<!----------------send mail supplier---------------->	
					<!--<div class="shadoows" onclick="close_diolog()" style="display: none;"></div>
						<div class="col-md-6 suppliers" id="send_supplier_data" style="padding: 0;display: none;" >
							<div class="form-group col-md-4">
								<label>Supplier Details</label></br></br>
								<select id="supplier_email_data" name="supplier_id" style="height: 30px; width: 200px;">
								<option value="">Please Select Supplier Name</option>
									 <?php
/*										$supplierData = $allSupplierData['supplierData'];
										for($i=0;$i<count($supplierData);$i++){
									*/?>
									<option value="<?php /*echo $supplierData[$i]['contact_email']; */?>"><?php /*echo $supplierData[$i]['company_name']; */?></option>
										<?php /*} */?>
								</select>
							<div class="clearfix"></div>
						</div>
						<button style="float: right; margin-top: 40px;" id="suppEmail" onclick="sendEmail()" class="btn btn-info btn-sm email">Send Mail</button>
						</div>	-->
                    <button style="float: right; margin-top: 40px;" id="suppEmail" onclick="sendMail()" class="btn btn-info btn-sm email">Send Mail</button>
                    <?php if($_REQUEST["t"]=="sort"){ ?>
                    <a href="supplier_order.php"><button style="float: right; margin-top: 40px;" class="btn btn-danger btn-sm email">Un-Sort</button></a>
                    <?php } else { ?>
                        <a href="supplier_order.php?t=sort"><button style="float: right; margin-top: 40px;" class="btn btn-info btn-sm email">Sort</button></a>
                    <?php } ?>
                    <div class="x_content">
                        <p class="text-muted font-13 m-b-30">
                            List of Orders Generated
                        </p>
                        <table id="orderTable" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>User Name</th>
                                <th>Order Id</th>
                                <th>Order Amount</th>
                                <th>Order Dated</th>
                                <th>Order Status</th>
                                <th>Payment Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php

                            $link = $conn->connect();//for sftailor
                            if ($link) {
                                $query = "select * from wp_orders order by order_id DESC";
                                if($_REQUEST['t'] =='sort'){
                                    $query = "select * from wp_orders order by email_status='1'";
                                }
                                $result = mysqli_query($link, $query);
                                if ($result) {
                                    $num = mysqli_num_rows($result);
                                    if ($num > 0) {
                                        $j = 0;
                                        while ($orderData = mysqli_fetch_array($result)) {
                                            $j++;
                                            ?>
                                            <tr>
                                                <td data-title='#'><?php echo $j ?></td>
                                                <?php
                                                    $userResp = $userClass->getParticularUserData($orderData['order_user_id']);
							
                                                    $userName = $userResp['UserData']['name'];
                                                ?>
                                                <td data-title='User Name'><?php echo $userName ?></td>
                                                <td data-title='Order Id'>
                                                    <a style="border-bottom: 1px solid blue; padding-bottom: 4px;" href='supodet?_=<?php echo $orderData['order_id'] ?>'>
                                                        <?php echo $orderData['order_number'] ?>
                                                    </a>
                                                </td>
                                                <?php
                                                $amount = $orderData['order_total'];
                                                ?>
                                                <td data-title='Order Amount'>$<?php echo $amount ?></td>
                                                <td data-title='Order Dated'><?php echo $orderData['order_date']; ?></td>
                                                <td data-title='Order Status'><?php echo $orderData['order_state'] ?></td>
                                                <td data-title='Payment Status'><?php echo $orderData['order_payment_status'] ?></td>

                                                    <td data-title='Action'>
														<?php if($orderData['email_status'] =="1"){
                                                            echo "<span style='background: limegreen none repeat scroll 0% 0%; color: snow; text-align: center; font-size: 11px; padding: 5px 22px;'>Ordered</span>";
                                                          //  echo "&nbsp;&nbsp;&nbsp;<input type='checkbox' name='check_email' checked onclick=sendEmail(".$orderData['order_id'].")>";
                                                            echo "&nbsp;&nbsp;<button id='suppEmail' style='margin-top: 3px; border-radius: 0px; height: 23px; line-height: 1px;' onclick=resendMail('".$orderData['order_id']."') class='btn btn-info btn-sm email'>Resend</button>";
                                                        } else {
                                                            echo "<span style='background: tomato none repeat scroll 0% 0%; color: snow; font-size: 11px; padding: 5px 7px;'>order not send</span>";
                                                            echo "&nbsp;&nbsp;&nbsp;<input type='checkbox' name='check_email' value='".$orderData['order_id']."'>";
                                                        }

                                                        ?>
                                                        <a href='#' onclick=delOrder('<?php echo $orderData['order_id'];?>') style="float: right; font-size: 18px; padding: 2px 4px; background: red none repeat scroll 0% 0%; color: white; margin-right: 10px;"><i class="fa fa-trash-o"></i> </a>



                                                    </td>
                                            
                                                </tr>
                                            <?php
                                        }
                                    }
                                }
                            }
                            ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <div class="gwc_box" id="cartList" data-price="￥">
                                    <table class="gwc_tb productitem-view-table" width="100%" cellspacing="0"
                                           cellpadding="0" border="0"></table>
                                </div>
 <div class="layui-layer layui-anim layui-layer-page layer-ext-seaning" id="layui-layer5"
                                 type="page" times="5"
                                 showtime="0" contype="string"
                                 style="z-index: 19891019; top: 65px; height: 600vh; left: 5%; width:90%;display:none;">

                                <div style="float: right; text-align: center; width: 87px; height: 26px; margin-top: 6px; border-radius: 2px; line-height: 1.8; font-weight: 600; background: darksalmon none repeat scroll 0% 0%; color: rgb(238, 238, 238); margin-right: 19px;">
                                    <input type='button' id='btn' value='Print' onclick='printDiv();'></div>

                                <div style="text-align: center; display:none; width: 87px; height: 26px; margin-top: 6px; border-radius: 2px; line-height: 1.8; font-weight: 600; background: darksalmon none repeat scroll 0% 0%; color: rgb(238, 238, 238); float: right; margin-right: 17px;">
                                    <input type='button' id='btn' value='Email' onclick='emailDiv();'></div>
                                <div class="layui-layer-title" style="cursor: move;" move="ok">Order Detail</div>

                                <div class="layui-layer-content order-detail-modal" style="height: 556px;"></div>
                                <span class="layui-layer-setwin">
                                    <a class="layui-layer-ico layui-layer-close layui-layer-close1"
                                       href="javascript:;"></a>
                                </span>
                            </div>
<!-- /page content -->
<?php
include('footer.php');
?>
<script>
orderListData();
$(document).ready(function () {
	$('#orderTable').DataTable({});
});

function orderListData() {
	$(".shadoows").show();
	$(".suppliers").show();
        var urlData = window.location.href;
        if (urlData.indexOf("?") == -1) {
            //window.location = 'supplier_order.php';
			$(".shadoows").hide();
			$(".suppliers").hide();
        }
        urlData = urlData.split("?_=");
        var oderIds = urlData[1];
        var url = "api/orderProcess.php";
        $.post(url, {"type": "getOrder", "order_id": +oderIds}, function (datas) {
            var status = datas.Status;
            var Message = datas.Message;
            var order = datas.order;
            var UserData = datas.UserData;
            var orderData = order.orderDetail;
            var datashow = "";
            if (status == "Success") {
                for (var a = 0; a < orderData.length; a++) {
                    var det_row_id = orderData[a].det_id;
                    var det_user_id = orderData[a].det_user_id;
                    var det_order_id = orderData[a].det_order_id;
                    var product_image = orderData[a].product_image;
                    var product_type = orderData[a].product_type;
                    var order_number = order.order_number;
                    var order_date = order.order_date;
                    var store_name = order.store_name;
                    var payment_status = order.order_payment_status;
                    var meas_id = order.meas_id;
                    var det_quantity = orderData[a].det_quantity;
                    var det_price = orderData[a].cart_product_total_amount;
                    var det_product_name = orderData[a].det_product_name;

                    var userName = UserData.name;
                    var order_product_image = "";

                    switch (product_type) {
                        case "Custom Suit":
                            order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                            break;
                        case "Accessories":
                            order_product_image = product_image;
                            break;
                        case "Collection":
                            order_product_image = product_image;
                            break;
                        case "Custom 2Pc Suit":
                            order_product_image = "http://scan2fit.com/sftailor/images/jackets/C-0522nochestdart.png";
                            break;
                        case "Custom Shirt":
                            order_product_image = "http://scan2fit.com/sftailor/images/shirts/57bandcollar.png";
                            break;
                        case "Custom Pant":
                            order_product_image = "http://scan2fit.com/sftailor/images/pants/formal.jpg";
                            break;
                        case "Custom Overcoat":
                            order_product_image = "http://scan2fit.com/sftailor/images/overcoat/overcoat_default.png";
                            break;
                    }
                    datashow = datashow + '<button id="clickonButton" onclick=get_product_d("' + det_user_id + '","' + det_row_id + '","' + encodeURI(product_type) + '","' +
                    order_number + '","' + encodeURI(order_date) + '","' + det_quantity + '","' + encodeURI(det_price) + '",' +
                    '"' + encodeURI(det_product_name) + '","' + encodeURI(store_name) + '","' + payment_status + '","' + meas_id + '") ' +
                    'href="javascript:void(0);" class="buyAgain" >View Details</button>';
                }
                ///* <td class="states" style="height: 110px;" width="10%">$' + det_price + '</td>'*/
                $(".productitem-view-table").html(datashow);
            }
        });
    }
    
    function get_product_d(userId, orderRowId, product_type, order_number, order_date, det_quantity, det_price,
    det_product_name, store_name, payment_status, meas_id) {
        product_type = decodeURI(product_type);
        order_number = decodeURI(order_number);
        order_date = decodeURI(order_date);
        det_quantity = decodeURI(det_quantity);
        det_price = decodeURI(det_price);
        det_product_name = decodeURI(det_product_name);
        store_name = decodeURI(store_name);
        meas_id = decodeURI(meas_id);
        payment_status = decodeURI(payment_status);
        userId = decodeURI(userId);
        if (store_name == "") {
            store_name = "SFTailor (Main)";
        }
        var url = "api/orderProcess.php";
        $.post(url, {
            "type": "getAllOrderData",
            "orderRowId": +orderRowId,
            "user_id": +userId,
            "meas_id": meas_id
        }, function (data) {
            var status = data.Status;
            var Message = data.Message;
            var UserData = data.UserData;
            var measurementData = data.finalMeasurement[0];
            var orderDetRowData = data.orderDetRowData[0];
            var datashow = "";
            if (status == "Success") {
                var userName = UserData.name;
                var userEmail = UserData.email;
                var userTelephone = UserData.mobile;
                var userWeight = UserData.weight;
                userWeight = userWeight.split(" ")[0];
                var userHeight = UserData.height;
                userHeight = userHeight.split(" ")[0];
                var measurementsitemData = "";
                var customizationInformation = "";
                switch (product_type) {
                    case "Custom Suit":
                        customizationInformation = '<div class="ddkfxx nextg"><ul><li>' +
                        '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Category</p>' +
                        '<p class="p2">' +
                        '<input id="j_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_category",this) class="ipfield" value="' + orderDetRowData.j_category + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/frontbutton.png" />Front Button</p>' +
                        '<p class="p2">' +
                        '<input id="j_frontbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_frontbutton",this) class="ipfield" value="' + orderDetRowData.j_frontbutton + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Style</p>' +
                        '<p class="p2">' +
                        '<input id="j_lapelstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Button Holes</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelbuttonholes" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholes",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholes + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Lapel Button Holes Style</p>' +
                        '<p class="p2">' +
                        '<input id="j_lapelbuttonholesstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesstyle + '" /> ' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonholethread.png" />Lapel Button Holes Thread</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelbuttonholesthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesthread",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesthread + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Ingredient</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelingredient" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelingredient",this) class="ipfield" value="' + orderDetRowData.j_lapelingredient + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Satin</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelsatin",this) class="ipfield" value="' + orderDetRowData.j_lapelsatin + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Chest Dart</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_chestdart",this) class="ipfield" value="' + orderDetRowData.j_chestdart + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Felt Collar</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_feltcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_feltcollar",this) class="ipfield" value="' + orderDetRowData.j_feltcollar + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Inner Flap</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_innerflap" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_innerflap",this) class="ipfield" value="' + orderDetRowData.j_innerflap + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Facing Style</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_facingstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_facingstyle",this) class="ipfield" value="' + orderDetRowData.j_facingstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_sleeveslit" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslit",this) class="ipfield" value="' + orderDetRowData.j_sleeveslit + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit Thread</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_sleeveslitthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslitthread",this) class="ipfield" value="' + orderDetRowData.j_sleeveslitthread + '" />' +
                        ' </p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/breastpocket.png" />Breast Pocket</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_breastpocket",this) class="ipfield" value="' + orderDetRowData.j_breastpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lowerpocket.png" />Lower Pocket</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lowerpocket",this) class="ipfield" value="' + orderDetRowData.j_lowerpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/coinpocket.png" />Coin Pocket</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_coinpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_coinpocket",this) class="ipfield" value="' + orderDetRowData.j_coinpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/backvent.png" />Back Vent</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_backvent" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_backvent",this) class="ipfield" value="' + orderDetRowData.j_backvent + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Style</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_liningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningstyle",this) class="ipfield" value="' + orderDetRowData.j_liningstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_liningoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningoption",this) class="ipfield" value="' + orderDetRowData.j_liningoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lining" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lining",this) class="ipfield" value="' + orderDetRowData.j_lining + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Style</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_shoulderstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderstyle",this) class="ipfield" value="' + orderDetRowData.j_shoulderstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Padding</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_shoulderpadding" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderpadding",this) class="ipfield" value="' + orderDetRowData.j_shoulderpadding + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_buttonoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonoption",this) class="ipfield" value="' + orderDetRowData.j_buttonoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Swatch</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_buttonswatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonswatch",this) class="ipfield" value="' + orderDetRowData.j_buttonswatch + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Thread Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_threadoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_threadoption",this) class="ipfield" value="' + orderDetRowData.j_threadoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_elbowpatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatch",this) class="ipfield" value="' + orderDetRowData.j_elbowpatch + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch Color</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_elbowpatchcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatchcolor",this) class="ipfield" value="' + orderDetRowData.j_elbowpatchcolor + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/canvas.png" />Canvas Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_canvasoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_canvasoption",this) class="ipfield" value="' + orderDetRowData.j_canvasoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Suit Color</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_suitcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_suitcolor",this) class="ipfield" value="' + orderDetRowData.j_suitcolor + '" />' + '' +
                        '</p></li>' +
                        '</ul> </div>';
                        measurementsitemData = "" +
                        "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Back length):" + measurementData.jacket_back_jacket_length + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Back shoulder):" + measurementData.jacket_back_shoulder + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Back waist height):" + measurementData.jacket_back_waist_height + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Bicep):" + measurementData.jacket_bicep + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Bottom):" + measurementData.jacket_bottom + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Calf):" + measurementData.jacket_calf + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Chest):" + measurementData.jacket_chest + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Collar):" + measurementData.jacket_collar + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Front shoulder):" + measurementData.jacket_front_shoulder + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Front waist height):" + measurementData.jacket_front_waist_height + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Front waist length):" + measurementData.jacket_front_waist_length + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Knee):" + measurementData.jacket_knee + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Nape to waist):" + measurementData.jacket_nape_to_waist + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Pant left outsteam):" + measurementData.jacket_pant_left_outseam + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Pant right outsteam):" + measurementData.jacket_pant_right_outseam + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Seat):" + measurementData.jacket_seat + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve left):" + measurementData.jacket_sleeve_left + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve right):" + measurementData.jacket_sleeve_right + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Stomach):" + measurementData.jacket_stomach + "        </dd>";
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Thigh):" + measurementData.jacket_thigh + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(U-rise):" + measurementData.jacket_u_rise + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Jacket(Wrist):" + measurementData.jacket_wrist + "        </dd>";
                        break;
                    case "Custom 2Pc Suit":
                        customizationInformation = '<div class="ddkfxx nextg"><ul><li>' +
                        '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Category</p>' +
                        '<p class="p2">' +
                        '<input id="j_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_category",this) class="ipfield" value="' + orderDetRowData.j_category + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/frontbutton.png" />Front Button</p>' +
                        '<p class="p2">' +
                        '<input id="j_frontbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_frontbutton",this) class="ipfield" value="' + orderDetRowData.j_frontbutton + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Style</p>' +
                        '<p class="p2">' +
                        '<input id="j_lapelstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Button Holes</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelbuttonholes" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholes",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholes + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Lapel Button Holes Style</p>' +
                        '<p class="p2">' +
                        '<input id="j_lapelbuttonholesstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesstyle",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesstyle + '" /> ' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonholethread.png" />Lapel Button Holes Thread</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelbuttonholesthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelbuttonholesthread",this) class="ipfield" value="' + orderDetRowData.j_lapelbuttonholesthread + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapel.png" />Lapel Ingredient</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelingredient" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelingredient",this) class="ipfield" value="' + orderDetRowData.j_lapelingredient + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Lapel Satin</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lapelsatin" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lapelsatin",this) class="ipfield" value="' + orderDetRowData.j_lapelsatin + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Chest Dart</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_chestdart" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_chestdart",this) class="ipfield" value="' + orderDetRowData.j_chestdart + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonstyle.png" />Felt Collar</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_feltcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_feltcollar",this) class="ipfield" value="' + orderDetRowData.j_feltcollar + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Inner Flap</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_innerflap" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_innerflap",this) class="ipfield" value="' + orderDetRowData.j_innerflap + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/interfacing.png" />Facing Style</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_facingstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_facingstyle",this) class="ipfield" value="' + orderDetRowData.j_facingstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_sleeveslit" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslit",this) class="ipfield" value="' + orderDetRowData.j_sleeveslit + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/sleevebutton.png" />Sleeve Slit Thread</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_sleeveslitthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_sleeveslitthread",this) class="ipfield" value="' + orderDetRowData.j_sleeveslitthread + '" />' +
                        ' </p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/breastpocket.png" />Breast Pocket</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_breastpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_breastpocket",this) class="ipfield" value="' + orderDetRowData.j_breastpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lowerpocket.png" />Lower Pocket</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lowerpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lowerpocket",this) class="ipfield" value="' + orderDetRowData.j_lowerpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/coinpocket.png" />Coin Pocket</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_coinpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_coinpocket",this) class="ipfield" value="' + orderDetRowData.j_coinpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/backvent.png" />Back Vent</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_backvent" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_backvent",this) class="ipfield" value="' + orderDetRowData.j_backvent + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Style</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_liningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningstyle",this) class="ipfield" value="' + orderDetRowData.j_liningstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_liningoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_liningoption",this) class="ipfield" value="' + orderDetRowData.j_liningoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/linning.png" />Lining</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_lining" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_lining",this) class="ipfield" value="' + orderDetRowData.j_lining + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Style</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_shoulderstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderstyle",this) class="ipfield" value="' + orderDetRowData.j_shoulderstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/highsleevehead.png" />Shoulder Padding</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_shoulderpadding" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_shoulderpadding",this) class="ipfield" value="' + orderDetRowData.j_shoulderpadding + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_buttonoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonoption",this) class="ipfield" value="' + orderDetRowData.j_buttonoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/buttonstyle.png" />Button Swatch</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_buttonswatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_buttonswatch",this) class="ipfield" value="' + orderDetRowData.j_buttonswatch + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/lapelbuttonhole.png" />Thread Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_threadoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_threadoption",this) class="ipfield" value="' + orderDetRowData.j_threadoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_elbowpatch" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatch",this) class="ipfield" value="' + orderDetRowData.j_elbowpatch + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/elbow.png" />Elbow Patch Color</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_elbowpatchcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_elbowpatchcolor",this) class="ipfield" value="' + orderDetRowData.j_elbowpatchcolor + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/canvas.png" />Canvas Option</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_canvasoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_canvasoption",this) class="ipfield" value="' + orderDetRowData.j_canvasoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/jacketIcon/chestdart.png" />Suit Color</p>' +
                        '<p class="p2"> ' +
                        '<input id="j_suitcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","j_suitcolor",this) class="ipfield" value="' + orderDetRowData.j_suitcolor + '" />' + '' +
                        '</p></li><li>' +
                        '<p class="p1"><img src="images/pantIcon/pant.png" />Category</p>' +
                        '<p class="p2">' +
                        '<input id="p_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_category",this) class="ipfield" value="' + orderDetRowData.p_category + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/sidepocket.png" />Front Pocket</p>' +
                        '<p class="p2">' +
                        '<input id="p_frontpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_frontpocket",this) class="ipfield" value="' + orderDetRowData.p_frontpocket + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/category.png" />Pleat Style </p>' +
                        '<p class="p2">' +
                        '<input id="p_pleatstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleatstyle",this) class="ipfield" value="' + orderDetRowData.p_pleatstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/watch.png" />Watch Pocket</p>' +
                        '<p class="p2">' +
                        '<input id="p_watchpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_watchpocket",this) class="ipfield" value="' + orderDetRowData.p_watchpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket </p>' +
                        '<p class="p2">' +
                        '<input id="p_backpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocket",this) class="ipfield" value="' + orderDetRowData.p_backpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt</p>' +
                        '<p class="p2">' +
                        '<input id="p_belt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_belt",this) class="ipfield" value="' + orderDetRowData.p_belt + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/category.png" />Pleat</p>' +
                        '<p class="p2">' +
                        '<input id="p_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleat",this) class="ipfield" value="' + orderDetRowData.p_pleat + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt Loop</p>' +
                        '<p class="p2">' +
                        '<input id="p_beltloop" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_beltloop",this) class="ipfield" value="' + orderDetRowData.p_beltloop + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/cuff.png" />Pant Bottom </p>' +
                        '<p class="p2">' +
                        '<input id="p_pantbottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantbottom",this) class="ipfield" value="' + orderDetRowData.p_pantbottom + '" />' +
                        '</p> </li>' +
                        '<li style="border-bottom: 1px solid !important;">' +
                        '<p class="p1"><img src="images/pantIcon/pant.png" />Pant Color</p>' +
                        '<p class="p2">' +
                        '<input id="p_pantcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantcolor",this) class="ipfield" value="' + orderDetRowData.p_pantcolor + '" />' +
                        '</p>' +
                        '</li></ul></div>';
                        measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Back length):" + measurementData.jacket_back_jacket_length + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Back shoulder):" + measurementData.jacket_back_shoulder + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Bicep):" + measurementData.jacket_bicep + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Chest):" + measurementData.jacket_chest + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Front shoulder):" + measurementData.jacket_front_shoulder + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Seat):" + measurementData.jacket_seat + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve left):" + measurementData.jacket_sleeve_left + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Sleeve right):" + measurementData.jacket_sleeve_right + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Jacket(Stomach):" + measurementData.jacket_stomach + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):" + measurementData.pant_back_waist_height + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):" + measurementData.pant_front_waist_height + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Bottom):" + measurementData.pant_bottom + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Calf):" + measurementData.pant_calf + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Knee):" + measurementData.pant_knee + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):" + measurementData.pant_pant_left_outseam + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):" + measurementData.pant_pant_right_outseam + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Seat):" + measurementData.pant_seat + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Thigh):" + measurementData.pant_thigh + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Waist):" + measurementData.pant_waist + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(U-rise):" + measurementData.pant_u_rise + "        </dd>";
                        break;
                    case "Collection":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                        '<li>' +
                        '<p class="p1">Category#</p>' +
                        '<p class="p2"><input readonly type="text" class="ipfield" value="Accessory" />' +
                        '</p></li> ' +
                        '<li>' +
                        '<p class="p1">Rc-Number</p>' +
                        '<p class="p2"><input id="a_rc_no" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_rc_no",this) class="ipfield" value="' + orderDetRowData.a_rc_no + '" />' +
                        '</p></li>' +
                        '<li style="border-bottom: 1px solid !important;">' +
                        '<p class="p1">Specification</p>' +
                        '<p class="p2"><input id="a_specification" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_specification",this) class="ipfield" value="' + orderDetRowData.a_specification + '" />' +
                        '</p></li>' +
                        '</ul> </div>';
                        break;
                    case "Accessories":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                        '<li>' +
                        '<p class="p1">Category#</p>' +
                        '<p class="p2"><input readonly type="text" class="ipfield" value="Accessory" />' +
                        '</p></li> ' +
                        '<li>' +
                        '<p class="p1">Rc-Number</p>' +
                        '<p class="p2"><input id="a_rc_no" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_rc_no",this) class="ipfield" value="' + orderDetRowData.a_rc_no + '" />' +
                        '</p></li>' +
                        '<li style="border-bottom: 1px solid !important;">' +
                        '<p class="p1">Specification</p>' +
                        '<p class="p2"><input id="a_specification" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","a_specification",this) class="ipfield" value="' + orderDetRowData.a_specification + '" />' +
                        '</p></li>' +
                        '</ul> </div>';
                        break;
                    case "Custom Shirt":
                        customizationInformation = '<div class="ddkfxx nextg"><ul><li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" />Category</p>' +
                        '<p class="p2"><input id="s_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_category",this) class="ipfield" value="' + orderDetRowData.s_category + '" />' +
                        '</p></li> ' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar</p>' +
                        '<p class="p2"><input id="s_collar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collar",this) class="ipfield" value="' + orderDetRowData.s_collar + '" />' + '' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar Button</p>' +
                        '<p class="p2"><input id="s_collarbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbutton",this) class="ipfield" value="' + orderDetRowData.s_collarbutton + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/collarbuttondown.png" />Collar Button Down</p>' +
                        '<p class="p2"><input id="s_collarbuttondown" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbuttondown",this) class="ipfield" value="' + orderDetRowData.s_collarbuttondown + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/collarbuttondown.png" />Collar Layer Option</p>' +
                        '<p class="p2"><input id="s_collarlayeroption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarlayeroption",this) class="ipfield" value="' + orderDetRowData.s_collarlayeroption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/collar.png" />Front Collar</p>' +
                        '<p class="p2"><input id="s_frontcollar" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_frontcollar",this) class="ipfield" value="' + orderDetRowData.s_frontcollar + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/collar.png" />Collar Belt</p>' +
                        '<p class="p2"><input id="s_collarbelt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_collarbelt",this) class="ipfield" value="' + orderDetRowData.s_collarbelt + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/cuff.png" />Cuff</p>' +
                        '<p class="p2"><input id="s_cuff" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_cuff",this) class="ipfield" value="' + orderDetRowData.s_cuff + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/cuffwidth.png" />Cuff Width</p>' +
                        '<p class="p2"><input id="s_cuffwidth" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_cuffwidth",this) class="ipfield" value="' + orderDetRowData.s_cuffwidth + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" />Placket</p>' +
                        '<p class="p2"><input id="s_placket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_placket",this) class="ipfield" value="' + orderDetRowData.s_placket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" />Placket Button</p>' +
                        '<p class="p2"><input id="s_placketbutton" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_placketbutton",this) class="ipfield" value="' + orderDetRowData.s_placketbutton + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/pocket.png" />Pocket</p>' +
                        '<p class="p2"><input id="s_pocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_pocket",this) class="ipfield" value="' + orderDetRowData.s_pocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/pleat.png" />Pleat</p>' +
                        '<p class="p2"><input id="s_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_pleat",this) class="ipfield" value="' + orderDetRowData.s_pleat + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/bottom.png" />Bottom</p>' +
                        '<p class="p2"><input id="s_bottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_bottom",this) class="ipfield" value="' + orderDetRowData.s_bottom + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" />Shirt Color</p>' +
                        '<p class="p2"><input id="s_shirtcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_shirtcolor",this) class="ipfield" value="' + orderDetRowData.s_shirtcolor + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" />Button Color</p>' +
                        '<p class="p2"><input id="s_buttoncolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoncolor",this) class="ipfield" value="' + orderDetRowData.s_buttoncolor + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Buttoning Style</p>' +
                        '<p class="p2"><input id="s_buttoningstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoningstyle",this) class="ipfield" value="' + orderDetRowData.s_buttoningstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Button Cut Thread</p>' +
                        '<p class="p2"><input id="s_buttoncutthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_buttoncutthread",this) class="ipfield" value="' + orderDetRowData.s_buttoncutthread + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/buttonhole.png" />Hole Stich Thread</p>' +
                        '<p class="p2"><input id="s_holestichthread" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_holestichthread",this) class="ipfield" value="' + orderDetRowData.s_holestichthread + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" />Mono Gram Text</p>' +
                        '<p class="p2"><input id="s_monogramtext" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramtext",this) class="ipfield" value="' + orderDetRowData.s_monogramtext + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" alt="font" />Mono Gram Font</p>' +
                        '<p class="p2"><input id="s_monogramfont" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramfont",this) class="ipfield" value="' + orderDetRowData.s_monogramfont + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Color" />Mono Gram Color</p>' +
                        '<p class="p2"><input id="s_monogramcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramcolor",this) class="ipfield" value="' + orderDetRowData.s_monogramcolor + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Position" />Mono Gram Position</p>' +
                        '<p class="p2"><input id="s_monogramposition" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramposition",this) class="ipfield" value="' + orderDetRowData.s_monogramposition + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" alt="Option" />Mono Gram Option</p>' +
                        '<p class="p2"><input id="s_monogramoption" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_monogramoption",this) class="ipfield" value="' + orderDetRowData.s_monogramoption + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast Fabric</p>' +
                        '<p class="p2"><input id="s_contrastfabric" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrastfabric",this) class="ipfield" value="' + orderDetRowData.s_contrastfabric + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast</p>' +
                        '<p class="p2"><input id="s_contrast" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrast",this) class="ipfield" value="' + orderDetRowData.s_contrast + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/shirt.png" alt="" />Contrast Position</p>' +
                        '<p class="p2"><input id="s_contrastposition" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_contrastposition",this) class="ipfield" value="' + orderDetRowData.s_contrastposition + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/shirtIcon/cuffwidth.png" alt="" />Sleeve</p>' +
                        '<p class="p2"><input id="s_sleeve" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_sleeve",this) class="ipfield" value="' + orderDetRowData.s_sleeve + '" />' +
                        '</p> </li>' +
                        '<li style="border-bottom: 1px solid !important;">' +
                        '<p class="p1"><img src="images/shirtIcon/placket.png" alt="" />Shoulder</p>' +
                        '<p class="p2"><input id="s_shoulder" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","s_shoulder",this) class="ipfield" value="' + orderDetRowData.s_shoulder + '" />' +
                        '</p></li>' +
                        '</ul> </div>';
                        measurementsitemData = "" +
                        "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Back shoulder):" + measurementData.shirt_back_shoulder + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Back Jacket Length):" + measurementData.shirt_back_jacket_length + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Back waist height):" + measurementData.shirt_back_waist_height + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Bicep):" + measurementData.shirt_bicep + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Bottom):" + measurementData.shirt_bottom + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Calf):" + measurementData.shirt_calf + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Chest):" + measurementData.shirt_chest + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Collar):" + measurementData.shirt_collar + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Front shoulder):" + measurementData.shirt_front_shoulder + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Front waist height):" + measurementData.shirt_front_waist_height + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Front waist length):" + measurementData.shirt_front_waist_length + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Knee):" + measurementData.shirt_knee + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Nape to waist):" + measurementData.shirt_nape_to_waist + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Left-outsteam):" + measurementData.shirt_pant_left_outseam + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Right-outsteam):" + measurementData.shirt_pant_right_outseam + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Seat):" + measurementData.shirt_seat + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Sleeve left):" + measurementData.shirt_sleeve_left + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Sleeve right):" + measurementData.shirt_sleeve_right + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Shirt(Stomach):" + measurementData.shirt_stomach + "        </dd>";
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Thigh):" + measurementData.shirt_thigh + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(U-rise):" + measurementData.shirt_u_rise + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Shirt(Wrist):" + measurementData.shirt_wrist + "        </dd>";
                        break;
                    case "Custom Pant":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/pant.png" />Category</p>' +
                        '<p class="p2">' +
                        '<input id="p_category" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_category",this) class="ipfield" value="' + orderDetRowData.p_category + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/sidepocket.png" />Front Pocket</p>' +
                        '<p class="p2">' +
                        '<input id="p_frontpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_frontpocket",this) class="ipfield" value="' + orderDetRowData.p_frontpocket + '" />' +
                        '</p></li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/category.png" />Pleat Style </p>' +
                        '<p class="p2">' +
                        '<input id="p_pleatstyle" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleatstyle",this) class="ipfield" value="' + orderDetRowData.p_pleatstyle + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/watch.png" />Watch Pocket</p>' +
                        '<p class="p2">' +
                        '<input id="p_watchpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_watchpocket",this) class="ipfield" value="' + orderDetRowData.p_watchpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/backpocket.png" />Back Pocket </p>' +
                        '<p class="p2">' +
                        '<input id="p_backpocket" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_backpocket",this) class="ipfield" value="' + orderDetRowData.p_backpocket + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt</p>' +
                        '<p class="p2">' +
                        '<input id="p_belt" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_belt",this) class="ipfield" value="' + orderDetRowData.p_belt + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/category.png" />Pleat</p>' +
                        '<p class="p2">' +
                        '<input id="p_pleat" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pleat",this) class="ipfield" value="' + orderDetRowData.p_pleat + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/beltloop.png" />Belt Loop</p>' +
                        '<p class="p2">' +
                        '<input id="p_beltloop" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_beltloop",this) class="ipfield" value="' + orderDetRowData.p_beltloop + '" />' +
                        '</p> </li>' +
                        '<li>' +
                        '<p class="p1"><img src="images/pantIcon/cuff.png" />Pant Bottom </p>' +
                        '<p class="p2">' +
                        '<input id="p_pantbottom" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantbottom",this) class="ipfield" value="' + orderDetRowData.p_pantbottom + '" />' +
                        '</p> </li>' +
                        '<li style="border-bottom: 1px solid !important;">' +
                        '<p class="p1"><img src="images/pantIcon/pant.png" />Pant Color</p>' +
                        '<p class="p2">' +
                        '<input id="p_pantcolor" type="text" onchange=updateData("' + orderDetRowData.det_id + '","wp_orders_detail","p_pantcolor",this) class="ipfield" value="' + orderDetRowData.p_pantcolor + '" />' +
                        '</p>' +
                        '</li></ul> </div>';
                        measurementsitemData = "<dl></dl> <dl> <dt> Finished Measurements<span>(Unit:" + measurementData.unit_type + ")</span></dt>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Back length):" + measurementData.pant_back_jacket_length + "</dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Back shoulder):" + measurementData.pant_back_shoulder + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Back Waist Height):" + measurementData.pant_back_waist_height + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Front Waist Height):" + measurementData.pant_front_waist_height + "</dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Bicep):" + measurementData.pant_bicep + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Bottom):" + measurementData.pant_bottom + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Calf):" + measurementData.pant_calf + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Chest):" + measurementData.pant_chest + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Collar):" + measurementData.pant_collar + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Front shoulder):" + measurementData.pant_front_shoulder + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Front waist length):" + measurementData.pant_front_waist_length + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Knee):" + measurementData.pant_knee + "</dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Nape to waist):" + measurementData.pant_nape_to_waist + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Left-outsteam):" + measurementData.pant_pant_left_outseam + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Right-outsteam):" + measurementData.pant_pant_right_outseam + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Seat):" + measurementData.pant_seat + "</dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve left):" + measurementData.pant_sleeve_left + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Sleeve right):" + measurementData.pant_sleeve_right + "        </dd>" +
                        //"<dd title= \"Seat(Accessory):\"> Pant(Stomach):" + measurementData.pant_stomach + "        </dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Thigh):" + measurementData.pant_thigh + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(Waist):" + measurementData.pant_waist + "</dd>" +
                        "<dd title= \"Seat(Accessory):\"> Pant(U-rise):" + measurementData.pant_u_rise + "        </dd>";
                        //"<dd title= \"Seat(Accessory):\"> Pant(Wrist):" + measurementData.pant_wrist + "        </dd>";
                        break;
                    case "Custom Overcoat":
                        customizationInformation = '<div class="ddkfxx nextg">  <ul> ' +
                        '<li><p class="p1">  category</p> <p class="p2" title="' + orderDetRowData.o_category + '"> ' + orderDetRowData.o_category + '    </p></li> ' +
                        '<li><p class="p1">  canvasstyle</p> <p class="p2" title="' + orderDetRowData.o_canvasstyle + '"> ' + orderDetRowData.o_canvasstyle + '    </p></li>' +
                        '<li><p class="p1">  lapelstyle</p> <p class="p2" title="' + orderDetRowData.o_lapelstyle + '"> ' + orderDetRowData.o_lapelstyle + '    </p> </li>' +
                        '<li><p class="p1">  pocketstype</p> <p class="p2" title="' + orderDetRowData.o_pocketstype + '"> ' + orderDetRowData.o_pocketstype + '    </p> </li>' +
                        '<li><p class="p1">  buttonstyle</p> <p class="p2" title="' + orderDetRowData.o_buttonstyle + '"> ' + orderDetRowData.o_buttonstyle + '    </p> </li>' +
                        '<li><p class="p1">  sweatpadstyle</p> <p class="p2" title="' + orderDetRowData.o_sweatpadstyle + '"> ' + orderDetRowData.o_sweatpadstyle + '    </p> </li>' +
                        '<li><p class="p1">  elbowpadstyle</p> <p class="p2" title="' + orderDetRowData.o_elbowpadstyle + '"> ' + orderDetRowData.o_elbowpadstyle + '    </p> </li>' +
                        '<li><p class="p1">  shoulderstyle</p> <p class="p2" title="' + orderDetRowData.o_shoulderstyle + '"> ' + orderDetRowData.o_shoulderstyle + '    </p> </li>' +
                        '<li><p class="p1">  shoulderpatchstyle</p> <p class="p2" title="' + orderDetRowData.o_shoulderpatchstyle + '"> ' + orderDetRowData.o_shoulderpatchstyle + '    </p> </li>' +
                        '<li><p class="p1">  shouldertabstyle</p> <p class="p2" title="' + orderDetRowData.o_shouldertabstyle + '"> ' + orderDetRowData.o_shouldertabstyle + '    </p> </li>' +
                        '<li><p class="p1">  sleeveslitstyle</p> <p class="p2" title="' + orderDetRowData.o_sleeveslitstyle + '"> ' + orderDetRowData.o_sleeveslitstyle + '    </p> </li>' +
                        '<li><p class="p1">  sleevebuttonstyle</p> <p class="p2" title="' + orderDetRowData.o_sleevebuttonstyle + '"> ' + orderDetRowData.o_sleevebuttonstyle + '    </p> </li>' +
                        '<li><p class="p1">  backvent</p> <p class="p2" title="' + orderDetRowData.o_backvent + '"> ' + orderDetRowData.o_backvent + '    </p> </li>' +
                        '<li><p class="p1">  stitchstyle</p> <p class="p2" title="' + orderDetRowData.o_stitchstyle + '"> ' + orderDetRowData.o_stitchstyle + '    </p> </li>' +
                        '<li><p class="p1">  backsideofarmhole</p> <p class="p2" title="' + orderDetRowData.o_backsideofarmhole + '"> ' + orderDetRowData.o_backsideofarmhole + '    </p> </li>' +
                        '<li><p class="p1">  topstichstyle</p> <p class="p2" title="' + orderDetRowData.o_topstichstyle + '"> ' + orderDetRowData.o_topstichstyle + '    </p> </li>' +
                        '<li><p class="p1">  breastpocket</p> <p class="p2" title="' + orderDetRowData.o_breastpocket + '"> ' + orderDetRowData.o_breastpocket + '    </p> </li>' +
                        '<li><p class="p1">  facingstyle</p> <p class="p2" title="' + orderDetRowData.o_facingstyle + '"> ' + orderDetRowData.o_facingstyle + '    </p> </li>' +
                        '<li><p class="p1">  pocketstyle</p> <p class="p2" title="' + orderDetRowData.o_pocketstyle + '"> ' + orderDetRowData.o_pocketstyle + '    </p> </li>' +
                        '<li><p class="p1">  collarstyle</p> <p class="p2" title="' + orderDetRowData.o_collarstyle + '"> ' + orderDetRowData.o_collarstyle + '    </p> </li>' +
                        '<li style="border-bottom: 1px solid !important;"><p class="p1">  liningstyle</p> <p class="p2" title="' + orderDetRowData.o_liningstyle + '"> ' + orderDetRowData.o_liningstyle + '    </p> </li>' +
                        '</ul> </div>';
                        break;
                }
            }

        });
    }

function sendMail(){
    $(".loader").show();
    $('input[name="check_email"]:checked').each(function() {
    var order_id = (this.value);
	var url = "api/supplierProcess.php";
    $.post(url,{"type":"supplierEmail","supplier_email":"null","order_id":order_id}, function (data) {
            window.location="supplier_order.php";
        });
    });
}
function resendMail(order_id){
    $(".loader").show();
	var url = "api/supplierProcess.php";
    $.post(url,{"type":"supplierEmail","supplier_email":"null","order_id":order_id}, function (data) {
            window.location="supplier_order.php";
    });
}


function close_diolog() {
	$(".shadoows").hide();
	$("#send_supplier_data").hide();
}
function delOrder(delid){
    var url = "update_store.php?type=delitemOrder&delid="+delid;
    $.get(url, function (data) {
        var json = $.parseJSON(data)
        {
            var status = json.status;
            if (status == "done") {
                window.location="supplier_order.php";
            }
            else {
                //alert('error');
            }
        }
    });
}
</script>
