<?php
include('header.php');
?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<meta charset="UTF-8">
<style>
    .boxes {
        background: white;
        min-height: 100px;
        border: 1px solid #ddd;
        margin-bottom: 10px;
    }
    .boxes .tabdiv {
        background: #eee none repeat scroll 0 0;
        border: 1px solid;
        font-weight: bold;
        margin-top: 22px;
        padding: 8px;
        text-align: center;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .savebtn {
        letter-spacing: 1px;
        margin-bottom: 18px;
        text-transform: uppercase;
    }
    .err {
        text-align: center;
        color: red;
    }
    table.table.custom {
        border: 1px solid #ddd;
        text-align:center;
        display:inline-block;
        overflow:scroll;
    }
    table.table.custom th {
        border-right:1px solid #ddd;
        text-align:center
    }
    table.table.custom td {
        border-right:1px solid #ddd;
        font-size: 14px;
    }
    .custom-input{
        width: 100%;
        border: 0;
        text-align: center;
        outline:0;
        background: transparent;
    }
    .table th{
        text-align:center;
    }
    .allowtable{
        height:490px;
        overflow-y:scroll;
        padding:0;
    }
    .size_field
    {
        width: 80%;
        text-align: right;
    }
    .matchingTabs:hover{
        cursor: pointer;
    }
</style>
<!-- page content -->
<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>
<div class="right_col" role="main">
    <div class="row">
        <ul class="nav nav-tabs">
            <li role="presentation" id="jacketallow" class="matchingTabs tabs active" onclick="switchMatching('jacket')"><a >Jacket Specification</a></li>
            <li role="presentation" class="tabs matchingTabs" id="pantallow" onclick="switchMatching('pant')"><a>Pant Specification</a></li>
            <li role="presentation" class="tabs matchingTabs" id="vestallow" onclick="switchMatching('vest')"><a>Vest Specification</a></li>
            <li role="presentation" class="tabs matchingTabs" id="shirtallow" onclick="switchMatching('shirt')"><a>Shirt Specification</a></li>
        </ul>
    </div>
    <div class="row">
        <div class="col-md-12 allowtable" id="shirt_table" style="display:block;overflow-x:scroll">
        </div>
        <div class="col-md-12 allowtable" id="jacket_table" style="display:none;overflow-x:scroll">
        </div>
        <div class="col-md-12 allowtable" id="vest_table" style="display:none">
        </div>
        <div class="col-md-12 allowtable" id="pant_table" style="display:none">
        </div>
        <div class="col-md-12 allowtable" id="bodystyle_table" style="display:none">
        </div>
        <div class="col-md-12 allowtable" id="pleatstyle_table" style="display:none">
        </div>
        <div class="col-md-12 allowtable" id="minmax_table" style="display:none">
        </div>
    </div>
</div>
<?php
include('footer.php');
?>
<script>
    function switchMatching(tab){
        $(".allowtable").css("display","none");
        $("#"+tab+"_table").css("display","block");
        $(".tabs").removeClass("active");
        $("#"+tab+"allow").addClass("active");
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "getMatchingData", "size_type": tab}, function (data) {
                var status = data.Status;
                if (status == "Success") {
                    var sizeMatchingData = data.sizeMatchingData;
                    var table = '<table class="table table-bordered" style="width:100%;overflow-x:scroll">';
                    var tr = "";
                        for (var j = 0; j < sizeMatchingData.length; j++) {
                            if(sizeMatchingData[j].title == "jacket_specification"){
                                tr = tr +'<tr><td>S.no</td><td>Jacket Specification</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+'>R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                                    '</tr>';
                            }
                            else if(sizeMatchingData[j].title == "pant_specification"){
                                tr = tr +'<tr><td>S.no</td><td>Pant Specification</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                                    '</tr>';
                            }
                            else if(sizeMatchingData[j].title == "vest_specification"){
                                tr = tr +'<tr><td>S.no</td><td>Vest Specification</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                                    '</tr>';
                            }
                            else if(sizeMatchingData[j].title == "shirt_specification"){
                                tr = tr +'<tr><td>S.no</td><td>Shirt Specification</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                                    '</tr>';
                            }
                            else
                            {
                                tr = tr +'<tr><td>'+j+'</td><td>'+sizeMatchingData[j].title+'</td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+'></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' ></td>' +
                                    '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' ></td>' +
                                    '</tr>';
                            }
                        }
                    $("#"+tab+"_table").html(table+tr+"</table>");
                } else {
                    showMessage(data.Message, "red");
                }
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
    }
    switchMatching("jacket");
    function updateValue(val,id,colm_val){
        var value = val.value;
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
            return false;
        }else {
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updateSizeMatching", "row_id": id, "value": value,"colm_val":colm_val},function (data) {
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
                if(data.Status == "Success")
                {
                    showMessage(data.Message,'green');
                }
                else
                {
                    showMessage(data.Message,'red');
                }
            });
        }
    }
    function getbodystyledata(){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getBodyStyleData"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var styleData = data.styleData;
                var dataShow = "";
                for(var i=0;i<styleData.length;i++){
                    dataShow += "<tr><td>"+(i+1)+"</td><td>"+styleData[i].option_name+"</td><td><input type='text' " +
                        "id='bs_"+styleData[i].row_id+"' value='"+styleData[i].back_shoulder+"' class='custom-input' " +
                        "onchange=updateBodyStyle(this,'back_shoulder')></td><td><input type='text' id='fs_"+styleData[i].row_id+
                        "' class='custom-input' onchange=updateBodyStyle(this,'front_shoulder') " +
                        "value='"+styleData[i].front_shoulder+"'></td><td><input type='text' id='co_"+styleData[i].row_id+"' " +
                        "class='custom-input' value='"+styleData[i].combination+"' onchange=updateBodyStyle(this,'combination')>"+
                        "</td></tr>";
                }
                $("#body_style_tbody").html(dataShow);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function getpleatstyledata(){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "getpleatstyledata"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var pleatData = data.pleatData;
                var dataShow = "";
                for(var i=0;i<pleatData.length;i++){
                    if(i == 0) {
                        dataShow += "<tr><th>S.No</th><th>" + pleatData[0].dress_style + "</th><th>"+ pleatData[0].no_pleat+
                            "</th><th>"+pleatData[0].onepleatlesstha2cm+"</th><th>"+pleatData[0].onpleatbetween2cmand25cm+
                            "</th><th>"+pleatData[0].twopleatlessthan25cm+"</th><th>"+pleatData[0].threepleatlessthan25cm+
                            "</th><th>"+pleatData[0].urise+"</th></tr>";
                    }else{
                        dataShow += "<tr><td>"+(i)+"</td><td>" + pleatData[i].dress_style + "</td><td><input type='text'"+
                            "class='custom-input' id='"+pleatData[i].row_id+"_no_pleat' onchange=updatePleatData(this) " +
                            "value='"+ pleatData[i].no_pleat+"' /></td><td><input type='text' class='custom-input' id='"+
                            pleatData[i].row_id+"_onepleatlesstha2cm' onchange=updatePleatData(this) value='"+
                            pleatData[i].onepleatlesstha2cm+"' /></td><td><input type='text' class='custom-input' id='"+
                            pleatData[i].row_id+"_onpleatbetween2cmand25cm' onchange=updatePleatData(this) value='"+
                            pleatData[i].onpleatbetween2cmand25cm+"' /></td><td><input type='text' class='custom-input' id='"+
                            pleatData[i].row_id+"_twopleatlessthan25cm' onchange=updatePleatData(this) value='"+
                            pleatData[i].twopleatlessthan25cm+"' /></td><td><input type='text' class='custom-input' id='"+
                            pleatData[i].row_id+"_threepleatlessthan25cm' onchange=updatePleatData(this) value='"+
                            pleatData[i].threepleatlessthan25cm+"' /></td><td><input type='text' class='custom-input' id='"+
                            pleatData[i].row_id+"_urise' onchange=updatePleatData(this) value='"+
                            pleatData[i].urise+"' /></td></tr>";
                    }
                }
                $("#pleat_style_tbody").html(dataShow);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function minmaxdata(){
        var url = "../api/registerUser.php";
        $.post(url, {"type": "minmaxdata"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var minmaxData = data.minmaxData;
                var dataShow = "";
                for(var i=0;i<minmaxData.length;i++){
                    var param = (minmaxData[i].parameter).split("_");
                    dataShow += "<tr><td>"+(i+1)+"</td><td style='text-transform:capitalize'>"+param[0]+" "+param[1]+
                        "</td><td><input type='text' class='custom-input' id='min_val-"+minmaxData[i].row_id+"' " +
                        "onchange=updateminmax(this) value='"+minmaxData[i].min_val+"' /></td><td>" +
                        "<input type='text' class='custom-input' id='max_val-"+minmaxData[i].row_id+"' " +
                        "onchange=updateminmax(this) value='"+minmaxData[i].max_val+"' /></td></tr>";
                }
                $("#minmax_tbody").html(dataShow);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function updateBodyStyle(obj,option_name){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            id = id.split("_")[1];
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updateBodyStyle", "row_id":id,"value":value,"option_name":option_name},function(){
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }function updatePleatData(obj){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            var option_name = id.split("_")[1];
            id = id.split("_")[0];
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updatePleatData", "row_id":id,"value":value,"option_name":option_name},function(){
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }function updateminmax(obj){
        var id = obj.id;
        var value = $("#"+id).val();
        if(value == ""){
            value = 0;
        }
        if(isNaN(value)){
            showMessage("Value Should Be in Numbers Only",'red');
        }else {
            var option_name = id.split("-")[0];
            id = id.split("-")[1];
            $(".loaderdiv").removeClass("loaderhidden");
            $(".loaderdiv").addClass("block");
            var url = "../api/registerUser.php";
            $.post(url, {"type": "updateminmaxData", "row_id":id,"value":value,"option_name":option_name},function(){
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            });
        }
    }
</script>
