<?php
include('header.php');
?>
<style>
    .loader {
        background: #eee none repeat scroll 0 0;
        height: 830px;
        opacity: 0.7;
        position: fixed;
        width: 100%;
        z-index: 2147483647;
        top:0;
    }
    .spinloader {
        left: 34%;
        position: absolute;
        top: 14%;
    }
</style>
<div class="loader" style="display: none">
    <img src="images/slack_load.gif" class="spinloader"/>
</div>
<div class="right_col" role="main">
    <div class="row tile_count"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>All PDF
                    </h2>
                    <!--  <ul class="nav navbar-right panel_toolbox">
                          <!--<li>
                              <form method="post" class="form-inline">
                                  <div class="form-group form-inline">
                                      <input type="file" class="form-control" name="file" id="file"/>
                                  </div>
                                  <input type="button" Value="Go" class="btn btn-primary btn-sm" name="filterButton"
                                         style="margin-top: 5px" onclick="uploadData();"/>
                              </form>
                          </li>-->
                    </ul>-->
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <p class="text-muted font-13 m-b-30">
                        List of Orders Generated
                    </p>
                    <div id="data">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .main-grid-box{height: 288px;
        padding-top: 10px;
        text-align: center;
        margin-left: 5%;
        margin-bottom: 24px;
        border: 1px solid #999;
        width: 28%;
    }
    .pdf-img img {
        margin-bottom: 12px;
    }
    .pdf-title{margin-bottom: 12px;margin-top: 9px;}

</style>
<?php
include('footer.php');
?>
