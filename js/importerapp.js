ImporterApp = function ()
{};
ImporterApp.prototype.Init = function (files)
{
	var myThis = this;
	var top = document.getElementById ('top');
	window.addEventListener ('resize', this.Resize.bind (this), false);
	this.Resize ();
	this.viewer = new ImporterViewer ();
	this.viewer.Init ('test');
	this.ProcessFiles(files);
};
ImporterApp.prototype.Resize = function ()
{
	function SetWidth (elem, value)
	{
		elem.width = 660;
		elem.style.width = value + 'px';
	}
	function SetHeight (elem, value)
	{
		elem.height = 800;
		elem.style.height = value + 'px';
	}
	var canvas = document.getElementById ('test');
	var height = document.body.clientHeight - top.offsetHeight;
	SetHeight (canvas, 0);
	SetWidth (canvas, 0);
	SetHeight (canvas, height);
};
ImporterApp.prototype.JsonLoaded = function (progressBar)
{
	var jsonData = this.viewer.GetJsonData ();
	this.meshVisibility = {};
	this.Generate (progressBar);
};
ImporterApp.prototype.Generate = function (progressBar)
{
	function ShowMeshes(importerApp,merge)
	{
		var menu  = document.getElementById("loader");
		importerApp.inGenerate = true;
		var environment =
		{
			onStart: function (taskCount)
			{
				menu.style.display="block";
			},
			onProgress: function (currentTask) {
				menu.style.display="block";
			},
			onFinish: function () {
				menu.style.display="none";
			}
		};
		if (merge)
		{
			var jsonData = importerApp.viewer.GetJsonData ();
			importerApp.viewer.SetJsonData (JSM.MergeJsonDataMeshes (jsonData));
		}
		importerApp.viewer.ShowAllMeshes (environment);
	}
	var jsonData = this.viewer.GetJsonData ();
	if (jsonData.materials.length === 0 || jsonData.meshes.length === 0)
	{
		this.GenerateError ('Failed to open file. Maybe something is wrong with your Obj file.');
		return;
	}
	var myThis = this;
	ShowMeshes(myThis, progressBar, false);
};
ImporterApp.prototype.ProcessFiles = function (fileList)
{
	if (this.inGenerate)
	{
		return;
	}
	if (fileList.length === 0)
	{
		return;
	}
	var myThis = this;
	JSM.ConvertFileListToJsonData (fileList,
	{
		onError : function ()
		{
			myThis.GenerateError ('No readable file found. You can open 3ds, obj and stl files.');
			return;
		},
		onReady : function (fileNames, jsonData)
		{
			myThis.fileNames = fileNames;
			myThis.viewer.SetJsonData(jsonData);
			var menu = document.getElementById ('loader');
			menu.style.display="block";
			myThis.JsonLoaded ("menu");
		}
	});
};
ImporterApp.prototype.FileSelected = function (event)
{
	event.stopPropagation ();
	event.preventDefault ();
    this.ProcessFiles (event.target.files);
};
// TODO: Use from JSModeler
var LoadJsonFile = function (fileName, onReady)
{
	var request = new XMLHttpRequest ();
	request.overrideMimeType ('application/json');
	request.open ('GET', fileName, true);
	request.onreadystatechange = function () {
		if (request.readyState == 4) {
			var jsonData = JSON.stringify({'files':['three4_gown_bdg11_3.obj']});
			var obj = $.parseJSON(jsonData);
			onReady (obj);
		}
	};
	request.send (fileName);
};