<?php
include('header.php');
?>
<style>
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: block;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }
    .main-grid-box {
        height: 300px;
        text-align: center;
        padding: 10px 10px;
        border: 1px solid;
        margin: 10px 5px;
    }

    #galleryBox{
        max-height: 600px;
        overflow-y: auto;
    }
    .delico{
        position: absolute;
        top: 10px;
        right: 20px;
        color: #000;
        font-size: 15px;
        background: #fff;
        border-radius: 50%;
        text-align: center;
        height: 20px;
        width: 20px;
        padding-top: 2px;
        cursor: pointer;
    }
    .imagetitle {
        font-size: 15px;
        color: #fff;
        font-weight: bold;
        background: rgba(0,0,0,0.8);
        padding: 3px;
    }
    .smallicon{
        font-size:18px;
        margin-right: 5px;
    }
    .evelist{
        margin-top:10px;
        font-size:18px;
        border-bottom:1px solid #eee;
        cursor:pointer;
    }
    .oldeventimg{
        height:100px;
        width:125px;
    }
    .oldgalleryimg{
        height:100px;
        width:125px;
    }
    .old_eve_img{
        position: absolute;
        top: 22px;
        height: 100px;
        width: 125px;
        opacity: 0;
    }
    .hide_now {
        display:none;
    }
    .percentprog {
        color: green;
        text-align: center;
        font-size: 17px;
        font-weight: bold;
        margin-top: 170px;
    }
    .ext-img {
        /*image-orientation: from-image;*/
        max-width: 100%;
        max-height: 95%;
    }
    .main-grid-box {
        height: 275px;
        text-align: center;
        padding: 10px 10px;
        border: 1px solid;
        margin: 10px 5px;
    }
</style>
<link href = "js/jquery-ui.css" rel = "stylesheet">
<script src="js/jquery-ui.js"></script>

<div class="loaderdiv loaderhidden">
    <img src="images/preloader.gif"/>
</div>

<!--  page content  -->
<div class="right_col" role="main">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Add Partner Information<small></small></h2>
                    <ul class="nav navbar-right panel_toolbox">
                        <li>
                            <div class="form-group form-inline">
                                <input type="button" class="btn btn-danger btn-sm" onclick="addEvent()" value=" + Add Partner" />
                            </div>
                        </li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <!--body part start here-->
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12" id="galleryBox" style="display:none">
            <button class="btn btn-danger btn-sm backbtn" onclick="showCategories()"><i class="fa fa-caret-left"></i> Back</button>
            <div class="col-md-12 col-sm-12 col-xs-12" id="galleryBox2"></div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12" id="eventBox"></div>
    </div>
    <!--body part end here-->
</div>
<!-- /page content -->
<?php
include('footer.php');
?>
<!-- script -->
<script>
    var idOrder = [];
    var idOrderAll = [];
    var oldEventImagechanged = "no";
    var oldGalleryImagechanged = "no";
    var currentCategory = "all";
    var gall_cat = "out";
    function getCategoryData(category) {
        currentCategory = category;
        gall_cat = "in";
        var url = "api/partnerProcess.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url, {"type": "getPartner"}, function (data) {
            var status = data.Status;
            var partnerData = data.data;
            if (status == "Success") {
                var partnerBox = "";
                if (category != "all") {
                    for (var a = 0; a < partnerData.length; a++) {
                        idOrderAll.push(partnerData[a].id);
                        if (unescape(category) === partnerData[a].category) {
                            partnerBox = partnerBox + '<div id="' + partnerData[a].sort_order + '" ' +
                            'name="' + partnerData[a].id + '" class="col-sm-2 main-grid-box sortId_' +
                            partnerData[a].sort_order + '"><img class="pdf-imgr ext-img" src="images/trans.png" style="background-image: url(api/Files/images/partner/'+partnerData[a].fairLogo+');background-size:100% auto;background-position: center center;background-repeat: no-repeat;height: 220px;width: 100%;" />'+
                            '<div class="action-point"><i class="fa fa-trash delico" ' +
                            'onclick=confirmDelete("'+partnerData[a].id+'","'+partnerData[a].fairLogo+'") >'+
                            '</i><i class="fa fa-edit delico" style="right:50px" ' +
                            'onclick=editPartner("'+partnerData[a].id+'","'+escape(partnerData[a].fairTitle)+'","'+escape(partnerData[a].fairLogo)+'","'+escape(partnerData[a].fairLink)+'")>' +
                            '</i></div><p class="imagetitle">'+(decodeURIComponent(partnerData[a].fairTitle).substr(0,15)+"...")+'</p></div>';
                        }else{
                            partnerBox = partnerBox + '<div id="'+partnerData[a].sort_order+'" name="'+partnerData[a].id+
                            '" class="col-sm-2 main-grid-box hide_now sortId_'+partnerData[a].sort_order+'">' +
                            '<img class="pdf-imgr ext-img" src="images/trans.png" style="background-image: url(api/Files/images/partner/'+partnerData[a].fairLogo+');background-size:100% auto;background-position: center center;background-repeat: no-repeat;height: 220px;width: 100%;" /></div>';
                        }
                    }
                } else {
                    for (var a = 0; a < partnerData.length; a++) {
                        idOrder.push(partnerData[a].id);
                        partnerBox = partnerBox + '<div id="'+partnerData[a].sort_order + '" name="' +
                        partnerData[a].id + '" class="col-sm-2 main-grid-box sortId_' + partnerData[a].sort_order +
                        '" ><img class="pdf-imgr ext-img" src="images/trans.png" style="background-image: url(api/Files/images/partner/'+partnerData[a].fairLogo+');background-size:100% auto;background-position: center center;background-repeat: no-repeat;height: 220px;width: 100%;" /><div class="action-point"><i class="fa fa-trash delico" ' +
                        'onclick=confirmDelete("'+partnerData[a].id+'","'+partnerData[a].fairLogo+'") >'+
                        '</i><i class="fa fa-edit delico" style="right:50px" onclick=editPartner("' + partnerData[a].id +
                        '","' + escape(partnerData[a].fairTitle) + '","' + escape(partnerData[a].fairLogo) + '","'+escape(partnerData[a].fairLink)+'")>' +
                        '</i></div><p class="imagetitle">' + (decodeURIComponent(partnerData[a].fairTitle).substr(0,15)+"...")+'</p></div>';
                    }
                }
                $("#eventBox").css("display", "none");
                $("#galleryBox").css("display", "block");
                $("#galleryBox2").html(partnerBox);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
            else {
                showMessage(data.Message, "red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Unable to process the request", "red");
        });
    }
    $('#galleryBox2').sortable({
        update: function(event, ui) {
            var sortOrder = $(this).sortable('toArray').toString();
            sortOrder = sortOrder.split(",");
            var updateOrderId = [];
            var updateSortOrder = [];
            for(var a=0; a < sortOrder.length; a++){
                var sortNumber = $(".sortId_"+sortOrder[a]).attr("name");
                sortNumber = parseInt(sortNumber);
                updateOrderId.push(sortNumber);
                updateSortOrder.push(a);
            }
            var url = "api/partnerProcess.php";
            $(".loaderdiv").removeClass("loaderhidden");
            $.post(url,{"type":'updateSortOrder',"sortOrder":updateSortOrder,"idOrder":updateOrderId},function (data) {
                var status = data.Status;
                if(status === "Success") {
//                    window.location.reload();
                    getCategoryData(currentCategory);
                }
            });
        }
    });
    function editGallery(gallery_id,gallery_title,gallery_image){
        gallery_title = unescape(gallery_title);
        gallery_image = unescape(gallery_image);
        $(".modal-title").html("<label style='color: green'>Edit Gallery Image!!!</label>");
        $(".modal-body").html("<p id='message'></p><div class='row'><div class='col-md-6'>" +
            "<div class='form-group'><label>Gallery Image</label><br><img src='api/Files/images/gallery/"+gallery_image+"' " +
            "class='oldgalleryimg' /><input type='file' onchange='readURL(this,"+'"oldgalleryimg"'+")' id='new_gallery_img'" +
            " class='old_eve_img'/></div></div><div class='col-md-6'><label>Image Title</label><input type='text' " +
            "class='form-control' placeholder='Enter Image Title' value='"+gallery_title+"' id='new_title' />" +
            "<div style='clear: both'></div><input type='button' value='Edit Gallery Info' class='btn btn-danger pull-right'" +
            " style='margin-top:20px' onclick=editGalleryNow('"+gallery_id+"') /></div></div>");
        $(".modal-footer").css("display","none");
        $("#myModal").modal("show");
    }
    function editGalleryNow(gallery_id){
        var new_title = $("#new_title").val();
        var new_gallery_img = $("#new_gallery_img").val();
        var data = new FormData();
        if(new_gallery_img !== ""){
            var file_new_gallery_img = document.getElementById('new_gallery_img');
            var new_gallery_image_ext = new_gallery_img.split(".");
            new_gallery_image_ext = new_gallery_image_ext[new_gallery_image_ext.length - 1];
            new_gallery_image_ext = new_gallery_image_ext.toLowerCase();
            if (new_gallery_image_ext == "jpg" || new_gallery_image_ext == "png" || new_gallery_image_ext == "gif" || new_gallery_image_ext == "jpeg") {
                data.append('newgalleryimg', file_new_gallery_img.files[0]);
                oldGalleryImagechanged = "yes";
            }
            else{
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color","red");
                $("#subbtn").attr("disabled",false);
                return false;
            }
        }
        if(new_title !== ""){
            $("#message").html("");
            $("#subbtn").attr("disabled",true);
            $(".loaderdiv").removeClass("loaderhidden");
            data.append('type', 'editGallery');
            data.append('gallery_id', gallery_id);
            data.append('gallery_title', new_title);
            data.append('oldGalleryImagechanged',oldGalleryImagechanged);
            var request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState == 4){
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if(status == "Success"){
                        getCategoryData(currentCategory);
                        $("#myModal").modal("hide");
                    }
                    else{
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                        $("#message").css("color","red");
                        $("#subbtn").attr("disabled",false);
                        return false;
                    }
                }
            };
            request.open('POST', 'api/galleryProcess.php');
            request.send(data);
        }
        else{
            $("#message").html("please fill required fields");
            $("#message").css("color","red");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }



    function confirmDelete(id,path) {
        $(".modal-title").html("<label style='color: green'>Confirm Status !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>Are you sure want to delete this Partner Information</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<label id="message" style="margin-right: 2%;color:red"></label><button type="button" ' +
        'class="btn btn-primary" data-dismiss="modal"> Close</button>'+'<button type="button" class="btn btn-danger" ' +
        'onclick=deleteData("'+id+'","'+path+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function deleteData(id,path) {
        var url ='api/partnerProcess.php';
        $.post(url,{"type":'deletePartner','id':id,"path":path},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                getCategoryData(currentCategory);
            }
            else{
                $("#myModal").modal("hide");
                getCategoryData(currentCategory);
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    function addEvent(){
        var url='api/partnerProcess.php';
        $.post(url,{"type":"getCategories"},function(data){
            var Status = data.Status;
            var categoriesData = "<option value='' selected='selected'>Select Category</option>";
            if(Status === "Success") {
                var categories = data.data;
                for(var i=0;i<categories.length;i++){
                    categoriesData += "<option value='"+categories[i].category_img+"' >"+categories[i].category+"</option>";
                }
            }
            categoriesData += "<option value='addnew'>Add New Category</option>";
            $(".modal-title").html("<label style='color: green'>Add New Partner Info</label>");
            $(".modal-body").html("<div class='row'><div class='col-md-12'><p class='text-center' id='imgError'" +
                " style='color:red'></p><div class='col-md-6 form-group'><label>Select Category</label>" +
                "<select onchange='showaddcategoryfield(this)' id='categoryname' class='form-control' >"+categoriesData+"</select></div>" +
                "<div class='col-md-6 form-group' style='display:none' id='addnewfield' ><label>Add New Category Name</label>" +
                "<input type='text' id='newcategoryname' class='form-control' placeholder='Enter New Category Name'/></div>" +
                "<div class='col-md-6 form-group' style='display:none' id='category_img_div' ><label>Select Category Image</label>" +
                "<input type='file' id='newcategoryimg' class='form-control' /></div><div class='col-md-6 form-group'>" +
                "<label>Select Image</label><input type='file' name='fabricFile[]' multiple id='galleryFile' " +
                "class='form-control'/></div><div class='col-md-6 form-group' id='partner_linkk'>" +
                "<label>Partner Link</label><input type='text' name='partner_link' id='partner_link' " +
                "class='form-control'/></div><div class='col-md-6 form-group' id='partner_namee'>" +
                "<label>Partner Name</label><input type='text' name='partner_name' id='partner_name' " +
                "class='form-control'/></div><div class='col-md-6'><input type='button' class='btn btn-danger pull-right'" +
                " style='margin-top:30px' onclick='addGalleryImage()'" +
                " id='subbtn' value='Submit'/></div></div><p class='percentprog' style='color:green;text-align:center'></div>");
            $(".modal-footer").css('display', 'none');
            $("#myModal").modal("show");
        }).fail(function(){
            alert("Server Error !!! Please Try After Some Time");
        });
    }
    function showaddcategoryfield(obj){
        var selectVal = $("#"+obj.id).val();
        if(selectVal === "addnew"){
            $("#addnewfield").css("display","block");
            $("#category_img_div").css("display","block");
//            $("#partner_linkk").css("display","none");
//            $("#partner_namee").css("display","none");

        }else{
            $("#addnewfield").css("display","none");
            $("#category_img_div").css("display","none");
        }
    }
    function addGalleryImage() {
        var iseventimg = "no";
        var galleryFile = $("#galleryFile").val();
        var eventselection = $("#categoryname :selected").text();
        var eventselectionimg = $("#newcategoryimg").val();
        var partnerlink = $("#partner_link").val();
        var partnername = $("#partner_name").val();
        console.log(galleryFile);
        if(eventselection === "Add New Category") {
            if($("#newcategoryname").val() == "" || $("#newcategoryimg").val() === "" || $("#galleryFile").val() === "" || $("#partner_link").val() === "" || $("#partner_name").val() === ""){
                $("#imgError").html("Please fill all fields");
                return false;
            }
            /*eventselectionimg = $("#newcategoryimg").val();
            newcategorieimg = $("#category_img_div").val();*/
            eventselection = $("#newcategoryname").val();
            eventselectionimg = $("#newcategoryimg").val();
        }
        else if(eventselection === "Select Category"){
            $("#imgError").html("Please Select Category First");
            return false;

        }
        else if(galleryFile === ""){
            $("#imgError").html("Please Select Category File Image");
            return false;
        }
        var data = new FormData();
        $("#imgError").html("");
        $("#subbtn").attr("disabled",true);
        if($("#newcategoryimg").val() !== ""){
            var event_file = $("#newcategoryimg").val();
            var _file_eve = document.getElementById('newcategoryimg');
            var eve_image_ext = event_file.split(".");
            eve_image_ext = eve_image_ext[eve_image_ext.length - 1];
            eve_image_ext = eve_image_ext.toLowerCase();
            if (eve_image_ext === "jpg" || eve_image_ext === "png" || eve_image_ext === "gif" || eve_image_ext === "jpeg") {
                data.append('eventimg', _file_eve.files[0]);
                iseventimg = "yes";
            }
            else{
                $("#imgError").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#subbtn").attr("disabled",false);
                return false;
            }

        }
        var _file = document.getElementById('galleryFile');
        for(var i = 0; i < _file.files.length; ++i){
            data.append('file_[]', _file.files[i]);
        }
        data.append('type',"addGalleryImg");
        data.append('event', eventselection);
        data.append('iseventimg',iseventimg);
        data.append('oldeventimg',eventselectionimg);
        data.append('partnerlink',partnerlink);
        data.append('partnername',partnername);

        var request = new XMLHttpRequest();
        request.onreadystatechange = function(){
            if(request.readyState == 4){
                var response = $.parseJSON(request.response);
                var status = response.Status;
                if(status == "Success")
                {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#imgError").html("Succcess");
                    $("#subbtn").attr("disabled",false);
                    $("#myModal").modal("hide");
                    if(gall_cat == "in"){
                        getCategoryData(currentCategory);
                    }
                    else if(gall_cat == "out"){
                        showCategories();
                    }
                    else{
                        location.reload();
                    }
                }
                else
                {
                    $(".loaderdiv").addClass("loaderhidden");
                    $("#imgError").html(response.Message);
                    $("#subbtn").attr("disabled",false);
                    return false;
                }
            }
        };
        request.upload.addEventListener('progress', function(e){
            $('.percentprog').html(parseInt((e.loaded/e.total)*100) + '% Complete');
        }, false);
        request.open('POST', 'api/partnerProcess.php');
        request.send(data);
    }

    function editPartner(gallery_id,gallery_title,gallery_image,partnerLink){

        gallery_title = unescape(gallery_title);

        gallery_image = unescape(gallery_image);
        partnerLink = unescape(partnerLink);

        $(".modal-title").html("<label style='color: green'>Edit Partner Image!!!</label>");

        $(".modal-body").html("<p id='message'></p><div class='row'><div class='col-md-6'>" +

            "<div class='form-group'><label>Partner Image</label><br><img src='api/Files/images/partner/"+gallery_image+"' " +

            "class='oldgalleryimg' /><input type='file' onchange='readURL(this,"+'"oldgalleryimg"'+")' id='new_gallery_img'" +

            " class='old_eve_img'/></div></div><div class='col-md-6'><label>Partner Name </label><input type='text' " +

            "class='form-control' placeholder='Enter Image Title' value='"+gallery_title+"' id='new_title' /><label>Partner Link</label>" +
             "<div style='clear: both'><input type='text' id='partner_link' value='"+partnerLink+"' class='form-control'/>"+
            "<div style='clear: both'></div> <input type='button' value='Edit Partner Info' class='btn btn-danger pull-right'" +

            " style='margin-top:20px' onclick=editPartnerNow('"+gallery_id+"') /></div></div>");

        $(".modal-footer").css("display","none");

        $("#myModal").modal("show");

    }

    function editPartnerNow(gallery_id){

        var new_title = $("#new_title").val();

        var new_gallery_img = $("#new_gallery_img").val();
        var partner_link = $("#partner_link").val();


        var data = new FormData();

        if(new_gallery_img !== ""){

            var file_new_gallery_img = document.getElementById('new_gallery_img');

            var new_gallery_image_ext = new_gallery_img.split(".");

            new_gallery_image_ext = new_gallery_image_ext[new_gallery_image_ext.length - 1];
            new_gallery_image_ext = new_gallery_image_ext.toLowerCase();

            if (new_gallery_image_ext == "jpg" || new_gallery_image_ext == "png" || new_gallery_image_ext == "gif"|| new_gallery_image_ext == "jpeg") {

                data.append('newgalleryimg', file_new_gallery_img.files[0]);

                oldGalleryImagechanged = "yes";

            }

            else{

                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");

                $("#message").css("color","red");

                $("#subbtn").attr("disabled",false);
                return false;

            }

        }

        if(new_title !== "" || partner_link!=""){

            $("#message").html("");

            $("#subbtn").attr("disabled",true);

            $(".loaderdiv").removeClass("loaderhidden");

            data.append('type', 'editGallery');

            data.append('gallery_id', gallery_id);

            data.append('gallery_title', new_title);
            data.append('partner_link', partner_link);

            data.append('oldGalleryImagechanged',oldGalleryImagechanged);

            var request = new XMLHttpRequest();

            request.onreadystatechange = function(){

                if(request.readyState == 4){

                    var response = $.parseJSON(request.response);

                    var status = response.Status;

                    if(status == "Success"){

                        getCategoryData(currentCategory);

                        $("#myModal").modal("hide");

                    }

                    else{

                        $(".loaderdiv").addClass("loaderhidden");

                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");

                        $("#message").css("color","red");

                        $("#subbtn").attr("disabled",false);

                        return false;

                    }

                }

            };

            request.open('POST', 'api/partnerProcess.php');

            request.send(data);

        }

        else{

            $("#message").html("please fill required fields");

            $("#message").css("color","red");

            $(".loaderdiv").addClass("loaderhidden");

        }

    }
    $('#eventBox').sortable({
        update: function(event, ui) {
            var sortOrder = $(this).sortable('toArray').toString();
            sortOrder = sortOrder.split(",");
            var updateOrderId = [];
            var updateSortOrder = [];
            for(var a=0; a < sortOrder.length; a++){
                var sortNumber = $(".events_"+sortOrder[a]).attr("name");
                updateOrderId.push(sortNumber);
                updateSortOrder.push(a);
            }
            //alert(JSON.stringify(updateOrderId)+ " => " +updateSortOrder);

            var url = "api/partnerProcess.php";
            $(".loaderdiv").removeClass("loaderhidden");
            $.post(url,{"type":'updateCatSortOrder',"sortOrder":updateSortOrder,"idOrder":updateOrderId},function (data) {
                var status = data.Status;
                if(status === "Success") {
                    $(".loaderdiv").addClass("loaderhidden");
//                    window.location.reload();
                    showEvent();
                }
            });
        }
    });

    function showCategories(){
        gall_cat = "out";
        var url = "api/partnerProcess.php";
        $.post(url,{"type":"getCategories"},function(data) {
            var Status = data.Status;
            if (Status === "Success") {
                var categories = data.data;
                var categoriesData = "";
                for (var i = 0; i < categories.length; i++) {
                    categoriesData += '<div id="'+categories[i].sort_order_cat+'" name="'+categories[i].category+'" ' +
                        'class="col-sm-2 main-grid-box events_'+categories[i].sort_order_cat+'"  style="cursor:pointer" >' +
                        '<img src="images/trans.png" style="background-image: url(api/Files/images/partner/'+categories[i].category_img+');background-size:100% auto;background-position: center center;background-repeat: no-repeat;height: 220px;width: 100%;"  class="pdf-imgr ext-img" ' +
                        'ondblclick=getCategoryData("'+escape(categories[i].category)+'");  />' +
                    '<i class="fa fa-trash delico" onclick=deleteCategory("'+escape(categories[i].category)+'")>' +
                    '</i><i class="fa fa-edit delico" style="right:50px" onclick=editCategory("'+escape(categories[i].category)+
                    '","'+categories[i].category_img+'")></i><p class="imagetitle">'+
                        (decodeURIComponent(categories[i].category)).substr(0,15)+"..."+'</p></div>';
                }
                /*categoriesData += '<div class="col-sm-3 main-grid-box" onclick=getCategoryData("all") ' +
                'style="border:1px solid #eee;cursor:pointer"><div class="pdf-imgr ext-img" style="text-align: ' +
                'center;padding-top: 75px;font-size: 24px;text-decoration: underline">All Images <br> Click To View</div></div>';*/

                $("#galleryBox").css("display","none");
                $("#eventBox").css("display","block");
                $("#eventBox").html(categoriesData);
            }else{
                $("#galleryBox").css("display","none");
                $("#eventBox").css("display","block");
                $("#eventBox").html("<p class='text-center' style='font-size:22px;margin-top:50px'>No Partner Category Found</p>");
            }
        });
    }
    function editCategory(old_event,oldimg){
        old_event = unescape(old_event);
        $(".modal-title").html("<label style='color: green'>Edit Category!!!</label>");
        $(".modal-body").html("<p id='message'></p><div class='row'><div class='col-md-6'><div class='form-group'>" +
        "<label>Category Image</label><br><img src='api/Files/images/partner/"+oldimg+"' class='oldeventimg' />" +
        "<input type='file' onchange='readURL(this,"+'"oldeventimg"'+")' id='new_event_img' class='old_eve_img'/>" +
        "</div></div><div class='col-md-6'><label>Event Name</label><input type='text' class='form-control' " +
        "placeholder='Enter Event Name' value='"+old_event+"' id='new_event' /><div style='clear: both'></div>" +
        "<input type='button' value='Edit Category Information' class='btn btn-danger pull-right' style='margin-top:20px'" +
        " onclick=editCategoryNow('"+escape(old_event)+"','"+oldimg+"') /></div></div>");
        $(".modal-footer").css("display","none");
        $("#myModal").modal("show");
    }
    function editCategoryNow(old_category,oldimg){
        var new_event = $("#new_event").val();
        var new_eve_img = $("#new_event_img").val();
        var data = new FormData();
        if(new_eve_img !== ""){
            var new_event_file = $("#new_event_img").val();
            var new_file_eve = document.getElementById('new_event_img');
            var new_eve_image_ext = new_event_file.split(".");
            new_eve_image_ext = new_eve_image_ext[new_eve_image_ext.length - 1];
            new_eve_image_ext = new_eve_image_ext.toLowerCase();
            if (new_eve_image_ext == "jpg" || new_eve_image_ext == "png" || new_eve_image_ext == "gif" || new_eve_image_ext == "jpeg") {
                data.append('newcatimg', new_file_eve.files[0]);
                oldEventImagechanged = "yes";
            }
            else{
                $("#message").html("Image File Should Be JPG, PNG, GIF Formats Only");
                $("#message").css("color","red");
                $("#subbtn").attr("disabled",false);
                return false;
            }
        }
        if(new_event !== ""){
            $("#message").html("");
            $("#subbtn").attr("disabled",true);
            $(".loaderdiv").removeClass("loaderhidden");
            data.append('type', 'editCategory');
            data.append('old_category', unescape(old_category));
            data.append('new_category', new_event);
            data.append('oldcategoryImagechanged',oldEventImagechanged);
            data.append('oldcategoryImage',oldimg);
            var request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState == 4){
                    var response = $.parseJSON(request.response);
                    var status = response.Status;
                    if(status == "Success"){
                        showCategories();
                        $("#myModal").modal("hide");
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#subbtn").attr("disabled",false);
                    }
                    else{
                        $(".loaderdiv").addClass("loaderhidden");
                        $("#message").html("Something Went Wrong !!! Please do the Same Littlebit Later....");
                        $("#message").css("color","red");
                        $("#subbtn").attr("disabled",false);
                        return false;
                    }
                }
            };
            request.open('POST','api/partnerProcess.php');
            request.send(data);
        }
        else{
            $("#message").html("please fill required fields");
            $("#message").css("color","red");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    function deleteCategory(category){
        category = unescape(category);
        $(".modal-title").html("<label style='color: green'>Confirm Delete !!!</label>");
        $(".modal-body").html("<label style='font-family: monospace'>This Event Contain Images!!!. Are you sure you want " +
        "to delete "+category+" Category and Images Also</label>");
        $(".modal-footer").css("display","block");
        $(".modal-footer").html('<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>' +
        '<button type="button" class="btn btn-danger" onclick=deleteCategoryNow("'+escape(category)+'")>Delete</button>');
        $("#myModal").modal("show");
    }
    function deleteCategoryNow(category) {
        var url ='api/partnerProcess.php';
        $.post(url,{"type":'deleteCategory','category':unescape(category)},function (data) {
            var status = data.Status;
            var message = data.Message;
            if(status === 'Success') {
                $("#myModal").modal("hide");
                showCategories(currentCategory)
            }
            else{
                $("#myModal").modal("hide");
            }
            $(".modal-backdrop").hide();
        }).fail(function () {
            $("#myModal").modal("hide");
            showMessage("Unable to process the request","red");
        });
        $(".modal-backdrop").hide();
    }
    function readURL(input,targetClass) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.'+targetClass).attr('src', e.target.result);
                if(targetClass == "oldgalleryimg"){
                    oldGalleryImagechanged = "yes";
                }else{
                    oldEventImagechanged = "yes";
                }
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    showCategories();
</script>
