<?php
include("header.php");
include("admin/webserver/database/db.php");
?>
<div class="banner top-assessoricetopbanner">
	<div class="">
	</div>
</div>
	<!-- //Slider -->
</div>
<style>
.neckties{background:#d4504f;}
.bowimgs {
    border: 1px solid #999;
    height: 107px;
    padding: 16px;
    width: 323px;
}
.col-md-4.accesstabimgs input {
    background: #d4504f none repeat scroll 0 0;
    border: 1px solid #d4504f;
    color: #fff;
    font-size: 19px;
    margin-bottom: 18px;
    padding: 2px;
    width: 100%;
}
.actives {
    border: 2px solid #202020;
}

.primarysuggestion {
	height: auto;
	margin-left: 10px;
	margin-top: 10px;
	width: auto;
}
.dbprim{margin-left: 6px;}
.steps {
	background: #fff none repeat scroll 0 0;
	border: 2px solid #aeaeae;
	border-radius: 7%;
	cursor: pointer;
	float: left;
	font-size: 13px;
	line-height: 18px;
	margin-bottom: 20px;
	margin-left: 2.5%;
	margin-top: 10px;
	max-height: 57px;
	padding-top: 9px;
	text-align: center;
	width: 10%;
}

   .stepsactive {
    background: #c4996c none repeat scroll 0 0;
    border: 2px solid #c4996c;
    color: white;
}


.outputsugg {
	 position: absolute;
	 margin-left: -70px;
	 z-index:-1;
}
.backbtnsugg
{
	height:68%;
}
.backbtnsuggparentdiv
{
	height: 180px;
	width: 130px;
}
.backbtnsuggparentdiv p
{
	font-weight:normal;
	font-size: 12px;
	line-height: 18px!important;
}
#forcasual .primarysuggestion > img
{
	padding: 40px;
}
.okok
{
	font-size: 19px;
	height: 52px;
	margin-left: 10px;
	margin-top: 10px;
	padding-top: 6px;
	width: 162px;
}
.overflow-cmd{overflow-y: auto; height: 660px; padding-top: 22px;}
.liningoptions img {
    height: 40px;
}
.prevnext {
    margin-top:111px;
    width: 432px;
}
.accesstabimg2 {
    padding: 10px;
    width: 22%!important;
}
.accesstabimg2 > img {
    height: 200px;
    width: 229px;
}
.agileinfo_about_bottom_grid li {
    border-left: 1px solid #888;
    float: left;
    font-size: 21px;
    font-weight: 500;
    padding-left: 18px;
    text-align: center;
    padding-right: 18px;
    cursor: pointer;
    text-transform: uppercase;
}
</style>
<?php
function getties()
{
	$query = "select * from categories where cat_type='Accessories' and cat_name='Ties' order by cat_id ASC";
	$result = mysql_query($query);
	$rows = mysql_fetch_array($result);
	echo $product_amount = $rows['cat_price'];
	return;
}
function getpocket()
{
	$query = "select * from categories where cat_type='Accessories' and cat_name='Pocket Squares' order by cat_id ASC";
	$result = mysql_query($query);
	$rows = mysql_fetch_array($result);
	echo $product_amount = $rows['cat_price'];
	return;
}
function getbowties()
{
	$query = "select * from categories where cat_type='Accessories' and cat_name='Bow Tie Satin' order by cat_id ASC";
	$result = mysql_query($query);
	$rows = mysql_fetch_array($result);
	echo $product_amount = $rows['cat_price'];
	return;
}
 ?>
	<!-- //Header -->
<div class="about-bottom wthree-3">
	<div class="container">
		<h2 class="tittle">THE FINISHING TOUCH</h2>
		<p>Your accessories are the finishing touches for your wardrobe.A beautiful tie and tasteful pocket square complete your outfit and make you feel like you can conquer the whole world. We offer a range of ties and pocket squares so you can look your absolute best for any occasion! </p></br>
			<div class="agileinfo_about_bottom_grids">
				<div class="col-md-12 agileinfo_about_bottom_grid accessories-s-tab">
                    <ul>
                        <li class="neckties" onclick="neckTie()">Neck Tie (One)</li>
                        <li class="neckties2" onclick="neckTie2()">Neck Tie (Two)</li>
                        <li class="squretabs" onclick="squrePocket()">Pocket Square</li>
                        <li class="bowtie" onclick="bowTie()">Bow Tie (One)</li>
                        <li class="bowtie2" onclick="bowTie2()">Bow Tie (Two)</li>
                        <li class="tiebar" onclick="tiebar()">Tie Bar</li>
                        <li class="cufflinks" onclick="cufflinks()">Cuff Links</li>
                    </ul>
				</div>
		<!-------Neck tie div--------->
			<div id="necktie">
				<?php 
					$query = "select * from wp_accessories where subtype='neck_tie_one' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
				?>
					<div class="col-md-4 accesstabimg">
						<img src="images/tab/accessories/<?php echo $row['images'];?>.png">
						<div class=""><input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getties(); ?>','<?php echo $row['subtype']; ?>')"></div>
					</div>
				<?php } ?>
			</div>	
			
			<!-------Neck tie 2 div--------->
			<div id="necktie2" style="display:none;">
				<?php 
					$query = "select * from wp_accessories where subtype='neck_tie_two' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
				?>
				<div class="col-md-4 accesstabimg accesstabimg2">
					<img src="images/tab/accessories/<?php echo $row['images'];?>.png">
					<div class=""><input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getties(); ?>','<?php echo $row['subtype']; ?>')"></div>
				</div>
				<?php } ?>	
			</div>	
			
			<!-------Square pocket Div--------->
			<div id="squrepocket" style="display:none;">
				<?php 
					$query = "select * from wp_accessories where subtype='pocket_square' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
				?>
				<div class="col-md-4 accesstabimg">
					<img src="images/tab/accessories/<?php echo $row['images'];?>.png">
					<div class=""><input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getpocket(); ?>','<?php echo $row['subtype']; ?>');"></div>
				</div>
				<?php } ?>	
				
			</div>	
			
			<!-------Bow Tie Div--------->
		
			<div id="bowties" style="display:none;float: right; width:100%;">	
			<?php 
				$j = 1;
					$query = "select * from wp_accessories where subtype='bow_tie_one' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
						if($j == 1){
							$st = "display:block";
						}
						else {
							$st = "display:none";
						}
						
				?>
				<div class="col-md-3 accesstabimgs remclass" id="cat<?php echo $j;?>" style="<?php echo $st;?>">
					<img src="images/tab/accessories/<?php echo $row['images'];?>.png" class="bowimgs">
					<div class="prevnext">
						<input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getbowties(); ?>','<?php echo $row['subtype']; ?>')" style="background: rgb(212, 80, 79) none repeat scroll 0px 0px; border: 1px solid rgb(212, 80, 79); color: rgb(255, 255, 255); font-size: 19px; margin-bottom: 18px; padding:5px; width: 24%; float: right; margin-right: 72px; margin-top: 0px;">
						<label id="previous" onclick="navigation('cat<?php echo $j;?>','prev')" class="prevnextbtn">Previous</label>
						<label id="next" onclick="navigation('cat<?php echo $j;?>','next')" class="prevnextbtn">Next</label>
					</div>
				</div>
				<?php $j++; } ?>
				
				 <div class="col-md-6" id="suitcolors" style="margin-bottom: 70px; float: right;">
					<p class="stylecaption">Satin Color</p>
					<div class="col-md-12" style="margin-bottom: 20px;">
							<div class='primarysuggestion remclass colorsuggestion' title="" id='catid_DXN008Ablack' onclick="colorActive('DXN008Ablack')">
                                <img src='images/tab/DXN008Ablack.png' />
								<p class="suggestionname" style="text-transform:uppercase;">DXN008A </br>Black</p>
                            </div>
							<div class='primarysuggestion remclass colorsuggestion' title="" id='catid_DXN029Anavy' onclick="colorActive('DXN029Anavy')">
                                <img src='images/tab/DXN029Anavy.png' />
								<p class="suggestionname" style="text-transform:uppercase;">DXN029A </br>Navy</p>
                            </div>
							<div class='primarysuggestion remclass colorsuggestion' title="" id='catid_DXN030Awhine' onclick="colorActive('DXN030Awhine')">
                                <img src='images/tab/DXN030Awhine.png' />
								<p class="suggestionname" style="text-transform:uppercase;">DXN030A </br>whine</p>
                            </div>
							<div class='primarysuggestion remclass colorsuggestion' title="" id='catid_DXN031Agrey' onclick="colorActive('DXN031Agrey')">
                                <img src='images/tab/DXN031Agrey.png' />
								<p class="suggestionname" style="text-transform:uppercase;">DXN031A </br>Grey</p>
                            </div>
							<div class='primarysuggestion remclass colorsuggestion' title="" id='catid_DXN031Ared' onclick="colorActive('DXN031Ared')">
                                <img src='images/tab/DXN031Ared.png' />
								<p class="suggestionname" style="text-transform:uppercase;">DXN031A </br>Red</p>
                            </div>
							<div class='primarysuggestion remclass colorsuggestion' title="" id='catid_DXN043Awhite' onclick="colorActive('DXN043Awhite')">
                                <img src='images/tab/DXN043Awhite.png' />
								<p class="suggestionname" style="text-transform:uppercase;">DXN043A </br>white</p>
                            </div>
						</div>	
							<div class="col-md-12" style="overflow-y: scroll; padding: 0px; height: 217px; margin-bottom: 50px;">
							<p class="stylecaption" style="margin-top: 7px; height: 34px; padding: 2px;">Rest Fabric</p>
						<?php
						$query = "select * from wp_colors where type='shirt' and onlyfor = 'shirt' and status = '1'";
						$result = mysql_query($query);
						if($result)
						{
							$num = mysql_num_rows($result);
							if($num>0)
							{
								$status = "done";
								$a=0;
							 while($rows = mysql_fetch_array($result))
                        {
                            $a++;
                            $colorName = $rows['colorName'];
                            $displayImage = $rows['displayImage'];
                            ?>
                            <div class='primarysuggestion remclass' id="catid_<?php echo $a;?>" onclick="colorActive('<?php echo $a;?>')" title="<?php echo $colorName ?>" id='suitcolor_<?php echo $colorName ?>'>
                                <img src='images/shirts/<?php echo $colorName.".png" ?>' />
								<p class="suggestionname" style="text-transform:uppercase;"><?php echo $colorName; ?></p>
                            </div>
							
							
                            <?php
                        }
							}
							else
							{
								echo "No Color Found";
							}
						}
						else
						{
							echo mysql_error();
						}
						?>
					</div>
				</div>
			</div>	
			
			
			<!------------bow tie 2 div -------------->
			<div id="bowties2" style="display:none;float: right; width:100%;">
				<?php 
					$query = "select * from wp_accessories where subtype='bow_tie_two' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
				?>
				<div class="col-md-4 accesstabimg accesstabimg2">
					<img src="images/tab/accessories/<?php echo $row['images'];?>.png">
					<div class=""><input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getbowties(); ?>','<?php echo $row['subtype']; ?>')"></div>
				</div>
				<?php } ?>
				
			</div>	
			
			
			<!-------Tie bar Div--------->
			<div id="tiebar" style="display:none;">
				<?php 
					$query = "select * from wp_accessories where subtype='tie_bar' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
				?>
				<div class="col-md-4 accesstabimg">
					<img src="images/tab/accessories/<?php echo $row['images'];?>.png">
					<div class=""><input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getties(); ?>','<?php echo $row['subtype']; ?>');"></div>
				</div>
				<?php } ?>
			</div>

			<!-------Cuff links Div--------->
			<div id="cufflinks" style="display:none;">
				<?php
					$query = "select * from wp_accessories where subtype='cuff_links' order by id ASC";
					$result = mysql_query($query);
					while($row = mysql_fetch_array($result)){
				?>
				<div class="col-md-4 accesstabimg">
					<img src="images/tab/accessories/<?php echo $row['images'];?>.png">
					<div class=""><input type="button" value="Add to cart" onclick="buyAccessories('<?php echo $row['category'];?>','<?php echo $row['specification'];?>','<?php echo $row['rc_no'];?>','<?php echo $row['images'];?>','<?php echo getties(); ?>','<?php echo $row['subtype']; ?>');"></div>
				</div>
				<?php } ?>
			</div>
			
			
		</div>
	</div>
</div>
<script>
function buyAccessories(category,specification,rc_no,image_type,price,subtype){
	var user_id = $("#user_id").val();
	var randomnum = Math.floor((Math.random() * 600000) + 1);
	var product_name = category+"_"+randomnum;
	var url = "admin/webserver/addto_cart.php?products_type=Accessories&category="+category+"&product_name="+
	product_name+"&specification="+specification+"&rc_no="+rc_no+"&user_id="+user_id+"&quantity=1&image_type="+image_type+"&subtype="+subtype;
    $.get(url, function (data) {
		var json = $.parseJSON(data)
		var status = json.status;
		if (status == "done") {
			window.location = "cart.php";
		}
		else {
			alert(status);
		}
	});
}
function neckTie(){
	$("#necktie").show();
	$("#necktie2").hide();
	$("#squrepocket").hide();
	$("#bowties").hide();
	$("#bowties2").hide();
	$("#tiebar").hide();
	$("#cufflinks").hide();
	$(".neckties").css("background","#d4504f");
	$(".neckties2").css("background","#202020");
	$(".squretabs").css("background","#202020");
	$(".bowtie").css("background","#202020");
	$(".bowtie2").css("background","#202020");
	$(".tiebar").css("background","#202020");
	$(".cufflinks").css("background","#202020");
}
function neckTie2(){
	$("#necktie2").show();
	$("#necktie").hide();
	$("#squrepocket").hide();
	$("#bowties").hide();
	$("#bowties2").hide();
	$("#tiebar").hide();
	$("#cufflinks").hide();
	$(".neckties2").css("background","#d4504f");
	$(".neckties").css("background","#202020");
	$(".squretabs").css("background","#202020");
	$(".bowtie").css("background","#202020");
	$(".bowtie2").css("background","#202020");
	$(".tiebar").css("background","#202020");
	$(".cufflinks").css("background","#202020");
}
function squrePocket(){
	$("#necktie").hide();
	$("#necktie2").hide();
	$("#bowties").hide();
	$("#bowties2").hide();
	$("#tiebar").hide();
	$("#cufflinks").hide();
	$("#squrepocket").show();
	$(".neckties2").css("background","#202020");
	$(".neckties").css("background","#202020");
	$(".bowtie").css("background","#202020");
	$(".bowtie2").css("background","#202020");
	$(".tiebar").css("background","#202020");
	$(".cufflinks").css("background","#202020");
	$(".squretabs").css("background","#d4504f");
}
function bowTie(){
	$("#necktie").hide();
	$("#necktie2").hide();
	$("#squrepocket").hide();
	$("#bowties").show();
	$("#bowties2").hide();
	$("#tiebar").hide();
	$("#cufflinks").hide();
	$(".neckties2").css("background","#202020");
	$(".neckties").css("background","#202020");
	$(".squretabs").css("background","#202020");
	$(".bowtie2").css("background","#202020");
	$(".tiebar").css("background","#202020");
	$(".cufflinks").css("background","#202020");
	$(".bowtie").css("background","#d4504f");
}
function bowTie2(){
	$("#necktie").hide();
	$("#necktie2").hide();
	$("#squrepocket").hide();
	$("#bowties").hide();
	$("#tiebar").hide();
	$("#cufflinks").hide();
	$("#bowties2").show();
	$(".neckties2").css("background","#202020");
	$(".neckties").css("background","#202020");
	$(".squretabs").css("background","#202020");
	$(".bowtie2").css("background","#d4504f");
	$(".bowtie").css("background","#202020");
	$(".tiebar").css("background","#202020");
	$(".cufflinks").css("background","#202020");
}
function tiebar(){
	$("#necktie").hide();
	$("#necktie2").hide();
	$("#squrepocket").hide();
	$("#bowties").hide();
	$("#tiebar").hide();
	$("#bowties2").hide();
	$("#cufflinks").hide();
	$("#tiebar").show();
	$(".neckties2").css("background","#202020");
	$(".neckties").css("background","#202020");
	$(".squretabs").css("background","#202020");
	$(".bowtie2").css("background","#202020");
	$(".bowtie").css("background","#202020");
	$(".cufflinks").css("background","#202020");
	$(".tiebar").css("background","#d4504f");
}
function cufflinks(){
	$("#necktie").hide();
	$("#necktie2").hide();
	$("#squrepocket").hide();
	$("#bowties").hide();
	$("#tiebar").hide();
	$("#bowties2").hide();
	$("#tiebar").hide();
	$("#cufflinks").show();
	$(".neckties2").css("background","#202020");
	$(".neckties").css("background","#202020");
	$(".squretabs").css("background","#202020");
	$(".bowtie2").css("background","#202020");
	$(".bowtie").css("background","#202020");
	$(".tiebar").css("background","#202020");
	$(".cufflinks").css("background","#d4504f");
}
function colorActive(catid){
	$(".remclass").removeClass("actives");
	$("#catid_"+catid).addClass("actives");
}
function navigation(switchers,nextprev){
	if(nextprev =="next"){
		if(switchers =="cat1"){
			$("#cat1").fadeOut("slow");
			$("#cat2").fadeIn("slow");
		}
		else if(switchers =="cat2"){
			$("#cat2").fadeOut("slow");
			$("#cat3").fadeIn("slow");
		}
		else if(switchers =="cat3"){
			$("#cat3").fadeOut("slow");
			$("#cat4").fadeIn("slow");
		}
		else if(switchers =="cat4"){
			$("#cat4").fadeOut("slow");
			$("#cat5").fadeIn("slow");
		}
		else if(switchers =="cat5"){
			$("#cat5").fadeOut("slow");
			$("#cat4").fadeIn("slow");
		}
	}
	if(nextprev =="prev"){
		if(switchers =="cat5"){
			$("#cat5").fadeOut("slow");
			$("#cat4").fadeIn("slow");
		}
		else if(switchers =="cat4"){
			$("#cat4").fadeOut("slow");
			$("#cat3").fadeIn("slow");
		}
		else if(switchers =="cat3"){
			$("#cat3").fadeOut("slow");
			$("#cat2").fadeIn("slow");
		}
		else if(switchers =="cat2"){
			$("#cat2").fadeOut("slow");
			$("#cat1").fadeIn("slow");
		}
		
	}
}  
</script>
<?php include("footer.php");?>