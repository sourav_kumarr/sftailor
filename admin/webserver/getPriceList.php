<?php
session_start();
include('database/db.php');
include('errorCodes.php');
include('functions.php');
$curr_tz = date_default_timezone_get();
date_default_timezone_set($curr_tz);
$status="error";
$message="unable to get data....";
$type = $_REQUEST['type'];
$requiredfields = array('type');
// validate required fields
if(!RequiredFields($_REQUEST, $requiredfields)){
    return false;
}
if($type   ==   "get_price_list")  //to get order of particular user only userid required
{
    $priceData = array();
    $query = "select * from categories where cat_type != 'Coupons' ";
    $result = mysql_query($query);
    if ($result)
    {
        $count = mysql_num_rows($result);
        if ($count > 0)
        {
            $status = "done";
            $message = "data found successfully";
            while ($rows = mysql_fetch_assoc($result))
            {
                $priceData[] = $rows;
            }
        }
        else
        {
            $status = "error";
            $message = "No price data found ";
        }
    }
    else
    {
        $status = "error";
        $message = "unable to find data " . mysql_error();
    }
    echo json_encode(array("status"=>$status,"message"=>$message,"data"=>$priceData));
}
else
{
    echo json_encode(array("status"=>$status,"message"=>$type." invalid input type value","data"=>$priceData));
}

?>