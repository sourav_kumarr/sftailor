<?php
error_reporting(0);
include('admins/api/Classes/CONNECT.php');
include('admins/api/Constants/DbConfig.php');
include('admins/api/Constants/configuration.php');
require_once('admins/api/Classes/USERCLASS.php');
$conn = new \Classes\CONNECT();
$userClass = new \Classes\USERCLASS();

?>
<!DOCTYPE html>
<html>
<head>
    <title>SFTailor</title>
    <link href="css/bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="css/bootstrap-datetimepicker.css">
    <link href="css/mystyle.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js" ></script>
    <script type="text/javascript" src="js/location.js"></script>
    <script type="text/javascript" src="js/moment.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js"></script>
</head>
<div class="loader" style="display: none">
	<img src="images/spin.gif" class="spinloader"/>
</div>
<style>
    .body{
        background: rgba(0, 0, 0, 0) url("./images/slider.jpg") no-repeat scroll 0 0 / 100% auto;
        margin: 0;
    }
    .custom_signup_button {
        border: 0 none;
        border-radius: 3px;
        float: right;
        margin-top: 20px;
        padding: 8px 20px;
        width: 200px;
    }
</style>
<?php
$successurl = "";
if(isset($_REQUEST['redurl'])){
	if($_REQUEST['redurl'] == "rgvsjuq45745sttpx"){
		$successurl = "custom_suit.php?fromtwo";
	}elseif($_REQUEST['redurl'] == "vidypnosmy178ghf"){
		$successurl = "custom_pants.php?fromtwo";
	}elseif($_REQUEST['redurl'] == "gnsjktyw12cre"){
		$successurl = "custom_shirts.php?fromtwo";
	}elseif($_REQUEST['redurl'] == "pl"){
        $successurl = "pricelist.php";
    }elseif($_REQUEST['redurl'] == "cp"){
        $successurl = "customize.php";
    }elseif($_REQUEST['redurl'] == "sb"){
        $successurl = "stylebox.php";
    }else{
		$successurl = "index.php";
	}
}else if(isset($_REQUEST['type'])){
    if($_REQUEST['type'] == 'fv') {
        $successurl = "sizematching.php";
    }
}else{
    $successurl = "index.php";
}
echo "<input type='hidden' id='successurl' value=".$successurl." />";
?>
<script>
$(document).ready(function(){
	///-----------------------------registration from=====------------------------------------------------------------------------
	$("#registerform").submit(function(e) {
		e.preventDefault();
		$('.loader').fadeIn("slow");
		$.ajax({
			url: "admin/webserver/signup.php", 
			type: "POST",             
			data: new FormData(this), 
			contentType: false,       
			cache: false,             
			processData:false,        
			success: function(data){
				$('.loader').fadeOut("slow");
				var json = $.parseJSON(data);
                var status = json.status;
                var message = json.message;
                if(status == "Registered Successfully"){
                    window.location="login.php?type=fv";
                }else{
                    $(".modal-title").html("<span style='color:red'>Error Occured</span>");
                    $(".modal-body").html("<span style='color:red'>"+status+"</span>");
                    $("#myModal").modal("show");
                }
			}
		});
	});
	///-----------------------------registration form end here=====------------------------------------------------------------------------
	///-----------------------------sign in form in here=====------------------------------------------------------------------------
	$("#loginform").on('submit',(function(e) {
		e.preventDefault();
        $.ajax({
            url: "admin/webserver/signin.php",
            type: "POST",
			data: new FormData(this), 
			contentType: false,       
			cache: false,             
			processData:false,        
			success: function(data){
                var json = $.parseJSON(data);
                var status = json.status;
                if(status == "done"){
                    var successurl = $("#successurl").val();
                    window.location=successurl;
                }else{
                    $(".modal-title").html("<span style='color:red'>Error Occured</span>");
                    if(status == "error") {
                        $(".modal-body").html("<span style='color:red'>Invalid UserName/Password</span>");
                    }else{
                        $(".modal-body").html("<span style='color:red'>"+status+"</span>");
                    }
                    $("#myModal").modal("show");
                }
            }
		});
	}));
});
</script>
<body class="body">
	<div class="col-md-12 black_spot">
		<div class="col-md-12 login_logo_div" style="">
			<a href="index.php"><img src="images/logo/logo.png" class="login_logo"/></a>
		</div>
		<!--  signin form start here    -->
		<div class="col-md-12" style="text-align:center;display:block" id="login_form" >
			<p class="login_caption3">Welcome to <span style="color:#d4504f">SFTailor</span></p>
			<form method="Post" id="loginform">
				<input type="hidden" name="block" value="login" />
				<div class="form-group" >
					<input required class="custom_login_feild" type="email" value="" name="email" Placeholder="Enter E-Mail" />
				</div>
				<div class="form-group" >
					<input required class="custom_login_feild" type="password" value="" name="password" Placeholder=" Enter Password" />
				</div>
				<div class="form-group" style="margin-top:40px">
					<input class="custom_login_button" type="submit" value="Login" />
				</div>
				<div class="form-group">
					<p class="create_account">Don't have account? <span style="color:#d4504f;cursor:pointer;font-size:24px" onclick="signup()">Create account</span></p>
				</div>
			</form>
		</div>
		<!--  signin form end here    ------------------------------------>
        <!--  signup form start here  ------------------------------------>
		<div class="col-md-1"></div>
		<div class="col-md-10" style="display:none;box-shadow:0 0 10px #333;border-radius:5px;background:rgba(255,255,255,0.7)" id="signup_form">
			<form method="Post" enctype="multipart/form-data" id="registerform">
				<div class="row cardview" style="">
                    <p id="errorhere" style="color:red;font-size:16px;padding:10px"></p>
					<div class="col-md-4">
						<div class="form-group">
							<label>First Name</label>
                            <input placeholder="First Name" name="fname" class="form-control" type="text" />
                        </div>
						<div class="form-group">
                            <label>Last Name</label>
                            <input placeholder="Last Name" name="lname" class="form-control" type="text">
						</div>
						<div class="form-group">
                            <label>Select Country</label>
                            <select name="b_country" class="countries form-control" id="countryId" onchange='datedate()'>
                                <option value="">Select Country</option>
                            </select>
                        </div>
						<div class="form-group">
                            <label>Select State</label>
                            <select name="b_state" class="states form-control" id="stateId">
                                <option value="">Select State</option>
                            </select>
                        </div>
						<div class="form-group">
							<label>Select City</label>
                            <div class="col-md-12" id="cityfeild" style="padding:0;">
                                <select name="b_city" class="cities form-control" id="cityId">
                                    <option value="">Select City</option>
                                </select>
							</div>
						</div>
						<div class="form-group">
                            <label style="margin-top:15px">D.O.B</label>
                            <input id="inputdob" name="dob" placeholder="Enter D.O.B" class="form-control" type="text">
                        </div>
					</div>
                    <div class="col-md-4">
						<div class="form-group" >
                            <label>Profile Picture</label>
                            <input name="file0" class="form-control" type="file">
                        </div>
                        <div class="form-group">
                            <label>Enter Height</label>
							<div class="row">
							    <div class="col-md-6">
								    <select name="heightparam" class="form-control" id="height_parameter" onchange='height_param()'>
                                        <option value="Inches">Inches</option>
                                        <option value="Centimeters">Centimeters</option>
									    <option value="Feets">Feets</option>
								    </select>
							    </div>
							    <div class="col-md-6" id='heightdiv'>
								    <input name='height' class='form-control' placeholder="Enter Height" />
							    </div>
							</div>
                        </div>
						<div class="form-group">
                            <label>E-Mail</label>
                            <input placeholder="Email" name="email" class="form-control" type="email">
                        </div>
						<div class="form-group">
                            <label>Password</label>
                            <input placeholder="Password" id="password" name="password" class="form-control" type="password">
                        </div>
						<div class="form-group">
                            <label>Confirm Password</label>
                            <input placeholder="Repeat Password" id="cpassword" class="form-control" type="password">
                        </div>
                        <div class="form-group">
                            <label>Stylist</label>
							<select class="form-control" name="stylist">
                                <option selected="" value="">Select Stylist</option>
                                <?php
                                    $link = $conn->connect();
                                    if ($link) {
                                        $query = "select * from wp_store order by store_id DESC";
                                        $result = mysqli_query($link, $query);
                                        if ($result) {
                                            $num = mysqli_num_rows($result);
                                            if ($num > 0) {
                                                while ($orderData = mysqli_fetch_array($result)) {
                                                    ?>
                                                    <option selected="" value="<?php echo $orderData['store_name']; ?>">
                                                        <?php echo $orderData['store_name']; ?>
                                                    </option>
                                                    <?php
                                                }
                                            }
                                        }
                                    }
                                ?>
                            </select>
                        </div>
					</div>
					<div class="col-md-4">
                        <div class="form-group">
                            <label>Select Gender</label>
                            <select class="form-control" name="gender">
                                <option selected="" value="">Select Gender</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                            </select>
                        </div>
						<div class="form-group">
							<label>Language</label>
                            <select class="form-control" name="lang">
                                <option selected="" value="">Select Language</option>
                                <option value="English(US)">English(US)</option>
                                <option value="English(UK)">English(UK)</option>
                                <option value="French">French</option>
                            </select>
						</div>
						<div class="form-group">
                            <label>Weight</label>
							<div class="row">
							    <div class="col-md-6">
								    <select name="weightparam" class="form-control">
									    <option selected="" value="">Parameter</option>
									    <option value="Kilograms">Kilograms</option>
									    <option value="Pounds">Pounds</option>
									    <option value="Stones">Stones</option>
								    </select>
							    </div>
							    <div class="col-md-6">
								    <input type='text' name='weight' class='form-control' placeholder='Enter Weight' />
							    </div>
                            </div>
						</div>
                        <div class="form-group">
                            <label>ZipCode</label>
                            <input type="text" placeholder="Enter ZipCode" name="zipcode" class="form-control" />
                        </div>
                        <div class="form-group">
                            <label>Address / Street No.</label>
                            <textarea placeholder="Enter Address" name="address" class="form-control"></textarea>
                        </div>
                        <div class="form-group" style="margin:0px">
							<input value="Register" class="btn btn-danger pull-right" style="width:200px" type="submit">
						</div>
						<div class="row" style="margin-top:10px">
							<input value="sftailor" name="site_name" type="hidden">
							<input value="Website" name="app_type" type="hidden">
						</div>
					</div>
					<div style="clear:both"></div>	
					<p class="create_account" style="text-align:center">Already Registered ? <span style="color:#fff;cursor:pointer;font-size:24px" onclick="loginform()">Login Here</span></p>
				</div>
			</form>
		</div>
		<div class="col-md-1"></div>
		<!--  signup form end here  -->
		<div class="col-md-12" style="text-align:center;display:none">
			<br><br><br><br><br>
			<div class="form-group" >
				<input class="custom_login_button" id="loginbtn" type="button" value="Login Here" onclick="loginnow()" />
			</div>
		</div>
	</div>
    <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Modal Header</h4>
                </div>
                <div class="modal-body">
                    <p>Some text in the modal.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
	function height_param(){
		var height_param_val = $("#height_parameter").val();
		if(height_param_val == "Feets"){
			$("#heightdiv").html("<div class='col-md-6' style='padding:0'><input name='height0' class='form-control' placeholder='Enter Feets' /></div><div class='col-md-6' style='padding:0'><input name='height1' class='form-control' placeholder='Enter Inches' /></div>");
		}
		else if(height_param_val == "Centimeters"){
			$("#heightdiv").html("<input name='height' class='form-control' placeholder='Enter Height' />");
		}
		else{
			$("#heightdiv").html("<input name='height' class='form-control' placeholder='Enter Height' />");
		}
	}
	function signup()
	{
		$("#signup_form").show();
		$("#login_form").hide();
	}
	function loginform()
	{
		$("#login_form").show();
		$("#signup_form").hide();
	}
	function datedate(){
		var country = $("#countryId").val();
		if(country == "United States"){
			$('#inputdob').datetimepicker({
				minView : 2,
				format:"mm/dd/yyyy",
				autoclose: true
			});
		}else{
			$('#inputdob').datetimepicker({
				minView : 2,
				format:"dd/mm/yyyy",
				autoclose: true
			});
		}
	}
</script>
<?php
if(isset($_REQUEST['type'])){
	if($_REQUEST['type'] == "reg"){
		?>
		<script>
			signup();
		</script>
		<?php
	}
}
?>
</html>