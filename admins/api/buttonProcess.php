<?php
/**
 * Created by PhpStorm.
 * User: sourav
 * Date: 10/5/2017
 * Time: 3:42 PM
 */
require_once('Classes/BUTTON.php');
require_once ('Constants/functions.php');
require_once ('Classes/excel_reader2.php');
require_once('Constants/configuration.php');
require_once('Constants/DbConfig.php');

$buttonClass = new \Classes\BUTTON();
$helplerClass = new \Classes\BUTTON();

$requiredfields = array('type');
$response = RequiredFields($_POST, $requiredfields);
if($response['Status'] == 'Failure'){
    $buttonClass->apiResponse($response);
    return false;
}
$type = $_REQUEST['type'];
if($type === 'addButtonImg') {
    $requiredfields = array('button_for','button_name','button_size','button_price');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $buttonClass->apiResponse($response);
        return false;
    }
    $button_for   = $_REQUEST['button_for'];
    $button_name  = $_REQUEST['button_name'];
    $button_size  = $_REQUEST['button_size'];
    $button_price = $_REQUEST['button_price'];
    $logoResponse = $buttonClass->addButtonImg($button_for,$button_name,$button_size,$button_price);
    $buttonClass->apiResponse($logoResponse);
}
else if($type == 'storeData') {

    $link = new \Classes\CONNECT();
    $storeData = $link->storeImage('btnFile','excel');
    if($storeData[Status] == Error) {
        $buttonClass->apiResponse($storeData);
        return false;
    }
    $data = new Spreadsheet_Excel_Reader($storeData['File_Path']);
    $validExcel = $helplerClass->validExcelSheet($data);
    if($validExcel[Status] == Error) {
        $buttonClass->apiResponse($validExcel);
        return false;
    }
    if(count($data->sheets[0]['cells'])>0) // checking sheet not empty
    {
        for($j=1;$j<=count($data->sheets[0]['cells']);$j++) // loop used to get each row of the sheet
        {
            $helplerClass->setButtonFor($data->sheets[0]['cells'][$j][2]);
            $helplerClass->setButtonName($data->sheets[0]['cells'][$j][3]);
            $helplerClass->setButtonSize($data->sheets[0]['cells'][$j][4]);
            $helplerClass->setButtonPrice($data->sheets[0]['cells'][$j][5]);
            $saveData = $helplerClass->save();
        }
    }
    $response[Status] = Success;
    $response[Message] = "data saved successfully";
    $buttonClass->apiResponse($response);
}
else if($type === 'getButtons') {
    $response = $buttonClass->getButtons();
    $buttonClass->apiResponse($response);
}
else if($type == "updateBtnStatus")
{
    $requiredfields = array('btnId','btnValue');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $buttonClass->apiResponse($response);
        return false;
    }
    $btnId       = $_REQUEST['btnId'];
    $btnValue    = $_REQUEST['btnValue'];
    $response    = $buttonClass->btnStatusChange($btnId,$btnValue);
    if($response[STATUS] == Error) {
        $buttonClass->apiResponse($response);
        return false;
    }
    $buttonClass->apiResponse($response);
}
else if($type == "deletebtn")
{
    $requiredfields = array('btnId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $buttonClass->apiResponse($response);
        return false;
    }
    $btnId = $_REQUEST['btnId'];
    $response = $buttonClass->deleteBtnId($btnId);
    if($response[STATUS] == Error) {
        $buttonClass->apiResponse($response);
        return false;
    }
    $buttonClass->apiResponse($response);
}
else if($type == "getPartBtn")
{
    $requiredfields = array('btnId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $buttonClass->apiResponse($response);
        return false;
    }
    $btnId = $_POST['btnId'];
    $response = $buttonClass->getPartBtn($btnId);
    $buttonClass->apiResponse($response);
}


else if($type === 'editButton') {
    $requiredfields = array('button_for','button_name','button_size','button_price','oldBtnImagechanged','oldBtnImage','btnId');
    $response = RequiredFields($_POST, $requiredfields);
    if($response['Status'] == 'Failure'){
        $buttonClass->apiResponse($response);
        return false;
    }
    $btnId = $_REQUEST['btnId'];
    $button_for = $_REQUEST['button_for'];
    $button_name = $_REQUEST['button_name'];
    $button_size = $_REQUEST['button_size'];
    $button_price = $_REQUEST['button_price'];
    $oldBtnImagechanged = $_REQUEST['oldBtnImagechanged'];
    $oldBtnImage = $_REQUEST['oldBtnImage'];
    $logoResponse = $buttonClass->editButton($btnId,$button_for,$button_name,$button_size,$button_price,$oldBtnImagechanged,$oldBtnImage);
    $buttonClass->apiResponse($logoResponse);
}
?>