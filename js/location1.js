function ajaxCall1() {
    this.send = function(data, url, method, success, type) {
        type = type||'json';
        var successRes = function(data) {
            success(data);
        }

        var errorRes = function(e) {
            console.log(e);
            //alert("Error found \nError Code: "+e.status+" \nError Message: "+e.statusText);
            $('#loader').modal('hide');
        }
        $.ajax({
            url: url,
            type: method,
            data: data,
            success: successRes,
            error: errorRes,
            dataType: type,
            timeout: 60000
        });
    }
}
function locationInfo() {
    //var rootUrl = "http://iamrohit.in/lab/php_ajax_country_state_city_dropdown/api.php";
    var rootUrl = "http://stsmentor.com/locationapi/locationApi.php";
    var call = new ajaxCall1();
    this.getCities = function(id) {
        $(".cities option:gt(0)").remove();
        var url = rootUrl+'?type=getCities&stateId=' + id;
        var method = "post";
        var data = {};
        $('.cities').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.cities').find("option:eq(0)").html("Select City");
            if(data.tp == 1){
                var count=0;
                $.each(data['result'], function(key, val) {
                    $.each(val, function(keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('cityid', keys);
                        count = count+(option.length);
                        $('.cities').append(option);
                    });
                });
                if(count == 0)
                {
                    $("#cityfeild").html("<input type='text' class='custom_signup_feild cf' placeholder='Enter City Manually' name = 'b_city' />");
                }
                else
                {
                    $("#cityfeild").html('<select id="cityId" class="cities custom_signup_feild selectboxApp" name="b_city">');
                    $.each(data['result'], function(key, val) {
                        $.each(val, function(keys, vals) {
                            var option = $('<option />');
                            option.attr('value', vals).text(vals);
                            option.attr('cityid', keys);
                            count = count+(option.length);
                            $('.cities').append(option);
                        });
                    });
                }
                $(".cities").prop("disabled",false);
            }
            else{
                //alert("no city found");
            }
        });
    };
    this.getStates = function(id) {
        $(".states option:gt(0)").remove();
        $(".cities option:gt(0)").remove();
        var country = $(".ma_country_field").val();
        var url = rootUrl+'?type=getStates&countryId=' + id;
        var method = "post";
        var data = {};
        $('.states').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.states').find("option:eq(0)").html("Select State");
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    $.each(val, function(keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('stateid', keys);
                        $('.states').append(option);
                    });
                });
                $(".states").prop("disabled",false);
            }
            else{
                //alert(data.msg);
            }
        });
    };
    this.getCountries = function() {
        var url = rootUrl+'?type=getCountries';
        var method = "post";
        var data = {};
        var selected_country = $(".ma_country_field").val();
        $('.countries').find("option:eq(0)").html("Please wait..");
        call.send(data, url, method, function(data) {
            $('.countries').find("option:eq(0)").html("Select Country");
            console.log(data);
            if(data.tp == 1){
                $.each(data['result'], function(key, val) {
                    $.each(val, function(keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('countryid', keys);
                        if(vals == selected_country){
                            option.attr('selected', "selected");
                            setStates(keys);
                        }

                        $('.countries').append(option);
                    });
                });
                $(".countries").prop("disabled",false);
            }
            else{
                //alert(data.msg);
            }
        });
    };
}
$(function() {
    var loc = new locationInfo();
    loc.getCountries();
    $(".countries").on("change", function(ev) {
        var countryId = $("option:selected", this).attr('countryid');
        if(countryId != ''){
            /*$(".cities").val("");*/
            loc.getStates(countryId);
        }
        else{
            $(".states option:gt(0)").remove();
        }
    });
    $(".states").on("change", function(ev) {
        var stateId = $("option:selected", this).attr('stateid');
        if(stateId != ''){
            loc.getCities(stateId);
        }
        else{
            $(".cities option:gt(0)").remove();
        }
    });
});
function  setStates(id) {
    var country = $(".ma_country_field").val();
    if(country == "United States")
    {
        setdatePickerUs();
    }
    else {
        setdatePickerOther();
    }
    var selected_state = $(".ma_state_field").val();
    var url = 'http://stsmentor.com/locationapi/locationApi.php?type=getStates&countryId=' + id;
    var method = "post";
    var data = {};
    $('.states').find("option:eq(0)").html("Please wait..");
    $.post(url, function (data) {
        $('.states').find("option:eq(0)").html("Select State");
        if(data.tp == 1){
            $.each(data['result'], function(key, val) {
                $.each(val, function(keys, vals) {
                    var option = $('<option />');
                    option.attr('value', vals).text(vals);
                    option.attr('stateid', keys);
                    if(vals == selected_state){
                        option.attr('selected', "selected");
                        setCity(keys);
                    }
                    $('.states').append(option);
                });
            });
            $(".states").prop("disabled",false);
        }
        else{
            //alert(data.msg);
        }
    });
}
function setCity(id) {
    var selectedCity = $(".ma_city_field").val();
    var url = 'http://stsmentor.com/locationapi/locationApi.php?type=getCities&stateId=' + id;
    var method = "post";
    var data = {};
    $('.cities').find("option:eq(0)").html("Please wait..");
    $.post(url, function (data) {
            $('.cities').find("option:eq(0)").html("Select City");
            if(data.tp == 1){
                var count=0;
                $.each(data['result'], function(key, val) {
                    $.each(val, function(keys, vals) {
                        var option = $('<option />');
                        option.attr('value', vals).text(vals);
                        option.attr('cityid', keys);
                        count = count+(option.length);
                        if(vals == selectedCity)
                        {
                            option.attr("selected","selected");
                        }
                        $('.cities').append(option);
                    });
                });
                if(count == 0)
                {
                    $("#cityfeild").html("<input type='text' value='"+selectedCity+"' class='custom_signup_feild cf' placeholder='Enter City Manually' name = 'b_city' />");
                }
                else
                {
                    $("#cityfeild").html('<select id="cityId" class="cities custom_signup_feild selectboxApp" name="b_city">');
                    $.each(data['result'], function(key, val) {
                        $.each(val, function(keys, vals) {
                            var option = $('<option />');
                            option.attr('value', vals).text(vals);
                            option.attr('cityid', keys);
                            count = count+(option.length);
                            if(vals == selectedCity)
                            {
                                option.attr("selected","selected");
                            }
                            $('.cities').append(option);
                        });
                    });
                }
                $(".cities").prop("disabled",false);
            }
            else{
                //alert("no city found");
            }
    });

}