<?php
/**
 * Created by PhpStorm.
 * User: sunil
 * Date: 1/5/17
 * Time: 1:27 PM
 */
namespace Classes;
require_once('CONNECT.php');
class GIFTCLASS
{
    public $link = null;
    public $response = array();
    public $currentDate=null;
    public $currentDateStamp=null;
    public $currentDateTime=null;
    public $currentDateTimeStamp=null;
    function __construct()
    {
        error_reporting(0);
        $this->link = new CONNECT();
        $this->currentDate = date('d M Y');
        $this->currentDateTime = date('d M Y h:i:s A');
        $this->currentDateStamp = strtotime($this->currentDate);
        $this->currentDateTimeStamp = strtotime($this->currentDateTime);
    }


    public function addGiftbox($gift_for,$gift_code,$price,$description){
              $link = $this->link->connect();
                if($link) {
                    if(isset($_FILES['stylebox_file'])){
                        $file_tmp = $_FILES['stylebox_file']['tmp_name'];
                        $file_name = $_FILES['stylebox_file']['name'];
                        $file_ext=strtolower(end(explode('.',$file_name)));
                        $expensions= array("jpeg","jpg","png","gif");
                        if(in_array($file_ext,$expensions)=== false){
                            $this->response[STATUS] = Error;
                            $this->response[MESSAGE] = "Invalid Selection of File";
                        }else{
                            $file_name = rand(100000,999999).".".$file_ext;
                            move_uploaded_file($file_tmp,"Files/images/giftbox/".$file_name);
                            $query = "insert into wp_gift_certificate (gift_for,gift_code,gift_price,gift_description,gift_image,created_date,status) values ('$gift_for','$gift_code','$price',
                            '$description','$file_name',NOW(),'1')";
                            $result = mysqli_query($link, $query);
                            if ($result) {
                                $this->response[STATUS] = Success;
                                $this->response[MESSAGE] = "Data Inserted Successfuly";
                            }
                            else {
                                $this->response[STATUS] = Error;
                                $this->response[MESSAGE] = $this->link->sqlError();
                            }
                        }
                    }else{
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = "File Not Selected";
                    }
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Connection Error";
                }
        return $this->response;
    }
    public function getGiftboxData(){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_gift_certificate order by g_id desc";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if($num>0){
                    $GiftboxData = array();
                    while($rows = mysqli_fetch_array($result)){
                        $GiftboxData[]=array(
                            "g_id"=>utf8_encode($rows['g_id']),
                            "gift_image"=>utf8_encode($rows['gift_image']),
                            "gift_for"=>utf8_encode($rows['gift_for']),
                            "gift_code"=>utf8_encode($rows['gift_code']),
                            "gift_price"=>utf8_encode($rows['gift_price']),
                            "gift_description"=>utf8_encode($rows['gift_description']),
                            "status"=>utf8_encode($rows['status'])
                        );
                    }
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data Found";
                    $this->response['GiftboxData'] = $GiftboxData;
                }
                else{
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Users Found";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }
    public function editGiftbox($g_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_gift_certificate where g_id='$g_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $giftData = array();
                    $rows = mysqli_fetch_assoc($result);
                    $giftData = $rows;
                    $this->response[STATUS] = Success;
                    $this->response[MESSAGE] = "Data update successfully";
                    $this->response['giftData'] = $giftData;
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "No Data Found";
                }
            }
            else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function updateGiftbox($title,$description,$code,$price,$g_id,$ImageChanged){
        $link = $this->link->connect();
        if($link) {
            if($ImageChanged == "yes"){
                $file_tmp = $_FILES['stylebox_file2']['tmp_name'];
                $file_name = $_FILES['stylebox_file2']['name'];
                $file_ext=strtolower(end(explode('.',$file_name)));
                $expensions= array("jpeg","jpg","png","gif");
                if(in_array($file_ext,$expensions)=== false){
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "Invalid Selection of File";
                    return false;
                }else {
                    $file_name = rand(100000, 999999) . "." . $file_ext;
                    move_uploaded_file($file_tmp, "Files/images/giftbox/" . $file_name);
                    $query = "update wp_gift_certificate set gift_for='$title',gift_code ='$code',gift_price='$price',gift_description='$description', 
                    gift_image='$file_name' where g_id = '$g_id'";
                }
            }
            else{
                $query = "update wp_gift_certificate set gift_for='$title',gift_code ='$code',gift_price='$price',gift_description='$description' 
                where g_id = '$g_id'";
            }
            $result = mysqli_query($link, $query);
            if ($result) {
                $this->response[STATUS] = Success;
                $this->response[MESSAGE] = "Data updated successfully!!";
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = $this->link->sqlError();
        }
        return $this->response;
    }

    public function deleteGiftbox($g_id){
        $link = $this->link->connect();
        if($link) {
            $query = "select * from wp_gift_certificate where g_id='$g_id'";
            $result = mysqli_query($link, $query);
            if ($result) {
                $num = mysqli_num_rows($result);
                if ($num > 0) {
                    $update = mysqli_query($link, "delete from wp_gift_certificate WHERE g_id='$g_id'");
                    if ($update) {
                        $this->response[STATUS] = Success;
                        $this->response[MESSAGE] = "Gift Box Has Been Deleted Successfully";
                    } else {
                        $this->response[STATUS] = Error;
                        $this->response[MESSAGE] = $this->link->sqlError();
                    }
                } else {
                    $this->response[STATUS] = Error;
                    $this->response[MESSAGE] = "UnAuthorized Access";
                }
            } else {
                $this->response[STATUS] = Error;
                $this->response[MESSAGE] = $this->link->sqlError();
            }
        }
        else{
            $this->response[STATUS] = Error;
            $this->response[MESSAGE] = "Connection Error";
        }
        return $this->response;
    }

    public function apiResponse($response)
    {
        header("Content-Type: application/json");
        echo json_encode($response);
    }
}