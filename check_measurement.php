<?php
include("header.php");
if ($user_id == "") {
    header("Location:login.php");
}
echo "<input type='hidden' id='meas_id' />";
?>
<style>
    .wthree-3{
        padding: 9em 0 1em;
    }

    .inner_measurementData{
        margin: 20px 0;
    }
    .specification_heading{
        margin:10px 0;
        text-transform: capitalize;
    }
    .inner_measurementData > label{
        font-size: 16px;
    }
    .update_sizematching{
        letter-spacing: 2px;
        width: 150px;
        margin: 10px 0;
    }
    .hiddenValues{
        display: none;
    }

    .matchingTabs > a{
        text-decoration: none!important;
        color:#333;
    }
    .matchingTabs > a:hover{
        cursor: pointer;
    }
    .tabsData{
        display: none;
    }
    .grayHeading{
        color:grey;
    }
    .block {
        display: block !important;
    }
    .loaderhidden {
        display: none !important;
    }
    .loaderdiv {
        background: rgba(0, 0, 0, 0.5) none repeat scroll 0 0;
        height: 700px;
        position: fixed;
        width: 100%;
        z-index: 99999;
        display: none;
    }

    .loaderdiv > img {
        height: 50px;
        margin: 300px 50%;
        width: 50px;
    }

    .savebtn {
        letter-spacing: 1px;
        margin-bottom: 18px;
        text-transform: uppercase;
    }
    .loaderr{
        display: none!important;
    }
    table.table.custom {
        border: 1px solid #ddd;
        text-align:center;
        display:inline-block;
    }
    table.table.custom th {
        border-right:1px solid #ddd;
        text-align:center
    }
    table.table.custom td {
        border-right:1px solid #ddd;
        font-size: 14px;
    }
    .custom-input{
        width: 100%;
        border: 0;
        text-align: center;
        outline:0;
        background: transparent;
        font-size: 13px;
    }
    .table th{
        text-align:center;
    }
    .allowtable{
        padding:0;
    }
    .size_field
    {
        width: 80%;
        text-align: right;
    }
    .matchingTabs:hover{
        cursor: pointer;
    }
    .range{
        font-size: 13px;
    }
    .inner_measurementData>div>span{
        color:#333!important;
    }
    .measurementBox{

    }
</style>
<div class="loaderdiv ">
    <img src="images/preloader.gif"/>
</div>
<div class="about-bottom wthree-3 checkMeasurement">
    <div class="measurementBox col-md-11 col-md-offset-1">
        <div class="row">
            <div class="col-md-6">
                <h2 class="tittle pull-left">Check Measurement</h2>
            </div>
            <div class="col-md-6 ">
                <div class="pull-right">
                    <label>Unit Parameter's : </label>
                    <select class="form-control paramselect" onchange="convertData()" style="width:200px" id="params">
                        <option value="Inches" selected="selected">Inches</option>
                        <option value="Cm">Cm</option>
                    </select>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-md-2">
                <h3 class="specification_heading">
                    Specification :
                </h3>
                <div class="size_specification"></div>
            </div>
            <div class="col-md-10">
                <div class="col-md-12 allowtable" id="shirt_table" style="display:block"></div>
                <div class="col-md-12 allowtable" id="jacket_table" style="display:none"></div>
                <div class="col-md-12 allowtable" id="vest_table" style="display:none"></div>
                <div class="col-md-12 allowtable" id="pant_table" style="display:none"></div>
            </div>
        </div>
        <hr>
        <div class="row">
            <ul class="nav nav-tabs">
                <li role="presentation" class="matchingTabs tabs active" id="jacketallow" onclick="switchMatching('jacket')">
                    <a> Jacket </a>
                </li>
                <li role="presentation" class="matchingTabs tabs" id="pantallow" onclick="switchMatching('pant')">
                    <a> Pant </a>
                </li>
                <li role="presentation" class="matchingTabs tabs" id="shirtallow" onclick="switchMatching('shirt')">
                    <a> Shirt </a>
                </li>
                <li role="presentation" class="matchingTabs tabs" id="vestallow" onclick="switchMatching('vest')">
                    <a> Vest </a>
                </li>

            </ul>
        </div>
        <div id="shirtBox" class="tabsData">
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt collar
                            <span class="range neck_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_collar')"></i>
                            <span id="final_shirt_collar">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_collar')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Shirt chest
                            <span class="range chest_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_chest')"></i>
                            <span id="final_shirt_chest">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_chest')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Shirt waist horizontal line
                            <span  class="range stomach_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_stomach')"></i>
                            <span id="final_shirt_stomach">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_stomach')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Shirt seat
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_seat')"></i>
                            <span id="final_shirt_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_seat')"></i>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt bicep
                            <span class="range bicep_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_bicep')"></i>
                            <span id="final_shirt_bicep">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_bicep')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt front shoulder
                            <span class="range shoulder_textf"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_front_shoulder')"></i>
                            <span id="final_shirt_front_shoulder">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_front_shoulder')"></i>
                            <span id="back_shoulder_body" style="display: none"></span>
                            <span id="front_shoulder_body" style="display: none"></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt back shoulder
                            <span class="range shoulder_textb"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_back_shoulder')"></i>
                            <span id="final_shirt_back_shoulder">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_back_shoulder')"></i>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt back jacket length
                            <span class="range back_lengthtext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_jacket')"></i>
                            <span id="final_shirt_jacket">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_jacket')"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt sleeve left
                            <span class="range sleeve_lefttext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_sleeve_left')"></i>
                            <span id="final_shirt_sleeve_left">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_sleeve_left')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt sleeve Right
                            <span class="range sleeve_righttext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_sleeve_right')"></i>
                            <span id="final_shirt_sleeve_right">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_sleeve_right')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Shirt wrist</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_shirt_wrist')"></i>
                            <span id="final_shirt_wrist">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_shirt_wrist')"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="jacketBox" class="tabsData">
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Jacket chest
                            <span class="range chest_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_chest')"></i>
                            <span  id="final_jacket_chest">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_chest')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Jacket waist horizontal line
                            <span  class="range stomach_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_stomach')"></i>
                            <span class="range" id="final_jacket_stomach">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_stomach')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Jacket seat
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_seat')"></i>
                            <span id="final_jacket_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_seat')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket bicep
                            <span class="range bicep_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_bicep')"></i>
                            <span id="final_jacket_bicep">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_bicep')"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket back shoulder
                            <span class="range shoulder_textb"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_back_shoulder')"></i>
                            <span id="final_jacket_back_shoulder">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_back_shoulder')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket front shoulder
                            <span class="range shoulder_textf"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_front_shoulder')"></i>
                            <span id="final_jacket_front_shoulder">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_front_shoulder')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket sleeve left
                            <span class="range sleeve_lefttext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_sleeve_left')"></i>
                            <span id="final_jacket_sleeve_left">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_sleeve_left')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket sleeve right
                            <span class="range sleeve_righttext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_sleeve_right')"></i>
                            <span id="final_jacket_sleeve_right">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_sleeve_right')"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket Length
                            <span class="range back_lengthtext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_jacket')"></i>
                            <span id="final_jacket_jacket">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_jacket')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Jacket Wrist</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_jacket_wrist')"></i>
                            <span id="final_jacket_wrist">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_jacket_wrist')"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="vestBox" class="tabsData">
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Vest chest
                            <span class="range chest_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_vest_chest')"></i>
                            <span id="final_vest_chest">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_vest_chest')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Vest waist horizontal line
                            <span  class="range stomach_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_vest_stomach')"></i>
                            <span id="final_vest_stomach">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_vest_stomach')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Vest length
                            <span class="range back_lengthtext"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_vest_jacket')"></i>
                            <span id="final_vest_jacket">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_vest_jacket')"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="pantBox" class="tabsData">
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant seat (no pleate)
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_seat')"></i>
                            <span id="final_pant_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_seat')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant seat (one pleate< 2.0 cm)
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant1_seat')"></i>
                            <span id="final_pant1_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant1_seat')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant seat (one pleate in b/w 2.0 and 2.5 cm)
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant11_seat')"></i>
                            <span id="final_pant11_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant11_seat')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant seat (Two Pleat <= 2.5cm)
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant2_seat')"></i>
                            <span id="final_pant2_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant2_seat')"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant seat (Three Pleat <= 2.5cm)
                            <span class="range seat_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant3_seat')"></i>
                            <span id="final_pant3_seat">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant3_seat')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant thigh (no pleat)</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_thigh')"></i>
                            <span id="final_pant_thigh">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_thigh')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="">Pant waist (no pleat)</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_waist')"></i>
                            <span id="final_pant_waist">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_waist')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="grayHeading">Pant calf (no pleat)
                            <span class="range calf_text"></span>
                        </label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_calf')"></i>
                            <span id="final_pant_calf">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_calf')"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="">Pant back waist height (no pleat)</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_b_waist')"></i>
                            <span id="final_pant_b_waist">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_b_waist')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="">Pant front waist height (no pleat)</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_f_waist')"></i>
                            <span id="final_pant_f_waist">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_f_waist')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="">Pant knee (no pleat)</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_knee')"></i>
                            <span id="final_pant_knee">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_knee')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Pant left outsteam</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_p_left')"></i>
                            <span id="final_pant_p_left">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_p_left')"></i>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Pant right outsteam</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_p_right')"></i>
                            <span id="final_pant_p_right">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_p_right')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label class="">Pant bottom</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_bottom')"></i>
                            <span id="final_pant_bottom">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_bottom')"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="inner_measurementData">
                        <label>Pant U-Rise (no pleat)</label>
                        <div>
                            <i class="fa fa-minus" style="margin-right:5px;cursor:pointer" onclick="incdec('minus','final_pant_rise')"></i>
                            <span id="final_pant_rise">0</span>
                            <i class="fa fa-plus" style="margin-left:5px;cursor:pointer" onclick="incdec('plus','final_pant_rise')"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row"></div>
        </div>
        <div class="row">
            <div class="col-md-11">
                <input type="button" class="btn btn-info pull-right savebtn" value="All Measurements" onclick="viewSaved1()" style="margin: 0 10px"/>
                <input type="button" class="btn btn-info pull-right savebtn" value="Save as" onclick="save()" style="margin: 0 10px" />&nbsp;&nbsp;&nbsp;
                <input type="button" class="btn btn-info pull-right savebtn" value="Save Changes" onclick="confirmsave()"  />
            </div>
        </div>
    </div>
</div>
<?php
include("footer.php");
?>
<script>
    var answer = "";
    var ques1, ques2, ques3, ques4, ques5, ques6, svalue, jvalue, pvalue, vvalue, value, answer1, answer2, answer3,
        answer4, answer5, answer6,janswer1, janswer2, janswer3, vanswer1, panswer1, collarshirt, chestshirt,
        chestjacket, chestvest, stomachshirt, stomachjacket, stomachpant, seatshirt, seatjacket, seatpant, bicepshirt,
        bicepjacket,backshouldershirt, backshoulderjacket, frontshouldershirt,frontshoulderjacket, sleeveleftshirt,
        sleeveleftjacket,sleeverightshirt,sleeverightjacket,thighpant,wristshirt,waistpant,calfpant,backwaistheightpant,
        frontwaistheightpant,kneepant,backjacketlengthshirt, backjacketlengthjacket,backjacketlengthvest,risepant,
        pantleftpant,pantrightpant,bottompant,frontshoulderbody, backshoulderbody,chest_min,chest_max,bicep_min,
        bicep_max,back_jacket_length_min,back_jacket_length_max,stomach_min,stomach_max,seat_min,seat_max,
        sleeve_left_min,sleeve_left_max,sleeve_right_min,sleeve_right_max,back_shoulder_min,back_shoulder_max,
        front_shoulder_min,front_shoulder_max,collar_min,collar_max,calf_min,calf_max;
    function getMeasureData(sizeData,meas_id) {
        answer1 = sizeData.shirt_que_1;
        answer2 = sizeData.shirt_que_2;
        answer3 = sizeData.shirt_que_3;
        answer4 = sizeData.shirt_que_4;
        answer5 = sizeData.shirt_que_5;
        answer6 = sizeData.shirt_que_6;
        janswer1 = sizeData.suit_que_1;
        janswer2 = sizeData.suit_que_2;
        janswer3 = sizeData.suit_que_3;
        vanswer1 = sizeData.vest_que_1;
        panswer1 = sizeData.pant_que_1;
        if (sizeData.unit_type == "Inches") {
            var chest = parseFloat(sizeData.chest) * 2.54;///cm conversion
            var stomach = parseFloat(sizeData.stomach) * 2.54;///cm conversion
            ////collar shirt
            if (chest - stomach < 7) {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            collarshirt = svalue;
            jvalue = value.split(";")[1];
            collarjacket = jvalue;
            vvalue = value.split(";")[2];
            collarvest = vvalue;
            pvalue = value.split(";")[3];
            collarpant = pvalue;
            var collar_shirt = parseFloat(svalue);
            $("#collar_shirt_allow").html((collar_shirt / 2.54).toFixed(1) + " Inches");
            // for collar jacket
            var collar_jacket = (parseFloat(jvalue));
            $("#collar_jacket_allow").html((collar_jacket / 2.54).toFixed(1) + " Inches");
            // for collar vest
            var collar_vest = (parseFloat(vvalue));
            $("#collar_vest_allow").html((collar_vest / 2.54).toFixed(1) + " Inches");
            // for collar pant
            var collar_pant = (parseFloat(pvalue));
            $("#collar_pant_allow").html((collar_pant / 2.54).toFixed(1) + " Inches");
            //for chest shirt
            if (chest - stomach < 7) {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            chestshirt = svalue;
            jvalue = value.split(";")[1];
            chestjacket = jvalue;
            vvalue = value.split(";")[2];
            chestvest = vvalue;
            pvalue = value.split(";")[3];
            chestpant = pvalue;
            // for chest shirt
            var chest_shirt = parseFloat(svalue);
            $("#chest_shirt_allow").html((chest_shirt / 2.54).toFixed(1) + " Inches");
            // for chest jacket
            var chest_jacket = (parseFloat(jvalue));
            $("#chest_jacket_allow").html((chest_jacket / 2.54).toFixed(1) + " Inches");
            //for chest vest
            var chest_vest = parseFloat(vvalue);
            $("#chest_vest_allow").html((chest_vest / 2.54).toFixed(1) + " Inches");
            //for chest pant
            var chest_pant = (parseFloat(pvalue));
            $("#chest_pant_allow").html((chest_pant / 2.54).toFixed(1) + " Inches");

            ////////for Stomach shirt
            if (chest - stomach < 7) {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            stomachshirt = svalue;
            jvalue = value.split(";")[1];
            stomachjacket = jvalue;
            vvalue = value.split(";")[2];
            stomachvest = vvalue;
            pvalue = value.split(";")[3];
            stomachpant = pvalue;
            var stomach_shirt = parseFloat(svalue);
            $("#stomach_shirt_allow").html((stomach_shirt / 2.54).toFixed(1) + " Inches");
            // for stomach jacket
            var stomach_jacket = (parseFloat(jvalue));
            $("#stomach_jacket_allow").html((stomach_jacket / 2.54).toFixed(1) + " Inches");
            // for stomach vest
            var stomach_vest = (parseFloat(vvalue));
            $("#stomach_vest_allow").html((stomach_vest / 2.54).toFixed(1) + " Inches");
            // for stomach pant
            var stomach_pant = (parseFloat(pvalue));
            $("#stomach_pant_allow").html((stomach_pant / 2.54).toFixed(1) + " Inches");

            //for seat shirt
            if (chest - stomach < 7) {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            seatshirt = svalue;
            jvalue = value.split(";")[1];
            seatjacket = jvalue;
            vvalue = value.split(";")[2];
            seatvest = vvalue;
            pvalue = value.split(";")[3];
            seatpant = pvalue;
            // for seat shirt
            var seat_shirt = parseFloat(svalue);
            $("#seat_shirt_allow").html((seat_shirt / 2.54).toFixed(1) + " Inches");
            // for seat jacket
            var seat_jacket = (parseFloat(jvalue));
            $("#seat_jacket_allow").html((seat_jacket / 2.54).toFixed(1) + " Inches");
            // for seat vest
            var seat_vest = parseFloat(vvalue);
            $("#seat_vest_allow").html((seat_vest / 2.54).toFixed(1) + " Inches");
            // for seat_pant
            var seat_pant = (parseFloat(pvalue));
            $("#seat_pant_allow").html((seat_pant / 2.54).toFixed(1) + " Inches");

            ///////for bicep shirt
            if (chest - stomach < 7) {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bicepshirt = svalue;
            jvalue = value.split(";")[1];
            bicepjacket = jvalue;
            vvalue = value.split(";")[2];
            bicepvest = vvalue;
            pvalue = value.split(";")[3];
            biceppant = pvalue;
            var bicep_shirt = parseFloat(svalue);
            $("#bicep_shirt_allow").html((bicep_shirt / 2.54).toFixed(1) + " Inches");
            //for bicep jacket
            var bicep_jacket = (parseFloat(jvalue));
            $("#bicep_jacket_allow").html((bicep_jacket / 2.54).toFixed(1) + " Inches");
            // for bicep vest
            var bicep_vest = parseFloat(vvalue);
            $("#bicep_vest_allow").html((bicep_vest / 2.54).toFixed(1) + " Inches");
            //for bicep pant
            var bicep_pant = (parseFloat(pvalue));
            $("#bicep_pant_allow").html((bicep_pant / 2.54).toFixed(1) + " Inches");

            //for back shoulder shirt
            if (chest - stomach < 7) {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backshouldershirt = svalue;
            jvalue = value.split(";")[1];
            backshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            backshouldervest = vvalue;
            pvalue = value.split(";")[3];
            backshoulderpant = pvalue;
            //for back shoulder shirt
            var back_shoulder_shirt = parseFloat(svalue);
            $("#back_shoulder_shirt_allow").html((back_shoulder_shirt / 2.54).toFixed(1) + " Inches");
            //for back shoulder jacket
            var back_shoulder_jacket = (parseFloat(jvalue));
            $("#back_shoulder_jacket_allow").html((back_shoulder_jacket / 2.54).toFixed(1) + " Inches");
            // for back shoulder vest
            var back_shoulder_vest = parseFloat(vvalue);
            $("#back_shoulder_vest_allow").html((back_shoulder_vest / 2.54).toFixed(1) + " Inches");
            //for back shoulder pant
            var back_shoulder_pant = (parseFloat(pvalue));
            $("#back_shoulder_pant_allow").html((back_shoulder_pant / 2.54).toFixed(1) + " Inches");

            ////////for front shoulder shirt
            if (chest - stomach < 7) {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontshouldershirt = svalue;
            jvalue = value.split(";")[1];
            frontshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            frontshouldervest = vvalue;
            pvalue = value.split(";")[3];
            frontshoulderpant = pvalue;
            // for front shoulder shirt
            var front_shoulder_shirt = parseFloat(svalue);
            $("#front_shoulder_shirt_allow").html((front_shoulder_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var front_shoulder_jacket = (parseFloat(jvalue));
            $("#front_shoulder_jacket_allow").html((front_shoulder_jacket / 2.54).toFixed(1) + " Inches");
            // for front shoulder vest
            var front_shoulder_vest = parseFloat(vvalue);
            $("#front_shoulder_vest_allow").html((front_shoulder_vest / 2.54).toFixed(1) + " Inches");
            // for front shoulder pant
            var front_shoulder_pant = (parseFloat(pvalue));
            $("#front_shoulder_pant_allow").html((front_shoulder_pant / 2.54).toFixed(1) + " Inches");

            //for left_sleeve_length shirt
            if (chest - stomach < 7) {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeveleftshirt = svalue;
            jvalue = value.split(";")[1];
            sleeveleftjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeveleftvest = vvalue;
            pvalue = value.split(";")[3];
            sleeveleftpant = pvalue;
            var left_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_left_shirt_allow").html((left_sleeve_length_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var left_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_left_jacket_allow").html((left_sleeve_length_jacket / 2.54).toFixed(1) + " Inches");
            //for front shoulder vest
            var left_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_left_vest_allow").html((left_sleeve_length_vest / 2.54).toFixed(1) + " Inches");
            //for front shoulder pant
            var left_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_left_pant_allow").html((left_sleeve_length_pant / 2.54).toFixed(1) + " Inches");

            /////for right_sleeve_length shirt
            if (chest - stomach < 7) {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeverightshirt = svalue;
            jvalue = value.split(";")[1];
            sleeverightjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeverightvest = vvalue;
            pvalue = value.split(";")[3];
            sleeverightpant = pvalue;
            //for right_sleeve_length shirt
            var right_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_right_shirt_allow").html((right_sleeve_length_shirt / 2.54).toFixed(1) + " Inches");
            //for right_sleeve_length jacket
            var right_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_right_jacket_allow").html((right_sleeve_length_jacket / 2.54).toFixed(1) + " Inches");
            //for right_sleeve_length vest
            var right_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_right_vest_allow").html((right_sleeve_length_vest / 2.54).toFixed(1) + " Inches");
            //for right_sleeve_length pant
            var right_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_right_pant_allow").html((right_sleeve_length_pant / 2.54).toFixed(1) + " Inches");

            //// for thigh shirt
            if (chest - stomach < 7) {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            thighshirt = svalue;
            jvalue = value.split(";")[1];
            thighjacket = jvalue;
            vvalue = value.split(";")[2];
            thighvest = vvalue;
            pvalue = value.split(";")[3];
            thighpant = pvalue;
            //for thigh shirt
            var thigh_shirt = parseFloat(svalue);
            $("#thigh_shirt_allow").html((thigh_shirt / 2.54).toFixed(1) + " Inches");
            //for thigh jacket
            var thigh_jacket = (parseFloat(jvalue));
            $("#thigh_jacket_allow").html((thigh_jacket / 2.54).toFixed(1) + " Inches");
            // for thigh vest
            var thigh_vest = parseFloat(vvalue);
            $("#thigh_vest_allow").html((thigh_vest / 2.54).toFixed(1) + " Inches");
            //for thigh pant
            var thigh_pant = (parseFloat(pvalue));
            $("#thigh_pant_allow").html((thigh_pant / 2.54).toFixed(1) + " Inches");

            //for nape shirt
            if (chest - stomach < 7) {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            napetowaistshirt = svalue;
            jvalue = value.split(";")[1];
            napetowaistjacket = jvalue;
            vvalue = value.split(";")[2];
            napetowaistvest = vvalue;
            pvalue = value.split(";")[3];
            napetowaistpant = pvalue;
            var nape_to_waist_shirt = parseFloat(svalue);
            $("#nape_shirt_allow").html((nape_to_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var nape_to_waist_jacket = (parseFloat(jvalue));
            $("#nape_jacket_allow").html((nape_to_waist_jacket / 2.54).toFixed(1) + " Inches");
            // for nape_to_waist vest
            var nape_to_waist_vest = parseFloat(vvalue);
            $("#nape_vest_allow").html((nape_to_waist_vest / 2.54).toFixed(1) + " Inches");
            //for nape_to_waist pant
            var nape_to_waist_pant = (parseFloat(pvalue));
            $("#nape_pant_allow").html((nape_to_waist_pant / 2.54).toFixed(1) + " Inches");

            ////////for front_waist shirt
            if (chest - stomach < 7) {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistlengthshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistlengthvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistlengthpant = pvalue;
            //for front shoulder shirt
            var front_waist_shirt = parseFloat(svalue);
            $("#front_waist_shirt_allow").html((front_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var front_waist_jacket = (parseFloat(jvalue));
            $("#front_waist_jacket_allow").html((front_waist_jacket / 2.54).toFixed(1) + " Inches");
            // for front shoulder vest
            var front_waist_vest = parseFloat(vvalue);
            $("#front_waist_vest_allow").html((front_waist_vest / 2.54).toFixed(1) + " Inches");
            // for front shoulder pant
            var front_waist_pant = (parseFloat(jvalue));
            $("#front_waist_pant_allow").html((front_waist_pant / 2.54).toFixed(1) + " Inches");

            //for wrist shirt
            if (chest - stomach < 7) {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            wristshirt = svalue;
            jvalue = value.split(";")[1];
            wristjacket = jvalue;
            vvalue = value.split(";")[2];
            wristvest = vvalue;
            pvalue = value.split(";")[3];
            wristpant = pvalue;
            var wrist_shirt = parseFloat(svalue);
            $("#wrist_shirt_allow").html((wrist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var wrist_jacket = (parseFloat(jvalue));
            $("#wrist_jacket_allow").html((wrist_jacket / 2.54).toFixed(1) + " Inches");
            // for wrist vest
            var wrist_vest = parseFloat(vvalue);
            $("#wrist_vest_allow").html((wrist_vest / 2.54).toFixed(1) + " Inches");
            //for wrist pant
            var wrist_pant = (parseFloat(pvalue));
            $("#wrist_pant_allow").html((wrist_pant / 2.54).toFixed(1) + " Inches");

            ///////for waist shirt
            if (chest - stomach < 7) {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            waistshirt = svalue;
            jvalue = value.split(";")[1];
            waistjacket = jvalue;
            vvalue = value.split(";")[2];
            waistvest = vvalue;
            pvalue = value.split(";")[3];
            waistpant = pvalue;
            var waist_shirt = parseFloat(svalue);
            $("#waist_shirt_allow").html((waist_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var waist_jacket = (parseFloat(jvalue));
            $("#waist_jacket_allow").html((waist_jacket / 2.54).toFixed(1) + " Inches");
            // for waist vest
            var waist_vest = parseFloat(vvalue);
            $("#waist_vest_allow").html((waist_vest / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var waist_pant = (parseFloat(pvalue));
            $("#waist_pant_allow").html((waist_pant / 2.54).toFixed(1) + " Inches");

            //for calf shirt
            if (chest - stomach < 7) {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            calfshirt = svalue;
            jvalue = value.split(";")[1];
            calfjacket = jvalue;
            vvalue = value.split(";")[2];
            calfvest = vvalue;
            pvalue = value.split(";")[3];
            calfpant = pvalue;
            var calf_shirt = parseFloat(svalue);
            $("#calf_shirt_allow").html((calf_shirt / 2.54).toFixed(1) + " Inches");
            //for front shoulder jacket
            var calf_jacket = (parseFloat(jvalue));
            $("#calf_jacket_allow").html((calf_jacket / 2.54).toFixed(1) + " Inches");
            // for calf vest
            var calf_vest = parseFloat(vvalue);
            $("#calf_vest_allow").html((calf_vest / 2.54).toFixed(1) + " Inches");
            //for calf pant
            var calf_pant = (parseFloat(pvalue));
            $("#calf_pant_allow").html((calf_pant / 2.54).toFixed(1) + " Inches");

            ////////for b_waist_shirt
            if (chest - stomach < 7) {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            backwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            backwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            backwaistheightpant = pvalue;
            //for b_waist shirt
            var b_waist_shirt = parseFloat(svalue);
            $("#b_waist_shirt_allow").html((b_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for b_waist jacket
            var b_waist_jacket = (parseFloat(jvalue));
            $("#b_waist_jacket_allow").html((b_waist_jacket / 2.54).toFixed(1) + " Inches");
            //for b_waist vest
            var b_waist_vest = parseFloat(vvalue);
            $("#b_waist_vest_allow").html((b_waist_vest / 2.54).toFixed(1) + " Inches");
            //for b_waist_pant
            var b_waist_pant = (parseFloat(pvalue));
            $("#b_waist_pant_allow").html((b_waist_pant / 2.54).toFixed(1) + " Inches");

            //for f_waist_shirt
            if (chest - stomach < 7) {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistheightpant = pvalue;
            //for f_waist shirt
            var f_waist_shirt = parseFloat(svalue);
            $("#f_waist_shirt_allow").html((f_waist_shirt / 2.54).toFixed(1) + " Inches");
            //for f_waist jacket
            var f_waist_jacket = (parseFloat(jvalue));
            $("#f_waist_jacket_allow").html((f_waist_jacket / 2.54).toFixed(1) + " Inches");
            //for f_waist vest
            var f_waist_vest = parseFloat(vvalue);
            $("#f_waist_vest_allow").html((f_waist_vest / 2.54).toFixed(1) + " Inches");
            //for f_waist pant
            var f_waist_pant = (parseFloat(pvalue));
            $("#f_waist_pant_allow").html((f_waist_pant / 2.54).toFixed(1) + " Inches");

            /////for knee shirt
            if (chest - stomach < 7) {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            kneeshirt = svalue;
            jvalue = value.split(";")[1];
            kneejacket = jvalue;
            vvalue = value.split(";")[2];
            kneevest = vvalue;
            pvalue = value.split(";")[3];
            kneepant = pvalue;
            //for knee shirt
            var knee_shirt = parseFloat(svalue);
            $("#knee_shirt_allow").html((knee_shirt / 2.54).toFixed(1) + " Inches");
            //for knee jacket
            var knee_jacket = (parseFloat(jvalue));
            $("#knee_jacket_allow").html((knee_jacket / 2.54).toFixed(1) + " Inches");
            //for knee vest
            var knee_vest = parseFloat(vvalue);
            $("#knee_vest_allow").html((knee_vest / 2.54).toFixed(1) + " Inches");
            //for knee pant
            var knee_pant = (parseFloat(pvalue));
            $("#knee_pant_allow").html((knee_pant / 2.54).toFixed(1) + " Inches");

            //for back_jacket_length shirt
            if (chest - stomach < 7) {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backjacketlengthshirt = svalue;
            jvalue = value.split(";")[1];
            backjacketlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            backjacketlengthvest = vvalue;
            pvalue = value.split(";")[3];
            backjacketlengthpant = pvalue;
            //for back_jacket_length shirt
            var jacket_shirt = parseFloat(svalue);
            $("#jacket_shirt_allow").html((jacket_shirt / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length jacket
            var jacket_jacket = (parseFloat(jvalue));
            $("#jacket_jacket_allow").html((jacket_jacket / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length vest
            var jacket_vest = parseFloat(vvalue);
            $("#jacket_vest_allow").html((jacket_vest / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length pant
            var jacket_pant = (parseFloat(pvalue));
            $("#jacket_pant_allow").html((jacket_pant / 2.54).toFixed(1) + " Inches");

            //for u_rise shirt
            panswer1 = sizeData.pant_que_1;
            if (chest - stomach < 7) {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            riseshirt = svalue;
            jvalue = value.split(";")[1];
            risejacket = jvalue;
            vvalue = value.split(";")[2];
            risevest = vvalue;
            pvalue = value.split(";")[3];
            risepant = pvalue;
            //for rise shirt
            var rise_shirt = parseFloat(svalue);
            $("#rise_shirt_allow").html((rise_shirt / 2.54).toFixed(1) + " Inches");
            //for rise jacket
            var rise_jacket = (parseFloat(jvalue));
            $("#rise_jacket_allow").html((rise_jacket / 2.54).toFixed(1) + " Inches");
            // for rise vest
            var rise_vest = parseFloat(vvalue);
            $("#rise_vest_allow").html((rise_vest / 2.54).toFixed(1) + " Inches");
            //for rise pant
            var rise_pant = (parseFloat(pvalue));
            $("#rise_pant_allow").html((rise_pant / 2.54).toFixed(1) + " Inches");

            ////////for pant_left_shirt
            if (chest - stomach < 7) {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantleftshirt = svalue;
            jvalue = value.split(";")[1];
            pantleftjacket = jvalue;
            vvalue = value.split(";")[2];
            pantleftvest = vvalue;
            pvalue = value.split(";")[3];
            pantleftpant = pvalue;
            var p_left_shirt = parseFloat(svalue);
            $("#p_left_shirt_allow").html((p_left_shirt / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length jacket
            var p_left_jacket = (parseFloat(jvalue));
            $("#p_left_jacket_allow").html((p_left_jacket / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length vest
            var p_left_vest = parseFloat(vvalue);
            $("#p_left_vest_allow").html((p_left_vest / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length pant
            var p_left_pant = (parseFloat(pvalue));
            $("#p_left_pant_allow").html((p_left_pant / 2.54).toFixed(1) + " Inches");

            //for p_right shirt
            if (chest - stomach < 7) {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantrightshirt = svalue;
            jvalue = value.split(";")[1];
            pantrightjacket = jvalue;
            vvalue = value.split(";")[2];
            pantrightvest = vvalue;
            pvalue = value.split(";")[3];
            pantrightpant = pvalue;
            var p_right_shirt = parseFloat(svalue);
            $("#p_right_shirt_allow").html((p_right_shirt / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length jacket
            var p_right_jacket = (parseFloat(jvalue));
            $("#p_right_jacket_allow").html((p_right_jacket / 2.54).toFixed(1) + " Inches");
            // for p_right vest
            var p_right_vest = parseFloat(vvalue);
            $("#p_right_vest_allow").html((p_right_vest / 2.54).toFixed(1) + " Inches");
            // for p_right pant
            var p_right_pant = (parseFloat(pvalue));
            $("#p_right_pant_allow").html((p_right_pant / 2.54).toFixed(1) + " Inches");

            /////for bottom shirt
            if (chest - stomach < 7) {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bottomshirt = svalue;
            jvalue = value.split(";")[1];
            bottomjacket = jvalue;
            vvalue = value.split(";")[2];
            bottomvest = vvalue;
            pvalue = value.split(";")[3];
            bottompant = pvalue;
            var bottom_shirt = parseFloat(svalue);
            $("#bottom_shirt_allow").html((bottom_shirt / 2.54).toFixed(1) + " Inches");
            //for bottom jacket
            var bottom_jacket = (parseFloat(jvalue));
            $("#bottom_jacket_allow").html((bottom_jacket / 2.54).toFixed(1) + " Inches");
            // for bottom vest
            var bottom_vest = parseFloat(vvalue);
            $("#bottom_vest_allow").html((bottom_vest / 2.54).toFixed(1) + " Inches");
            // for bottom pant
            var bottom_pant = (parseFloat(pvalue));
            $("#bottom_pant_allow").html((bottom_pant / 2.54).toFixed(1) + " Inches");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
        else {
            ///cm from backend need to conversion into inches
            var chest = parseFloat(sizeData.chest);///cm conversion
            var stomach = parseFloat(sizeData.stomach);///cm conversion
            ////collar shirt
            if (chest - stomach < 7) {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("collar", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            collarshirt = svalue;
            jvalue = value.split(";")[1];
            collarjacket = jvalue;
            vvalue = value.split(";")[2];
            collarvest = vvalue;
            pvalue = value.split(";")[3];
            collarpant = pvalue;
            //for collar shirt
            var collar_shirt = parseFloat(svalue);
            $("#collar_shirt_allow").html(collar_shirt + " Cm");
            // for collar jacket
            var collar_jacket = (parseFloat(jvalue));
            $("#collar_jacket_allow").html(collar_jacket + " Cm");
            // for collar vest
            var collar_vest = (parseFloat(vvalue));
            $("#collar_vest_allow").html(collar_vest + " Cm");
            // for collar pant
            var collar_pant = (parseFloat(pvalue));
            $("#collar_pant_allow").html(collar_pant + " Cm");

            //for chest shirt
            if (chest - stomach < 7) {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("chest", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            chestshirt = svalue;
            jvalue = value.split(";")[1];
            chestjacket = jvalue;
            vvalue = value.split(";")[2];
            chestvest = vvalue;
            pvalue = value.split(";")[3];
            chestpant = pvalue;
            //for chest shirt
            var chest_shirt = parseFloat(svalue);
            $("#chest_shirt_allow").html(chest_shirt + " Cm");
            // for chest jacket
            var chest_jacket = (parseFloat(jvalue));
            $("#chest_jacket_allow").html(chest_jacket + " Cm");
            //for chest vest
            var chest_vest = parseFloat(vvalue);
            $("#chest_vest_allow").html(chest_vest + " Cm");
            // for chest pant
            var chest_pant = (parseFloat(pvalue));
            $("#chest_pant_allow").html(chest_pant + " Cm");

            ////////for Stomach shirt
            if (chest - stomach < 7) {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("stomach", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            stomachshirt = svalue;
            jvalue = value.split(";")[1];
            stomachjacket = jvalue;
            vvalue = value.split(";")[2];
            stomachvest = vvalue;
            pvalue = value.split(";")[3];
            stomachpant = pvalue;
            // for stomach shirt
            var stomach_shirt = parseFloat(svalue);
            $("#stomach_shirt_allow").html(stomach_shirt + " Cm");
            // for stomach jacket
            var stomach_jacket = (parseFloat(jvalue));
            $("#stomach_jacket_allow").html(stomach_jacket + " Cm");
            // for stomach vest
            var stomach_vest = parseFloat(vvalue);
            $("#stomach_vest_allow").html(stomach_vest + " Cm");
            // for stomach pant
            var stomach_pant = (parseFloat(pvalue));
            $("#stomach_pant_allow").html(stomach_pant + " Cm");

            //for seat shirt
            if (chest - stomach < 7) {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("seat", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            seatshirt = svalue;
            jvalue = value.split(";")[1];
            seatjacket = jvalue;
            vvalue = value.split(";")[2];
            seatvest = vvalue;
            pvalue = value.split(";")[3];
            seatpant = pvalue;
            var seat_shirt = parseFloat(svalue);
            $("#seat_shirt_allow").html(seat_shirt + " Cm");
            // for seat jacket
            var seat_jacket = (parseFloat(jvalue));
            $("#seat_jacket_allow").html(seat_jacket + " Cm");
            //for seat vest
            var seat_vest = parseFloat(vvalue);
            $("#seat_vest_allow").html(seat_vest + " Cm");
            //for seat pant
            var seat_pant = (parseFloat(pvalue));
            $("#seat_pant_allow").html(seat_pant + " Cm");

            ///////for bicep shirt
            if (chest - stomach < 7) {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bicep", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bicepshirt = svalue;
            jvalue = value.split(";")[1];
            bicepjacket = jvalue;
            vvalue = value.split(";")[2];
            bicepvest = vvalue;
            pvalue = value.split(";")[3];
            biceppant = pvalue;
            var bicep_shirt = parseFloat(svalue);
            $("#bicep_shirt_allow").html(bicep_shirt + " Cm");
            //for bicep jacket
            var bicep_jacket = (parseFloat(jvalue));
            $("#bicep_jacket_allow").html(bicep_jacket + " Cm");
            //for bicep vest
            var bicep_vest = parseFloat(vvalue);
            $("#bicep_vest_allow").html(bicep_vest + " Cm");
            //for bicep pant
            var bicep_pant = (parseFloat(pvalue));
            $("#bicep_pant_allow").html(bicep_pant + " Cm");

            //for back shoulder shirt
            if (chest - stomach < 7) {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backshouldershirt = svalue;
            jvalue = value.split(";")[1];
            backshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            backshouldervest = vvalue;
            pvalue = value.split(";")[3];
            backshoulderpant = pvalue;
            var back_shoulder_shirt = parseFloat(svalue);
            $("#back_shoulder_shirt_allow").html(back_shoulder_shirt + " Cm");
            //for back shoulder jacket
            var back_shoulder_jacket = (parseFloat(jvalue));
            $("#back_shoulder_jacket_allow").html(back_shoulder_jacket + " Cm");
            //for back shoulder vest
            var back_shoulder_vest = parseFloat(vvalue);
            $("#back_shoulder_vest_allow").html(back_shoulder_vest + " Cm");
            //for back shoulder pant
            var back_shoulder_pant = (parseFloat(pvalue));
            $("#back_shoulder_pant_allow").html(back_shoulder_pant + " Cm");

            ////////for front shoulder shirt
            if (chest - stomach < 7) {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_shoulder_width", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontshouldershirt = svalue;
            jvalue = value.split(";")[1];
            frontshoulderjacket = jvalue;
            vvalue = value.split(";")[2];
            frontshouldervest = vvalue;
            pvalue = value.split(";")[3];
            frontshoulderpant = pvalue;
            var front_shoulder_shirt = parseFloat(svalue);
            $("#front_shoulder_shirt_allow").html(front_shoulder_shirt + " Cm");
            //for front shoulder jacket
            var front_shoulder_jacket = (parseFloat(jvalue));
            $("#front_shoulder_jacket_allow").html(front_shoulder_jacket + " Cm");
            // for front shoulder vest
            var front_shoulder_vest = parseFloat(vvalue);
            $("#front_shoulder_vest_allow").html(front_shoulder_vest + " Cm");
            //for front shoulder pant
            var front_shoulder_pant = (parseFloat(pvalue));
            $("#front_shoulder_pant_allow").html(front_shoulder_pant + " Cm");

            //for left_sleeve_length shirt
            if (chest - stomach < 7) {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("left_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeveleftshirt = svalue;
            jvalue = value.split(";")[1];
            sleeveleftjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeveleftvest = vvalue;
            pvalue = value.split(";")[3];
            sleeveleftpant = pvalue;
            var left_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_left_shirt_allow").html(left_sleeve_length_shirt + " Cm");
            //for sleeve length left jacket
            var left_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_left_jacket_allow").html(left_sleeve_length_jacket + " Cm");
            //for sleeve length left vest
            var left_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_left_vest_allow").html(left_sleeve_length_vest + " Cm");
            //for sleeve length left pant
            var left_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_left_pant_allow").html(left_sleeve_length_pant + " Cm");

            /////for right_sleeve_length shirt
            if (chest - stomach < 7) {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("right_sleeve_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            sleeverightshirt = svalue;
            jvalue = value.split(";")[1];
            sleeverightjacket = jvalue;
            vvalue = value.split(";")[2];
            sleeverightvest = vvalue;
            pvalue = value.split(";")[3];
            sleeverightpant = pvalue;
            var right_sleeve_length_shirt = parseFloat(svalue);
            $("#sleeve_right_shirt_allow").html(right_sleeve_length_shirt + " Cm");
            //for sleeve length right jacket
            var right_sleeve_length_jacket = (parseFloat(jvalue));
            $("#sleeve_right_jacket_allow").html(right_sleeve_length_jacket + " Cm");
            //for sleeve length right vest
            var right_sleeve_length_vest = parseFloat(vvalue);
            $("#sleeve_right_vest_allow").html(right_sleeve_length_vest + " Cm");
            //for sleeve length right pant
            var right_sleeve_length_pant = (parseFloat(pvalue));
            $("#sleeve_right_pant_allow").html(right_sleeve_length_pant + " Cm");

            //// for thigh shirt
            if (chest - stomach < 7) {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("thigh", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            thighshirt = svalue;
            jvalue = value.split(";")[1];
            thighjacket = jvalue;
            vvalue = value.split(";")[2];
            thighvest = vvalue;
            pvalue = value.split(";")[3];
            thighpant = pvalue;
            //for thigh shirt
            var thigh_shirt = parseFloat(svalue);
            $("#thigh_shirt_allow").html(thigh_shirt + " Cm");
            //for thigh jacket
            var thigh_jacket = (parseFloat(jvalue));
            $("#thigh_jacket_allow").html(thigh_jacket + " Cm");
            //for thigh vest
            var thigh_vest = parseFloat(vvalue);
            $("#thigh_vest_allow").html(thigh_vest + " Cm");
            //for thigh pant
            var thigh_pant = (parseFloat(pvalue));
            $("#thigh_pant_allow").html(thigh_pant + " Cm");

            //for nape shirt
            if (chest - stomach < 7) {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("nape_to_waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            napetowaistshirt = svalue;
            jvalue = value.split(";")[1];
            napetowaistjacket = jvalue;
            vvalue = value.split(";")[2];
            napetowaistvest = vvalue;
            pvalue = value.split(";")[3];
            napetowaistpant = pvalue;
            //for nape to waist shirt
            var nape_to_waist_shirt = parseFloat(svalue);
            $("#nape_shirt_allow").html(nape_to_waist_shirt + " Cm");
            //for nape to waist jacket
            var nape_to_waist_jacket = (parseFloat(jvalue));
            $("#nape_jacket_allow").html(nape_to_waist_jacket + " Cm");
            //for nape to waist vest
            var nape_to_waist_vest = parseFloat(vvalue);
            $("#nape_vest_allow").html(nape_to_waist_vest + " Cm");
            //for nape to waist pant
            var nape_to_waist_pant = (parseFloat(pvalue));
            $("#nape_pant_allow").html(nape_to_waist_pant + " Cm");

            ////////for front_waist shirt
            if (chest - stomach < 7) {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistlengthshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistlengthvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistlengthpant = pvalue;
            //for front shoulder shirt
            var front_waist_shirt = parseFloat(svalue);
            $("#front_waist_shirt_allow").html(front_waist_shirt + " Cm");
            //for front shoulder jacket
            var front_waist_jacket = (parseFloat(jvalue));
            $("#front_waist_jacket_allow").html(front_waist_jacket + " Cm");
            //for front shoulder vest
            var front_waist_vest = parseFloat(vvalue);
            $("#front_waist_vest_allow").html(front_waist_vest + " Cm");
            //for front shoulder pant
            var front_waist_pant = (parseFloat(pvalue));
            $("#front_waist_pant_allow").html(front_waist_pant + " Cm");

            //for wrist shirt
            if (chest - stomach < 7) {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("wrist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            wristshirt = svalue;
            jvalue = value.split(";")[1];
            wristjacket = jvalue;
            vvalue = value.split(";")[2];
            wristvest = vvalue;
            pvalue = value.split(";")[3];
            wristpant = pvalue;
            //for front shoulder shirt
            var wrist_shirt = parseFloat(svalue);
            $("#wrist_shirt_allow").html(wrist_shirt + " Cm");
            //for front shoulder jacket
            var wrist_jacket = (parseFloat(jvalue));
            $("#wrist_jacket_allow").html(wrist_jacket + " Cm");
            //for front shoulder vest
            var wrist_vest = parseFloat(vvalue);
            $("#wrist_vest_allow").html(wrist_vest + " Cm");
            //for front shoulder pant
            var wrist_pant = (parseFloat(pvalue));
            $("#wrist_pant_allow").html(wrist_pant + " Cm");

            ///////for waist shirt
            if (chest - stomach < 7) {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("waist", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            waistshirt = svalue;
            jvalue = value.split(";")[1];
            waistjacket = jvalue;
            vvalue = value.split(";")[2];
            waistvest = vvalue;
            pvalue = value.split(";")[3];
            waistpant = pvalue;
            //for front shoulder shirt
            var waist_shirt = parseFloat(svalue);
            $("#waist_shirt_allow").html(waist_shirt + " Cm");
            //for front shoulder jacket
            var waist_jacket = (parseFloat(jvalue));
            $("#waist_jacket_allow").html(waist_jacket + " Cm");
            //for front shoulder vest
            var waist_vest = parseFloat(vvalue);
            $("#waist_vest_allow").html(waist_vest + " Cm");
            //for front shoulder pant
            var waist_pant = (parseFloat(pvalue));
            $("#waist_pant_allow").html(waist_pant + " Cm");

            //for calf shirt
            if (chest - stomach < 7) {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("calf", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            calfshirt = svalue;
            jvalue = value.split(";")[1];
            calfjacket = jvalue;
            vvalue = value.split(";")[2];
            calfvest = vvalue;
            pvalue = value.split(";")[3];
            calfpant = pvalue;
            //for front shoulder shirt
            var calf_shirt = parseFloat(svalue);
            $("#calf_shirt_allow").html(calf_shirt + " Cm");
            //for front shoulder jacket
            var calf_jacket = (parseFloat(jvalue));
            $("#calf_jacket_allow").html(calf_jacket + " Cm");
            //for front shoulder vest
            var calf_vest = parseFloat(vvalue);
            $("#calf_vest_allow").html(calf_vest + " Cm");
            //for front shoulder pant
            var calf_pant = (parseFloat(pvalue));
            $("#calf_pant_allow").html(calf_pant + " Cm");

            ////////for b_waist_shirt
            if (chest - stomach < 7) {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            backwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            backwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            backwaistheightpant = pvalue;
            //for waist shirt
            var b_waist_shirt = parseFloat(svalue);
            $("#b_waist_shirt_allow").html(b_waist_shirt + " Cm");
            //for waist jacket
            var b_waist_jacket = (parseFloat(jvalue));
            $("#b_waist_jacket_allow").html(b_waist_jacket + " Cm");
            //for waist vest
            var b_waist_vest = parseFloat(vvalue);
            $("#b_waist_vest_allow").html(b_waist_vest + " Cm");
            //for waist pant
            var b_waist_pant = (parseFloat(pvalue));
            $("#b_waist_pant_allow").html(b_waist_pant + " Cm");

            //for f_waist_shirt
            if (chest - stomach < 7) {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("front_waist_height", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            frontwaistheightshirt = svalue;
            jvalue = value.split(";")[1];
            frontwaistheightjacket = jvalue;
            vvalue = value.split(";")[2];
            frontwaistheightvest = vvalue;
            pvalue = value.split(";")[3];
            frontwaistheightpant = pvalue;
            //for front shoulder shirt
            var f_waist_shirt = parseFloat(svalue);
            $("#f_waist_shirt_allow").html(f_waist_shirt + " Cm");
            //for front shoulder jacket
            var f_waist_jacket = parseFloat(jvalue);
            $("#f_waist_jacket_allow").html(f_waist_jacket + " Cm");
            //for front shoulder vest
            var f_waist_vest = parseFloat(vvalue);
            $("#f_waist_vest_allow").html(f_waist_vest + " Cm");
            //for front shoulder pant
            var f_waist_pant = parseFloat(pvalue);
            $("#f_waist_pant_allow").html(f_waist_pant + " Cm");

            /////for knee shirt
            if (chest - stomach < 7) {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("knee", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            kneeshirt = svalue;
            jvalue = value.split(";")[1];
            kneejacket = jvalue;
            vvalue = value.split(";")[2];
            kneevest = vvalue;
            pvalue = value.split(";")[3];
            kneepant = pvalue;
            //for front shoulder shirt
            var knee_shirt = parseFloat(svalue);
            $("#knee_shirt_allow").html(knee_shirt + " Cm");
            //for front shoulder jacket
            var knee_jacket = (parseFloat(jvalue));
            $("#knee_jacket_allow").html(knee_jacket + " Cm");
            //for front shoulder vest
            var knee_vest = parseFloat(vvalue);
            $("#knee_vest_allow").html(knee_vest + " Cm");
            //for front shoulder pant
            var knee_pant = (parseFloat(pvalue));
            $("#knee_pant_allow").html(knee_pant + " Cm");

            //for back_jacket_length shirt
            if (chest - stomach < 7) {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("back_jacket_length", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            backjacketlengthshirt = svalue;
            jvalue = value.split(";")[1];
            backjacketlengthjacket = jvalue;
            vvalue = value.split(";")[2];
            backjacketlengthvest = vvalue;
            pvalue = value.split(";")[3];
            backjacketlengthpant = pvalue;
            //for back_jacket_length shirt
            var jacket_shirt = parseFloat(svalue);
            $("#jacket_shirt_allow").html(jacket_shirt + " Cm");
            //for back_jacket_length jacket
            var jacket_jacket = (parseFloat(jvalue));
            $("#jacket_jacket_allow").html(jacket_jacket + " Cm");
            //for back_jacket_length vest
            var jacket_vest = parseFloat(vvalue);
            $("#jacket_vest_allow").html(jacket_vest + " Cm");
            //for back_jacket_length pant
            var jacket_pant = (parseFloat(pvalue));
            $("#jacket_pant_allow").html(jacket_pant + " Cm");

            //for u_rise shirt
            if (chest - stomach < 7) {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("u_rise", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            riseshirt = svalue;
            jvalue = value.split(";")[1];
            risejacket = jvalue;
            vvalue = value.split(";")[2];
            risevest = vvalue;
            pvalue = value.split(";")[3];
            risepant = pvalue;
            //for back_jacket_length shirt
            var rise_shirt = parseFloat(svalue);
            $("#rise_shirt_allow").html(rise_shirt + " Cm");
            //for back_jacket_length jacket
            var rise_jacket = (parseFloat(jvalue));
            $("#rise_jacket_allow").html(rise_jacket + " Cm");
            //for back_jacket_length vest
            var rise_vest = parseFloat(vvalue);
            $("#rise_vest_allow").html(rise_vest + " Cm");
            //for back_jacket_length pant
            var rise_pant = (parseFloat(pvalue));
            $("#rise_pant_allow").html(rise_pant + " Cm");

            ////////for pant_left_shirt
            if (chest - stomach < 7) {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_left_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantleftshirt = svalue;
            jvalue = value.split(";")[1];
            pantleftjacket = jvalue;
            vvalue = value.split(";")[2];
            pantleftvest = vvalue;
            pvalue = value.split(";")[3];
            pantleftpant = pvalue;
            //for back_jacket_length shirt
            var p_left_shirt = parseFloat(svalue);
            $("#p_left_shirt_allow").html(p_left_shirt + " Cm");
            //for back_jacket_length jacket
            var p_left_jacket = (parseFloat(jvalue));
            $("#p_left_jacket_allow").html(p_left_jacket + " Cm");
            //for back_jacket_length vest
            var p_left_vest = parseFloat(vvalue);
            $("#p_left_vest_allow").html(p_left_vest + " Cm");
            //for back_jacket_length pant
            var p_left_pant = (parseFloat(pvalue));
            $("#p_left_pant_allow").html(p_left_pant + " Cm");

            //for p_right shirt
            if (chest - stomach < 7) {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("pants_right_outseam", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            pantrightshirt = svalue;
            jvalue = value.split(";")[1];
            pantrightjacket = jvalue;
            vvalue = value.split(";")[2];
            pantrightvest = vvalue;
            pvalue = value.split(";")[3];
            pantrightpant = pvalue;
            //for back_jacket_length shirt
            var p_right_shirt = parseFloat(svalue);
            $("#p_right_shirt_allow").html(p_right_shirt + " Cm");
            //for back_jacket_length jacket
            var p_right_jacket = (parseFloat(jvalue));
            $("#p_right_jacket_allow").html(p_right_jacket + " Cm");
            //for back_jacket_length vest
            var p_right_vest = parseFloat(vvalue);
            $("#p_right_vest_allow").html(p_right_vest + " Cm");
            //for back_jacket_length pant
            var p_right_pant = (parseFloat(pvalue));
            $("#p_right_pant_allow").html(p_right_pant + " Cm");

            /////for bottom shirt
            if (chest - stomach < 7) {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1lessthan7:" + janswer1 + "@jques2lessthan7:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            } else {
                value = getResponse2("bottom", "ques1:" + answer1 + "@ques2:" + answer2 + "@ques3:" + answer3 + "@ques4:" + answer4 + "@ques5:" + answer5 + "@ques6:" + answer6 + "@jques1:" + janswer1 + "@jques2:" + janswer2 + "@jques3:" + janswer3 + "@vques1:" + vanswer1 + "@pques1:" + panswer1);
            }
            svalue = value.split(";")[0];
            bottomshirt = svalue;
            jvalue = value.split(";")[1];
            bottomjacket = jvalue;
            vvalue = value.split(";")[2];
            bottomvest = vvalue;
            pvalue = value.split(";")[3];
            bottompant = pvalue;
            //for bottom shirt
            var bottom_shirt = parseFloat(svalue);
            $("#bottom_shirt_allow").html(bottom_shirt + " Cm");
            //for bottom jacket
            var bottom_jacket = (parseFloat(jvalue));
            $("#bottom_jacket_allow").html(bottom_jacket + " Cm");
            //for bottom vest
            var bottom_vest = parseFloat(vvalue);
            $("#bottom_vest_allow").html(bottom_vest + " Cm");
            //for bottom pant
            var bottom_pant = (parseFloat(pvalue));
            $("#bottom_pant_allow").html(bottom_pant + " Cm");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
        measurementData(meas_id,sizeData);
    }
    function getResponse2(body_param, ques_answer) {
        var url = 'api/registerUser.php';
        var jvalue = 0;
        var svalue = 0;
        var vvalue = 0;
        var pvalue = 0;
        var data = {"type": "queryProcess2", "body_param": body_param, "ques_answer": ques_answer};
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            async: false,
            success: function (data) {
                jvalue = data.jvalue;
                svalue = data.svalue;
                vvalue = data.vvalue;
                pvalue = data.pvalue;
            }
        });
        return svalue + ";" + jvalue + ";" + vvalue + ";" + pvalue;
    }
    function getPantvaluepleat(pant_answer) {
        var url = 'api/registerUser.php';
        var result = 0;
        var data = {"type": "pantpleat", "pant_answer": pant_answer};
        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            async: false,
            success: function (data) {
                result = data.pleatData;
                var oldpantseat = parseFloat($("#final_pant_seat").html().split(" ")[0]);
                var oldpantrise = parseFloat($("#final_pant_rise").html().split(" ")[0]);
                if ($("#params").val() == "Inches") {
                    var val = parseFloat(oldpantseat + parseFloat((result.no_pleat) / 2.54)).toFixed(1);
                    $("#final_pant_seat").html(val + " " + $("#params").val());
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#final_pant_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat + parseFloat((result.onepleatlesstha2cm) / 2.54)).toFixed(1);
                    $("#final_pant1_seat").html(val + " " + $("#params").val());
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#final_pant1_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat + parseFloat((result.onpleatbetween2cmand25cm) / 2.54)).toFixed(1);
                    $("#final_pant11_seat").html(val + " " + $("#params").val());
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#final_pant11_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat + parseFloat((result.twopleatlessthan25cm) / 2.54)).toFixed(1);
                    $("#final_pant2_seat").html(val + " " + $("#params").val());
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#final_pant2_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat + parseFloat((result.threepleatlessthan25cm) / 2.54)).toFixed(1);
                    $("#final_pant3_seat").html(val + " " + $("#params").val());
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#final_pant3_seat").css("color", "red");
                    }
                    $("#final_pant_rise").html(parseFloat(oldpantrise + parseFloat((result.urise) / 2.54)).toFixed(1) + " " + $("#params").val());
                    $("#final_pant1_rise").html(parseFloat(oldpantrise + parseFloat((result.urise) / 2.54)).toFixed(1) + " " + $("#params").val());
                    $("#final_pant11_rise").html(parseFloat(oldpantrise + parseFloat((result.urise) / 2.54)).toFixed(1) + " " + $("#params").val());
                    $("#final_pant2_rise").html(parseFloat(oldpantrise + parseFloat((result.urise) / 2.54)).toFixed(1) + " " + $("#params").val());
                    $("#final_pant3_rise").html(parseFloat(oldpantrise + parseFloat((result.urise) / 2.54)).toFixed(1) + " " + $("#params").val());
                } else {
                    var val = parseFloat(oldpantseat) + parseFloat(result.no_pleat);
                    $("#final_pant_seat").html(val + " " + $("#params").val());
                    if (val < seat_min || val > seat_max) {
                        $("#final_pant_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat) + parseFloat(result.onepleatlesstha2cm);
                    $("#final_pant1_seat").html(val + " " + $("#params").val());
                    if (val < seat_min || val > seat_max) {
                        $("#final_pant1_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat) + parseFloat(result.onpleatbetween2cmand25cm);
                    $("#final_pant11_seat").html(val + " " + $("#params").val());
                    if (val < seat_min || val > seat_max) {
                        $("#final_pant11_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat) + parseFloat(result.twopleatlessthan25cm);
                    $("#final_pant2_seat").html(val + " " + $("#params").val());
                    if (val < seat_min || val > seat_max) {
                        $("#final_pant2_seat").css("color", "red");
                    }
                    var val = parseFloat(oldpantseat) + parseFloat(result.threepleatlessthan25cm);
                    $("#final_pant3_seat").html(val + " " + $("#params").val());
                    if (val < seat_min || val > seat_max) {
                        $("#final_pant3_seat").css("color", "red");
                    }
                    $("#final_pant_rise").html(parseFloat(oldpantrise) + parseFloat(result.urise) + " " + $("#params").val());
                    $("#final_pant1_rise").html(parseFloat(oldpantrise) + parseFloat(result.urise) + " " + $("#params").val());
                    $("#final_pant11_rise").html(parseFloat(oldpantrise) + parseFloat(result.urise) + " " + $("#params").val());
                    $("#final_pant2_rise").html(parseFloat(oldpantrise) + parseFloat(result.urise) + " " + $("#params").val());
                    $("#final_pant3_rise").html(parseFloat(oldpantrise) + parseFloat(result.urise) + " " + $("#params").val());
                }
            }
        });
    }
    function getData1(meas_id,user_id) {
        $("#meas_id").val(meas_id);
        var params = $("#params").val();
        $("#modal").modal("hide");
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        var url = "api/registerUser.php";
        $.post(url, {"type": "getStyleGenieData", "user_id": user_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                $(".loaderdiv").removeClass("loaderhidden");
                $(".loaderdiv").addClass("block");
                var sizeData = data.userData;
                if (sizeData.unit_type == "Inches") {
                    $("#params").prop('selectedIndex', 0);
                    $("#collar").html((parseFloat(sizeData.collar)).toFixed(1) + " Inches");
                    $("#chest_front").html((parseFloat(sizeData.chest)).toFixed(1) + " Inches");
                    $("#stomach").html((parseFloat(sizeData.stomach)).toFixed(1) + " Inches");
                    $("#seat_front").html((parseFloat(sizeData.seat)).toFixed(1) + " Inches");
                    $("#bicep_front").html((parseFloat(sizeData.bicep)).toFixed(1) + " Inches");
                    $("#shoulder_back").html((parseFloat(sizeData.back_sholuder)).toFixed(1) + " Inches");
                    $("#shoulder_front").html((parseFloat(sizeData.front_shoulder)).toFixed(1) + " Inches");
                    $("#shoulder_sleeve_right").html((parseFloat(sizeData.sleeve_length_left)).toFixed(1) + " Inches");
                    $("#shoulder_sleeve_left").html((parseFloat(sizeData.sleeve_length_right)).toFixed(1) + " Inches");
                    $("#thigh").html((parseFloat(sizeData.thigh)).toFixed(1) + " Inches");
                    $("#napetowaist_back").html((parseFloat(sizeData.nape_to_waist)).toFixed(1) + " Inches");
                    $("#front_waist_length").html((parseFloat(sizeData.front_waist_length)).toFixed(1) + " Inches");
                    $("#wrist").html((parseFloat(sizeData.wrist)).toFixed(1) + " Inches");
                    $("#waist").html((parseFloat(sizeData.waist)).toFixed(1) + " Inches");
                    $("#calf").html((parseFloat(sizeData.calf)).toFixed(1) + " Inches");
                    $("#back_waist_height").html((parseFloat(sizeData.back_waist_height)).toFixed(1) + " Inches");
                    $("#front_waist_height").html((parseFloat(sizeData.front_waist_height)).toFixed(1) + " Inches");
                    $("#knee_right").html((parseFloat(sizeData.knee)).toFixed(1) + " Inches");
                    $("#back_jacket_length").html((parseFloat(sizeData.back_jacket_length)).toFixed(1) + " Inches");
                    $("#u_rise").html((parseFloat(sizeData.u_rise)).toFixed(1) + " Inches");
                    $("#outseam_l_pants").html((parseFloat(sizeData.pant_left_outseam)).toFixed(1) + " Inches");
                    $("#outseam_r_pants").html((parseFloat(sizeData.pant_right_outseam)).toFixed(1) + " Inches");
                    $("#bottom").html((parseFloat(sizeData.bottom)).toFixed(1) + " Inches");
                }
                else {
                    $("#params").prop('selectedIndex', 1);
                    $("#collar").html(parseFloat(sizeData.collar).toFixed(1) + " Cm");
                    $("#chest_front").html(parseFloat(sizeData.chest).toFixed(1) + " Cm");
                    $("#stomach").html(parseFloat(sizeData.stomach).toFixed(1) + " Cm");
                    $("#seat_front").html(parseFloat(sizeData.seat).toFixed(1) + " Cm");
                    $("#bicep_front").html(parseFloat(sizeData.bicep).toFixed(1) + " Cm");
                    $("#shoulder_back").html(parseFloat(sizeData.back_sholuder).toFixed(1) + " Cm");
                    $("#shoulder_front").html(parseFloat(sizeData.front_shoulder).toFixed(1) + " Cm");
                    $("#shoulder_sleeve_right").html(parseFloat(sizeData.sleeve_length_left).toFixed(1) + " Cm");
                    $("#shoulder_sleeve_left").html(parseFloat(sizeData.sleeve_length_right).toFixed(1) + " Cm");
                    $("#thigh").html(parseFloat(sizeData.thigh).toFixed(1) + " Cm");
                    $("#napetowaist_back").html(parseFloat(sizeData.nape_to_waist).toFixed(1) + " Cm");
                    $("#front_waist_length").html(parseFloat(sizeData.front_waist_length).toFixed(1) + " Cm");
                    $("#wrist").html(parseFloat(sizeData.wrist).toFixed(1) + " Cm");
                    $("#waist").html(parseFloat(sizeData.waist).toFixed(1) + " Cm");
                    $("#calf").html(parseFloat(sizeData.calf).toFixed(1) + " Cm");
                    $("#back_waist_height").html(parseFloat(sizeData.back_waist_height).toFixed(1) + " Cm");
                    $("#front_waist_height").html(parseFloat(sizeData.front_waist_height).toFixed(1) + " Cm");
                    $("#knee_right").html(parseFloat(sizeData.knee).toFixed(1) + " Cm");
                    $("#back_jacket_length").html(parseFloat(sizeData.back_jacket_length).toFixed(1) + " Cm");
                    $("#u_rise").html(parseFloat(sizeData.u_rise).toFixed(1) + " Cm");
                    $("#outseam_l_pants").html(parseFloat(sizeData.pant_left_outseam).toFixed(1) + " Cm");
                    $("#outseam_r_pants").html(parseFloat(sizeData.pant_right_outseam).toFixed(1) + " Cm");
                    $("#bottom").html(parseFloat(sizeData.bottom).toFixed(1) + " Cm");
                }
                var shoulder_slope = sizeData.shoulder_slope;
                var shoulder_slopeArray = shoulder_slope.split(",");
                var url = "api/registerUser.php";
                if (shoulder_slopeArray[0] == shoulder_slopeArray[1]) {
                    $.post(url, {
                        "type": "getBodyStyle", "left_side": shoulder_slopeArray[0],
                        "right_side": shoulder_slopeArray[1], "combination": "both"
                    }, function (data) {
                        var Status = data.Status;
                        if (Status == "Success") {
                            if ($("#params").val() == "Inches") {
                                frontshoulderbody = data.front
                                backshoulderbody = data.back
                                $("#back_shoulder_body").html((data.back / 2.54).toFixed(1) + " Inches");
                                $("#front_shoulder_body").html((data.front / 2.54).toFixed(1) + " Inches");
                            } else {
                                $("#back_shoulder_body").html(data.back + " Cm");
                                $("#front_shoulder_body").html(data.front + " Cm");
                                frontshoulderbody = data.front
                                backshoulderbody = data.back
                            }
                            writeminmax(sizeData,meas_id);
                        }
                    });
                }
                else {
                    $.post(url, {
                        "type": "getBodyStyle", "left_side": shoulder_slopeArray[0],
                        "right_side": shoulder_slopeArray[1], "combination": "single"
                    }, function (data) {
                        var Status = data.Status;
                        if (Status == "Success") {
                            if ($("#params").val() == "Inches") {
                                frontshoulderbody = data.front;
                                backshoulderbody = data.back;
                                $("#back_shoulder_body").html((data.back / 2.54).toFixed(1) + " Inches");
                                $("#front_shoulder_body").html((data.front / 2.54).toFixed(1) + " Inches");
                            } else {
                                $("#back_shoulder_body").html(data.back + " Cm");
                                $("#front_shoulder_body").html(data.front + " Cm");
                                frontshoulderbody = data.front
                                backshoulderbody = data.back;
                            }
                            writeminmax(sizeData,meas_id);
                        }
                        else
                        {
                            showMessage(data.Message,"red");
                            $(".loaderdiv").removeClass("block");
                            $(".loaderdiv").addClass("loaderhidden");
                        }
                    });
                }
            }
            else {
                showMessage(data.Message, 'red');
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        }).fail(function () {
            showMessage("Server Error !!! Please Try After Some Time....", 'red');
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        });
    }
    function confirmsave() {
        var meas_id = $("#meas_id").val();
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        var shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        var shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var vest_chest = $("#final_vest_chest").text().split(" ")[0];
        var shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        var shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var jacket_seat = $("#final_jacket_seat").text().split(" ")[0];
        var pant_seat = $("#final_pant_seat").text().split(" ")[0];
        var pant1_seat = $("#final_pant1_seat").text().split(" ")[0];
        var pant11_seat = $("#final_pant11_seat").text().split(" ")[0];
        var pant2_seat = $("#final_pant2_seat").text().split(" ")[0];
        var pant3_seat = $("#final_pant3_seat").text().split(" ")[0];
        var shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        var shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        var shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        var shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        var shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        var pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        var shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        var jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        var pant_waist = $("#final_pant_waist").text().split(" ")[0];
        var pant_calf = $("#final_pant_calf").text().split(" ")[0];
        var pant_back_waist_height = $("#final_pant_b_waist").text().split(" ")[0];
        var pant_front_waist_height = $("#final_pant_f_waist").text().split(" ")[0];
        var pant_knee = $("#final_pant_knee").text().split(" ")[0];
        var shirt_back_jacket_length = $("#final_shirt_jacket").text().split(" ")[0];
        var jacket_back_jacket_length = $("#final_jacket_jacket").text().split(" ")[0];
        var vest_back_jacket_length = $("#final_vest_jacket").text().split(" ")[0];
        var pant_u_rise = $("#final_pant_rise").text().split(" ")[0];
        var pant_pant_left_outseam = $("#final_pant_p_left").text().split(" ")[0];
        var pant_pant_right_outseam = $("#final_pant_p_right").text().split(" ")[0];
        var pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        var url = 'api/registerUser.php';
        $.post(url,
            {
                'type': 'updateFinalMeasurement',
                'shirt_collar': shirt_collar,
                'shirt_chest': shirt_chest,
                'jacket_chest': jacket_chest,
                'vest_chest': vest_chest,
                'shirt_stomach': shirt_stomach,
                'jacket_stomach': jacket_stomach,
                'vest_stomach': vest_stomach,
                'shirt_seat': shirt_seat,
                'jacket_seat': jacket_seat,
                'pant_seat': pant_seat,
                'pant1_seat': pant1_seat,
                'pant11_seat': pant11_seat,
                'pant2_seat': pant2_seat,
                'pant3_seat': pant3_seat,
                'shirt_bicep': shirt_bicep,
                'jacket_bicep': jacket_bicep,
                'shirt_back_shoulder': shirt_back_shoulder,
                'jacket_back_shoulder': jacket_back_shoulder,
                'shirt_front_shoulder': shirt_front_shoulder,
                'jacket_front_shoulder': jacket_front_shoulder,
                'shirt_sleeve_left': shirt_sleeve_left,
                'jacket_sleeve_left': jacket_sleeve_left,
                'shirt_sleeve_right': shirt_sleeve_right,
                'jacket_sleeve_right': jacket_sleeve_right,
                'pant_thigh': pant_thigh,
                'shirt_wrist': shirt_wrist,
                'jacket_wrist': jacket_wrist,
                'pant_calf': pant_calf,
                'pant_back_waist_height': pant_back_waist_height,
                'pant_front_waist_height': pant_front_waist_height,
                'pant_knee': pant_knee,
                'shirt_back_jacket_length': shirt_back_jacket_length,
                'jacket_back_jacket_length': jacket_back_jacket_length,
                'vest_back_jacket_length': vest_back_jacket_length,
                'pant_u_rise': pant_u_rise,
                'pant_pant_left_outseam': pant_pant_left_outseam,
                'pant_pant_right_outseam': pant_pant_right_outseam,
                'pant_bottom': pant_bottom,
                'pant_waist': pant_waist,
                'meas_id': meas_id,
                'unit_type': unit_type
            },
            function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    $("#modal").modal("hide");
                    location.reload();
                } else {
                    $(".err").html(data.Message);
                    return false;
                }
            }).fail(function () {
            $(".err").html("Server Error !!! Please Try After Some Time...");
            return false;
        });
    }
    function convertData() {
        showminmaxrange();
        var tab = $(".matchingTabs.tabs.active").attr("id");
        if(tab == "jacketallow"){
            switchMatching("jacket");
        }
        else if(tab == "pantallow"){
            switchMatching("pant");
        }
        else if(tab == "vestallow"){
            switchMatching("vest");
        }
        else if(tab == "shirtallow"){
            switchMatching("shirt");
        }
        /*for specification insert when tab is change end   here */
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        //old data of collar
        var collar = $("#collar").text().split(" ")[0];
        var collar_shirt_allow = $("#collar_shirt_allow").text().split(" ")[0];
        var collar_jacket_allow = $("#collar_jacket_allow").text().split(" ")[0];
        var collar_vest_allow = $("#collar_vest_allow").text().split(" ")[0];
        var collar_pant_allow = $("#collar_pant_allow").text().split(" ")[0];
        var final_shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        //old data of chest
        var chest_front = $("#chest_front").text().split(" ")[0];
        var chest_shirt_allow = $("#chest_shirt_allow").text().split(" ")[0];
        var chest_jacket_allow = $("#chest_jacket_allow").text().split(" ")[0];
        var chest_vest_allow = $("#chest_vest_allow").text().split(" ")[0];
        var chest_pant_allow = $("#chest_pant_allow").text().split(" ")[0];
        var final_shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var final_jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var final_vest_chest = $("#final_vest_chest").text().split(" ")[0];
        //old data of stomach
        var stomach = $("#stomach").text().split(" ")[0];
        var stomach_shirt_allow = $("#stomach_shirt_allow").text().split(" ")[0];
        var stomach_jacket_allow = $("#stomach_jacket_allow").text().split(" ")[0];
        var stomach_vest_allow = $("#stomach_vest_allow").text().split(" ")[0];
        var stomach_pant_allow = $("#stomach_pant_allow").text().split(" ")[0];
        var final_shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var final_jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var final_vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        //old data of seat
        var seat_front = $("#seat_front").text().split(" ")[0];
        var seat_shirt_allow = $("#seat_shirt_allow").text().split(" ")[0];
        var seat_jacket_allow = $("#seat_jacket_allow").text().split(" ")[0];
        var seat_vest_allow = $("#seat_vest_allow").text().split(" ")[0];
        var seat_pant_allow = $("#seat_pant_allow").text().split(" ")[0];
        var final_shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var final_jacket_seat = $("#final_jacket_seat").text().split(" ")[0];
        var final_pant_seat = $("#final_pant_seat").text().split(" ")[0];
        var final_pant1_seat = $("#final_pant1_seat").text().split(" ")[0];
        var final_pant11_seat = $("#final_pant11_seat").text().split(" ")[0];
        var final_pant2_seat = $("#final_pant2_seat").text().split(" ")[0];
        var final_pant3_seat = $("#final_pant3_seat").text().split(" ")[0];
        //old data of bicep
        var bicep_front = $("#bicep_front").text().split(" ")[0];
        var bicep_shirt_allow = $("#bicep_shirt_allow").text().split(" ")[0];
        var bicep_jacket_allow = $("#bicep_jacket_allow").text().split(" ")[0];
        var bicep_vest_allow = $("#bicep_vest_allow").text().split(" ")[0];
        var bicep_pant_allow = $("#bicep_pant_allow").text().split(" ")[0];
        var final_shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var final_jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        //old data of shoulder_back
        var shoulder_back = $("#shoulder_back").text().split(" ")[0];
        var back_shoulder_body = $("#back_shoulder_body").text().split(" ")[0];
        var back_shoulder_shirt_allow = $("#back_shoulder_shirt_allow").text().split(" ")[0];
        var back_shoulder_jacket_allow = $("#back_shoulder_jacket_allow").text().split(" ")[0];
        var back_shoulder_vest_allow = $("#back_shoulder_vest_allow").text().split(" ")[0];
        var back_shoulder_pant_allow = $("#back_shoulder_pant_allow").text().split(" ")[0];
        var final_shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var final_jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        //old data of shoulder_front
        var shoulder_front = $("#shoulder_front").text().split(" ")[0];
        var front_shoulder_body = $("#front_shoulder_body").text().split(" ")[0];
        var front_shoulder_shirt_allow = $("#front_shoulder_shirt_allow").text().split(" ")[0];
        var front_shoulder_jacket_allow = $("#front_shoulder_jacket_allow").text().split(" ")[0];
        var front_shoulder_vest_allow = $("#front_shoulder_vest_allow").text().split(" ")[0];
        var front_shoulder_pant_allow = $("#front_shoulder_pant_allow").text().split(" ")[0];
        var final_shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var final_jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        //old data of shoulder_sleeve_right
        var shoulder_sleeve_right = $("#shoulder_sleeve_right").text().split(" ")[0];
        var sleeve_right_shirt_allow = $("#sleeve_right_shirt_allow").text().split(" ")[0];
        var sleeve_right_jacket_allow = $("#sleeve_right_jacket_allow").text().split(" ")[0];
        var sleeve_right_vest_allow = $("#sleeve_right_vest_allow").text().split(" ")[0];
        var sleeve_right_pant_allow = $("#sleeve_right_pant_allow").text().split(" ")[0];
        var final_shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var final_jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        //old data of shoulder_sleeve_left
        var shoulder_sleeve_left = $("#shoulder_sleeve_left").text().split(" ")[0];
        var sleeve_left_shirt_allow = $("#sleeve_left_shirt_allow").text().split(" ")[0];
        var sleeve_left_jacket_allow = $("#sleeve_left_jacket_allow").text().split(" ")[0];
        var sleeve_left_vest_allow = $("#sleeve_left_vest_allow").text().split(" ")[0];
        var sleeve_left_pant_allow = $("#sleeve_left_pant_allow").text().split(" ")[0];
        var final_shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var final_jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        //old data of thigh
        var thigh = $("#thigh").text().split(" ")[0];
        var thigh_shirt_allow = $("#thigh_shirt_allow").text().split(" ")[0];
        var thigh_jacket_allow = $("#thigh_jacket_allow").text().split(" ")[0];
        var thigh_vest_allow = $("#thigh_vest_allow").text().split(" ")[0];
        var thigh_pant_allow = $("#thigh_pant_allow").text().split(" ")[0];
        var final_pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        //old data of napetowaist_back
        var napetowaist_back = $("#napetowaist_back").text().split(" ")[0];
        var nape_shirt_allow = $("#nape_shirt_allow").text().split(" ")[0];
        var nape_jacket_allow = $("#nape_jacket_allow").text().split(" ")[0];
        var nape_vest_allow = $("#nape_vest_allow").text().split(" ")[0];
        var nape_pant_allow = $("#nape_pant_allow").text().split(" ")[0];
        //old data of front_waist_length
        var front_waist_length = $("#front_waist_length").text().split(" ")[0];
        var front_waist_shirt_allow = $("#front_waist_shirt_allow").text().split(" ")[0];
        var front_waist_jacket_allow = $("#front_waist_jacket_allow").text().split(" ")[0];
        var front_waist_vest_allow = $("#front_waist_vest_allow").text().split(" ")[0];
        var front_waist_pant_allow = $("#front_waist_pant_allow").text().split(" ")[0];
        //old data of wrist
        var wrist = $("#wrist").text().split(" ")[0];
        var wrist_shirt_allow = $("#wrist_shirt_allow").text().split(" ")[0];
        var wrist_jacket_allow = $("#wrist_jacket_allow").text().split(" ")[0];
        var wrist_vest_allow = $("#wrist_vest_allow").text().split(" ")[0];
        var wrist_pant_allow = $("#wrist_pant_allow").text().split(" ")[0];
        var final_shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        //old data of waist
        var waist = $("#waist").text().split(" ")[0];
        var waist_shirt_allow = $("#waist_shirt_allow").text().split(" ")[0];
        var waist_jacket_allow = $("#waist_jacket_allow").text().split(" ")[0];
        var waist_vest_allow = $("#waist_vest_allow").text().split(" ")[0];
        var waist_pant_allow = $("#waist_pant_allow").text().split(" ")[0];
        var final_pant_waist = $("#final_pant_waist").text().split(" ")[0];
        //old data of calf
        var calf = $("#calf").text().split(" ")[0];
        var calf_shirt_allow = $("#calf_shirt_allow").text().split(" ")[0];
        var calf_jacket_allow = $("#calf_jacket_allow").text().split(" ")[0];
        var calf_vest_allow = $("#calf_vest_allow").text().split(" ")[0];
        var calf_pant_allow = $("#calf_pant_allow").text().split(" ")[0];
        var final_pant_calf = $("#final_pant_calf").text().split(" ")[0];
        //old data of back_waist_height
        var back_waist_height = $("#back_waist_height").text().split(" ")[0];
        var b_waist_shirt_allow = $("#b_waist_shirt_allow").text().split(" ")[0];
        var b_waist_jacket_allow = $("#b_waist_jacket_allow").text().split(" ")[0];
        var b_waist_vest_allow = $("#b_waist_vest_allow").text().split(" ")[0];
        var b_waist_pant_allow = $("#b_waist_pant_allow").text().split(" ")[0];
        var final_pant_b_waist = $("#final_pant_b_waist").text().split(" ")[0];
        //old data of front_waist_height
        var front_waist_height = $("#front_waist_height").text().split(" ")[0];
        var f_waist_shirt_allow = $("#f_waist_shirt_allow").text().split(" ")[0];
        var f_waist_jacket_allow = $("#f_waist_jacket_allow").text().split(" ")[0];
        var f_waist_vest_allow = $("#f_waist_vest_allow").text().split(" ")[0];
        var f_waist_pant_allow = $("#f_waist_pant_allow").text().split(" ")[0];
        var final_pant_f_waist = $("#final_pant_f_waist").text().split(" ")[0];
        //old data of knee_right
        var knee_right = $("#knee_right").text().split(" ")[0];
        var knee_shirt_allow = $("#knee_shirt_allow").text().split(" ")[0];
        var knee_jacket_allow = $("#knee_jacket_allow").text().split(" ")[0];
        var knee_vest_allow = $("#knee_vest_allow").text().split(" ")[0];
        var knee_pant_allow = $("#knee_pant_allow").text().split(" ")[0];
        var final_pant_knee = $("#final_pant_knee").text().split(" ")[0];
        //old data of back_jacket_length
        var back_jacket_length = $("#back_jacket_length").text().split(" ")[0];
        var jacket_shirt_allow = $("#jacket_shirt_allow").text().split(" ")[0];
        var jacket_jacket_allow = $("#jacket_jacket_allow").text().split(" ")[0];
        var jacket_vest_allow = $("#jacket_vest_allow").text().split(" ")[0];
        var jacket_pant_allow = $("#jacket_pant_allow").text().split(" ")[0];
        var final_shirt_jacket = $("#final_shirt_jacket").text().split(" ")[0];
        var final_jacket_jacket = $("#final_jacket_jacket").text().split(" ")[0];
        var final_vest_jacket = $("#final_vest_jacket").text().split(" ")[0];
        //old data of u_rise
        var u_rise = $("#u_rise").text().split(" ")[0];
        var rise_shirt_allow = $("#rise_shirt_allow").text().split(" ")[0];
        var rise_jacket_allow = $("#rise_jacket_allow").text().split(" ")[0];
        var rise_vest_allow = $("#rise_vest_allow").text().split(" ")[0];
        var rise_pant_allow = $("#rise_pant_allow").text().split(" ")[0];
        var final_pant_rise = $("#final_pant_rise").text().split(" ")[0];
        var final_pant1_rise = $("#final_pant1_rise").text().split(" ")[0];
        var final_pant11_rise = $("#final_pant11_rise").text().split(" ")[0];
        var final_pant2_rise = $("#final_pant2_rise").text().split(" ")[0];
        var final_pant3_rise = $("#final_pant3_rise").text().split(" ")[0];
        //old data of outseam_l_pants
        var outseam_l_pants = $("#outseam_l_pants").text().split(" ")[0];
        var p_left_shirt_allow = $("#p_left_shirt_allow").text().split(" ")[0];
        var p_left_jacket_allow = $("#p_left_jacket_allow").text().split(" ")[0];
        var p_left_vest_allow = $("#p_left_vest_allow").text().split(" ")[0];
        var p_left_pant_allow = $("#p_left_pant_allow").text().split(" ")[0];
        var final_pant_p_left = $("#final_pant_p_left").text().split(" ")[0];
        //old data of outseam_r_pants
        var outseam_r_pants = $("#outseam_r_pants").text().split(" ")[0];
        var p_right_shirt_allow = $("#p_right_shirt_allow").text().split(" ")[0];
        var p_right_jacket_allow = $("#p_right_jacket_allow").text().split(" ")[0];
        var p_right_vest_allow = $("#p_right_vest_allow").text().split(" ")[0];
        var p_right_pant_allow = $("#p_right_pant_allow").text().split(" ")[0];
        var final_pant_p_right = $("#final_pant_p_right").text().split(" ")[0];
        //old data of bottom
        var bottom = $("#bottom").text().split(" ")[0];
        //var bottom_body = $("#bottom_body").text().split(" ")[0];
        var bottom_shirt_allow = $("#bottom_shirt_allow").text().split(" ")[0];
        var bottom_jacket_allow = $("#bottom_jacket_allow").text().split(" ")[0];
        var bottom_vest_allow = $("#bottom_vest_allow").text().split(" ")[0];
        var bottom_pant_allow = $("#bottom_pant_allow").text().split(" ")[0];
        var final_pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        var final_jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        if (unit_type == "Inches") {
            //show in centimeter means conversion required from inches to centimeter
            //for collar
            $("#collar").text(parseFloat(collar * 2.54).toFixed(1) + " Cm");
            $("#collar_shirt_allow").text(collarshirt + " Cm");
            $("#collar_jacket_allow").text(collarjacket + " Cm");
            $("#collar_vest_allow").text(collarvest + " Cm");
            $("#collar_pant_allow").text(collarpant + " Cm");
            $("#final_shirt_collar").text(parseFloat(final_shirt_collar * 2.54).toFixed(1) + " Cm");
            //for chest_front
            $("#chest_front").text(parseFloat(chest_front * 2.54).toFixed(1) + " Cm");
            $("#chest_shirt_allow").text(chestshirt + " Cm");
            $("#chest_jacket_allow").text(chestjacket + " Cm");
            $("#chest_vest_allow").text(chestvest + " Cm");
            $("#chest_pant_allow").text(chestpant + " Cm");
            $("#final_shirt_chest").text(parseFloat(final_shirt_chest * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_chest").text(parseFloat(final_jacket_chest * 2.54).toFixed(1) + " Cm");
            $("#final_vest_chest").text(parseFloat(final_vest_chest * 2.54).toFixed(1) + " Cm");
            //for stomach
            $("#stomach").text(parseFloat(stomach * 2.54).toFixed(1) + " Cm");
            $("#stomach_shirt_allow").text(stomachshirt + " Cm");
            $("#stomach_jacket_allow").text(stomachjacket + " Cm");
            $("#stomach_vest_allow").text(stomachvest + " Cm");
            $("#stomach_pant_allow").text(stomachpant + " Cm");
            $("#final_shirt_stomach").text(parseFloat(final_shirt_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_stomach").text(parseFloat(final_jacket_stomach * 2.54).toFixed(1) + " Cm");
            $("#final_vest_stomach").text(parseFloat(final_vest_stomach * 2.54).toFixed(1) + " Cm");
            //for seat_front
            $("#seat_front").text(parseFloat(seat_front * 2.54).toFixed(1) + " Cm");
            $("#seat_shirt_allow").text(seatshirt + " Cm");
            $("#seat_jacket_allow").text(seatjacket + " Cm");
            $("#seat_vest_allow").text(seatvest + " Cm");
            $("#seat_pant_allow").text(seatpant + " Cm");
            $("#final_shirt_seat").text(parseFloat(final_shirt_seat * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_seat").text(parseFloat(final_jacket_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant_seat").text(parseFloat(final_pant_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant1_seat").text(parseFloat(final_pant1_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant11_seat").text(parseFloat(final_pant11_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant2_seat").text(parseFloat(final_pant2_seat * 2.54).toFixed(1) + " Cm");
            $("#final_pant3_seat").text(parseFloat(final_pant3_seat * 2.54).toFixed(1) + " Cm");
            //for bicep_front
            $("#bicep_front").text(parseFloat(bicep_front * 2.54).toFixed(1) + " Cm");
            $("#bicep_shirt_allow").text(bicepshirt + " Cm");
            $("#bicep_jacket_allow").text(bicepjacket + " Cm");
            $("#bicep_vest_allow").text(bicepvest + " Cm");
            $("#bicep_pant_allow").text(biceppant + " Cm");
            $("#final_shirt_bicep").text(parseFloat(final_shirt_bicep * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_bicep").text(parseFloat(final_jacket_bicep * 2.54).toFixed(1) + " Cm");
            //for shoulder_back
            $("#shoulder_back").text(parseFloat(shoulder_back * 2.54).toFixed(1) + " Cm");
            $("#back_shoulder_body").text(backshoulderbody + " Cm");
            $("#back_shoulder_shirt_allow").text(backshouldershirt + " Cm");
            $("#back_shoulder_jacket_allow").text(backshoulderjacket + " Cm");
            $("#back_shoulder_vest_allow").text(backshouldervest + " Cm");
            $("#back_shoulder_pant_allow").text(backshoulderpant + " Cm");
            $("#final_shirt_back_shoulder").text(parseFloat(final_shirt_back_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_back_shoulder").text(parseFloat(final_jacket_back_shoulder * 2.54).toFixed(1) + " Cm");
            //for shoulder_front
            $("#shoulder_front").text(parseFloat(shoulder_front * 2.54).toFixed(1) + " Cm");
            $("#front_shoulder_body").text(frontshoulderbody + " Cm");
            $("#front_shoulder_shirt_allow").text(frontshouldershirt + " Cm");
            $("#front_shoulder_jacket_allow").text(frontshoulderjacket + " Cm");
            $("#front_shoulder_vest_allow").text(frontshouldervest + " Cm");
            $("#front_shoulder_pant_allow").text(frontshoulderpant + " Cm");
            $("#final_shirt_front_shoulder").text(parseFloat(final_shirt_front_shoulder * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_front_shoulder").text(parseFloat(final_jacket_front_shoulder * 2.54).toFixed(1) + " Cm");
            //for shoulder_sleeve_right
            $("#shoulder_sleeve_right").text(parseFloat(shoulder_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#sleeve_right_shirt_allow").text(sleeverightshirt + " Cm");
            $("#sleeve_right_jacket_allow").text(sleeverightjacket + " Cm");
            $("#sleeve_right_vest_allow").text(sleeverightvest + " Cm");
            $("#sleeve_right_pant_allow").text(sleeverightpant + " Cm");
            $("#final_shirt_sleeve_right").text(parseFloat(final_shirt_sleeve_right * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_sleeve_right").text(parseFloat(final_jacket_sleeve_right * 2.54).toFixed(1) + " Cm");
            //for shoulder_sleeve_left
            $("#shoulder_sleeve_left").text(parseFloat(shoulder_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#sleeve_left_shirt_allow").text(sleeveleftshirt + " Cm");
            $("#sleeve_left_jacket_allow").text(sleeveleftjacket + " Cm");
            $("#sleeve_left_vest_allow").text(sleeveleftvest + " Cm");
            $("#sleeve_left_pant_allow").text(sleeveleftpant + " Cm");
            $("#final_shirt_sleeve_left").text(parseFloat(final_shirt_sleeve_left * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_sleeve_left").text(parseFloat(final_jacket_sleeve_left * 2.54).toFixed(1) + " Cm");
            //for thigh
            $("#thigh").text(parseFloat(thigh * 2.54).toFixed(1) + " Cm");
            $("#thigh_shirt_allow").text(thighshirt + " Cm");
            $("#thigh_jacket_allow").text(thighjacket + " Cm");
            $("#thigh_vest_allow").text(thighvest + " Cm");
            $("#thigh_pant_allow").text(thighpant + " Cm");
            $("#final_pant_thigh").text(parseFloat(final_pant_thigh * 2.54).toFixed(1) + " Cm");
            //for napetowaist_back
            $("#napetowaist_back").text(parseFloat(napetowaist_back * 2.54).toFixed(1) + " Cm");
            $("#nape_shirt_allow").text(napetowaistshirt + " Cm");
            $("#nape_jacket_allow").text(napetowaistjacket + " Cm");
            $("#nape_vest_allow").text(napetowaistvest + " Cm");
            $("#nape_pant_allow").text(napetowaistpant + " Cm");
            //for front_waist_length
            $("#front_waist_length").text(parseFloat(front_waist_length * 2.54).toFixed(1) + " Cm");
            $("#front_waist_shirt_allow").text(frontwaistlengthshirt + " Cm");
            $("#front_waist_jacket_allow").text(frontwaistlengthjacket + " Cm");
            $("#front_waist_vest_allow").text(frontwaistlengthvest + " Cm");
            $("#front_waist_pant_allow").text(frontwaistlengthpant + " Cm");
            //for wrist
            $("#wrist").text(parseFloat(wrist * 2.54).toFixed(1) + " Cm");
            $("#wrist_shirt_allow").text(wristshirt + " Cm");
            $("#wrist_jacket_allow").text(wristjacket + " Cm");
            $("#wrist_vest_allow").text(wristvest + " Cm");
            $("#wrist_pant_allow").text(wristpant + " Cm");
            $("#final_shirt_wrist").text(parseFloat(final_shirt_wrist * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_wrist").text(parseFloat(final_jacket_wrist * 2.54).toFixed(1) + " Cm");
            //for waist
            $("#waist").text(parseFloat(waist * 2.54).toFixed(1) + " Cm");
            $("#waist_shirt_allow").text(waistshirt + " Cm");
            $("#waist_jacket_allow").text(waistjacket + " Cm");
            $("#waist_vest_allow").text(waistvest + " Cm");
            $("#waist_pant_allow").text(waistpant + " Cm");
            $("#final_pant_waist").text(parseFloat(final_pant_waist * 2.54).toFixed(1) + " Cm");
            //for calf
            $("#calf").text(parseFloat(calf * 2.54).toFixed(1) + " Cm");
            $("#calf_shirt_allow").text(calfshirt + " Cm");
            $("#calf_jacket_allow").text(calfjacket + " Cm");
            $("#calf_vest_allow").text(calfvest + " Cm");
            $("#calf_pant_allow").text(calfpant + " Cm");
            $("#final_pant_calf").text(parseFloat(final_pant_calf * 2.54).toFixed(1) + " Cm");
            //for back_waist_height
            $("#back_waist_height").text(parseFloat(back_waist_height * 2.54).toFixed(1) + " Cm");
            $("#b_waist_shirt_allow").text(backwaistheightshirt + " Cm");
            $("#b_waist_jacket_allow").text(backwaistheightjacket + " Cm");
            $("#b_waist_vest_allow").text(backwaistheightvest + " Cm");
            $("#b_waist_pant_allow").text(backwaistheightpant + " Cm");
            $("#final_pant_b_waist").text(parseFloat(final_pant_b_waist * 2.54).toFixed(1) + " Cm");
            //for front_waist_height
            $("#front_waist_height").text(parseFloat(front_waist_height * 2.54).toFixed(1) + " Cm");
            $("#f_waist_shirt_allow").text(frontwaistheightshirt + " Cm");
            $("#f_waist_jacket_allow").text(frontwaistheightjacket + " Cm");
            $("#f_waist_vest_allow").text(frontwaistheightvest + " Cm");
            $("#f_waist_pant_allow").text(frontwaistheightpant + " Cm");
            $("#final_pant_f_waist").text(parseFloat(final_pant_f_waist * 2.54).toFixed(1) + " Cm");
            //for knee_right
            $("#knee_right").text(parseFloat(knee_right * 2.54).toFixed(1) + " Cm");
            $("#knee_shirt_allow").text(kneeshirt + " Cm");
            $("#knee_jacket_allow").text(kneejacket + " Cm");
            $("#knee_vest_allow").text(kneevest + " Cm");
            $("#knee_pant_allow").text(kneepant + " Cm");
            $("#final_pant_knee").text(parseFloat(final_pant_knee * 2.54).toFixed(1) + " Cm");
            //for back_jacket_length
            $("#back_jacket_length").text(parseFloat(back_jacket_length * 2.54).toFixed(1) + " Cm");
            $("#jacket_shirt_allow").text(backjacketlengthshirt + " Cm");
            $("#jacket_jacket_allow").text(backjacketlengthjacket + " Cm");
            $("#jacket_vest_allow").text(backjacketlengthvest + " Cm");
            $("#jacket_pant_allow").text(backjacketlengthpant + " Cm");
            $("#final_shirt_jacket").text(parseFloat(final_shirt_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_jacket_jacket").text(parseFloat(final_jacket_jacket * 2.54).toFixed(1) + " Cm");
            $("#final_vest_jacket").text(parseFloat(final_vest_jacket * 2.54).toFixed(1) + " Cm");
            //for u_rise
            $("#u_rise").text(parseFloat(u_rise * 2.54).toFixed(1) + " Cm");
            $("#rise_shirt_allow").text(riseshirt + " Cm");
            $("#rise_jacket_allow").text(risejacket + " Cm");
            $("#rise_vest_allow").text(risevest + " Cm");
            $("#rise_pant_allow").text(risepant + " Cm");
            $("#final_pant_rise").text(parseFloat(final_pant_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant1_rise").text(parseFloat(final_pant1_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant11_rise").text(parseFloat(final_pant11_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant2_rise").text(parseFloat(final_pant2_rise * 2.54).toFixed(1) + " Cm");
            $("#final_pant3_rise").text(parseFloat(final_pant3_rise * 2.54).toFixed(1) + " Cm");
            //for outseam_l_pants
            $("#outseam_l_pants").text(parseFloat(outseam_l_pants * 2.54).toFixed(1) + " Cm");
            $("#p_left_shirt_allow").text(pantleftshirt + " Cm");
            $("#p_left_jacket_allow").text(pantleftjacket + " Cm");
            $("#p_left_vest_allow").text(pantleftvest + " Cm");
            $("#p_left_pant_allow").text(pantleftpant + " Cm");
            $("#final_pant_p_left").text(parseFloat(final_pant_p_left * 2.54).toFixed(1) + " Cm");
            //for outseam_r_pants
            $("#outseam_r_pants").text(parseFloat(outseam_r_pants * 2.54).toFixed(1) + " Cm");
            $("#p_right_shirt_allow").text(pantrightshirt + " Cm");
            $("#p_right_jacket_allow").text(pantrightjacket + " Cm");
            $("#p_right_vest_allow").text(pantrightvest + " Cm");
            $("#p_right_pant_allow").text(pantrightpant + " Cm");
            $("#final_pant_p_right").text(parseFloat(final_pant_p_right * 2.54).toFixed(1) + " Cm");
            //for bottom
            $("#bottom").text(parseFloat(bottom * 2.54).toFixed(1) + " Cm");
            $("#bottom_shirt_allow").text(bottomshirt + " Cm");
            $("#bottom_jacket_allow").text(bottomjacket + " Cm");
            $("#bottom_vest_allow").text(bottomvest + " Cm");
            $("#bottom_pant_allow").text(bottompant + " Cm");
            $("#final_pant_bottom").text(parseFloat(final_pant_bottom * 2.54).toFixed(1) + " Cm");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        } else {
            //show in Inches means conversion required from centimeter to Inches
            //for collar
            $("#collar").text(parseFloat(collar / 2.54).toFixed(1) + " Inches");
            $("#collar_shirt_allow").text(parseFloat(collar_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#collar_jacket_allow").text(parseFloat(collar_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#collar_vest_allow").text(parseFloat(collar_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#collar_pant_allow").text(parseFloat(collar_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_collar").text(parseFloat(final_shirt_collar / 2.54).toFixed(1) + " Inches");
            //for chest_front
            $("#chest_front").text(parseFloat(chest_front / 2.54).toFixed(1) + " Inches");
            $("#chest_shirt_allow").text(parseFloat(chest_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#chest_jacket_allow").text(parseFloat(chest_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#chest_vest_allow").text(parseFloat(chest_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#chest_pant_allow").text(parseFloat(chest_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_chest").text(parseFloat(final_shirt_chest / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_chest").text(parseFloat(final_jacket_chest / 2.54).toFixed(1) + " Inches");
            $("#final_vest_chest").text(parseFloat(final_vest_chest / 2.54).toFixed(1) + " Inches");
            //for stomach
            $("#stomach").text(parseFloat(stomach / 2.54).toFixed(1) + " Inches");
            $("#stomach_shirt_allow").text(parseFloat(stomach_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#stomach_jacket_allow").text(parseFloat(stomach_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#stomach_vest_allow").text(parseFloat(stomach_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#stomach_pant_allow").text(parseFloat(stomach_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_stomach").text(parseFloat(final_shirt_stomach / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_stomach").text(parseFloat(final_jacket_stomach / 2.54).toFixed(1) + " Inches");
            $("#final_vest_stomach").text(parseFloat(final_vest_stomach / 2.54).toFixed(1) + " Inches");
            //for seat_front
            $("#seat_front").text(parseFloat(seat_front / 2.54).toFixed(1) + " Inches");
            $("#seat_shirt_allow").text(parseFloat(seat_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#seat_jacket_allow").text(parseFloat(seat_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#seat_vest_allow").text(parseFloat(seat_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#seat_pant_allow").text(parseFloat(seat_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_seat").text(parseFloat(final_shirt_seat / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_seat").text(parseFloat(final_jacket_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant_seat").text(parseFloat(final_pant_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant1_seat").text(parseFloat(final_pant1_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant11_seat").text(parseFloat(final_pant11_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant2_seat").text(parseFloat(final_pant2_seat / 2.54).toFixed(1) + " Inches");
            $("#final_pant3_seat").text(parseFloat(final_pant3_seat / 2.54).toFixed(1) + " Inches");
            //for bicep_front
            $("#bicep_front").text(parseFloat(bicep_front / 2.54).toFixed(1) + " Inches");
            $("#bicep_shirt_allow").text(parseFloat(bicep_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#bicep_jacket_allow").text(parseFloat(bicep_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#bicep_vest_allow").text(parseFloat(bicep_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#bicep_pant_allow").text(parseFloat(bicep_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_bicep").text(parseFloat(final_shirt_bicep / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_bicep").text(parseFloat(final_jacket_bicep / 2.54).toFixed(1) + " Inches");
            //for shoulder_back
            $("#shoulder_back").text(parseFloat(shoulder_back / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_body").text(parseFloat(back_shoulder_body / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_shirt_allow").text(parseFloat(back_shoulder_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_jacket_allow").text(parseFloat(back_shoulder_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_vest_allow").text(parseFloat(back_shoulder_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#back_shoulder_pant_allow").text(parseFloat(back_shoulder_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_back_shoulder").text(parseFloat(final_shirt_back_shoulder / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_back_shoulder").text(parseFloat(final_jacket_back_shoulder / 2.54).toFixed(1) + " Inches");
            //for shoulder_front
            $("#shoulder_front").text(parseFloat(shoulder_front / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_body").text(parseFloat(front_shoulder_body / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_shirt_allow").text(parseFloat(front_shoulder_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_jacket_allow").text(parseFloat(front_shoulder_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_vest_allow").text(parseFloat(front_shoulder_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#front_shoulder_pant_allow").text(parseFloat(front_shoulder_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_front_shoulder").text(parseFloat(final_shirt_front_shoulder / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_front_shoulder").text(parseFloat(final_jacket_front_shoulder / 2.54).toFixed(1) + " Inches");
            //for shoulder_sleeve_right
            $("#shoulder_sleeve_right").text(parseFloat(shoulder_sleeve_right / 2.54).toFixed(1) + " Inches");
            $("#sleeve_right_shirt_allow").text(parseFloat(sleeve_right_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_right_jacket_allow").text(parseFloat(sleeve_right_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_right_vest_allow").text(parseFloat(sleeve_right_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_right_pant_allow").text(parseFloat(sleeve_right_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_sleeve_right").text(parseFloat(final_shirt_sleeve_right / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_sleeve_right").text(parseFloat(final_jacket_sleeve_right / 2.54).toFixed(1) + " Inches");
            //for shoulder_sleeve_left
            $("#shoulder_sleeve_left").text(parseFloat(shoulder_sleeve_left / 2.54).toFixed(1) + " Inches");
            $("#sleeve_left_shirt_allow").text(parseFloat(sleeve_left_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_left_jacket_allow").text(parseFloat(sleeve_left_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_left_vest_allow").text(parseFloat(sleeve_left_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#sleeve_left_pant_allow").text(parseFloat(sleeve_left_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_sleeve_left").text(parseFloat(final_shirt_sleeve_left / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_sleeve_left").text(parseFloat(final_jacket_sleeve_left / 2.54).toFixed(1) + " Inches");
            //for thigh
            $("#thigh").text(parseFloat(thigh / 2.54).toFixed(1) + " Inches");
            $("#thigh_shirt_allow").text(parseFloat(thigh_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#thigh_jacket_allow").text(parseFloat(thigh_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#thigh_vest_allow").text(parseFloat(thigh_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#thigh_pant_allow").text(parseFloat(thigh_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_thigh").text(parseFloat(final_pant_thigh / 2.54).toFixed(1) + " Inches");
            //for napetowaist_back
            $("#napetowaist_back").text(parseFloat(napetowaist_back / 2.54).toFixed(1) + " Inches");
            $("#nape_shirt_allow").text(parseFloat(nape_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#nape_jacket_allow").text(parseFloat(nape_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#nape_vest_allow").text(parseFloat(nape_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#nape_pant_allow").text(parseFloat(nape_pant_allow / 2.54).toFixed(1) + " Inches");
            //for front_waist_length
            $("#front_waist_length").text(parseFloat(front_waist_length / 2.54).toFixed(1) + " Inches");
            $("#front_waist_shirt_allow").text(parseFloat(front_waist_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#front_waist_jacket_allow").text(parseFloat(front_waist_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#front_waist_vest_allow").text(parseFloat(front_waist_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#front_waist_pant_allow").text(parseFloat(front_waist_pant_allow / 2.54).toFixed(1) + " Inches");
            //for wrist
            $("#wrist").text(parseFloat(wrist / 2.54).toFixed(1) + " Inches");
            $("#wrist_shirt_allow").text(parseFloat(wrist_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#wrist_jacket_allow").text(parseFloat(wrist_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#wrist_vest_allow").text(parseFloat(wrist_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#wrist_pant_allow").text(parseFloat(wrist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_wrist").text(parseFloat(final_shirt_wrist / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_wrist").text(parseFloat(final_jacket_wrist / 2.54).toFixed(1) + " Inches");
            //for wrist
            $("#waist").text(parseFloat(waist / 2.54).toFixed(1) + " Inches");
            $("#waist_shirt_allow").text(parseFloat(waist_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#waist_jacket_allow").text(parseFloat(waist_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#waist_vest_allow").text(parseFloat(waist_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#waist_pant_allow").text(parseFloat(waist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_waist").text(parseFloat(final_pant_waist / 2.54).toFixed(1) + " Inches");
            //for calf
            $("#calf").text(parseFloat(calf / 2.54).toFixed(1) + " Inches");
            $("#calf_shirt_allow").text(parseFloat(calf_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#calf_jacket_allow").text(parseFloat(calf_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#calf_vest_allow").text(parseFloat(calf_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#calf_pant_allow").text(parseFloat(calf_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_calf").text(parseFloat(final_pant_calf / 2.54).toFixed(1) + " Inches");
            //for back_waist_height
            $("#back_waist_height").text(parseFloat(back_waist_height / 2.54).toFixed(1) + " Inches");
            $("#b_waist_shirt_allow").text(parseFloat(b_waist_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#b_waist_jacket_allow").text(parseFloat(b_waist_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#b_waist_vest_allow").text(parseFloat(b_waist_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#b_waist_pant_allow").text(parseFloat(b_waist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_b_waist").text(parseFloat(final_pant_b_waist / 2.54).toFixed(1) + " Inches");
            //for front_waist_height
            $("#front_waist_height").text(parseFloat(front_waist_height / 2.54).toFixed(1) + " Inches");
            $("#f_waist_shirt_allow").text(parseFloat(f_waist_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#f_waist_jacket_allow").text(parseFloat(f_waist_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#f_waist_vest_allow").text(parseFloat(f_waist_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#f_waist_pant_allow").text(parseFloat(f_waist_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_f_waist").text(parseFloat(final_pant_f_waist / 2.54).toFixed(1) + " Inches");
            //for knee_right
            $("#knee_right").text(parseFloat(knee_right / 2.54).toFixed(1) + " Inches");
            $("#knee_shirt_allow").text(parseFloat(knee_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#knee_jacket_allow").text(parseFloat(knee_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#knee_vest_allow").text(parseFloat(knee_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#knee_pant_allow").text(parseFloat(knee_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_knee").text(parseFloat(final_pant_knee / 2.54).toFixed(1) + " Inches");
            //for back_jacket_length
            $("#back_jacket_length").text(parseFloat(back_jacket_length / 2.54).toFixed(1) + " Inches");
            $("#jacket_shirt_allow").text(parseFloat(jacket_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#jacket_jacket_allow").text(parseFloat(jacket_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#jacket_vest_allow").text(parseFloat(jacket_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#jacket_pant_allow").text(parseFloat(jacket_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_shirt_jacket").text(parseFloat(final_shirt_jacket / 2.54).toFixed(1) + " Inches");
            $("#final_jacket_jacket").text(parseFloat(final_jacket_jacket / 2.54).toFixed(1) + " Inches");
            $("#final_vest_jacket").text(parseFloat(final_vest_jacket / 2.54).toFixed(1) + " Inches");
            //for u_rise
            $("#u_rise").text(parseFloat(u_rise / 2.54).toFixed(1) + " Inches");
            $("#rise_shirt_allow").text(parseFloat(rise_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#rise_jacket_allow").text(parseFloat(rise_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#rise_vest_allow").text(parseFloat(rise_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#rise_pant_allow").text(parseFloat(rise_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant1_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant11_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant2_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            $("#final_pant3_rise").text(parseFloat(final_pant_rise / 2.54).toFixed(1) + " Inches");
            //for outseam_l_pants
            $("#outseam_l_pants").text(parseFloat(outseam_l_pants / 2.54).toFixed(1) + " Inches");
            $("#p_left_shirt_allow").text(parseFloat(p_left_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#p_left_jacket_allow").text(parseFloat(p_left_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#p_left_vest_allow").text(parseFloat(p_left_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#p_left_pant_allow").text(parseFloat(p_left_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_p_left").text(parseFloat(final_pant_p_left / 2.54).toFixed(1) + " Inches");
            //for outseam_r_pants
            $("#outseam_r_pants").text(parseFloat(outseam_r_pants / 2.54).toFixed(1) + " Inches");
            $("#p_right_shirt_allow").text(parseFloat(p_right_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#p_right_jacket_allow").text(parseFloat(p_right_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#p_right_vest_allow").text(parseFloat(p_right_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#p_right_pant_allow").text(parseFloat(p_right_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_p_right").text(parseFloat(final_pant_p_right / 2.54).toFixed(1) + " Inches");
            //for bottom
            $("#bottom").text(parseFloat(bottom / 2.54).toFixed(1) + " Inches");
            $("#bottom_shirt_allow").text(parseFloat(bottom_shirt_allow / 2.54).toFixed(1) + " Inches");
            $("#bottom_jacket_allow").text(parseFloat(bottom_jacket_allow / 2.54).toFixed(1) + " Inches");
            $("#bottom_vest_allow").text(parseFloat(bottom_vest_allow / 2.54).toFixed(1) + " Inches");
            $("#bottom_pant_allow").text(parseFloat(bottom_pant_allow / 2.54).toFixed(1) + " Inches");
            $("#final_pant_bottom").text(parseFloat(final_pant_bottom / 2.54).toFixed(1) + " Inches");
            $(".loaderdiv").removeClass("block");
            $(".loaderdiv").addClass("loaderhidden");
        }
    }
    function showMessage(message, color) {
        $(".modal-header").css({"display": "none"});
        $(".modal-body").css({"display": "block"});
        $(".modal-body").html("<span style='color:" + color + "'>" + message + "</span>");
        $(".modal-footer").css({"display": "none"});
        $("#modal").modal("show");
        setTimeout(function () {
            $("#modal").modal("hide");
        }, 3000);
    }
    function incdec(op, _id) {
        if (op == "minus") {
            var val = $("#" + _id).text().split(" ")[0];
            if ($("#params").val() == "Inches") {
                val = parseFloat(val) - parseFloat(0.25);
                if (_id == "final_shirt_collar") {
                    if (val < ((collar_min / 2.54).toFixed(1)) || val > ((collar_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_chest") {
                    if (val < ((chest_min / 2.54).toFixed(1)) || val > ((chest_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_stomach") {
                    if (val < ((stomach_min / 2.54).toFixed(1)) || val > ((stomach_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_bicep") {
                    if (val < ((bicep_min / 2.54).toFixed(1)) || val > ((bicep_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_back_shoulder") {
                    if (val < ((back_shoulder_min / 2.54).toFixed(1)) || val > ((back_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_front_shoulder") {
                    if (val < ((front_shoulder_min / 2.54).toFixed(1)) || val > ((front_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_right") {
                    if (val < ((sleeve_right_min / 2.54).toFixed(1)) || val > ((sleeve_right_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_left") {
                    if (val < ((sleeve_left_min / 2.54).toFixed(1)) || val > ((sleeve_left_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_jacket") {
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_chest") {
                    if (val < ((chest_min / 2.54).toFixed(1)) || val > ((chest_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_stomach") {
                    if (val < ((stomach_min / 2.54).toFixed(1)) || val > ((stomach_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_bicep") {
                    if (val < ((bicep_min / 2.54).toFixed(1)) || val > ((bicep_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_back_shoulder") {
                    if (val < ((back_shoulder_min / 2.54).toFixed(1)) || val > ((back_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_front_shoulder") {
                    if (val < ((front_shoulder_min / 2.54).toFixed(1)) || val > ((front_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_right") {
                    if (val < ((sleeve_right_min / 2.54).toFixed(1)) || val > ((sleeve_right_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_left") {
                    if (val < ((sleeve_left_min / 2.54).toFixed(1)) || val > ((sleeve_left_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_jacket") {
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_chest") {
                    if (val < ((chest_min / 2.54).toFixed(1)) || val > ((chest_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_stomach") {
                    if (val < ((stomach_min / 2.54).toFixed(1)) || val > ((stomach_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_jacket") {
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_calf") {
                    if (val < ((calf_min / 2.54).toFixed(1)) || val > ((calf_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant1_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant11_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant2_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant3_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Inches");
            } else {
                val = parseFloat(val) - parseFloat(0.5);
                if (_id == "final_shirt_collar") {
                    if (val < collar_min || val > collar_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_chest") {
                    if (val < chest_min || val > chest_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_stomach") {

                    if (val < stomach_min || val > stomach_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_bicep") {
                    if (val < bicep_min || val > bicep_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_back_shoulder") {
                    if (val < back_shoulder_min || val > back_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_front_shoulder") {
                    if (val < front_shoulder_min || val > front_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_right") {
                    if (val < sleeve_right_min || val > sleeve_right_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_left") {
                    if (val < sleeve_left_min || val > sleeve_left_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_jacket") {
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_chest") {
                    if (val < chest_min || val > chest_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_stomach") {
                    if (val < stomach_min || val > stomach_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_bicep") {
                    if (val < bicep_min || val > bicep_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_back_shoulder") {
                    if (val < back_shoulder_min || val > back_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_front_shoulder") {
                    if (val < front_shoulder_min || val > front_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_right") {
                    if (val < sleeve_right_min || val > sleeve_right_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_left") {
                    if (val < sleeve_left_min || val > sleeve_left_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_jacket") {
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_chest") {
                    if (val < chest_min || val > chest_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_stomach") {
                    if (val < stomach_min || val > stomach_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_jacket") {
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_calf") {
                    if (val < calf_min || val > calf_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant1_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant11_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant2_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant3_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Cm");
            }
        }
        else if (op == "plus") {
            var val = $("#" + _id).text().split(" ")[0];
            if ($("#params").val() == "Inches") {
                val = parseFloat(val) + parseFloat(0.25);
                if (_id == "final_shirt_collar") {
                    if (val < ((collar_min / 2.54).toFixed(1)) || val > ((collar_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_chest") {
                    if (val < ((chest_min / 2.54).toFixed(1)) || val > ((chest_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_stomach") {
                    if (val < ((stomach_min / 2.54).toFixed(1)) || val > ((stomach_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_bicep") {
                    if (val < ((bicep_min / 2.54).toFixed(1)) || val > ((bicep_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_back_shoulder") {
                    if (val < ((back_shoulder_min / 2.54).toFixed(1)) || val > ((back_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_front_shoulder") {
                    if (val < ((front_shoulder_min / 2.54).toFixed(1)) || val > ((front_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_right") {
                    if (val < ((sleeve_right_min / 2.54).toFixed(1)) || val > ((sleeve_right_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_left") {
                    if (val < ((sleeve_left_min / 2.54).toFixed(1)) || val > ((sleeve_left_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_jacket") {
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_chest") {
                    if (val < ((chest_min / 2.54).toFixed(1)) || val > ((chest_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_stomach") {
                    if (val < ((stomach_min / 2.54).toFixed(1)) || val > ((stomach_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_bicep") {
                    if (val < ((bicep_min / 2.54).toFixed(1)) || val > ((bicep_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_back_shoulder") {
                    if (val < ((back_shoulder_min / 2.54).toFixed(1)) || val > ((back_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_front_shoulder") {
                    if (val < ((front_shoulder_min / 2.54).toFixed(1)) || val > ((front_shoulder_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_right") {
                    if (val < ((sleeve_right_min / 2.54).toFixed(1)) || val > ((sleeve_right_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_left") {
                    if (val < ((sleeve_left_min / 2.54).toFixed(1)) || val > ((sleeve_left_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_jacket") {
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_chest") {
                    if (val < ((chest_min / 2.54).toFixed(1)) || val > ((chest_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_stomach") {
                    if (val < ((stomach_min / 2.54).toFixed(1)) || val > ((stomach_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_jacket") {
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_calf") {
                    if (val < ((calf_min / 2.54).toFixed(1)) || val > ((calf_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant1_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant11_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant2_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant3_seat") {
                    if (val < ((seat_min / 2.54).toFixed(1)) || val > ((seat_max / 2.54).toFixed(1))) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Inches");
            }
            else {
                val = parseFloat(val) + parseFloat(0.5);
                if (_id == "final_shirt_collar") {
                    if (val < collar_min || val > collar_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_chest") {
                    if (val < chest_min || val > chest_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_stomach") {
                    if (val < stomach_min || val > stomach_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_bicep") {
                    if (val < bicep_min || val > bicep_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_back_shoulder") {
                    if (val < back_shoulder_min || val > back_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_front_shoulder") {
                    if (val < front_shoulder_min || val > front_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_right") {
                    if (val < sleeve_right_min || val > sleeve_right_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_sleeve_left") {
                    if (val < sleeve_left_min || val > sleeve_left_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_shirt_jacket") {
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_chest") {
                    if (val < chest_min || val > chest_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_stomach") {
                    if (val < stomach_min || val > stomach_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_bicep") {
                    if (val < bicep_min || val > bicep_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_back_shoulder") {
                    if (val < back_shoulder_min || val > back_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_front_shoulder") {
                    if (val < front_shoulder_min || val > front_shoulder_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_right") {
                    if (val < sleeve_right_min || val > sleeve_right_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_sleeve_left") {
                    if (val < sleeve_left_min || val > sleeve_left_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_jacket_jacket") {
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_chest") {
                    if (val < chest_min || val > chest_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_stomach") {
                    if (val < stomach_min || val > stomach_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_vest_jacket") {
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant_calf") {
                    if (val < calf_min || val > calf_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant1_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant11_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant2_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                else if (_id == "final_pant3_seat") {
                    if (val < seat_min || val > seat_max) {
                        $("#" + _id).css("color", "red");
                    } else {
                        $("#" + _id).css("color", "#999");
                    }
                }
                $("#" + _id).text(val.toFixed(1) + " Cm");
            }
        }

        /*for specification insert when tab is change start here */
        var params = $("#params").val();
        if(params == "Inches"){
            /*svalue = value.split(";")[0];*/
            var speci_jacket_chest                     = (parseFloat(($("#final_jacket_chest").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_stomach                   = (parseFloat(($("#final_jacket_stomach").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_bicep                     = (parseFloat(($("#final_jacket_bicep").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_pant_waist                       = (parseFloat(($("#final_pant_waist").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_pant_u_rise                      = (parseFloat(($("#final_pant_rise").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_pant_thigh                       = (parseFloat(($("#final_pant_thigh").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_collar                     = (parseFloat(($("#final_shirt_collar").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_front_shoulder            = (parseFloat(($("#final_jacket_front_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_back_shoulder             = (parseFloat(($("#final_jacket_back_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_back                      = (parseFloat(($("#final_jacket_jacket").html()).split(" ")[0]) * 2.54).toFixed(1);

            var speci_shirt_chest                      = (parseFloat(($("#final_shirt_chest").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_stomach                    = (parseFloat(($("#final_shirt_stomach").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_bicep                      = (parseFloat(($("#final_shirt_bicep").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_front_shoulder             = (parseFloat(($("#final_shirt_front_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_back_shoulder              = (parseFloat(($("#final_shirt_back_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_back                       = (parseFloat(($("#final_shirt_jacket").html()).split(" ")[0]) * 2.54).toFixed(1);

            var speci_vest_chest                       = (parseFloat(($("#final_vest_chest").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_vest_stomach                       = (parseFloat(($("#final_vest_stomach").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_vest_back                       = (parseFloat(($("#final_vest_jacket").html()).split(" ")[0]) * 2.54).toFixed(1);


        }
        else if(params == "Cm"){
            var speci_jacket_chest             = ($("#final_jacket_chest").html()).split(" ")[0];
            var speci_jacket_stomach           = ($("#final_jacket_stomach").html()).split(" ")[0];
            var speci_jacket_bicep             = ($("#final_jacket_bicep").html()).split(" ")[0];
            var speci_pant_waist               = ($("#final_pant_waist").html()).split(" ")[0];
            var speci_pant_u_rise              = ($("#final_pant_rise").html()).split(" ")[0];
            var speci_pant_thigh               = ($("#final_pant_thigh").html()).split(" ")[0];
            var speci_shirt_collar             = ($("#final_shirt_collar").html()).split(" ")[0];
            var speci_jacket_front_shoulder    = ($("#final_jacket_front_shoulder").html()).split(" ")[0];
            var speci_jacket_back_shoulder     = ($("#final_jacket_back_shoulder").html()).split(" ")[0];
            var speci_jacket_back              = ($("#final_jacket_jacket").html()).split(" ")[0];

            var speci_shirt_chest              = ($("#final_shirt_chest").html()).split(" ")[0];
            var speci_shirt_stomach            = ($("#final_shirt_stomach").html()).split(" ")[0];
            var speci_shirt_bicep              = ($("#final_shirt_bicep").html()).split(" ")[0];
            var speci_shirt_front_shoulder     = ($("#final_shirt_front_shoulder").html()).split(" ")[0];
            var speci_shirt_back_shoulder      = ($("#final_shirt_back_shoulder").html()).split(" ")[0];
            var speci_shirt_back               = ($("#final_shirt_jacket").html()).split(" ")[0];

            var speci_vest_chest               = ($("#final_vest_chest").html()).split(" ")[0];
            var speci_vest_stomach               = ($("#final_vest_stomach").html()).split(" ")[0];
            var speci_vest_back               = ($("#final_vest_jacket").html()).split(" ")[0];

        }
        var tab = $(".matchingTabs.tabs.active").attr("id");
       if(tab == "jacketallow"){
            var speci_type              = "jacket_specification";
            get_specification(speci_type,speci_jacket_chest,speci_jacket_stomach,speci_jacket_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_jacket_back);
        }
        else if(tab == "pantallow"){
            var speci_type              = "pant_specification";
            get_specification(speci_type,speci_jacket_chest,speci_jacket_stomach,speci_jacket_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_jacket_back);

        }
        else if(tab == "vestallow"){
            var speci_type              = "vest_specification";
            get_specification(speci_type,speci_vest_chest,speci_vest_stomach,speci_jacket_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_vest_back);

        }
        else if(tab == "shirtallow"){
            var speci_type              = "shirt_specification";
            get_specification(speci_type,speci_shirt_chest,speci_shirt_stomach,speci_shirt_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_shirt_front_shoulder,speci_shirt_back_shoulder,speci_shirt_back);

        }
        /*for specification insert when tab is change end   here */


    }
    function subtractshoulder(user_id, backshoulder, frontshoulder) {
        var url = 'api/registerUser.php';
        $.post(url, {
            'type': 'subtractshoulder',
            "user_id": user_id,
            "backshoulder": backshoulder,
            "frontshoulder": frontshoulder
        }, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                //alert("done");
            }
        });
    }
    function writeminmax(sizeData,meas_id) {
        var url = 'api/registerUser.php';
        $.post(url, {'type': 'minmaxdata'}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                var minmaxdata = data.minmaxData;
                chest_min = parseFloat(minmaxdata[0].min_val);
                chest_max = parseFloat(minmaxdata[0].max_val);
                bicep_min = parseFloat(minmaxdata[1].min_val);
                bicep_max = parseFloat(minmaxdata[1].max_val);
                back_jacket_length_min = parseFloat(minmaxdata[2].min_val);
                back_jacket_length_max = parseFloat(minmaxdata[2].max_val);
                stomach_min = parseFloat(minmaxdata[3].min_val);
                stomach_max = parseFloat(minmaxdata[3].max_val);
                sleeve_left_min = parseFloat(minmaxdata[4].min_val);
                sleeve_left_max = parseFloat(minmaxdata[4].max_val);
                sleeve_right_min = parseFloat(minmaxdata[5].min_val);
                sleeve_right_max = parseFloat(minmaxdata[5].max_val);
                back_shoulder_min = parseFloat(minmaxdata[6].min_val);
                back_shoulder_max = parseFloat(minmaxdata[6].max_val);
                front_shoulder_min = parseFloat(minmaxdata[6].min_val);
                front_shoulder_max = parseFloat(minmaxdata[6].max_val);
                calf_min = parseFloat(minmaxdata[7].min_val);
                calf_max = parseFloat(minmaxdata[7].max_val);
                collar_min = parseFloat(minmaxdata[8].min_val);
                collar_max = parseFloat(minmaxdata[8].max_val);
                seat_min = parseFloat(minmaxdata[9].min_val);
                seat_max = parseFloat(minmaxdata[9].max_val);
                showminmaxrange();
                getMeasureData(sizeData,meas_id);
                //measurementData(meas_id);
            }
            else
            {
                showMessage(data.Message,"red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function showminmaxrange() {
        var url = 'api/registerUser.php';
        $.post(url, {'type': 'minmaxdata'}, function (data) {
            var Status = data.Status;
            if (Status == "Success") {
                var minmaxdata = data.minmaxData;
                if ($("#params").val() == "Inches") {
                    for (var i = 0; i < minmaxdata.length; i++) {
                        if (minmaxdata[i].parameter == "shoulder_") {
                            var min = (minmaxdata[i].min_val / 2.54).toFixed(1);
                            var max = (minmaxdata[i].max_val / 2.54).toFixed(1);
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("." + minmaxdata[i].parameter + "textb").html(text);
                            $("." + minmaxdata[i].parameter + "textf").html(text);
                        } else {
                            var min = (minmaxdata[i].min_val / 2.54).toFixed(1);
                            var max = (minmaxdata[i].max_val / 2.54).toFixed(1);
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("." + minmaxdata[i].parameter + "text").html(text);
                        }
                    }
                } else {
                    for (var i = 0; i < minmaxdata.length; i++) {
                        if (minmaxdata[i].parameter == "shoulder_") {
                            var min = minmaxdata[i].min_val;
                            var max = minmaxdata[i].max_val;
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("." + minmaxdata[i].parameter + "textb").html(text);
                            $("." + minmaxdata[i].parameter + "textf").html(text);
                        } else {
                            var min = minmaxdata[i].min_val;
                            var max = minmaxdata[i].max_val;
                            var text = "<br>Range ( " + min + " - " + max + " )";
                            $("." + minmaxdata[i].parameter + "text").html(text);
                        }
                    }
                }
            }
        });
    }
    function measurementData(measurement_id){
        var url = "api/registerUser.php";
        $.post(url, {"type": "getParticularMeasurement", "meas_id": measurement_id}, function (data) {
            var status = data.Status;
            if (status == "Success") {

                var measurement = data.measurements;
                if(measurement.unit_type == "Inches" && $("#params").val() == "Inches"){
                    var val = (parseFloat(measurement.shirt_collar));
                    $("#final_shirt_collar").html(val + " Inches");
                    if (val < ((collar_min/2.54).toFixed(1)) || val > ((collar_max/2.54).toFixed(1))) {
                        $("#final_shirt_collar").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_chest));
                    $("#final_shirt_chest").html(parseFloat(measurement.shirt_chest)+ " Inches");
                    if (val < ((chest_min/2.54).toFixed(1)) || val > ((chest_max/2.54).toFixed(1))) {
                        $("#final_shirt_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_chest));
                    $("#final_jacket_chest").html(parseFloat(measurement.jacket_chest)+ " Inches");
                    if (val < ((chest_min/2.54).toFixed(1)) || val > ((chest_max/2.54).toFixed(1))) {
                        $("#final_jacket_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_chest));
                    $("#final_vest_chest").html(parseFloat(measurement.vest_chest)+ " Inches");
                    if (val < ((chest_min/2.54).toFixed(1)) || val > ((chest_max/2.54).toFixed(1))) {
                        $("#final_vest_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_stomach));
                    $("#final_shirt_stomach").html(parseFloat(measurement.shirt_stomach)+ " Inches");
                    if (val < ((stomach_min/2.54).toFixed(1)) || val > ((stomach_max/2.54).toFixed(1))) {
                        $("#final_shirt_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_stomach));
                    $("#final_jacket_stomach").html(parseFloat(measurement.jacket_stomach)+ " Inches");
                    if (val < ((stomach_min/2.54).toFixed(1)) || val > ((stomach_max/2.54).toFixed(1))) {
                        $("#final_jacket_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_stomach));
                    $("#final_vest_stomach").html(parseFloat(measurement.vest_stomach)+ " Inches");
                    if (val < ((stomach_min/2.54).toFixed(1)) || val > ((stomach_max/2.54).toFixed(1))) {
                        $("#final_vest_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_seat));
                    $("#final_shirt_seat").html(parseFloat(measurement.shirt_seat)+ " Inches");
                    if (val < ((seat_min/2.54).toFixed(1)) || val > ((seat_max/2.54).toFixed(1))) {
                        $("#final_shirt_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_seat));
                    $("#final_jacket_seat").html(parseFloat(measurement.jacket_seat)+ " Inches");
                    if (val < ((seat_min/2.54).toFixed(1)) || val > ((seat_max/2.54).toFixed(1))) {
                        $("#final_jacket_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_seat));
                    $("#final_pant_seat").html(parseFloat(measurement.pant_seat)+ " Inches");
                    if (val < ((seat_min/2.54).toFixed(1)) || val > ((seat_max/2.54).toFixed(1))) {
                        $("#final_pant_seat").css("color", "red");
                        $("#final_pant1_seat").css("color", "red");
                        $("#final_pant11_seat").css("color", "red");
                        $("#final_pant2_seat").css("color", "red");
                        $("#final_pant3_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_bicep));
                    $("#final_shirt_bicep").html(parseFloat(measurement.shirt_bicep)+ " Inches");
                    if (val < ((bicep_min/2.54).toFixed(1)) || val > ((bicep_max/2.54).toFixed(1))) {
                        $("#final_shirt_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_bicep));
                    $("#final_jacket_bicep").html(parseFloat(measurement.jacket_bicep)+ " Inches");
                    if (val < ((bicep_min/2.54).toFixed(1)) || val > ((bicep_max/2.54 ).toFixed(1))) {
                        $("#final_jacket_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_shoulder));
                    $("#final_shirt_back_shoulder").html(parseFloat(measurement.shirt_back_shoulder)+ " Inches");
                    if (val < ((back_shoulder_min/2.54).toFixed(1)) || val > ((back_shoulder_max/2.54).toFixed(1))) {
                        $("#final_shirt_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_back_shoulder));
                    $("#final_jacket_back_shoulder").html(parseFloat(measurement.jacket_back_shoulder)+ " Inches");
                    if (val < ((back_shoulder_min/2.54).toFixed(1)) || val > ((back_shoulder_max/2.54).toFixed(1))) {
                        $("#final_jacket_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_front_shoulder));
                    $("#final_shirt_front_shoulder").html(parseFloat(measurement.shirt_front_shoulder)+ " Inches");
                    if (val < ((front_shoulder_min/2.54).toFixed(1)) || val > ((front_shoulder_max/2.54).toFixed(1))) {
                        $("#final_shirt_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_front_shoulder));
                    $("#final_jacket_front_shoulder").html(parseFloat(measurement.jacket_front_shoulder)+ " Inches");
                    if (val < ((front_shoulder_min/2.54).toFixed(1)) || val > ((front_shoulder_max/2.54).toFixed(1))) {
                        $("#final_jacket_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_left));
                    $("#final_shirt_sleeve_left").html(parseFloat(measurement.shirt_sleeve_left)+ " Inches");
                    if (val < ((sleeve_left_min/2.54).toFixed(1)) || val > ((sleeve_left_max/2.54).toFixed(1))) {
                        $("#final_shirt_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_left));
                    $("#final_jacket_sleeve_left").html(parseFloat(measurement.jacket_sleeve_left)+ " Inches");
                    if (val < ((sleeve_left_min/2.54).toFixed(1)) || val > ((sleeve_left_max/2.54).toFixed(1))) {
                        $("#final_jacket_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_right));
                    $("#final_shirt_sleeve_right").html(parseFloat(measurement.shirt_sleeve_right)+ " Inches");
                    if (val < ((sleeve_right_min/2.54).toFixed(1)) || val > ((sleeve_right_max/2.54).toFixed(1))) {
                        $("#final_shirt_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_right));
                    $("#final_jacket_sleeve_right").html(parseFloat(measurement.jacket_sleeve_right)+ " Inches");
                    if (val < ((sleeve_right_min/2.54).toFixed(1)) || val > ((sleeve_right_max/2.54).toFixed(1))) {
                        $("#final_jacket_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_thigh));
                    $("#final_pant_thigh").html(parseFloat(measurement.pant_thigh)+ " Inches");
                    var val = (parseFloat(measurement.shirt_wrist));
                    $("#final_shirt_wrist").html(parseFloat(measurement.shirt_wrist)+ " Inches");
                    var val = (parseFloat(measurement.jacket_wrist));
                    $("#final_jacket_wrist").html(parseFloat(measurement.jacket_wrist)+ " Inches");
                    var val = (parseFloat(measurement.pant_waist));
                    $("#final_pant_waist").html(parseFloat(measurement.pant_waist)+ " Inches");
                    var val = (parseFloat(measurement.pant_calf));
                    $("#final_pant_calf").html(parseFloat(measurement.pant_calf)+ " Inches");
                    if (val < ((calf_min/2.54).toFixed(1)) || val > ((calf_max/2.54).toFixed(1))) {
                        $("#final_pant_calf").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_back_waist_height));
                    $("#final_pant_b_waist").html(parseFloat(measurement.pant_back_waist_height)+ " Inches");
                    var val = (parseFloat(measurement.pant_front_waist_height));
                    $("#final_pant_f_waist").html(parseFloat(measurement.pant_front_waist_height)+ " Inches");
                    var val = (parseFloat(measurement.pant_knee));
                    $("#final_pant_knee").html(parseFloat(measurement.pant_knee)+ " Inches");
                    var val = (parseFloat(measurement.shirt_back_jacket_length));
                    $("#final_shirt_jacket").html(parseFloat(measurement.shirt_back_jacket_length)+ " Inches");
                    if (val < ((back_jacket_length_min/2.54 ).toFixed(1)) || val > ((back_jacket_length_max/2.54 ).toFixed(1))) {
                        $("#final_shirt_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_jacket_length));
                    $("#final_jacket_jacket").html(parseFloat(measurement.jacket_back_jacket_length)+ " Inches");
                    if (val < ((back_jacket_length_min/2.54 ).toFixed(1)) || val > ((back_jacket_length_max/2.54 ).toFixed(1))) {
                        $("#final_jacket_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_back_jacket_length));
                    $("#final_vest_jacket").html(parseFloat(measurement.vest_back_jacket_length)+ " Inches");
                    if (val < ((back_jacket_length_min/2.54).toFixed(1)) || val > ((back_jacket_length_max/2.54 ).toFixed(1))) {
                        $("#final_vest_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_u_rise));
                    $("#final_pant_rise").html(parseFloat(measurement.pant_u_rise)+ " Inches");
                    var val = (parseFloat(measurement.pant_pant_left_outseam));
                    $("#final_pant_p_left").html(parseFloat(measurement.pant_pant_left_outseam)+ " Inches");
                    var val = (parseFloat(measurement.pant_pant_right_outseam));
                    $("#final_pant_p_right").html(parseFloat(measurement.pant_pant_right_outseam)+ " Inches");
                    var val = (parseFloat(measurement.pant_bottom));
                    $("#final_pant_bottom").html(parseFloat(measurement.pant_bottom)+ " Inches");
                }
                else if(measurement.unit_type == "Cm" && $("#params").val() == "Inches"){
                    var val = (parseFloat(measurement.shirt_collar)/2.54).toFixed(1);
                    $("#final_shirt_collar").html(val+" Inches");

                    if (val < parseFloat((collar_min / 2.54 ).toFixed(1)) || val > parseFloat((collar_max  / 2.54).toFixed(1))) {
                        $("#final_shirt_collar").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_chest)/2.54).toFixed(1);
                    $("#final_shirt_chest").html(val+" Inches");

                    if (val < parseFloat((chest_min / 2.54).toFixed(1)) || val > parseFloat((chest_max / 2.54).toFixed(1))) {
                        $("#final_shirt_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_chest)/2.54).toFixed(1);
                    $("#final_jacket_chest").html(val+" Inches");

                    if (val < parseFloat((chest_min / 2.54).toFixed(1)) || val > parseFloat((chest_max / 2.54).toFixed(1))) {
                        $("#final_jacket_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_chest)/2.54).toFixed(1);
                    $("#final_vest_chest").html(val+" Inches");

                    if (val < parseFloat((chest_min / 2.54).toFixed(1)) || val > parseFloat((chest_max / 2.54).toFixed(1))) {
                        $("#final_vest_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_stomach)/2.54).toFixed(1);
                    $("#final_shirt_stomach").html(val+ " Inches");

                    if (val < parseFloat((stomach_min / 2.54).toFixed(1)) || val > parseFloat((stomach_max / 2.54).toFixed(1))) {
                        $("#final_shirt_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_stomach)/2.54).toFixed(1);
                    $("#final_jacket_stomach").html(val+ " Inches");

                    if (val < parseFloat((stomach_min / 2.54).toFixed(1)) || val > parseFloat((stomach_max / 2.54).toFixed(1))) {
                        $("#final_jacket_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_stomach)/2.54).toFixed(1);

                    $("#final_vest_stomach").html(val+ " Inches");
                    if (val < parseFloat((stomach_min / 2.54).toFixed(1)) || val > parseFloat((stomach_max / 2.54).toFixed(1))) {
                        $("#final_vest_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_seat)/2.54).toFixed(1);
                    if (val < parseFloat((seat_min / 2.54).toFixed(1)) || val > parseFloat((seat_max / 2.54).toFixed(1))) {
                        $("#final_shirt_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_seat)/2.54).toFixed(1);
                    $("#final_jacket_seat").html(val+ " Inches");
                    if (val < parseFloat((seat_min / 2.54).toFixed(1)) || val > parseFloat((seat_max / 2.54).toFixed(1))) {
                        $("#final_jacket_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_seat)/2.54).toFixed(1);
                    $("#final_pant_seat").html(val+" Inches");
                    if (val < parseFloat((seat_min / 2.54).toFixed(1)) || val > parseFloat((seat_max / 2.54).toFixed(1))) {
                        $("#final_pant_seat").css("color", "red");
                        $("#final_pant1_seat").css("color", "red");
                        $("#final_pant11_seat").css("color", "red");
                        $("#final_pant2_seat").css("color", "red");
                        $("#final_pant3_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_bicep)/2.54).toFixed(1);
                    $("#final_shirt_bicep").html(val+" Inches");

                    if (val < parseFloat((bicep_min / 2.54).toFixed(1)) || val > parseFloat((bicep_max / 2.54).toFixed(1))) {
                        $("#final_shirt_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_bicep)/2.54).toFixed(1);
                    $("#final_jacket_bicep").html(val+" Inches");

                    if (val < parseFloat((bicep_min / 2.54).toFixed(1)) || val > parseFloat((bicep_max / 2.54).toFixed(1))) {
                        $("#final_jacket_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_shoulder)/2.54).toFixed(1);
                    $("#final_shirt_back_shoulder").html(val+" Inches");

                    if (val < parseFloat((back_shoulder_min / 2.54).toFixed(1)) || val > parseFloat((back_shoulder_max / 2.54).toFixed(1))) {
                        $("#final_shirt_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_back_shoulder)/2.54).toFixed(1);
                    $("#final_jacket_back_shoulder").html(val+" Inches");

                    if (val < parseFloat((back_shoulder_min / 2.54).toFixed(1)) || val > parseFloat((back_shoulder_max / 2.54).toFixed(1))) {
                        $("#final_jacket_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_front_shoulder)/2.54).toFixed(1);
                    $("#final_shirt_front_shoulder").html(val+" Inches");

                    if (val < parseFloat((front_shoulder_min / 2.54).toFixed(1)) || val > parseFloat((front_shoulder_max / 2.54).toFixed(1))) {
                        $("#final_shirt_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_front_shoulder)/2.54).toFixed(1);
                    $("#final_jacket_front_shoulder").html(val+" Inches");

                    if (val < parseFloat((front_shoulder_min / 2.54).toFixed(1)) || val > parseFloat((front_shoulder_max / 2.54).toFixed(1))) {
                        $("#final_jacket_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_left)/2.54).toFixed(1);
                    $("#final_shirt_sleeve_left").html(val+" Inches");
                    if (val < parseFloat((sleeve_left_min / 2.54).toFixed(1)) || val > parseFloat((sleeve_left_max / 2.54).toFixed(1))) {
                        $("#final_shirt_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_left)/2.54).toFixed(1);
                    $("#final_jacket_sleeve_left").html(val+" Inches");
                    if (val < parseFloat((sleeve_left_min / 2.54).toFixed(1)) || val > parseFloat((sleeve_left_max / 2.54).toFixed(1))) {
                        $("#final_jacket_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_right)/2.54).toFixed(1);
                    $("#final_shirt_sleeve_right").html(val+" Inches");
                    if (val < parseFloat((sleeve_right_min / 2.54).toFixed(1)) || val > parseFloat((sleeve_right_max / 2.54).toFixed(1))) {
                        $("#final_shirt_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_right)/2.54).toFixed(1);
                    $("#final_jacket_sleeve_right").html(val+" Inches");
                    if (val < parseFloat((sleeve_right_min / 2.54).toFixed(1)) || val > parseFloat((sleeve_right_max / 2.54).toFixed(1))) {
                        $("#final_jacket_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_thigh)/2.54).toFixed(1);
                    $("#final_pant_thigh").html(val+" Inches");

                    var val = (parseFloat(measurement.shirt_wrist)/2.54).toFixed(1);
                    $("#final_shirt_wrist").html(val+" Inches");
                    var val = (parseFloat(measurement.jacket_wrist)/2.54).toFixed(1);
                    $("#final_jacket_wrist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_waist)/2.54).toFixed(1);

                    $("#final_pant_waist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_calf)/2.54).toFixed(1);
                    $("#final_pant_calf").html(val+" Inches");
                    if (val < parseFloat((calf_min / 2.54).toFixed(1)) || val > parseFloat((calf_max / 2.54).toFixed(1))) {
                        $("#final_pant_calf").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_back_waist_height)/2.54).toFixed(1);
                    $("#final_pant_b_waist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_front_waist_height)/2.54).toFixed(1);
                    $("#final_pant_f_waist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_knee)/2.54).toFixed(1);
                    $("#final_pant_knee").html(val+" Inches");
                    var val = (parseFloat(measurement.shirt_back_jacket_length)/2.54).toFixed(1);

                    $("#final_shirt_jacket").html(val+" Inches");
                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#final_shirt_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_jacket_length)/2.54).toFixed(1);
                    $("#final_jacket_jacket").html(val+" Inches");

                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#final_jacket_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_back_jacket_length)/2.54).toFixed(1);
                    $("#final_vest_jacket").html(val+" Inches");

                    if (val < ((back_jacket_length_min / 2.54).toFixed(1)) || val > ((back_jacket_length_max / 2.54).toFixed(1))) {
                        $("#final_vest_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_u_rise)/2.54).toFixed(1);
                    $("#final_pant_rise").html(val+" Inches");

                    var val = (parseFloat(measurement.pant_pant_left_outseam)/2.54).toFixed(1);
                    $("#final_pant_p_left").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_pant_right_outseam)/2.54).toFixed(1);
                    $("#final_pant_p_right").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_bottom)/2.54).toFixed(1);
                    $("#final_pant_bottom").html(val+" Inches");
                }
                else if(measurement.unit_type == "Cm" && $("#params").val() == "Cm"){
                    var val = (parseFloat(measurement.shirt_collar)).toFixed(1);
                    $("#final_shirt_collar").html(val+" Cm");
                    if (val < parseFloat(collar_min).toFixed(1) || val > parseFloat(collar_max).toFixed(1)) {
                        $("#final_shirt_collar").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_chest)).toFixed(1);
                    $("#final_shirt_chest").html(val+" Inches");
                    if (val < parseFloat((chest_min).toFixed(1)) || val > parseFloat((chest_max).toFixed(1))) {
                        $("#final_shirt_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_chest)).toFixed(1);
                    $("#final_jacket_chest").html(val+" Cm");
                    if (val < parseFloat((chest_min).toFixed(1)) || val > parseFloat((chest_max).toFixed(1))) {
                        $("#final_jacket_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_chest)).toFixed(1);
                    $("#final_vest_chest").html(val+" Cm");
                    if (val < parseFloat((chest_min).toFixed(1)) || val > parseFloat((chest_max).toFixed(1))) {
                        $("#final_vest_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_stomach)).toFixed(1);
                    $("#final_shirt_stomach").html(val+ " Cm");
                    if (val < parseFloat((stomach_min).toFixed(1)) || val > parseFloat((stomach_max).toFixed(1))) {
                        $("#final_shirt_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_stomach)).toFixed(1);
                    $("#final_jacket_stomach").html(val+ " Cm");
                    if (val < parseFloat((stomach_min).toFixed(1)) || val > parseFloat((stomach_max).toFixed(1))) {
                        $("#final_jacket_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_stomach)).toFixed(1);
                    $("#final_vest_stomach").html(val+ " Cm");
                    if (val < parseFloat((stomach_min).toFixed(1)) || val > parseFloat((stomach_max).toFixed(1))) {
                        $("#final_vest_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_seat)).toFixed(1);
                    $("#final_shirt_seat").html(val+ " Cm");
                    if (val < parseFloat((seat_min).toFixed(1)) || val > parseFloat((seat_max).toFixed(1))) {
                        $("#final_shirt_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_seat)).toFixed(1);
                    $("#final_jacket_seat").html(val+ " Cm");
                    if (val < parseFloat((seat_min).toFixed(1)) || val > parseFloat((seat_max).toFixed(1))) {
                        $("#final_jacket_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_seat)).toFixed(1);
                    $("#final_pant_seat").html(val+" Cm");
                    if (val < parseFloat((seat_min).toFixed(1)) || val > parseFloat((seat_max).toFixed(1))) {
                        $("#final_pant_seat").css("color", "red");
                        $("#final_pant1_seat").css("color", "red");
                        $("#final_pant11_seat").css("color", "red");
                        $("#final_pant2_seat").css("color", "red");
                        $("#final_pant3_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_bicep)).toFixed(1);
                    $("#final_shirt_bicep").html(val+" Cm");
                    if (val < parseFloat((bicep_min).toFixed(1)) || val > parseFloat((bicep_max).toFixed(1))) {
                        $("#final_shirt_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_bicep)).toFixed(1);
                    $("#final_jacket_bicep").html(val+" Cm");
                    if (val < parseFloat((bicep_min).toFixed(1)) || val > parseFloat((bicep_max).toFixed(1))) {
                        $("#final_jacket_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_shoulder)).toFixed(1);
                    $("#final_shirt_back_shoulder").html(val+" Cm");
                    if (val < parseFloat((back_shoulder_min).toFixed(1)) || val > parseFloat((back_shoulder_max).toFixed(1))) {
                        $("#final_shirt_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_back_shoulder)).toFixed(1);
                    $("#final_jacket_back_shoulder").html(val+" Cm");
                    if (val < parseFloat((back_shoulder_min).toFixed(1)) || val > parseFloat((back_shoulder_max).toFixed(1))) {
                        $("#final_jacket_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_front_shoulder)).toFixed(1);
                    $("#final_shirt_front_shoulder").html(val+" Cm");
                    if (val < parseFloat((front_shoulder_min).toFixed(1)) || val > parseFloat((front_shoulder_max).toFixed(1))) {
                        $("#final_shirt_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_front_shoulder)).toFixed(1);
                    $("#final_jacket_front_shoulder").html(val+" Cm");
                    if (val < parseFloat((front_shoulder_min).toFixed(1)) || val > parseFloat((front_shoulder_max).toFixed(1))) {
                        $("#final_jacket_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_left)).toFixed(1);
                    $("#final_shirt_sleeve_left").html(val+" Cm");
                    if (val < parseFloat((sleeve_left_min).toFixed(1)) || val > parseFloat((sleeve_left_max).toFixed(1))) {
                        $("#final_shirt_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_left)).toFixed(1);
                    $("#final_jacket_sleeve_left").html(val+" Cm");
                    if (val < parseFloat((sleeve_left_min).toFixed(1)) || val > parseFloat((sleeve_left_max).toFixed(1))) {
                        $("#final_jacket_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_right)).toFixed(1);
                    $("#final_shirt_sleeve_right").html(val+" Cm");
                    if (val < parseFloat((sleeve_right_min).toFixed(1)) || val > parseFloat((sleeve_right_max).toFixed(1))) {
                        $("#final_shirt_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_right)).toFixed(1);
                    $("#final_jacket_sleeve_right").html(val+" Cm");
                    if (val < parseFloat((sleeve_right_min).toFixed(1)) || val > parseFloat((sleeve_right_max).toFixed(1))) {
                        $("#final_jacket_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_thigh)).toFixed(1);
                    $("#final_pant_thigh").html(val+" Cm");
                    var val = (parseFloat(measurement.shirt_wrist)).toFixed(1);
                    $("#final_shirt_wrist").html(val+" Cm");
                    var val = (parseFloat(measurement.jacket_wrist)).toFixed(1);
                    $("#final_jacket_wrist").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_waist)).toFixed(1);
                    $("#final_pant_waist").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_calf)).toFixed(1);
                    $("#final_pant_calf").html(val+" Cm");
                    if (val < parseFloat((calf_min).toFixed(1)) || val > parseFloat((calf_max).toFixed(1))) {
                        $("#final_pant_calf").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_back_waist_height)).toFixed(1);
                    $("#final_pant_b_waist").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_front_waist_height)).toFixed(1);
                    $("#final_pant_f_waist").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_knee)).toFixed(1);
                    $("#final_pant_knee").html(val+" Cm");
                    var val = (parseFloat(measurement.shirt_back_jacket_length)).toFixed(1);
                    $("#final_shirt_jacket").html(val+" Cm");
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#final_shirt_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_jacket_length)).toFixed(1);
                    $("#final_jacket_jacket").html(val+" Cm");
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#final_jacket_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_back_jacket_length)).toFixed(1);
                    $("#final_vest_jacket").html(val+" Cm");
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#final_vest_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_u_rise)).toFixed(1);
                    $("#final_pant_rise").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_pant_left_outseam)).toFixed(1);
                    $("#final_pant_p_left").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_pant_right_outseam)).toFixed(1);
                    $("#final_pant_p_right").html(val+" Cm");
                    var val = (parseFloat(measurement.pant_bottom)).toFixed(1);
                    $("#final_pant_bottom").html(val+" Cm");
                }
                else if(measurement.unit_type == "Inches" && $("#params").val() == "Cm"){
                    var val = (parseFloat(measurement.shirt_collar)*2.54).toFixed(1);
                    $("#final_shirt_collar").html(val+" Inches");
                    if (val < collar_min || val > collar_max ) {
                        $("#final_shirt_collar").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_chest)*2.54).toFixed(1);
                    $("#final_shirt_chest").html(val+" Inches");
                    if (val < chest_min || val > chest_max) {
                        $("#final_shirt_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_chest)*2.54).toFixed(1);
                    $("#final_jacket_chest").html(val+" Inches");
                    if (val < chest_min || val > chest_max) {
                        $("#final_jacket_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_chest)*2.54).toFixed(1);
                    $("#final_vest_chest").html(val+" Inches");
                    if (val < chest_min || val > chest_max) {
                        $("#final_vest_chest").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_stomach)*2.54).toFixed(1);
                    $("#final_shirt_stomach").html(val+ " Inches");
                    if (val < stomach_min || val > stomach_max) {
                        $("#final_shirt_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_stomach)*2.54).toFixed(1);
                    $("#final_jacket_stomach").html(val+ " Inches");
                    if (val < stomach_min || val > stomach_max) {
                        $("#final_jacket_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_stomach)*2.54).toFixed(1);
                    $("#final_vest_stomach").html(val+ " Inches");
                    if (val < stomach_min || val > stomach_max) {
                        $("#final_vest_stomach").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_seat)*2.54).toFixed(1);
                    $("#final_shirt_seat").html(val+ " Inches");
                    if (val < seat_min || val > seat_max) {
                        $("#final_shirt_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_seat)*2.54).toFixed(1);
                    $("#final_jacket_seat").html(val+ " Inches");
                    if (val < seat_min || val > seat_max) {
                        $("#final_jacket_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_seat)*2.54).toFixed(1);
                    $("#final_pant_seat").html(val+" Inches");
                    if (val < seat_min || val > seat_max) {
                        $("#final_pant_seat").css("color", "red");
                        $("#final_pant1_seat").css("color", "red");
                        $("#final_pant11_seat").css("color", "red");
                        $("#final_pant2_seat").css("color", "red");
                        $("#final_pant3_seat").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_bicep)*2.54).toFixed(1);
                    $("#final_shirt_bicep").html(val+" Inches");
                    if (val < bicep_min || val > bicep_max) {
                        $("#final_shirt_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_bicep)*2.54).toFixed(1);
                    $("#final_jacket_bicep").html(val+" Inches");
                    if (val < bicep_min || val > bicep_max) {
                        $("#final_jacket_bicep").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_shoulder)*2.54).toFixed(1);
                    $("#final_shirt_back_shoulder").html(val+" Inches");
                    if (val < back_shoulder_min || val > back_shoulder_max) {
                        $("#final_shirt_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_back_shoulder)*2.54).toFixed(1);
                    $("#final_jacket_back_shoulder").html(val+" Inches");
                    if (val < back_shoulder_min || val > back_shoulder_max) {
                        $("#final_jacket_back_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_front_shoulder)*2.54).toFixed(1);
                    $("#final_shirt_front_shoulder").html(val+" Inches");
                    if (val < front_shoulder_min || val > front_shoulder_max) {
                        $("#final_shirt_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_front_shoulder)*2.54).toFixed(1);
                    $("#final_jacket_front_shoulder").html(val+" Inches");
                    if (val < front_shoulder_min || val > front_shoulder_max) {
                        $("#final_jacket_front_shoulder").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_left)*2.54).toFixed(1);
                    $("#final_shirt_sleeve_left").html(val+" Inches");
                    if (val < sleeve_left_min || val > sleeve_left_max) {
                        $("#final_shirt_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_left)*2.54).toFixed(1);
                    $("#final_jacket_sleeve_left").html(val+" Inches");
                    if (val < sleeve_left_min || val > sleeve_left_max) {
                        $("#final_jacket_sleeve_left").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_sleeve_right)*2.54).toFixed(1);
                    $("#final_shirt_sleeve_right").html(val+" Inches");
                    if (val < sleeve_right_min || val > sleeve_right_max) {
                        $("#final_shirt_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.jacket_sleeve_right)*2.54).toFixed(1);
                    $("#final_jacket_sleeve_right").html(val+" Inches");
                    if (val < sleeve_right_min || val > sleeve_right_max) {
                        $("#final_jacket_sleeve_right").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_thigh)*2.54).toFixed(1);
                    $("#final_pant_thigh").html(val+" Inches");
                    var val = (parseFloat(measurement.shirt_wrist)*2.54).toFixed(1);
                    $("#final_shirt_wrist").html(val+" Inches");
                    var val = (parseFloat(measurement.jacket_wrist)*2.54).toFixed(1);
                    $("#final_jacket_wrist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_waist)*2.54).toFixed(1);
                    $("#final_pant_waist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_calf)*2.54).toFixed(1);
                    $("#final_pant_calf").html(val+" Inches");
                    if (val < calf_min || val > calf_max) {
                        $("#final_pant_calf").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_back_waist_height)*2.54).toFixed(1);
                    $("#final_pant_b_waist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_front_waist_height)*2.54).toFixed(1);
                    $("#final_pant_f_waist").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_knee)*2.54).toFixed(1);
                    $("#final_pant_knee").html(val+" Inches");
                    var val = (parseFloat(measurement.shirt_back_jacket_length)*2.54).toFixed(1);
                    $("#final_shirt_jacket").html(val+" Inches");
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#final_shirt_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.shirt_back_jacket_length)*2.54).toFixed(1);
                    $("#final_jacket_jacket").html(val+" Inches");
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#final_jacket_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.vest_back_jacket_length)*2.54).toFixed(1);
                    $("#final_vest_jacket").html(val+" Inches");
                    if (val < back_jacket_length_min || val > back_jacket_length_max) {
                        $("#final_vest_jacket").css("color", "red");
                    }
                    var val = (parseFloat(measurement.pant_u_rise)*2.54).toFixed(1);
                    $("#final_pant_rise").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_pant_left_outseam)*2.54).toFixed(1);
                    $("#final_pant_p_left").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_pant_right_outseam)*2.54).toFixed(1);
                    $("#final_pant_p_right").html(val+" Inches");
                    var val = (parseFloat(measurement.pant_bottom)*2.54).toFixed(1);
                    $("#final_pant_bottom").html(val+" Inches");
                }
                getPantvaluepleat(panswer1);
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");

                if(measurement.unit_type == "Inches"){
                    var speci_jacket_chest          =   (measurement.jacket_chest   * 2.56).toFixed(2);
                    var speci_jacket_stomach        =   (measurement.jacket_stomach * 2.56).toFixed(2);
                    var speci_jacket_bicep          =   (measurement.jacket_bicep   * 2.56).toFixed(2);
                    var speci_jacket_back           =   (measurement.jacket_back_jacket_length  * 2.56).toFixed(2);
                    var speci_jacket_front_shoulder =   (measurement.jacket_back_shoulder   * 2.56).toFixed(2);
                    var speci_jacket_back_shoulder  =   (measurement.jacket_front_shoulder  * 2.56).toFixed(2);
                }
                else if(measurement.unit_type  == "Cm"){
                    var speci_jacket_chest          =   measurement.jacket_chest;
                    var speci_jacket_stomach        =   measurement.jacket_stomach;
                    var speci_jacket_bicep          =   measurement.jacket_bicep;
                    var speci_jacket_back           =   measurement.jacket_back_jacket_length;
                    var speci_jacket_front_shoulder =   measurement.jacket_back_shoulder ;
                    var speci_jacket_back_shoulder  =   measurement.jacket_front_shoulder;

                }
                get_specification("jacket_specification",speci_jacket_chest,speci_jacket_stomach,speci_jacket_bicep,100,100,100,
                    100,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_jacket_back);

            }
            else{
                showMessage("Invalid Measurement Data","red");
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function get_specification(speci_type,chest,stomach,bicep,waist,u_rise,thigh,collar,front_shoulder,back_shoulder,back_jacket_length){
        front_shoulder = front_shoulder;
        back_shoulder = back_shoulder;
        var shoulder = ((parseFloat(front_shoulder) + parseFloat(back_shoulder))/2).toFixed(2);
        var url = "api/registerUser.php";
        $.post(url,{"type":"getMatchingRange", "chest":chest,"stomach":stomach,"bicep":bicep,
            "waist":waist,"u_rise":u_rise,"thigh":thigh,"collar":collar,"shoulder":shoulder,
            "back_jacket_length":back_jacket_length},function(data){
            var status = data.Status;
            if(status == "Success")
            {
                var jacketRange = data.jacketRange;
                var pantRange = data.pantRange;
                var vestRange = data.vestRange;
                var shirtRange = data.shirtRange;
                var shirtNeckRange = data.shirtNeckRange;
                var specification_li = "<li class='speci_li jacket_specification_li'>Your Jacket specification is : " +
                    "<i class='fa fa-minus' style='margin-right:5px;cursor:pointer' onclick=rangeIncDec('minus','jacket_specification','jacketRange')>" +
                    "</i><span id='jacketRange'>"+jacketRange+"</span> R" +
                    "<i class='fa fa-plus' style='margin-left:5px;cursor:pointer' onclick=rangeIncDec('plus','jacket_specification','jacketRange')></i></li>" +
                    "<li class='speci_li vest_specification_li'>Your Vest specification is : " +
                    "<i class='fa fa-minus' style='margin-right:5px;cursor:pointer' onclick=rangeIncDec('minus','vest_specification','vestRange')></i>" +
                    "<span id='vestRange'> "+vestRange+"</span> R" +
                    "<i class='fa fa-plus' style='margin-left:5px;cursor:pointer' onclick=rangeIncDec('plus','vest_specification','vestRange')></i></li>"+
                    "<li class='speci_li shirt_specification_li'>Your Shirt specification is : " +
                    "<i class='fa fa-minus' style='margin-right:5px;cursor:pointer' onclick=rangeIncDec('minus','shirt_specification','shirtRange')></i>" +
                    "<span id='shirtRange'>"+shirtRange+"</span>" +
                    "<i class='fa fa-plus' style='margin-left:5px;cursor:pointer' onclick=rangeIncDec('plus','shirt_specification','shirtRange')></i></li>"+
                    "<li class='speci_li pant_specification_li'>Your Pant specification is : " +
                    "<i class='fa fa-minus' style='margin-right:5px;cursor:pointer' onclick=rangeIncDec('minus','pant_specification','pantRange')></i>" +
                    "<span id='pantRange'>"+pantRange+"</span> R" +
                    "<i class='fa fa-plus' style='margin-left:5px;cursor:pointer' onclick=rangeIncDec('plus','pant_specification','pantRange')></i></li>"+
                    "<li class='speci_li shirt_specification_li'>Your Shirt Specification : <span id='shirtNeckRange'>"+shirtNeckRange+"</span>" +
                    "( size based on the neck measurement only )</li>";
                $(".size_specification").html(specification_li);
                $(".speci_li").hide();
                $("."+speci_type+"_li").show();
            }
            else
            {
                showMessage(data.Message, 'red');
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }

        });
    }
    function save() {
        $(".modal-header").css({"display": "block"});
        $(".modal-body").css({"display": "block"});
        $(".modal-footer").css({"display": "block"});
        $(".modal-title").html("<span>Enter Measurement Details</span>");
        $(".modal-body").html("<div class='row'><p class='err'></p><div class='col-md-6 form-group'><label>Enter Measurement Name</label>" +
            "<input type='text' placeholder='Measurement Name' class='form-control' id='meas_name'/></div></div>");
        $(".modal-footer").html("<input type='button' value='Cancel' class='btn btn-default' data-dismiss='modal'/>" +
            "<input type='button' value='Save Now' class='btn btn-primary' onclick='saveas()' />");
        $("#modal").modal("show");
    }
    function saveas() {
        var meas_name = $("#meas_name").val();
        if (meas_name == "") {
            $(".err").html("Please Enter Measurement Name First");
            return false;
        }
        var user_id = $("#user_id").val();
        if (user_id == "") {
            $(".err").html("Session Expired Please Login Again....");
            return false;
        }
        var unit_type = $("#final_shirt_collar").text().split(" ")[1];
        var shirt_collar = $("#final_shirt_collar").text().split(" ")[0];
        var shirt_chest = $("#final_shirt_chest").text().split(" ")[0];
        var jacket_chest = $("#final_jacket_chest").text().split(" ")[0];
        var vest_chest = $("#final_vest_chest").text().split(" ")[0];
        var shirt_stomach = $("#final_shirt_stomach").text().split(" ")[0];
        var jacket_stomach = $("#final_jacket_stomach").text().split(" ")[0];
        var vest_stomach = $("#final_vest_stomach").text().split(" ")[0];
        var shirt_seat = $("#final_shirt_seat").text().split(" ")[0];
        var jacket_seat = $("#final_jacket_seat").text().split(" ")[0];
        var pant_seat = $("#final_pant_seat").text().split(" ")[0];
        var pant1_seat = $("#final_pant1_seat").text().split(" ")[0];
        var pant11_seat = $("#final_pant11_seat").text().split(" ")[0];
        var pant2_seat = $("#final_pant2_seat").text().split(" ")[0];
        var pant3_seat = $("#final_pant3_seat").text().split(" ")[0];
        var shirt_bicep = $("#final_shirt_bicep").text().split(" ")[0];
        var jacket_bicep = $("#final_jacket_bicep").text().split(" ")[0];
        var shirt_back_shoulder = $("#final_shirt_back_shoulder").text().split(" ")[0];
        var jacket_back_shoulder = $("#final_jacket_back_shoulder").text().split(" ")[0];
        var shirt_front_shoulder = $("#final_shirt_front_shoulder").text().split(" ")[0];
        var jacket_front_shoulder = $("#final_jacket_front_shoulder").text().split(" ")[0];
        var shirt_sleeve_left = $("#final_shirt_sleeve_left").text().split(" ")[0];
        var jacket_sleeve_left = $("#final_jacket_sleeve_left").text().split(" ")[0];
        var shirt_sleeve_right = $("#final_shirt_sleeve_right").text().split(" ")[0];
        var jacket_sleeve_right = $("#final_jacket_sleeve_right").text().split(" ")[0];
        var pant_thigh = $("#final_pant_thigh").text().split(" ")[0];
        var shirt_wrist = $("#final_shirt_wrist").text().split(" ")[0];
        var jacket_wrist = $("#final_jacket_wrist").text().split(" ")[0];
        var pant_waist = $("#final_pant_waist").text().split(" ")[0];
        var pant_calf = $("#final_pant_calf").text().split(" ")[0];
        var pant_back_waist_height = $("#final_pant_b_waist").text().split(" ")[0];
        var pant_front_waist_height = $("#final_pant_f_waist").text().split(" ")[0];
        var pant_knee = $("#final_pant_knee").text().split(" ")[0];
        var shirt_back_jacket_length = $("#final_shirt_jacket").text().split(" ")[0];
        var jacket_back_jacket_length = $("#final_jacket_jacket").text().split(" ")[0];
        var vest_back_jacket_length = $("#final_vest_jacket").text().split(" ")[0];
        var pant_u_rise = $("#final_pant_rise").text().split(" ")[0];
        var pant_pant_left_outseam = $("#final_pant_p_left").text().split(" ")[0];
        var pant_pant_right_outseam = $("#final_pant_p_right").text().split(" ")[0];
        var pant_bottom = $("#final_pant_bottom").text().split(" ")[0];
        var url = 'api/registerUser.php';
        $.post(url,
            {
                'type': 'storeFinalMeasurement',
                'shirt_collar': shirt_collar,
                'shirt_chest': shirt_chest,
                'jacket_chest': jacket_chest,
                'vest_chest': vest_chest,
                'shirt_stomach': shirt_stomach,
                'jacket_stomach': jacket_stomach,
                'vest_stomach': vest_stomach,
                'shirt_seat': shirt_seat,
                'jacket_seat': jacket_seat,
                'pant_seat': pant_seat,
                'pant1_seat': pant1_seat,
                'pant11_seat': pant11_seat,
                'pant2_seat': pant2_seat,
                'pant3_seat': pant3_seat,
                'shirt_bicep': shirt_bicep,
                'jacket_bicep': jacket_bicep,
                'shirt_back_shoulder': shirt_back_shoulder,
                'jacket_back_shoulder': jacket_back_shoulder,
                'shirt_front_shoulder': shirt_front_shoulder,
                'jacket_front_shoulder': jacket_front_shoulder,
                'shirt_sleeve_left': shirt_sleeve_left,
                'jacket_sleeve_left': jacket_sleeve_left,
                'shirt_sleeve_right': shirt_sleeve_right,
                'jacket_sleeve_right': jacket_sleeve_right,
                'pant_thigh': pant_thigh,
                'shirt_wrist': shirt_wrist,
                'jacket_wrist': jacket_wrist,
                'pant_calf': pant_calf,
                'pant_back_waist_height': pant_back_waist_height,
                'pant_front_waist_height': pant_front_waist_height,
                'pant_knee': pant_knee,
                'shirt_back_jacket_length': shirt_back_jacket_length,
                'jacket_back_jacket_length': jacket_back_jacket_length,
                'vest_back_jacket_length': vest_back_jacket_length,
                'pant_u_rise': pant_u_rise,
                'pant_pant_left_outseam': pant_pant_left_outseam,
                'pant_pant_right_outseam': pant_pant_right_outseam,
                'pant_bottom': pant_bottom,
                'meas_name': meas_name,
                'pant_waist': pant_waist,
                'user_id': user_id,
                'unit_type': unit_type
            },
            function (data) {
                var Status = data.Status;
                if (Status == "Success") {
                    $("#modal").modal("hide");
                    location.reload();
                } else {
                    $(".err").html(data.Message);
                    return false;
                }
            }).fail(function () {
            $(".err").html("Server Error !!! Please Try After Some Time...");
            return false;
        });
    }
    function checkTwinData(){
        $("#jacketBox").show();
        var user_id = $("#user_id").val();
        if(user_id == "" || user_id == undefined){
            showMessage("User id should not be blank", 'red');
            return false;
        }
        var url = "api/registerUser.php";
        $(".loaderdiv").removeClass("loaderhidden");
        $(".loaderdiv").addClass("block");
        $.post(url,{"type":"getStyleGenieData", "user_id":user_id},function(data){
            var status = data.Status;
            var userData = data.userData;
            if(status == "Success"){
                var chest               =     (userData.chest);
                var collar              =     (userData.collar);
                var stomach             =     (userData.stomach);
                var bicep               =     (userData.bicep);
                var waist               =     (userData.waist);
                var u_rise              =     (userData.u_rise);
                var thigh               =     (userData.thigh);
                var back_jacket_length  =     (userData.back_jacket_length);
                var back_shoulder       =     (userData.back_sholuder);
                var front_shoulder      =     (userData.front_shoulder);
                var shoulder            =     ((parseFloat(back_shoulder) + parseFloat(front_shoulder))/2);
                if(userData.unit_type == "Inches"){
                    var chest               =     (userData.chest * 2.56).toFixed(2);
                    var collar              =     (userData.collar * 2.56).toFixed(2);
                    var stomach             =     (userData.stomach * 2.56).toFixed(2);
                    var bicep               =     (userData.bicep * 2.56).toFixed(2);
                    var waist               =     (userData.waist * 2.56).toFixed(2);
                    var u_rise              =     (userData.u_rise * 2.56).toFixed(2);
                    var thigh               =     (userData.thigh * 2.56).toFixed(2);
                    var back_jacket_length  =     (userData.back_jacket_length * 2.56).toFixed(2);
                    var back_shoulder       =     (userData.back_sholuder * 2.56).toFixed(2);
                    var front_shoulder      =     (userData.front_shoulder * 2.56).toFixed(2);
                    var shoulder            =     ((parseFloat(back_shoulder) + parseFloat(front_shoulder))/2).toFixed(2);
                }

                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");






            }
            else
            {
                showMessage(data.Message, 'red');
                $(".loaderdiv").removeClass("block");
                $(".loaderdiv").addClass("loaderhidden");
            }
        });
    }
    function viewSaved1(){
        var user_id = $("#user_id").val();
        var url = "api/registerUser.php";
        $.post(url,{"type":"getAllMeasurement","user_id":user_id},function(data){
            var Status = data.Status;
            if(Status == "Success"){
                var measurements = data.measurements;
                var datashow = "<div class='row'><div class='container-fluid'><table class='table table-bordered'><tr>" +
                    "<th>S.No</th><th>Measurement Name</th><th>Added On</th><th>Action</th>";
                for(var i=0;i<measurements.length;i++){
                    var date = new Date(measurements[i].added_on*1000);
                    var year    = date.getFullYear();
                    var month   = date.getMonth()+1;
                    var dated     = date.getDate();
                    var added_on = dated+"/"+month+"/"+year;
                    datashow += "<tr><td>"+(i+1)+"</td><td>"+measurements[i].meas_name+"</td><td>"+added_on+"</td>" +
                        "<td><input type='button' value='View Measurement' class='btn btn-danger btn-sm' " +
                        "onclick=getData1('"+measurements[i].measurement_id+"','"+user_id+"') /></td></tr>";
                }
                datashow+="</table></div></div>";
                $(".modal-title").html("<span style='color:green'>Select Measurement File</span>");
                $(".modal-body").html(datashow);
                $(".modal-footer").css("display","none");
                $("#modal").modal("show");
            }
            else{
                showMessage("Not Any Measurement Saved Yet","red");
            }
        }).fail(function(){
            showMessage("Server Error !!! Please Try After Some Time","red");
        });
    }
    function switchMatching(tab){
        $(".tabs").removeClass("active");
        $("#"+tab+"allow").addClass("active");
        $(".tabsData").hide();
        $("#"+tab+"Box").show();
        switchMatching2(tab);
    }
    /*for range increase or decrease start here*/

    var jacket_specification = [];
    var vest_specification = [];
    var shirt_specification = [];
    var pant_specification = [];

    /*for range increase or decrease end   here*/
    function switchMatching2(tab){
        $(".allowtable").css("display","none");
        $("#"+tab+"_table").css("display","block");
        $(".tabs").removeClass("active");
        $("#"+tab+"allow").addClass("active");
        var url = "api/registerUser.php";
        $.post(url, {"type": "getMatchingData", "size_type": tab}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var sizeMatchingData = data.sizeMatchingData;
                var table = '<table class="table table-bordered" style="width:100%;overflow-x:scroll">';
                var tr = "";
                for (var j = 0; j < sizeMatchingData.length; j++) {
                    if(sizeMatchingData[j].title == "jacket_specification"){
                        tr = tr +'<tr><td>S.no</td><td>Jacket Specification</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_1" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+'>R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_2" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_3" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_4" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_5" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_6" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_7" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_8" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_9" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_10" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_11" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                            '<td><input class="custom-input size_field" id="jacket_reading_12" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                            '</tr>';
                    }
                    else if(sizeMatchingData[j].title == "pant_specification"){
                        tr = tr +'<tr><td>S.no</td><td>Pant Specification</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_1" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_2" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_3" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_4" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_5" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_6" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_7" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_8" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_9" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_10" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_11" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                            '<td><input class="custom-input size_field" id="pant_reading_12" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                            '</tr>';
                    }
                    else if(sizeMatchingData[j].title == "vest_specification"){
                        tr = tr +'<tr><td>S.no</td><td>Vest Specification</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_1" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_2" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_3" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_4" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_5" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_6" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_7" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_8" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_9" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_10" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_11" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' >R</td>' +
                            '<td><input class="custom-input size_field" id="vest_reading_12" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' >R</td>' +
                            '</tr>';
                    }
                    else if(sizeMatchingData[j].title == "shirt_specification"){
                        tr = tr +'<tr><td>S.no</td><td>Shirt Specification</td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_1" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1") type="text" value='+sizeMatchingData[j].reading_1+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_2" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2") type="text" value='+sizeMatchingData[j].reading_2+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_3" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3") type="text" value='+sizeMatchingData[j].reading_3+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_4" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4") type="text" value='+sizeMatchingData[j].reading_4+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_5" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5") type="text" value='+sizeMatchingData[j].reading_5+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_6" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6") type="text" value='+sizeMatchingData[j].reading_6+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_7" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7") type="text" value='+sizeMatchingData[j].reading_7+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_8" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8") type="text" value='+sizeMatchingData[j].reading_8+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_9" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9") type="text" value='+sizeMatchingData[j].reading_9+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_10" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+sizeMatchingData[j].reading_10+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_11" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+sizeMatchingData[j].reading_11+' ></td>' +
                            '<td><input class="custom-input size_field" id="shirt_reading_12" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+sizeMatchingData[j].reading_12+' ></td>' +
                            '</tr>';
                    }
                    else
                    {
                        var reading_1 = sizeMatchingData[j].reading_1;
                        var reading_2 = sizeMatchingData[j].reading_2;
                        var reading_3 = sizeMatchingData[j].reading_3;
                        var reading_4 = sizeMatchingData[j].reading_4;
                        var reading_5 = sizeMatchingData[j].reading_5;
                        var reading_6 = sizeMatchingData[j].reading_6;
                        var reading_7 = sizeMatchingData[j].reading_7;
                        var reading_8 = sizeMatchingData[j].reading_8;
                        var reading_9 = sizeMatchingData[j].reading_9;
                        var reading_10 = sizeMatchingData[j].reading_10;
                        var reading_11 = sizeMatchingData[j].reading_11;
                        var reading_12 = sizeMatchingData[j].reading_12;
                        if($("#params").val() == "Inches"){
                            reading_1       =   (reading_1   / 2.56).toFixed(0);
                            reading_2       =   (reading_2   / 2.56).toFixed(0);
                            reading_3       =   (reading_3   / 2.56).toFixed(0);
                            reading_4       =   (reading_4   / 2.56).toFixed(0);
                            reading_5       =   (reading_5   / 2.56).toFixed(0);
                            reading_6       =   (reading_6   / 2.56).toFixed(0);
                            reading_7       =   (reading_7   / 2.56).toFixed(0);
                            reading_8       =   (reading_8   / 2.56).toFixed(0);
                            reading_9       =   (reading_9   / 2.56).toFixed(0);
                            reading_10      =   (reading_10  / 2.56).toFixed(0);
                            reading_11      =   (reading_11  / 2.56).toFixed(0);
                            reading_12      =   (reading_12  / 2.56).toFixed(0);
                        }
                        tr = tr +'<tr><td>'+j+'</td><td>'+sizeMatchingData[j].title+'</td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_1")  type="text" value='+reading_1+'></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_2")  type="text" value='+reading_2+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_3")  type="text" value='+reading_3+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_4")  type="text" value='+reading_4+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_5")  type="text" value='+reading_5+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_6")  type="text" value='+reading_6+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_7")  type="text" value='+reading_7+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_8")  type="text" value='+reading_8+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_9")  type="text" value='+reading_9+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_10") type="text" value='+reading_10+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_11") type="text" value='+reading_11+' ></td>' +
                            '<td><input class="custom-input size_field" onchange=updateValue(this,"'+sizeMatchingData[j].id+'","reading_12") type="text" value='+reading_12+' ></td>' +
                            '</tr>';
                    }
                }
                $("#"+tab+"_table").html(table+tr+"</table>");


            } else {
                showMessage(data.Message, "red");
            }
        });

        /*for specification insert when tab is change start here */
        var params = $("#params").val();
        if(params == "Inches"){
                /*svalue = value.split(";")[0];*/
            var speci_jacket_chest                     = (parseFloat(($("#final_jacket_chest").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_stomach                   = (parseFloat(($("#final_jacket_stomach").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_bicep                     = (parseFloat(($("#final_jacket_bicep").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_pant_waist                       = (parseFloat(($("#final_pant_waist").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_pant_u_rise                      = (parseFloat(($("#final_pant_rise").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_pant_thigh                       = (parseFloat(($("#final_pant_thigh").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_collar                     = (parseFloat(($("#final_shirt_collar").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_front_shoulder            = (parseFloat(($("#final_jacket_front_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_back_shoulder             = (parseFloat(($("#final_jacket_back_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_jacket_back                      = (parseFloat(($("#final_jacket_jacket").html()).split(" ")[0]) * 2.54).toFixed(1);

            var speci_shirt_chest                      = (parseFloat(($("#final_shirt_chest").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_stomach                    = (parseFloat(($("#final_shirt_stomach").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_bicep                      = (parseFloat(($("#final_shirt_bicep").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_front_shoulder             = (parseFloat(($("#final_shirt_front_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_back_shoulder              = (parseFloat(($("#final_shirt_back_shoulder").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_shirt_back                       = (parseFloat(($("#final_shirt_jacket").html()).split(" ")[0]) * 2.54).toFixed(1);

            var speci_vest_chest                       = (parseFloat(($("#final_vest_chest").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_vest_stomach                       = (parseFloat(($("#final_vest_stomach").html()).split(" ")[0]) * 2.54).toFixed(1);
            var speci_vest_back                       = (parseFloat(($("#final_vest_jacket").html()).split(" ")[0]) * 2.54).toFixed(1);

        }
        else if(params == "Cm"){
            var speci_jacket_chest             = ($("#final_jacket_chest").html()).split(" ")[0];
            var speci_jacket_stomach           = ($("#final_jacket_stomach").html()).split(" ")[0];
            var speci_jacket_bicep             = ($("#final_jacket_bicep").html()).split(" ")[0];
            var speci_pant_waist               = ($("#final_pant_waist").html()).split(" ")[0];
            var speci_pant_u_rise              = ($("#final_pant_rise").html()).split(" ")[0];
            var speci_pant_thigh               = ($("#final_pant_thigh").html()).split(" ")[0];
            var speci_shirt_collar             = ($("#final_shirt_collar").html()).split(" ")[0];
            var speci_jacket_front_shoulder    = ($("#final_jacket_front_shoulder").html()).split(" ")[0];
            var speci_jacket_back_shoulder     = ($("#final_jacket_back_shoulder").html()).split(" ")[0];
            var speci_jacket_back              = ($("#final_jacket_jacket").html()).split(" ")[0];

            var speci_shirt_chest              = ($("#final_shirt_chest").html()).split(" ")[0];
            var speci_shirt_stomach            = ($("#final_shirt_stomach").html()).split(" ")[0];
            var speci_shirt_bicep              = ($("#final_shirt_bicep").html()).split(" ")[0];
            var speci_shirt_front_shoulder     = ($("#final_shirt_front_shoulder").html()).split(" ")[0];
            var speci_shirt_back_shoulder      = ($("#final_shirt_back_shoulder").html()).split(" ")[0];
            var speci_shirt_back               = ($("#final_shirt_jacket").html()).split(" ")[0];


            var speci_vest_chest               = ($("#final_vest_chest").html()).split(" ")[0];
            var speci_vest_stomach               = ($("#final_vest_stomach").html()).split(" ")[0];
            var speci_vest_back               = ($("#final_vest_jacket").html()).split(" ")[0];
        }
        if(tab == "jacket"){
            var speci_type              = "jacket_specification";
            get_specification(speci_type,speci_jacket_chest,speci_jacket_stomach,speci_jacket_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_jacket_back);
        }
        else if(tab == "pant"){
            var speci_type              = "pant_specification";
            get_specification(speci_type,speci_jacket_chest,speci_jacket_stomach,speci_jacket_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_jacket_back);

        }
        else if(tab == "vest"){
            var speci_type              = "vest_specification";
            get_specification(speci_type,speci_vest_chest,speci_vest_stomach,speci_jacket_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_jacket_front_shoulder,speci_jacket_back_shoulder,speci_vest_back);

        }
        else if(tab == "shirt"){
            var speci_type              = "shirt_specification";
            get_specification(speci_type,speci_shirt_chest,speci_shirt_stomach,speci_shirt_bicep,speci_pant_waist,speci_pant_u_rise,
                speci_pant_thigh,speci_shirt_collar,speci_shirt_front_shoulder,speci_shirt_back_shoulder,speci_shirt_back);

        }
        /*for specification insert when tab is change end   here */
    }


    function getAllMatchingData(){
        var url = "api/registerUser.php";
        $.post(url, {"type": "getAllMatchingData"}, function (data) {
            var status = data.Status;
            if (status == "Success") {
                var sizeMatchingData = data.sizeMatchingData;
                /*for save data increment and decrement of  range start here */
                for (var k = 0; k < sizeMatchingData.length; k++) {
                    if(sizeMatchingData[k].title == "jacket_specification"){
                        jacket_specification =[];
                        jacket_specification.push(sizeMatchingData[k].reading_1,sizeMatchingData[k].reading_2,
                            sizeMatchingData[k].reading_3,sizeMatchingData[k].reading_4,sizeMatchingData[k].reading_5,
                            sizeMatchingData[k].reading_6,sizeMatchingData[k].reading_7,sizeMatchingData[k].reading_8,
                            sizeMatchingData[k].reading_9,sizeMatchingData[k].reading_10,sizeMatchingData[k].reading_11,sizeMatchingData[k].reading_12);
                    }
                    else if(sizeMatchingData[k].title == "pant_specification"){
                        pant_specification =[];
                        pant_specification.push(sizeMatchingData[k].reading_1,sizeMatchingData[k].reading_2,
                            sizeMatchingData[k].reading_3,sizeMatchingData[k].reading_4,sizeMatchingData[k].reading_5,
                            sizeMatchingData[k].reading_6,sizeMatchingData[k].reading_7,sizeMatchingData[k].reading_8,
                            sizeMatchingData[k].reading_9,sizeMatchingData[k].reading_10,sizeMatchingData[k].reading_11,sizeMatchingData[k].reading_12);
                    }
                    else if(sizeMatchingData[k].title == "vest_specification"){
                        vest_specification = [];
                        vest_specification.push(sizeMatchingData[k].reading_1,sizeMatchingData[k].reading_2,
                            sizeMatchingData[k].reading_3,sizeMatchingData[k].reading_4,sizeMatchingData[k].reading_5,
                            sizeMatchingData[k].reading_6,sizeMatchingData[k].reading_7,sizeMatchingData[k].reading_8,
                            sizeMatchingData[k].reading_9,sizeMatchingData[k].reading_10,sizeMatchingData[k].reading_11,sizeMatchingData[k].reading_12);

                    }
                    else if(sizeMatchingData[k].title == "shirt_specification"){
                        shirt_specification = [];
                        shirt_specification.push(sizeMatchingData[k].reading_1,sizeMatchingData[k].reading_2,
                            sizeMatchingData[k].reading_3,sizeMatchingData[k].reading_4,sizeMatchingData[k].reading_5,
                            sizeMatchingData[k].reading_6,sizeMatchingData[k].reading_7,sizeMatchingData[k].reading_8,
                            sizeMatchingData[k].reading_9,sizeMatchingData[k].reading_10,sizeMatchingData[k].reading_11,sizeMatchingData[k].reading_12);
                    }
                }
                /*for save data increment and decrement of range  end   here */
            }
            else{
                //alert(status);
            }
        });
    }
    getAllMatchingData();

    /*range increment and decrement start here*/
    function rangeIncDec(ope,speci_type,fieldId){
        var value = $("#"+fieldId).html();
        var speci_value = 0;

        if(ope == "minus"){
            switch(speci_type){
                case "jacket_specification":
                    jacket_specification.sort(function(a, b){return a - b})
                    for(var i=0; i < jacket_specification.length ; i++){
                        if(jacket_specification[i] ==  value){
                            var curr_index = i;
                            if(curr_index>0) {
                                curr_index--;
                                speci_value  =jacket_specification[curr_index];
                            }
                            else
                            {
                                speci_value = jacket_specification[i];
                            }
                        }
                    }
                    break;
                case "vest_specification":
                    vest_specification.sort(function(a, b){return a - b});
                    for(var i=0; i < vest_specification.length ; i++){
                        if(vest_specification[i] ==  parseInt(value)){
                            var curr_index = i;
                            if(curr_index>0) {
                                curr_index--;
                                speci_value  =vest_specification[curr_index];
                            }
                            else
                            {
                                speci_value = vest_specification[i];
                            }
                        }
                        else{
                            //alert("else  == "+vest_specification[i] +" == "+  value);
                        }
                    }
                    break;
                case "shirt_specification":
                    /*shirt_specification.sort(function(a, b){return a - b});*/
                    for(var i=0; i < shirt_specification.length ; i++){
                        if(shirt_specification[i] ==  value){
                            var curr_index = i;
                            if(curr_index>0) {
                                curr_index--;
                                speci_value  =shirt_specification[curr_index];
                            }
                            else
                            {
                                speci_value = shirt_specification[i];
                            }
                        }
                    }
                    break;
                case "pant_specification":
                    pant_specification.sort(function(a, b){return a - b})
                    for(var i=0; i < pant_specification.length ; i++){
                        if(pant_specification[i] ==  value){
                            var curr_index = i;
                            if(curr_index>0) {
                                curr_index--;
                                speci_value  =pant_specification[curr_index];
                            }
                            else
                            {
                                speci_value = pant_specification[i];
                            }
                        }
                    }
                    break;
            }
        }
        else if(ope == "plus"){
            switch(speci_type){
                case "jacket_specification":
                    jacket_specification.sort(function(a, b){return a - b})
                    for(var i=0; i < jacket_specification.length ; i++){
                        if(jacket_specification[i] ==  value){
                            var curr_index = i;
                            if(curr_index < jacket_specification[i]) {
                                curr_index++;
                                speci_value  =jacket_specification[curr_index];
                            }
                            else
                            {
                                speci_value = jacket_specification[i];
                            }
                        }
                    }
                    break;
                case "vest_specification":

                    vest_specification.sort(function(a, b){return a - b});
                    for(var i=0; i <vest_specification.length; i++){
                        if(vest_specification[i] ==  parseInt(value)){
                            var curr_index = i;
                            if(curr_index < vest_specification.length-1) {
                                curr_index++;
                                speci_value  =vest_specification[curr_index];
                            }
                            else
                            {
                                speci_value = vest_specification[i];
                            }
                        }
                    }
                    break;
                case "shirt_specification":
                    /*shirt_specification.sort(function(a, b){return a - b});*/
                    for(var i=0; i < shirt_specification.length ; i++){
                        if(shirt_specification[i] ==  value){

                            var curr_index = i;
                            if(curr_index < shirt_specification.length) {
                                curr_index++;
                                speci_value  =shirt_specification[curr_index];
                            }
                            else
                            {
                                speci_value = shirt_specification[i];
                            }
                        }
                        else{
                        }
                    }
                    break;
                case "pant_specification":
                    pant_specification.sort(function(a, b){return a - b});
                    for(var i=0; i < pant_specification.length ; i++){
                        if(pant_specification[i] ==  value){
                            var curr_index = i;
                            if(curr_index < pant_specification.length) {
                                curr_index++;
                                speci_value  =pant_specification[curr_index];
                            }
                            else
                            {
                                speci_value = pant_specification[i];
                            }
                        }
                    }
                    break;
            }
        }
        $("#"+fieldId).html(speci_value);
    }
    /*range increment and decrement end here*/

    switchMatching2("jacket");
    checkTwinData();
    viewSaved1();
</script>